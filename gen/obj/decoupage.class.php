<?php
//$Id$ 
//gen openMairie le 05/06/2018 12:52

require_once "../obj/om_dbform.class.php";

class decoupage_gen extends om_dbform {

    protected $_absolute_class_name = "decoupage";

    var $table = "decoupage";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "bureau",
        "code_voie",
        "dernier_impair",
        "dernier_pair",
        "id",
        "premier_impair",
        "premier_pair"
    );
    
    var $foreign_keys_extended = array(
        "bureau" => array("bureau", ),
        "voie" => array("voie", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("bureau");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "bureau",
            "code_voie",
            "premier_impair",
            "dernier_impair",
            "premier_pair",
            "dernier_pair",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau ORDER BY bureau.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_code_voie() {
        return "SELECT voie.code, voie.libelle_voie FROM ".DB_PREFIXE."voie ORDER BY voie.libelle_voie ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_code_voie_by_id() {
        return "SELECT voie.code, voie.libelle_voie FROM ".DB_PREFIXE."voie WHERE code = '<idx>'";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if (!is_numeric($val['bureau'])) {
            $this->valF['bureau'] = ""; // -> requis
        } else {
            $this->valF['bureau'] = $val['bureau'];
        }
        $this->valF['code_voie'] = $val['code_voie'];
        if (!is_numeric($val['premier_impair'])) {
            $this->valF['premier_impair'] = ""; // -> requis
        } else {
            $this->valF['premier_impair'] = $val['premier_impair'];
        }
        if (!is_numeric($val['dernier_impair'])) {
            $this->valF['dernier_impair'] = ""; // -> requis
        } else {
            $this->valF['dernier_impair'] = $val['dernier_impair'];
        }
        if (!is_numeric($val['premier_pair'])) {
            $this->valF['premier_pair'] = ""; // -> requis
        } else {
            $this->valF['premier_pair'] = $val['premier_pair'];
        }
        if (!is_numeric($val['dernier_pair'])) {
            $this->valF['dernier_pair'] = ""; // -> requis
        } else {
            $this->valF['dernier_pair'] = $val['dernier_pair'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("code_voie", "selecthiddenstatic");
            } else {
                $form->setType("code_voie", "select");
            }
            $form->setType("premier_impair", "text");
            $form->setType("dernier_impair", "text");
            $form->setType("premier_pair", "text");
            $form->setType("dernier_pair", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("code_voie", "selecthiddenstatic");
            } else {
                $form->setType("code_voie", "select");
            }
            $form->setType("premier_impair", "text");
            $form->setType("dernier_impair", "text");
            $form->setType("premier_pair", "text");
            $form->setType("dernier_pair", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("bureau", "selectstatic");
            $form->setType("code_voie", "selectstatic");
            $form->setType("premier_impair", "hiddenstatic");
            $form->setType("dernier_impair", "hiddenstatic");
            $form->setType("premier_pair", "hiddenstatic");
            $form->setType("dernier_pair", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("bureau", "selectstatic");
            $form->setType("code_voie", "selectstatic");
            $form->setType("premier_impair", "static");
            $form->setType("dernier_impair", "static");
            $form->setType("premier_pair", "static");
            $form->setType("dernier_pair", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('bureau','VerifNum(this)');
        $form->setOnchange('premier_impair','VerifNum(this)');
        $form->setOnchange('dernier_impair','VerifNum(this)');
        $form->setOnchange('premier_pair','VerifNum(this)');
        $form->setOnchange('dernier_pair','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("bureau", 11);
        $form->setTaille("code_voie", 10);
        $form->setTaille("premier_impair", 11);
        $form->setTaille("dernier_impair", 11);
        $form->setTaille("premier_pair", 11);
        $form->setTaille("dernier_pair", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("bureau", 11);
        $form->setMax("code_voie", 10);
        $form->setMax("premier_impair", 11);
        $form->setMax("dernier_impair", 11);
        $form->setMax("premier_pair", 11);
        $form->setMax("dernier_pair", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('bureau', __('bureau'));
        $form->setLib('code_voie', __('code_voie'));
        $form->setLib('premier_impair', __('premier_impair'));
        $form->setLib('dernier_impair', __('dernier_impair'));
        $form->setLib('premier_pair', __('premier_pair'));
        $form->setLib('dernier_pair', __('dernier_pair'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // bureau
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "bureau",
            $this->get_var_sql_forminc__sql("bureau"),
            $this->get_var_sql_forminc__sql("bureau_by_id"),
            false
        );
        // code_voie
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "code_voie",
            $this->get_var_sql_forminc__sql("code_voie"),
            $this->get_var_sql_forminc__sql("code_voie_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('bureau', $this->retourformulaire))
                $form->setVal('bureau', $idxformulaire);
            if($this->is_in_context_of_foreign_key('voie', $this->retourformulaire))
                $form->setVal('code_voie', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
