<?php
//$Id$ 
//gen openMairie le 25/09/2023 18:16

require_once "../obj/om_dbform.class.php";

class reu_scrutin_gen extends om_dbform {

    protected $_absolute_class_name = "reu_scrutin";

    var $table = "reu_scrutin";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "id",
        "om_collectivite"
    );
    var $unique_key = array(
      array("om_collectivite","referentiel_id"),
    );
    var $foreign_keys_extended = array(
        "arrets_liste" => array("arrets_liste", ),
        "canton" => array("canton", ),
        "circonscription" => array("circonscription", ),
        "om_collectivite" => array("om_collectivite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "type",
            "partiel",
            "libelle",
            "zone",
            "date_tour1",
            "date_tour2",
            "date_debut",
            "date_l30",
            "date_fin",
            "j5_livrable_demande_id",
            "j5_livrable_demande_date",
            "j5_livrable",
            "j5_livrable_date",
            "emarge_livrable_demande_id",
            "emarge_livrable_demande_date",
            "emarge_livrable",
            "emarge_livrable_date",
            "om_collectivite",
            "referentiel_id",
            "canton",
            "circonscription_legislative",
            "circonscription_metropolitaine",
            "arrets_liste",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_arrets_liste() {
        return "SELECT arrets_liste.arrets_liste, arrets_liste.livrable_demande_id FROM ".DB_PREFIXE."arrets_liste ORDER BY arrets_liste.livrable_demande_id ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_arrets_liste_by_id() {
        return "SELECT arrets_liste.arrets_liste, arrets_liste.livrable_demande_id FROM ".DB_PREFIXE."arrets_liste WHERE arrets_liste = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton() {
        return "SELECT canton.id, canton.libelle FROM ".DB_PREFIXE."canton ORDER BY canton.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton_by_id() {
        return "SELECT canton.id, canton.libelle FROM ".DB_PREFIXE."canton WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_circonscription_legislative() {
        return "SELECT circonscription.id, circonscription.libelle FROM ".DB_PREFIXE."circonscription ORDER BY circonscription.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_circonscription_legislative_by_id() {
        return "SELECT circonscription.id, circonscription.libelle FROM ".DB_PREFIXE."circonscription WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_circonscription_metropolitaine() {
        return "SELECT circonscription.id, circonscription.libelle FROM ".DB_PREFIXE."circonscription ORDER BY circonscription.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_circonscription_metropolitaine_by_id() {
        return "SELECT circonscription.id, circonscription.libelle FROM ".DB_PREFIXE."circonscription WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if ($val['type'] == "") {
            $this->valF['type'] = NULL;
        } else {
            $this->valF['type'] = $val['type'];
        }
        if ($val['partiel'] == 1 || $val['partiel'] == "t" || $val['partiel'] == "Oui") {
            $this->valF['partiel'] = true;
        } else {
            $this->valF['partiel'] = false;
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
            $this->valF['zone'] = $val['zone'];
        if ($val['date_tour1'] != "") {
            $this->valF['date_tour1'] = $this->dateDB($val['date_tour1']);
        } else {
            $this->valF['date_tour1'] = NULL;
        }
        if ($val['date_tour2'] != "") {
            $this->valF['date_tour2'] = $this->dateDB($val['date_tour2']);
        } else {
            $this->valF['date_tour2'] = NULL;
        }
        if ($val['date_debut'] != "") {
            $this->valF['date_debut'] = $this->dateDB($val['date_debut']);
        } else {
            $this->valF['date_debut'] = NULL;
        }
        if ($val['date_l30'] != "") {
            $this->valF['date_l30'] = $this->dateDB($val['date_l30']);
        } else {
            $this->valF['date_l30'] = NULL;
        }
        if ($val['date_fin'] != "") {
            $this->valF['date_fin'] = $this->dateDB($val['date_fin']);
        } else {
            $this->valF['date_fin'] = NULL;
        }
        if (!is_numeric($val['j5_livrable_demande_id'])) {
            $this->valF['j5_livrable_demande_id'] = NULL;
        } else {
            $this->valF['j5_livrable_demande_id'] = $val['j5_livrable_demande_id'];
        }
            $this->valF['j5_livrable_demande_date'] = $val['j5_livrable_demande_date'];
        if ($val['j5_livrable'] == "") {
            $this->valF['j5_livrable'] = NULL;
        } else {
            $this->valF['j5_livrable'] = $val['j5_livrable'];
        }
            $this->valF['j5_livrable_date'] = $val['j5_livrable_date'];
        if (!is_numeric($val['emarge_livrable_demande_id'])) {
            $this->valF['emarge_livrable_demande_id'] = NULL;
        } else {
            $this->valF['emarge_livrable_demande_id'] = $val['emarge_livrable_demande_id'];
        }
            $this->valF['emarge_livrable_demande_date'] = $val['emarge_livrable_demande_date'];
        if ($val['emarge_livrable'] == "") {
            $this->valF['emarge_livrable'] = NULL;
        } else {
            $this->valF['emarge_livrable'] = $val['emarge_livrable'];
        }
            $this->valF['emarge_livrable_date'] = $val['emarge_livrable_date'];
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        if (!is_numeric($val['referentiel_id'])) {
            $this->valF['referentiel_id'] = NULL;
        } else {
            $this->valF['referentiel_id'] = $val['referentiel_id'];
        }
        if (!is_numeric($val['canton'])) {
            $this->valF['canton'] = NULL;
        } else {
            $this->valF['canton'] = $val['canton'];
        }
        if (!is_numeric($val['circonscription_legislative'])) {
            $this->valF['circonscription_legislative'] = NULL;
        } else {
            $this->valF['circonscription_legislative'] = $val['circonscription_legislative'];
        }
        if (!is_numeric($val['circonscription_metropolitaine'])) {
            $this->valF['circonscription_metropolitaine'] = NULL;
        } else {
            $this->valF['circonscription_metropolitaine'] = $val['circonscription_metropolitaine'];
        }
        if (!is_numeric($val['arrets_liste'])) {
            $this->valF['arrets_liste'] = NULL;
        } else {
            $this->valF['arrets_liste'] = $val['arrets_liste'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            $form->setType("type", "text");
            $form->setType("partiel", "checkbox");
            $form->setType("libelle", "text");
            $form->setType("zone", "textarea");
            $form->setType("date_tour1", "date");
            $form->setType("date_tour2", "date");
            $form->setType("date_debut", "date");
            $form->setType("date_l30", "date");
            $form->setType("date_fin", "date");
            $form->setType("j5_livrable_demande_id", "text");
            $form->setType("j5_livrable_demande_date", "text");
            $form->setType("j5_livrable", "text");
            $form->setType("j5_livrable_date", "text");
            $form->setType("emarge_livrable_demande_id", "text");
            $form->setType("emarge_livrable_demande_date", "text");
            $form->setType("emarge_livrable", "text");
            $form->setType("emarge_livrable_date", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("referentiel_id", "text");
            if ($this->is_in_context_of_foreign_key("canton", $this->retourformulaire)) {
                $form->setType("canton", "selecthiddenstatic");
            } else {
                $form->setType("canton", "select");
            }
            if ($this->is_in_context_of_foreign_key("circonscription", $this->retourformulaire)) {
                $form->setType("circonscription_legislative", "selecthiddenstatic");
            } else {
                $form->setType("circonscription_legislative", "select");
            }
            if ($this->is_in_context_of_foreign_key("circonscription", $this->retourformulaire)) {
                $form->setType("circonscription_metropolitaine", "selecthiddenstatic");
            } else {
                $form->setType("circonscription_metropolitaine", "select");
            }
            if ($this->is_in_context_of_foreign_key("arrets_liste", $this->retourformulaire)) {
                $form->setType("arrets_liste", "selecthiddenstatic");
            } else {
                $form->setType("arrets_liste", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            $form->setType("type", "text");
            $form->setType("partiel", "checkbox");
            $form->setType("libelle", "text");
            $form->setType("zone", "textarea");
            $form->setType("date_tour1", "date");
            $form->setType("date_tour2", "date");
            $form->setType("date_debut", "date");
            $form->setType("date_l30", "date");
            $form->setType("date_fin", "date");
            $form->setType("j5_livrable_demande_id", "text");
            $form->setType("j5_livrable_demande_date", "text");
            $form->setType("j5_livrable", "text");
            $form->setType("j5_livrable_date", "text");
            $form->setType("emarge_livrable_demande_id", "text");
            $form->setType("emarge_livrable_demande_date", "text");
            $form->setType("emarge_livrable", "text");
            $form->setType("emarge_livrable_date", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("referentiel_id", "text");
            if ($this->is_in_context_of_foreign_key("canton", $this->retourformulaire)) {
                $form->setType("canton", "selecthiddenstatic");
            } else {
                $form->setType("canton", "select");
            }
            if ($this->is_in_context_of_foreign_key("circonscription", $this->retourformulaire)) {
                $form->setType("circonscription_legislative", "selecthiddenstatic");
            } else {
                $form->setType("circonscription_legislative", "select");
            }
            if ($this->is_in_context_of_foreign_key("circonscription", $this->retourformulaire)) {
                $form->setType("circonscription_metropolitaine", "selecthiddenstatic");
            } else {
                $form->setType("circonscription_metropolitaine", "select");
            }
            if ($this->is_in_context_of_foreign_key("arrets_liste", $this->retourformulaire)) {
                $form->setType("arrets_liste", "selecthiddenstatic");
            } else {
                $form->setType("arrets_liste", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("type", "hiddenstatic");
            $form->setType("partiel", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("zone", "hiddenstatic");
            $form->setType("date_tour1", "hiddenstatic");
            $form->setType("date_tour2", "hiddenstatic");
            $form->setType("date_debut", "hiddenstatic");
            $form->setType("date_l30", "hiddenstatic");
            $form->setType("date_fin", "hiddenstatic");
            $form->setType("j5_livrable_demande_id", "hiddenstatic");
            $form->setType("j5_livrable_demande_date", "hiddenstatic");
            $form->setType("j5_livrable", "hiddenstatic");
            $form->setType("j5_livrable_date", "hiddenstatic");
            $form->setType("emarge_livrable_demande_id", "hiddenstatic");
            $form->setType("emarge_livrable_demande_date", "hiddenstatic");
            $form->setType("emarge_livrable", "hiddenstatic");
            $form->setType("emarge_livrable_date", "hiddenstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
            $form->setType("referentiel_id", "hiddenstatic");
            $form->setType("canton", "selectstatic");
            $form->setType("circonscription_legislative", "selectstatic");
            $form->setType("circonscription_metropolitaine", "selectstatic");
            $form->setType("arrets_liste", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("type", "static");
            $form->setType("partiel", "checkboxstatic");
            $form->setType("libelle", "static");
            $form->setType("zone", "textareastatic");
            $form->setType("date_tour1", "datestatic");
            $form->setType("date_tour2", "datestatic");
            $form->setType("date_debut", "datestatic");
            $form->setType("date_l30", "datestatic");
            $form->setType("date_fin", "datestatic");
            $form->setType("j5_livrable_demande_id", "static");
            $form->setType("j5_livrable_demande_date", "static");
            $form->setType("j5_livrable", "static");
            $form->setType("j5_livrable_date", "static");
            $form->setType("emarge_livrable_demande_id", "static");
            $form->setType("emarge_livrable_demande_date", "static");
            $form->setType("emarge_livrable", "static");
            $form->setType("emarge_livrable_date", "static");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("referentiel_id", "static");
            $form->setType("canton", "selectstatic");
            $form->setType("circonscription_legislative", "selectstatic");
            $form->setType("circonscription_metropolitaine", "selectstatic");
            $form->setType("arrets_liste", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('date_tour1','fdate(this)');
        $form->setOnchange('date_tour2','fdate(this)');
        $form->setOnchange('date_debut','fdate(this)');
        $form->setOnchange('date_l30','fdate(this)');
        $form->setOnchange('date_fin','fdate(this)');
        $form->setOnchange('j5_livrable_demande_id','VerifNum(this)');
        $form->setOnchange('emarge_livrable_demande_id','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
        $form->setOnchange('referentiel_id','VerifNum(this)');
        $form->setOnchange('canton','VerifNum(this)');
        $form->setOnchange('circonscription_legislative','VerifNum(this)');
        $form->setOnchange('circonscription_metropolitaine','VerifNum(this)');
        $form->setOnchange('arrets_liste','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("type", 30);
        $form->setTaille("partiel", 1);
        $form->setTaille("libelle", 30);
        $form->setTaille("zone", 80);
        $form->setTaille("date_tour1", 12);
        $form->setTaille("date_tour2", 12);
        $form->setTaille("date_debut", 12);
        $form->setTaille("date_l30", 12);
        $form->setTaille("date_fin", 12);
        $form->setTaille("j5_livrable_demande_id", 11);
        $form->setTaille("j5_livrable_demande_date", 8);
        $form->setTaille("j5_livrable", 30);
        $form->setTaille("j5_livrable_date", 8);
        $form->setTaille("emarge_livrable_demande_id", 11);
        $form->setTaille("emarge_livrable_demande_date", 8);
        $form->setTaille("emarge_livrable", 30);
        $form->setTaille("emarge_livrable_date", 8);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("referentiel_id", 11);
        $form->setTaille("canton", 11);
        $form->setTaille("circonscription_legislative", 11);
        $form->setTaille("circonscription_metropolitaine", 11);
        $form->setTaille("arrets_liste", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("type", 250);
        $form->setMax("partiel", 1);
        $form->setMax("libelle", 250);
        $form->setMax("zone", 6);
        $form->setMax("date_tour1", 12);
        $form->setMax("date_tour2", 12);
        $form->setMax("date_debut", 12);
        $form->setMax("date_l30", 12);
        $form->setMax("date_fin", 12);
        $form->setMax("j5_livrable_demande_id", 11);
        $form->setMax("j5_livrable_demande_date", 8);
        $form->setMax("j5_livrable", 100);
        $form->setMax("j5_livrable_date", 8);
        $form->setMax("emarge_livrable_demande_id", 11);
        $form->setMax("emarge_livrable_demande_date", 8);
        $form->setMax("emarge_livrable", 100);
        $form->setMax("emarge_livrable_date", 8);
        $form->setMax("om_collectivite", 11);
        $form->setMax("referentiel_id", 11);
        $form->setMax("canton", 11);
        $form->setMax("circonscription_legislative", 11);
        $form->setMax("circonscription_metropolitaine", 11);
        $form->setMax("arrets_liste", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('type', __('type'));
        $form->setLib('partiel', __('partiel'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('zone', __('zone'));
        $form->setLib('date_tour1', __('date_tour1'));
        $form->setLib('date_tour2', __('date_tour2'));
        $form->setLib('date_debut', __('date_debut'));
        $form->setLib('date_l30', __('date_l30'));
        $form->setLib('date_fin', __('date_fin'));
        $form->setLib('j5_livrable_demande_id', __('j5_livrable_demande_id'));
        $form->setLib('j5_livrable_demande_date', __('j5_livrable_demande_date'));
        $form->setLib('j5_livrable', __('j5_livrable'));
        $form->setLib('j5_livrable_date', __('j5_livrable_date'));
        $form->setLib('emarge_livrable_demande_id', __('emarge_livrable_demande_id'));
        $form->setLib('emarge_livrable_demande_date', __('emarge_livrable_demande_date'));
        $form->setLib('emarge_livrable', __('emarge_livrable'));
        $form->setLib('emarge_livrable_date', __('emarge_livrable_date'));
        $form->setLib('om_collectivite', __('om_collectivite'));
        $form->setLib('referentiel_id', __('referentiel_id'));
        $form->setLib('canton', __('canton'));
        $form->setLib('circonscription_legislative', __('circonscription_legislative'));
        $form->setLib('circonscription_metropolitaine', __('circonscription_metropolitaine'));
        $form->setLib('arrets_liste', __('arrets_liste'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // arrets_liste
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "arrets_liste",
            $this->get_var_sql_forminc__sql("arrets_liste"),
            $this->get_var_sql_forminc__sql("arrets_liste_by_id"),
            false
        );
        // canton
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "canton",
            $this->get_var_sql_forminc__sql("canton"),
            $this->get_var_sql_forminc__sql("canton_by_id"),
            false
        );
        // circonscription_legislative
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "circonscription_legislative",
            $this->get_var_sql_forminc__sql("circonscription_legislative"),
            $this->get_var_sql_forminc__sql("circonscription_legislative_by_id"),
            false
        );
        // circonscription_metropolitaine
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "circonscription_metropolitaine",
            $this->get_var_sql_forminc__sql("circonscription_metropolitaine"),
            $this->get_var_sql_forminc__sql("circonscription_metropolitaine_by_id"),
            false
        );
        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('arrets_liste', $this->retourformulaire))
                $form->setVal('arrets_liste', $idxformulaire);
            if($this->is_in_context_of_foreign_key('canton', $this->retourformulaire))
                $form->setVal('canton', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('circonscription', $this->retourformulaire))
                $form->setVal('circonscription_legislative', $idxformulaire);
            if($this->is_in_context_of_foreign_key('circonscription', $this->retourformulaire))
                $form->setVal('circonscription_metropolitaine', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : composition_scrutin
        $this->rechercheTable($this->f->db, "composition_scrutin", "scrutin", $id);
    }


}
