<?php
//$Id$ 
//gen openMairie le 02/11/2023 12:41

require_once "../obj/om_dbform.class.php";

class arrets_liste_gen extends om_dbform {

    protected $_absolute_class_name = "arrets_liste";

    var $table = "arrets_liste";
    var $clePrimaire = "arrets_liste";
    var $typeCle = "N";
    var $required_field = array(
        "arrets_liste"
    );
    
    var $foreign_keys_extended = array(
        "om_collectivite" => array("om_collectivite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("livrable_demande_id");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "arrets_liste",
            "livrable_demande_id",
            "livrable_demande_date",
            "livrable",
            "livrable_date",
            "om_collectivite",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['arrets_liste'])) {
            $this->valF['arrets_liste'] = ""; // -> requis
        } else {
            $this->valF['arrets_liste'] = $val['arrets_liste'];
        }
        if (!is_numeric($val['livrable_demande_id'])) {
            $this->valF['livrable_demande_id'] = NULL;
        } else {
            $this->valF['livrable_demande_id'] = $val['livrable_demande_id'];
        }
        if ($val['livrable_demande_date'] != "") {
            $this->valF['livrable_demande_date'] = $this->dateDB($val['livrable_demande_date']);
        } else {
            $this->valF['livrable_demande_date'] = NULL;
        }
        if ($val['livrable'] == "") {
            $this->valF['livrable'] = NULL;
        } else {
            $this->valF['livrable'] = $val['livrable'];
        }
        if ($val['livrable_date'] != "") {
            $this->valF['livrable_date'] = $this->dateDB($val['livrable_date']);
        } else {
            $this->valF['livrable_date'] = NULL;
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = NULL;
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("arrets_liste", "hidden");
            $form->setType("livrable_demande_id", "text");
            $form->setType("livrable_demande_date", "date");
            $form->setType("livrable", "text");
            $form->setType("livrable_date", "date");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("arrets_liste", "hiddenstatic");
            $form->setType("livrable_demande_id", "text");
            $form->setType("livrable_demande_date", "date");
            $form->setType("livrable", "text");
            $form->setType("livrable_date", "date");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("arrets_liste", "hiddenstatic");
            $form->setType("livrable_demande_id", "hiddenstatic");
            $form->setType("livrable_demande_date", "hiddenstatic");
            $form->setType("livrable", "hiddenstatic");
            $form->setType("livrable_date", "hiddenstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("arrets_liste", "static");
            $form->setType("livrable_demande_id", "static");
            $form->setType("livrable_demande_date", "datestatic");
            $form->setType("livrable", "static");
            $form->setType("livrable_date", "datestatic");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('arrets_liste','VerifNum(this)');
        $form->setOnchange('livrable_demande_id','VerifNum(this)');
        $form->setOnchange('livrable_demande_date','fdate(this)');
        $form->setOnchange('livrable_date','fdate(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("arrets_liste", 11);
        $form->setTaille("livrable_demande_id", 11);
        $form->setTaille("livrable_demande_date", 12);
        $form->setTaille("livrable", 30);
        $form->setTaille("livrable_date", 12);
        $form->setTaille("om_collectivite", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("arrets_liste", 11);
        $form->setMax("livrable_demande_id", 11);
        $form->setMax("livrable_demande_date", 12);
        $form->setMax("livrable", 100);
        $form->setMax("livrable_date", 12);
        $form->setMax("om_collectivite", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('arrets_liste', __('arrets_liste'));
        $form->setLib('livrable_demande_id', __('livrable_demande_id'));
        $form->setLib('livrable_demande_date', __('livrable_demande_date'));
        $form->setLib('livrable', __('livrable'));
        $form->setLib('livrable_date', __('livrable_date'));
        $form->setLib('om_collectivite', __('om_collectivite'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : reu_scrutin
        $this->rechercheTable($this->f->db, "reu_scrutin", "arrets_liste", $id);
    }


}
