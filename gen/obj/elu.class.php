<?php
//$Id$ 
//gen openMairie le 26/12/2019 10:59

require_once "../obj/om_dbform.class.php";

class elu_gen extends om_dbform {

    protected $_absolute_class_name = "elu";

    var $table = "elu";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "adresse",
        "cp",
        "id",
        "nom",
        "prenom",
        "ville"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("nom");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "nom",
            "prenom",
            "nomjf",
            "date_naissance",
            "lieu_naissance",
            "adresse",
            "cp",
            "ville",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        $this->valF['nom'] = $val['nom'];
        $this->valF['prenom'] = $val['prenom'];
        if ($val['nomjf'] == "") {
            $this->valF['nomjf'] = NULL;
        } else {
            $this->valF['nomjf'] = $val['nomjf'];
        }
        if ($val['date_naissance'] != "") {
            $this->valF['date_naissance'] = $this->dateDB($val['date_naissance']);
        } else {
            $this->valF['date_naissance'] = NULL;
        }
        if ($val['lieu_naissance'] == "") {
            $this->valF['lieu_naissance'] = NULL;
        } else {
            $this->valF['lieu_naissance'] = $val['lieu_naissance'];
        }
        $this->valF['adresse'] = $val['adresse'];
        $this->valF['cp'] = $val['cp'];
        $this->valF['ville'] = $val['ville'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            $form->setType("nomjf", "text");
            $form->setType("date_naissance", "date");
            $form->setType("lieu_naissance", "text");
            $form->setType("adresse", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            $form->setType("nomjf", "text");
            $form->setType("date_naissance", "date");
            $form->setType("lieu_naissance", "text");
            $form->setType("adresse", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("prenom", "hiddenstatic");
            $form->setType("nomjf", "hiddenstatic");
            $form->setType("date_naissance", "hiddenstatic");
            $form->setType("lieu_naissance", "hiddenstatic");
            $form->setType("adresse", "hiddenstatic");
            $form->setType("cp", "hiddenstatic");
            $form->setType("ville", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("nom", "static");
            $form->setType("prenom", "static");
            $form->setType("nomjf", "static");
            $form->setType("date_naissance", "datestatic");
            $form->setType("lieu_naissance", "static");
            $form->setType("adresse", "static");
            $form->setType("cp", "static");
            $form->setType("ville", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('date_naissance','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("nom", 30);
        $form->setTaille("prenom", 30);
        $form->setTaille("nomjf", 30);
        $form->setTaille("date_naissance", 12);
        $form->setTaille("lieu_naissance", 30);
        $form->setTaille("adresse", 30);
        $form->setTaille("cp", 10);
        $form->setTaille("ville", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("nom", 40);
        $form->setMax("prenom", 40);
        $form->setMax("nomjf", 40);
        $form->setMax("date_naissance", 12);
        $form->setMax("lieu_naissance", 40);
        $form->setMax("adresse", 80);
        $form->setMax("cp", 5);
        $form->setMax("ville", 40);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('nom', __('nom'));
        $form->setLib('prenom', __('prenom'));
        $form->setLib('nomjf', __('nomjf'));
        $form->setLib('date_naissance', __('date_naissance'));
        $form->setLib('lieu_naissance', __('lieu_naissance'));
        $form->setLib('adresse', __('adresse'));
        $form->setLib('cp', __('cp'));
        $form->setLib('ville', __('ville'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : affectation
        $this->rechercheTable($this->f->db, "affectation", "elu", $id);
    }


}
