<?php
//$Id$ 
//gen openMairie le 15/02/2022 09:09

require_once "../obj/om_dbform.class.php";

class candidat_gen extends om_dbform {

    protected $_absolute_class_name = "candidat";

    var $table = "candidat";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "composition_scrutin",
        "id",
        "nom"
    );
    
    var $foreign_keys_extended = array(
        "composition_scrutin" => array("composition_scrutin", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("nom");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "nom",
            "composition_scrutin",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_composition_scrutin() {
        return "SELECT composition_scrutin.id, composition_scrutin.libelle FROM ".DB_PREFIXE."composition_scrutin ORDER BY composition_scrutin.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_composition_scrutin_by_id() {
        return "SELECT composition_scrutin.id, composition_scrutin.libelle FROM ".DB_PREFIXE."composition_scrutin WHERE id = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        $this->valF['nom'] = $val['nom'];
        if (!is_numeric($val['composition_scrutin'])) {
            $this->valF['composition_scrutin'] = ""; // -> requis
        } else {
            $this->valF['composition_scrutin'] = $val['composition_scrutin'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            $form->setType("nom", "text");
            if ($this->is_in_context_of_foreign_key("composition_scrutin", $this->retourformulaire)) {
                $form->setType("composition_scrutin", "selecthiddenstatic");
            } else {
                $form->setType("composition_scrutin", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            $form->setType("nom", "text");
            if ($this->is_in_context_of_foreign_key("composition_scrutin", $this->retourformulaire)) {
                $form->setType("composition_scrutin", "selecthiddenstatic");
            } else {
                $form->setType("composition_scrutin", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("composition_scrutin", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("nom", "static");
            $form->setType("composition_scrutin", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('composition_scrutin','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("nom", 30);
        $form->setTaille("composition_scrutin", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("nom", 50);
        $form->setMax("composition_scrutin", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('nom', __('nom'));
        $form->setLib('composition_scrutin', __('composition_scrutin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // composition_scrutin
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "composition_scrutin",
            $this->get_var_sql_forminc__sql("composition_scrutin"),
            $this->get_var_sql_forminc__sql("composition_scrutin_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('composition_scrutin', $this->retourformulaire))
                $form->setVal('composition_scrutin', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : affectation
        $this->rechercheTable($this->f->db, "affectation", "candidat", $id);
    }


}
