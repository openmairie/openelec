<?php
//$Id$ 
//gen openMairie le 02/05/2023 15:42

require_once "../obj/om_dbform.class.php";

class parametrage_nb_jures_gen extends om_dbform {

    protected $_absolute_class_name = "parametrage_nb_jures";

    var $table = "parametrage_nb_jures";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "canton",
        "om_collectivite"
    );
    var $unique_key = array(
      array("canton","om_collectivite"),
    );
    var $foreign_keys_extended = array(
        "canton" => array("canton", ),
        "om_collectivite" => array("om_collectivite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("canton");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "canton",
            "om_collectivite",
            "nb_jures",
            "nb_suppleants",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton() {
        return "SELECT canton.id, canton.libelle FROM ".DB_PREFIXE."canton ORDER BY canton.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton_by_id() {
        return "SELECT canton.id, canton.libelle FROM ".DB_PREFIXE."canton WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = 0; // -> default
        } else {
            $this->valF['id'] = $val['id'];
        }
        if (!is_numeric($val['canton'])) {
            $this->valF['canton'] = ""; // -> requis
        } else {
            $this->valF['canton'] = $val['canton'];
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        if (!is_numeric($val['nb_jures'])) {
            $this->valF['nb_jures'] = NULL;
        } else {
            $this->valF['nb_jures'] = $val['nb_jures'];
        }
        if (!is_numeric($val['nb_suppleants'])) {
            $this->valF['nb_suppleants'] = NULL;
        } else {
            $this->valF['nb_suppleants'] = $val['nb_suppleants'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            if ($this->is_in_context_of_foreign_key("canton", $this->retourformulaire)) {
                $form->setType("canton", "selecthiddenstatic");
            } else {
                $form->setType("canton", "select");
            }
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("nb_jures", "text");
            $form->setType("nb_suppleants", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("canton", $this->retourformulaire)) {
                $form->setType("canton", "selecthiddenstatic");
            } else {
                $form->setType("canton", "select");
            }
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("nb_jures", "text");
            $form->setType("nb_suppleants", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("canton", "selectstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
            $form->setType("nb_jures", "hiddenstatic");
            $form->setType("nb_suppleants", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("canton", "selectstatic");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("nb_jures", "static");
            $form->setType("nb_suppleants", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('canton','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
        $form->setOnchange('nb_jures','VerifNum(this)');
        $form->setOnchange('nb_suppleants','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("canton", 11);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("nb_jures", 11);
        $form->setTaille("nb_suppleants", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("canton", 11);
        $form->setMax("om_collectivite", 11);
        $form->setMax("nb_jures", 11);
        $form->setMax("nb_suppleants", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('canton', __('canton'));
        $form->setLib('om_collectivite', __('om_collectivite'));
        $form->setLib('nb_jures', __('nb_jures'));
        $form->setLib('nb_suppleants', __('nb_suppleants'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // canton
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "canton",
            $this->get_var_sql_forminc__sql("canton"),
            $this->get_var_sql_forminc__sql("canton_by_id"),
            false
        );
        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('canton', $this->retourformulaire))
                $form->setVal('canton', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
