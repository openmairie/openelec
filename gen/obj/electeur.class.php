<?php
//$Id$ 
//gen openMairie le 02/09/2022 15:24

require_once "../obj/om_dbform.class.php";

class electeur_gen extends om_dbform {

    protected $_absolute_class_name = "electeur";

    var $table = "electeur";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "bureau",
        "code_nationalite",
        "code_voie",
        "id",
        "liste",
        "om_collectivite"
    );
    var $unique_key = array(
      array("bureau","liste","numero_bureau","om_collectivite"),
      array("ine","liste","om_collectivite"),
    );
    var $foreign_keys_extended = array(
        "bureau" => array("bureau", ),
        "nationalite" => array("nationalite", ),
        "liste" => array("liste", ),
        "om_collectivite" => array("om_collectivite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("liste");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "liste",
            "bureau",
            "bureauforce",
            "numero_bureau",
            "date_modif",
            "utilisateur",
            "civilite",
            "sexe",
            "nom",
            "nom_usage",
            "prenom",
            "date_naissance",
            "code_departement_naissance",
            "libelle_departement_naissance",
            "code_lieu_de_naissance",
            "libelle_lieu_de_naissance",
            "code_nationalite",
            "code_voie",
            "libelle_voie",
            "numero_habitation",
            "complement_numero",
            "complement",
            "provenance",
            "libelle_provenance",
            "resident",
            "adresse_resident",
            "complement_resident",
            "cp_resident",
            "ville_resident",
            "tableau",
            "date_tableau",
            "mouvement",
            "date_mouvement",
            "typecat",
            "carte",
            "procuration",
            "jury",
            "date_inscription",
            "code_inscription",
            "jury_effectif",
            "date_jeffectif",
            "om_collectivite",
            "profession",
            "motif_dispense_jury",
            "telephone",
            "courriel",
            "ine",
            "reu_sync_info",
            "a_transmettre",
            "date_saisie_carte_retour",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau ORDER BY bureau.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_code_nationalite() {
        return "SELECT nationalite.code, nationalite.libelle_nationalite FROM ".DB_PREFIXE."nationalite WHERE ((nationalite.om_validite_debut IS NULL AND (nationalite.om_validite_fin IS NULL OR nationalite.om_validite_fin > CURRENT_DATE)) OR (nationalite.om_validite_debut <= CURRENT_DATE AND (nationalite.om_validite_fin IS NULL OR nationalite.om_validite_fin > CURRENT_DATE))) ORDER BY nationalite.libelle_nationalite ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_code_nationalite_by_id() {
        return "SELECT nationalite.code, nationalite.libelle_nationalite FROM ".DB_PREFIXE."nationalite WHERE code = '<idx>'";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste() {
        return "SELECT liste.liste, liste.libelle_liste FROM ".DB_PREFIXE."liste ORDER BY liste.libelle_liste ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste_by_id() {
        return "SELECT liste.liste, liste.libelle_liste FROM ".DB_PREFIXE."liste WHERE liste = '<idx>'";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        $this->valF['liste'] = $val['liste'];
        if (!is_numeric($val['bureau'])) {
            $this->valF['bureau'] = ""; // -> requis
        } else {
            $this->valF['bureau'] = $val['bureau'];
        }
        if ($val['bureauforce'] == "") {
            $this->valF['bureauforce'] = ""; // -> default
        } else {
            $this->valF['bureauforce'] = $val['bureauforce'];
        }
        if (!is_numeric($val['numero_bureau'])) {
            $this->valF['numero_bureau'] = NULL;
        } else {
            $this->valF['numero_bureau'] = $val['numero_bureau'];
        }
        if ($val['date_modif'] != "") {
            $this->valF['date_modif'] = $this->dateDB($val['date_modif']);
        } else {
            $this->valF['date_modif'] = NULL;
        }
        if ($val['utilisateur'] == "") {
            $this->valF['utilisateur'] = ""; // -> default
        } else {
            $this->valF['utilisateur'] = $val['utilisateur'];
        }
        if ($val['civilite'] == "") {
            $this->valF['civilite'] = ""; // -> default
        } else {
            $this->valF['civilite'] = $val['civilite'];
        }
        if ($val['sexe'] == "") {
            $this->valF['sexe'] = ""; // -> default
        } else {
            $this->valF['sexe'] = $val['sexe'];
        }
        if ($val['nom'] == "") {
            $this->valF['nom'] = ""; // -> default
        } else {
            $this->valF['nom'] = $val['nom'];
        }
        if ($val['nom_usage'] == "") {
            $this->valF['nom_usage'] = NULL;
        } else {
            $this->valF['nom_usage'] = $val['nom_usage'];
        }
        if ($val['prenom'] == "") {
            $this->valF['prenom'] = ""; // -> default
        } else {
            $this->valF['prenom'] = $val['prenom'];
        }
        if ($val['date_naissance'] != "") {
            $this->valF['date_naissance'] = $this->dateDB($val['date_naissance']);
        } else {
            $this->valF['date_naissance'] = NULL;
        }
        if ($val['code_departement_naissance'] == "") {
            $this->valF['code_departement_naissance'] = ""; // -> default
        } else {
            $this->valF['code_departement_naissance'] = $val['code_departement_naissance'];
        }
        if ($val['libelle_departement_naissance'] == "") {
            $this->valF['libelle_departement_naissance'] = ""; // -> default
        } else {
            $this->valF['libelle_departement_naissance'] = $val['libelle_departement_naissance'];
        }
        if ($val['code_lieu_de_naissance'] == "") {
            $this->valF['code_lieu_de_naissance'] = ""; // -> default
        } else {
            $this->valF['code_lieu_de_naissance'] = $val['code_lieu_de_naissance'];
        }
        if ($val['libelle_lieu_de_naissance'] == "") {
            $this->valF['libelle_lieu_de_naissance'] = ""; // -> default
        } else {
            $this->valF['libelle_lieu_de_naissance'] = $val['libelle_lieu_de_naissance'];
        }
        $this->valF['code_nationalite'] = $val['code_nationalite'];
        $this->valF['code_voie'] = $val['code_voie'];
        if ($val['libelle_voie'] == "") {
            $this->valF['libelle_voie'] = ""; // -> default
        } else {
            $this->valF['libelle_voie'] = $val['libelle_voie'];
        }
        if (!is_numeric($val['numero_habitation'])) {
            $this->valF['numero_habitation'] = 0; // -> default
        } else {
            $this->valF['numero_habitation'] = $val['numero_habitation'];
        }
        if ($val['complement_numero'] == "") {
            $this->valF['complement_numero'] = NULL;
        } else {
            $this->valF['complement_numero'] = $val['complement_numero'];
        }
        if ($val['complement'] == "") {
            $this->valF['complement'] = NULL;
        } else {
            $this->valF['complement'] = $val['complement'];
        }
        if ($val['provenance'] == "") {
            $this->valF['provenance'] = NULL;
        } else {
            $this->valF['provenance'] = $val['provenance'];
        }
        if ($val['libelle_provenance'] == "") {
            $this->valF['libelle_provenance'] = ""; // -> default
        } else {
            $this->valF['libelle_provenance'] = $val['libelle_provenance'];
        }
        if ($val['resident'] == "") {
            $this->valF['resident'] = NULL;
        } else {
            $this->valF['resident'] = $val['resident'];
        }
        if ($val['adresse_resident'] == "") {
            $this->valF['adresse_resident'] = NULL;
        } else {
            $this->valF['adresse_resident'] = $val['adresse_resident'];
        }
        if ($val['complement_resident'] == "") {
            $this->valF['complement_resident'] = NULL;
        } else {
            $this->valF['complement_resident'] = $val['complement_resident'];
        }
        if ($val['cp_resident'] == "") {
            $this->valF['cp_resident'] = NULL;
        } else {
            $this->valF['cp_resident'] = $val['cp_resident'];
        }
        if ($val['ville_resident'] == "") {
            $this->valF['ville_resident'] = NULL;
        } else {
            $this->valF['ville_resident'] = $val['ville_resident'];
        }
        if ($val['tableau'] == "") {
            $this->valF['tableau'] = NULL;
        } else {
            $this->valF['tableau'] = $val['tableau'];
        }
        if ($val['date_tableau'] != "") {
            $this->valF['date_tableau'] = $this->dateDB($val['date_tableau']);
        } else {
            $this->valF['date_tableau'] = NULL;
        }
        if ($val['mouvement'] == "") {
            $this->valF['mouvement'] = NULL;
        } else {
            $this->valF['mouvement'] = $val['mouvement'];
        }
        if ($val['date_mouvement'] != "") {
            $this->valF['date_mouvement'] = $this->dateDB($val['date_mouvement']);
        } else {
            $this->valF['date_mouvement'] = NULL;
        }
        if ($val['typecat'] == "") {
            $this->valF['typecat'] = ""; // -> default
        } else {
            $this->valF['typecat'] = $val['typecat'];
        }
        if (!is_numeric($val['carte'])) {
            $this->valF['carte'] = 0; // -> default
        } else {
            $this->valF['carte'] = $val['carte'];
        }
        if ($val['procuration'] == "") {
            $this->valF['procuration'] = NULL;
        } else {
            $this->valF['procuration'] = $val['procuration'];
        }
        if (!is_numeric($val['jury'])) {
            $this->valF['jury'] = 0; // -> default
        } else {
            $this->valF['jury'] = $val['jury'];
        }
        if ($val['date_inscription'] != "") {
            $this->valF['date_inscription'] = $this->dateDB($val['date_inscription']);
        } else {
            $this->valF['date_inscription'] = NULL;
        }
        if ($val['code_inscription'] == "") {
            $this->valF['code_inscription'] = NULL;
        } else {
            $this->valF['code_inscription'] = $val['code_inscription'];
        }
        if ($val['jury_effectif'] == "") {
            $this->valF['jury_effectif'] = NULL;
        } else {
            $this->valF['jury_effectif'] = $val['jury_effectif'];
        }
        if ($val['date_jeffectif'] != "") {
            $this->valF['date_jeffectif'] = $this->dateDB($val['date_jeffectif']);
        } else {
            $this->valF['date_jeffectif'] = NULL;
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        if ($val['profession'] == "") {
            $this->valF['profession'] = NULL;
        } else {
            $this->valF['profession'] = $val['profession'];
        }
        if ($val['motif_dispense_jury'] == "") {
            $this->valF['motif_dispense_jury'] = NULL;
        } else {
            $this->valF['motif_dispense_jury'] = $val['motif_dispense_jury'];
        }
        if ($val['telephone'] == "") {
            $this->valF['telephone'] = NULL;
        } else {
            $this->valF['telephone'] = $val['telephone'];
        }
        if ($val['courriel'] == "") {
            $this->valF['courriel'] = NULL;
        } else {
            $this->valF['courriel'] = $val['courriel'];
        }
        if (!is_numeric($val['ine'])) {
            $this->valF['ine'] = NULL;
        } else {
            $this->valF['ine'] = $val['ine'];
        }
        if ($val['reu_sync_info'] == "") {
            $this->valF['reu_sync_info'] = NULL;
        } else {
            $this->valF['reu_sync_info'] = $val['reu_sync_info'];
        }
        if ($val['a_transmettre'] == 1 || $val['a_transmettre'] == "t" || $val['a_transmettre'] == "Oui") {
            $this->valF['a_transmettre'] = true;
        } else {
            $this->valF['a_transmettre'] = false;
        }
        if ($val['date_saisie_carte_retour'] != "") {
            $this->valF['date_saisie_carte_retour'] = $this->dateDB($val['date_saisie_carte_retour']);
        } else {
            $this->valF['date_saisie_carte_retour'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            if ($this->is_in_context_of_foreign_key("liste", $this->retourformulaire)) {
                $form->setType("liste", "selecthiddenstatic");
            } else {
                $form->setType("liste", "select");
            }
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            $form->setType("bureauforce", "text");
            $form->setType("numero_bureau", "text");
            $form->setType("date_modif", "date");
            $form->setType("utilisateur", "text");
            $form->setType("civilite", "text");
            $form->setType("sexe", "text");
            $form->setType("nom", "text");
            $form->setType("nom_usage", "text");
            $form->setType("prenom", "text");
            $form->setType("date_naissance", "date");
            $form->setType("code_departement_naissance", "text");
            $form->setType("libelle_departement_naissance", "text");
            $form->setType("code_lieu_de_naissance", "text");
            $form->setType("libelle_lieu_de_naissance", "text");
            if ($this->is_in_context_of_foreign_key("nationalite", $this->retourformulaire)) {
                $form->setType("code_nationalite", "selecthiddenstatic");
            } else {
                $form->setType("code_nationalite", "select");
            }
            $form->setType("code_voie", "text");
            $form->setType("libelle_voie", "text");
            $form->setType("numero_habitation", "text");
            $form->setType("complement_numero", "text");
            $form->setType("complement", "text");
            $form->setType("provenance", "text");
            $form->setType("libelle_provenance", "text");
            $form->setType("resident", "text");
            $form->setType("adresse_resident", "text");
            $form->setType("complement_resident", "text");
            $form->setType("cp_resident", "text");
            $form->setType("ville_resident", "text");
            $form->setType("tableau", "text");
            $form->setType("date_tableau", "date");
            $form->setType("mouvement", "text");
            $form->setType("date_mouvement", "date");
            $form->setType("typecat", "text");
            $form->setType("carte", "text");
            $form->setType("procuration", "text");
            $form->setType("jury", "text");
            $form->setType("date_inscription", "date");
            $form->setType("code_inscription", "text");
            $form->setType("jury_effectif", "text");
            $form->setType("date_jeffectif", "date");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("profession", "text");
            $form->setType("motif_dispense_jury", "text");
            $form->setType("telephone", "text");
            $form->setType("courriel", "text");
            $form->setType("ine", "text");
            $form->setType("reu_sync_info", "text");
            $form->setType("a_transmettre", "checkbox");
            $form->setType("date_saisie_carte_retour", "date");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("liste", $this->retourformulaire)) {
                $form->setType("liste", "selecthiddenstatic");
            } else {
                $form->setType("liste", "select");
            }
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            $form->setType("bureauforce", "text");
            $form->setType("numero_bureau", "text");
            $form->setType("date_modif", "date");
            $form->setType("utilisateur", "text");
            $form->setType("civilite", "text");
            $form->setType("sexe", "text");
            $form->setType("nom", "text");
            $form->setType("nom_usage", "text");
            $form->setType("prenom", "text");
            $form->setType("date_naissance", "date");
            $form->setType("code_departement_naissance", "text");
            $form->setType("libelle_departement_naissance", "text");
            $form->setType("code_lieu_de_naissance", "text");
            $form->setType("libelle_lieu_de_naissance", "text");
            if ($this->is_in_context_of_foreign_key("nationalite", $this->retourformulaire)) {
                $form->setType("code_nationalite", "selecthiddenstatic");
            } else {
                $form->setType("code_nationalite", "select");
            }
            $form->setType("code_voie", "text");
            $form->setType("libelle_voie", "text");
            $form->setType("numero_habitation", "text");
            $form->setType("complement_numero", "text");
            $form->setType("complement", "text");
            $form->setType("provenance", "text");
            $form->setType("libelle_provenance", "text");
            $form->setType("resident", "text");
            $form->setType("adresse_resident", "text");
            $form->setType("complement_resident", "text");
            $form->setType("cp_resident", "text");
            $form->setType("ville_resident", "text");
            $form->setType("tableau", "text");
            $form->setType("date_tableau", "date");
            $form->setType("mouvement", "text");
            $form->setType("date_mouvement", "date");
            $form->setType("typecat", "text");
            $form->setType("carte", "text");
            $form->setType("procuration", "text");
            $form->setType("jury", "text");
            $form->setType("date_inscription", "date");
            $form->setType("code_inscription", "text");
            $form->setType("jury_effectif", "text");
            $form->setType("date_jeffectif", "date");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("profession", "text");
            $form->setType("motif_dispense_jury", "text");
            $form->setType("telephone", "text");
            $form->setType("courriel", "text");
            $form->setType("ine", "text");
            $form->setType("reu_sync_info", "text");
            $form->setType("a_transmettre", "checkbox");
            $form->setType("date_saisie_carte_retour", "date");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("liste", "selectstatic");
            $form->setType("bureau", "selectstatic");
            $form->setType("bureauforce", "hiddenstatic");
            $form->setType("numero_bureau", "hiddenstatic");
            $form->setType("date_modif", "hiddenstatic");
            $form->setType("utilisateur", "hiddenstatic");
            $form->setType("civilite", "hiddenstatic");
            $form->setType("sexe", "hiddenstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("nom_usage", "hiddenstatic");
            $form->setType("prenom", "hiddenstatic");
            $form->setType("date_naissance", "hiddenstatic");
            $form->setType("code_departement_naissance", "hiddenstatic");
            $form->setType("libelle_departement_naissance", "hiddenstatic");
            $form->setType("code_lieu_de_naissance", "hiddenstatic");
            $form->setType("libelle_lieu_de_naissance", "hiddenstatic");
            $form->setType("code_nationalite", "selectstatic");
            $form->setType("code_voie", "hiddenstatic");
            $form->setType("libelle_voie", "hiddenstatic");
            $form->setType("numero_habitation", "hiddenstatic");
            $form->setType("complement_numero", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("provenance", "hiddenstatic");
            $form->setType("libelle_provenance", "hiddenstatic");
            $form->setType("resident", "hiddenstatic");
            $form->setType("adresse_resident", "hiddenstatic");
            $form->setType("complement_resident", "hiddenstatic");
            $form->setType("cp_resident", "hiddenstatic");
            $form->setType("ville_resident", "hiddenstatic");
            $form->setType("tableau", "hiddenstatic");
            $form->setType("date_tableau", "hiddenstatic");
            $form->setType("mouvement", "hiddenstatic");
            $form->setType("date_mouvement", "hiddenstatic");
            $form->setType("typecat", "hiddenstatic");
            $form->setType("carte", "hiddenstatic");
            $form->setType("procuration", "hiddenstatic");
            $form->setType("jury", "hiddenstatic");
            $form->setType("date_inscription", "hiddenstatic");
            $form->setType("code_inscription", "hiddenstatic");
            $form->setType("jury_effectif", "hiddenstatic");
            $form->setType("date_jeffectif", "hiddenstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
            $form->setType("profession", "hiddenstatic");
            $form->setType("motif_dispense_jury", "hiddenstatic");
            $form->setType("telephone", "hiddenstatic");
            $form->setType("courriel", "hiddenstatic");
            $form->setType("ine", "hiddenstatic");
            $form->setType("reu_sync_info", "hiddenstatic");
            $form->setType("a_transmettre", "hiddenstatic");
            $form->setType("date_saisie_carte_retour", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("liste", "selectstatic");
            $form->setType("bureau", "selectstatic");
            $form->setType("bureauforce", "static");
            $form->setType("numero_bureau", "static");
            $form->setType("date_modif", "datestatic");
            $form->setType("utilisateur", "static");
            $form->setType("civilite", "static");
            $form->setType("sexe", "static");
            $form->setType("nom", "static");
            $form->setType("nom_usage", "static");
            $form->setType("prenom", "static");
            $form->setType("date_naissance", "datestatic");
            $form->setType("code_departement_naissance", "static");
            $form->setType("libelle_departement_naissance", "static");
            $form->setType("code_lieu_de_naissance", "static");
            $form->setType("libelle_lieu_de_naissance", "static");
            $form->setType("code_nationalite", "selectstatic");
            $form->setType("code_voie", "static");
            $form->setType("libelle_voie", "static");
            $form->setType("numero_habitation", "static");
            $form->setType("complement_numero", "static");
            $form->setType("complement", "static");
            $form->setType("provenance", "static");
            $form->setType("libelle_provenance", "static");
            $form->setType("resident", "static");
            $form->setType("adresse_resident", "static");
            $form->setType("complement_resident", "static");
            $form->setType("cp_resident", "static");
            $form->setType("ville_resident", "static");
            $form->setType("tableau", "static");
            $form->setType("date_tableau", "datestatic");
            $form->setType("mouvement", "static");
            $form->setType("date_mouvement", "datestatic");
            $form->setType("typecat", "static");
            $form->setType("carte", "static");
            $form->setType("procuration", "static");
            $form->setType("jury", "static");
            $form->setType("date_inscription", "datestatic");
            $form->setType("code_inscription", "static");
            $form->setType("jury_effectif", "static");
            $form->setType("date_jeffectif", "datestatic");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("profession", "static");
            $form->setType("motif_dispense_jury", "static");
            $form->setType("telephone", "static");
            $form->setType("courriel", "static");
            $form->setType("ine", "static");
            $form->setType("reu_sync_info", "static");
            $form->setType("a_transmettre", "checkboxstatic");
            $form->setType("date_saisie_carte_retour", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('bureau','VerifNum(this)');
        $form->setOnchange('numero_bureau','VerifNum(this)');
        $form->setOnchange('date_modif','fdate(this)');
        $form->setOnchange('date_naissance','fdate(this)');
        $form->setOnchange('numero_habitation','VerifNum(this)');
        $form->setOnchange('date_tableau','fdate(this)');
        $form->setOnchange('date_mouvement','fdate(this)');
        $form->setOnchange('carte','VerifNum(this)');
        $form->setOnchange('jury','VerifNum(this)');
        $form->setOnchange('date_inscription','fdate(this)');
        $form->setOnchange('date_jeffectif','fdate(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
        $form->setOnchange('ine','VerifNum(this)');
        $form->setOnchange('date_saisie_carte_retour','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("liste", 10);
        $form->setTaille("bureau", 11);
        $form->setTaille("bureauforce", 10);
        $form->setTaille("numero_bureau", 20);
        $form->setTaille("date_modif", 12);
        $form->setTaille("utilisateur", 30);
        $form->setTaille("civilite", 10);
        $form->setTaille("sexe", 10);
        $form->setTaille("nom", 30);
        $form->setTaille("nom_usage", 30);
        $form->setTaille("prenom", 30);
        $form->setTaille("date_naissance", 12);
        $form->setTaille("code_departement_naissance", 10);
        $form->setTaille("libelle_departement_naissance", 30);
        $form->setTaille("code_lieu_de_naissance", 10);
        $form->setTaille("libelle_lieu_de_naissance", 30);
        $form->setTaille("code_nationalite", 10);
        $form->setTaille("code_voie", 10);
        $form->setTaille("libelle_voie", 30);
        $form->setTaille("numero_habitation", 11);
        $form->setTaille("complement_numero", 10);
        $form->setTaille("complement", 30);
        $form->setTaille("provenance", 10);
        $form->setTaille("libelle_provenance", 30);
        $form->setTaille("resident", 10);
        $form->setTaille("adresse_resident", 30);
        $form->setTaille("complement_resident", 30);
        $form->setTaille("cp_resident", 10);
        $form->setTaille("ville_resident", 30);
        $form->setTaille("tableau", 10);
        $form->setTaille("date_tableau", 12);
        $form->setTaille("mouvement", 20);
        $form->setTaille("date_mouvement", 12);
        $form->setTaille("typecat", 30);
        $form->setTaille("carte", 6);
        $form->setTaille("procuration", 30);
        $form->setTaille("jury", 6);
        $form->setTaille("date_inscription", 12);
        $form->setTaille("code_inscription", 20);
        $form->setTaille("jury_effectif", 10);
        $form->setTaille("date_jeffectif", 12);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("profession", 30);
        $form->setTaille("motif_dispense_jury", 30);
        $form->setTaille("telephone", 30);
        $form->setTaille("courriel", 30);
        $form->setTaille("ine", 11);
        $form->setTaille("reu_sync_info", 30);
        $form->setTaille("a_transmettre", 1);
        $form->setTaille("date_saisie_carte_retour", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("liste", 6);
        $form->setMax("bureau", 11);
        $form->setMax("bureauforce", 3);
        $form->setMax("numero_bureau", 20);
        $form->setMax("date_modif", 12);
        $form->setMax("utilisateur", 30);
        $form->setMax("civilite", 4);
        $form->setMax("sexe", 1);
        $form->setMax("nom", 63);
        $form->setMax("nom_usage", 63);
        $form->setMax("prenom", 70);
        $form->setMax("date_naissance", 12);
        $form->setMax("code_departement_naissance", 5);
        $form->setMax("libelle_departement_naissance", 70);
        $form->setMax("code_lieu_de_naissance", 6);
        $form->setMax("libelle_lieu_de_naissance", 70);
        $form->setMax("code_nationalite", 4);
        $form->setMax("code_voie", 10);
        $form->setMax("libelle_voie", 50);
        $form->setMax("numero_habitation", 11);
        $form->setMax("complement_numero", 10);
        $form->setMax("complement", 80);
        $form->setMax("provenance", 6);
        $form->setMax("libelle_provenance", 50);
        $form->setMax("resident", 3);
        $form->setMax("adresse_resident", 100);
        $form->setMax("complement_resident", 100);
        $form->setMax("cp_resident", 10);
        $form->setMax("ville_resident", 100);
        $form->setMax("tableau", 10);
        $form->setMax("date_tableau", 12);
        $form->setMax("mouvement", 20);
        $form->setMax("date_mouvement", 12);
        $form->setMax("typecat", 30);
        $form->setMax("carte", 6);
        $form->setMax("procuration", 255);
        $form->setMax("jury", 6);
        $form->setMax("date_inscription", 12);
        $form->setMax("code_inscription", 20);
        $form->setMax("jury_effectif", 3);
        $form->setMax("date_jeffectif", 12);
        $form->setMax("om_collectivite", 11);
        $form->setMax("profession", 80);
        $form->setMax("motif_dispense_jury", 80);
        $form->setMax("telephone", 30);
        $form->setMax("courriel", 100);
        $form->setMax("ine", 11);
        $form->setMax("reu_sync_info", 250);
        $form->setMax("a_transmettre", 1);
        $form->setMax("date_saisie_carte_retour", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('liste', __('liste'));
        $form->setLib('bureau', __('bureau'));
        $form->setLib('bureauforce', __('bureauforce'));
        $form->setLib('numero_bureau', __('numero_bureau'));
        $form->setLib('date_modif', __('date_modif'));
        $form->setLib('utilisateur', __('utilisateur'));
        $form->setLib('civilite', __('civilite'));
        $form->setLib('sexe', __('sexe'));
        $form->setLib('nom', __('nom'));
        $form->setLib('nom_usage', __('nom_usage'));
        $form->setLib('prenom', __('prenom'));
        $form->setLib('date_naissance', __('date_naissance'));
        $form->setLib('code_departement_naissance', __('code_departement_naissance'));
        $form->setLib('libelle_departement_naissance', __('libelle_departement_naissance'));
        $form->setLib('code_lieu_de_naissance', __('code_lieu_de_naissance'));
        $form->setLib('libelle_lieu_de_naissance', __('libelle_lieu_de_naissance'));
        $form->setLib('code_nationalite', __('code_nationalite'));
        $form->setLib('code_voie', __('code_voie'));
        $form->setLib('libelle_voie', __('libelle_voie'));
        $form->setLib('numero_habitation', __('numero_habitation'));
        $form->setLib('complement_numero', __('complement_numero'));
        $form->setLib('complement', __('complement'));
        $form->setLib('provenance', __('provenance'));
        $form->setLib('libelle_provenance', __('libelle_provenance'));
        $form->setLib('resident', __('resident'));
        $form->setLib('adresse_resident', __('adresse_resident'));
        $form->setLib('complement_resident', __('complement_resident'));
        $form->setLib('cp_resident', __('cp_resident'));
        $form->setLib('ville_resident', __('ville_resident'));
        $form->setLib('tableau', __('tableau'));
        $form->setLib('date_tableau', __('date_tableau'));
        $form->setLib('mouvement', __('mouvement'));
        $form->setLib('date_mouvement', __('date_mouvement'));
        $form->setLib('typecat', __('typecat'));
        $form->setLib('carte', __('carte'));
        $form->setLib('procuration', __('procuration'));
        $form->setLib('jury', __('jury'));
        $form->setLib('date_inscription', __('date_inscription'));
        $form->setLib('code_inscription', __('code_inscription'));
        $form->setLib('jury_effectif', __('jury_effectif'));
        $form->setLib('date_jeffectif', __('date_jeffectif'));
        $form->setLib('om_collectivite', __('om_collectivite'));
        $form->setLib('profession', __('profession'));
        $form->setLib('motif_dispense_jury', __('motif_dispense_jury'));
        $form->setLib('telephone', __('telephone'));
        $form->setLib('courriel', __('courriel'));
        $form->setLib('ine', __('ine'));
        $form->setLib('reu_sync_info', __('reu_sync_info'));
        $form->setLib('a_transmettre', __('a_transmettre'));
        $form->setLib('date_saisie_carte_retour', __('date_saisie_carte_retour'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // bureau
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "bureau",
            $this->get_var_sql_forminc__sql("bureau"),
            $this->get_var_sql_forminc__sql("bureau_by_id"),
            false
        );
        // code_nationalite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "code_nationalite",
            $this->get_var_sql_forminc__sql("code_nationalite"),
            $this->get_var_sql_forminc__sql("code_nationalite_by_id"),
            true
        );
        // liste
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "liste",
            $this->get_var_sql_forminc__sql("liste"),
            $this->get_var_sql_forminc__sql("liste_by_id"),
            false
        );
        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('bureau', $this->retourformulaire))
                $form->setVal('bureau', $idxformulaire);
            if($this->is_in_context_of_foreign_key('nationalite', $this->retourformulaire))
                $form->setVal('code_nationalite', $idxformulaire);
            if($this->is_in_context_of_foreign_key('liste', $this->retourformulaire))
                $form->setVal('liste', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : procuration
        $this->rechercheTable($this->f->db, "procuration", "mandant", $id);
        // Verification de la cle secondaire : procuration
        $this->rechercheTable($this->f->db, "procuration", "mandataire", $id);
    }


}
