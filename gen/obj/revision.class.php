<?php
//$Id$ 
//gen openMairie le 15/05/2018 12:51

require_once "../obj/om_dbform.class.php";

class revision_gen extends om_dbform {

    protected $_absolute_class_name = "revision";

    var $table = "revision";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "date_debut",
        "date_effet",
        "date_tr1",
        "date_tr2",
        "id",
        "libelle"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "libelle",
            "date_debut",
            "date_effet",
            "date_tr1",
            "date_tr2",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if ($val['date_debut'] != "") {
            $this->valF['date_debut'] = $this->dateDB($val['date_debut']);
        }
        if ($val['date_effet'] != "") {
            $this->valF['date_effet'] = $this->dateDB($val['date_effet']);
        }
        if ($val['date_tr1'] != "") {
            $this->valF['date_tr1'] = $this->dateDB($val['date_tr1']);
        }
        if ($val['date_tr2'] != "") {
            $this->valF['date_tr2'] = $this->dateDB($val['date_tr2']);
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            $form->setType("libelle", "text");
            $form->setType("date_debut", "date");
            $form->setType("date_effet", "date");
            $form->setType("date_tr1", "date");
            $form->setType("date_tr2", "date");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("date_debut", "date");
            $form->setType("date_effet", "date");
            $form->setType("date_tr1", "date");
            $form->setType("date_tr2", "date");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("date_debut", "hiddenstatic");
            $form->setType("date_effet", "hiddenstatic");
            $form->setType("date_tr1", "hiddenstatic");
            $form->setType("date_tr2", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("libelle", "static");
            $form->setType("date_debut", "datestatic");
            $form->setType("date_effet", "datestatic");
            $form->setType("date_tr1", "datestatic");
            $form->setType("date_tr2", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('date_debut','fdate(this)');
        $form->setOnchange('date_effet','fdate(this)');
        $form->setOnchange('date_tr1','fdate(this)');
        $form->setOnchange('date_tr2','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("date_debut", 12);
        $form->setTaille("date_effet", 12);
        $form->setTaille("date_tr1", 12);
        $form->setTaille("date_tr2", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("libelle", 150);
        $form->setMax("date_debut", 12);
        $form->setMax("date_effet", 12);
        $form->setMax("date_tr1", 12);
        $form->setMax("date_tr2", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('date_debut', __('date_debut'));
        $form->setLib('date_effet', __('date_effet'));
        $form->setLib('date_tr1', __('date_tr1'));
        $form->setLib('date_tr2', __('date_tr2'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
