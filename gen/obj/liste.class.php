<?php
//$Id$ 
//gen openMairie le 16/04/2019 12:55

require_once "../obj/om_dbform.class.php";

class liste_gen extends om_dbform {

    protected $_absolute_class_name = "liste";

    var $table = "liste";
    var $clePrimaire = "liste";
    var $typeCle = "A";
    var $required_field = array(
        "libelle_liste",
        "liste",
        "liste_insee"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle_liste");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "liste",
            "libelle_liste",
            "liste_insee",
            "officielle",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['liste'] = $val['liste'];
        $this->valF['libelle_liste'] = $val['libelle_liste'];
        $this->valF['liste_insee'] = $val['liste_insee'];
        if ($val['officielle'] == 1 || $val['officielle'] == "t" || $val['officielle'] == "Oui") {
            $this->valF['officielle'] = true;
        } else {
            $this->valF['officielle'] = false;
        }
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("liste", "text");
            $form->setType("libelle_liste", "text");
            $form->setType("liste_insee", "text");
            $form->setType("officielle", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("liste", "hiddenstatic");
            $form->setType("libelle_liste", "text");
            $form->setType("liste_insee", "text");
            $form->setType("officielle", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("liste", "hiddenstatic");
            $form->setType("libelle_liste", "hiddenstatic");
            $form->setType("liste_insee", "hiddenstatic");
            $form->setType("officielle", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("liste", "static");
            $form->setType("libelle_liste", "static");
            $form->setType("liste_insee", "static");
            $form->setType("officielle", "checkboxstatic");
        }

    }

    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("liste", 10);
        $form->setTaille("libelle_liste", 30);
        $form->setTaille("liste_insee", 10);
        $form->setTaille("officielle", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("liste", 6);
        $form->setMax("libelle_liste", 40);
        $form->setMax("liste_insee", 10);
        $form->setMax("officielle", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('liste', __('liste'));
        $form->setLib('libelle_liste', __('libelle_liste'));
        $form->setLib('liste_insee', __('liste_insee'));
        $form->setLib('officielle', __('officielle'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : electeur
        $this->rechercheTable($this->f->db, "electeur", "liste", $id);
        // Verification de la cle secondaire : mouvement
        $this->rechercheTable($this->f->db, "mouvement", "liste", $id);
        // Verification de la cle secondaire : numerobureau
        $this->rechercheTable($this->f->db, "numerobureau", "liste", $id);
    }


}
