<?php
//$Id$ 
//gen openMairie le 04/11/2021 15:56

require_once "../obj/om_dbform.class.php";

class fichier_gen extends om_dbform {

    protected $_absolute_class_name = "fichier";

    var $table = "fichier";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "creationdate",
        "creationtime",
        "filename",
        "id",
        "mimetype",
        "om_collectivite",
        "size",
        "type",
        "uid"
    );
    
    var $foreign_keys_extended = array(
        "om_collectivite" => array("om_collectivite", ),
    );
    var $abstract_type = array(
        "uid" => "file",
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("creationdate");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "creationdate",
            "creationtime",
            "uid",
            "filename",
            "size",
            "mimetype",
            "type",
            "om_collectivite",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if ($val['creationdate'] != "") {
            $this->valF['creationdate'] = $this->dateDB($val['creationdate']);
        }
            $this->valF['creationtime'] = $val['creationtime'];
        $this->valF['uid'] = $val['uid'];
        $this->valF['filename'] = $val['filename'];
        if (!is_numeric($val['size'])) {
            $this->valF['size'] = ""; // -> requis
        } else {
            $this->valF['size'] = $val['size'];
        }
        $this->valF['mimetype'] = $val['mimetype'];
        $this->valF['type'] = $val['type'];
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            $form->setType("creationdate", "date");
            $form->setType("creationtime", "text");
            if ($this->retourformulaire == "") {
                $form->setType("uid", "upload");
            } else {
                $form->setType("uid", "upload2");
            }
            $form->setType("filename", "text");
            $form->setType("size", "text");
            $form->setType("mimetype", "text");
            $form->setType("type", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            $form->setType("creationdate", "date");
            $form->setType("creationtime", "text");
            if ($this->retourformulaire == "") {
                $form->setType("uid", "upload");
            } else {
                $form->setType("uid", "upload2");
            }
            $form->setType("filename", "text");
            $form->setType("size", "text");
            $form->setType("mimetype", "text");
            $form->setType("type", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("creationdate", "hiddenstatic");
            $form->setType("creationtime", "hiddenstatic");
            $form->setType("uid", "filestatic");
            $form->setType("filename", "hiddenstatic");
            $form->setType("size", "hiddenstatic");
            $form->setType("mimetype", "hiddenstatic");
            $form->setType("type", "hiddenstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("creationdate", "datestatic");
            $form->setType("creationtime", "static");
            $form->setType("uid", "file");
            $form->setType("filename", "static");
            $form->setType("size", "static");
            $form->setType("mimetype", "static");
            $form->setType("type", "static");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('creationdate','fdate(this)');
        $form->setOnchange('size','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("creationdate", 12);
        $form->setTaille("creationtime", 8);
        $form->setTaille("uid", 30);
        $form->setTaille("filename", 30);
        $form->setTaille("size", 11);
        $form->setTaille("mimetype", 30);
        $form->setTaille("type", 30);
        $form->setTaille("om_collectivite", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("creationdate", 12);
        $form->setMax("creationtime", 8);
        $form->setMax("uid", 100);
        $form->setMax("filename", 256);
        $form->setMax("size", 11);
        $form->setMax("mimetype", 256);
        $form->setMax("type", 256);
        $form->setMax("om_collectivite", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('creationdate', __('creationdate'));
        $form->setLib('creationtime', __('creationtime'));
        $form->setLib('uid', __('uid'));
        $form->setLib('filename', __('filename'));
        $form->setLib('size', __('size'));
        $form->setLib('mimetype', __('mimetype'));
        $form->setLib('type', __('type'));
        $form->setLib('om_collectivite', __('om_collectivite'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
