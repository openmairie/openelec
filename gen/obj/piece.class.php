<?php
//$Id$ 
//gen openMairie le 29/01/2019 09:58

require_once "../obj/om_dbform.class.php";

class piece_gen extends om_dbform {

    protected $_absolute_class_name = "piece";

    var $table = "piece";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "fichier",
        "id",
        "libelle",
        "mouvement",
        "om_collectivite",
        "piece_type"
    );
    
    var $foreign_keys_extended = array(
        "mouvement" => array("mouvement", "inscription", "modification", "radiation", ),
        "om_collectivite" => array("om_collectivite", ),
        "piece_type" => array("piece_type", ),
    );
    var $abstract_type = array(
        "fichier" => "file",
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "mouvement",
            "libelle",
            "piece_type",
            "fichier",
            "om_collectivite",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mouvement() {
        return "SELECT mouvement.id, mouvement.etat FROM ".DB_PREFIXE."mouvement ORDER BY mouvement.etat ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mouvement_by_id() {
        return "SELECT mouvement.id, mouvement.etat FROM ".DB_PREFIXE."mouvement WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_piece_type() {
        return "SELECT piece_type.id, piece_type.libelle FROM ".DB_PREFIXE."piece_type WHERE ((piece_type.om_validite_debut IS NULL AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE)) OR (piece_type.om_validite_debut <= CURRENT_DATE AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE))) ORDER BY piece_type.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_piece_type_by_id() {
        return "SELECT piece_type.id, piece_type.libelle FROM ".DB_PREFIXE."piece_type WHERE id = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if (!is_numeric($val['mouvement'])) {
            $this->valF['mouvement'] = ""; // -> requis
        } else {
            $this->valF['mouvement'] = $val['mouvement'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['piece_type'])) {
            $this->valF['piece_type'] = ""; // -> requis
        } else {
            $this->valF['piece_type'] = $val['piece_type'];
        }
        $this->valF['fichier'] = $val['fichier'];
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            if ($this->is_in_context_of_foreign_key("mouvement", $this->retourformulaire)) {
                $form->setType("mouvement", "selecthiddenstatic");
            } else {
                $form->setType("mouvement", "select");
            }
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("piece_type", $this->retourformulaire)) {
                $form->setType("piece_type", "selecthiddenstatic");
            } else {
                $form->setType("piece_type", "select");
            }
            if ($this->retourformulaire == "") {
                $form->setType("fichier", "upload");
            } else {
                $form->setType("fichier", "upload2");
            }
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("mouvement", $this->retourformulaire)) {
                $form->setType("mouvement", "selecthiddenstatic");
            } else {
                $form->setType("mouvement", "select");
            }
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("piece_type", $this->retourformulaire)) {
                $form->setType("piece_type", "selecthiddenstatic");
            } else {
                $form->setType("piece_type", "select");
            }
            if ($this->retourformulaire == "") {
                $form->setType("fichier", "upload");
            } else {
                $form->setType("fichier", "upload2");
            }
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("mouvement", "selectstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("piece_type", "selectstatic");
            $form->setType("fichier", "filestatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("mouvement", "selectstatic");
            $form->setType("libelle", "static");
            $form->setType("piece_type", "selectstatic");
            $form->setType("fichier", "file");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('mouvement','VerifNum(this)');
        $form->setOnchange('piece_type','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("mouvement", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("piece_type", 11);
        $form->setTaille("fichier", 30);
        $form->setTaille("om_collectivite", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("mouvement", 11);
        $form->setMax("libelle", 100);
        $form->setMax("piece_type", 11);
        $form->setMax("fichier", 50);
        $form->setMax("om_collectivite", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('mouvement', __('mouvement'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('piece_type', __('piece_type'));
        $form->setLib('fichier', __('fichier'));
        $form->setLib('om_collectivite', __('om_collectivite'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // mouvement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "mouvement",
            $this->get_var_sql_forminc__sql("mouvement"),
            $this->get_var_sql_forminc__sql("mouvement_by_id"),
            false
        );
        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
        // piece_type
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "piece_type",
            $this->get_var_sql_forminc__sql("piece_type"),
            $this->get_var_sql_forminc__sql("piece_type_by_id"),
            true
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('mouvement', $this->retourformulaire))
                $form->setVal('mouvement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
            if($this->is_in_context_of_foreign_key('piece_type', $this->retourformulaire))
                $form->setVal('piece_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
