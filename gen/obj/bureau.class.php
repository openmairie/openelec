<?php
//$Id$ 
//gen openMairie le 18/02/2022 03:08

require_once "../obj/om_dbform.class.php";

class bureau_gen extends om_dbform {

    protected $_absolute_class_name = "bureau";

    var $table = "bureau";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "canton",
        "circonscription",
        "code",
        "id",
        "libelle",
        "om_collectivite"
    );
    var $unique_key = array(
      array("code","om_collectivite"),
    );
    var $foreign_keys_extended = array(
        "canton" => array("canton", ),
        "circonscription" => array("circonscription", ),
        "om_collectivite" => array("om_collectivite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "code",
            "libelle",
            "adresse1",
            "adresse2",
            "adresse3",
            "canton",
            "circonscription",
            "om_collectivite",
            "referentiel_id",
            "surcharge_adresse_carte_electorale",
            "adresse_numero_voie",
            "adresse_libelle_voie",
            "adresse_complement1",
            "adresse_complement2",
            "adresse_lieu_dit",
            "adresse_code_postal",
            "adresse_ville",
            "adresse_pays",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton() {
        return "SELECT canton.id, canton.libelle FROM ".DB_PREFIXE."canton ORDER BY canton.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton_by_id() {
        return "SELECT canton.id, canton.libelle FROM ".DB_PREFIXE."canton WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_circonscription() {
        return "SELECT circonscription.id, circonscription.libelle FROM ".DB_PREFIXE."circonscription ORDER BY circonscription.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_circonscription_by_id() {
        return "SELECT circonscription.id, circonscription.libelle FROM ".DB_PREFIXE."circonscription WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        $this->valF['code'] = $val['code'];
        $this->valF['libelle'] = $val['libelle'];
        if ($val['adresse1'] == "") {
            $this->valF['adresse1'] = NULL;
        } else {
            $this->valF['adresse1'] = $val['adresse1'];
        }
        if ($val['adresse2'] == "") {
            $this->valF['adresse2'] = NULL;
        } else {
            $this->valF['adresse2'] = $val['adresse2'];
        }
        if ($val['adresse3'] == "") {
            $this->valF['adresse3'] = NULL;
        } else {
            $this->valF['adresse3'] = $val['adresse3'];
        }
        if (!is_numeric($val['canton'])) {
            $this->valF['canton'] = ""; // -> requis
        } else {
            $this->valF['canton'] = $val['canton'];
        }
        if (!is_numeric($val['circonscription'])) {
            $this->valF['circonscription'] = ""; // -> requis
        } else {
            $this->valF['circonscription'] = $val['circonscription'];
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        if (!is_numeric($val['referentiel_id'])) {
            $this->valF['referentiel_id'] = NULL;
        } else {
            $this->valF['referentiel_id'] = $val['referentiel_id'];
        }
        if ($val['surcharge_adresse_carte_electorale'] == 1 || $val['surcharge_adresse_carte_electorale'] == "t" || $val['surcharge_adresse_carte_electorale'] == "Oui") {
            $this->valF['surcharge_adresse_carte_electorale'] = true;
        } else {
            $this->valF['surcharge_adresse_carte_electorale'] = false;
        }
        if ($val['adresse_numero_voie'] == "") {
            $this->valF['adresse_numero_voie'] = NULL;
        } else {
            $this->valF['adresse_numero_voie'] = $val['adresse_numero_voie'];
        }
        if ($val['adresse_libelle_voie'] == "") {
            $this->valF['adresse_libelle_voie'] = NULL;
        } else {
            $this->valF['adresse_libelle_voie'] = $val['adresse_libelle_voie'];
        }
        if ($val['adresse_complement1'] == "") {
            $this->valF['adresse_complement1'] = NULL;
        } else {
            $this->valF['adresse_complement1'] = $val['adresse_complement1'];
        }
        if ($val['adresse_complement2'] == "") {
            $this->valF['adresse_complement2'] = NULL;
        } else {
            $this->valF['adresse_complement2'] = $val['adresse_complement2'];
        }
        if ($val['adresse_lieu_dit'] == "") {
            $this->valF['adresse_lieu_dit'] = NULL;
        } else {
            $this->valF['adresse_lieu_dit'] = $val['adresse_lieu_dit'];
        }
        if ($val['adresse_code_postal'] == "") {
            $this->valF['adresse_code_postal'] = NULL;
        } else {
            $this->valF['adresse_code_postal'] = $val['adresse_code_postal'];
        }
        if ($val['adresse_ville'] == "") {
            $this->valF['adresse_ville'] = NULL;
        } else {
            $this->valF['adresse_ville'] = $val['adresse_ville'];
        }
        if ($val['adresse_pays'] == "") {
            $this->valF['adresse_pays'] = NULL;
        } else {
            $this->valF['adresse_pays'] = $val['adresse_pays'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("adresse3", "text");
            if ($this->is_in_context_of_foreign_key("canton", $this->retourformulaire)) {
                $form->setType("canton", "selecthiddenstatic");
            } else {
                $form->setType("canton", "select");
            }
            if ($this->is_in_context_of_foreign_key("circonscription", $this->retourformulaire)) {
                $form->setType("circonscription", "selecthiddenstatic");
            } else {
                $form->setType("circonscription", "select");
            }
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("referentiel_id", "text");
            $form->setType("surcharge_adresse_carte_electorale", "checkbox");
            $form->setType("adresse_numero_voie", "text");
            $form->setType("adresse_libelle_voie", "text");
            $form->setType("adresse_complement1", "text");
            $form->setType("adresse_complement2", "text");
            $form->setType("adresse_lieu_dit", "text");
            $form->setType("adresse_code_postal", "text");
            $form->setType("adresse_ville", "text");
            $form->setType("adresse_pays", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("adresse1", "text");
            $form->setType("adresse2", "text");
            $form->setType("adresse3", "text");
            if ($this->is_in_context_of_foreign_key("canton", $this->retourformulaire)) {
                $form->setType("canton", "selecthiddenstatic");
            } else {
                $form->setType("canton", "select");
            }
            if ($this->is_in_context_of_foreign_key("circonscription", $this->retourformulaire)) {
                $form->setType("circonscription", "selecthiddenstatic");
            } else {
                $form->setType("circonscription", "select");
            }
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("referentiel_id", "text");
            $form->setType("surcharge_adresse_carte_electorale", "checkbox");
            $form->setType("adresse_numero_voie", "text");
            $form->setType("adresse_libelle_voie", "text");
            $form->setType("adresse_complement1", "text");
            $form->setType("adresse_complement2", "text");
            $form->setType("adresse_lieu_dit", "text");
            $form->setType("adresse_code_postal", "text");
            $form->setType("adresse_ville", "text");
            $form->setType("adresse_pays", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("adresse1", "hiddenstatic");
            $form->setType("adresse2", "hiddenstatic");
            $form->setType("adresse3", "hiddenstatic");
            $form->setType("canton", "selectstatic");
            $form->setType("circonscription", "selectstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
            $form->setType("referentiel_id", "hiddenstatic");
            $form->setType("surcharge_adresse_carte_electorale", "hiddenstatic");
            $form->setType("adresse_numero_voie", "hiddenstatic");
            $form->setType("adresse_libelle_voie", "hiddenstatic");
            $form->setType("adresse_complement1", "hiddenstatic");
            $form->setType("adresse_complement2", "hiddenstatic");
            $form->setType("adresse_lieu_dit", "hiddenstatic");
            $form->setType("adresse_code_postal", "hiddenstatic");
            $form->setType("adresse_ville", "hiddenstatic");
            $form->setType("adresse_pays", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("adresse1", "static");
            $form->setType("adresse2", "static");
            $form->setType("adresse3", "static");
            $form->setType("canton", "selectstatic");
            $form->setType("circonscription", "selectstatic");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("referentiel_id", "static");
            $form->setType("surcharge_adresse_carte_electorale", "checkboxstatic");
            $form->setType("adresse_numero_voie", "static");
            $form->setType("adresse_libelle_voie", "static");
            $form->setType("adresse_complement1", "static");
            $form->setType("adresse_complement2", "static");
            $form->setType("adresse_lieu_dit", "static");
            $form->setType("adresse_code_postal", "static");
            $form->setType("adresse_ville", "static");
            $form->setType("adresse_pays", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('canton','VerifNum(this)');
        $form->setOnchange('circonscription','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
        $form->setOnchange('referentiel_id','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("code", 10);
        $form->setTaille("libelle", 30);
        $form->setTaille("adresse1", 30);
        $form->setTaille("adresse2", 30);
        $form->setTaille("adresse3", 30);
        $form->setTaille("canton", 11);
        $form->setTaille("circonscription", 11);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("referentiel_id", 11);
        $form->setTaille("surcharge_adresse_carte_electorale", 1);
        $form->setTaille("adresse_numero_voie", 20);
        $form->setTaille("adresse_libelle_voie", 30);
        $form->setTaille("adresse_complement1", 30);
        $form->setTaille("adresse_complement2", 30);
        $form->setTaille("adresse_lieu_dit", 30);
        $form->setTaille("adresse_code_postal", 20);
        $form->setTaille("adresse_ville", 30);
        $form->setTaille("adresse_pays", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("code", 4);
        $form->setMax("libelle", 80);
        $form->setMax("adresse1", 40);
        $form->setMax("adresse2", 40);
        $form->setMax("adresse3", 40);
        $form->setMax("canton", 11);
        $form->setMax("circonscription", 11);
        $form->setMax("om_collectivite", 11);
        $form->setMax("referentiel_id", 11);
        $form->setMax("surcharge_adresse_carte_electorale", 1);
        $form->setMax("adresse_numero_voie", 20);
        $form->setMax("adresse_libelle_voie", 150);
        $form->setMax("adresse_complement1", 50);
        $form->setMax("adresse_complement2", 50);
        $form->setMax("adresse_lieu_dit", 50);
        $form->setMax("adresse_code_postal", 20);
        $form->setMax("adresse_ville", 50);
        $form->setMax("adresse_pays", 50);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('code', __('code'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('adresse1', __('adresse1'));
        $form->setLib('adresse2', __('adresse2'));
        $form->setLib('adresse3', __('adresse3'));
        $form->setLib('canton', __('canton'));
        $form->setLib('circonscription', __('circonscription'));
        $form->setLib('om_collectivite', __('om_collectivite'));
        $form->setLib('referentiel_id', __('referentiel_id'));
        $form->setLib('surcharge_adresse_carte_electorale', __('surcharge_adresse_carte_electorale'));
        $form->setLib('adresse_numero_voie', __('adresse_numero_voie'));
        $form->setLib('adresse_libelle_voie', __('adresse_libelle_voie'));
        $form->setLib('adresse_complement1', __('adresse_complement1'));
        $form->setLib('adresse_complement2', __('adresse_complement2'));
        $form->setLib('adresse_lieu_dit', __('adresse_lieu_dit'));
        $form->setLib('adresse_code_postal', __('adresse_code_postal'));
        $form->setLib('adresse_ville', __('adresse_ville'));
        $form->setLib('adresse_pays', __('adresse_pays'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // canton
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "canton",
            $this->get_var_sql_forminc__sql("canton"),
            $this->get_var_sql_forminc__sql("canton_by_id"),
            false
        );
        // circonscription
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "circonscription",
            $this->get_var_sql_forminc__sql("circonscription"),
            $this->get_var_sql_forminc__sql("circonscription_by_id"),
            false
        );
        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('canton', $this->retourformulaire))
                $form->setVal('canton', $idxformulaire);
            if($this->is_in_context_of_foreign_key('circonscription', $this->retourformulaire))
                $form->setVal('circonscription', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : affectation
        $this->rechercheTable($this->f->db, "affectation", "bureau", $id);
        // Verification de la cle secondaire : candidature
        $this->rechercheTable($this->f->db, "candidature", "bureau", $id);
        // Verification de la cle secondaire : composition_bureau
        $this->rechercheTable($this->f->db, "composition_bureau", "bureau", $id);
        // Verification de la cle secondaire : decoupage
        $this->rechercheTable($this->f->db, "decoupage", "bureau", $id);
        // Verification de la cle secondaire : electeur
        $this->rechercheTable($this->f->db, "electeur", "bureau", $id);
        // Verification de la cle secondaire : mouvement
        $this->rechercheTable($this->f->db, "mouvement", "ancien_bureau", $id);
        // Verification de la cle secondaire : mouvement
        $this->rechercheTable($this->f->db, "mouvement", "bureau", $id);
        // Verification de la cle secondaire : numerobureau
        $this->rechercheTable($this->f->db, "numerobureau", "bureau", $id);
    }


}
