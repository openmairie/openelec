<?php
//$Id$ 
//gen openMairie le 15/05/2018 12:51

require_once "../obj/om_dbform.class.php";

class commune_gen extends om_dbform {

    protected $_absolute_class_name = "commune";

    var $table = "commune";
    var $clePrimaire = "code";
    var $typeCle = "A";
    var $required_field = array(
        "code",
        "code_commune",
        "code_departement",
        "libelle_commune"
    );
    
    var $foreign_keys_extended = array(
        "departement" => array("departement", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("code_commune");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "code",
            "code_commune",
            "libelle_commune",
            "code_departement",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_code_departement() {
        return "SELECT departement.code, departement.libelle_departement FROM ".DB_PREFIXE."departement ORDER BY departement.libelle_departement ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_code_departement_by_id() {
        return "SELECT departement.code, departement.libelle_departement FROM ".DB_PREFIXE."departement WHERE code = '<idx>'";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['code'] = $val['code'];
        $this->valF['code_commune'] = $val['code_commune'];
        $this->valF['libelle_commune'] = $val['libelle_commune'];
        $this->valF['code_departement'] = $val['code_departement'];
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("code", "text");
            $form->setType("code_commune", "text");
            $form->setType("libelle_commune", "text");
            if ($this->is_in_context_of_foreign_key("departement", $this->retourformulaire)) {
                $form->setType("code_departement", "selecthiddenstatic");
            } else {
                $form->setType("code_departement", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("code", "hiddenstatic");
            $form->setType("code_commune", "text");
            $form->setType("libelle_commune", "text");
            if ($this->is_in_context_of_foreign_key("departement", $this->retourformulaire)) {
                $form->setType("code_departement", "selecthiddenstatic");
            } else {
                $form->setType("code_departement", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("code", "hiddenstatic");
            $form->setType("code_commune", "hiddenstatic");
            $form->setType("libelle_commune", "hiddenstatic");
            $form->setType("code_departement", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("code", "static");
            $form->setType("code_commune", "static");
            $form->setType("libelle_commune", "static");
            $form->setType("code_departement", "selectstatic");
        }

    }

    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("code", 10);
        $form->setTaille("code_commune", 10);
        $form->setTaille("libelle_commune", 30);
        $form->setTaille("code_departement", 10);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("code", 6);
        $form->setMax("code_commune", 4);
        $form->setMax("libelle_commune", 45);
        $form->setMax("code_departement", 5);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('code', __('code'));
        $form->setLib('code_commune', __('code_commune'));
        $form->setLib('libelle_commune', __('libelle_commune'));
        $form->setLib('code_departement', __('code_departement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // code_departement
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "code_departement",
            $this->get_var_sql_forminc__sql("code_departement"),
            $this->get_var_sql_forminc__sql("code_departement_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('departement', $this->retourformulaire))
                $form->setVal('code_departement', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
