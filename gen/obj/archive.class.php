<?php
//$Id$ 
//gen openMairie le 16/04/2019 12:55

require_once "../obj/om_dbform.class.php";

class archive_gen extends om_dbform {

    protected $_absolute_class_name = "archive";

    var $table = "archive";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "code_nationalite",
        "code_voie",
        "date_naissance",
        "id",
        "liste",
        "om_collectivite"
    );
    
    var $foreign_keys_extended = array(
        "om_collectivite" => array("om_collectivite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("types");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "types",
            "electeur_id",
            "liste",
            "bureau_de_vote_code",
            "bureauforce",
            "numero_bureau",
            "date_modif",
            "utilisateur",
            "civilite",
            "sexe",
            "nom",
            "nom_usage",
            "prenom",
            "date_naissance",
            "code_departement_naissance",
            "libelle_departement_naissance",
            "code_lieu_de_naissance",
            "libelle_lieu_de_naissance",
            "code_nationalite",
            "code_voie",
            "libelle_voie",
            "numero_habitation",
            "complement_numero",
            "complement",
            "provenance",
            "libelle_provenance",
            "ancien_bureau_de_vote_code",
            "observation",
            "resident",
            "adresse_resident",
            "complement_resident",
            "cp_resident",
            "ville_resident",
            "tableau",
            "date_tableau",
            "envoi_cnen",
            "date_cnen",
            "mouvement",
            "typecat",
            "date_mouvement",
            "etat",
            "om_collectivite",
            "telephone",
            "courriel",
            "bureau_de_vote_libelle",
            "ancien_bureau_de_vote_libelle",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if ($val['types'] == "") {
            $this->valF['types'] = ""; // -> default
        } else {
            $this->valF['types'] = $val['types'];
        }
        if (!is_numeric($val['electeur_id'])) {
            $this->valF['electeur_id'] = NULL;
        } else {
            $this->valF['electeur_id'] = $val['electeur_id'];
        }
        $this->valF['liste'] = $val['liste'];
        if ($val['bureau_de_vote_code'] == "") {
            $this->valF['bureau_de_vote_code'] = NULL;
        } else {
            $this->valF['bureau_de_vote_code'] = $val['bureau_de_vote_code'];
        }
        if ($val['bureauforce'] == "") {
            $this->valF['bureauforce'] = ""; // -> default
        } else {
            $this->valF['bureauforce'] = $val['bureauforce'];
        }
        if (!is_numeric($val['numero_bureau'])) {
            $this->valF['numero_bureau'] = NULL;
        } else {
            $this->valF['numero_bureau'] = $val['numero_bureau'];
        }
        if ($val['date_modif'] != "") {
            $this->valF['date_modif'] = $this->dateDB($val['date_modif']);
        } else {
            $this->valF['date_modif'] = NULL;
        }
        if ($val['utilisateur'] == "") {
            $this->valF['utilisateur'] = ""; // -> default
        } else {
            $this->valF['utilisateur'] = $val['utilisateur'];
        }
        if ($val['civilite'] == "") {
            $this->valF['civilite'] = ""; // -> default
        } else {
            $this->valF['civilite'] = $val['civilite'];
        }
        if ($val['sexe'] == "") {
            $this->valF['sexe'] = ""; // -> default
        } else {
            $this->valF['sexe'] = $val['sexe'];
        }
        if ($val['nom'] == "") {
            $this->valF['nom'] = ""; // -> default
        } else {
            $this->valF['nom'] = $val['nom'];
        }
        if ($val['nom_usage'] == "") {
            $this->valF['nom_usage'] = NULL;
        } else {
            $this->valF['nom_usage'] = $val['nom_usage'];
        }
        if ($val['prenom'] == "") {
            $this->valF['prenom'] = ""; // -> default
        } else {
            $this->valF['prenom'] = $val['prenom'];
        }
        if ($val['date_naissance'] != "") {
            $this->valF['date_naissance'] = $this->dateDB($val['date_naissance']);
        }
        if ($val['code_departement_naissance'] == "") {
            $this->valF['code_departement_naissance'] = ""; // -> default
        } else {
            $this->valF['code_departement_naissance'] = $val['code_departement_naissance'];
        }
        if ($val['libelle_departement_naissance'] == "") {
            $this->valF['libelle_departement_naissance'] = ""; // -> default
        } else {
            $this->valF['libelle_departement_naissance'] = $val['libelle_departement_naissance'];
        }
        if ($val['code_lieu_de_naissance'] == "") {
            $this->valF['code_lieu_de_naissance'] = ""; // -> default
        } else {
            $this->valF['code_lieu_de_naissance'] = $val['code_lieu_de_naissance'];
        }
        if ($val['libelle_lieu_de_naissance'] == "") {
            $this->valF['libelle_lieu_de_naissance'] = ""; // -> default
        } else {
            $this->valF['libelle_lieu_de_naissance'] = $val['libelle_lieu_de_naissance'];
        }
        $this->valF['code_nationalite'] = $val['code_nationalite'];
        $this->valF['code_voie'] = $val['code_voie'];
        if ($val['libelle_voie'] == "") {
            $this->valF['libelle_voie'] = ""; // -> default
        } else {
            $this->valF['libelle_voie'] = $val['libelle_voie'];
        }
        if (!is_numeric($val['numero_habitation'])) {
            $this->valF['numero_habitation'] = 0; // -> default
        } else {
            $this->valF['numero_habitation'] = $val['numero_habitation'];
        }
        if ($val['complement_numero'] == "") {
            $this->valF['complement_numero'] = NULL;
        } else {
            $this->valF['complement_numero'] = $val['complement_numero'];
        }
        if ($val['complement'] == "") {
            $this->valF['complement'] = NULL;
        } else {
            $this->valF['complement'] = $val['complement'];
        }
        if ($val['provenance'] == "") {
            $this->valF['provenance'] = NULL;
        } else {
            $this->valF['provenance'] = $val['provenance'];
        }
        if ($val['libelle_provenance'] == "") {
            $this->valF['libelle_provenance'] = ""; // -> default
        } else {
            $this->valF['libelle_provenance'] = $val['libelle_provenance'];
        }
        if ($val['ancien_bureau_de_vote_code'] == "") {
            $this->valF['ancien_bureau_de_vote_code'] = NULL;
        } else {
            $this->valF['ancien_bureau_de_vote_code'] = $val['ancien_bureau_de_vote_code'];
        }
        if ($val['observation'] == "") {
            $this->valF['observation'] = ""; // -> default
        } else {
            $this->valF['observation'] = $val['observation'];
        }
        if ($val['resident'] == "") {
            $this->valF['resident'] = NULL;
        } else {
            $this->valF['resident'] = $val['resident'];
        }
        if ($val['adresse_resident'] == "") {
            $this->valF['adresse_resident'] = NULL;
        } else {
            $this->valF['adresse_resident'] = $val['adresse_resident'];
        }
        if ($val['complement_resident'] == "") {
            $this->valF['complement_resident'] = NULL;
        } else {
            $this->valF['complement_resident'] = $val['complement_resident'];
        }
        if ($val['cp_resident'] == "") {
            $this->valF['cp_resident'] = NULL;
        } else {
            $this->valF['cp_resident'] = $val['cp_resident'];
        }
        if ($val['ville_resident'] == "") {
            $this->valF['ville_resident'] = NULL;
        } else {
            $this->valF['ville_resident'] = $val['ville_resident'];
        }
        if ($val['tableau'] == "") {
            $this->valF['tableau'] = NULL;
        } else {
            $this->valF['tableau'] = $val['tableau'];
        }
        if ($val['date_tableau'] != "") {
            $this->valF['date_tableau'] = $this->dateDB($val['date_tableau']);
        } else {
            $this->valF['date_tableau'] = NULL;
        }
        if ($val['envoi_cnen'] == "") {
            $this->valF['envoi_cnen'] = NULL;
        } else {
            $this->valF['envoi_cnen'] = $val['envoi_cnen'];
        }
        if ($val['date_cnen'] != "") {
            $this->valF['date_cnen'] = $this->dateDB($val['date_cnen']);
        } else {
            $this->valF['date_cnen'] = NULL;
        }
        if (!is_numeric($val['mouvement'])) {
            $this->valF['mouvement'] = 0; // -> default
        } else {
            $this->valF['mouvement'] = $val['mouvement'];
        }
        if ($val['typecat'] == "") {
            $this->valF['typecat'] = ""; // -> default
        } else {
            $this->valF['typecat'] = $val['typecat'];
        }
        if ($val['date_mouvement'] != "") {
            $this->valF['date_mouvement'] = $this->dateDB($val['date_mouvement']);
        } else {
            $this->valF['date_mouvement'] = NULL;
        }
        if ($val['etat'] == "") {
            $this->valF['etat'] = NULL;
        } else {
            $this->valF['etat'] = $val['etat'];
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        if ($val['telephone'] == "") {
            $this->valF['telephone'] = NULL;
        } else {
            $this->valF['telephone'] = $val['telephone'];
        }
        if ($val['courriel'] == "") {
            $this->valF['courriel'] = NULL;
        } else {
            $this->valF['courriel'] = $val['courriel'];
        }
        if ($val['bureau_de_vote_libelle'] == "") {
            $this->valF['bureau_de_vote_libelle'] = NULL;
        } else {
            $this->valF['bureau_de_vote_libelle'] = $val['bureau_de_vote_libelle'];
        }
        if ($val['ancien_bureau_de_vote_libelle'] == "") {
            $this->valF['ancien_bureau_de_vote_libelle'] = NULL;
        } else {
            $this->valF['ancien_bureau_de_vote_libelle'] = $val['ancien_bureau_de_vote_libelle'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            $form->setType("types", "text");
            $form->setType("electeur_id", "text");
            $form->setType("liste", "text");
            $form->setType("bureau_de_vote_code", "text");
            $form->setType("bureauforce", "text");
            $form->setType("numero_bureau", "text");
            $form->setType("date_modif", "date");
            $form->setType("utilisateur", "text");
            $form->setType("civilite", "text");
            $form->setType("sexe", "text");
            $form->setType("nom", "text");
            $form->setType("nom_usage", "text");
            $form->setType("prenom", "text");
            $form->setType("date_naissance", "date");
            $form->setType("code_departement_naissance", "text");
            $form->setType("libelle_departement_naissance", "text");
            $form->setType("code_lieu_de_naissance", "text");
            $form->setType("libelle_lieu_de_naissance", "text");
            $form->setType("code_nationalite", "text");
            $form->setType("code_voie", "text");
            $form->setType("libelle_voie", "text");
            $form->setType("numero_habitation", "text");
            $form->setType("complement_numero", "text");
            $form->setType("complement", "text");
            $form->setType("provenance", "text");
            $form->setType("libelle_provenance", "text");
            $form->setType("ancien_bureau_de_vote_code", "text");
            $form->setType("observation", "text");
            $form->setType("resident", "text");
            $form->setType("adresse_resident", "text");
            $form->setType("complement_resident", "text");
            $form->setType("cp_resident", "text");
            $form->setType("ville_resident", "text");
            $form->setType("tableau", "text");
            $form->setType("date_tableau", "date");
            $form->setType("envoi_cnen", "text");
            $form->setType("date_cnen", "date");
            $form->setType("mouvement", "text");
            $form->setType("typecat", "text");
            $form->setType("date_mouvement", "date");
            $form->setType("etat", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("telephone", "text");
            $form->setType("courriel", "text");
            $form->setType("bureau_de_vote_libelle", "text");
            $form->setType("ancien_bureau_de_vote_libelle", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            $form->setType("types", "text");
            $form->setType("electeur_id", "text");
            $form->setType("liste", "text");
            $form->setType("bureau_de_vote_code", "text");
            $form->setType("bureauforce", "text");
            $form->setType("numero_bureau", "text");
            $form->setType("date_modif", "date");
            $form->setType("utilisateur", "text");
            $form->setType("civilite", "text");
            $form->setType("sexe", "text");
            $form->setType("nom", "text");
            $form->setType("nom_usage", "text");
            $form->setType("prenom", "text");
            $form->setType("date_naissance", "date");
            $form->setType("code_departement_naissance", "text");
            $form->setType("libelle_departement_naissance", "text");
            $form->setType("code_lieu_de_naissance", "text");
            $form->setType("libelle_lieu_de_naissance", "text");
            $form->setType("code_nationalite", "text");
            $form->setType("code_voie", "text");
            $form->setType("libelle_voie", "text");
            $form->setType("numero_habitation", "text");
            $form->setType("complement_numero", "text");
            $form->setType("complement", "text");
            $form->setType("provenance", "text");
            $form->setType("libelle_provenance", "text");
            $form->setType("ancien_bureau_de_vote_code", "text");
            $form->setType("observation", "text");
            $form->setType("resident", "text");
            $form->setType("adresse_resident", "text");
            $form->setType("complement_resident", "text");
            $form->setType("cp_resident", "text");
            $form->setType("ville_resident", "text");
            $form->setType("tableau", "text");
            $form->setType("date_tableau", "date");
            $form->setType("envoi_cnen", "text");
            $form->setType("date_cnen", "date");
            $form->setType("mouvement", "text");
            $form->setType("typecat", "text");
            $form->setType("date_mouvement", "date");
            $form->setType("etat", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("telephone", "text");
            $form->setType("courriel", "text");
            $form->setType("bureau_de_vote_libelle", "text");
            $form->setType("ancien_bureau_de_vote_libelle", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("types", "hiddenstatic");
            $form->setType("electeur_id", "hiddenstatic");
            $form->setType("liste", "hiddenstatic");
            $form->setType("bureau_de_vote_code", "hiddenstatic");
            $form->setType("bureauforce", "hiddenstatic");
            $form->setType("numero_bureau", "hiddenstatic");
            $form->setType("date_modif", "hiddenstatic");
            $form->setType("utilisateur", "hiddenstatic");
            $form->setType("civilite", "hiddenstatic");
            $form->setType("sexe", "hiddenstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("nom_usage", "hiddenstatic");
            $form->setType("prenom", "hiddenstatic");
            $form->setType("date_naissance", "hiddenstatic");
            $form->setType("code_departement_naissance", "hiddenstatic");
            $form->setType("libelle_departement_naissance", "hiddenstatic");
            $form->setType("code_lieu_de_naissance", "hiddenstatic");
            $form->setType("libelle_lieu_de_naissance", "hiddenstatic");
            $form->setType("code_nationalite", "hiddenstatic");
            $form->setType("code_voie", "hiddenstatic");
            $form->setType("libelle_voie", "hiddenstatic");
            $form->setType("numero_habitation", "hiddenstatic");
            $form->setType("complement_numero", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            $form->setType("provenance", "hiddenstatic");
            $form->setType("libelle_provenance", "hiddenstatic");
            $form->setType("ancien_bureau_de_vote_code", "hiddenstatic");
            $form->setType("observation", "hiddenstatic");
            $form->setType("resident", "hiddenstatic");
            $form->setType("adresse_resident", "hiddenstatic");
            $form->setType("complement_resident", "hiddenstatic");
            $form->setType("cp_resident", "hiddenstatic");
            $form->setType("ville_resident", "hiddenstatic");
            $form->setType("tableau", "hiddenstatic");
            $form->setType("date_tableau", "hiddenstatic");
            $form->setType("envoi_cnen", "hiddenstatic");
            $form->setType("date_cnen", "hiddenstatic");
            $form->setType("mouvement", "hiddenstatic");
            $form->setType("typecat", "hiddenstatic");
            $form->setType("date_mouvement", "hiddenstatic");
            $form->setType("etat", "hiddenstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
            $form->setType("telephone", "hiddenstatic");
            $form->setType("courriel", "hiddenstatic");
            $form->setType("bureau_de_vote_libelle", "hiddenstatic");
            $form->setType("ancien_bureau_de_vote_libelle", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("types", "static");
            $form->setType("electeur_id", "static");
            $form->setType("liste", "static");
            $form->setType("bureau_de_vote_code", "static");
            $form->setType("bureauforce", "static");
            $form->setType("numero_bureau", "static");
            $form->setType("date_modif", "datestatic");
            $form->setType("utilisateur", "static");
            $form->setType("civilite", "static");
            $form->setType("sexe", "static");
            $form->setType("nom", "static");
            $form->setType("nom_usage", "static");
            $form->setType("prenom", "static");
            $form->setType("date_naissance", "datestatic");
            $form->setType("code_departement_naissance", "static");
            $form->setType("libelle_departement_naissance", "static");
            $form->setType("code_lieu_de_naissance", "static");
            $form->setType("libelle_lieu_de_naissance", "static");
            $form->setType("code_nationalite", "static");
            $form->setType("code_voie", "static");
            $form->setType("libelle_voie", "static");
            $form->setType("numero_habitation", "static");
            $form->setType("complement_numero", "static");
            $form->setType("complement", "static");
            $form->setType("provenance", "static");
            $form->setType("libelle_provenance", "static");
            $form->setType("ancien_bureau_de_vote_code", "static");
            $form->setType("observation", "static");
            $form->setType("resident", "static");
            $form->setType("adresse_resident", "static");
            $form->setType("complement_resident", "static");
            $form->setType("cp_resident", "static");
            $form->setType("ville_resident", "static");
            $form->setType("tableau", "static");
            $form->setType("date_tableau", "datestatic");
            $form->setType("envoi_cnen", "static");
            $form->setType("date_cnen", "datestatic");
            $form->setType("mouvement", "static");
            $form->setType("typecat", "static");
            $form->setType("date_mouvement", "datestatic");
            $form->setType("etat", "static");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("telephone", "static");
            $form->setType("courriel", "static");
            $form->setType("bureau_de_vote_libelle", "static");
            $form->setType("ancien_bureau_de_vote_libelle", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('electeur_id','VerifNum(this)');
        $form->setOnchange('numero_bureau','VerifNum(this)');
        $form->setOnchange('date_modif','fdate(this)');
        $form->setOnchange('date_naissance','fdate(this)');
        $form->setOnchange('numero_habitation','VerifNum(this)');
        $form->setOnchange('date_tableau','fdate(this)');
        $form->setOnchange('date_cnen','fdate(this)');
        $form->setOnchange('mouvement','VerifNum(this)');
        $form->setOnchange('date_mouvement','fdate(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("types", 20);
        $form->setTaille("electeur_id", 11);
        $form->setTaille("liste", 10);
        $form->setTaille("bureau_de_vote_code", 20);
        $form->setTaille("bureauforce", 10);
        $form->setTaille("numero_bureau", 20);
        $form->setTaille("date_modif", 12);
        $form->setTaille("utilisateur", 30);
        $form->setTaille("civilite", 10);
        $form->setTaille("sexe", 10);
        $form->setTaille("nom", 30);
        $form->setTaille("nom_usage", 30);
        $form->setTaille("prenom", 30);
        $form->setTaille("date_naissance", 12);
        $form->setTaille("code_departement_naissance", 10);
        $form->setTaille("libelle_departement_naissance", 30);
        $form->setTaille("code_lieu_de_naissance", 10);
        $form->setTaille("libelle_lieu_de_naissance", 30);
        $form->setTaille("code_nationalite", 10);
        $form->setTaille("code_voie", 10);
        $form->setTaille("libelle_voie", 30);
        $form->setTaille("numero_habitation", 11);
        $form->setTaille("complement_numero", 10);
        $form->setTaille("complement", 30);
        $form->setTaille("provenance", 10);
        $form->setTaille("libelle_provenance", 30);
        $form->setTaille("ancien_bureau_de_vote_code", 20);
        $form->setTaille("observation", 30);
        $form->setTaille("resident", 10);
        $form->setTaille("adresse_resident", 30);
        $form->setTaille("complement_resident", 30);
        $form->setTaille("cp_resident", 10);
        $form->setTaille("ville_resident", 30);
        $form->setTaille("tableau", 10);
        $form->setTaille("date_tableau", 12);
        $form->setTaille("envoi_cnen", 10);
        $form->setTaille("date_cnen", 12);
        $form->setTaille("mouvement", 20);
        $form->setTaille("typecat", 20);
        $form->setTaille("date_mouvement", 12);
        $form->setTaille("etat", 10);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("telephone", 30);
        $form->setTaille("courriel", 30);
        $form->setTaille("bureau_de_vote_libelle", 30);
        $form->setTaille("ancien_bureau_de_vote_libelle", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("types", 20);
        $form->setMax("electeur_id", 11);
        $form->setMax("liste", 6);
        $form->setMax("bureau_de_vote_code", 20);
        $form->setMax("bureauforce", 3);
        $form->setMax("numero_bureau", 20);
        $form->setMax("date_modif", 12);
        $form->setMax("utilisateur", 30);
        $form->setMax("civilite", 4);
        $form->setMax("sexe", 1);
        $form->setMax("nom", 63);
        $form->setMax("nom_usage", 63);
        $form->setMax("prenom", 70);
        $form->setMax("date_naissance", 12);
        $form->setMax("code_departement_naissance", 5);
        $form->setMax("libelle_departement_naissance", 70);
        $form->setMax("code_lieu_de_naissance", 6);
        $form->setMax("libelle_lieu_de_naissance", 70);
        $form->setMax("code_nationalite", 4);
        $form->setMax("code_voie", 10);
        $form->setMax("libelle_voie", 50);
        $form->setMax("numero_habitation", 11);
        $form->setMax("complement_numero", 10);
        $form->setMax("complement", 80);
        $form->setMax("provenance", 6);
        $form->setMax("libelle_provenance", 50);
        $form->setMax("ancien_bureau_de_vote_code", 20);
        $form->setMax("observation", 100);
        $form->setMax("resident", 3);
        $form->setMax("adresse_resident", 40);
        $form->setMax("complement_resident", 40);
        $form->setMax("cp_resident", 10);
        $form->setMax("ville_resident", 50);
        $form->setMax("tableau", 10);
        $form->setMax("date_tableau", 12);
        $form->setMax("envoi_cnen", 3);
        $form->setMax("date_cnen", 12);
        $form->setMax("mouvement", 20);
        $form->setMax("typecat", 20);
        $form->setMax("date_mouvement", 12);
        $form->setMax("etat", 10);
        $form->setMax("om_collectivite", 11);
        $form->setMax("telephone", 30);
        $form->setMax("courriel", 100);
        $form->setMax("bureau_de_vote_libelle", 100);
        $form->setMax("ancien_bureau_de_vote_libelle", 100);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('types', __('types'));
        $form->setLib('electeur_id', __('electeur_id'));
        $form->setLib('liste', __('liste'));
        $form->setLib('bureau_de_vote_code', __('bureau_de_vote_code'));
        $form->setLib('bureauforce', __('bureauforce'));
        $form->setLib('numero_bureau', __('numero_bureau'));
        $form->setLib('date_modif', __('date_modif'));
        $form->setLib('utilisateur', __('utilisateur'));
        $form->setLib('civilite', __('civilite'));
        $form->setLib('sexe', __('sexe'));
        $form->setLib('nom', __('nom'));
        $form->setLib('nom_usage', __('nom_usage'));
        $form->setLib('prenom', __('prenom'));
        $form->setLib('date_naissance', __('date_naissance'));
        $form->setLib('code_departement_naissance', __('code_departement_naissance'));
        $form->setLib('libelle_departement_naissance', __('libelle_departement_naissance'));
        $form->setLib('code_lieu_de_naissance', __('code_lieu_de_naissance'));
        $form->setLib('libelle_lieu_de_naissance', __('libelle_lieu_de_naissance'));
        $form->setLib('code_nationalite', __('code_nationalite'));
        $form->setLib('code_voie', __('code_voie'));
        $form->setLib('libelle_voie', __('libelle_voie'));
        $form->setLib('numero_habitation', __('numero_habitation'));
        $form->setLib('complement_numero', __('complement_numero'));
        $form->setLib('complement', __('complement'));
        $form->setLib('provenance', __('provenance'));
        $form->setLib('libelle_provenance', __('libelle_provenance'));
        $form->setLib('ancien_bureau_de_vote_code', __('ancien_bureau_de_vote_code'));
        $form->setLib('observation', __('observation'));
        $form->setLib('resident', __('resident'));
        $form->setLib('adresse_resident', __('adresse_resident'));
        $form->setLib('complement_resident', __('complement_resident'));
        $form->setLib('cp_resident', __('cp_resident'));
        $form->setLib('ville_resident', __('ville_resident'));
        $form->setLib('tableau', __('tableau'));
        $form->setLib('date_tableau', __('date_tableau'));
        $form->setLib('envoi_cnen', __('envoi_cnen'));
        $form->setLib('date_cnen', __('date_cnen'));
        $form->setLib('mouvement', __('mouvement'));
        $form->setLib('typecat', __('typecat'));
        $form->setLib('date_mouvement', __('date_mouvement'));
        $form->setLib('etat', __('etat'));
        $form->setLib('om_collectivite', __('om_collectivite'));
        $form->setLib('telephone', __('telephone'));
        $form->setLib('courriel', __('courriel'));
        $form->setLib('bureau_de_vote_libelle', __('bureau_de_vote_libelle'));
        $form->setLib('ancien_bureau_de_vote_libelle', __('ancien_bureau_de_vote_libelle'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
