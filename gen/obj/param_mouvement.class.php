<?php
//$Id$ 
//gen openMairie le 15/02/2019 15:50

require_once "../obj/om_dbform.class.php";

class param_mouvement_gen extends om_dbform {

    protected $_absolute_class_name = "param_mouvement";

    var $table = "param_mouvement";
    var $clePrimaire = "code";
    var $typeCle = "A";
    
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "code",
            "libelle",
            "typecat",
            "effet",
            "cnen",
            "codeinscription",
            "coderadiation",
            "edition_carte_electeur",
            "insee_import_radiation",
            "om_validite_debut",
            "om_validite_fin",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if ($val['code'] == "") {
            $this->valF['code'] = ""; // -> default
        } else {
            $this->valF['code'] = $val['code'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = ""; // -> default
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
        if ($val['typecat'] == "") {
            $this->valF['typecat'] = ""; // -> default
        } else {
            $this->valF['typecat'] = $val['typecat'];
        }
        if ($val['effet'] == "") {
            $this->valF['effet'] = ""; // -> default
        } else {
            $this->valF['effet'] = $val['effet'];
        }
        if ($val['cnen'] == "") {
            $this->valF['cnen'] = ""; // -> default
        } else {
            $this->valF['cnen'] = $val['cnen'];
        }
        if ($val['codeinscription'] == "") {
            $this->valF['codeinscription'] = ""; // -> default
        } else {
            $this->valF['codeinscription'] = $val['codeinscription'];
        }
        if ($val['coderadiation'] == "") {
            $this->valF['coderadiation'] = ""; // -> default
        } else {
            $this->valF['coderadiation'] = $val['coderadiation'];
        }
        if (!is_numeric($val['edition_carte_electeur'])) {
            $this->valF['edition_carte_electeur'] = NULL;
        } else {
            $this->valF['edition_carte_electeur'] = $val['edition_carte_electeur'];
        }
        if ($val['insee_import_radiation'] == "") {
            $this->valF['insee_import_radiation'] = NULL;
        } else {
            $this->valF['insee_import_radiation'] = $val['insee_import_radiation'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("typecat", "text");
            $form->setType("effet", "text");
            $form->setType("cnen", "text");
            $form->setType("codeinscription", "text");
            $form->setType("coderadiation", "text");
            $form->setType("edition_carte_electeur", "text");
            $form->setType("insee_import_radiation", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("typecat", "text");
            $form->setType("effet", "text");
            $form->setType("cnen", "text");
            $form->setType("codeinscription", "text");
            $form->setType("coderadiation", "text");
            $form->setType("edition_carte_electeur", "text");
            $form->setType("insee_import_radiation", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("typecat", "hiddenstatic");
            $form->setType("effet", "hiddenstatic");
            $form->setType("cnen", "hiddenstatic");
            $form->setType("codeinscription", "hiddenstatic");
            $form->setType("coderadiation", "hiddenstatic");
            $form->setType("edition_carte_electeur", "hiddenstatic");
            $form->setType("insee_import_radiation", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("typecat", "static");
            $form->setType("effet", "static");
            $form->setType("cnen", "static");
            $form->setType("codeinscription", "static");
            $form->setType("coderadiation", "static");
            $form->setType("edition_carte_electeur", "static");
            $form->setType("insee_import_radiation", "static");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('edition_carte_electeur','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("code", 20);
        $form->setTaille("libelle", 30);
        $form->setTaille("typecat", 20);
        $form->setTaille("effet", 10);
        $form->setTaille("cnen", 10);
        $form->setTaille("codeinscription", 10);
        $form->setTaille("coderadiation", 10);
        $form->setTaille("edition_carte_electeur", 11);
        $form->setTaille("insee_import_radiation", 30);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("code", 20);
        $form->setMax("libelle", 100);
        $form->setMax("typecat", 20);
        $form->setMax("effet", 10);
        $form->setMax("cnen", 3);
        $form->setMax("codeinscription", 1);
        $form->setMax("coderadiation", 1);
        $form->setMax("edition_carte_electeur", 11);
        $form->setMax("insee_import_radiation", 100);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('code', __('code'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('typecat', __('typecat'));
        $form->setLib('effet', __('effet'));
        $form->setLib('cnen', __('cnen'));
        $form->setLib('codeinscription', __('codeinscription'));
        $form->setLib('coderadiation', __('coderadiation'));
        $form->setLib('edition_carte_electeur', __('edition_carte_electeur'));
        $form->setLib('insee_import_radiation', __('insee_import_radiation'));
        $form->setLib('om_validite_debut', __('om_validite_debut'));
        $form->setLib('om_validite_fin', __('om_validite_fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
