<?php
//$Id$ 
//gen openMairie le 18/02/2022 01:27

require_once "../obj/om_dbform.class.php";

class candidature_gen extends om_dbform {

    protected $_absolute_class_name = "candidature";

    var $table = "candidature";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "agent",
        "bureau",
        "composition_scrutin",
        "id",
        "periode",
        "poste"
    );
    
    var $foreign_keys_extended = array(
        "agent" => array("agent", ),
        "bureau" => array("bureau", ),
        "composition_scrutin" => array("composition_scrutin", ),
        "periode" => array("periode", ),
        "poste" => array("poste", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("agent");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "agent",
            "composition_scrutin",
            "periode",
            "poste",
            "bureau",
            "note",
            "decision",
            "recuperation",
            "debut",
            "fin",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_agent() {
        return "SELECT agent.id, agent.nom FROM ".DB_PREFIXE."agent ORDER BY agent.nom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_agent_by_id() {
        return "SELECT agent.id, agent.nom FROM ".DB_PREFIXE."agent WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau ORDER BY bureau.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_composition_scrutin() {
        return "SELECT composition_scrutin.id, composition_scrutin.libelle FROM ".DB_PREFIXE."composition_scrutin ORDER BY composition_scrutin.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_composition_scrutin_by_id() {
        return "SELECT composition_scrutin.id, composition_scrutin.libelle FROM ".DB_PREFIXE."composition_scrutin WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_periode() {
        return "SELECT periode.periode, periode.libelle FROM ".DB_PREFIXE."periode ORDER BY periode.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_periode_by_id() {
        return "SELECT periode.periode, periode.libelle FROM ".DB_PREFIXE."periode WHERE periode = '<idx>'";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_poste() {
        return "SELECT poste.poste, poste.libelle FROM ".DB_PREFIXE."poste ORDER BY poste.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_poste_by_id() {
        return "SELECT poste.poste, poste.libelle FROM ".DB_PREFIXE."poste WHERE poste = '<idx>'";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if (!is_numeric($val['agent'])) {
            $this->valF['agent'] = ""; // -> requis
        } else {
            $this->valF['agent'] = $val['agent'];
        }
        if (!is_numeric($val['composition_scrutin'])) {
            $this->valF['composition_scrutin'] = ""; // -> requis
        } else {
            $this->valF['composition_scrutin'] = $val['composition_scrutin'];
        }
        $this->valF['periode'] = $val['periode'];
        $this->valF['poste'] = $val['poste'];
        if (!is_numeric($val['bureau'])) {
            $this->valF['bureau'] = ""; // -> requis
        } else {
            $this->valF['bureau'] = $val['bureau'];
        }
            $this->valF['note'] = $val['note'];
        if ($val['decision'] == 1 || $val['decision'] == "t" || $val['decision'] == "Oui") {
            $this->valF['decision'] = true;
        } else {
            $this->valF['decision'] = false;
        }
        if ($val['recuperation'] == 1 || $val['recuperation'] == "t" || $val['recuperation'] == "Oui") {
            $this->valF['recuperation'] = true;
        } else {
            $this->valF['recuperation'] = false;
        }
            $this->valF['debut'] = $val['debut'];
            $this->valF['fin'] = $val['fin'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            if ($this->is_in_context_of_foreign_key("agent", $this->retourformulaire)) {
                $form->setType("agent", "selecthiddenstatic");
            } else {
                $form->setType("agent", "select");
            }
            if ($this->is_in_context_of_foreign_key("composition_scrutin", $this->retourformulaire)) {
                $form->setType("composition_scrutin", "selecthiddenstatic");
            } else {
                $form->setType("composition_scrutin", "select");
            }
            if ($this->is_in_context_of_foreign_key("periode", $this->retourformulaire)) {
                $form->setType("periode", "selecthiddenstatic");
            } else {
                $form->setType("periode", "select");
            }
            if ($this->is_in_context_of_foreign_key("poste", $this->retourformulaire)) {
                $form->setType("poste", "selecthiddenstatic");
            } else {
                $form->setType("poste", "select");
            }
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            $form->setType("note", "textarea");
            $form->setType("decision", "checkbox");
            $form->setType("recuperation", "checkbox");
            $form->setType("debut", "text");
            $form->setType("fin", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("agent", $this->retourformulaire)) {
                $form->setType("agent", "selecthiddenstatic");
            } else {
                $form->setType("agent", "select");
            }
            if ($this->is_in_context_of_foreign_key("composition_scrutin", $this->retourformulaire)) {
                $form->setType("composition_scrutin", "selecthiddenstatic");
            } else {
                $form->setType("composition_scrutin", "select");
            }
            if ($this->is_in_context_of_foreign_key("periode", $this->retourformulaire)) {
                $form->setType("periode", "selecthiddenstatic");
            } else {
                $form->setType("periode", "select");
            }
            if ($this->is_in_context_of_foreign_key("poste", $this->retourformulaire)) {
                $form->setType("poste", "selecthiddenstatic");
            } else {
                $form->setType("poste", "select");
            }
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            $form->setType("note", "textarea");
            $form->setType("decision", "checkbox");
            $form->setType("recuperation", "checkbox");
            $form->setType("debut", "text");
            $form->setType("fin", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("agent", "selectstatic");
            $form->setType("composition_scrutin", "selectstatic");
            $form->setType("periode", "selectstatic");
            $form->setType("poste", "selectstatic");
            $form->setType("bureau", "selectstatic");
            $form->setType("note", "hiddenstatic");
            $form->setType("decision", "hiddenstatic");
            $form->setType("recuperation", "hiddenstatic");
            $form->setType("debut", "hiddenstatic");
            $form->setType("fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("agent", "selectstatic");
            $form->setType("composition_scrutin", "selectstatic");
            $form->setType("periode", "selectstatic");
            $form->setType("poste", "selectstatic");
            $form->setType("bureau", "selectstatic");
            $form->setType("note", "textareastatic");
            $form->setType("decision", "checkboxstatic");
            $form->setType("recuperation", "checkboxstatic");
            $form->setType("debut", "static");
            $form->setType("fin", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('agent','VerifNum(this)');
        $form->setOnchange('composition_scrutin','VerifNum(this)');
        $form->setOnchange('bureau','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("agent", 11);
        $form->setTaille("composition_scrutin", 11);
        $form->setTaille("periode", 20);
        $form->setTaille("poste", 20);
        $form->setTaille("bureau", 11);
        $form->setTaille("note", 80);
        $form->setTaille("decision", 1);
        $form->setTaille("recuperation", 1);
        $form->setTaille("debut", 8);
        $form->setTaille("fin", 8);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("agent", 11);
        $form->setMax("composition_scrutin", 11);
        $form->setMax("periode", 20);
        $form->setMax("poste", 20);
        $form->setMax("bureau", 11);
        $form->setMax("note", 6);
        $form->setMax("decision", 1);
        $form->setMax("recuperation", 1);
        $form->setMax("debut", 8);
        $form->setMax("fin", 8);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('agent', __('agent'));
        $form->setLib('composition_scrutin', __('composition_scrutin'));
        $form->setLib('periode', __('periode'));
        $form->setLib('poste', __('poste'));
        $form->setLib('bureau', __('bureau'));
        $form->setLib('note', __('note'));
        $form->setLib('decision', __('decision'));
        $form->setLib('recuperation', __('recuperation'));
        $form->setLib('debut', __('debut'));
        $form->setLib('fin', __('fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // agent
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "agent",
            $this->get_var_sql_forminc__sql("agent"),
            $this->get_var_sql_forminc__sql("agent_by_id"),
            false
        );
        // bureau
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "bureau",
            $this->get_var_sql_forminc__sql("bureau"),
            $this->get_var_sql_forminc__sql("bureau_by_id"),
            false
        );
        // composition_scrutin
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "composition_scrutin",
            $this->get_var_sql_forminc__sql("composition_scrutin"),
            $this->get_var_sql_forminc__sql("composition_scrutin_by_id"),
            false
        );
        // periode
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "periode",
            $this->get_var_sql_forminc__sql("periode"),
            $this->get_var_sql_forminc__sql("periode_by_id"),
            false
        );
        // poste
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "poste",
            $this->get_var_sql_forminc__sql("poste"),
            $this->get_var_sql_forminc__sql("poste_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('agent', $this->retourformulaire))
                $form->setVal('agent', $idxformulaire);
            if($this->is_in_context_of_foreign_key('bureau', $this->retourformulaire))
                $form->setVal('bureau', $idxformulaire);
            if($this->is_in_context_of_foreign_key('composition_scrutin', $this->retourformulaire))
                $form->setVal('composition_scrutin', $idxformulaire);
            if($this->is_in_context_of_foreign_key('periode', $this->retourformulaire))
                $form->setVal('periode', $idxformulaire);
            if($this->is_in_context_of_foreign_key('poste', $this->retourformulaire))
                $form->setVal('poste', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
