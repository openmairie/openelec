<?php
//$Id$ 
//gen openMairie le 15/05/2018 12:51

require_once "../obj/om_dbform.class.php";

class departement_gen extends om_dbform {

    protected $_absolute_class_name = "departement";

    var $table = "departement";
    var $clePrimaire = "code";
    var $typeCle = "A";
    var $required_field = array(
        "code",
        "libelle_departement"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle_departement");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "code",
            "libelle_departement",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['code'] = $val['code'];
        $this->valF['libelle_departement'] = $val['libelle_departement'];
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("code", "text");
            $form->setType("libelle_departement", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle_departement", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle_departement", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("code", "static");
            $form->setType("libelle_departement", "static");
        }

    }

    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("code", 10);
        $form->setTaille("libelle_departement", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("code", 5);
        $form->setMax("libelle_departement", 51);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('code', __('code'));
        $form->setLib('libelle_departement', __('libelle_departement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : commune
        $this->rechercheTable($this->f->db, "commune", "code_departement", $id);
    }


}
