<?php
//$Id$ 
//gen openMairie le 05/06/2018 12:52

require_once "../obj/om_dbform.class.php";

class numerobureau_gen extends om_dbform {

    protected $_absolute_class_name = "numerobureau";

    var $table = "numerobureau";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "bureau",
        "dernier_numero",
        "dernier_numero_provisoire",
        "id",
        "liste",
        "om_collectivite"
    );
    var $unique_key = array(
      array("bureau","liste","om_collectivite"),
    );
    var $foreign_keys_extended = array(
        "bureau" => array("bureau", ),
        "liste" => array("liste", ),
        "om_collectivite" => array("om_collectivite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("om_collectivite");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "om_collectivite",
            "liste",
            "bureau",
            "dernier_numero",
            "dernier_numero_provisoire",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau ORDER BY bureau.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste() {
        return "SELECT liste.liste, liste.libelle_liste FROM ".DB_PREFIXE."liste ORDER BY liste.libelle_liste ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste_by_id() {
        return "SELECT liste.liste, liste.libelle_liste FROM ".DB_PREFIXE."liste WHERE liste = '<idx>'";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        $this->valF['liste'] = $val['liste'];
        if (!is_numeric($val['bureau'])) {
            $this->valF['bureau'] = ""; // -> requis
        } else {
            $this->valF['bureau'] = $val['bureau'];
        }
        if (!is_numeric($val['dernier_numero'])) {
            $this->valF['dernier_numero'] = ""; // -> requis
        } else {
            $this->valF['dernier_numero'] = $val['dernier_numero'];
        }
        if (!is_numeric($val['dernier_numero_provisoire'])) {
            $this->valF['dernier_numero_provisoire'] = ""; // -> requis
        } else {
            $this->valF['dernier_numero_provisoire'] = $val['dernier_numero_provisoire'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            if ($this->is_in_context_of_foreign_key("liste", $this->retourformulaire)) {
                $form->setType("liste", "selecthiddenstatic");
            } else {
                $form->setType("liste", "select");
            }
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            $form->setType("dernier_numero", "text");
            $form->setType("dernier_numero_provisoire", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            if ($this->is_in_context_of_foreign_key("liste", $this->retourformulaire)) {
                $form->setType("liste", "selecthiddenstatic");
            } else {
                $form->setType("liste", "select");
            }
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            $form->setType("dernier_numero", "text");
            $form->setType("dernier_numero_provisoire", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
            $form->setType("liste", "selectstatic");
            $form->setType("bureau", "selectstatic");
            $form->setType("dernier_numero", "hiddenstatic");
            $form->setType("dernier_numero_provisoire", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("liste", "selectstatic");
            $form->setType("bureau", "selectstatic");
            $form->setType("dernier_numero", "static");
            $form->setType("dernier_numero_provisoire", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
        $form->setOnchange('bureau','VerifNum(this)');
        $form->setOnchange('dernier_numero','VerifNum(this)');
        $form->setOnchange('dernier_numero_provisoire','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("liste", 10);
        $form->setTaille("bureau", 11);
        $form->setTaille("dernier_numero", 20);
        $form->setTaille("dernier_numero_provisoire", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("om_collectivite", 11);
        $form->setMax("liste", 6);
        $form->setMax("bureau", 11);
        $form->setMax("dernier_numero", 20);
        $form->setMax("dernier_numero_provisoire", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('om_collectivite', __('om_collectivite'));
        $form->setLib('liste', __('liste'));
        $form->setLib('bureau', __('bureau'));
        $form->setLib('dernier_numero', __('dernier_numero'));
        $form->setLib('dernier_numero_provisoire', __('dernier_numero_provisoire'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // bureau
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "bureau",
            $this->get_var_sql_forminc__sql("bureau"),
            $this->get_var_sql_forminc__sql("bureau_by_id"),
            false
        );
        // liste
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "liste",
            $this->get_var_sql_forminc__sql("liste"),
            $this->get_var_sql_forminc__sql("liste_by_id"),
            false
        );
        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('bureau', $this->retourformulaire))
                $form->setVal('bureau', $idxformulaire);
            if($this->is_in_context_of_foreign_key('liste', $this->retourformulaire))
                $form->setVal('liste', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
