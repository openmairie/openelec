<?php
//$Id$ 
//gen openMairie le 26/12/2019 10:59

require_once "../obj/om_dbform.class.php";

class agent_gen extends om_dbform {

    protected $_absolute_class_name = "agent";

    var $table = "agent";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "adresse",
        "cp",
        "id",
        "nom",
        "prenom",
        "ville"
    );
    
    var $foreign_keys_extended = array(
        "grade" => array("grade", ),
        "service" => array("service", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("nom");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "nom",
            "prenom",
            "adresse",
            "cp",
            "ville",
            "telephone",
            "service",
            "telephone_pro",
            "grade",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_grade() {
        return "SELECT grade.grade, grade.libelle FROM ".DB_PREFIXE."grade ORDER BY grade.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_grade_by_id() {
        return "SELECT grade.grade, grade.libelle FROM ".DB_PREFIXE."grade WHERE grade = '<idx>'";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service ORDER BY service.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_service_by_id() {
        return "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = '<idx>'";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        $this->valF['nom'] = $val['nom'];
        $this->valF['prenom'] = $val['prenom'];
        $this->valF['adresse'] = $val['adresse'];
        $this->valF['cp'] = $val['cp'];
        $this->valF['ville'] = $val['ville'];
        if ($val['telephone'] == "") {
            $this->valF['telephone'] = NULL;
        } else {
            $this->valF['telephone'] = $val['telephone'];
        }
        if ($val['service'] == "") {
            $this->valF['service'] = NULL;
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['telephone_pro'] == "") {
            $this->valF['telephone_pro'] = NULL;
        } else {
            $this->valF['telephone_pro'] = $val['telephone_pro'];
        }
        if ($val['grade'] == "") {
            $this->valF['grade'] = NULL;
        } else {
            $this->valF['grade'] = $val['grade'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            $form->setType("adresse", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("telephone", "text");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("telephone_pro", "text");
            if ($this->is_in_context_of_foreign_key("grade", $this->retourformulaire)) {
                $form->setType("grade", "selecthiddenstatic");
            } else {
                $form->setType("grade", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            $form->setType("adresse", "text");
            $form->setType("cp", "text");
            $form->setType("ville", "text");
            $form->setType("telephone", "text");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("telephone_pro", "text");
            if ($this->is_in_context_of_foreign_key("grade", $this->retourformulaire)) {
                $form->setType("grade", "selecthiddenstatic");
            } else {
                $form->setType("grade", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("prenom", "hiddenstatic");
            $form->setType("adresse", "hiddenstatic");
            $form->setType("cp", "hiddenstatic");
            $form->setType("ville", "hiddenstatic");
            $form->setType("telephone", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("telephone_pro", "hiddenstatic");
            $form->setType("grade", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("nom", "static");
            $form->setType("prenom", "static");
            $form->setType("adresse", "static");
            $form->setType("cp", "static");
            $form->setType("ville", "static");
            $form->setType("telephone", "static");
            $form->setType("service", "selectstatic");
            $form->setType("telephone_pro", "static");
            $form->setType("grade", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("nom", 30);
        $form->setTaille("prenom", 30);
        $form->setTaille("adresse", 30);
        $form->setTaille("cp", 10);
        $form->setTaille("ville", 30);
        $form->setTaille("telephone", 14);
        $form->setTaille("service", 10);
        $form->setTaille("telephone_pro", 14);
        $form->setTaille("grade", 10);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("nom", 40);
        $form->setMax("prenom", 40);
        $form->setMax("adresse", 80);
        $form->setMax("cp", 5);
        $form->setMax("ville", 40);
        $form->setMax("telephone", 14);
        $form->setMax("service", 10);
        $form->setMax("telephone_pro", 14);
        $form->setMax("grade", 10);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('nom', __('nom'));
        $form->setLib('prenom', __('prenom'));
        $form->setLib('adresse', __('adresse'));
        $form->setLib('cp', __('cp'));
        $form->setLib('ville', __('ville'));
        $form->setLib('telephone', __('telephone'));
        $form->setLib('service', __('service'));
        $form->setLib('telephone_pro', __('telephone_pro'));
        $form->setLib('grade', __('grade'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // grade
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "grade",
            $this->get_var_sql_forminc__sql("grade"),
            $this->get_var_sql_forminc__sql("grade_by_id"),
            false
        );
        // service
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "service",
            $this->get_var_sql_forminc__sql("service"),
            $this->get_var_sql_forminc__sql("service_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('grade', $this->retourformulaire))
                $form->setVal('grade', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : candidature
        $this->rechercheTable($this->f->db, "candidature", "agent", $id);
    }


}
