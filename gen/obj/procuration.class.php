<?php
//$Id$ 
//gen openMairie le 17/03/2022 09:34

require_once "../obj/om_dbform.class.php";

class procuration_gen extends om_dbform {

    protected $_absolute_class_name = "procuration";

    var $table = "procuration";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "date_accord",
        "debut_validite",
        "fin_validite",
        "id",
        "mandant_ine",
        "mandataire_ine",
        "om_collectivite",
        "statut"
    );
    
    var $foreign_keys_extended = array(
        "electeur" => array("electeur", ),
        "om_collectivite" => array("om_collectivite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("mandant");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "mandant",
            "mandataire",
            "debut_validite",
            "fin_validite",
            "date_accord",
            "motif_refus",
            "id_externe",
            "statut",
            "mandant_ine",
            "mandataire_ine",
            "mandant_infos",
            "mandataire_infos",
            "mandant_resume",
            "mandataire_resume",
            "om_collectivite",
            "vu",
            "historique",
            "autorite_nom_prenom",
            "autorite_commune",
            "autorite_consulat",
            "autorite_type",
            "notes",
            "annulation_autorite_nom_prenom",
            "annulation_autorite_commune",
            "annulation_autorite_consulat",
            "annulation_autorite_type",
            "annulation_date",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mandant() {
        return "SELECT electeur.id, electeur.liste FROM ".DB_PREFIXE."electeur ORDER BY electeur.liste ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mandant_by_id() {
        return "SELECT electeur.id, electeur.liste FROM ".DB_PREFIXE."electeur WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mandataire() {
        return "SELECT electeur.id, electeur.liste FROM ".DB_PREFIXE."electeur ORDER BY electeur.liste ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mandataire_by_id() {
        return "SELECT electeur.id, electeur.liste FROM ".DB_PREFIXE."electeur WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_collectivite_by_id() {
        return "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if (!is_numeric($val['mandant'])) {
            $this->valF['mandant'] = NULL;
        } else {
            $this->valF['mandant'] = $val['mandant'];
        }
        if (!is_numeric($val['mandataire'])) {
            $this->valF['mandataire'] = NULL;
        } else {
            $this->valF['mandataire'] = $val['mandataire'];
        }
        if ($val['debut_validite'] != "") {
            $this->valF['debut_validite'] = $this->dateDB($val['debut_validite']);
        }
        if ($val['fin_validite'] != "") {
            $this->valF['fin_validite'] = $this->dateDB($val['fin_validite']);
        }
        if ($val['date_accord'] != "") {
            $this->valF['date_accord'] = $this->dateDB($val['date_accord']);
        }
        if ($val['motif_refus'] == "") {
            $this->valF['motif_refus'] = NULL;
        } else {
            $this->valF['motif_refus'] = $val['motif_refus'];
        }
        if ($val['id_externe'] == "") {
            $this->valF['id_externe'] = NULL;
        } else {
            $this->valF['id_externe'] = $val['id_externe'];
        }
        $this->valF['statut'] = $val['statut'];
        if (!is_numeric($val['mandant_ine'])) {
            $this->valF['mandant_ine'] = ""; // -> requis
        } else {
            $this->valF['mandant_ine'] = $val['mandant_ine'];
        }
        if (!is_numeric($val['mandataire_ine'])) {
            $this->valF['mandataire_ine'] = ""; // -> requis
        } else {
            $this->valF['mandataire_ine'] = $val['mandataire_ine'];
        }
            $this->valF['mandant_infos'] = $val['mandant_infos'];
            $this->valF['mandataire_infos'] = $val['mandataire_infos'];
        if ($val['mandant_resume'] == "") {
            $this->valF['mandant_resume'] = NULL;
        } else {
            $this->valF['mandant_resume'] = $val['mandant_resume'];
        }
        if ($val['mandataire_resume'] == "") {
            $this->valF['mandataire_resume'] = NULL;
        } else {
            $this->valF['mandataire_resume'] = $val['mandataire_resume'];
        }
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        if ($val['vu'] == 1 || $val['vu'] == "t" || $val['vu'] == "Oui") {
            $this->valF['vu'] = true;
        } else {
            $this->valF['vu'] = false;
        }
            $this->valF['historique'] = $val['historique'];
        if ($val['autorite_nom_prenom'] == "") {
            $this->valF['autorite_nom_prenom'] = NULL;
        } else {
            $this->valF['autorite_nom_prenom'] = $val['autorite_nom_prenom'];
        }
        if ($val['autorite_commune'] == "") {
            $this->valF['autorite_commune'] = NULL;
        } else {
            $this->valF['autorite_commune'] = $val['autorite_commune'];
        }
        if ($val['autorite_consulat'] == "") {
            $this->valF['autorite_consulat'] = NULL;
        } else {
            $this->valF['autorite_consulat'] = $val['autorite_consulat'];
        }
        if ($val['autorite_type'] == "") {
            $this->valF['autorite_type'] = NULL;
        } else {
            $this->valF['autorite_type'] = $val['autorite_type'];
        }
            $this->valF['notes'] = $val['notes'];
        if ($val['annulation_autorite_nom_prenom'] == "") {
            $this->valF['annulation_autorite_nom_prenom'] = NULL;
        } else {
            $this->valF['annulation_autorite_nom_prenom'] = $val['annulation_autorite_nom_prenom'];
        }
        if ($val['annulation_autorite_commune'] == "") {
            $this->valF['annulation_autorite_commune'] = NULL;
        } else {
            $this->valF['annulation_autorite_commune'] = $val['annulation_autorite_commune'];
        }
        if ($val['annulation_autorite_consulat'] == "") {
            $this->valF['annulation_autorite_consulat'] = NULL;
        } else {
            $this->valF['annulation_autorite_consulat'] = $val['annulation_autorite_consulat'];
        }
        if ($val['annulation_autorite_type'] == "") {
            $this->valF['annulation_autorite_type'] = NULL;
        } else {
            $this->valF['annulation_autorite_type'] = $val['annulation_autorite_type'];
        }
        if ($val['annulation_date'] != "") {
            $this->valF['annulation_date'] = $this->dateDB($val['annulation_date']);
        } else {
            $this->valF['annulation_date'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            if ($this->is_in_context_of_foreign_key("electeur", $this->retourformulaire)) {
                $form->setType("mandant", "selecthiddenstatic");
            } else {
                $form->setType("mandant", "select");
            }
            if ($this->is_in_context_of_foreign_key("electeur", $this->retourformulaire)) {
                $form->setType("mandataire", "selecthiddenstatic");
            } else {
                $form->setType("mandataire", "select");
            }
            $form->setType("debut_validite", "date");
            $form->setType("fin_validite", "date");
            $form->setType("date_accord", "date");
            $form->setType("motif_refus", "text");
            $form->setType("id_externe", "text");
            $form->setType("statut", "text");
            $form->setType("mandant_ine", "text");
            $form->setType("mandataire_ine", "text");
            $form->setType("mandant_infos", "textarea");
            $form->setType("mandataire_infos", "textarea");
            $form->setType("mandant_resume", "text");
            $form->setType("mandataire_resume", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("vu", "checkbox");
            $form->setType("historique", "textarea");
            $form->setType("autorite_nom_prenom", "text");
            $form->setType("autorite_commune", "text");
            $form->setType("autorite_consulat", "text");
            $form->setType("autorite_type", "text");
            $form->setType("notes", "textarea");
            $form->setType("annulation_autorite_nom_prenom", "text");
            $form->setType("annulation_autorite_commune", "text");
            $form->setType("annulation_autorite_consulat", "text");
            $form->setType("annulation_autorite_type", "text");
            $form->setType("annulation_date", "date");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("electeur", $this->retourformulaire)) {
                $form->setType("mandant", "selecthiddenstatic");
            } else {
                $form->setType("mandant", "select");
            }
            if ($this->is_in_context_of_foreign_key("electeur", $this->retourformulaire)) {
                $form->setType("mandataire", "selecthiddenstatic");
            } else {
                $form->setType("mandataire", "select");
            }
            $form->setType("debut_validite", "date");
            $form->setType("fin_validite", "date");
            $form->setType("date_accord", "date");
            $form->setType("motif_refus", "text");
            $form->setType("id_externe", "text");
            $form->setType("statut", "text");
            $form->setType("mandant_ine", "text");
            $form->setType("mandataire_ine", "text");
            $form->setType("mandant_infos", "textarea");
            $form->setType("mandataire_infos", "textarea");
            $form->setType("mandant_resume", "text");
            $form->setType("mandataire_resume", "text");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selecthiddenstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "select");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("vu", "checkbox");
            $form->setType("historique", "textarea");
            $form->setType("autorite_nom_prenom", "text");
            $form->setType("autorite_commune", "text");
            $form->setType("autorite_consulat", "text");
            $form->setType("autorite_type", "text");
            $form->setType("notes", "textarea");
            $form->setType("annulation_autorite_nom_prenom", "text");
            $form->setType("annulation_autorite_commune", "text");
            $form->setType("annulation_autorite_consulat", "text");
            $form->setType("annulation_autorite_type", "text");
            $form->setType("annulation_date", "date");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("mandant", "selectstatic");
            $form->setType("mandataire", "selectstatic");
            $form->setType("debut_validite", "hiddenstatic");
            $form->setType("fin_validite", "hiddenstatic");
            $form->setType("date_accord", "hiddenstatic");
            $form->setType("motif_refus", "hiddenstatic");
            $form->setType("id_externe", "hiddenstatic");
            $form->setType("statut", "hiddenstatic");
            $form->setType("mandant_ine", "hiddenstatic");
            $form->setType("mandataire_ine", "hiddenstatic");
            $form->setType("mandant_infos", "hiddenstatic");
            $form->setType("mandataire_infos", "hiddenstatic");
            $form->setType("mandant_resume", "hiddenstatic");
            $form->setType("mandataire_resume", "hiddenstatic");
            if ($_SESSION["niveau"] == 2) {
                $form->setType("om_collectivite", "selectstatic");
            } else {
                $form->setType("om_collectivite", "hidden");
            }
            $form->setType("vu", "hiddenstatic");
            $form->setType("historique", "hiddenstatic");
            $form->setType("autorite_nom_prenom", "hiddenstatic");
            $form->setType("autorite_commune", "hiddenstatic");
            $form->setType("autorite_consulat", "hiddenstatic");
            $form->setType("autorite_type", "hiddenstatic");
            $form->setType("notes", "hiddenstatic");
            $form->setType("annulation_autorite_nom_prenom", "hiddenstatic");
            $form->setType("annulation_autorite_commune", "hiddenstatic");
            $form->setType("annulation_autorite_consulat", "hiddenstatic");
            $form->setType("annulation_autorite_type", "hiddenstatic");
            $form->setType("annulation_date", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("mandant", "selectstatic");
            $form->setType("mandataire", "selectstatic");
            $form->setType("debut_validite", "datestatic");
            $form->setType("fin_validite", "datestatic");
            $form->setType("date_accord", "datestatic");
            $form->setType("motif_refus", "static");
            $form->setType("id_externe", "static");
            $form->setType("statut", "static");
            $form->setType("mandant_ine", "static");
            $form->setType("mandataire_ine", "static");
            $form->setType("mandant_infos", "textareastatic");
            $form->setType("mandataire_infos", "textareastatic");
            $form->setType("mandant_resume", "static");
            $form->setType("mandataire_resume", "static");
            if ($this->is_in_context_of_foreign_key("om_collectivite", $this->retourformulaire)) {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            } else {
                if($_SESSION["niveau"] == 2) {
                    $form->setType("om_collectivite", "selectstatic");
                } else {
                    $form->setType("om_collectivite", "hidden");
                }
            }
            $form->setType("vu", "checkboxstatic");
            $form->setType("historique", "textareastatic");
            $form->setType("autorite_nom_prenom", "static");
            $form->setType("autorite_commune", "static");
            $form->setType("autorite_consulat", "static");
            $form->setType("autorite_type", "static");
            $form->setType("notes", "textareastatic");
            $form->setType("annulation_autorite_nom_prenom", "static");
            $form->setType("annulation_autorite_commune", "static");
            $form->setType("annulation_autorite_consulat", "static");
            $form->setType("annulation_autorite_type", "static");
            $form->setType("annulation_date", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('mandant','VerifNum(this)');
        $form->setOnchange('mandataire','VerifNum(this)');
        $form->setOnchange('debut_validite','fdate(this)');
        $form->setOnchange('fin_validite','fdate(this)');
        $form->setOnchange('date_accord','fdate(this)');
        $form->setOnchange('mandant_ine','VerifNum(this)');
        $form->setOnchange('mandataire_ine','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
        $form->setOnchange('annulation_date','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("mandant", 11);
        $form->setTaille("mandataire", 11);
        $form->setTaille("debut_validite", 12);
        $form->setTaille("fin_validite", 12);
        $form->setTaille("date_accord", 12);
        $form->setTaille("motif_refus", 30);
        $form->setTaille("id_externe", 20);
        $form->setTaille("statut", 30);
        $form->setTaille("mandant_ine", 11);
        $form->setTaille("mandataire_ine", 11);
        $form->setTaille("mandant_infos", 80);
        $form->setTaille("mandataire_infos", 80);
        $form->setTaille("mandant_resume", 30);
        $form->setTaille("mandataire_resume", 30);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("vu", 1);
        $form->setTaille("historique", 80);
        $form->setTaille("autorite_nom_prenom", 30);
        $form->setTaille("autorite_commune", 30);
        $form->setTaille("autorite_consulat", 30);
        $form->setTaille("autorite_type", 30);
        $form->setTaille("notes", 80);
        $form->setTaille("annulation_autorite_nom_prenom", 30);
        $form->setTaille("annulation_autorite_commune", 30);
        $form->setTaille("annulation_autorite_consulat", 30);
        $form->setTaille("annulation_autorite_type", 30);
        $form->setTaille("annulation_date", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("mandant", 11);
        $form->setMax("mandataire", 11);
        $form->setMax("debut_validite", 12);
        $form->setMax("fin_validite", 12);
        $form->setMax("date_accord", 12);
        $form->setMax("motif_refus", 250);
        $form->setMax("id_externe", 20);
        $form->setMax("statut", 50);
        $form->setMax("mandant_ine", 11);
        $form->setMax("mandataire_ine", 11);
        $form->setMax("mandant_infos", 6);
        $form->setMax("mandataire_infos", 6);
        $form->setMax("mandant_resume", 250);
        $form->setMax("mandataire_resume", 250);
        $form->setMax("om_collectivite", 11);
        $form->setMax("vu", 1);
        $form->setMax("historique", 6);
        $form->setMax("autorite_nom_prenom", 250);
        $form->setMax("autorite_commune", 250);
        $form->setMax("autorite_consulat", 250);
        $form->setMax("autorite_type", 250);
        $form->setMax("notes", 6);
        $form->setMax("annulation_autorite_nom_prenom", 250);
        $form->setMax("annulation_autorite_commune", 250);
        $form->setMax("annulation_autorite_consulat", 250);
        $form->setMax("annulation_autorite_type", 250);
        $form->setMax("annulation_date", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('mandant', __('mandant'));
        $form->setLib('mandataire', __('mandataire'));
        $form->setLib('debut_validite', __('debut_validite'));
        $form->setLib('fin_validite', __('fin_validite'));
        $form->setLib('date_accord', __('date_accord'));
        $form->setLib('motif_refus', __('motif_refus'));
        $form->setLib('id_externe', __('id_externe'));
        $form->setLib('statut', __('statut'));
        $form->setLib('mandant_ine', __('mandant_ine'));
        $form->setLib('mandataire_ine', __('mandataire_ine'));
        $form->setLib('mandant_infos', __('mandant_infos'));
        $form->setLib('mandataire_infos', __('mandataire_infos'));
        $form->setLib('mandant_resume', __('mandant_resume'));
        $form->setLib('mandataire_resume', __('mandataire_resume'));
        $form->setLib('om_collectivite', __('om_collectivite'));
        $form->setLib('vu', __('vu'));
        $form->setLib('historique', __('historique'));
        $form->setLib('autorite_nom_prenom', __('autorite_nom_prenom'));
        $form->setLib('autorite_commune', __('autorite_commune'));
        $form->setLib('autorite_consulat', __('autorite_consulat'));
        $form->setLib('autorite_type', __('autorite_type'));
        $form->setLib('notes', __('notes'));
        $form->setLib('annulation_autorite_nom_prenom', __('annulation_autorite_nom_prenom'));
        $form->setLib('annulation_autorite_commune', __('annulation_autorite_commune'));
        $form->setLib('annulation_autorite_consulat', __('annulation_autorite_consulat'));
        $form->setLib('annulation_autorite_type', __('annulation_autorite_type'));
        $form->setLib('annulation_date', __('annulation_date'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // mandant
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "mandant",
            $this->get_var_sql_forminc__sql("mandant"),
            $this->get_var_sql_forminc__sql("mandant_by_id"),
            false
        );
        // mandataire
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "mandataire",
            $this->get_var_sql_forminc__sql("mandataire"),
            $this->get_var_sql_forminc__sql("mandataire_by_id"),
            false
        );
        // om_collectivite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "om_collectivite",
            $this->get_var_sql_forminc__sql("om_collectivite"),
            $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
            false
        );
    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('om_collectivite', $this->retourformulaire))
                $form->setVal('om_collectivite', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('electeur', $this->retourformulaire))
                $form->setVal('mandant', $idxformulaire);
            if($this->is_in_context_of_foreign_key('electeur', $this->retourformulaire))
                $form->setVal('mandataire', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
