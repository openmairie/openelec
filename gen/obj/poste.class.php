<?php
//$Id$ 
//gen openMairie le 26/12/2019 10:59

require_once "../obj/om_dbform.class.php";

class poste_gen extends om_dbform {

    protected $_absolute_class_name = "poste";

    var $table = "poste";
    var $clePrimaire = "poste";
    var $typeCle = "A";
    var $required_field = array(
        "libelle",
        "nature",
        "ordre",
        "poste"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "poste",
            "libelle",
            "nature",
            "ordre",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['poste'] = $val['poste'];
        $this->valF['libelle'] = $val['libelle'];
        $this->valF['nature'] = $val['nature'];
        if (!is_numeric($val['ordre'])) {
            $this->valF['ordre'] = ""; // -> requis
        } else {
            $this->valF['ordre'] = $val['ordre'];
        }
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("poste", "text");
            $form->setType("libelle", "text");
            $form->setType("nature", "text");
            $form->setType("ordre", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("poste", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("nature", "text");
            $form->setType("ordre", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("poste", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("nature", "hiddenstatic");
            $form->setType("ordre", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("poste", "static");
            $form->setType("libelle", "static");
            $form->setType("nature", "static");
            $form->setType("ordre", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('ordre','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("poste", 20);
        $form->setTaille("libelle", 20);
        $form->setTaille("nature", 15);
        $form->setTaille("ordre", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("poste", 20);
        $form->setMax("libelle", 20);
        $form->setMax("nature", 15);
        $form->setMax("ordre", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('poste', __('poste'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('nature', __('nature'));
        $form->setLib('ordre', __('ordre'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : affectation
        $this->rechercheTable($this->f->db, "affectation", "poste", $id);
        // Verification de la cle secondaire : candidature
        $this->rechercheTable($this->f->db, "candidature", "poste", $id);
    }


}
