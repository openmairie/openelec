<?php
//$Id$ 
//gen openMairie le 15/02/2022 09:09

require_once "../obj/om_dbform.class.php";

class affectation_gen extends om_dbform {

    protected $_absolute_class_name = "affectation";

    var $table = "affectation";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "bureau",
        "composition_scrutin",
        "elu",
        "id",
        "periode",
        "poste"
    );
    
    var $foreign_keys_extended = array(
        "bureau" => array("bureau", ),
        "candidat" => array("candidat", ),
        "composition_scrutin" => array("composition_scrutin", ),
        "elu" => array("elu", ),
        "periode" => array("periode", ),
        "poste" => array("poste", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("elu");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "elu",
            "composition_scrutin",
            "periode",
            "poste",
            "bureau",
            "note",
            "decision",
            "candidat",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau ORDER BY bureau.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return "SELECT bureau.id, bureau.libelle FROM ".DB_PREFIXE."bureau WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_candidat() {
        return "SELECT candidat.id, candidat.nom FROM ".DB_PREFIXE."candidat ORDER BY candidat.nom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_candidat_by_id() {
        return "SELECT candidat.id, candidat.nom FROM ".DB_PREFIXE."candidat WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_composition_scrutin() {
        return "SELECT composition_scrutin.id, composition_scrutin.libelle FROM ".DB_PREFIXE."composition_scrutin ORDER BY composition_scrutin.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_composition_scrutin_by_id() {
        return "SELECT composition_scrutin.id, composition_scrutin.libelle FROM ".DB_PREFIXE."composition_scrutin WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_elu() {
        return "SELECT elu.id, elu.nom FROM ".DB_PREFIXE."elu ORDER BY elu.nom ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_elu_by_id() {
        return "SELECT elu.id, elu.nom FROM ".DB_PREFIXE."elu WHERE id = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_periode() {
        return "SELECT periode.periode, periode.libelle FROM ".DB_PREFIXE."periode ORDER BY periode.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_periode_by_id() {
        return "SELECT periode.periode, periode.libelle FROM ".DB_PREFIXE."periode WHERE periode = '<idx>'";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_poste() {
        return "SELECT poste.poste, poste.libelle FROM ".DB_PREFIXE."poste ORDER BY poste.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_poste_by_id() {
        return "SELECT poste.poste, poste.libelle FROM ".DB_PREFIXE."poste WHERE poste = '<idx>'";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if (!is_numeric($val['elu'])) {
            $this->valF['elu'] = ""; // -> requis
        } else {
            $this->valF['elu'] = $val['elu'];
        }
        if (!is_numeric($val['composition_scrutin'])) {
            $this->valF['composition_scrutin'] = ""; // -> requis
        } else {
            $this->valF['composition_scrutin'] = $val['composition_scrutin'];
        }
        $this->valF['periode'] = $val['periode'];
        $this->valF['poste'] = $val['poste'];
        if (!is_numeric($val['bureau'])) {
            $this->valF['bureau'] = ""; // -> requis
        } else {
            $this->valF['bureau'] = $val['bureau'];
        }
            $this->valF['note'] = $val['note'];
        if ($val['decision'] == 1 || $val['decision'] == "t" || $val['decision'] == "Oui") {
            $this->valF['decision'] = true;
        } else {
            $this->valF['decision'] = false;
        }
        if (!is_numeric($val['candidat'])) {
            $this->valF['candidat'] = NULL;
        } else {
            $this->valF['candidat'] = $val['candidat'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            if ($this->is_in_context_of_foreign_key("elu", $this->retourformulaire)) {
                $form->setType("elu", "selecthiddenstatic");
            } else {
                $form->setType("elu", "select");
            }
            if ($this->is_in_context_of_foreign_key("composition_scrutin", $this->retourformulaire)) {
                $form->setType("composition_scrutin", "selecthiddenstatic");
            } else {
                $form->setType("composition_scrutin", "select");
            }
            if ($this->is_in_context_of_foreign_key("periode", $this->retourformulaire)) {
                $form->setType("periode", "selecthiddenstatic");
            } else {
                $form->setType("periode", "select");
            }
            if ($this->is_in_context_of_foreign_key("poste", $this->retourformulaire)) {
                $form->setType("poste", "selecthiddenstatic");
            } else {
                $form->setType("poste", "select");
            }
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            $form->setType("note", "textarea");
            $form->setType("decision", "checkbox");
            if ($this->is_in_context_of_foreign_key("candidat", $this->retourformulaire)) {
                $form->setType("candidat", "selecthiddenstatic");
            } else {
                $form->setType("candidat", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("elu", $this->retourformulaire)) {
                $form->setType("elu", "selecthiddenstatic");
            } else {
                $form->setType("elu", "select");
            }
            if ($this->is_in_context_of_foreign_key("composition_scrutin", $this->retourformulaire)) {
                $form->setType("composition_scrutin", "selecthiddenstatic");
            } else {
                $form->setType("composition_scrutin", "select");
            }
            if ($this->is_in_context_of_foreign_key("periode", $this->retourformulaire)) {
                $form->setType("periode", "selecthiddenstatic");
            } else {
                $form->setType("periode", "select");
            }
            if ($this->is_in_context_of_foreign_key("poste", $this->retourformulaire)) {
                $form->setType("poste", "selecthiddenstatic");
            } else {
                $form->setType("poste", "select");
            }
            if ($this->is_in_context_of_foreign_key("bureau", $this->retourformulaire)) {
                $form->setType("bureau", "selecthiddenstatic");
            } else {
                $form->setType("bureau", "select");
            }
            $form->setType("note", "textarea");
            $form->setType("decision", "checkbox");
            if ($this->is_in_context_of_foreign_key("candidat", $this->retourformulaire)) {
                $form->setType("candidat", "selecthiddenstatic");
            } else {
                $form->setType("candidat", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("elu", "selectstatic");
            $form->setType("composition_scrutin", "selectstatic");
            $form->setType("periode", "selectstatic");
            $form->setType("poste", "selectstatic");
            $form->setType("bureau", "selectstatic");
            $form->setType("note", "hiddenstatic");
            $form->setType("decision", "hiddenstatic");
            $form->setType("candidat", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("elu", "selectstatic");
            $form->setType("composition_scrutin", "selectstatic");
            $form->setType("periode", "selectstatic");
            $form->setType("poste", "selectstatic");
            $form->setType("bureau", "selectstatic");
            $form->setType("note", "textareastatic");
            $form->setType("decision", "checkboxstatic");
            $form->setType("candidat", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('elu','VerifNum(this)');
        $form->setOnchange('composition_scrutin','VerifNum(this)');
        $form->setOnchange('bureau','VerifNum(this)');
        $form->setOnchange('candidat','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("elu", 11);
        $form->setTaille("composition_scrutin", 11);
        $form->setTaille("periode", 20);
        $form->setTaille("poste", 20);
        $form->setTaille("bureau", 11);
        $form->setTaille("note", 80);
        $form->setTaille("decision", 1);
        $form->setTaille("candidat", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("elu", 11);
        $form->setMax("composition_scrutin", 11);
        $form->setMax("periode", 20);
        $form->setMax("poste", 20);
        $form->setMax("bureau", 11);
        $form->setMax("note", 6);
        $form->setMax("decision", 1);
        $form->setMax("candidat", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('elu', __('elu'));
        $form->setLib('composition_scrutin', __('composition_scrutin'));
        $form->setLib('periode', __('periode'));
        $form->setLib('poste', __('poste'));
        $form->setLib('bureau', __('bureau'));
        $form->setLib('note', __('note'));
        $form->setLib('decision', __('decision'));
        $form->setLib('candidat', __('candidat'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // bureau
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "bureau",
            $this->get_var_sql_forminc__sql("bureau"),
            $this->get_var_sql_forminc__sql("bureau_by_id"),
            false
        );
        // candidat
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "candidat",
            $this->get_var_sql_forminc__sql("candidat"),
            $this->get_var_sql_forminc__sql("candidat_by_id"),
            false
        );
        // composition_scrutin
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "composition_scrutin",
            $this->get_var_sql_forminc__sql("composition_scrutin"),
            $this->get_var_sql_forminc__sql("composition_scrutin_by_id"),
            false
        );
        // elu
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "elu",
            $this->get_var_sql_forminc__sql("elu"),
            $this->get_var_sql_forminc__sql("elu_by_id"),
            false
        );
        // periode
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "periode",
            $this->get_var_sql_forminc__sql("periode"),
            $this->get_var_sql_forminc__sql("periode_by_id"),
            false
        );
        // poste
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "poste",
            $this->get_var_sql_forminc__sql("poste"),
            $this->get_var_sql_forminc__sql("poste_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('bureau', $this->retourformulaire))
                $form->setVal('bureau', $idxformulaire);
            if($this->is_in_context_of_foreign_key('candidat', $this->retourformulaire))
                $form->setVal('candidat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('composition_scrutin', $this->retourformulaire))
                $form->setVal('composition_scrutin', $idxformulaire);
            if($this->is_in_context_of_foreign_key('elu', $this->retourformulaire))
                $form->setVal('elu', $idxformulaire);
            if($this->is_in_context_of_foreign_key('periode', $this->retourformulaire))
                $form->setVal('periode', $idxformulaire);
            if($this->is_in_context_of_foreign_key('poste', $this->retourformulaire))
                $form->setVal('poste', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
