<?php
//$Id$ 
//gen openMairie le 26/12/2019 10:59

require_once "../obj/om_dbform.class.php";

class periode_gen extends om_dbform {

    protected $_absolute_class_name = "periode";

    var $table = "periode";
    var $clePrimaire = "periode";
    var $typeCle = "A";
    var $required_field = array(
        "debut",
        "fin",
        "libelle",
        "periode"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "periode",
            "libelle",
            "debut",
            "fin",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        $this->valF['periode'] = $val['periode'];
        $this->valF['libelle'] = $val['libelle'];
            $this->valF['debut'] = $val['debut'];
            $this->valF['fin'] = $val['fin'];
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("periode", "text");
            $form->setType("libelle", "text");
            $form->setType("debut", "text");
            $form->setType("fin", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("periode", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("debut", "text");
            $form->setType("fin", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("periode", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("debut", "hiddenstatic");
            $form->setType("fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("periode", "static");
            $form->setType("libelle", "static");
            $form->setType("debut", "static");
            $form->setType("fin", "static");
        }

    }

    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("periode", 20);
        $form->setTaille("libelle", 30);
        $form->setTaille("debut", 8);
        $form->setTaille("fin", 8);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("periode", 20);
        $form->setMax("libelle", 40);
        $form->setMax("debut", 8);
        $form->setMax("fin", 8);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('periode', __('periode'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('debut', __('debut'));
        $form->setLib('fin', __('fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : affectation
        $this->rechercheTable($this->f->db, "affectation", "periode", $id);
        // Verification de la cle secondaire : candidature
        $this->rechercheTable($this->f->db, "candidature", "periode", $id);
    }


}
