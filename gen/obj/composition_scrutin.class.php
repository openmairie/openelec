<?php
//$Id$ 
//gen openMairie le 18/02/2022 03:08

require_once "../obj/om_dbform.class.php";

class composition_scrutin_gen extends om_dbform {

    protected $_absolute_class_name = "composition_scrutin";

    var $table = "composition_scrutin";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "date_tour",
        "id",
        "libelle",
        "scrutin",
        "tour"
    );
    
    var $foreign_keys_extended = array(
        "reu_scrutin" => array("reu_scrutin", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "scrutin",
            "libelle",
            "tour",
            "date_tour",
            "solde",
            "convocation_agent",
            "convocation_president",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_scrutin() {
        return "SELECT reu_scrutin.id, reu_scrutin.libelle FROM ".DB_PREFIXE."reu_scrutin ORDER BY reu_scrutin.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_scrutin_by_id() {
        return "SELECT reu_scrutin.id, reu_scrutin.libelle FROM ".DB_PREFIXE."reu_scrutin WHERE id = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
        if (!is_numeric($val['scrutin'])) {
            $this->valF['scrutin'] = ""; // -> requis
        } else {
            $this->valF['scrutin'] = $val['scrutin'];
        }
        $this->valF['libelle'] = $val['libelle'];
        $this->valF['tour'] = $val['tour'];
        if ($val['date_tour'] != "") {
            $this->valF['date_tour'] = $this->dateDB($val['date_tour']);
        }
        if ($val['solde'] == 1 || $val['solde'] == "t" || $val['solde'] == "Oui") {
            $this->valF['solde'] = true;
        } else {
            $this->valF['solde'] = false;
        }
        if ($val['convocation_agent'] == "") {
            $this->valF['convocation_agent'] = NULL;
        } else {
            $this->valF['convocation_agent'] = $val['convocation_agent'];
        }
        if ($val['convocation_president'] == "") {
            $this->valF['convocation_president'] = NULL;
        } else {
            $this->valF['convocation_president'] = $val['convocation_president'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            if ($this->is_in_context_of_foreign_key("reu_scrutin", $this->retourformulaire)) {
                $form->setType("scrutin", "selecthiddenstatic");
            } else {
                $form->setType("scrutin", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("tour", "text");
            $form->setType("date_tour", "date");
            $form->setType("solde", "checkbox");
            $form->setType("convocation_agent", "text");
            $form->setType("convocation_president", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("reu_scrutin", $this->retourformulaire)) {
                $form->setType("scrutin", "selecthiddenstatic");
            } else {
                $form->setType("scrutin", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("tour", "text");
            $form->setType("date_tour", "date");
            $form->setType("solde", "checkbox");
            $form->setType("convocation_agent", "text");
            $form->setType("convocation_president", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("scrutin", "selectstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("tour", "hiddenstatic");
            $form->setType("date_tour", "hiddenstatic");
            $form->setType("solde", "hiddenstatic");
            $form->setType("convocation_agent", "hiddenstatic");
            $form->setType("convocation_president", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("scrutin", "selectstatic");
            $form->setType("libelle", "static");
            $form->setType("tour", "static");
            $form->setType("date_tour", "datestatic");
            $form->setType("solde", "checkboxstatic");
            $form->setType("convocation_agent", "static");
            $form->setType("convocation_president", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('scrutin','VerifNum(this)');
        $form->setOnchange('date_tour','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("scrutin", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("tour", 10);
        $form->setTaille("date_tour", 12);
        $form->setTaille("solde", 1);
        $form->setTaille("convocation_agent", 30);
        $form->setTaille("convocation_president", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("scrutin", 11);
        $form->setMax("libelle", 250);
        $form->setMax("tour", 1);
        $form->setMax("date_tour", 12);
        $form->setMax("solde", 1);
        $form->setMax("convocation_agent", 250);
        $form->setMax("convocation_president", 250);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('scrutin', __('scrutin'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('tour', __('tour'));
        $form->setLib('date_tour', __('date_tour'));
        $form->setLib('solde', __('solde'));
        $form->setLib('convocation_agent', __('convocation_agent'));
        $form->setLib('convocation_president', __('convocation_president'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // scrutin
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "scrutin",
            $this->get_var_sql_forminc__sql("scrutin"),
            $this->get_var_sql_forminc__sql("scrutin_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('reu_scrutin', $this->retourformulaire))
                $form->setVal('scrutin', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : affectation
        $this->rechercheTable($this->f->db, "affectation", "composition_scrutin", $id);
        // Verification de la cle secondaire : candidat
        $this->rechercheTable($this->f->db, "candidat", "composition_scrutin", $id);
        // Verification de la cle secondaire : candidature
        $this->rechercheTable($this->f->db, "candidature", "composition_scrutin", $id);
        // Verification de la cle secondaire : composition_bureau
        $this->rechercheTable($this->f->db, "composition_bureau", "composition_scrutin", $id);
    }


}
