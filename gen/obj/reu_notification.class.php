<?php
//$Id$ 
//gen openMairie le 18/02/2019 08:12

require_once "../obj/om_dbform.class.php";

class reu_notification_gen extends om_dbform {

    protected $_absolute_class_name = "reu_notification";

    var $table = "reu_notification";
    var $clePrimaire = "id";
    var $typeCle = "N";
    var $required_field = array(
        "id",
        "om_collectivite"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "date_de_creation",
            "libelle",
            "id_demande",
            "id_electeur",
            "lu",
            "lien_uri",
            "resultat_de_notification",
            "type_de_notification",
            "date_de_collecte",
            "om_collectivite",
            "traitee",
            "rapport_traitement",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['id'])) {
            $this->valF['id'] = ""; // -> requis
        } else {
            $this->valF['id'] = $val['id'];
        }
            $this->valF['date_de_creation'] = $val['date_de_creation'];
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
        if ($val['id_demande'] == "") {
            $this->valF['id_demande'] = NULL;
        } else {
            $this->valF['id_demande'] = $val['id_demande'];
        }
        if (!is_numeric($val['id_electeur'])) {
            $this->valF['id_electeur'] = NULL;
        } else {
            $this->valF['id_electeur'] = $val['id_electeur'];
        }
        if ($val['lu'] == 1 || $val['lu'] == "t" || $val['lu'] == "Oui") {
            $this->valF['lu'] = true;
        } else {
            $this->valF['lu'] = false;
        }
        if ($val['lien_uri'] == "") {
            $this->valF['lien_uri'] = NULL;
        } else {
            $this->valF['lien_uri'] = $val['lien_uri'];
        }
        if ($val['resultat_de_notification'] == "") {
            $this->valF['resultat_de_notification'] = NULL;
        } else {
            $this->valF['resultat_de_notification'] = $val['resultat_de_notification'];
        }
        if ($val['type_de_notification'] == "") {
            $this->valF['type_de_notification'] = NULL;
        } else {
            $this->valF['type_de_notification'] = $val['type_de_notification'];
        }
            $this->valF['date_de_collecte'] = $val['date_de_collecte'];
        if (!is_numeric($val['om_collectivite'])) {
            $this->valF['om_collectivite'] = ""; // -> requis
        } else {
            if($_SESSION['niveau']==1) {
                $this->valF['om_collectivite'] = $_SESSION['collectivite'];
            } else {
                $this->valF['om_collectivite'] = $val['om_collectivite'];
            }
        }
        if ($val['traitee'] == 1 || $val['traitee'] == "t" || $val['traitee'] == "Oui") {
            $this->valF['traitee'] = true;
        } else {
            $this->valF['traitee'] = false;
        }
            $this->valF['rapport_traitement'] = $val['rapport_traitement'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("id", "hidden");
            $form->setType("date_de_creation", "text");
            $form->setType("libelle", "text");
            $form->setType("id_demande", "text");
            $form->setType("id_electeur", "text");
            $form->setType("lu", "checkbox");
            $form->setType("lien_uri", "text");
            $form->setType("resultat_de_notification", "text");
            $form->setType("type_de_notification", "text");
            $form->setType("date_de_collecte", "text");
            $form->setType("om_collectivite", "text");
            $form->setType("traitee", "checkbox");
            $form->setType("rapport_traitement", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("id", "hiddenstatic");
            $form->setType("date_de_creation", "text");
            $form->setType("libelle", "text");
            $form->setType("id_demande", "text");
            $form->setType("id_electeur", "text");
            $form->setType("lu", "checkbox");
            $form->setType("lien_uri", "text");
            $form->setType("resultat_de_notification", "text");
            $form->setType("type_de_notification", "text");
            $form->setType("date_de_collecte", "text");
            $form->setType("om_collectivite", "text");
            $form->setType("traitee", "checkbox");
            $form->setType("rapport_traitement", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("id", "hiddenstatic");
            $form->setType("date_de_creation", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("id_demande", "hiddenstatic");
            $form->setType("id_electeur", "hiddenstatic");
            $form->setType("lu", "hiddenstatic");
            $form->setType("lien_uri", "hiddenstatic");
            $form->setType("resultat_de_notification", "hiddenstatic");
            $form->setType("type_de_notification", "hiddenstatic");
            $form->setType("date_de_collecte", "hiddenstatic");
            $form->setType("om_collectivite", "hiddenstatic");
            $form->setType("traitee", "hiddenstatic");
            $form->setType("rapport_traitement", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("id", "static");
            $form->setType("date_de_creation", "static");
            $form->setType("libelle", "static");
            $form->setType("id_demande", "static");
            $form->setType("id_electeur", "static");
            $form->setType("lu", "checkboxstatic");
            $form->setType("lien_uri", "static");
            $form->setType("resultat_de_notification", "static");
            $form->setType("type_de_notification", "static");
            $form->setType("date_de_collecte", "static");
            $form->setType("om_collectivite", "static");
            $form->setType("traitee", "checkboxstatic");
            $form->setType("rapport_traitement", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('id','VerifNum(this)');
        $form->setOnchange('id_electeur','VerifNum(this)');
        $form->setOnchange('om_collectivite','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("id", 11);
        $form->setTaille("date_de_creation", 8);
        $form->setTaille("libelle", 30);
        $form->setTaille("id_demande", 20);
        $form->setTaille("id_electeur", 11);
        $form->setTaille("lu", 1);
        $form->setTaille("lien_uri", 30);
        $form->setTaille("resultat_de_notification", 30);
        $form->setTaille("type_de_notification", 30);
        $form->setTaille("date_de_collecte", 8);
        $form->setTaille("om_collectivite", 11);
        $form->setTaille("traitee", 1);
        $form->setTaille("rapport_traitement", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("id", 11);
        $form->setMax("date_de_creation", 8);
        $form->setMax("libelle", 500);
        $form->setMax("id_demande", 20);
        $form->setMax("id_electeur", 11);
        $form->setMax("lu", 1);
        $form->setMax("lien_uri", 100);
        $form->setMax("resultat_de_notification", 250);
        $form->setMax("type_de_notification", 250);
        $form->setMax("date_de_collecte", 8);
        $form->setMax("om_collectivite", 11);
        $form->setMax("traitee", 1);
        $form->setMax("rapport_traitement", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('id', __('id'));
        $form->setLib('date_de_creation', __('date_de_creation'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('id_demande', __('id_demande'));
        $form->setLib('id_electeur', __('id_electeur'));
        $form->setLib('lu', __('lu'));
        $form->setLib('lien_uri', __('lien_uri'));
        $form->setLib('resultat_de_notification', __('resultat_de_notification'));
        $form->setLib('type_de_notification', __('type_de_notification'));
        $form->setLib('date_de_collecte', __('date_de_collecte'));
        $form->setLib('om_collectivite', __('om_collectivite'));
        $form->setLib('traitee', __('traitee'));
        $form->setLib('rapport_traitement', __('rapport_traitement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setVal

    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation==0 and $maj==0 and $_SESSION['niveau']==1) {
            $form->setVal('om_collectivite', $_SESSION['collectivite']);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
