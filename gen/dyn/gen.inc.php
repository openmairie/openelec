<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Surcharge applicative de la classe 'om_dbform'.
 */
$om_dbform_path_override = "../obj/om_dbform.class.php";
$om_dbform_class_override = "om_dbform";


$administration_parametrage = "Administration & Paramétrage";

$tables_to_avoid = array(
    'reu_sync_listes',
    'reu_referentiel_ugle',
    'reu_referentiel_commune',
    'reu_referentiel_pays',
    'reu_livrable',
    'reu_sync_procurations',
);

// Liste des chaînes de caractères à traduire
$strings_to_translate = array(
    __("piece"),
);

/**
 *
 */
$tables_to_overload = array(
    "agent" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "Agents",
        "breadcrumb_in_page_title" => array("Scrutins", "Composition des bureaux", ),
    ),
    "bureau" => array(
        //
        "displayed_fields_in_tableinc" => array(
            "id", "code", "libelle", "canton", "circonscription", "referentiel_id",
        ),
        "tablename_in_page_title" => "Bureaux",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Découpage", ),
    ),
    "canton" => array(
        //
        "tablename_in_page_title" => "Cantons",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Découpage", ),
    ),
    "circonscription" => array(
        //
        "tablename_in_page_title" => "Circonscriptions",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Découpage", ),
    ),
    "commune" => array(
        //
        "tablename_in_page_title" => "Communes",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Tables de références", ),
    ),
    "composition_scrutin" => array(
        "tablename_in_page_title" => "Composition",
        "breadcrumb_in_page_title" => array("Scrutins", "Composition des bureaux", ),
    ),
    "decoupage" => array(
        //
        "tablename_in_page_title" => "Découpage",
        "breadcrumb_in_page_title" => array("-", ),
    ),
    "departement" => array(
        //
        "tablename_in_page_title" => "Départements / Pays",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Tables de références", ),
    ),
    "electeur" => array(
        //
        "tablename_in_page_title" => "Liste électorale",
        "breadcrumb_in_page_title" => array("Consultation", ),
    ),
    "elu" => array(
        "tabs_in_form" => false,
        "tablename_in_page_title" => "Élus",
        "breadcrumb_in_page_title" => array("Scrutins", "Composition des bureaux", ),
    ),
    "fichier" => array(
        "displayed_fields_in_tableinc" => array(
            "id", "creationdate", "creationtime", "filename", "size", "mimetype", "type", "om_collectivite",
        ),
        "specific_config_for_fields" => array(
            "uid" => array(
                "abstract_type" => "file",
            ),
        )
    ),
    "grade" => array(
        "tablename_in_page_title" => "Grades",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Composition des bureaux", ),
    ),
    "liste" => array(
        //
        "tablename_in_page_title" => "Listes",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Métier", ),
    ),
    "mouvement" => array(
        "extended_class" => array("inscription", "modification", "radiation", ),
    ),
    "membre_commission" => array(
        //
        "tablename_in_page_title" => "Membres de la commission",
        "breadcrumb_in_page_title" => array("-", ),
    ),
    "nationalite" => array(
        //
        "tablename_in_page_title" => "Nationalités",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Tables de références", ),
    ),
    "numerobureau" => array(
        //
        "tablename_in_page_title" => "Séquences Numéro Bureau",
        "breadcrumb_in_page_title" => array("-", ),
    ),
    "om_collectivite" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Général", ),
    ),
    "om_dashboard" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Tableaux de bord", ),
    ),
    "om_droit" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Gestion des utilisateurs", ),
    ),
    "om_etat" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Éditions", ),
    ),
    "om_lettretype" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Éditions", ),
    ),
    "om_logo" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Éditions", ),
    ),
    "om_parametre" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Général", ),
    ),
    "om_permission" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Gestion des utilisateurs", ),
    ),
    "om_profil" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Gestion des utilisateurs", ),
    ),
    "om_requete" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Éditions", ),
    ),
    "om_sousetat" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Éditions", ),
    ),
    "om_utilisateur" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Gestion des utilisateurs", ),
    ),
    "om_widget" => array(
        "tabs_in_form" => false,
        "breadcrumb_in_page_title" => array($administration_parametrage, "Tableaux de bord", ),
    ),
    "param_mouvement" => array(
        //
        "tablename_in_page_title" => "Types de mouvements",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Métier", ),
    ),
    "parametrage_nb_jures" => array(
        //
        "tablename_in_page_title" => "Nombres de jurés d'assise",
        "breadcrumb_in_page_title" => array("-", ),
    ),
    "periode" => array(
        "tablename_in_page_title" => "Périodes",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Composition des bureaux", ),
    ),
    "poste" => array(
        "tablename_in_page_title" => "Postes",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Composition des bureaux", ),
    ),
    "revision" => array(
        //
        "tablename_in_page_title" => "Révisions",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Métier", ),
    ),
    "service" => array(
        "tablename_in_page_title" => "Services",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Composition des bureaux", ),
    ),
    "voie" => array(
        //
        "tablename_in_page_title" => "Voies",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Découpage", ),
    ),
    "piece" => array(
        "tablename_in_page_title" => "Pièce",
        "breadcrumb_in_page_title" => array("-", ),
        "displayed_fields_in_tableinc" => array(
            "libelle", "piece_type",
        ),
        "specific_config_for_fields" => array(
            "fichier" => array(
                "abstract_type" => "file",
            ),
        )
    ),
    "reu_notification" => array(
        "tablename_in_page_title" => "Notification",
        "breadcrumb_in_page_title" => array("-", ),
    ),
    "reu_scrutin" => array(
        "breadcrumb_in_page_title" => array("Traitement", ),
        "tablename_in_page_title" => "Élections",
        "displayed_fields_in_tableinc" => array(
            "id", "type", "partiel", "libelle", "zone", "date_tour1", "date_tour2", "date_debut", "date_l30", "date_fin",
        ),
        "tabs_in_form" => false,
    ),
    "trace" => array(
        "tablename_in_page_title" => "Traces",
        "breadcrumb_in_page_title" => array($administration_parametrage, "Métier", ),
    ),
);
