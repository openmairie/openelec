<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package openelec
 * @version SVN : $Id: gen.inc.php 2835 2014-08-05 16:42:02Z fmichon $
 */

$files_to_avoid = array(
    "fpdf_etiquette.class.php",
    "fpdf_fromdb_fromarray.class.php",
    "fpdf_table.class.php",
    "fpdf_carte_electorale.class.php",
    "openelec.class.php",
    "om_gen.class.php",
    "om_application_override.class.php",
    "reu.class.php",
);

$permissions = array(
    //
    "datetableau_consulter",
    "datetableau_changer",
    //
    "liste_de_travail_changer",
    //
    "electeur_carteretour_tab",
    //
    "reu_sync_l_e_reu_non_rattaches_tab",
    "reu_sync_l_e_oel_rattaches_exact_tab",
    "reu_sync_l_e_oel_non_rattaches_tab",
    "reu_sync_l_e_oel_rattaches_approx_tab",
    "reu_sync_l_e_reu_non_rattaches_affecter_bureau",
    //
    "electeur_exporter",
    "inscription_exporter",
    "modification_exporter",
    "radiation_exporter",
);
