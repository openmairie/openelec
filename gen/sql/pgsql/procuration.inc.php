<?php
//$Id$ 
//gen openMairie le 17/03/2022 09:34

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("procuration");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."procuration
    LEFT JOIN ".DB_PREFIXE."electeur as electeur0 
        ON procuration.mandant=electeur0.id 
    LEFT JOIN ".DB_PREFIXE."electeur as electeur1 
        ON procuration.mandataire=electeur1.id 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON procuration.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'procuration.id as "'.__("id").'"',
    'electeur0.liste as "'.__("mandant").'"',
    'electeur1.liste as "'.__("mandataire").'"',
    'to_char(procuration.debut_validite ,\'DD/MM/YYYY\') as "'.__("debut_validite").'"',
    'to_char(procuration.fin_validite ,\'DD/MM/YYYY\') as "'.__("fin_validite").'"',
    'to_char(procuration.date_accord ,\'DD/MM/YYYY\') as "'.__("date_accord").'"',
    'procuration.motif_refus as "'.__("motif_refus").'"',
    'procuration.id_externe as "'.__("id_externe").'"',
    'procuration.statut as "'.__("statut").'"',
    'procuration.mandant_ine as "'.__("mandant_ine").'"',
    'procuration.mandataire_ine as "'.__("mandataire_ine").'"',
    'procuration.mandant_resume as "'.__("mandant_resume").'"',
    'procuration.mandataire_resume as "'.__("mandataire_resume").'"',
    "case procuration.vu when 't' then 'Oui' else 'Non' end as \"".__("vu")."\"",
    'procuration.autorite_nom_prenom as "'.__("autorite_nom_prenom").'"',
    'procuration.autorite_commune as "'.__("autorite_commune").'"',
    'procuration.autorite_consulat as "'.__("autorite_consulat").'"',
    'procuration.autorite_type as "'.__("autorite_type").'"',
    'procuration.annulation_autorite_nom_prenom as "'.__("annulation_autorite_nom_prenom").'"',
    'procuration.annulation_autorite_commune as "'.__("annulation_autorite_commune").'"',
    'procuration.annulation_autorite_consulat as "'.__("annulation_autorite_consulat").'"',
    'procuration.annulation_autorite_type as "'.__("annulation_autorite_type").'"',
    'to_char(procuration.annulation_date ,\'DD/MM/YYYY\') as "'.__("annulation_date").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'procuration.mandant_infos as "'.__("mandant_infos").'"',
    'procuration.mandataire_infos as "'.__("mandataire_infos").'"',
    'procuration.om_collectivite as "'.__("om_collectivite").'"',
    'procuration.historique as "'.__("historique").'"',
    'procuration.notes as "'.__("notes").'"',
    );
//
$champRecherche = array(
    'procuration.id as "'.__("id").'"',
    'electeur0.liste as "'.__("mandant").'"',
    'electeur1.liste as "'.__("mandataire").'"',
    'procuration.motif_refus as "'.__("motif_refus").'"',
    'procuration.id_externe as "'.__("id_externe").'"',
    'procuration.statut as "'.__("statut").'"',
    'procuration.mandant_ine as "'.__("mandant_ine").'"',
    'procuration.mandataire_ine as "'.__("mandataire_ine").'"',
    'procuration.mandant_resume as "'.__("mandant_resume").'"',
    'procuration.mandataire_resume as "'.__("mandataire_resume").'"',
    'procuration.autorite_nom_prenom as "'.__("autorite_nom_prenom").'"',
    'procuration.autorite_commune as "'.__("autorite_commune").'"',
    'procuration.autorite_consulat as "'.__("autorite_consulat").'"',
    'procuration.autorite_type as "'.__("autorite_type").'"',
    'procuration.annulation_autorite_nom_prenom as "'.__("annulation_autorite_nom_prenom").'"',
    'procuration.annulation_autorite_commune as "'.__("annulation_autorite_commune").'"',
    'procuration.annulation_autorite_consulat as "'.__("annulation_autorite_consulat").'"',
    'procuration.annulation_autorite_type as "'.__("annulation_autorite_type").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY electeur0.liste ASC NULLS LAST";
$edition="procuration";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (procuration.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "electeur" => array("electeur", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - electeur
if (in_array($retourformulaire, $foreign_keys_extended["electeur"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (procuration.mandant = ".intval($idxformulaire)." OR procuration.mandataire = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (procuration.om_collectivite = '".$_SESSION["collectivite"]."') AND (procuration.mandant = ".intval($idxformulaire)." OR procuration.mandataire = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (procuration.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (procuration.om_collectivite = '".$_SESSION["collectivite"]."') AND (procuration.om_collectivite = ".intval($idxformulaire).") ";
    }
}

