<?php
//$Id$ 
//gen openMairie le 18/11/2021 23:35

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("mouvement");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."mouvement
    LEFT JOIN ".DB_PREFIXE."bureau as bureau0 
        ON mouvement.ancien_bureau=bureau0.id 
    LEFT JOIN ".DB_PREFIXE."bureau as bureau1 
        ON mouvement.bureau=bureau1.id 
    LEFT JOIN ".DB_PREFIXE."nationalite 
        ON mouvement.code_nationalite=nationalite.code 
    LEFT JOIN ".DB_PREFIXE."liste 
        ON mouvement.liste=liste.liste 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON mouvement.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'mouvement.id as "'.__("id").'"',
    'mouvement.etat as "'.__("etat").'"',
    'liste.libelle_liste as "'.__("liste").'"',
    'mouvement.types as "'.__("types").'"',
    'mouvement.electeur_id as "'.__("electeur_id").'"',
    'bureau1.libelle as "'.__("bureau").'"',
    'mouvement.bureau_de_vote_code as "'.__("bureau_de_vote_code").'"',
    'mouvement.bureauforce as "'.__("bureauforce").'"',
    'mouvement.numero_bureau as "'.__("numero_bureau").'"',
    'to_char(mouvement.date_modif ,\'DD/MM/YYYY\') as "'.__("date_modif").'"',
    'mouvement.utilisateur as "'.__("utilisateur").'"',
    'mouvement.civilite as "'.__("civilite").'"',
    'mouvement.sexe as "'.__("sexe").'"',
    'mouvement.nom as "'.__("nom").'"',
    'mouvement.nom_usage as "'.__("nom_usage").'"',
    'mouvement.prenom as "'.__("prenom").'"',
    'to_char(mouvement.date_naissance ,\'DD/MM/YYYY\') as "'.__("date_naissance").'"',
    'mouvement.code_departement_naissance as "'.__("code_departement_naissance").'"',
    'mouvement.libelle_departement_naissance as "'.__("libelle_departement_naissance").'"',
    'mouvement.code_lieu_de_naissance as "'.__("code_lieu_de_naissance").'"',
    'mouvement.libelle_lieu_de_naissance as "'.__("libelle_lieu_de_naissance").'"',
    'nationalite.libelle_nationalite as "'.__("code_nationalite").'"',
    'mouvement.code_voie as "'.__("code_voie").'"',
    'mouvement.libelle_voie as "'.__("libelle_voie").'"',
    'mouvement.numero_habitation as "'.__("numero_habitation").'"',
    'mouvement.complement_numero as "'.__("complement_numero").'"',
    'mouvement.complement as "'.__("complement").'"',
    'mouvement.provenance as "'.__("provenance").'"',
    'mouvement.libelle_provenance as "'.__("libelle_provenance").'"',
    'mouvement.ancien_bureau_de_vote_code as "'.__("ancien_bureau_de_vote_code").'"',
    'mouvement.observation as "'.__("observation").'"',
    'mouvement.resident as "'.__("resident").'"',
    'mouvement.adresse_resident as "'.__("adresse_resident").'"',
    'mouvement.complement_resident as "'.__("complement_resident").'"',
    'mouvement.cp_resident as "'.__("cp_resident").'"',
    'mouvement.ville_resident as "'.__("ville_resident").'"',
    'mouvement.tableau as "'.__("tableau").'"',
    'to_char(mouvement.date_j5 ,\'DD/MM/YYYY\') as "'.__("date_j5").'"',
    'to_char(mouvement.date_tableau ,\'DD/MM/YYYY\') as "'.__("date_tableau").'"',
    'mouvement.envoi_cnen as "'.__("envoi_cnen").'"',
    'to_char(mouvement.date_cnen ,\'DD/MM/YYYY\') as "'.__("date_cnen").'"',
    'mouvement.telephone as "'.__("telephone").'"',
    'mouvement.courriel as "'.__("courriel").'"',
    'mouvement.ine as "'.__("ine").'"',
    'mouvement.id_demande as "'.__("id_demande").'"',
    'mouvement.statut as "'.__("statut").'"',
    'mouvement.visa as "'.__("visa").'"',
    'to_char(mouvement.date_visa ,\'DD/MM/YYYY\') as "'.__("date_visa").'"',
    'to_char(mouvement.date_complet ,\'DD/MM/YYYY\') as "'.__("date_complet").'"',
    'to_char(mouvement.date_demande ,\'DD/MM/YYYY\') as "'.__("date_demande").'"',
    'bureau0.libelle as "'.__("ancien_bureau").'"',
    'mouvement.bureau_de_vote_libelle as "'.__("bureau_de_vote_libelle").'"',
    'mouvement.ancien_bureau_de_vote_libelle as "'.__("ancien_bureau_de_vote_libelle").'"',
    'mouvement.provenance_demande as "'.__("provenance_demande").'"',
    "case mouvement.vu when 't' then 'Oui' else 'Non' end as \"".__("vu")."\"",
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'mouvement.om_collectivite as "'.__("om_collectivite").'"',
    'mouvement.adresse_rattachement_reu as "'.__("adresse_rattachement_reu").'"',
    'mouvement.historique as "'.__("historique").'"',
    'mouvement.archive_electeur as "'.__("archive_electeur").'"',
    );
//
$champRecherche = array(
    'mouvement.id as "'.__("id").'"',
    'mouvement.etat as "'.__("etat").'"',
    'liste.libelle_liste as "'.__("liste").'"',
    'mouvement.types as "'.__("types").'"',
    'mouvement.electeur_id as "'.__("electeur_id").'"',
    'bureau1.libelle as "'.__("bureau").'"',
    'mouvement.bureau_de_vote_code as "'.__("bureau_de_vote_code").'"',
    'mouvement.bureauforce as "'.__("bureauforce").'"',
    'mouvement.numero_bureau as "'.__("numero_bureau").'"',
    'mouvement.utilisateur as "'.__("utilisateur").'"',
    'mouvement.civilite as "'.__("civilite").'"',
    'mouvement.sexe as "'.__("sexe").'"',
    'mouvement.nom as "'.__("nom").'"',
    'mouvement.nom_usage as "'.__("nom_usage").'"',
    'mouvement.prenom as "'.__("prenom").'"',
    'mouvement.code_departement_naissance as "'.__("code_departement_naissance").'"',
    'mouvement.libelle_departement_naissance as "'.__("libelle_departement_naissance").'"',
    'mouvement.code_lieu_de_naissance as "'.__("code_lieu_de_naissance").'"',
    'mouvement.libelle_lieu_de_naissance as "'.__("libelle_lieu_de_naissance").'"',
    'nationalite.libelle_nationalite as "'.__("code_nationalite").'"',
    'mouvement.code_voie as "'.__("code_voie").'"',
    'mouvement.libelle_voie as "'.__("libelle_voie").'"',
    'mouvement.numero_habitation as "'.__("numero_habitation").'"',
    'mouvement.complement_numero as "'.__("complement_numero").'"',
    'mouvement.complement as "'.__("complement").'"',
    'mouvement.provenance as "'.__("provenance").'"',
    'mouvement.libelle_provenance as "'.__("libelle_provenance").'"',
    'mouvement.ancien_bureau_de_vote_code as "'.__("ancien_bureau_de_vote_code").'"',
    'mouvement.observation as "'.__("observation").'"',
    'mouvement.resident as "'.__("resident").'"',
    'mouvement.adresse_resident as "'.__("adresse_resident").'"',
    'mouvement.complement_resident as "'.__("complement_resident").'"',
    'mouvement.cp_resident as "'.__("cp_resident").'"',
    'mouvement.ville_resident as "'.__("ville_resident").'"',
    'mouvement.tableau as "'.__("tableau").'"',
    'mouvement.envoi_cnen as "'.__("envoi_cnen").'"',
    'mouvement.telephone as "'.__("telephone").'"',
    'mouvement.courriel as "'.__("courriel").'"',
    'mouvement.ine as "'.__("ine").'"',
    'mouvement.id_demande as "'.__("id_demande").'"',
    'mouvement.statut as "'.__("statut").'"',
    'mouvement.visa as "'.__("visa").'"',
    'bureau0.libelle as "'.__("ancien_bureau").'"',
    'mouvement.bureau_de_vote_libelle as "'.__("bureau_de_vote_libelle").'"',
    'mouvement.ancien_bureau_de_vote_libelle as "'.__("ancien_bureau_de_vote_libelle").'"',
    'mouvement.provenance_demande as "'.__("provenance_demande").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY mouvement.etat ASC NULLS LAST";
$edition="mouvement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (mouvement.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "bureau" => array("bureau", ),
    "nationalite" => array("nationalite", ),
    "liste" => array("liste", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - bureau
if (in_array($retourformulaire, $foreign_keys_extended["bureau"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (mouvement.ancien_bureau = ".intval($idxformulaire)." OR mouvement.bureau = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (mouvement.om_collectivite = '".$_SESSION["collectivite"]."') AND (mouvement.ancien_bureau = ".intval($idxformulaire)." OR mouvement.bureau = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - nationalite
if (in_array($retourformulaire, $foreign_keys_extended["nationalite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (mouvement.code_nationalite = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (mouvement.om_collectivite = '".$_SESSION["collectivite"]."') AND (mouvement.code_nationalite = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - liste
if (in_array($retourformulaire, $foreign_keys_extended["liste"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (mouvement.liste = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (mouvement.om_collectivite = '".$_SESSION["collectivite"]."') AND (mouvement.liste = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (mouvement.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (mouvement.om_collectivite = '".$_SESSION["collectivite"]."') AND (mouvement.om_collectivite = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'piece',
);

