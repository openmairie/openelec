<?php
//$Id$ 
//gen openMairie le 06/06/2018 15:37

$DEBUG=0;
$serie=15;
$ent = __("-")." -> ".__("Découpage");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."decoupage
    LEFT JOIN ".DB_PREFIXE."bureau 
        ON decoupage.bureau=bureau.id 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON decoupage.code_voie=voie.code ";
// SELECT 
$champAffiche = array(
    'decoupage.id as "'.__("id").'"',
    'bureau.libelle as "'.__("bureau").'"',
    'voie.libelle_voie as "'.__("code_voie").'"',
    'decoupage.premier_impair as "'.__("premier_impair").'"',
    'decoupage.dernier_impair as "'.__("dernier_impair").'"',
    'decoupage.premier_pair as "'.__("premier_pair").'"',
    'decoupage.dernier_pair as "'.__("dernier_pair").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'decoupage.id as "'.__("id").'"',
    'bureau.libelle as "'.__("bureau").'"',
    'voie.libelle_voie as "'.__("code_voie").'"',
    'decoupage.premier_impair as "'.__("premier_impair").'"',
    'decoupage.dernier_impair as "'.__("dernier_impair").'"',
    'decoupage.premier_pair as "'.__("premier_pair").'"',
    'decoupage.dernier_pair as "'.__("dernier_pair").'"',
    );
$tri="ORDER BY bureau.libelle ASC NULLS LAST";
$edition="decoupage";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "bureau" => array("bureau", ),
    "voie" => array("voie", ),
);
// Filtre listing sous formulaire - bureau
if (in_array($retourformulaire, $foreign_keys_extended["bureau"])) {
    $selection = " WHERE (decoupage.bureau = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - voie
if (in_array($retourformulaire, $foreign_keys_extended["voie"])) {
    $selection = " WHERE (decoupage.code_voie = '".$f->db->escapeSimple($idxformulaire)."') ";
}

