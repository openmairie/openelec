<?php
//$Id$ 
//gen openMairie le 15/02/2022 17:28

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Composition des bureaux")." -> ".__("Périodes");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."periode";
// SELECT 
$champAffiche = array(
    'periode.periode as "'.__("periode").'"',
    'periode.libelle as "'.__("libelle").'"',
    'periode.debut as "'.__("debut").'"',
    'periode.fin as "'.__("fin").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'periode.periode as "'.__("periode").'"',
    'periode.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY periode.libelle ASC NULLS LAST";
$edition="periode";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'affectation',
    'candidature',
);

