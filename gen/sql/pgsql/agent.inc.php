<?php
//$Id$ 
//gen openMairie le 18/02/2022 00:25

$DEBUG=0;
$serie=15;
$ent = __("Scrutins")." -> ".__("Composition des bureaux")." -> ".__("Agents");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."agent
    LEFT JOIN ".DB_PREFIXE."grade 
        ON agent.grade=grade.grade 
    LEFT JOIN ".DB_PREFIXE."service 
        ON agent.service=service.service ";
// SELECT 
$champAffiche = array(
    'agent.id as "'.__("id").'"',
    'agent.nom as "'.__("nom").'"',
    'agent.prenom as "'.__("prenom").'"',
    'agent.adresse as "'.__("adresse").'"',
    'agent.cp as "'.__("cp").'"',
    'agent.ville as "'.__("ville").'"',
    'agent.telephone as "'.__("telephone").'"',
    'service.libelle as "'.__("service").'"',
    'agent.telephone_pro as "'.__("telephone_pro").'"',
    'grade.libelle as "'.__("grade").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'agent.id as "'.__("id").'"',
    'agent.nom as "'.__("nom").'"',
    'agent.prenom as "'.__("prenom").'"',
    'agent.adresse as "'.__("adresse").'"',
    'agent.cp as "'.__("cp").'"',
    'agent.ville as "'.__("ville").'"',
    'agent.telephone as "'.__("telephone").'"',
    'service.libelle as "'.__("service").'"',
    'agent.telephone_pro as "'.__("telephone_pro").'"',
    'grade.libelle as "'.__("grade").'"',
    );
$tri="ORDER BY agent.nom ASC NULLS LAST";
$edition="agent";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "grade" => array("grade", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - grade
if (in_array($retourformulaire, $foreign_keys_extended["grade"])) {
    $selection = " WHERE (agent.grade = '".$f->db->escapeSimple($idxformulaire)."') ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (agent.service = '".$f->db->escapeSimple($idxformulaire)."') ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'candidature',
);

