<?php
//$Id$ 
//gen openMairie le 18/02/2022 03:08

$DEBUG=0;
$serie=15;
$ent = __("Scrutins")." -> ".__("Composition des bureaux")." -> ".__("Composition");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."composition_scrutin
    LEFT JOIN ".DB_PREFIXE."reu_scrutin 
        ON composition_scrutin.scrutin=reu_scrutin.id ";
// SELECT 
$champAffiche = array(
    'composition_scrutin.id as "'.__("id").'"',
    'reu_scrutin.libelle as "'.__("scrutin").'"',
    'composition_scrutin.libelle as "'.__("libelle").'"',
    'composition_scrutin.tour as "'.__("tour").'"',
    'to_char(composition_scrutin.date_tour ,\'DD/MM/YYYY\') as "'.__("date_tour").'"',
    "case composition_scrutin.solde when 't' then 'Oui' else 'Non' end as \"".__("solde")."\"",
    'composition_scrutin.convocation_agent as "'.__("convocation_agent").'"',
    'composition_scrutin.convocation_president as "'.__("convocation_president").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'composition_scrutin.id as "'.__("id").'"',
    'reu_scrutin.libelle as "'.__("scrutin").'"',
    'composition_scrutin.libelle as "'.__("libelle").'"',
    'composition_scrutin.tour as "'.__("tour").'"',
    'composition_scrutin.convocation_agent as "'.__("convocation_agent").'"',
    'composition_scrutin.convocation_president as "'.__("convocation_president").'"',
    );
$tri="ORDER BY composition_scrutin.libelle ASC NULLS LAST";
$edition="composition_scrutin";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "reu_scrutin" => array("reu_scrutin", ),
);
// Filtre listing sous formulaire - reu_scrutin
if (in_array($retourformulaire, $foreign_keys_extended["reu_scrutin"])) {
    $selection = " WHERE (composition_scrutin.scrutin = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'affectation',
    'candidat',
    'candidature',
    'composition_bureau',
);

