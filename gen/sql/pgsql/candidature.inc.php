<?php
//$Id$ 
//gen openMairie le 18/02/2022 01:27

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("candidature");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."candidature
    LEFT JOIN ".DB_PREFIXE."agent 
        ON candidature.agent=agent.id 
    LEFT JOIN ".DB_PREFIXE."bureau 
        ON candidature.bureau=bureau.id 
    LEFT JOIN ".DB_PREFIXE."composition_scrutin 
        ON candidature.composition_scrutin=composition_scrutin.id 
    LEFT JOIN ".DB_PREFIXE."periode 
        ON candidature.periode=periode.periode 
    LEFT JOIN ".DB_PREFIXE."poste 
        ON candidature.poste=poste.poste ";
// SELECT 
$champAffiche = array(
    'candidature.id as "'.__("id").'"',
    'agent.nom as "'.__("agent").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    'periode.libelle as "'.__("periode").'"',
    'poste.libelle as "'.__("poste").'"',
    'bureau.libelle as "'.__("bureau").'"',
    "case candidature.decision when 't' then 'Oui' else 'Non' end as \"".__("decision")."\"",
    "case candidature.recuperation when 't' then 'Oui' else 'Non' end as \"".__("recuperation")."\"",
    'candidature.debut as "'.__("debut").'"',
    'candidature.fin as "'.__("fin").'"',
    );
//
$champNonAffiche = array(
    'candidature.note as "'.__("note").'"',
    );
//
$champRecherche = array(
    'candidature.id as "'.__("id").'"',
    'agent.nom as "'.__("agent").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    'periode.libelle as "'.__("periode").'"',
    'poste.libelle as "'.__("poste").'"',
    'bureau.libelle as "'.__("bureau").'"',
    );
$tri="ORDER BY agent.nom ASC NULLS LAST";
$edition="candidature";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "agent" => array("agent", ),
    "bureau" => array("bureau", ),
    "composition_scrutin" => array("composition_scrutin", ),
    "periode" => array("periode", ),
    "poste" => array("poste", ),
);
// Filtre listing sous formulaire - agent
if (in_array($retourformulaire, $foreign_keys_extended["agent"])) {
    $selection = " WHERE (candidature.agent = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - bureau
if (in_array($retourformulaire, $foreign_keys_extended["bureau"])) {
    $selection = " WHERE (candidature.bureau = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - composition_scrutin
if (in_array($retourformulaire, $foreign_keys_extended["composition_scrutin"])) {
    $selection = " WHERE (candidature.composition_scrutin = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - periode
if (in_array($retourformulaire, $foreign_keys_extended["periode"])) {
    $selection = " WHERE (candidature.periode = '".$f->db->escapeSimple($idxformulaire)."') ";
}
// Filtre listing sous formulaire - poste
if (in_array($retourformulaire, $foreign_keys_extended["poste"])) {
    $selection = " WHERE (candidature.poste = '".$f->db->escapeSimple($idxformulaire)."') ";
}

