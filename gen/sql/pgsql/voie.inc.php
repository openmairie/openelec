<?php
//$Id$ 
//gen openMairie le 06/06/2018 11:20

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Découpage")." -> ".__("Voies");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."voie
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON voie.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'voie.code as "'.__("code").'"',
    'voie.libelle_voie as "'.__("libelle_voie").'"',
    'voie.cp as "'.__("cp").'"',
    'voie.ville as "'.__("ville").'"',
    'voie.abrege as "'.__("abrege").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'voie.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'voie.code as "'.__("code").'"',
    'voie.libelle_voie as "'.__("libelle_voie").'"',
    'voie.cp as "'.__("cp").'"',
    'voie.ville as "'.__("ville").'"',
    'voie.abrege as "'.__("abrege").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY voie.libelle_voie ASC NULLS LAST";
$edition="voie";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (voie.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (voie.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (voie.om_collectivite = '".$_SESSION["collectivite"]."') AND (voie.om_collectivite = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'decoupage',
);

