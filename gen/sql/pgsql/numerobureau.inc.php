<?php
//$Id$ 
//gen openMairie le 06/06/2018 15:31

$DEBUG=0;
$serie=15;
$ent = __("-")." -> ".__("Séquences Numéro Bureau");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."numerobureau
    LEFT JOIN ".DB_PREFIXE."bureau 
        ON numerobureau.bureau=bureau.id 
    LEFT JOIN ".DB_PREFIXE."liste 
        ON numerobureau.liste=liste.liste 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON numerobureau.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'numerobureau.id as "'.__("id").'"',
    'liste.libelle_liste as "'.__("liste").'"',
    'bureau.libelle as "'.__("bureau").'"',
    'numerobureau.dernier_numero as "'.__("dernier_numero").'"',
    'numerobureau.dernier_numero_provisoire as "'.__("dernier_numero_provisoire").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'numerobureau.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'numerobureau.id as "'.__("id").'"',
    'liste.libelle_liste as "'.__("liste").'"',
    'bureau.libelle as "'.__("bureau").'"',
    'numerobureau.dernier_numero as "'.__("dernier_numero").'"',
    'numerobureau.dernier_numero_provisoire as "'.__("dernier_numero_provisoire").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY om_collectivite.libelle ASC NULLS LAST";
$edition="numerobureau";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (numerobureau.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "bureau" => array("bureau", ),
    "liste" => array("liste", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - bureau
if (in_array($retourformulaire, $foreign_keys_extended["bureau"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (numerobureau.bureau = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (numerobureau.om_collectivite = '".$_SESSION["collectivite"]."') AND (numerobureau.bureau = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - liste
if (in_array($retourformulaire, $foreign_keys_extended["liste"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (numerobureau.liste = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (numerobureau.om_collectivite = '".$_SESSION["collectivite"]."') AND (numerobureau.liste = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (numerobureau.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (numerobureau.om_collectivite = '".$_SESSION["collectivite"]."') AND (numerobureau.om_collectivite = ".intval($idxformulaire).") ";
    }
}

