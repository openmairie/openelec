<?php
//$Id$ 
//gen openMairie le 02/05/2023 15:42

$DEBUG=0;
$serie=15;
$ent = __("-")." -> ".__("Nombres de jurés d'assise");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."parametrage_nb_jures
    LEFT JOIN ".DB_PREFIXE."canton 
        ON parametrage_nb_jures.canton=canton.id 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON parametrage_nb_jures.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'parametrage_nb_jures.id as "'.__("id").'"',
    'canton.libelle as "'.__("canton").'"',
    'parametrage_nb_jures.nb_jures as "'.__("nb_jures").'"',
    'parametrage_nb_jures.nb_suppleants as "'.__("nb_suppleants").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'parametrage_nb_jures.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'parametrage_nb_jures.id as "'.__("id").'"',
    'canton.libelle as "'.__("canton").'"',
    'parametrage_nb_jures.nb_jures as "'.__("nb_jures").'"',
    'parametrage_nb_jures.nb_suppleants as "'.__("nb_suppleants").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY canton.libelle ASC NULLS LAST";
$edition="parametrage_nb_jures";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (parametrage_nb_jures.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "canton" => array("canton", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - canton
if (in_array($retourformulaire, $foreign_keys_extended["canton"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (parametrage_nb_jures.canton = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (parametrage_nb_jures.om_collectivite = '".$_SESSION["collectivite"]."') AND (parametrage_nb_jures.canton = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (parametrage_nb_jures.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (parametrage_nb_jures.om_collectivite = '".$_SESSION["collectivite"]."') AND (parametrage_nb_jures.om_collectivite = ".intval($idxformulaire).") ";
    }
}

