<?php
//$Id$ 
//gen openMairie le 02/09/2022 15:24

$DEBUG=0;
$serie=15;
$ent = __("Consultation")." -> ".__("Liste électorale");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."electeur
    LEFT JOIN ".DB_PREFIXE."bureau 
        ON electeur.bureau=bureau.id 
    LEFT JOIN ".DB_PREFIXE."nationalite 
        ON electeur.code_nationalite=nationalite.code 
    LEFT JOIN ".DB_PREFIXE."liste 
        ON electeur.liste=liste.liste 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON electeur.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'electeur.id as "'.__("id").'"',
    'liste.libelle_liste as "'.__("liste").'"',
    'bureau.libelle as "'.__("bureau").'"',
    'electeur.bureauforce as "'.__("bureauforce").'"',
    'electeur.numero_bureau as "'.__("numero_bureau").'"',
    'to_char(electeur.date_modif ,\'DD/MM/YYYY\') as "'.__("date_modif").'"',
    'electeur.utilisateur as "'.__("utilisateur").'"',
    'electeur.civilite as "'.__("civilite").'"',
    'electeur.sexe as "'.__("sexe").'"',
    'electeur.nom as "'.__("nom").'"',
    'electeur.nom_usage as "'.__("nom_usage").'"',
    'electeur.prenom as "'.__("prenom").'"',
    'to_char(electeur.date_naissance ,\'DD/MM/YYYY\') as "'.__("date_naissance").'"',
    'electeur.code_departement_naissance as "'.__("code_departement_naissance").'"',
    'electeur.libelle_departement_naissance as "'.__("libelle_departement_naissance").'"',
    'electeur.code_lieu_de_naissance as "'.__("code_lieu_de_naissance").'"',
    'electeur.libelle_lieu_de_naissance as "'.__("libelle_lieu_de_naissance").'"',
    'nationalite.libelle_nationalite as "'.__("code_nationalite").'"',
    'electeur.code_voie as "'.__("code_voie").'"',
    'electeur.libelle_voie as "'.__("libelle_voie").'"',
    'electeur.numero_habitation as "'.__("numero_habitation").'"',
    'electeur.complement_numero as "'.__("complement_numero").'"',
    'electeur.complement as "'.__("complement").'"',
    'electeur.provenance as "'.__("provenance").'"',
    'electeur.libelle_provenance as "'.__("libelle_provenance").'"',
    'electeur.resident as "'.__("resident").'"',
    'electeur.adresse_resident as "'.__("adresse_resident").'"',
    'electeur.complement_resident as "'.__("complement_resident").'"',
    'electeur.cp_resident as "'.__("cp_resident").'"',
    'electeur.ville_resident as "'.__("ville_resident").'"',
    'electeur.tableau as "'.__("tableau").'"',
    'to_char(electeur.date_tableau ,\'DD/MM/YYYY\') as "'.__("date_tableau").'"',
    'electeur.mouvement as "'.__("mouvement").'"',
    'to_char(electeur.date_mouvement ,\'DD/MM/YYYY\') as "'.__("date_mouvement").'"',
    'electeur.typecat as "'.__("typecat").'"',
    'electeur.carte as "'.__("carte").'"',
    'electeur.procuration as "'.__("procuration").'"',
    'electeur.jury as "'.__("jury").'"',
    'to_char(electeur.date_inscription ,\'DD/MM/YYYY\') as "'.__("date_inscription").'"',
    'electeur.code_inscription as "'.__("code_inscription").'"',
    'electeur.jury_effectif as "'.__("jury_effectif").'"',
    'to_char(electeur.date_jeffectif ,\'DD/MM/YYYY\') as "'.__("date_jeffectif").'"',
    'electeur.profession as "'.__("profession").'"',
    'electeur.motif_dispense_jury as "'.__("motif_dispense_jury").'"',
    'electeur.telephone as "'.__("telephone").'"',
    'electeur.courriel as "'.__("courriel").'"',
    'electeur.ine as "'.__("ine").'"',
    'electeur.reu_sync_info as "'.__("reu_sync_info").'"',
    "case electeur.a_transmettre when 't' then 'Oui' else 'Non' end as \"".__("a_transmettre")."\"",
    'to_char(electeur.date_saisie_carte_retour ,\'DD/MM/YYYY\') as "'.__("date_saisie_carte_retour").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'electeur.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'electeur.id as "'.__("id").'"',
    'liste.libelle_liste as "'.__("liste").'"',
    'bureau.libelle as "'.__("bureau").'"',
    'electeur.bureauforce as "'.__("bureauforce").'"',
    'electeur.numero_bureau as "'.__("numero_bureau").'"',
    'electeur.utilisateur as "'.__("utilisateur").'"',
    'electeur.civilite as "'.__("civilite").'"',
    'electeur.sexe as "'.__("sexe").'"',
    'electeur.nom as "'.__("nom").'"',
    'electeur.nom_usage as "'.__("nom_usage").'"',
    'electeur.prenom as "'.__("prenom").'"',
    'electeur.code_departement_naissance as "'.__("code_departement_naissance").'"',
    'electeur.libelle_departement_naissance as "'.__("libelle_departement_naissance").'"',
    'electeur.code_lieu_de_naissance as "'.__("code_lieu_de_naissance").'"',
    'electeur.libelle_lieu_de_naissance as "'.__("libelle_lieu_de_naissance").'"',
    'nationalite.libelle_nationalite as "'.__("code_nationalite").'"',
    'electeur.code_voie as "'.__("code_voie").'"',
    'electeur.libelle_voie as "'.__("libelle_voie").'"',
    'electeur.numero_habitation as "'.__("numero_habitation").'"',
    'electeur.complement_numero as "'.__("complement_numero").'"',
    'electeur.complement as "'.__("complement").'"',
    'electeur.provenance as "'.__("provenance").'"',
    'electeur.libelle_provenance as "'.__("libelle_provenance").'"',
    'electeur.resident as "'.__("resident").'"',
    'electeur.adresse_resident as "'.__("adresse_resident").'"',
    'electeur.complement_resident as "'.__("complement_resident").'"',
    'electeur.cp_resident as "'.__("cp_resident").'"',
    'electeur.ville_resident as "'.__("ville_resident").'"',
    'electeur.tableau as "'.__("tableau").'"',
    'electeur.mouvement as "'.__("mouvement").'"',
    'electeur.typecat as "'.__("typecat").'"',
    'electeur.carte as "'.__("carte").'"',
    'electeur.procuration as "'.__("procuration").'"',
    'electeur.jury as "'.__("jury").'"',
    'electeur.code_inscription as "'.__("code_inscription").'"',
    'electeur.jury_effectif as "'.__("jury_effectif").'"',
    'electeur.profession as "'.__("profession").'"',
    'electeur.motif_dispense_jury as "'.__("motif_dispense_jury").'"',
    'electeur.telephone as "'.__("telephone").'"',
    'electeur.courriel as "'.__("courriel").'"',
    'electeur.ine as "'.__("ine").'"',
    'electeur.reu_sync_info as "'.__("reu_sync_info").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY liste.libelle_liste ASC NULLS LAST";
$edition="electeur";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (electeur.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "bureau" => array("bureau", ),
    "nationalite" => array("nationalite", ),
    "liste" => array("liste", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - bureau
if (in_array($retourformulaire, $foreign_keys_extended["bureau"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (electeur.bureau = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (electeur.om_collectivite = '".$_SESSION["collectivite"]."') AND (electeur.bureau = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - nationalite
if (in_array($retourformulaire, $foreign_keys_extended["nationalite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (electeur.code_nationalite = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (electeur.om_collectivite = '".$_SESSION["collectivite"]."') AND (electeur.code_nationalite = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - liste
if (in_array($retourformulaire, $foreign_keys_extended["liste"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (electeur.liste = '".$f->db->escapeSimple($idxformulaire)."') ";
    } else {
        // Filtre MONO
        $selection = " WHERE (electeur.om_collectivite = '".$_SESSION["collectivite"]."') AND (electeur.liste = '".$f->db->escapeSimple($idxformulaire)."') ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (electeur.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (electeur.om_collectivite = '".$_SESSION["collectivite"]."') AND (electeur.om_collectivite = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'procuration',
);

