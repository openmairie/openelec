<?php
//$Id$ 
//gen openMairie le 06/06/2018 11:20

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Métier")." -> ".__("Révisions");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."revision";
// SELECT 
$champAffiche = array(
    'revision.id as "'.__("id").'"',
    'revision.libelle as "'.__("libelle").'"',
    'to_char(revision.date_debut ,\'DD/MM/YYYY\') as "'.__("date_debut").'"',
    'to_char(revision.date_effet ,\'DD/MM/YYYY\') as "'.__("date_effet").'"',
    'to_char(revision.date_tr1 ,\'DD/MM/YYYY\') as "'.__("date_tr1").'"',
    'to_char(revision.date_tr2 ,\'DD/MM/YYYY\') as "'.__("date_tr2").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'revision.id as "'.__("id").'"',
    'revision.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY revision.libelle ASC NULLS LAST";
$edition="revision";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

