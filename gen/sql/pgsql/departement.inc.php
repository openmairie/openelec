<?php
//$Id$ 
//gen openMairie le 06/06/2018 11:20

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Tables de références")." -> ".__("Départements / Pays");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."departement";
// SELECT 
$champAffiche = array(
    'departement.code as "'.__("code").'"',
    'departement.libelle_departement as "'.__("libelle_departement").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'departement.code as "'.__("code").'"',
    'departement.libelle_departement as "'.__("libelle_departement").'"',
    );
$tri="ORDER BY departement.libelle_departement ASC NULLS LAST";
$edition="departement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'commune',
);

