<?php
//$Id$ 
//gen openMairie le 17/02/2022 17:25

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Découpage")." -> ".__("Circonscriptions");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."circonscription";
// SELECT 
$champAffiche = array(
    'circonscription.id as "'.__("id").'"',
    'circonscription.code as "'.__("code").'"',
    'circonscription.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'circonscription.id as "'.__("id").'"',
    'circonscription.code as "'.__("code").'"',
    'circonscription.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY circonscription.libelle ASC NULLS LAST";
$edition="circonscription";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'bureau',
    'reu_scrutin',
);

