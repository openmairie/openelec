<?php
//$Id$ 
//gen openMairie le 04/11/2021 16:51

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("fichier");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."fichier
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON fichier.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'fichier.id as "'.__("id").'"',
    'to_char(fichier.creationdate ,\'DD/MM/YYYY\') as "'.__("creationdate").'"',
    'fichier.creationtime as "'.__("creationtime").'"',
    'fichier.filename as "'.__("filename").'"',
    'fichier.size as "'.__("size").'"',
    'fichier.mimetype as "'.__("mimetype").'"',
    'fichier.type as "'.__("type").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'fichier.uid as "'.__("uid").'"',
    'fichier.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'fichier.id as "'.__("id").'"',
    'fichier.filename as "'.__("filename").'"',
    'fichier.size as "'.__("size").'"',
    'fichier.mimetype as "'.__("mimetype").'"',
    'fichier.type as "'.__("type").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY fichier.creationdate ASC NULLS LAST";
$edition="fichier";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (fichier.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (fichier.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (fichier.om_collectivite = '".$_SESSION["collectivite"]."') AND (fichier.om_collectivite = ".intval($idxformulaire).") ";
    }
}

