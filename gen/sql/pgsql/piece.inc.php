<?php
//$Id$ 
//gen openMairie le 30/01/2019 10:58

$DEBUG=0;
$serie=15;
$ent = __("-")." -> ".__("Pièce");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."piece
    LEFT JOIN ".DB_PREFIXE."mouvement 
        ON piece.mouvement=mouvement.id 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON piece.om_collectivite=om_collectivite.om_collectivite 
    LEFT JOIN ".DB_PREFIXE."piece_type 
        ON piece.piece_type=piece_type.id ";
// SELECT 
$champAffiche = array(
    'piece.id as "'.__("id").'"',
    'piece.libelle as "'.__("libelle").'"',
    'piece_type.libelle as "'.__("piece_type").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'piece.mouvement as "'.__("mouvement").'"',
    'piece.fichier as "'.__("fichier").'"',
    'piece.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'piece.id as "'.__("id").'"',
    'piece.libelle as "'.__("libelle").'"',
    'piece_type.libelle as "'.__("piece_type").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY piece.libelle ASC NULLS LAST";
$edition="piece";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (piece.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "mouvement" => array("mouvement", "inscription", "modification", "radiation", ),
    "om_collectivite" => array("om_collectivite", ),
    "piece_type" => array("piece_type", ),
);
// Filtre listing sous formulaire - mouvement
if (in_array($retourformulaire, $foreign_keys_extended["mouvement"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (piece.mouvement = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (piece.om_collectivite = '".$_SESSION["collectivite"]."') AND (piece.mouvement = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (piece.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (piece.om_collectivite = '".$_SESSION["collectivite"]."') AND (piece.om_collectivite = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - piece_type
if (in_array($retourformulaire, $foreign_keys_extended["piece_type"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (piece.piece_type = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (piece.om_collectivite = '".$_SESSION["collectivite"]."') AND (piece.piece_type = ".intval($idxformulaire).") ";
    }
}

