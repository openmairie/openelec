<?php
//$Id$ 
//gen openMairie le 18/02/2019 08:12

$DEBUG=0;
$serie=15;
$ent = __("-")." -> ".__("Notification");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."reu_notification";
// SELECT 
$champAffiche = array(
    'reu_notification.id as "'.__("id").'"',
    'reu_notification.date_de_creation as "'.__("date_de_creation").'"',
    'reu_notification.libelle as "'.__("libelle").'"',
    'reu_notification.id_demande as "'.__("id_demande").'"',
    'reu_notification.id_electeur as "'.__("id_electeur").'"',
    "case reu_notification.lu when 't' then 'Oui' else 'Non' end as \"".__("lu")."\"",
    'reu_notification.lien_uri as "'.__("lien_uri").'"',
    'reu_notification.resultat_de_notification as "'.__("resultat_de_notification").'"',
    'reu_notification.type_de_notification as "'.__("type_de_notification").'"',
    'reu_notification.date_de_collecte as "'.__("date_de_collecte").'"',
    "case reu_notification.traitee when 't' then 'Oui' else 'Non' end as \"".__("traitee")."\"",
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'reu_notification.om_collectivite as "'.__("om_collectivite").'"',
    'reu_notification.rapport_traitement as "'.__("rapport_traitement").'"',
    );
//
$champRecherche = array(
    'reu_notification.id as "'.__("id").'"',
    'reu_notification.libelle as "'.__("libelle").'"',
    'reu_notification.id_demande as "'.__("id_demande").'"',
    'reu_notification.id_electeur as "'.__("id_electeur").'"',
    'reu_notification.lien_uri as "'.__("lien_uri").'"',
    'reu_notification.resultat_de_notification as "'.__("resultat_de_notification").'"',
    'reu_notification.type_de_notification as "'.__("type_de_notification").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY reu_notification.libelle ASC NULLS LAST";
$edition="reu_notification";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (reu_notification.om_collectivite = '".$_SESSION["collectivite"]."') ";
}

