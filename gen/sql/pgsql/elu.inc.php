<?php
//$Id$ 
//gen openMairie le 18/02/2022 00:25

$DEBUG=0;
$serie=15;
$ent = __("Scrutins")." -> ".__("Composition des bureaux")." -> ".__("Élus");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."elu";
// SELECT 
$champAffiche = array(
    'elu.id as "'.__("id").'"',
    'elu.nom as "'.__("nom").'"',
    'elu.prenom as "'.__("prenom").'"',
    'elu.nomjf as "'.__("nomjf").'"',
    'to_char(elu.date_naissance ,\'DD/MM/YYYY\') as "'.__("date_naissance").'"',
    'elu.lieu_naissance as "'.__("lieu_naissance").'"',
    'elu.adresse as "'.__("adresse").'"',
    'elu.cp as "'.__("cp").'"',
    'elu.ville as "'.__("ville").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'elu.id as "'.__("id").'"',
    'elu.nom as "'.__("nom").'"',
    'elu.prenom as "'.__("prenom").'"',
    'elu.nomjf as "'.__("nomjf").'"',
    'elu.lieu_naissance as "'.__("lieu_naissance").'"',
    'elu.adresse as "'.__("adresse").'"',
    'elu.cp as "'.__("cp").'"',
    'elu.ville as "'.__("ville").'"',
    );
$tri="ORDER BY elu.nom ASC NULLS LAST";
$edition="elu";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'affectation',
);

