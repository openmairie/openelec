<?php
//$Id$ 
//gen openMairie le 06/06/2018 11:20

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Tables de références")." -> ".__("Communes");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."commune
    LEFT JOIN ".DB_PREFIXE."departement 
        ON commune.code_departement=departement.code ";
// SELECT 
$champAffiche = array(
    'commune.code as "'.__("code").'"',
    'commune.code_commune as "'.__("code_commune").'"',
    'commune.libelle_commune as "'.__("libelle_commune").'"',
    'departement.libelle_departement as "'.__("code_departement").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'commune.code as "'.__("code").'"',
    'commune.code_commune as "'.__("code_commune").'"',
    'commune.libelle_commune as "'.__("libelle_commune").'"',
    'departement.libelle_departement as "'.__("code_departement").'"',
    );
$tri="ORDER BY commune.code_commune ASC NULLS LAST";
$edition="commune";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "departement" => array("departement", ),
);
// Filtre listing sous formulaire - departement
if (in_array($retourformulaire, $foreign_keys_extended["departement"])) {
    $selection = " WHERE (commune.code_departement = '".$f->db->escapeSimple($idxformulaire)."') ";
}

