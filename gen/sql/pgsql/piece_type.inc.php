<?php
//$Id$ 
//gen openMairie le 21/01/2019 14:25

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("piece_type");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."piece_type";
// SELECT 
$champAffiche = array(
    'piece_type.id as "'.__("id").'"',
    'piece_type.libelle as "'.__("libelle").'"',
    'piece_type.code as "'.__("code").'"',
    'to_char(piece_type.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(piece_type.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
    );
//
$champNonAffiche = array(
    'piece_type.description as "'.__("description").'"',
    );
//
$champRecherche = array(
    'piece_type.id as "'.__("id").'"',
    'piece_type.libelle as "'.__("libelle").'"',
    'piece_type.code as "'.__("code").'"',
    );
$tri="ORDER BY piece_type.libelle ASC NULLS LAST";
$edition="piece_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((piece_type.om_validite_debut IS NULL AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE)) OR (piece_type.om_validite_debut <= CURRENT_DATE AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((piece_type.om_validite_debut IS NULL AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE)) OR (piece_type.om_validite_debut <= CURRENT_DATE AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'piece',
);

