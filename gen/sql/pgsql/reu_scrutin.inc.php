<?php
//$Id$ 
//gen openMairie le 25/09/2023 18:16

$DEBUG=0;
$serie=15;
$ent = __("Traitement")." -> ".__("Élections");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."reu_scrutin
    LEFT JOIN ".DB_PREFIXE."arrets_liste 
        ON reu_scrutin.arrets_liste=arrets_liste.arrets_liste 
    LEFT JOIN ".DB_PREFIXE."canton 
        ON reu_scrutin.canton=canton.id 
    LEFT JOIN ".DB_PREFIXE."circonscription as circonscription2 
        ON reu_scrutin.circonscription_legislative=circonscription2.id 
    LEFT JOIN ".DB_PREFIXE."circonscription as circonscription3 
        ON reu_scrutin.circonscription_metropolitaine=circonscription3.id 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON reu_scrutin.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'reu_scrutin.id as "'.__("id").'"',
    'reu_scrutin.type as "'.__("type").'"',
    "case reu_scrutin.partiel when 't' then 'Oui' else 'Non' end as \"".__("partiel")."\"",
    'reu_scrutin.libelle as "'.__("libelle").'"',
    'to_char(reu_scrutin.date_tour1 ,\'DD/MM/YYYY\') as "'.__("date_tour1").'"',
    'to_char(reu_scrutin.date_tour2 ,\'DD/MM/YYYY\') as "'.__("date_tour2").'"',
    'to_char(reu_scrutin.date_debut ,\'DD/MM/YYYY\') as "'.__("date_debut").'"',
    'to_char(reu_scrutin.date_l30 ,\'DD/MM/YYYY\') as "'.__("date_l30").'"',
    'to_char(reu_scrutin.date_fin ,\'DD/MM/YYYY\') as "'.__("date_fin").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'reu_scrutin.zone as "'.__("zone").'"',
    'reu_scrutin.j5_livrable_demande_id as "'.__("j5_livrable_demande_id").'"',
    'reu_scrutin.j5_livrable_demande_date as "'.__("j5_livrable_demande_date").'"',
    'reu_scrutin.j5_livrable as "'.__("j5_livrable").'"',
    'reu_scrutin.j5_livrable_date as "'.__("j5_livrable_date").'"',
    'reu_scrutin.emarge_livrable_demande_id as "'.__("emarge_livrable_demande_id").'"',
    'reu_scrutin.emarge_livrable_demande_date as "'.__("emarge_livrable_demande_date").'"',
    'reu_scrutin.emarge_livrable as "'.__("emarge_livrable").'"',
    'reu_scrutin.emarge_livrable_date as "'.__("emarge_livrable_date").'"',
    'reu_scrutin.om_collectivite as "'.__("om_collectivite").'"',
    'reu_scrutin.referentiel_id as "'.__("referentiel_id").'"',
    'reu_scrutin.canton as "'.__("canton").'"',
    'reu_scrutin.circonscription_legislative as "'.__("circonscription_legislative").'"',
    'reu_scrutin.circonscription_metropolitaine as "'.__("circonscription_metropolitaine").'"',
    'reu_scrutin.arrets_liste as "'.__("arrets_liste").'"',
    );
//
$champRecherche = array(
    'reu_scrutin.id as "'.__("id").'"',
    'reu_scrutin.type as "'.__("type").'"',
    'reu_scrutin.libelle as "'.__("libelle").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY reu_scrutin.libelle ASC NULLS LAST";
$edition="reu_scrutin";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (reu_scrutin.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "arrets_liste" => array("arrets_liste", ),
    "canton" => array("canton", ),
    "circonscription" => array("circonscription", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - arrets_liste
if (in_array($retourformulaire, $foreign_keys_extended["arrets_liste"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (reu_scrutin.arrets_liste = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (reu_scrutin.om_collectivite = '".$_SESSION["collectivite"]."') AND (reu_scrutin.arrets_liste = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - canton
if (in_array($retourformulaire, $foreign_keys_extended["canton"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (reu_scrutin.canton = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (reu_scrutin.om_collectivite = '".$_SESSION["collectivite"]."') AND (reu_scrutin.canton = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - circonscription
if (in_array($retourformulaire, $foreign_keys_extended["circonscription"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (reu_scrutin.circonscription_legislative = ".intval($idxformulaire)." OR reu_scrutin.circonscription_metropolitaine = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (reu_scrutin.om_collectivite = '".$_SESSION["collectivite"]."') AND (reu_scrutin.circonscription_legislative = ".intval($idxformulaire)." OR reu_scrutin.circonscription_metropolitaine = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (reu_scrutin.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (reu_scrutin.om_collectivite = '".$_SESSION["collectivite"]."') AND (reu_scrutin.om_collectivite = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'composition_scrutin',
);

