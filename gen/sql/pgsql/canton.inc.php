<?php
//$Id$ 
//gen openMairie le 17/02/2022 17:25

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Découpage")." -> ".__("Cantons");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."canton";
// SELECT 
$champAffiche = array(
    'canton.id as "'.__("id").'"',
    'canton.code as "'.__("code").'"',
    'canton.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'canton.id as "'.__("id").'"',
    'canton.code as "'.__("code").'"',
    'canton.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY canton.libelle ASC NULLS LAST";
$edition="canton";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'bureau',
    'parametrage_nb_jures',
    'reu_scrutin',
);

