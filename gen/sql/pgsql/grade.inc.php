<?php
//$Id$ 
//gen openMairie le 15/02/2022 17:28

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Composition des bureaux")." -> ".__("Grades");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."grade";
// SELECT 
$champAffiche = array(
    'grade.grade as "'.__("grade").'"',
    'grade.libelle as "'.__("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'grade.grade as "'.__("grade").'"',
    'grade.libelle as "'.__("libelle").'"',
    );
$tri="ORDER BY grade.libelle ASC NULLS LAST";
$edition="grade";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'agent',
);

