<?php
//$Id$ 
//gen openMairie le 15/02/2022 09:09

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("candidat");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."candidat
    LEFT JOIN ".DB_PREFIXE."composition_scrutin 
        ON candidat.composition_scrutin=composition_scrutin.id ";
// SELECT 
$champAffiche = array(
    'candidat.id as "'.__("id").'"',
    'candidat.nom as "'.__("nom").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'candidat.id as "'.__("id").'"',
    'candidat.nom as "'.__("nom").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    );
$tri="ORDER BY candidat.nom ASC NULLS LAST";
$edition="candidat";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "composition_scrutin" => array("composition_scrutin", ),
);
// Filtre listing sous formulaire - composition_scrutin
if (in_array($retourformulaire, $foreign_keys_extended["composition_scrutin"])) {
    $selection = " WHERE (candidat.composition_scrutin = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'affectation',
);

