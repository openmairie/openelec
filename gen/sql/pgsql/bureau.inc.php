<?php
//$Id$ 
//gen openMairie le 18/02/2022 03:08

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Découpage")." -> ".__("Bureaux");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."bureau
    LEFT JOIN ".DB_PREFIXE."canton 
        ON bureau.canton=canton.id 
    LEFT JOIN ".DB_PREFIXE."circonscription 
        ON bureau.circonscription=circonscription.id 
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON bureau.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'bureau.id as "'.__("id").'"',
    'bureau.code as "'.__("code").'"',
    'bureau.libelle as "'.__("libelle").'"',
    'canton.libelle as "'.__("canton").'"',
    'circonscription.libelle as "'.__("circonscription").'"',
    'bureau.referentiel_id as "'.__("referentiel_id").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'bureau.adresse1 as "'.__("adresse1").'"',
    'bureau.adresse2 as "'.__("adresse2").'"',
    'bureau.adresse3 as "'.__("adresse3").'"',
    'bureau.om_collectivite as "'.__("om_collectivite").'"',
    'bureau.surcharge_adresse_carte_electorale as "'.__("surcharge_adresse_carte_electorale").'"',
    'bureau.adresse_numero_voie as "'.__("adresse_numero_voie").'"',
    'bureau.adresse_libelle_voie as "'.__("adresse_libelle_voie").'"',
    'bureau.adresse_complement1 as "'.__("adresse_complement1").'"',
    'bureau.adresse_complement2 as "'.__("adresse_complement2").'"',
    'bureau.adresse_lieu_dit as "'.__("adresse_lieu_dit").'"',
    'bureau.adresse_code_postal as "'.__("adresse_code_postal").'"',
    'bureau.adresse_ville as "'.__("adresse_ville").'"',
    'bureau.adresse_pays as "'.__("adresse_pays").'"',
    );
//
$champRecherche = array(
    'bureau.id as "'.__("id").'"',
    'bureau.code as "'.__("code").'"',
    'bureau.libelle as "'.__("libelle").'"',
    'canton.libelle as "'.__("canton").'"',
    'circonscription.libelle as "'.__("circonscription").'"',
    'bureau.referentiel_id as "'.__("referentiel_id").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY bureau.libelle ASC NULLS LAST";
$edition="bureau";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (bureau.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "canton" => array("canton", ),
    "circonscription" => array("circonscription", ),
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - canton
if (in_array($retourformulaire, $foreign_keys_extended["canton"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (bureau.canton = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (bureau.om_collectivite = '".$_SESSION["collectivite"]."') AND (bureau.canton = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - circonscription
if (in_array($retourformulaire, $foreign_keys_extended["circonscription"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (bureau.circonscription = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (bureau.om_collectivite = '".$_SESSION["collectivite"]."') AND (bureau.circonscription = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (bureau.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (bureau.om_collectivite = '".$_SESSION["collectivite"]."') AND (bureau.om_collectivite = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'affectation',
    'candidature',
    'composition_bureau',
    'decoupage',
    'electeur',
    'mouvement',
    'numerobureau',
);

