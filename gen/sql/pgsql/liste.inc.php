<?php
//$Id$ 
//gen openMairie le 16/04/2019 12:55

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Métier")." -> ".__("Listes");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."liste";
// SELECT 
$champAffiche = array(
    'liste.liste as "'.__("liste").'"',
    'liste.libelle_liste as "'.__("libelle_liste").'"',
    'liste.liste_insee as "'.__("liste_insee").'"',
    "case liste.officielle when 't' then 'Oui' else 'Non' end as \"".__("officielle")."\"",
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'liste.liste as "'.__("liste").'"',
    'liste.libelle_liste as "'.__("libelle_liste").'"',
    'liste.liste_insee as "'.__("liste_insee").'"',
    );
$tri="ORDER BY liste.libelle_liste ASC NULLS LAST";
$edition="liste";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'electeur',
    'mouvement',
    'numerobureau',
);

