<?php
//$Id$ 
//gen openMairie le 02/11/2023 12:41

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("arrets_liste");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."arrets_liste
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON arrets_liste.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'arrets_liste.arrets_liste as "'.__("arrets_liste").'"',
    'arrets_liste.livrable_demande_id as "'.__("livrable_demande_id").'"',
    'to_char(arrets_liste.livrable_demande_date ,\'DD/MM/YYYY\') as "'.__("livrable_demande_date").'"',
    'arrets_liste.livrable as "'.__("livrable").'"',
    'to_char(arrets_liste.livrable_date ,\'DD/MM/YYYY\') as "'.__("livrable_date").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'arrets_liste.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'arrets_liste.arrets_liste as "'.__("arrets_liste").'"',
    'arrets_liste.livrable_demande_id as "'.__("livrable_demande_id").'"',
    'arrets_liste.livrable as "'.__("livrable").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY arrets_liste.livrable_demande_id ASC NULLS LAST";
$edition="arrets_liste";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (arrets_liste.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (arrets_liste.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (arrets_liste.om_collectivite = '".$_SESSION["collectivite"]."') AND (arrets_liste.om_collectivite = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'reu_scrutin',
);

