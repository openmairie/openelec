<?php
//$Id$ 
//gen openMairie le 18/02/2022 03:08

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("composition_bureau");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."composition_bureau
    LEFT JOIN ".DB_PREFIXE."bureau 
        ON composition_bureau.bureau=bureau.id 
    LEFT JOIN ".DB_PREFIXE."composition_scrutin 
        ON composition_bureau.composition_scrutin=composition_scrutin.id ";
// SELECT 
$champAffiche = array(
    'composition_bureau.id as "'.__("id").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    'bureau.libelle as "'.__("bureau").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'composition_bureau.id as "'.__("id").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    'bureau.libelle as "'.__("bureau").'"',
    );
$tri="ORDER BY composition_scrutin.libelle ASC NULLS LAST";
$edition="composition_bureau";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "bureau" => array("bureau", ),
    "composition_scrutin" => array("composition_scrutin", ),
);
// Filtre listing sous formulaire - bureau
if (in_array($retourformulaire, $foreign_keys_extended["bureau"])) {
    $selection = " WHERE (composition_bureau.bureau = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - composition_scrutin
if (in_array($retourformulaire, $foreign_keys_extended["composition_scrutin"])) {
    $selection = " WHERE (composition_bureau.composition_scrutin = ".intval($idxformulaire).") ";
}

