<?php
//$Id$ 
//gen openMairie le 06/06/2018 11:20

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Métier")." -> ".__("Types de mouvements");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."param_mouvement";
// SELECT 
$champAffiche = array(
    'param_mouvement.code as "'.__("code").'"',
    'param_mouvement.libelle as "'.__("libelle").'"',
    'param_mouvement.typecat as "'.__("typecat").'"',
    'param_mouvement.effet as "'.__("effet").'"',
    'param_mouvement.cnen as "'.__("cnen").'"',
    'param_mouvement.codeinscription as "'.__("codeinscription").'"',
    'param_mouvement.coderadiation as "'.__("coderadiation").'"',
    'param_mouvement.edition_carte_electeur as "'.__("edition_carte_electeur").'"',
    'param_mouvement.insee_import_radiation as "'.__("insee_import_radiation").'"',
    'to_char(param_mouvement.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(param_mouvement.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'param_mouvement.code as "'.__("code").'"',
    'param_mouvement.libelle as "'.__("libelle").'"',
    'param_mouvement.typecat as "'.__("typecat").'"',
    'param_mouvement.effet as "'.__("effet").'"',
    'param_mouvement.cnen as "'.__("cnen").'"',
    'param_mouvement.codeinscription as "'.__("codeinscription").'"',
    'param_mouvement.coderadiation as "'.__("coderadiation").'"',
    'param_mouvement.edition_carte_electeur as "'.__("edition_carte_electeur").'"',
    'param_mouvement.insee_import_radiation as "'.__("insee_import_radiation").'"',
    );
$tri="ORDER BY param_mouvement.libelle ASC NULLS LAST";
$edition="param_mouvement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((param_mouvement.om_validite_debut IS NULL AND (param_mouvement.om_validite_fin IS NULL OR param_mouvement.om_validite_fin > CURRENT_DATE)) OR (param_mouvement.om_validite_debut <= CURRENT_DATE AND (param_mouvement.om_validite_fin IS NULL OR param_mouvement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((param_mouvement.om_validite_debut IS NULL AND (param_mouvement.om_validite_fin IS NULL OR param_mouvement.om_validite_fin > CURRENT_DATE)) OR (param_mouvement.om_validite_debut <= CURRENT_DATE AND (param_mouvement.om_validite_fin IS NULL OR param_mouvement.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

