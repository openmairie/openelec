<?php
//$Id$ 
//gen openMairie le 06/06/2018 15:31

$DEBUG=0;
$serie=15;
$ent = __("-")." -> ".__("Membres de la commission");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."membre_commission
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON membre_commission.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'membre_commission.id as "'.__("id").'"',
    'membre_commission.civilite as "'.__("civilite").'"',
    'membre_commission.nom as "'.__("nom").'"',
    'membre_commission.prenom as "'.__("prenom").'"',
    'membre_commission.adresse as "'.__("adresse").'"',
    'membre_commission.complement as "'.__("complement").'"',
    'membre_commission.cp as "'.__("cp").'"',
    'membre_commission.ville as "'.__("ville").'"',
    'membre_commission.cedex as "'.__("cedex").'"',
    'membre_commission.bp as "'.__("bp").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'membre_commission.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'membre_commission.id as "'.__("id").'"',
    'membre_commission.civilite as "'.__("civilite").'"',
    'membre_commission.nom as "'.__("nom").'"',
    'membre_commission.prenom as "'.__("prenom").'"',
    'membre_commission.adresse as "'.__("adresse").'"',
    'membre_commission.complement as "'.__("complement").'"',
    'membre_commission.cp as "'.__("cp").'"',
    'membre_commission.ville as "'.__("ville").'"',
    'membre_commission.cedex as "'.__("cedex").'"',
    'membre_commission.bp as "'.__("bp").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY membre_commission.civilite ASC NULLS LAST";
$edition="membre_commission";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (membre_commission.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (membre_commission.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (membre_commission.om_collectivite = '".$_SESSION["collectivite"]."') AND (membre_commission.om_collectivite = ".intval($idxformulaire).") ";
    }
}

