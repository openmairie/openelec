<?php
//$Id$ 
//gen openMairie le 15/02/2022 09:09

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("affectation");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."affectation
    LEFT JOIN ".DB_PREFIXE."bureau 
        ON affectation.bureau=bureau.id 
    LEFT JOIN ".DB_PREFIXE."candidat 
        ON affectation.candidat=candidat.id 
    LEFT JOIN ".DB_PREFIXE."composition_scrutin 
        ON affectation.composition_scrutin=composition_scrutin.id 
    LEFT JOIN ".DB_PREFIXE."elu 
        ON affectation.elu=elu.id 
    LEFT JOIN ".DB_PREFIXE."periode 
        ON affectation.periode=periode.periode 
    LEFT JOIN ".DB_PREFIXE."poste 
        ON affectation.poste=poste.poste ";
// SELECT 
$champAffiche = array(
    'affectation.id as "'.__("id").'"',
    'elu.nom as "'.__("elu").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    'periode.libelle as "'.__("periode").'"',
    'poste.libelle as "'.__("poste").'"',
    'bureau.libelle as "'.__("bureau").'"',
    "case affectation.decision when 't' then 'Oui' else 'Non' end as \"".__("decision")."\"",
    'candidat.nom as "'.__("candidat").'"',
    );
//
$champNonAffiche = array(
    'affectation.note as "'.__("note").'"',
    );
//
$champRecherche = array(
    'affectation.id as "'.__("id").'"',
    'elu.nom as "'.__("elu").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    'periode.libelle as "'.__("periode").'"',
    'poste.libelle as "'.__("poste").'"',
    'bureau.libelle as "'.__("bureau").'"',
    'candidat.nom as "'.__("candidat").'"',
    );
$tri="ORDER BY elu.nom ASC NULLS LAST";
$edition="affectation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "bureau" => array("bureau", ),
    "candidat" => array("candidat", ),
    "composition_scrutin" => array("composition_scrutin", ),
    "elu" => array("elu", ),
    "periode" => array("periode", ),
    "poste" => array("poste", ),
);
// Filtre listing sous formulaire - bureau
if (in_array($retourformulaire, $foreign_keys_extended["bureau"])) {
    $selection = " WHERE (affectation.bureau = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - candidat
if (in_array($retourformulaire, $foreign_keys_extended["candidat"])) {
    $selection = " WHERE (affectation.candidat = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - composition_scrutin
if (in_array($retourformulaire, $foreign_keys_extended["composition_scrutin"])) {
    $selection = " WHERE (affectation.composition_scrutin = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - elu
if (in_array($retourformulaire, $foreign_keys_extended["elu"])) {
    $selection = " WHERE (affectation.elu = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - periode
if (in_array($retourformulaire, $foreign_keys_extended["periode"])) {
    $selection = " WHERE (affectation.periode = '".$f->db->escapeSimple($idxformulaire)."') ";
}
// Filtre listing sous formulaire - poste
if (in_array($retourformulaire, $foreign_keys_extended["poste"])) {
    $selection = " WHERE (affectation.poste = '".$f->db->escapeSimple($idxformulaire)."') ";
}

