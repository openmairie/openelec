<?php
//$Id$ 
//gen openMairie le 16/04/2019 12:55

$DEBUG=0;
$serie=15;
$ent = __("application")." -> ".__("archive");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."archive
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON archive.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'archive.id as "'.__("id").'"',
    'archive.types as "'.__("types").'"',
    'archive.electeur_id as "'.__("electeur_id").'"',
    'archive.liste as "'.__("liste").'"',
    'archive.bureau_de_vote_code as "'.__("bureau_de_vote_code").'"',
    'archive.bureauforce as "'.__("bureauforce").'"',
    'archive.numero_bureau as "'.__("numero_bureau").'"',
    'to_char(archive.date_modif ,\'DD/MM/YYYY\') as "'.__("date_modif").'"',
    'archive.utilisateur as "'.__("utilisateur").'"',
    'archive.civilite as "'.__("civilite").'"',
    'archive.sexe as "'.__("sexe").'"',
    'archive.nom as "'.__("nom").'"',
    'archive.nom_usage as "'.__("nom_usage").'"',
    'archive.prenom as "'.__("prenom").'"',
    'to_char(archive.date_naissance ,\'DD/MM/YYYY\') as "'.__("date_naissance").'"',
    'archive.code_departement_naissance as "'.__("code_departement_naissance").'"',
    'archive.libelle_departement_naissance as "'.__("libelle_departement_naissance").'"',
    'archive.code_lieu_de_naissance as "'.__("code_lieu_de_naissance").'"',
    'archive.libelle_lieu_de_naissance as "'.__("libelle_lieu_de_naissance").'"',
    'archive.code_nationalite as "'.__("code_nationalite").'"',
    'archive.code_voie as "'.__("code_voie").'"',
    'archive.libelle_voie as "'.__("libelle_voie").'"',
    'archive.numero_habitation as "'.__("numero_habitation").'"',
    'archive.complement_numero as "'.__("complement_numero").'"',
    'archive.complement as "'.__("complement").'"',
    'archive.provenance as "'.__("provenance").'"',
    'archive.libelle_provenance as "'.__("libelle_provenance").'"',
    'archive.ancien_bureau_de_vote_code as "'.__("ancien_bureau_de_vote_code").'"',
    'archive.observation as "'.__("observation").'"',
    'archive.resident as "'.__("resident").'"',
    'archive.adresse_resident as "'.__("adresse_resident").'"',
    'archive.complement_resident as "'.__("complement_resident").'"',
    'archive.cp_resident as "'.__("cp_resident").'"',
    'archive.ville_resident as "'.__("ville_resident").'"',
    'archive.tableau as "'.__("tableau").'"',
    'to_char(archive.date_tableau ,\'DD/MM/YYYY\') as "'.__("date_tableau").'"',
    'archive.envoi_cnen as "'.__("envoi_cnen").'"',
    'to_char(archive.date_cnen ,\'DD/MM/YYYY\') as "'.__("date_cnen").'"',
    'archive.mouvement as "'.__("mouvement").'"',
    'archive.typecat as "'.__("typecat").'"',
    'to_char(archive.date_mouvement ,\'DD/MM/YYYY\') as "'.__("date_mouvement").'"',
    'archive.etat as "'.__("etat").'"',
    'archive.telephone as "'.__("telephone").'"',
    'archive.courriel as "'.__("courriel").'"',
    'archive.bureau_de_vote_libelle as "'.__("bureau_de_vote_libelle").'"',
    'archive.ancien_bureau_de_vote_libelle as "'.__("ancien_bureau_de_vote_libelle").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'archive.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'archive.id as "'.__("id").'"',
    'archive.types as "'.__("types").'"',
    'archive.electeur_id as "'.__("electeur_id").'"',
    'archive.liste as "'.__("liste").'"',
    'archive.bureau_de_vote_code as "'.__("bureau_de_vote_code").'"',
    'archive.bureauforce as "'.__("bureauforce").'"',
    'archive.numero_bureau as "'.__("numero_bureau").'"',
    'archive.utilisateur as "'.__("utilisateur").'"',
    'archive.civilite as "'.__("civilite").'"',
    'archive.sexe as "'.__("sexe").'"',
    'archive.nom as "'.__("nom").'"',
    'archive.nom_usage as "'.__("nom_usage").'"',
    'archive.prenom as "'.__("prenom").'"',
    'archive.code_departement_naissance as "'.__("code_departement_naissance").'"',
    'archive.libelle_departement_naissance as "'.__("libelle_departement_naissance").'"',
    'archive.code_lieu_de_naissance as "'.__("code_lieu_de_naissance").'"',
    'archive.libelle_lieu_de_naissance as "'.__("libelle_lieu_de_naissance").'"',
    'archive.code_nationalite as "'.__("code_nationalite").'"',
    'archive.code_voie as "'.__("code_voie").'"',
    'archive.libelle_voie as "'.__("libelle_voie").'"',
    'archive.numero_habitation as "'.__("numero_habitation").'"',
    'archive.complement_numero as "'.__("complement_numero").'"',
    'archive.complement as "'.__("complement").'"',
    'archive.provenance as "'.__("provenance").'"',
    'archive.libelle_provenance as "'.__("libelle_provenance").'"',
    'archive.ancien_bureau_de_vote_code as "'.__("ancien_bureau_de_vote_code").'"',
    'archive.observation as "'.__("observation").'"',
    'archive.resident as "'.__("resident").'"',
    'archive.adresse_resident as "'.__("adresse_resident").'"',
    'archive.complement_resident as "'.__("complement_resident").'"',
    'archive.cp_resident as "'.__("cp_resident").'"',
    'archive.ville_resident as "'.__("ville_resident").'"',
    'archive.tableau as "'.__("tableau").'"',
    'archive.envoi_cnen as "'.__("envoi_cnen").'"',
    'archive.mouvement as "'.__("mouvement").'"',
    'archive.typecat as "'.__("typecat").'"',
    'archive.etat as "'.__("etat").'"',
    'archive.telephone as "'.__("telephone").'"',
    'archive.courriel as "'.__("courriel").'"',
    'archive.bureau_de_vote_libelle as "'.__("bureau_de_vote_libelle").'"',
    'archive.ancien_bureau_de_vote_libelle as "'.__("ancien_bureau_de_vote_libelle").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY archive.types ASC NULLS LAST";
$edition="archive";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (archive.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (archive.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (archive.om_collectivite = '".$_SESSION["collectivite"]."') AND (archive.om_collectivite = ".intval($idxformulaire).") ";
    }
}

