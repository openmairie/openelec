<?php
//$Id$ 
//gen openMairie le 07/02/2019 19:16

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Tables de références")." -> ".__("Nationalités");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."nationalite";
// SELECT 
$champAffiche = array(
    'nationalite.code as "'.__("code").'"',
    'nationalite.libelle_nationalite as "'.__("libelle_nationalite").'"',
    'to_char(nationalite.om_validite_debut ,\'DD/MM/YYYY\') as "'.__("om_validite_debut").'"',
    'to_char(nationalite.om_validite_fin ,\'DD/MM/YYYY\') as "'.__("om_validite_fin").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'nationalite.code as "'.__("code").'"',
    'nationalite.libelle_nationalite as "'.__("libelle_nationalite").'"',
    );
$tri="ORDER BY nationalite.libelle_nationalite ASC NULLS LAST";
$edition="nationalite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((nationalite.om_validite_debut IS NULL AND (nationalite.om_validite_fin IS NULL OR nationalite.om_validite_fin > CURRENT_DATE)) OR (nationalite.om_validite_debut <= CURRENT_DATE AND (nationalite.om_validite_fin IS NULL OR nationalite.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((nationalite.om_validite_debut IS NULL AND (nationalite.om_validite_fin IS NULL OR nationalite.om_validite_fin > CURRENT_DATE)) OR (nationalite.om_validite_debut <= CURRENT_DATE AND (nationalite.om_validite_fin IS NULL OR nationalite.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite)
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'electeur',
    'mouvement',
);

