<?php
//$Id$ 
//gen openMairie le 04/11/2021 16:48

$DEBUG=0;
$serie=15;
$ent = __("Administration & Paramétrage")." -> ".__("Métier")." -> ".__("Traces");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."trace
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON trace.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'trace.id as "'.__("id").'"',
    'to_char(trace.creationdate ,\'DD/MM/YYYY\') as "'.__("creationdate").'"',
    'trace.creationtime as "'.__("creationtime").'"',
    'trace.type as "'.__("type").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
//
$champNonAffiche = array(
    'trace.trace as "'.__("trace").'"',
    'trace.om_collectivite as "'.__("om_collectivite").'"',
    );
//
$champRecherche = array(
    'trace.id as "'.__("id").'"',
    'trace.type as "'.__("type").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
$tri="ORDER BY trace.creationdate ASC NULLS LAST";
$edition="trace";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (trace.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (trace.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (trace.om_collectivite = '".$_SESSION["collectivite"]."') AND (trace.om_collectivite = ".intval($idxformulaire).") ";
    }
}

