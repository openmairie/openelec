<?php
/**
 * Ce fichier permet de lister les statistiques disponibles
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
$f = new openelec(
    null,
    "statistiques",
    __("Statistiques")
);

/**
 *
 */
$statistiques = array (
    0 => array (
        'lien' => "mouvement_bureau",
        'titre' => __("Mouvements details par bureau"),
        'description' => __("Nombre d'inscription office (code insee 8), d'inscription nouvelle (code insee 1), et de deces (code insee D) par bureau pour la liste et la date de tableau en cours"),
        "href" => "../app/index.php?module=form&obj=mouvement&action=304&idx=0",
        "right" => "mouvement_edition_pdf__statistiques_mouvement_bureau",
    ),
    1 => array (
        'lien' => "mouvement_voie",
        'titre' => __("Mouvements details par voie"),
        'description' => __("Nombre d'inscription office (code insee 8), d'inscription nouvelle (code insee 1), et de deces (code insee D) par voie pour la liste et la date de tableau en cours"),
        "href" => "../app/index.php?module=form&obj=mouvement&action=302&idx=0",
        "right" => "mouvement_edition_pdf__statistiques_mouvement_voie",
    ),
    2 => array (
        'lien' => "electeur_age_bureau",
        'titre' => __("Electeurs par tranche d'age / details par bureau"),
        'description' => __("Nombre d'electeurs, par tranche d'age par bureau pour la liste en cours"),
        "href" => "../app/index.php?module=form&obj=electeur&action=401&idx=0",
        "right" => "electeur_edition_pdf__statistiques_electeur_age_bureau",
    ),
    3 => array (
        'lien' => "electeur_sexe_bureau",
        'titre' => __("Electeurs par sexe / details par bureau"),
        'description' => __("Nombre d'electeurs, d'hommes, de femmes par bureau pour la liste en cours"),
        "href" => "../app/index.php?module=form&obj=electeur&action=402&idx=0",
        "right" => "electeur_edition_pdf__statistiques_electeur_sexe_bureau",
    ),
    4 => array (
        'lien' => "electeur_sexe_nationalite",
        'titre' => __("Electeurs par sexe / details par nationalite"),
        'description' => __("Nombre d'electeurs, d'hommes, de femmes par nationalite pour la liste en cours"),
        "href" => "../app/index.php?module=form&obj=electeur&action=404&idx=0",
        "right" => "electeur_edition_pdf__statistiques_electeur_sexe_nationalite",
    ),
    5 => array (
        'lien' => "electeur_sexe_departementnaissance",
        'titre' => __("Electeurs par sexe / details par departement de naissance"),
        'description' => __("Nombre d'electeurs, d'hommes, de femmes par departement (ou pays) de naissance pour la liste en cours"),
        "href" => "../app/index.php?module=form&obj=electeur&action=403&idx=0",
        "right" => "electeur_edition_pdf__statistiques_electeur_sexe_departementnaissance",
    ),
    6 => array (
        'lien' => "electeur_sexe_voie",
        'titre' => __("Electeurs par sexe / details par voie"),
        'description' => __("Nombre d'electeurs, d'hommes, de femmes par voie pour la liste en cours"),
        "href" => "../app/index.php?module=form&obj=electeur&action=405&idx=0",
        "right" => "electeur_edition_pdf__statistiques_electeur_sexe_voie",
    ),
    7 => array (
        'lien' => "io_sexe",
        'titre' => __("Mouvements inscrits office (8) par sexe / details par bureau"),
        'description' => __("Nombre d'electeurs en inscrits d'office, d'hommes, de femmes par bureau"),
        "href" => "../app/index.php?module=form&obj=mouvement&action=303&idx=0",
        "right" => "mouvement_edition_pdf__statistiques_mouvement_io_sexe_bureau",
    ),
);

/**
 *
 */
$listing = "";
$odd = true;
foreach ($statistiques as $key => $s) {
    //
    if (array_key_exists("right", $s) === true
        && $f->isAccredited($s["right"]) !== true) {
        continue;
    }
    //
    $odd = !$odd;
    $listing .= sprintf(
        '<tr class="stats %1$s">
            <td class="titre">
                <a class="om-prev-icon statistiques-16" target="_blank" href="%2$s">%3$s</a>
            </td>
            <td class="description">
                %4$s
            </td>
        </tr>',
        ($odd === true ? "odd" : "even"),
        $s["href"],
        $s['titre'],
        $s['description']
    );
}
printf(
    '<table class="editions">%s</table>',
    $listing
);
