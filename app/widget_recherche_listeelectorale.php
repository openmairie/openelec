<?php
/**
 * WIDGET DASHBOARD - widget_recherche_listeelectorale.
 *
 * Recherche dans la liste électorale.
 *
 * L'objet de ce widget est de permettre la saisie d'un nom d'électeur à rechercher
 * pour accéder rapidement à sa fiche.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
if (isset($f) === false) {
    $f = new openelec(null);
}

/**
 * RECHERCHE GLOBALE
 */
if ($f->isAccredited("electeur_tab")) {
    echo "<span class=\"description\">";
    echo __("Saisir votre recherche");
    echo "</span>";
    echo "<div id=\"rechercheglobale\">\n";
    echo "<form name=\"f1\" id=\"f1\" method=\"post\" ";
    echo "action=\"".OM_ROUTE_TAB."&obj=electeur\">";
    echo "<input type=\"text\" id=\"dashboard_recherche\" class=\"champFormulaire\" value=\"\" name=\"recherche\" />";
    echo "<input type=\"submit\" id=\"search-submit\" class=\"om-search-button\" name=\"s1\" value=\"".__("Recherche")."\" />";
    echo "</form>";
    echo "</div>\n";
} else {
    //
    $widget_is_empty = true;
}
