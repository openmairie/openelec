<?php
/**
 * Ce script permet d'interfacer l'application.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
$flag = filter_input(INPUT_GET, 'module');
if (in_array($flag, array("login", "logout", )) === false) {
    $flag = "nohtml";
}
$f = new openelec($flag);
$f->view_main();
