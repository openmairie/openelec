<?php
/**
 * Ce fichier permet l'edition de documents par bureau
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
$f = new openelec(
    null,
    "bureau_edition",
    __("Editions par bureau")
);

/**
 * Description de la page
 */
$description = __("Cette interface vous permet d'editer les étiquettes d'électeur par bureau.");
$f->displayDescription($description);

/**
 *
 */
//
$documents = array(
    array(
        "title" => __("Etiquettes d'électeur"),
        "icon" => "etiquetteelecteur",
        "tooltip" => __("Etiquettes d'electeur"),
        "href" => "../app/index.php?module=form&obj=electeur&action=305&mode_edition=parbureau&bureau_code=",
        "id" => "action-edition-pdf-etiquetteelecteur-bureau-<bureau>",
        "right" => "electeur_edition_pdf__etiquette_electorale",
    ),
);

/**
 *
 */
//
$available_documents = array();
foreach ($documents as $doc) {
    if (array_key_exists("right", $doc) === true
        && $f->isAccredited($doc["right"]) !== true) {
        continue;
    }
    $available_documents[] = $doc;
}
//
$cells = "";
foreach ($available_documents as $doc) {
    $cells .= sprintf(
        '<th>&nbsp;%s</th>',
        $doc["title"]
    );
}
$head = sprintf(
    '<tr class="ui-tabs-nav ui-accordion ui-state-default tab-title"><td class="firstcol">&nbsp;</td>%s</tr>',
    $cells
);
//
$lines = "";
$class = "even";
foreach ($f->get_all__bureau__by_my_collectivite() as $bureau) {
    //
    $cells = "";
    $cpt = 1;
    foreach ($available_documents as $doc) {
        $cells .= sprintf(
            '<td>
                <a href="%1$s" target="_blank" id="%4$s">
                    <span class="om-icon om-icon-25 om-icon-fix %2$s-25" title="%3$s">%3$s</span>
                </a>
            </td>',
            $doc["href"].$bureau["code"],
            $doc["icon"],
            $doc["tooltip"],
            str_replace("<bureau>", $bureau["code"], $doc["id"])
        );
        $cpt++;
    }
    //
    ($class == "even" ? $class = "odd" : $class = "even");
    $lines .= sprintf(
        '<tr class="tab-data %1$s"><td>%2$s</td>%3$s</tr>',
        $class,
        $bureau["code"]." - ".$bureau["libelle"],
        $cells
    );
}
//
printf(
    '<div id="bureau_edition">
    <table class="tab-tab">
        <thead>%s</thead>
        <tbody>%s</tbody>
    </table>
</div>',
    $head,
    $lines
);
