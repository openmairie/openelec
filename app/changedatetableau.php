<?php
/**
 * Ce script permet d'interfacer le formulaire de changement de date de tableau.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
$f = new openelec(
    "nohtml",
    "datetableau_changer",
    __("Changement de la date de tableau")
);

//
if (isset ($_POST['changedatetableau_action_valid'])) {
    //
    $datetableau = $f->formatDate($_POST['datetableau'], false);
    //
    if ($datetableau == false) {
        // message 'erreur'
        $message_class = "error";
        $message = __("La date n'est pas valide.");
    } else {
        $ret = $f->insert_or_update_parameter("datetableau", $datetableau);
        if ($ret !== true) {
            //
            $message_class = "error";
            $message = __("Une erreur est survenue. Contactez votre administrateur.");
        } else {
            //
            if ($_SESSION['niveau'] == '2') {
                $cle = " om_parametre.libelle='datetableau' ";
                $valF["valeur"] = $datetableau;
                $res = $f->db->autoexecute(
                    sprintf('%1$s%2$s', DB_PREFIXE, "om_parametre"),
                    $valF,
                    DB_AUTOQUERY_UPDATE,
                    $cle
                );
                $f->addToLog(
                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "om_parametre")."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                    VERBOSE_MODE
                );
                $f->isDatabaseError($res);
            }
            //
            $message_class = "ok";
            $message = __("La date de tableau est changee.");
        }
    }
    // message
    $f->addToMessage($message_class, $message);
}

//
$f->getCollectivite();

//
$f->setFlag(null);
$f->display();

//
echo "<div class=\"instructions\">";
echo "<p>";
echo __("Ici vous pouvez mettre a jour votre date de tableau.");
echo "</p>";
echo "</div>";
//
echo "\n<div id=\"changedatetableauform\" class=\"formulaire\">\n";
//
echo "<form method=\"post\" id=\"changedatetableauform_form\" action=\"../app/changedatetableau.php\">\n";
//
echo "\t<div class=\"field\">\n";
echo "\t\t<label for=\"datetableau\">".__("Date de tableau")."</label>\n";
echo "\t\t<input type=\"text\" onchange=\"fdate(this)\" name=\"datetableau\" ";
echo "maxlength=\"10\" size=\"10\" class=\"champFormulaire datepicker\" ";
echo "tabindex=\"1\" size=\"15\" ";
echo "value=\"".$f->formatdate($f->getParameter("datetableau"), true)."\"/>\n";
echo "\t</div>\n";
//
echo "\t<div class=\"formControls\">\n";
echo "\t\t<input name=\"changedatetableau.action.valid\" tabindex=\"4\" value=\"".__("Valider")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
echo "<a class=\"retour\" title=\"".__("Retour")."\" ";
echo "href=\"".OM_ROUTE_DASHBOARD."\">";
echo __("Retour");
echo "</a>";
echo "\t</div>\n";
//
echo "</form>\n";
//
echo "</div>\n";
