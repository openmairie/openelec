/**
 * Ce script javascript ...
 *
 * @package openelec
 * @version SVN : $Id$
 */


/**
 * Au chargement de la page
 */
$(function() {
    /**
     * Gère le comportement de la page settings (administration et paramétrage).
     */
    handle_settings_page();
    // Autocomplete: cas des tables où on stocke ID + libellé d'un autre objet.
    // Et pas juste l'id, donc il faut dupliquer.
    // Listeners sur le champ id cachés, et pas sur le champs de recherche.
    // Sinon dans certains cas, le onchange n'est pas déclenché. P. ex. sur
    // certaines machines pendant les tests.
    // Voie dans classe mouvement et ses filles.
    $("#autocomplete-voie-id").on("change", function(event) {
        $("#libelle_voie").val($("#autocomplete-voie-search").val());
    });
    // Commune de provenance dans classe mouvement et ses filles.
    $("#autocomplete-commune-id").on("change", function(event) {
        // Extraction du libelle (suppression de l'id)
        var libelle = $("#autocomplete-commune-search").val().replace(/\d* \d* - /, "");
        $("#libelle_provenance").val(libelle);
    });
    //
    bind_traitement_tabs();
    // USERPAGE - Bind le lien (login) pour ouvir le dialog
    userpage_bind_link();
});

/**
 *
 */
function app_initialize_content(tinymce_load) {
    // Aide à la saisie sur tous les champs date : la saisie de la touche espace
    // insère la date du jour si le champ est vide
    $("input.datepicker").keypress(function(event) {
        if (event.which == 32) {
            if ($(this).val() == "") {
                var today = new Date().toLocaleDateString("fr");
                $(this).val(today);
            }
        }
    });
    //
    handle__widget_form__select_multiple_permissions();
    //
    bind__procuration__mandant_mandataire__form_widget();
    handle__procuration__mandant_mandataire__form_widget();
    bind__procuration__annulation_autorite_type__form_widget();
    handle__procuration__annulation_autorite_type__form_widget();
    bind__procuration__autorite_type__form_widget();
    handle__procuration__autorite_type__form_widget();
    bind__procuration__statut__form_widget();
    handle__procuration__statut__form_widget();
    //
    bind_inscription_search_form_widget();
    handle_inscription_search_form_widget();
    //
    bind_adresse_resident_form_widget();
    handle_adresse_resident_form_widget();
    //
    bind_naissance_form_widget();
    handle_naissance_form_widget();
    //
    bind_decoupage_form_widget();
    handle_decoupage_form_widget();
    // Focus sur le champ login au chargement de la page.
    cursorFocusAndGoToTop($('#login_form #login'));
    // Focus sur le champ de recherche simple sur la recherche avancée d'un listing.
    cursorFocusAndGoToTop($('#adv-search-classic-fields input[name="recherche"]'));
    // Focus sur le champ de recherche standard d'un listing.
    cursorFocusAndGoToTop($('div.tab-search input[name="recherche"]'));
    // Focus sur le champ de recherche sur le tableau de bord
    cursorFocusAndGoToTop($("#dashboard_recherche"));
    // Focus sur le champ de recherche nom dans le formulaire de recherche d'un électeur
    cursorFocusAndGoToTop($("#electeur_search_form #nom"));
    // Gestion du formulaire de changement de collectivité de travail
    handle_changecollectivite_form();
    // Gestion du formulaire de changement de liste de travail
    handle_changeliste_form();
    // Surcharge autocomplete framework
    handle_autocomplete_readonly();
    //
    $( ".tooltip" ).dialog({
        autoOpen: false,
        width: 460,
        show: "blind",
        hide: "explode",
        modal: true
    });
    $( ".tooltip-opener" ).click(function() {
        ;
        $("#"+$(this).attr("rel")).dialog( "open" );

        return false;
    });
    (function($){
        //Within this wrapper, $ is the jQuery namespace
        $.fn.bind_action_for_overlay = function(obj, width, height, callback, callbackParams) {
            if( typeof(width) == 'undefined' ){
                width = 'auto';
            }
            if( typeof(height) == 'undefined' ){
                height = 'auto';
            }
            if( typeof(callback) == 'undefined' ){
                callback = '';
            }
            if( typeof(callbackParams) == 'undefined' ){
                callbackParams = '';
            }

            // bind de la function passée en arg sur l'event click des actions portlet
            $(this).off("click").on("click", function(event) {
                //
                elem_href = $(this).attr('href');
                if (elem_href != '#') {
                    $(this).attr('data-href', elem_href);
                    $(this).attr('href', '#');
                }
                //
                popupIt(obj, $(this).attr('data-href'), width, height, callback, callbackParams);
            });
            return $(this);
        }
    })(jQuery);

    // Bind actions edit-jury depuis fiche de l'électeur
    $('a[id^=action-form-electeur][id$=-edit-jury]').each(function(){
        $(this).bind_action_for_overlay("electeur_jury");
    });
    //
    $("div.ui-dialog #sousform-electeur_jury a.retour").off('click').on('click', function(event) {
        $('div.ui-dialog #sousform-electeur_jury').dialog('close').remove();
        form_container_refresh("form");
        return false;
    });
    // Module REU - Formulaires overlay pour ugle et compte logiciel
    $('a[id^=action-reu-overlay-change-]').each(function(){
        $(this).bind_action_for_overlay("reu-change-form");
    });
    $("div.ui-dialog #sousform-reu-change-form a.retour").off('click').on('click', function(event) {
        $('div.ui-dialog #sousform-reu-change-form').dialog('close').remove();
        return false;
    });
    $('div.ui-dialog #sousform-reu-change-form').on('dialogclose', function(event) {
        window.location.reload();
        return false;
    });
    // Module Notification par mail - Formulaires overlay pour la sélection des profils
    $('a[id^=action-notification-select-profil]').each(function(){
        $(this).bind_action_for_overlay("select-profil-form");
    });
    $("div.ui-dialog #sousform-select-profil-form a.retour").off('click').on('click', function(event) {
        $('div.ui-dialog #sousform-select-profil-form').dialog('close').remove();
        return false;
    });
    $('div.ui-dialog #sousform-select-profil-form').on('dialogclose', function(event) {
        window.location.reload();
        return false;
    });
    // $('div.ui-dialog #sousform-select-profil-form').on('dialogclose', function(event) {
    //     window.location.reload();
    //     return false;
    // });
    // Navigation spéciale sur le traitement par lot multi
    handle_multi_traitement_par_lot();
    //
    if ($('form#reu_sync_l_e_reu_non_rattaches_bind_electeur div#autocomplete-electeur input#autocomplete-electeur-search').length > 0) {
        //
        form_action = $('form#reu_sync_l_e_reu_non_rattaches_bind_electeur').attr("action");
        regexp = /numero_d_electeur=(.*?)(?:\s|$)/g;
        match = regexp.exec(form_action);
        numero_d_electeur = match[1];

        link = $('span.form-snippet-autocomplete').attr('data-href')+"&numero_d_electeur="+numero_d_electeur;
        $('span.form-snippet-autocomplete').attr("data-href", link);
        $("#autocomplete-electeur-search").autocomplete( "option", "source", link);
    }
    // USERPAGE - Bind les liens identifiés pour ouvrir une popup
    userpage_bind_popup();
}

/**
 * Le focus sur un input scrolle de manière à montrer le champ sur lequel est fait le focus.
 * Dans notre cas nous avons besoin de focus au chargement de la page, il est donc compliqué
 * pour l'utilisateur de s'y retrouver si il ne voit pas le haut de la page.
 */
var cursorFocusAndGoToTop = function(elem) {
    if (elem.length === 0) {
        return;
    }
    var x = window.scrollX, y = window.scrollY;
    elem.focus();
    window.scrollTo(0, 0);
}

/**
 * Gère le comportement de la page settings (administration et paramétrage) :
 *  - autofocus sur le champ de recherche
 *  - comportement du livesearch
 */
 function handle_settings_page() {
    // Auto focus sur le champ de recherche au chargement de la page
    $('#settings-live-search #filter').focus();
    // Live search dans le contenu de la page settings
    $("#settings-live-search #filter").keyup(function(){
        var filter = $(this).val(), count = 0;
        $("div.list-group a").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }
        });
    });
}

function handle__widget_form__select_multiple_permissions() {
    //
    $(".field-type-select_multiple_permissions select#permissions").DualListBox({
        'json': false,
        'title': "éléments",
        'warning': "Cette action va déplacer beaucoup d'éléments et peut ralentir votre navigateur."
    });
    //
    if ($(".field-type-select_multiple_permissions").length === 0) {
        return;
    }
}

// Navigation spéciale sur le traitement par lot multi
function handle_multi_traitement_par_lot() {
    $("#sousform-multi_traitement_par_lot form").off('submit').on('submit', function(event) {
        affichersform("multi_traitement_par_lot", $(this).attr('action'), window.document.trtmulti);
        return false;
    });
    $("#sousform-multi_traitement_par_lot a.retour-submit").off('click').on('click', function(event) {
        affichersform("multi_traitement_par_lot", window.document.trtmulti.action, window.document.trtmulti);//
        return false;
    });
}

function handle_autocomplete_readonly() {
    $("input.autocomplete").each (function(){
        var obj = $(this).parents().attr("id").replace("autocomplete-","");
        var autocomplete_id = "autocomplete-"+obj;
        if ($("#"+autocomplete_id+"-id").val() == "") {
            $("#"+autocomplete_id+"-search").prop('readonly', false);
        } else {
            $("#"+autocomplete_id+"-search").prop('readonly', true);
        }
        $("#"+autocomplete_id+"-id").change(function(){
            if ($("#"+autocomplete_id+"-id").val() == "") {
                $("#"+autocomplete_id+"-search").prop('readonly', false);
                $("#"+autocomplete_id+"-search").focus();
            } else {
                $("#"+autocomplete_id+"-search").prop('readonly', true);
            }
        });
    });
}

/**
 * USERPAGE - Gère l'affichage d'une page de gestion des préférences utilisateur
 * depuis l'action 'login'.
 */
function userpage_bind_link() {
    $("#actions .actions-list-elem.login").on("click", function(event) {
        load_dialog_userpage();
    });
}
/**
 * USERPAGE - Certains liens doivent ouvrir une popup (OAUTH2 connexion iframe interdite)
 */
function userpage_bind_popup() {
    $("#userpage a.popup").on("click", function(event) {
        window.open($(this).attr("href"), 'userpage_popup','menubar=no, scrollbars=no, top=100, left=100, width=640, height=580');
        return false;
    });
}
/**
 * USERPAGE - Ouvre une fenêtre de dialogue et charge la vue userpage
 */
function load_dialog_userpage() {
    var dialog = $('<div id=\"userpage\" style="display:none"></div>').insertAfter('#footer');
    $.ajax({
        type: "GET",
        url: '../app/index.php?module=user',
        cache: false,
        success: function(html){
            dialog.empty();
            dialog.append(html);
            $(dialog).dialog({
                close: function(ev, ui) {
                    if (typeof(callback) === "function") {
                        callback(callbackParams);
                    }
                    $(this).remove();
                },
                resizable: false,
                modal: true,
                width: '800px',
                height: 'auto',
                position: 'center top',
            });
            om_initialize_content();
        },
        async : false
    });
    return false;
}
/**
 * USERPAGE - Recharge la vue userpage dans le conteneur dialog
 */
function reload_dialog_userpage() {
    var dialog = $("#userpage");
    dialog.empty();
    dialog.html(msg_loading);
    $.ajax({
        type: "GET",
        url: '../app/index.php?module=user',
        cache: false,
        success: function(html){
            dialog.empty();
            dialog.append(html);
            om_initialize_content();
        },
        async : false
    });
    return false;
}

function bind_traitement_tabs() {
    //
    var $tabs = $("#traitement-tabs").tabs({
        load: function(event, ui) {
            //
            var link = $.data(ui.tab, 'load.tabs');
            if ($("#sousform-href").length) {
                $("#sousform-href").attr("data-href", link);
            }
            om_initialize_content(true);
            return true;
        },
        select: function(event, ui) {
            // Suppression du contenu de l'onglet precedemment selectionne
            // #ui-tabs-X correspond uniquement aux ids des onglets charges
            // dynamiquement
            selectedTabIndex = $tabs.tabs('option', 'selected');
            selectedTabIndex = selectedTabIndex + 1;
            $("#ui-tabs-"+selectedTabIndex).empty();
            // Gestion de la recherche
            // Si le nouvel onglet clique est un onglet qui charge dynamiquement
            // son contenu
            var url = $.data(ui.tab, 'load.tabs');
            if (url) {
                // On affiche la recherche
                var recherchedyn = document.getElementById("recherchedyn");
                if (recherchedyn != null) {
                    var recherche = document.getElementById("recherchedyn").value;
                    url += "&recherche="+recherche;
                    $("#recherche_onglet").removeAttr("style");
                    $tabs.tabs("url", ui.index, url);
                }
            } else {
                // On cache la recherche
                $("#recherche_onglet").attr("style", "display:none;")
            }
            return true;
        },
        ajaxOptions: {
            error: function(xhr, status, index, anchor) {
                $(anchor.hash).html(msg_alert_error_tabs);
            }
        }
    });

}

/**
 * Permet de positionner sur le tableau de bord en javascript.
 */
function refresh_header_and_menu_and_dashboard() {
    url_dashboard = $("a.logo").attr('href');
    $.ajax({
        type: "GET",
        url: url_dashboard,
        async: false,
        success: function(html){
            container = '<div id="bigcontainer">'+html+'</div>';
            $("#menu").replaceWith($(container).find("div[id='menu']").get(0));
            $("#header").replaceWith($(container).find("div[id='header']").get(0));
            $("#menuopen_val").empty("");
            menu_bind_accordion();
            $("#content").replaceWith($(container).find("div[id='content']").get(0));
            om_initialize_content();
            window.history.pushState("","", url_dashboard);
        }
    });
}

/**
 * Depuis le tableau de bord, on souhaite cacher le bouton et binder le
 * change du select sur le submit qui lui déclenche une fonction spécifique
 * JS de changement de collectivité et de rechargement des blocs en AJAX.
 */
function handle_changecollectivite_form() {
    $("#dashboard #changecollectiviteform div.formControls").hide();
    $("#changecollectivite_form").submit(function(event){
        data = "collectivite="+$("select[name='collectivite']").val();
        link = "../app/changecollectivite.php";
        // execution de la requete en POST
        $.ajax({
            type: "POST",
            url: link,
            cache: false,
            data: data,
            async: false,
            success: function(html){
                refresh_header_and_menu_and_dashboard();
                $("#info").html($(html).find("div.message").get(0));
            }
        });
        event.preventDefault();
        return false;
    });
    $("#dashboard #changecollectiviteform select[name='collectivite']").change(function(){
        $("#changecollectivite_form").submit();
    });
}

/**
 * Depuis le tableau de bord, on souhaite cacher le bouton et binder le
 * change du select sur le submit qui lui déclenche une fonction spécifique
 * JS de changement de liste et de rechargement des blocs en AJAX.
 */
function handle_changeliste_form() {
    $("#dashboard #changelisteform div.formControls").hide();
    $("#changeliste_form").submit(function(event){
        data = "liste="+$("select[name='liste']").val();
        link = "../app/changeliste.php";
        // execution de la requete en POST
        $.ajax({
            type: "POST",
            url: link,
            cache: false,
            data: data,
            async: false,
            success: function(html){
                refresh_header_and_menu_and_dashboard();
                $("#info").html($(html).find("div.message").get(0));
            }
        });
        event.preventDefault();
        return false;
    });
    $("#dashboard #changelisteform select[name='liste']").change(function(){
        $("#changeliste_form").submit();
    });
}



function doublon1() {
    var nom=document.f1.nom.value;
    var prenom=document.f1.prenom.value;
    var marital=document.f1.nom_usage.value;
    var datenaissance=document.f1.date_naissance.value;
    if(fenetreouverte==true) pfenetre.close();
    pfenetre=window.open("../app/index.php?module=form&obj=inscription&action=203&nom="+nom+"&prenom="+prenom+"&marital="+marital+"&datenaissance="+datenaissance,"rechercheDoublon","toolbar=no,scrollbars=yes,status=no, width=640,height=400,top=120,left=120");
    fenetreouverte=true;
}

function entre2dates() {
    if (fenetreouverte==true) pfenetre.close();
    deb = document.f1.datedebut.value;
    fin = document.f1.datefin.value;
    prov = get_select_multiple_values(document.f1.provenance_demande);
    bur = document.f1.par_bureau.value;
    if (deb == "" || fin == "" || prov.length == 0) {
        alert("Vous devez saisir une date de debut et une date de fin correctes, ainsi que sélectionner au moins une provenance !");
    }
    else {
        d = deb.substring(0,2)+deb.substring(3,5)+deb.substring(6,10);
        f = fin.substring(0,2)+fin.substring(3,5)+fin.substring(6,10);
        glob = '&globale=true';
        if (bur == 'Oui') {
            glob = '';
        }
        pfenetre=window.open("../app/index.php?module=form&obj=revision&action=303&mode_edition=commission&commission=entre_deux_dates&edition_avec_recapitulatif=false&first="+d+"&last="+f+"&provenance_demande="+prov+glob,"mouvement","toolbar=no,scrollbars=yes,status=no, width=650,height=600,top=80,left=80");
        fenetreouverte=true;
    }
}

function get_select_multiple_values(select) {
    var result = [];
    var options = select && select.options;
    var opt;

    for (var i=0, iLen=options.length; i<iLen; i++) {
        opt = options[i];

        if (opt.selected) {
            result.push(opt.value || opt.text);
        }
    }
    return result;
}

/**
 * fonction de validation du formulaire d'édition des courriers
 * de convocation aux commissions
 */
function checkformconvocationcommission() {
    message = $("#courriercommission textarea[name=message]").val();
    // Si pas de message on empeche la validation du formulaire
    // puis en affiche un message d'erreur
    if (message == "") {
        alert("Vous devez saisir le message du courrier de convocation");
        return false;
    }
}

//
function recapitulatif_mention() {
    //
    if (fenetreouverte == true) {
        pfenetre.close();
    }
    //
    var dateelection1 = document.getElementById("traitement_election_mention_form").dateelection1;
    var dateelection2 = document.getElementById("traitement_election_mention_form").dateelection2;
    //
    pfenetre = window.open("../app/index.php?module=module_election&view=edition_pdf__recapitulatif_mention&dateelection1="+dateelection1.value+"&dateelection2="+dateelection2.value,"statbureau","toolbar=no,scrollbars=yes,status=no, width=650,height=600,top=80,left=80");
    //
    fenetreouverte = true;
}

//
function epuration_procuration_stats() {
    //
    if (fenetreouverte == true) {
        pfenetre.close();
    }
    //
    var dateelection = document.getElementById("traitement_election_epuration_procuration_form").dateelection;
    //
    pfenetre=window.open("../app/index.php?module=form&obj=procuration&action=401&dateElection="+dateelection.value,"statbureau","toolbar=no,scrollbars=yes,status=no, width=650,height=600,top=80,left=80");
    //
    fenetreouverte = true;
}

//
function VerifNum (champ) {
    if  (isNaN (champ.value)) {
        alert ("Vous devez entrer uniquement des chiffres!");
        champ.value = "";
        return;
    }
    champ.value = champ.value.replace (".", "");
}

function VerifNumDecoupage(champ) {
    if (isNaN(champ.value)) {
        alert("vous ne devez entrer \ndes chiffres\nuiniquement");
        champ.value=0;
        return;
    }
    champ.value=champ.value.replace(".","");
    if (champ.value>90001) {
        alert("La valeur du champ \ndoit etre inferieure \no 90001");
        champ.value=90001;
        return;
    }
}

/**
 * TRT
 */
// Cette fonction permet d'afficher une invite de confirmation a l'utilisateur
// pour verifier si il est sur de vouloir executer le traitement
function trt_confirm() {
    //
    if (confirm("Etes-vous sur de vouloir confirmer cette action ?")) {
        //
        return true;
    } else {
        //
        return false;
    }
}

//
function trt_form_trigger_succes() {
    //
}

//
function trt_form_submit(id, link, formulaire) {
    // Si le formulaire n'a pas une classe no_confirmation
    if (!$(formulaire).hasClass("no_confirmation")) {
        // Alors on execute la fonction trt_confirm qui permet de demander
        // a l'utilisateur si il est sur de vouloir continuer
        if (trt_confirm() == false) {
            // Si l'utilisateur n'est pas sur alors on sort de la fonction
            return false;
        }
    }
    // On affiche le spinner pour que l'utilisateur voit de l'animation a
    // l'ecran pendant l'execution du traitement
    $("#"+id).html(msg_loading);
    // Composition de la chaine data en fonction des elements du formulaire
    var data = ""
    if (formulaire) {
        for (i=0;i<formulaire.elements.length;i++) {
            if ((formulaire.elements[i].type == "checkbox" && formulaire.elements[i].checked == true) ||
                formulaire.elements[i].type != "checkbox") {
                data+=formulaire.elements[i].name+"="+formulaire.elements[i].value+"&";
            }
        }
    }
    //
    url = "../app/index.php?module=form&snippet=trt_ajax&obj="+link
    // Execution de la requete en POST
    $.ajax({
        type: "POST",
        url: url,
        cache: false,
        data: data,
        async: false,
        success: function(html){
            $("#"+id).empty();
            $("#"+id).append(html);
            trt_form_trigger_succes();
            om_initialize_content();
        }
    });
    // Si le formulaire a une classe hide_button_on_success
    if ($(formulaire).hasClass("hide_button_on_success")) {
        if ($("#"+id+" div.message").hasClass("ui-state-valid")) {
            $("#"+formulaire.id+" .formControls").hide();
        }
    }
    // Si le formulaire n'a pas une classe no_status_update
    if (!$(formulaire).hasClass("no_status_update")) {
        // execution de la requete en GET qui met a jour le formulaire du
        // traitement
        $.ajax({
            type: "GET",
            url: url+"&action=traitement_"+link,
            cache: false,
            async: false,
            success: function(html){
                $("#traitement_"+link+"_status").empty();
                $("#traitement_"+link+"_status").append(html);
                om_initialize_content();
            }
        });
    }
    //
    return true;
}
function trt_dialog_load() {
    var dialog = $('#trt_log_dialog');
    $(dialog).dialog({
        close: function(ev, ui) {
            if (typeof(callback) === "function") {
                callback(callbackParams);
            }
            $(this).remove();
        },
        resizable: false,
        modal: true,
        width: '800px',
        height: 'auto',
        position: 'center top',
    });
    om_initialize_content();
    return false;
}
//
function ajaxItForm(container_id, link, formulaire) {
    // composition de la chaine data en fonction des elements du formulaire
    var data = ""
    if (formulaire) {
        for (i=0;i<formulaire.elements.length;i++) {
            if (formulaire.elements[i].type == "checkbox" && formulaire.elements[i].checked == false) {
                //
            } else {
                data+=formulaire.elements[i].name+"="+formulaire.elements[i].value+"&";
            }
        }
    }
    // On affiche le spinner pour que l'utilisateur voit de l'animation a
    // l'ecran pendant l'execution du traitement
    $("#"+container_id).html(msg_loading);
    // execution de la requete en POST
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: data,
        success: function(html){
            $('#'+container_id).empty();
            $('#'+container_id).append(html);
            om_initialize_content();
        }
    });
}

function reload_my_tab(tabs_id) {
    var current_index = $(tabs_id).tabs("option","selected");
    $(tabs_id).tabs('load',current_index);
}

//
// Cette fonction permet d'afficher une invite de confirmation a l'utilisateur
// pour verifier si il est sur de vouloir soumettre le formulaire
function form_submit_confirm(form) {
    //
    if (confirm("Etes-vous sur de vouloir confirmer cette action ?")) {
        //
        form.submit();
    } else {
        //
        return false;
    }
}

/**
 * Cette fonction permet de charger dans un dialog jqueryui un formulaire tel
 * qu'il aurait été chargé avec ajaxIt
 *
 * @param objsf string : objet de sousformulaire
 * @param link string : lien vers un sousformulaire (..sousform.php...)
 * @param width integer: width en px
 * @param height integer: height en px
 * @param callback function (optionel) : nom de la méthode à appeler
 *                                       à la fermeture du dialog
 * @param callbackParams mixed (optionel) : paramètre à traiter dans la function
 *                                          callback
 *
 **/
function popupIt(objsf, link, width, height, callback, callbackParams) {
    // Insertion du conteneur du dialog
    var dialog = $('<div id=\"sousform-'+objsf+'\"></div>').insertAfter('#tabs-1 .formControls');
    $('<input type=\"text\" name=\"recherchedyn\" id=\"recherchedyn\" value=\"\" class=\"champFormulaire\" style=\"display:none\" />').insertAfter('#sousform-'+objsf);

    // execution de la requete passee en parametre
    // (idem ajaxIt + callback)
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            //Suppression d'un precedent dialog
            dialog.empty();
            //Ajout du contenu recupere
            dialog.append(html);
            //Creation du dialog
            $(dialog).dialog({
                //OnClose suppression du contenu
                close: function(ev, ui) {
                    // Si le formulaire est submit et valide on execute la méthode
                    // passée en paramètre
                    if (typeof(callback) === "function") {
                        callback(callbackParams);
                    }
                    $(this).remove();
                },
                resizable: true,
                modal: true,
                width: 'auto',
                height: 'auto',
                position: 'left top',
            });
            //Initialisation du theme OM
            om_initialize_content();
        },
        async : false
    });
    //Fermeture du dialog lors d'un clic sur le bouton retour
    $('#sousform-'+objsf).off('click').on('click', 'a.retour', function() {
        $(dialog).dialog('close').remove();
        return false;
    });
}

/**
 * Vérifie si la vaeur du champ passé en paramètre est un courriel valide. Si
 * c'est le cas, on ne fait rien. Si ce n'est pas le cas, on l'indique à
 * l'utilisateur sans l'obliger à modifier sa saisie.
 */
function is_email(field) {
    myVar = field.value;
    var regEmail = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$', 'i');
    if (regEmail.test(myVar) != true) {
        alert("le courriel saisi n'est pas valide");
    }
}

/**
 *
 */
function bind_inscription_search_form_widget() {
    if ($("#inscription_search").length === 0
        && $("form#snippet-search_ine-form").length === 0) {
        return;
    }
    $("#recherche_par").on("change", function(event) {
        handle_inscription_search_form_widget();
    });
}
/**
 *
 */
function handle_inscription_search_form_widget() {
    if ($("#inscription_search").length === 0
        && $("form#snippet-search_ine-form").length === 0) {
        return;
    }
    recherche_par = $("#recherche_par").val();
    if (recherche_par == "ine") {
        $(".bloc-ine").show();
        $(".bloc-etat-civil").hide();
    } else if (recherche_par == "etat_civil") {
        $(".bloc-ine").hide();
        $(".bloc-etat-civil").show();
        $(".field-recherche-exacte").hide();
        $(".field-sexe").show();
        $(".field-prenom").show();
    } else {
        $(".bloc-ine").hide();
        $(".bloc-etat-civil").show();
        $(".field-recherche-exacte").show();
        $(".field-sexe").hide();
        $(".field-prenom").hide();
    }
}

/**
 *
 */
function bind_adresse_resident_form_widget() {
    if ($(".form-inscription-maj-0").length === 0 
        && $(".form-modification-maj-0").length === 0
        && $(".form-modification-maj-1").length === 0
        && $(".form-inscription-maj-1").length === 0
        && $(".form-inscription-maj-62").length === 0) {
        //
        return;
    }
    $("#resident").on("change", function(event) {
        handle_adresse_resident_form_widget();
    });
}
/**
 *
 */
function handle_adresse_resident_form_widget() {
    if ($(".form-inscription-maj-0").length === 0 
        && $(".form-modification-maj-0").length === 0
        && $(".form-modification-maj-1").length === 0
        && $(".form-inscription-maj-1").length === 0
        && $(".form-inscription-maj-62").length === 0) {
        //
        return;
    }
    //
    resident = $("#resident").val();
    if (resident == "Non") {
        $(".bloc.resident-adresse").hide();
        $(".bloc.resident-adresse .required-conditional").each(function(){
            $(this).hide();
        });
    } else {
        $(".bloc.resident-adresse").show();
        $(".bloc.resident-adresse .required-conditional").each(function(){
            $(this).show();
        });
    }
}
/**
 *
 */
function bind_decoupage_form_widget() {
    if ($(".form-inscription-maj-0").length === 0 
        && $(".form-modification-maj-0").length === 0
        && $(".form-modification-maj-1").length === 0
        && $(".form-inscription-maj-1").length === 0
        && $(".form-inscription-maj-62").length === 0) {
        //
        return;
    }
    $("#numero_habitation").on("change", function(event) {
        handle_decoupage_form_widget();
    });
    $("#autocomplete-voie-id").on("change", function(event) {
        handle_decoupage_form_widget();
    });
    $("#bureauforce").on("change", function(event) {
        handle_decoupage_form_widget();
    });
    $("#bureau").on("change", function(event) {
        handle_decoupage_form_widget();
    });
}

/**
 *
 */
function handle_decoupage_form_widget() {
    if ($(".form-inscription-maj-0").length === 0 
        && $(".form-modification-maj-0").length === 0
        && $(".form-modification-maj-1").length === 0
        && $(".form-inscription-maj-1").length === 0
        && $(".form-inscription-maj-62").length === 0) {
        //
        return;
    }
    if ($("#numero_habitation").val() == "0") {
        $("#numero_habitation").val("");
    }
    //
    bureauforce = $("#bureauforce").val();
    $('.info-bureau-decoupage').remove();
    if (bureauforce == "Non") {
        numero_habitation = $("#numero_habitation").val();
        voie = $("#autocomplete-voie-id").val();
        $.ajax({
            type: "GET",
            url: '../app/index.php?module=form&obj=inscription&idx=0&action=251&voie='+voie+'&numero_habitation='+numero_habitation,
            cache: false,
            success: function(output){
                result = JSON.parse(output);
                $('span.info-bureau-decoupage').remove();
                if (result["code"] < 0 && voie != "") {
                    $("#bureau").val("");
                    $('<span class="info-bureau-decoupage">Aucune affectation automatique dans le découpage</span>').insertAfter("#bureau");
                } else if (result["code"] == 0) {
                    $('<span class="info-bureau-decoupage">Ce bureau a été affecté automatiquement à partir de l\'adresse saisie</span>').insertAfter("#bureau");
                    $("#bureau").val(result["bureau"]);
                } else {
                    $("#bureau").val("");
                }
                
            },
            async : true
        });
    } else {
        $('<span class="info-bureau-decoupage">Le choix du bureau est fait manuellement (bureau forcé)</span>').insertAfter("#bureau");
    }
}
var origine_codebureau = "-1";
function forcerlebureau(bureau) {
    // ================================================================
    // si un bureau est choisi, le bureau est force
    // ================================================================
    if (origine_codebureau=="-1") origine_codebureau = bureau;
    if (document.f1.bureau.value=="") {
        document.f1.bureauforce.value="Non";
    } else {
        document.f1.bureauforce.value="Oui";
    }
}
function nepasforcerlebureau() {
    // ================================================================
    // si le bureau n'est plus force, le bureau revient à sa valeur initiale
    // ================================================================
    if (origine_codebureau=="-1") origine_codebureau = document.f1.bureau.value;
    if (document.f1.bureauforce.value=="Non") document.f1.bureau.value=origine_codebureau;
}

/**
 *
 */
function bind_naissance_form_widget() {
    if ($(".form-inscription-maj-0").length === 0 
        && $(".form-modification-maj-0").length === 0
        && $(".form-modification-maj-1").length === 0
        && $(".form-inscription-maj-1").length === 0) {
        //
        return;
    }
    $("#autocomplete-pays-naissance-id").on("change", function(event) {
        $("#code_departement_naissance").val($("#autocomplete-pays-naissance-id").val());
        $("#libelle_departement_naissance").val("");
        $("#code_lieu_de_naissance").val("");
    });
    $("#autocomplete-ancien-departement-francais-algerie-id").on("change", function(event) {
        $("#code_departement_naissance").val($("#autocomplete-ancien-departement-francais-algerie-id").val());
        $("#libelle_departement_naissance").val("");
        $("#code_lieu_de_naissance").val("");
    });
    $("#autocomplete-commune-naissance-id").on("change", function(event) {
        $("#code_lieu_de_naissance").val($("#autocomplete-commune-naissance-id").val());
        $("#code_departement_naissance").val("");
        $("#libelle_departement_naissance").val("");
    });
}

/**
 *
 */
function clear_naissance_form_widget() {
    if ($(".form-inscription-maj-0").length === 0 
        && $(".form-modification-maj-0").length === 0
        && $(".form-modification-maj-1").length === 0
        && $(".form-inscription-maj-1").length === 0) {
        //
        return;
    }
    //
    if ($("#naissance_type_saisie").val() == "Etranger") {
        //Né à l'étranger 
        clear_autocomplete('autocomplete-commune-naissance');
        clear_autocomplete('autocomplete-ancien-departement-francais-algerie');
        $("#libelle_lieu_de_naissance").val("");
    } else if($("#naissance_type_saisie").val() == "France"){
        //Né en France
        clear_autocomplete('autocomplete-pays-naissance');
        clear_autocomplete('autocomplete-pays-naissance-colonnie');
        $("#libelle_lieu_de_naissance").val("");
    } else if($("#naissance_type_saisie").val() == "Ancien_dep_franc"){
        //Né ancien département français d'Algérie
        clear_autocomplete('autocomplete-commune-naissance');
        clear_autocomplete('autocomplete-pays-naissance');
    } else if($("#naissance_type_saisie").val() == "autre"){
        //Lieu de naissance autre
        clear_autocomplete('autocomplete-commune-naissance');
        clear_autocomplete('autocomplete-pays-naissance');
        clear_autocomplete('autocomplete-pays-naissance-colonnie');
    }
    handle_naissance_form_widget();
}

/**
 *
 */
function handle_naissance_form_widget() {
    if ($(".form-inscription-maj-0").length === 0 
        && $(".form-modification-maj-0").length === 0
        && $(".form-modification-maj-1").length === 0
        && $(".form-inscription-maj-1").length === 0) {
        //
        return;
    }
    //
    $(".field-code_departement_naissance").hide();
    $(".field-libelle_departement_naissance").hide();
    $(".field-code_lieu_de_naissance").hide();
    $(".field-libelle_lieu_de_naissance").hide();
    $(".field-code_departement_naissance input").attr('readonly', false);
    $(".field-libelle_departement_naissance input").attr('readonly', false);
    $(".field-code_lieu_de_naissance input").attr('readonly', false);
    $(".field-libelle_lieu_de_naissance input").attr('readonly', false);

    if ($("#naissance_type_saisie").val() == "Etranger") {
        //Né à l'étranger 
        $(".field-live_ancien_departement_francais_algerie").hide();
        $(".field-live_pays_de_naissance").show();
        $(".field-live_commune_de_naissance").hide();
        $(".field-libelle_lieu_de_naissance").show();
    } else if($("#naissance_type_saisie").val() == "France"){
        //Né en France
        $(".field-live_ancien_departement_francais_algerie").hide();
        $(".field-live_pays_de_naissance").hide();
        $(".field-live_commune_de_naissance").show();
        $(".field-libelle_lieu_de_naissance").hide();
    } else if($("#naissance_type_saisie").val() == "Ancien_dep_franc"){
        //Né ancien département français d'Algérie
        $(".field-live_ancien_departement_francais_algerie").show();
        $(".field-live_pays_de_naissance").hide();
        $(".field-live_commune_de_naissance").hide();
        $(".field-libelle_lieu_de_naissance").show();
    } else if($("#naissance_type_saisie").val() == "autre"){
        // Autre
        $(".field-live_ancien_departement_francais_algerie").hide();
        $(".field-live_pays_de_naissance").hide();
        $(".field-live_commune_de_naissance").hide();
        //
        $(".field-code_departement_naissance").show();
        $(".field-libelle_departement_naissance").show();
        $(".field-code_lieu_de_naissance").show();
        $(".field-libelle_lieu_de_naissance").show();
        //
        $(".field-code_departement_naissance input").attr('readonly', true);
        $(".field-libelle_departement_naissance input").attr('readonly', true);
        $(".field-code_lieu_de_naissance input").attr('readonly', true);
        $(".field-libelle_lieu_de_naissance input").attr('readonly', true);
        //
        if ($("#backup_code_departement_naissance").length == 0) {
            $(
                '<input type="hidden" value="'+ $(".field-code_departement_naissance input").val() +'" id="backup_code_departement_naissance" />'+
                '<input type="hidden" value="'+ $(".field-libelle_departement_naissance input").val() +'" id="backup_libelle_departement_naissance" />'+
                '<input type="hidden" value="'+ $(".field-code_lieu_de_naissance input").val() +'" id="backup_code_lieu_de_naissance" />'+
                '<input type="hidden" value="'+ $(".field-libelle_lieu_de_naissance input").val() +'" id="backup_libelle_lieu_de_naissance" />'
            ).insertAfter("#naissance_type_saisie");
        } else {
            $(".field-code_departement_naissance input").val($("#backup_code_departement_naissance").val());
            $(".field-libelle_departement_naissance input").val($("#backup_libelle_departement_naissance").val());
            $(".field-code_lieu_de_naissance input").val($("#backup_code_lieu_de_naissance").val());
            $(".field-libelle_lieu_de_naissance input").val($("#backup_libelle_lieu_de_naissance").val());
        }
    }
}

/**
 * FORM_HELPER - 
 */
function bind__procuration__statut__form_widget() {
    if ($(".form-procuration-maj-0").length === 0
        && $(".form-procuration-maj-1").length === 0) {
        //
        return;
    }
    $("#statut").on("change", function(event) {
        handle__procuration__statut__form_widget();
    });
}

/**
 * FORM_HELPER - 
 */
function handle__procuration__statut__form_widget() {
    if ($(".form-procuration-maj-0").length === 0
        && $(".form-procuration-maj-1").length === 0) {
        //
        return;
    }
    //
    statut = $("#statut").val();
    if (statut == "demande_refusee") {
        $(".field--procuration--motif_refus").show();
    } else {
        $(".field--procuration--motif_refus").hide();
    }
}

/**
 * FORM_HELPER - 
 */
function bind__procuration__annulation_autorite_type__form_widget() {
    if ($(".form-procuration-maj-23").length === 0) {
        //
        return;
    }
    $("#annulation_autorite_type").on("change", function(event) {
        handle__procuration__annulation_autorite_type__form_widget();
    });
}
/**
 * FORM_HELPER - 
 */
function bind__procuration__autorite_type__form_widget() {
    if ($(".form-procuration-maj-0").length === 0
        && $(".form-procuration-maj-1").length === 0) {
        //
        return;
    }
    $("#autorite_type").on("change", function(event) {
        handle__procuration__autorite_type__form_widget();
    });
}

/**
 * FORM_HELPER - 
 */
function handle__procuration__annulation_autorite_type__form_widget() {
    if ($(".form-procuration-maj-23").length === 0) {
        //
        return;
    }
    //
    autorite_type = $("#annulation_autorite_type").val();
    if (autorite_type == "CSL") {
        $(".field--procuration--annulation_autorite_commune").hide();
        $(".field--procuration--annulation_autorite_consulat").show();
    } else {
        $(".field--procuration--annulation_autorite_commune").show();
        $(".field--procuration--annulation_autorite_consulat").hide();
    }
}

/**
 * FORM_HELPER - 
 */
function handle__procuration__autorite_type__form_widget() {
    if ($(".form-procuration-maj-0").length === 0
        && $(".form-procuration-maj-1").length === 0) {
        //
        return;
    }
    //
    autorite_type = $("#autorite_type").val();
    if (autorite_type == "CSL") {
        $(".field--procuration--autorite_commune").hide();
        $(".field--procuration--autorite_consulat").show();
    } else {
        $(".field--procuration--autorite_commune").show();
        $(".field--procuration--autorite_consulat").hide();
    }
}

/**
 * FORM_HELPER - 
 */
function bind__procuration__mandant_mandataire__form_widget() {
    if ($(".form-procuration-maj-0").length === 0
        && $(".form-procuration-maj-1").length === 0) {
        //
        return;
    }
//    $("#autocomplete-electeur-mandant-id").on("change", function(event) {
//        handle__procuration__mandant_mandataire__form_widget();
//    });
//    $("#autocomplete-electeur-mandant-id").on("change", function(event) {
//        handle__procuration__mandant_mandataire__form_widget();
//    });
    $("#mandant_hors_commune").on("change", function(event) {
        handle__procuration__mandant_mandataire__form_widget();
    });
    $("#mandataire_hors_commune").on("change", function(event) {
        handle__procuration__mandant_mandataire__form_widget();
    });
}


/**
 * FORM_HELPER - 
 */
function handle__procuration__mandant_mandataire__form_widget() {
    if ($(".form-procuration-maj-0").length === 0
        && $(".form-procuration-maj-1").length === 0) {
        //
        return;
    }
    if ($("#mandant_hors_commune").is(':checked') === true) {
        $(".field--procuration--mandant").hide();
        $(".field--procuration--mandant_ine").show();
        clear_autocomplete("autocomplete-electeur-mandant");
    } else {
        $(".field--procuration--mandant").show();
        $(".field--procuration--mandant_ine").hide();
        $("#mandant_ine").val("");
    }
    if ($("#mandataire_hors_commune").is(':checked') === true) {
        $(".field--procuration--mandataire").hide();
        $(".field--procuration--mandataire_ine").show();
        clear_autocomplete("autocomplete-electeur-mandataire");
    } else {
        $(".field--procuration--mandataire").show();
        $(".field--procuration--mandataire_ine").hide();
        $("#mandataire_ine").val("");
    }
}


/**
 * FORM WIDGET - SEARCH_INE
 */
// Cette fonction permet d'associer le formulaire à la fonction ajaxSubmit
// et de charger de nouveau le formulaire dans le dialog
function snippet_bind_form_submit(snippet) {
    //
    $("#snippet-"+snippet+"-form").submit(function() {
        // submit the form
        $(this).ajaxSubmit({
            // avant la soumission du formulaire
            beforeSend:function() {
                    // Affichage du spinner
                    $("#snippet-"+snippet+"-container div.message").remove();
                    $("#snippet-"+snippet+"-form").html(msg_loading);
                },
            // lors de la validation du formulaire
            success: function(html){
                // chargement de l'url en lieu et place de l'ancienne
                $("#snippet-"+snippet+"-container").empty();
                $("#snippet-"+snippet+"-container").append(html);
                // initilisation du contenu
                om_initialize_content();
                // initialisation du formulaire
                snippet_bind_form_submit(snippet);
            }});
        // retour de la valeur false pour que le formulaire ne soit pas soumis
        // de manière normale
        return false;
    });
}
// Cette fonction permet de charger le formulaire d'upload dans le dialog lors
// du premier appel
function snippet_load_form(snippet, link) {
    //
    var dialog = $("<div id=\"snippet-"+snippet+"-container\" role=\"dialog\"></div>").insertAfter('#footer');
    //
    $.ajax({
        type: "GET",
        url: link,
        success: function(html){
            // chargement de l'url dans le conteneur dialog
            dialog.empty();
            dialog.append(html);
            // initilisation du contenu
            om_initialize_content();
            // initialisation du formulaire
            snippet_bind_form_submit(snippet);
            // affichage du dialog
            $(dialog).dialog({
                // a la fermeture du dialog
                close: function(ev, ui) {
                    // suppression du contenu
                    $(this).remove();
                },
                resizable: false,
                modal: true,
                width: 'auto',
                height: 'auto',
                position: 'center'
            });
        }
    });
    // fermeture du dialog lors d'un clic sur le bouton retour
    dialog.on("click",'a.linkjsclosewindow',function() {
        $(dialog).dialog('close').remove();
        return false;
    });
}
// Cette fonction permet de retourner les informations sur le fichier téléchargé
// du formulaire d'upload vers le formulaire d'origine
function snippet_return_one_value(snippet, form, champ, value) {
    $("form[name|="+form+"] #"+champ).attr('value', value);
    $("#snippet-"+snippet+"-container").dialog('close').remove();
}

function snippet_search_ine(champ) {
    form_snippet_href_base = $("span.form-snippet-search_ine").attr('data-href');
    var link = form_snippet_href_base+"&origine="+champ+"&form=f1";
    snippet_load_form("search_ine", link);
}

/**
 * Récupère à l'aide d'une ajax le message indiquant le nombre de lot
 * à traiter pour un mode de traitement par lot donné. Affiche ensuite
 * ce message sur le formulaire en cours.
 * 
 * @param {string} mode nom du mode de traitement
 */
function display_number_of_mouvement(mode) {
    var message = $('div#form-message');
    // Si aucun mode n'a été sélectionné le message n'a pas a être affiché
    if (mode != '') {
        $.ajax({
            type: "GET",
            url: '../app/index.php?module=form&obj=mouvement&action=5&mode=' + mode,
            cache: false,
            success: function(html){
                // Vérifie si il existe déjà un message lié à la validation du formulaire
                oldMessage = $('div#form-container div.message');
                if (oldMessage.get(0) != undefined) {
                    oldMessage.remove();
                }
                // Vide les anciens messages avant de remplir le suivant
                message.empty();
                message.append(html);
            },
            async : false
        });
    } else {
        // Supprime le message déjà affiché si aucun mode de traitement par lot
        // n'a été saisi
        message.empty();
    }
}