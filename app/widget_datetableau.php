<?php
/**
 * WIDGET DASHBOARD - widget_datetableau.
 *
 * Date de tableau.
 *
 * L'objet de ce widget est de présenter la date de tableau et un lien pour accéder
 * à l'écran qui permet de la changer.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
if (isset($f) === false) {
    $f = new openelec(null);
}

/**
 * DATE DE TABLEAU
 */
if ($f->isAccredited("datetableau_consulter")) {
    //
    $date_tableau_valide = true;
    //
    /**
     *
     */
    $inst_revision = $f->get_inst__om_dbform(array(
        "obj" => "revision",
    ));
    $date_tableau_valide = $inst_revision->is_date_tableau_valid();
    //
    if ($date_tableau_valide == false) {
        $f->displayMessage("error", __("La date de tableau n'est pas valide. Merci de la corriger."));
    } else if (time() > strtotime($f->getParameter("datetableau"))) {
        $f->displayMessage("error", __("La date de tableau est dans le passe. Attention de ne pas oublier de la changer."));
    }
    echo "<p class=\"datetableau\">";
    $accredited = $f->isAccredited("datetableau_changer");
    if ($accredited) {
        echo "<a href=\"../app/changedatetableau.php\" title=\"".__("Cliquer ici pour mettre a jour la date de tableau")."\">";
    }
    echo "<img src=\"../app/img/calendar.png\" alt=\"\" title=\"\" />";
    echo " ".$f->formatdate($f->getParameter("datetableau"));
    if ($accredited) {
        echo "</a>";
    }
    echo "</p>";
} else {
    //
    $widget_is_empty = true;
}
