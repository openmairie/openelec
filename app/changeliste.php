<?php
/**
 * Ce script permet d'interfacer le formulaire de changement de liste.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
$f = new openelec(
    "nohtml",
    "liste_de_travail_changer",
    __("Choix de la liste de travail")
);

/**
 *
 */
require_once("../obj/workliste.class.php");

//
$workliste = new workListe("../app/changeliste.php");
$workliste->validateForm();

//
$f->setFlag(null);
$f->display();

//
$workliste->showForm();
