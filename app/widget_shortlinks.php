<?php
/**
 * WIDGET DASHBOARD - widget_shortlinks.
 *
 * Raccourcis.
 *
 * L'objet de ce widget est de présenter des raccoucis (liens) vers des écrans
 * de l'application.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
if (isset($f) === false) {
    $f = new openelec(null);
}

//
$args = array();
foreach (explode("\"n", $content) as $elem) {
    $arg = explode("=", $elem);
    if (count($arg) == 2) {
        $args[trim($arg[0])] = trim($arg[1]);
    }
}

//
$filter = null;
if (array_key_exists("filtre", $args)) {
    $filters_available = array(
        "saisie",
        "saisie_mention",
        "saisie_electeur",
        "traitements",
    );
    if (in_array($args["filtre"], $filters_available) === true) {
        $filter = $args["filtre"];
    }
}

//
$shortlinks = array(
    array(
        "href" => OM_ROUTE_FORM."&obj=inscription&idx=0&action=101",
        "class" => "mvt-inscription",
        "title" => __("Inscription"),
        "description" => __("Effectuer une recherche de doublon avant d'acceder au formulaire d'inscription d'un electeur."),
        "right" => "inscription",
        "parameters" => array(
            "is_collectivity_mono" => true,
        ),
        "filters" => array("saisie", "saisie_electeur", ),
    ),
    array(
        "href" => OM_ROUTE_FORM."&obj=modification&idx=0&action=101",
        "class" => "mvt-modification",
        "title" => __("Modification"),
        "description" => __("Rechercher sur la liste electorale un electeur a modifier (changement d'adresse, correction etat civil, ...)."),
        "right" => "electeur_modification",
        "parameters" => array(
            "is_collectivity_mono" => true,
        ),
        "filters" => array("saisie", "saisie_electeur", ),
    ),
    array(
        "href" => OM_ROUTE_FORM."&obj=radiation&idx=0&action=101",
        "class" => "mvt-radiation",
        "title" => __("Radiation"),
        "description" => __("Rechercher sur la liste electorale un electeur a radier (depart de la commune, deces, ...)."),
        "right" => "electeur_radiation",
        "parameters" => array(
            "is_collectivity_mono" => true,
        ),
        "filters" => array("saisie", "saisie_electeur", ),
    ),
    array(
        "href" => OM_ROUTE_FORM."&obj=procuration&action=0",
        "class" => "procuration",
        "title" => __("Procuration"),
        "description" => __("Un mandant donne procuration a un mandataire sur une periode donnee."),
        "right" => "procuration",
        "parameters" => array(
            "is_collectivity_mono" => true,
        ),
        "filters" => array("saisie", "saisie_mention", ),
    ),
    array(
        "href" => "../app/index.php?module=module_commission",
        "class" => "traitement",
        "title" => __("Commission"),
        "description" => __("Tableau de bord destine a la preparation et la validation des commissions."),
        "right" => "module_commission",
        "parameters" => array(
            "is_collectivity_mono" => true,
        ),
        "filters" => array("traitements", ),
    ),
    array(
        "href" => "../app/index.php?module=module_traitement_j5",
        "class" => "traitement",
        "title" => __("5 jours"),
        "description" => __("Tableau de bord destine aux traitements des 5 jours."),
        "right" => "module_traitement_j5",
        "parameters" => array(
            "is_collectivity_mono" => true,
        ),
        "filters" => array("traitements", ),
    ),
    array(
        "href" => "../app/index.php?module=module_traitement_annuel",
        "class" => "traitement",
        "title" => __("Annuel"),
        "description" => __("Tableau de bord destine a la preparation et la validation des traitements annuels du 10 janvier et du 28 fevrier."),
        "right" => "module_traitement_annuel",
        "parameters" => array(
            "is_collectivity_mono" => true,
        ),
        "filters" => array("traitements", ),
    ),
);

//
$shortlinks_to_display = array();
foreach ($shortlinks as $shortlink) {
    if ($filter !== null
        && in_array($filter, $shortlink["filters"]) === false) {
        continue;
    }
    if ($f->isAccredited($shortlink["right"]) === false) {
        continue;
    }
    $parameter = true;
    foreach ($shortlink["parameters"] as $key => $value) {
        if ($f->getParameter($key) !== $value) {
            $parameter = false;
            continue;
        }
    }
    if ($parameter !== true) {
        continue;
    }
    $shortlinks_to_display[] = $shortlink;
}

//
if (count($shortlinks_to_display) == 0) {
    $widget_is_empty = true;
    return;
}

//
$listing = "";
$counter = 0;
foreach ($shortlinks_to_display as $shortlink) {
    $listing .= sprintf(
        '<li class="ui-corner-all col%1$s">
            <a href="%2$s">
                <span class="link">
                    <span class="om-icon om-icon-25 om-icon-fix %3$s-25"><!-- --></span>
                    %4$s
                </span>
                <span class="description">
                    %5$s
                </span>
            </a>
        </li>',
        $counter++,
        $shortlink["href"],
        $shortlink["class"],
        $shortlink["title"],
        $shortlink["description"]
    );
}
printf(
    '<ul class="menu-friendly">%s</ul>',
    $listing
);
