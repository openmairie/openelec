<?php
/**
 * WIDGET DASHBOARD - widget_collectivite_de_travail.
 *
 * Collectivité de travail.
 *
 * L'objet de ce widget est d'afficher la collectivité sur laquelle l'utilisateur
 * est connecté et de lui permettre d'en sélectionner une autre.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
if (isset($f) === false) {
    $f = new openelec(null);
}

/**
 *
 */
require_once("../obj/workcollectivite.class.php");

// Important le code doit etre place ici pour que la nouvelle collectivite soit
// affichee correctement dans le header
$workcollectivite = new workCollectivite(OM_ROUTE_DASHBOARD);
$workcollectivite->validateForm();

/**
 * CHANGE COLLECTIVITE
 */
if (count($workcollectivite->getCollectivites()) > 1
    && $_SESSION["multi_utilisateur"] == 1
    && $f->isAccredited('collectivite_de_travail_changer')) {
    $workcollectivite->showForm(false);
} else {
    //
    $widget_is_empty = true;
}
