<?php
/**
 * WIDGET DASHBOARD - widget_election.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
if (isset($f) === false) {
    $f = new openelec(null, null, __("Widget - Élections"));
}

// Affichage du widget
$inst_om_widget = $f->get_inst__om_dbform(array(
    "obj" => "om_widget",
    "idx" => 0,
));
if (isset($content) !== true) {
    $content = null;
}
$widget_is_empty = $inst_om_widget->view_widget_election($content);
