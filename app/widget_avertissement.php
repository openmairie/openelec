<?php
/**
 * WIDGET DASHBOARD - widget_avertissement.
 *
 * Avertissement.
 *
 * L'objet de ce widget est d'expliquer les risques de déployer un logiciel
 * métier sans expertise sur ce dernier.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
if (isset($f) === false) {
    $f = new openelec(null);
}

printf('openElec est une application sensible, nécessitant un paramétrage précis. Un mauvais paramétrage peut entrainer une révision incorrecte de la liste électorale, des erreurs d\'éditions ou de lien avec l\'INSEE. Ni l\'équipe du projet openElec ni le chef de projet ne peuvent être tenus pour responsables d\'un éventuel dysfonctionnement comme ceci est précisé dans la licence jointe. Vous pouvez, si vous le souhaitez, faire appel a un prestataire spécialisé qui peut fournir support, hot-line, maintenance, et garantir le fonctionnement en environnement de production. <a target="_blank" href="http://www.openmairie.org/catalogue/openelec">http://www.openmairie.org/catalogue/openelec</a>');
