<?php
/**
 * WIDGET DASHBOARD - widget_stats_electeurs.
 *
 * Statistiques électeurs.
 *
 * L'objet de ce widget est de présenter des métriques sur le nombre
 * d'électeurs toutes listes confondues.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
if (isset($f) === false) {
    $f = new openelec("nohtml");
}

//
$oel_infos = array(
    "LP" => array(
        "nb" => 0,
        "title" => "LISTE PRINCIPALE",
    ),
    "LCE" => array(
        "nb" => 0,
        "title" => "LISTE COMPLÉMENTAIRE EUROPÉENNE",
    ),
    "LCM" => array(
        "nb" => 0,
        "title" => "LISTE COMPLÉMENTAIRE MUNICIPALE",
    ),
);
$ret = $f->get_all_results_from_db_query(sprintf(
    'SELECT liste.liste_insee, liste.libelle_liste, count(electeur.id) as nb FROM %1$selecteur INNER JOIN %1$sliste ON electeur.liste=liste.liste %3$s GROUP BY liste.liste_insee, liste.libelle_liste',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    ($f->getParameter("is_collectivity_multi") == true ? "" : sprintf('WHERE electeur.om_collectivite=%s', intval($_SESSION["collectivite"])))
));
foreach ($ret["result"] as $key => $value) {
    if (isset($oel_infos[$value["liste_insee"]])) {
        $oel_infos[$value["liste_insee"]]["nb"] = intval($value["nb"]);
    }
}
//
$reu_infos = array();
$inst_reu = $f->get_inst__reu();
if ($inst_reu !== null) {
    if ($inst_reu->is_connexion_logicielle_valid() === true) {
        $indicateurs = $inst_reu->get_indicateurs();
        if ($indicateurs == null) {
            $indicateurs = array(
                "result" => array(),
            );
        }
        $reu_infos = $indicateurs["result"];
        $reu_infos["message_date_de_calcul"] = sprintf(
            'Chiffre provenant du REU calculé le %s',
            str_replace("T", " à ", gavoe($reu_infos, array("dateCalcul", )))
        );
        //
        if (isset($indicateurs)
            && is_array($indicateurs)
            && isset($indicateurs["result"])
            && is_array($indicateurs["result"])
            && isset($indicateurs["result"]["nbElecteursSansBureauVote"])
            && intval($indicateurs["result"]["nbElecteursSansBureauVote"]) > 0) {
            //
            $message = "<b>-> ".__("Electeurs sans bureau de vote dans le REU")."</b><br/><br/>";
            $message .= "Vous avez ".intval($indicateurs["result"]["nbElecteursSansBureauVote"])." électeur(s) sans bureau de vote dans le REU. Pour corriger ça, il vous faut d'abord vérifier que toutes les inscriptions d'office sont validées et ensuite transmettre les informations via l'onglet 'transmission par lot' du menu 'Traitement > REU' depuis openElec.";
            $this->f->displayMessage("error", $message);
        }

    } else {
        $inst_reu = null;
    }
}
if ($f->getParameter("is_collectivity_multi") == true) {
    $electeurs_without_bureau = $f->get_all_results_from_db_query(sprintf(
        'SELECT
            electeur.om_collectivite,
            count(*) as nb
        FROM %1$selecteur
            INNER JOIN %1$sreu_sync_listes
                ON (electeur.ine=reu_sync_listes.numero_d_electeur::int and reu_sync_listes.code_ugle=(\'com\'||electeur.om_collectivite))
        WHERE
            electeur.numero_bureau is null
            AND reu_sync_listes.code_bureau_de_vote is null
        GROUP BY
            electeur.om_collectivite
        ORDER BY
            electeur.om_collectivite',
        DB_PREFIXE
    ));
    if (count($electeurs_without_bureau["result"]) != 0) {
        $message = "<b>-> ".__("Electeurs sans numéro d'ordre et sans bureau de vote dans le REU")."</b><br/><br/>";
        $message .= count($electeurs_without_bureau["result"])." collectivité(s) avec des électeurs sans numéro d'ordre et sans bureau de vote dans le REU. Pour corriger ça, il vous faut d'abord vérifier que toutes les inscriptions d'office sont validées et ensuite transmettre les informations via l'onglet 'transmission par lot' du menu 'Traitement > REU' depuis openElec.";
        $message .= "<ul>";
        foreach ($electeurs_without_bureau["result"] as $electeur_without_bureau) {
            $message .= "<li>".$electeur_without_bureau["om_collectivite"]." (".$electeur_without_bureau["nb"].")</li>";
        }
        $message .= "</ul>";
        $this->f->displayMessage("error", $message);
    }
}

//
printf(
    '
    <div id="stats-electeurs">
    <div class="card-group">
        <div class="card text-center text-white bg-info">
            <div class="card-body bloc-lp">
                <div class="card-title nb-electeurs">%s</div>
                <span title="%s">%s</span>
                %s
            </div>
        </div>
        <div class="card text-center bg-light">
            <div class="card-body bloc-lce">
                <div class="card-title nb-electeurs">%s</div>
                <span title="%s">%s</span>
                %s
            </div>
        </div>
        <div class="card text-center bg-light">
            <div class="card-body bloc-lcm">
                <div class="card-title nb-electeurs">%s</div>
                <span title="%s">%s</span>
                %s
            </div>
        </div>
    </div>
    </div>
    ',
    //
    gavoe($oel_infos, array("LP", "nb", )),
    gavoe($oel_infos, array("LP", "title", )),
    "LP",
    ($inst_reu === null ? "" : sprintf(
        '<div>(%s) <span class="info-16 info-white-16" title="%s"></span></div>',
        gavoe($reu_infos, array("nbElecteursInscritsLP", )),
        gavoe($reu_infos, array("message_date_de_calcul", ))
    )),
    //
    gavoe($oel_infos, array("LCE", "nb", )),
    gavoe($oel_infos, array("LCE", "title", )),
    "LCE",
    ($inst_reu === null ? "" : sprintf(
        '<div>(%s) <span class="info-16" title="%s"></span></div>',
        gavoe($reu_infos, array("nbElecteursInscritsLCE", )),
        gavoe($reu_infos, array("message_date_de_calcul", ))
    )),
    //
    gavoe($oel_infos, array("LCM", "nb", )),
    gavoe($oel_infos, array("LCM", "title", )),
    "LCM",
    ($inst_reu === null ? "" : sprintf(
        '<div>(%s) <span class="info-16" title="%s"></span></div>',
        gavoe($reu_infos, array("nbElecteursInscritsLCM", )),
        gavoe($reu_infos, array("message_date_de_calcul", ))
    ))
);
