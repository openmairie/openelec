<?php
/**
 * WIDGET DASHBOARD - widget_actualites_reu.
 *
 * Actualités REU.
 *
 * L'objet de ce widget est d'afficher les actualités REU.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
if (isset($f) === false) {
    $f = new openelec(null);
}

if ($f->getParameter("is_collectivity_multi") == true) {
    $widget_is_empty = true;
    return;
}

$inst_reu = $f->get_inst__reu();
if ($inst_reu === null) {
    $f->displayMessage(
        "error",
        __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
    );
    return;
}
if ($inst_reu->is_connexion_logicielle_valid() !== true) {
    $f->displayMessage(
        "error",
        ERROR_MSG_REU_CONNECT
    );
    return;
}

require_once "../obj/module_reu.class.php";
$inst_module_reu = new module_reu();
$output = $inst_module_reu->get_display__actualites_list();
if ($output === "") {
    $widget_is_empty = true;
} else {
    echo $output;
}
