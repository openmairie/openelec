<?php
/**
 * WIDGET DASHBOARD - widget_liste_de_travail.
 *
 * Liste de travail.
 *
 * L'objet de ce widget est d'afficher la liste sur laquelle l'utilisateur
 * est connecté et de lui permettre d'en sélectionner une autre.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
if (isset($f) === false) {
    $f = new openelec(null);
}

/**
 *
 */
require_once("../obj/workliste.class.php");

// Important le code doit etre place ici pour que la nouvelle liste soit
// affichee correctement dans le header
$workliste = new workListe(OM_ROUTE_DASHBOARD);
$workliste->validateForm();

/**
 * CHANGE LISTE
 */
if (count($workliste->getListes()) > 1
    && $f->isAccredited('liste_de_travail_changer')) {
    $workliste->showForm(false);
} else {
    //
    $widget_is_empty = true;
}
