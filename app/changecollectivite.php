<?php
/**
 * Ce script permet d'interfacer le formulaire de changement de collectivité.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
$f = new openelec(
    "nohtml",
    "collectivite_de_travail_changer",
    __("Choix de la collectivite de travail")
);

/**
 *
 */
require_once("../obj/workcollectivite.class.php");

//
$workcollectivite = new workCollectivite("../app/changecollectivite.php");
$workcollectivite->validateForm();

//
$f->setFlag(null);
$f->display();

//
$workcollectivite->showForm();
