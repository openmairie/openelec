<?php
/**
 * Ce fichier permet l'editions generales
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/openelec.class.php";
$f = new openelec(
    null,
    "edition",
    __("Editions Generales")
);

$description = __("Ces editions portent sur tous les electeurs, bureaux de vote confondus.");
$f->displayDescription($description);

// Liste des éditions
$tab_editions = array (
    2 => array (
        'img' => "<span class=\"om-icon om-icon-25 om-icon-fix etiquetteelecteur-25\"><!-- --></span>",
        'titre' => "ETIQUETTES DE PROPAGANDE",
        'pdf' => "../app/index.php?module=form&obj=electeur&action=305&mode_edition=commune&stockage=true",
        'generer' => "OUI",
        'filename' => "etiquettes-electeur-".$_SESSION["collectivite"]."-".$f->normalizeString($_SESSION['libelle_collectivite'])."-liste".$_SESSION["liste"].".pdf",
        'id' => "edition-pdf-etiquetteselecteur",
        "right" => "electeur_edition_pdf__etiquette_electorale",
    ),
    3 => array (
        'img' => "<span class=\"om-icon om-icon-25 om-icon-fix commissionstat-25\"><!-- --></span>",
        'titre' => "STATISTIQUES GENERALES DES MOUVEMENTS",
        'pdf' => "../app/index.php?module=form&obj=mouvement&action=301",
        'id' => "edition-pdf-statistiques-generales-des-mouvements",
        'right' => "mouvement_edition_pdf__statistiques_generales_des_mouvements",
        'generer' => "NON",
    ),
);

//
echo "<table class=\"editions\">\n";

//
$odd = true;
foreach ($tab_editions as $key => $edition) {
    //
    if (array_key_exists("right", $edition) === true
        && $f->isAccredited($edition["right"]) !== true) {
        continue;
    }
    //
    $odd = !$odd;
    echo "\t<tr class=\"editions ".($odd === true ? "odd" : "even")."\">";
    echo "<td class=\"icone\">".$edition ['img']."</td>";
    echo "<td class=\"titre\">".$edition ['titre']."</td>";
    echo "<td class=\"icone\">";
    if ($edition['generer'] == "NON") {
        echo " <a class=\"lien\" id=\"".$edition["id"]."\" href=\"".$edition['pdf']."\"";
        if (!preg_match("#javascript#", $edition['pdf'])) {
            echo " target=\"_blank\"";
        }
        echo ">";
        echo "<span class=\"om-icon om-icon-25 om-icon-fix pdf-25\"><!-- --></span>";
        echo "<br />";
        echo __("Visualiser");
        echo "</a>";
        echo "</td><td>&nbsp;";
    } else {
        //
        $sql = sprintf(
            'SELECT * FROM %1$sfichier WHERE fichier.om_collectivite=%2$s AND fichier.filename=\'%3$s\' ORDER BY creationdate DESC, creationtime DESC LIMIT 1',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $edition['filename']
        );
        $res = $f->db->query($sql);
        $f->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $f->isDatabaseError($res);
        //
        $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
        //
        if ($row != null) {
            echo "<a class=\"lien\" id=\"".$edition["id"]."\" href=\"".OM_ROUTE_FORM."&snippet=file&uid=".$row["uid"]."\" target=\"_blank\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix pdf-25\"><!-- --></span>";
            echo "<br />";
            echo __("Visualiser");
            echo "</a>";
            echo "</td><td>";
            echo " <a class=\"lien\" href=\"".$edition ['pdf']."\" id=\"".$edition["id"]."-generer\">".__("Generer")."</a><br />";
            echo " Taille : [".number_format($row["size"], 0, ',', ' ')." Octets]<br />";
            echo " Date : [". $f->formatDate($row["creationdate"])." à ".$row["creationtime"]."]";
        } else {
            echo "&nbsp;</td><td>";
            echo " <a class=\"lien\" href=\"".$edition ['pdf']."\" id=\"".$edition["id"]."-generer\">".__("Generer")."</a><br />";
            echo __("Aucun fichier n'a ete genere");
        }
    }
    echo "</td>";
    echo "</tr>\n";
}
echo "</table>";
