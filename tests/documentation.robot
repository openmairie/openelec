*** Settings ***
Documentation     TestSuite "Documentation" : cette suite permet d'extraire
...    automatiquement les captures à destination de la documentation.
# On inclut les mots-clefs
Resource    resources/resources.robot
# On ouvre et on ferme le navigateur respectivement au début et à la fin
# du Test Suite.
Suite Setup    For Suite Setup
Suite Teardown    For Suite Teardown
# A chaque début de Test Case on positionne la taille de la fenêtre
# pour obtenir des captures homogènes
Test Setup    Set Window Size  ${1280}  ${1024}


*** Keywords ***
Highlight heading
    [Arguments]  ${locator}

    #Update element style  ${locator}  margin-top  0.75em
    Highlight  ${locator}


*** Test Cases ***
Prérequis

    [Documentation]  L'objet de ce 'Test Case' est de respecter les prérequis
    ...    nécessaires aux captures d'écran.

    [Tags]  doc

    # Création des répertoires destinés à recevoir les captures d'écran
    # selon le respect de l'architecture de la documentation
    Create Directory  results/screenshots
    Create Directory  results/screenshots/preambule
    Create Directory  results/screenshots/editions
    Create Directory  results/screenshots/consultation
    Create Directory  results/screenshots/utilisateur_saisies
    Create Directory  results/screenshots/traitements
    Create Directory  results/screenshots/administration_parametrage


Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de de
    ...    données cohérent pour les scénarios fonctionnels qui suivent.

    [Tags]  doc

    # On veut que la version du logiciel affichée sur les captures soit la majeure
    Copy File  ${EXECDIR}${/}binary_files${/}doc_version.inc.php  ${EXECDIR}${/}..${/}dyn${/}version.inc.php

    #
    Depuis la page d'accueil  admin  admin
    Click Element  css=#dashboard p.datetableau a
    Page Title Should Be  Changement De La Date De Tableau
    Input Text  datetableau  10/01/2018
    Click Element  css=input[name='changedatetableau.action.valid']
    Page Title Should Be  Changement De La Date De Tableau
    Valid Message Should Be  La date de tableau est changée.


Préambule

    [Documentation]  Section 'Préambule'.

    [Tags]  doc

    # Les méthodes Suite Setup et Suite Teardown gèrent l'ouverture et la
    # fermeture du navigateur. Dans le cas de ce TestSuite on a besoin de
    # travailler sur un navigateur fraichement ouvert pour être sûr que la
    # variable de session est neuve.
    Fermer le navigateur
    Ouvrir le navigateur
    Depuis la page de login
    Capture viewport screenshot  screenshots/preambule/a_connexion_formulaire.png
    #
    Input Username    admin
    Input Password    plop
    Click Button    login.action.connect
    WUX  Error Message Should Be    Votre identifiant ou votre mot de passe est incorrect.
    Capture and crop page screenshot  screenshots/preambule/a_connexion_message_erreur.png
    ...  css=div.message
    #
    Input Username    admin
    Input Password    admin
    Click Button    login.action.connect
    Wait Until Element Is Visible    css=#actions a.actions-logout
    Capture and crop page screenshot  screenshots/preambule/a_connexion_message_ok.png
    ...  css=div.message
    #
    Capture and crop page screenshot  screenshots/preambule/a_ergonomie_actions_globales.png
    ...  footer
    Capture and crop page screenshot  screenshots/preambule/a_ergonomie_actions_personnelles.png
    ...  actions
    Capture and crop page screenshot  screenshots/preambule/a_ergonomie_logo.png
    ...  logo
    Capture and crop page screenshot  screenshots/preambule/a_ergonomie_raccourcis.png
    ...  shortlinks
    #
    Highlight heading  css=li.actions-logout
    Capture and crop page screenshot  screenshots/preambule/a_deconnexion_action.png
    ...  header
    #
    Go To Submenu In Menu  saisie  procuration
    Capture and crop page screenshot  screenshots/preambule/a_ergonomie_menu.png
    ...  menu
    #
    Go To Dashboard
    Se déconnecter
    Capture and crop page screenshot  screenshots/preambule/a_deconnexion_message_ok.png
    ...  css=div.message
    #
    Depuis la page d'accueil  admin  admin
    Go To Dashboard
    Remove element  dashboard
    Update element style  css=#content  height  300px
    Add pointy note  css=#logo  Logo  position=right
    Highlight heading  css=#menu
    Add note  css=#menu  Menu  position=right
    Add pointy note  css=#shortlinks  Raccourcis  position=bottom
    Add pointy note  css=#actions  Actions personnelles  position=left
    Highlight heading  css=#footer
    Add note  css=#footer  Actions globales  position=top
    Capture viewport screenshot  screenshots/preambule/a_ergonomie_generale_detail.png
    #
    Go To Dashboard
    Capture and crop page screenshot  screenshots/preambule/a_tableau-de-bord-exemple.png
    ...  content
    #
    Go To Dashboard
    Highlight heading  css=#actions li.liste
    Highlight heading  css=.widget_liste_de_travail
    Capture viewport screenshot  screenshots/preambule/a_liste_en_cours.png
    #
    Go To Dashboard
    Highlight heading  css=.widget_datetableau
    Capture viewport screenshot  screenshots/preambule/a_consultation_de_la_date_de_tableau.png
    Click Element  css=.widget_datetableau p.datetableau a
    Capture and crop page screenshot  screenshots/preambule/a_modifier_la_date_de_tableau.png
    ...  content


Édition

    [Documentation]  Section 'Préambule'.

    [Tags]  doc

    #
    Depuis la page d'accueil  admin  admin
    Go To Dashboard
    # MENU
    Click On Menu Rubrik    edition
    Capture and crop page screenshot  screenshots/editions/a_menu-rubrik-edition.png
    ...    css=#menu-list
    #
    Go To Submenu In Menu  edition  menu-editions-generales
    Capture and crop page screenshot  screenshots/editions/a_edition_generales.png
    ...    css=#content
    #
    Go To Submenu In Menu  edition  menu-editions-par-bureau
    Capture and crop page screenshot  screenshots/editions/a_edition_par_bureau.png
    ...    css=#content
    #
    Go To Submenu In Menu  edition  carte_electorale
    Capture and crop page screenshot  screenshots/editions/a_module_carte_electorale.png
    ...    css=#content
    #
    Go To Submenu In Menu  edition  statistiques
    Capture and crop page screenshot  screenshots/editions/a_edition_statistiques.png
    ...    css=#content
    #
    Go To Submenu In Menu  edition  reqmo
    Capture and crop page screenshot  screenshots/editions/a_edition_requetes_memorisees.png
    ...    css=#content
    #
    Click Element  css=#action-reqmo-list__electeur__pour_une_liste-exporter
    Select From List By Value  css=select#sortie  csv
    Highlight heading  css=select#sortie
    Capture and crop page screenshot  screenshots/editions/a_edition_sortie_csv.png
    ...    css=#content


Consultation

    [Documentation]  Section 'Consultation'.

    [Tags]  doc

    #
    Depuis la page d'accueil  admin  admin
    Go To Dashboard
    # MENU
    Click On Menu Rubrik  consultation
    Capture and crop page screenshot  screenshots/consultation/a_menu-rubrik-consultation.png
    ...    css=#menu-list
    #
    Go To Submenu In Menu  consultation  procurations
    Add pointy note  css=.icons  Actions  width=60  position=top
    Add pointy note  css=div.adv-search-widget input  Recherche  position=right
    Capture and crop page screenshot  screenshots/consultation/a_consultation.png
    ...    css=#content
    #
    Go To Submenu In Menu  consultation  procurations
    Click Element  css=#toggle-advanced-display
    Wait Until Page Contains Element  css=#bureau
    Capture and crop page screenshot  screenshots/consultation/a_consultation_procuration.png
    ...    css=#content
    #
    Go To Submenu In Menu  consultation  electeurs
    Click Link  AZERTY
    Capture and crop page screenshot  screenshots/consultation/a_consultation_fiche_electeur.png
    ...    css=#content

    &{mouvement01} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  bureauforce=Oui
    ...  bureau=1 HOTEL DE VILLE
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDDOCELECTEUR01
    ...  prenom=DOCELECTEUR01
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE DE L'HOTEL DE VILLE
    ${mouvement01_id} =  Ajouter le mouvement d'inscription  ${mouvement01}
    Depuis le contexte de l'inscription  ${mouvement01_id}
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_onglet_piece.png
    ...  css=div#content a#piece
    On clique sur l'onglet  piece  Pièce(s)
    Click On Add Button
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_onglet_piece_formulaire.png
    ...  content

Saisie Électeur

    [Documentation]  Section 'Saisie Électeur'.

    [Tags]  doc

    #
    Depuis la page d'accueil  admin  admin
    Go To Dashboard
    # MENU
    Click On Menu Rubrik  saisie
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_menu-rubrik-saisie.png
    ...    css=#menu-list
    #
    Go To Submenu In Menu  saisie  inscription
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_inscription_recherche.png
    ...    css=#content
    #
    Input Text  css=#nom  LACROIX
    Click Element  css=input[name="inscription_search_form.action.search"]
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_inscription_doublon.png
    ...    css=#content
    #
    Click Element  css=input[name="inscription_doublon_form.action.valid"]
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_inscription_form.png
    ...    css=#content
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_inscription_france.png
    ...    css=#fieldset-form-inscription-etat-civil
    Select From List By Label  naissance_type_saisie  Né à l'étranger
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_inscription_etranger.png
    ...    css=#fieldset-form-inscription-etat-civil
    Select From List By Label  naissance_type_saisie  Né dans un ancien département français d'Algérie
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_inscription_ancien_departement_francais.png
    ...    css=#fieldset-form-inscription-etat-civil
    #
    Go To Submenu In Menu  saisie  modification
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_modification_recherche.png
    ...    css=#content
    Input Text  css=#nom  DUPONT
    Click Element  css=input[name="electeur_search_form.action.valid"]
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_modification_recherche_resultats.png
    ...    css=#content
    Click Element  css=#tab-modification-add_search_form_results tr:not(.mouvement-encours) .modification-16
    Sleep  1
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_modification_form.png
    ...    css=#content
    #
    Go To Submenu In Menu  saisie  radiation
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_radiation_recherche.png
    ...    css=#content
    Input Text  css=#nom  DUPONT
    Click Element  css=input[name="electeur_search_form.action.valid"]
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_radiation_recherche_resultats.png
    ...    css=#content
    Click Element  css=#tab-radiation-add_search_form_results tr:not(.mouvement-encours) .radiation-16
    Sleep  1
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_radiation_form.png
    ...    css=#content
    #
    Go To Submenu In Menu  saisie  procuration
    Capture and crop page screenshot  screenshots/utilisateur_saisies/a_saisie_procuration.png
    ...    css=#content


Traitement

    [Documentation]  Section 'Traitement'.

    [Tags]  doc

    #
    Depuis la page d'accueil  admin  admin
    Go To Dashboard
    # MENU
    Click On Menu Rubrik  traitement
    Capture and crop page screenshot  screenshots/traitements/a_menu-rubrik-traitement.png
    ...    css=#menu-list

    # Commission
    Go To Dashboard
    Go To Submenu In Menu  traitement  commission
    Capture and crop page screenshot  screenshots/traitements/a_module_commission_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_commission.png
    ...    css=#content
    Click Link  Convocation
    Sleep  1
    Capture and crop page screenshot  screenshots/traitements/a_module_commission_membres.png
    ...    css=#content
    Click Link  Membres
    Sleep  1
    Capture and crop page screenshot  screenshots/traitements/a_module_commission_convocation.png
    ...    css=#content

    # Archivage
    Go To Dashboard
    Go To Submenu In Menu  traitement  archivage
    Capture and crop page screenshot  screenshots/traitements/a_module_archivage_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_archivage.png
    ...    css=#content

    # Cartes en retour
    Go To Dashboard
    Go To Submenu In Menu  traitement  carteretour
    Capture and crop page screenshot  screenshots/traitements/a_module_cartes_en_retour_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_cartes_en_retour.png
    ...    css=#content

    # Élection
    Go To Dashboard
    Go To Submenu In Menu  traitement  module-election
    Capture and crop page screenshot  screenshots/traitements/a_module_election_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_election.png
    ...    css=#content

    # Module Procurations
    Go To Dashboard
    Go To Submenu In Menu  traitement  module-procuration
    Capture and crop page screenshot  screenshots/traitements/a_module_procuration_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_procuration.png
    ...    css=#content
    Click Link  Épuration
    Sleep  1
    Capture and crop page screenshot  screenshots/traitements/a_module_procuration_epuration.png
    ...    css=#content

    # Jury
    Go To Dashboard
    Go To Submenu In Menu  traitement  jury
    Capture and crop page screenshot  screenshots/traitements/a_module_jury_d_assises_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_jury_d_assises.png
    ...    css=#content
    On clique sur l'onglet de traitement  jury-parametrage  Paramétrage
    Capture and crop page screenshot  screenshots/traitements/a_module_jury_d_assises_parametrage_collectivite.png
    ...    css=#content
    On clique sur l'onglet de traitement  jury-liste-preparatoire-courante  Liste Préparatoire Courante
    Click Link  DUPONT
    Sleep  1
    Capture and crop page screenshot  screenshots/traitements/a_module_jury_d_assises_jure_effectif.png
    ...    css=#content

    # Jury Suppléant
    Ajouter le paramètre depuis le menu  option_module_jures_suppleants  true  null
    Go To Dashboard
    Go To Submenu In Menu  traitement  jury
    Capture and crop page screenshot  screenshots/traitements/a_module_suppleant_jury_d_assises_tirage_sort.png
    ...    css=#content
    On clique sur l'onglet de traitement  jury-parametrage  Paramétrage
    Capture and crop page screenshot  screenshots/traitements/a_module_suppleant_jury_d_assises_parametrage_collectivite.png
    ...    css=#content
    On clique sur l'onglet de traitement  jury-liste-preparatoire-courante  Liste Préparatoire Courante
    Click Link  DUPONT
    Wait Until Page Contains Element  css=#jury
    Select From List By Label  jury  Suppléant
    Capture and crop page screenshot  screenshots/traitements/a_module_jury_d_assises_suppleant_jure_effectif.png
    ...    css=#content

    # REU
    Go To Dashboard
    Go To Submenu In Menu  traitement  reu
    Capture and crop page screenshot  screenshots/traitements/a_module_reu_menu.png
    ...    css=#menu
    Sleep  5
    Capture and crop page screenshot  screenshots/traitements/a_module_reu_onglet_main.png
    ...    css=#content

    # Module Election REU Scrutin

    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence

    # On accède au module élection et on synchronise les scrutins
    Go To Submenu In Menu  traitement  module-election
    WUX  Click Element  css=#action-tab-reu_scrutin-corner-sync_scrutin
    WUX  Click On Submit Button
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_listing.png
    ...    css=#content
    # Lorsque j-20 n'est pas disponible
    Click Element  css=#action-tab-reu_scrutin-left-consulter-2
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_bloc_pas_propice_j20.png
    ...    css=#arret_listes
    Click On Back Button
    # On accède au scrutin J-20
    Click Link  Test COMJ-20 disponible AZE
    WUX  Element Should Contain  css=#libelle  Test COMJ-20 disponible AZE
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_j20.png
    ...    css=#content
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_action_j20.png
    ...    css=#portlet-actions
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_bloc_propice_j20.png
    ...    css=#arret_listes
    # On demande le livrable j-20
    Click On Form Portlet Action  reu_scrutin  demander_j20_livrable  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test COMJ-20 disponible AZE
    WUX  Element Should Contain  css=#libelle  Test COMJ-20 disponible AZE
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_j20_attente.png
    ...    css=#arret_listes
    # Synchronisation des notifications pour récupérer la notification livrable j20
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_livrable_j20.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_j20.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  290
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test COMJ-20 disponible AZE
    WUX  Element Should Contain  css=#libelle  Test COMJ-20 disponible AZE
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_j20_valid.png
    ...    css=#arret_listes
    Click Link  Générer
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_j20_edition.png
    ...    css=#edition_arret_listes

    Go To Submenu In Menu  traitement  module-election
    Click Link  Test COMJ-20 disponible AZE
    WUX  Element Should Contain  css=#libelle  Test COMJ-20 disponible AZE
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_bloc_pas_propice_j5.png
    ...    css=#mouv_j5

    Go To Submenu In Menu  traitement  module-election
    Click Link  Test J-5 disponible
    WUX  Element Should Contain  css=#libelle  Test J-5 disponible
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_bloc_propice_j5.png
    ...    css=#mouv_j5
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_action_j5.png
    ...    css=#portlet-actions

    Click On Form Portlet Action  reu_scrutin  demander_j5_livrable  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test J-5 disponible
    WUX  Element Should Contain  css=#libelle  Test J-5 disponible
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_j5_attente.png
    ...    css=#mouv_j5
    # Synchronisation des notifications pour récupérer la notification livrable
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_livrable_j5.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_j5.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  291

    Go To Submenu In Menu  traitement  module-election
    Click Link  Test J-5 disponible
    WUX  Element Should Contain  css=#libelle  Test J-5 disponible
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_j5_valid.png
    ...    css=#mouv_j5

    Go To Submenu In Menu  traitement  module-election
    Click Link  Test aucune action dispo
    WUX  Element Should Contain  css=#libelle  Test aucune action dispo
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_action_emarge.png
    ...    css=#portlet-actions
    Click On Form Portlet Action  reu_scrutin  demander_emarge_livrable  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test aucune action dispo
    WUX  Element Should Contain  css=#libelle  Test aucune action dispo
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_emarge_attente.png
    ...    css=#liste_emarge
    # Synchronisation des notifications pour récupérer la notification livrable
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_livrable_emarge.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_emarge.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  292

    Go To Submenu In Menu  traitement  module-election
    Click Link  Test aucune action dispo
    WUX  Element Should Contain  css=#libelle  Test aucune action dispo
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_emarge_valid.png
    ...    css=#liste_emarge
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_edition_recap_mention.png
    ...    css=#edition_recap_mention
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_edition_liste_elec.png
    ...    css=#edition_liste_elec_par_bureau
    Capture and crop page screenshot  screenshots/traitements/a_module_election_scrutin_livrable_edition_emarge.png
    ...    css=#edition_liste_emarge_par_bureau

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut

    # Refonte
    Go To Dashboard
    Go To Submenu In Menu  traitement  refonte
    Capture and crop page screenshot  screenshots/traitements/a_module_refonte_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_refonte.png
    ...    css=#content


    # Traitement Annuel
    Go To Dashboard
    Go To Submenu In Menu  traitement  module-traitement-annuel
    Capture and crop page screenshot  screenshots/traitements/a_module_traitement_fin_d_annee_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_traitement_fin_d_annee.png
    ...    css=#content


    # Traitement J-5
    Go To Dashboard
    Go To Submenu In Menu  traitement  module-traitement-j5
    Capture and crop page screenshot  screenshots/traitements/a_module_traitement_j5_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_traitement_j5.png
    ...    css=#content

    # Redécoupage
    Go To Dashboard
    Go To Submenu In Menu  traitement  redecoupage
    Capture and crop page screenshot  screenshots/traitements/a_module_redecoupage_menu.png
    ...    css=#menu
    Capture and crop page screenshot  screenshots/traitements/a_module_redecoupage.png
    ...    css=#content
    On clique sur l'onglet de traitement  traitement_redecoupage  Redécoupage
    Capture and crop page screenshot  screenshots/traitements/a_module_redecoupage_redecoupage.png
    ...    css=#content
    On clique sur l'onglet de traitement  decoupage_initialisation  Initialisation
    Capture and crop page screenshot  screenshots/traitements/a_module_redecoupage_initialisation.png
    ...    css=#content


Administration & paramétrage

    [Documentation]  Section 'Administration & Paramétrage'.

    [Tags]  doc

    #
    Depuis la page d'accueil  admin  admin
    Go To Dashboard
    # MENU
    Depuis le menu 'Administration & Paramétrage'
    Capture and crop page screenshot  screenshots/administration_parametrage/a_menu-rubrik-administration.png
    ...    css=#content

    #
    Depuis le listing des utilisateurs
    Capture and crop page screenshot  screenshots/administration_parametrage/a_parametrage_utilisateurs.png
    ...    css=#content

    #
    Depuis le listing  param_mouvement
    Capture and crop page screenshot  screenshots/administration_parametrage/a_parametrage_mouvements.png
    ...    css=#content

    #
    Depuis le listing des voies
    Capture and crop page screenshot  screenshots/administration_parametrage/a_decoupage_voie.png
    ...    css=#content
    Click Link  AVENUE DE LA LIBERATION
    Click Element  css=#decoupage
    WUX  Page Should Contain  90001
    Click Link  90001
    WUX  Element Should Be Visible  css=#fieldset-sousform-decoupage-identification
    Capture and crop page screenshot  screenshots/administration_parametrage/a_decoupage_voie_affection_bureau.png
    ...    css=#content

    # Notification par courriel aux utilisateurs
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Initialiser un statut valide  Oui
    # On traite les notifications afin d'avoir une tâches en cours sur le tableau de bord
    Synchroniser les listes électorales

    # Synchronisation des notifications pour récupérer une taches en cours
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_taches_en_cours_courriel.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_taches_en_cours_courriel.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

    ${json} =  Set Variable  { "module": "synchro_last_notifications"}
    Appeler le web service  Post  maintenance  ${json}
    ${json} =  Set Variable  { "module": "treat_all_notifications"}
    Appeler le web service  Post  maintenance  ${json}

    Accéder au controlpanel notification par courriel
    Capture and crop page screenshot  screenshots/administration_parametrage/a_notification_courriel_mono.png
    ...    css=#content

    Depuis la page d'accueil  multi  admin
    Accéder au controlpanel notification par courriel
    Capture and crop page screenshot  screenshots/administration_parametrage/a_notification_courriel_multi.png
    ...    css=#content

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut


Multi

    [Documentation]  Section 'Multi'.

    [Tags]  doc

    #
    Depuis la page d'accueil  multi  admin
    Go To Dashboard
    #
    Click Element  css=#actions li.collectivite a.collectivite
    Capture and crop page screenshot  screenshots/module_multi/a_choix_de_la_collectivite_de_travail.png
    ...    css=#content
    #
    Go To Submenu In Menu  traitement  menu-traitements-multi
    Page Title Should Be  Traitement > Module Multi Collectivités
    Capture and crop page screenshot  screenshots/module_multi/a_traitement_multi_collectivite_step_1.png
    ...    css=#content
    Click Element  css=#selectall
    Click Element  css=input[type="submit"]
    Capture and crop page screenshot  screenshots/module_multi/a_traitement_multi_collectivite_step_2.png
    ...    css=#content
    #
    Click Link  Facturation
    WUX  Page Should Contain  Ce module permet de générer les documents nécessaires à la facturation des communes. Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs.
    La page ne doit pas contenir d'erreur
    Capture and crop page screenshot  screenshots/module_multi/a_traitement_facturation.png
    ...    css=#content
    #
    Click Element  css=#module-multi-tab-statistiques
    WUX  Page Should Contain  Ce module permet de générer les documents nécessaires aux statistiques sur la saisie. Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs.
    La page ne doit pas contenir d'erreur
    Capture and crop page screenshot  screenshots/module_multi/a_traitement_statistiques.png
    ...    css=#content


Déconstitution du jeu de données
    [Documentation]
    [Tags]  doc
    #
    Remove File  ${EXECDIR}${/}..${/}dyn${/}version.inc.php

