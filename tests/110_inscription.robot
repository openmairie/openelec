*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Inscription.


*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    Depuis la page d'accueil  admin  admin

    # Création d'une voie avec un tiret pour test de non régression
    # Dans une version précédente l'ajout d'une inscription était impossible
    &{voie01} =  Create Dictionary
    ...  libelle_voie=RUE DE LA - LOI
    ...  cp=96125
    ...  ville=LIBREVILLE
    ${voie01_id} =  Ajouter la voie  ${voie01}
    Set Suite Variable  ${voie01}
    Set Suite Variable  ${voie01_id}
    Changer la date de tableau  10/01/2015


Saisie d'une nouvelle inscription
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  saisie  inscription
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Recherche De Doublon
    CheckBox Should Be Selected  css=#exact

    ## Pas de critères

    # Aucun critère
    Click On Submit Button Until Message  Vous devez saisir au moins un critère de recherche.
    Error Message Should Be  Vous devez saisir au moins un critère de recherche.
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Recherche De Doublon
    Submenu In Menu Should Be Selected  saisie  inscription
    CheckBox Should Be Selected  css=#exact

    ## Doublon avec recherche exacte

    # Doublon avec recherche exacte
    Input Text  css=#nom  BIG
    Click On Submit Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Résultats De La Recherche De Doublon
    Submenu In Menu Should Be Selected  saisie  inscription

    # Le clic sur le bouton Inscrire doit nous amener au formulaire
    Click On Submit Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Inscription
    Submenu In Menu Should Be Selected  saisie  inscription
    Form Value Should Be  css=#nom  BIG

    # La liste à choix du widget de saisie du lieu de naissance ne doit pas nous
    # présenter la proposition 'Lieu de naissance non référencé'
    ${naissance_type_saisie_liste_label} =  Create List
    ...  Né en France
    ...  Né à l'étranger
    ...  Né dans un ancien département français d'Algérie
    Select List Should Be  css=#naissance_type_saisie  ${naissance_type_saisie_liste_label}

    # Le retour doit nous ramener aux résultats de recherche
    Click On Back Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Résultats De La Recherche De Doublon
    Submenu In Menu Should Be Selected  saisie  inscription

    # Le retour doit nous ramener à la recherche
    Click On Back Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Recherche De Doublon
    Submenu In Menu Should Be Selected  saisie  inscription
    Form Value Should Be  css=#nom  BIG
    CheckBox Should Be Selected  css=#exact

    ## Doublon sans recherche exacte

    # Doublon sans recherche exacte
    Input Text  css=#nom  LA
    Unselect Checkbox  css=#exact
    Click On Submit Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Résultats De La Recherche De Doublon
    Submenu In Menu Should Be Selected  saisie  inscription

    # Le clic sur le bouton Inscrire doit nous amener au formulaire
    Click On Submit Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Inscription
    Submenu In Menu Should Be Selected  saisie  inscription
    Form Value Should Be  css=#nom  LA

    # Le retour doit nous ramener aux résultats de recherche
    Click On Back Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Résultats De La Recherche De Doublon
    Submenu In Menu Should Be Selected  saisie  inscription

    # Le retour doit nous ramener à la recherche
    Click On Back Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Recherche De Doublon
    Submenu In Menu Should Be Selected  saisie  inscription
    Form Value Should Be  css=#nom  LA
    CheckBox Should Not Be Selected  css=#exact

    ## Nom sans doublon

    # Nom sans aucune correspondance
    Input Text  css=#nom  CEN'OMNEXISTEPAS
    Click On Submit Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Inscription
    Submenu In Menu Should Be Selected  saisie  inscription
    Form Value Should Be  css=#nom  CEN'OMNEXISTEPAS

    # Le retour doit nous ramener à la recherche
    Click On Back Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Recherche De Doublon
    Submenu In Menu Should Be Selected  saisie  inscription
    Form Value Should Be  css=#nom  CEN'OMNEXISTEPAS

    #
    Input Text  css=#datenaissance  01/01/1900
    Click On Submit Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Inscription
    Submenu In Menu Should Be Selected  saisie  inscription
    Form Value Should Be  css=#nom  CEN'OMNEXISTEPAS
    Form Value Should Be  css=#date_naissance  01/01/1900

    # Vérification des doublons
    Click Link  Vérification des doublons
    ${contenu_pdf} =  Create List  CEN'OMNEXISTEPAS  Statistiques - Doublon
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    #
    Input Text  css=#date_demande  ${DATE_FORMAT_DD/MM/YYYY}
    Select From List By Label  css=#types  PREMIERE INSCRIPTION
    Select From List By Label  css=#bureau  1 HOTEL DE VILLE
    Select From List By Label  css=#bureauforce  Oui
    Input Text  css=#nom  MASURE
    Input Text  css=#prenom  NOLAN
    Select From List By Label  css=#sexe  Féminin
    Input Text  css=#date_naissance  14/03/1965
    #Input Text  css=#code_departement_naissance  13
    #Input Text  css=#libelle_departement_naissance  BOUCHES-DU-RHONE
    #Input Text  css=#code_lieu_de_naissance  13 004
    #Input Text  css=#libelle_lieu_de_naissance  ARLES
    Input Text  css=#autocomplete-commune-naissance-search  arles 13
    WUX  Click On Link  13 004 - ARLES

    # Saisie de voie avec autocomplétion
    Input Text  css=#autocomplete-voie-search  basse
    WUX  Click On Link  RUE BASSE

    # Saisie de commune de provenance avec autocomplétion
    Input Text  css=#autocomplete-commune-search  chat vert
    WUX  Click On Link  83 039 - CHATEAUVERT

    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${mouvement_id} =  Get Element Attribute  css=.form-content #id  value
    Page Title Should Be  Consultation > Inscription > ${mouvement_id} - Mme MASURE NOLAN
    First Tab Title Should Be  Inscription
    Submenu In Menu Should Be Selected  consultation  inscriptions
    Portlet Action Should Be In Form  inscription  modifier
    Portlet Action Should Be In Form  inscription  supprimer

    #
    Click On Back Button
    Page Title Should Be  Consultation > Inscription
    First Tab Title Should Be  Inscription
    Submenu In Menu Should Be Selected  consultation  inscriptions

    #
    Go To Submenu In Menu  saisie  inscription
    Input Text  css=#nom   MASURE
    Click On Submit Button
    Page Title Should Be  Saisie > Nouvelle Inscription
    First Tab Title Should Be  Résultats De La Recherche De Doublon
    Submenu In Menu Should Be Selected  saisie  inscription


Vérification de la présence du libelle lieu de naissance dans ajout et modif d'inscription
    &{mouvement01} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST110ELECTEUR01
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    # ...  ne_en_france=13 105 - SENAS
    # ...  code_commune_de_naissance=13 105
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE BASSE
    ${mouvement01_id} =  Ajouter le mouvement d'inscription  ${mouvement01}

    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement01.libelle_commune_de_naissance}
    Click On Form Portlet Action  inscription  modifier
    # Vérification du libellé dans un autocomplete
    Form Value Should Be  css=#autocomplete-commune-naissance-search  ${mouvement01.commune_de_naissance}

    &{mouvement02} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST110ELECTEUR02
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né à l'étranger
    ...  pays_de_naissance=99134 - ROYAUME D'ESPAGNE
    # ...  ne_a_l_etranger=99134 - ROYAUME D'ESPAGNE
    ...  libelle_lieu_de_naissance=MADRID
    ...  libelle_voie=RUE BASSE
    ${mouvement02_id} =  Ajouter le mouvement d'inscription  ${mouvement02}

    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement02.libelle_lieu_de_naissance}
    Click On Form Portlet Action  inscription  modifier
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement02.libelle_lieu_de_naissance}


    &{mouvement03} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST110ELECTEUR03
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né à l'étranger
    ...  pays_de_naissance=99 - ETRANGER
    ...  libelle_lieu_de_naissance=MEXICO
    ...  libelle_voie=RUE BASSE
    ${mouvement03_id} =  Ajouter le mouvement d'inscription  ${mouvement03}

    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement03.libelle_lieu_de_naissance}
    Click On Form Portlet Action  inscription  modifier
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement03.libelle_lieu_de_naissance}

    &{mouvement04} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST110ELECTEUR04
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né dans un ancien département français d'Algérie
    ...  ancien_departement_francais_algerie=91352 - ALGER
    ...  libelle_lieu_de_naissance=EL MADANIA
    ...  libelle_voie=RUE BASSE
    ${mouvement04_id} =  Ajouter le mouvement d'inscription  ${mouvement04}

    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement04.libelle_lieu_de_naissance}
    Click On Form Portlet Action  inscription  modifier
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement04.libelle_lieu_de_naissance}
    #
    Appliquer le traitement de fin d'année



Listing des inscriptions
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  consultation  inscriptions
    Page Title Should Be  Consultation > Inscription
    First Tab Title Should Be  Inscription
    Element Should Be Visible  css=#tab-inscription


Recherche par date de naissance dans le listing des inscriptions
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  consultation  inscriptions
    # Aucun résultat
    Rechercher en recherche avancée simple  01/01/1754
    Page Should Contain  Aucun enregistrement.
    # Recherche générale avec une date complète
    Rechercher en recherche avancée simple  30/01/1968
    Page Should Contain  30/01/1968 à LAPEYROUSE-MORNAY
    Page Should Not Contain  12/03/1992 à AURONS
    # Recherche générale avec une date tronquée
    Rechercher en recherche avancée simple  05/1968
    Page Should Contain  20/05/1968 à FUVEAU
    Page Should Contain  20/05/1968 à ARACHES
    Page Should Not Contain  30/01/1968 à LAPEYROUSE-MORNAY
    # Recherche générale avec l'année
    Rechercher en recherche avancée simple  1968
    Page Should Contain  20/05/1968 à FUVEAU
    Page Should Contain  30/01/1968 à LAPEYROUSE-MORNAY
    Page Should Not Contain  12/03/1992 à AURONS
    # Recherche sur date de naissance avec une date tronquée
    Rechercher en recherche avancée simple  05/1968
    Page Should Contain  20/05/1968 à FUVEAU
    Page Should Contain  20/05/1968 à ARACHES
    Page Should Not Contain  30/01/1968 à LAPEYROUSE-MORNAY


Vérification de l'ajout d'une inscription avec un libellé de voie avec tiret

    &{mouvement05} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  bureauforce=Oui
    ...  bureau=1 HOTEL DE VILLE
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST110ELECTEUR05
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=${voie01.libelle_voie}
    ${mouvement05_id} =  Ajouter le mouvement d'inscription  ${mouvement05}


Vérification de l'ajout des pièces dans un mouvement d'inscription
    [Documentation]

    #Vérification des lien d'action dans la vue synthétique
    Depuis la page d'accueil  admin  admin
    &{mouvement_06} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  bureauforce=Oui
    ...  bureau=1 HOTEL DE VILLE
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST110ELECTEUR06
    ...  prenom=TEST110ELECTEUR06
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=${voie01.libelle_voie}
    ${mouvement06_id} =  Ajouter le mouvement d'inscription  ${mouvement06}

    &{piece_01} =  Create Dictionary
    ...  libelle=CERFA110IN2019
    ...  piece_type=Cerfa
    ...  fichier=manuel.pdf
    ...  type_mouvement=inscription
    ...  mouvement=${mouvement06_id}
    ${piece_01_id} =  Ajouter une piece au mouvement  ${piece_01}

    &{piece_recup} =  Create Dictionary
    ...  piece_id=${piece_01_id}
    ...  type_mouvement=inscription
    ...  mouvement=${mouvement06_id}

    ${piece01_uid} =  Récupérer l'uid de la pièce  ${piece_recup}
    ${piece01_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${piece01_uid}
    ${piece01_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${piece01_uid}
    Le fichier doit exister  ${piece01_path}
    Le fichier doit exister  ${piece01_md_path}

    Appliquer le traitement de fin d'année
    Appliquer l'archivage

    File Should Not Exist  ${piece01_path}
    File Should Not Exist  ${piece01_md_path}


Message d'avertissement lorsque les adresses de contact et de rattachement sont identiques
    [Documentation]  Depuis la page de consultation d’une inscription, lorsque l’adresse de rattachement
    ...  de l'électeur est identique à son adresse de contact, un message d'avertissement est affiché. 

    &{mouvement} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  bureauforce=Oui
    ...  bureau=1 HOTEL DE VILLE
    ...  types=PREMIERE INSCRIPTION
    ...  nom=TST_WARN_MSG
    ...  prenom=ADRESSE_IDENTIQUE
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=${voie01.libelle_voie}
    ...  resident=Oui
    ...  adresse_resident=0 ${voie01.libelle_voie}
    ...  cp_resident=${voie01.cp}
    ...  ville_resident=plop
    Acceder au formulaire d'ajout d'un mouvement  TST_WARN_MSG
    Remplir le formulaire de mouvement d'inscription  ${mouvement}
    Click On Submit Button
    WUX  Element Should Contain  css=.panel_information.ui-state-warning  L'adresse de rattachement de l'électeur est identique à son adresse de contact.
