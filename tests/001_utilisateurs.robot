*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Synchronisation des utilisateurs avec un annuaire LDAP

    [Documentation]  On teste la synchronisation des utilisateurs avec le ldap
    ...  Les utilisateurs qui devront être ajoutés et mis à jour :
    ...  einstein, newton, galieleo, tesla
    ...  Et les utilisateurs qui devront être supprimés :
    ...  ldap_instructeur et ldap_service

    [Tags]  exclude

    #
    Depuis la page d'accueil  admin  admin
    # On accède à l'écran de synchronisation via le menu
    Depuis le menu 'Administration & Paramétrage'
    Click Element  css=.annuaire
    # On vérifie le titre de l'écran
    Page Title Should Be  Administration & Paramétrage > Gestion Des Utilisateurs > Utilisateur > Synchronisation Annuaire
    # ATTENTION POSTULAT : Il y a deux utilisateurs LDAP dans la base
    # et le ldap auquel nous sommes connectés contient 4 utilisateurs qui ne
    # sont pas les deux déjà en base
    Page Should Contain  Il y a 4 utilisateur(s) présent(s) dans l'annuaire et non présent(s) dans la base => 4 ajout(s)
    Page Should Contain  Il y a 0 utilisateur(s) présent(s) dans la base et non présent(s) dans l'annuaire => 0 suppression(s)
    Page Should Contain  Il y a 0 utilisateur(s) présent(s) à la fois dans la base et l'annuaire => 0 mise(s) à jour
    # On clique sur "Synchroniser"
    Click On Submit Button Until Message  La synchronisation des utilisateurs est terminée.
    # On vérifie que tout s'est bien passé
    Valid Message Should Be  La synchronisation des utilisateurs est terminée.

    # On vérifie que les 3 utilisateurs sont bien présents avec l'information LDAP
    Depuis le contexte de l'utilisateur  einstein
    Depuis le contexte de l'utilisateur  newton
    Depuis le contexte de l'utilisateur  galieleo
    Depuis le contexte de l'utilisateur  tesla

    # On supprime un des 3 utilisateurs
    Supprimer l'utilisateur  galieleo

    # On retourne au tableau de bord
    Go To Dashboard
    # On accède à l'écran de synchronisation via le menu
    Depuis le menu 'Administration & Paramétrage'
    Click Element  css=.annuaire
    # On vérifie le titre de l'écran
    Page Title Should Be  Administration & Paramétrage > Gestion Des Utilisateurs > Utilisateur > Synchronisation Annuaire
    # ATTENTION POSTULAT : Il n'y a aucun utilisateur LDAP dans la base
    # et le ldap auquel nous sommes connectés contient 3 utilisateurs
    Page Should Contain  Il y a 1 utilisateur(s) présent(s) dans l'annuaire et non présent(s) dans la base => 1 ajout(s)
    Page Should Contain  Il y a 0 utilisateur(s) présent(s) dans la base et non présent(s) dans l'annuaire => 0 suppression(s)
    Page Should Contain  Il y a 3 utilisateur(s) présent(s) à la fois dans la base et l'annuaire => 3 mise(s) à jour
    # On clique sur "Synchroniser"
    Click On Submit Button Until Message  La synchronisation des utilisateurs est terminée.
    # On vérifie que tout s'est bien passé
    Valid Message Should Be  La synchronisation des utilisateurs est terminée.