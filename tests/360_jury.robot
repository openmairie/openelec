*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite des jurys d'assises


*** Test Cases ***
Éditions des informations de juré
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  jury  # Traitement → Jury D'assises
    Page Title Should Be  Traitement > Module Jury D'assises

    #
    Tirer au sort les jurés titulaires
    WUX  Valid Message Should Contain  Le traitement est terminé.

    # On accède à l'onglet 'Liste Préparatoire Courante'
    Click Link  Liste préparatoire courante
    WUX  Click Element  css=tr:nth-child(2) .edit-16  # Modifier le 1er électeur
    WUX  Select From List By Label  css=#sousform-electeur_jury select[name='jury_effectif']  Oui
    Input Text  name=profession  ostréiculteur
    Clear Element Text  name=date_jeffectif
    Click On Submit Button In Subform
    #
    La page ne doit pas contenir d'erreur
    WUX  Page Should Contain  Date de prefecture obligatoire
    Input Text  name=date_jeffectif  06/08/2013
    Click On Submit Button In Subform
    WUX  Page Should Contain  Vos modifications ont bien été enregistrées.
    ${electeur_id} =  Get Value  name=id

    ## On revient sur l'onglet 'Tirage Aléatoire'
    Accéder à l'onglet Tirage Aléatoire du module Jury D'assises
    # Vérification édition pdf étiquette
    WUX  Click Element  css=#action-module-jury-pdf-edtiquettes
    ${contenu_pdf} =  Create List  35210 LIBREVILLE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    # Vérification édition pdf
    WUX  Click Element  css=#action-module-jury-pdf-liste-preparatoire
    ${contenu_pdf} =  Create List
    ...  Liste préparatoire du jury d'assises - CANTON DE LIBREVILLE
    ...  ostréiculteur
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    # Vérification export CSV
    Click Link  Export CSV des électeurs sélectionnés
    WUX  Click On Submit Button In Reqmo
    Page Title Should Be  Export > Requêtes Mémorisées > List__electeur_jury
    Page Should Contain  ostréiculteur

    # Consultation → Liste électorale
    Go To Submenu In Menu  consultation  electeurs
    Rechercher en recherche avancée simple  ${electeur_id}
    Click Link  ${electeur_id}
    Page Should Contain  Jury d'assises :
    Page Should Contain  L'électeur fait parti de la liste provisoire courante.
    Page Should Contain  Profession de l'électeur : ostréiculteur
    Page Should Contain  L'électeur est juré effectif le : 06/08/2013

    Click Link  Juré d'assises

    WUX  Element Should Be Visible  css=#ui-dialog-title-sousform-electeur_jury
    WUX  Select From List By Label  css=#sousform-electeur_jury select[name='jury_effectif']  Non
    WUX  Click Element  css=#sousform-electeur_jury input[type='submit']
    WUX  Element Should Contain  css=#sousform-electeur_jury  Vos modifications ont bien été enregistrées.
    WUX  Click Element  css=#sousform-electeur_jury a.retour
    WUX  Element Should Not Be Visible  css=#ui-dialog-title-sousform-electeur_jury
    Page Should Not Contain   Date préfecture  # sous formulaire fermés
    Page Should Not Contain  L'électeur est juré effectif le : 06/08/2013

Vérification du message d'erreur lors du tirage aléatoire
    [Documentation]  Permet de vérifier que lorsque nous avons un nombre de jurés à 0 dans le paramétrage, un message d'erreur nous prévient.

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  jury
    # On accède au paramétrage pour changer le nombre de jurés à tirer.
    Click Element  css=#jury-parametrage
    WUX  Click Element  css=#action-soustab-parametrage_nb_jures-left-consulter-1
    WUX  Click On SubForm Portlet Action  parametrage_nb_jures  modifier
    # On met le nombre de jurés à 0.
    Input Text  css=#nb_jures  0
    Click On Submit Button In SubForm
    # On fait le tirage aléatoire et on vérifie que le message d'erreur est bien présent.
    Accéder à l'onglet Tirage Aléatoire du module Jury D'assises
    WUX  Element Should Be Visible  name:traitement.jury.form.valid
    Click Element  name:traitement.jury.form.valid
    Handle Alert
    WUX  Error Message Should Contain  Le nombre d'électeurs à tirer au sort est à 0.

    # On remet le nombre de jurés à 10
    Click Element  css=#jury-parametrage
    WUX  Click Element  css=#action-soustab-parametrage_nb_jures-left-consulter-1
    WUX  Click On SubForm Portlet Action  parametrage_nb_jures  modifier
    Input Text  css=#nb_jures  10
    Click On Submit Button In SubForm
