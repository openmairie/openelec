*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Module Traitement J-5.


*** Test Cases ***
Fonctionnement basique
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    ${datetableau} =  Set Variable  10/01/2014
    #
    Changer la date de tableau  10/01/2014

    #
    Depuis le listing  param_mouvement
    Use Simple Search  Tous  CHANGEMENT ADRESSE
    Click Link  CHANGEMENT ADRESSE
    Click On Form Portlet Action  param_mouvement  modifier
    Select From List By Label  css=#effet  Immédiat
    Click On Submit Button

    # Création de deux électeurs pour faire des mouvements de modification
    # et de radition
    &{mouvement01} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST400ELECTEUR01
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  code_commune_de_naissance=13 105
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE BASSE
    ${mouvement01_id} =  Ajouter le mouvement d'inscription  ${mouvement01}
    #
    &{mouvement02} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST400ELECTEUR02
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  code_commune_de_naissance=13 105
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE BASSE
    ${mouvement02_id} =  Ajouter le mouvement d'inscription  ${mouvement02}
    #
    Appliquer le traitement de fin d'année

    # Nouvelle inscription à effet Immédiat
    &{mouvement11} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=INSCRIPTION JUDICIAIRE
    ...  nom=DURANDTEST400IMMEDIAT
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  code_commune_de_naissance=13 105
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE BASSE
    ${mouvement11_id} =  Ajouter le mouvement d'inscription  ${mouvement11}
    # Nouvelle inscription à effet Annuel
    &{mouvement12} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST400ANNUEL
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  code_commune_de_naissance=13 105
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE BASSE
    ${mouvement12_id} =  Ajouter le mouvement d'inscription  ${mouvement12}
    # Modification d'adresse
    &{mouvement13} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  electeur_nom=${mouvement01.nom}
    ...  types=CHANGEMENT ADRESSE
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  code_commune_de_naissance=13 105
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE BASSE
    ...  complement=ETAGE 1
    ${mouvement13_id} =  Ajouter le mouvement de modification  ${mouvement13}
    # Radiation
    &{mouvement14} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  electeur_nom=${mouvement02.nom}
    ...  types=DECES
    ${mouvement14_id} =  Ajouter le mouvement de radiation  ${mouvement14}
    #
    Go To Submenu In Menu  traitement  module-traitement-j5
    Page Title Should Be  Traitement > Module Traitement J-5
    #
    Element Should Contain  css=#traitement_j5 div.alert  ${datetableau}
    #
    Select Checkbox  mouvementatraiter[]
    Click Button  Valider la sélection
    WUX  Element Should Contain  css=#traitement_j5  Étape 2 - Vérification et application du Traitement J-5 du ${DATE_FORMAT_DD/MM/YYYY} [Tableau du ${datetableau}]
    La page ne doit pas contenir d'erreur
    #
    WUX  Click Element  css=#action-traitement_j5-pdf-recapitulatif
    ${contenu_pdf} =  Create List
    ...  Détail des mouvements à appliquer au traitement J-5 du ${DATE_FORMAT_DD/MM/YYYY} [Tableau du ${datetableau}]
    ...  LP - LISTE PRINCIPALE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Button  Appliquer le traitement J-5
    Handle Alert
    WUX  Valid Message Should Be  Le traitement est terminé. Voir le détail
    La page ne doit pas contenir d'erreur

    # L'électeur doit être dans la liste électorale
    Go To Submenu In Menu  consultation  electeurs
    Rechercher en recherche avancée simple  ${mouvement11.nom}
    Page Should Contain  1 - 1 enregistrement(s) sur 1 = [${mouvement11.nom}]

    # L'électeur ne doit pas être dans la liste électorale
    Go To Submenu In Menu  consultation  electeurs
    Rechercher en recherche avancée simple  ${mouvement12.nom}
    Page Should Contain  1 - 0 enregistrement(s) sur 0 = [${mouvement12.nom}]

    # L'électeur ne doit pas être dans la liste électorale
    Go To Submenu In Menu  consultation  electeurs
    Rechercher en recherche avancée simple  ${mouvement02.nom}
    Page Should Contain  1 - 0 enregistrement(s) sur 0 = [${mouvement02.nom}]

    # L'électeur doit avoir le nouveau complément dans son adresse
    Go To Submenu In Menu  consultation  electeurs
    Rechercher en recherche avancée simple  ${mouvement01.nom}
    Click Element  css=[id*="action-tab-electeur-left-consulter-"]
    WUX  Element Should Contain  css=div.adresse p.contenu  ${mouvement13.complement}

