*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Module REU

*** Test Cases ***
Vérification de la partie interface du module reu
    Réinitialiser les bureaux de vote REU
    #Initialisation des fichiers txt
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_reu_all_false.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_reu_all_true.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_reu_config.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu_all_false.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu.txt
    #Vérification sans le fichier reu.inc.php
    Depuis La Page D'accueil  admin  admin
    Remove File  ..${/}dyn${/}reu.inc.php
    Accéder au module REU
    WUX  Element Should Contain  css=#config_system  ERROR
    WUX  Element Should Contain  css=#param_ugle  ERROR
    WUX  Element Should Contain  css=#param_compte_logiciel  ERROR
    WUX  Element Should Contain  css=#api_reu_dispo  ERROR
    WUX  Element Should Contain  css=#connexion_logicielle  ERROR
    #Lorsque le tableau du reu.inc.php est vide.
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu_vide.inc.php  ..${/}dyn${/}
    Move File  ..${/}dyn${/}reu_vide.inc.php  ..${/}dyn${/}reu.inc.php
    Depuis La Page D'accueil  admin  admin
    Accéder au module REU
    WUX  Element Should Contain  css=#config_system  ERROR
    WUX  Element Should Contain  css=#param_ugle  ERROR
    WUX  Element Should Contain  css=#param_compte_logiciel  ERROR
    WUX  Element Should Contain  css=#api_reu_dispo  ERROR
    WUX  Element Should Contain  css=#connexion_logicielle  ERROR
    Remove File  ..${/}dyn${/}reu.inc.php
    #Vérification avec le fichier reu.inc.php et le config systeme à true
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis La Page D'accueil  admin  admin
    Accéder au module REU
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu_all_false.txt
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu_config.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu.txt
    Depuis La Page D'accueil  admin  admin
    Accéder au module REU
    Accéder au module REU
    WUX  Element Should Contain  css=#config_system  OK
    WUX  Element Should Contain  css=#param_ugle  ERROR
    WUX  Element Should Contain  css=#param_compte_logiciel  ERROR
    WUX  Element Should Contain  css=#api_reu_dispo  ERROR
    WUX  Element Should Contain  css=#connexion_logicielle  ERROR
    WUX  Element Should Contain  css=#actu  Aucune actualité.
    #Vérification avec toute les methodes à true sauf ugle et compte logiciel
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu_config.txt
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu_all_true.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu.txt
    Depuis La Page D'accueil  admin  admin
    Accéder au module REU
    WUX  Element Should Contain  css=#config_system  OK
    WUX  Element Should Contain  css=#param_ugle  ERROR
    WUX  Element Should Contain  css=#param_compte_logiciel  ERROR
    WUX  Element Should Contain  css=#api_reu_dispo  OK
    WUX  Element Should Contain  css=#connexion_logicielle  OK
    WUX  Element Should Contain  css=#actu  Aucune actualité.
    #Ajout du compte logiciel et de l'ugle
    ${ugle} =  Set Variable  ugle
    ${compte_logiciel} =  Set Variable  compte logiciel
    Saisir l'UGLE  ${ugle}
    Saisir le compte logiciel  ${compte_logiciel}
    WUX  Element Should Contain  css=#config_system  OK
    WUX  Element Should Contain  css=#param_ugle  OK
    WUX  Element Should Contain  css=#param_compte_logiciel  OK
    WUX  Element Should Contain  css=#api_reu_dispo  OK
    WUX  Element Should Contain  css=#connexion_logicielle  OK
    WUX  Element Should Contain  css=#actu  Test d'actualité pour l'interface reu

    #Vérification de la partie synchronisation
    &{test_radiation_reu} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=TEST420INTERFACEREUNOM
    ...  prenom=TEST420INTERFACEREUPRENOM
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE BASSE
    ${test_radiation_reu_id} =  Ajouter le mouvement d'inscription  ${test_radiation_reu}

    Synchroniser les bureaux
    Appliquer le traitement de fin d'année
    Désactiver le reu et mettre les fichiers par défaut

Vérification des réponse de l'API REST lorsque la synchronisation des listes électorales n'est pas faite
    Remettre les réponses par défaut
    Réinitialiser les bureaux de vote REU
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    ${json} =  Set Variable  { "module": "treat_all_notifications"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  500  Nombre de collectivités traitées : 1\rNombre de collectivités en erreurs : 1\r\rLIBREVILLE : Cette fonctionnalité n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.
    ${json} =  Set Variable  { "module": "synchro_last_notifications"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  500  Nombre de collectivités traitées : 1\rNombre de collectivités en erreurs : 1\r\rLIBREVILLE : Cette fonctionnalité n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.
    Désactiver le reu et mettre les fichiers par défaut

Vérification de la synchronisation des listes
    Remettre les réponses par défaut
    Réinitialiser les bureaux de vote REU
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Accéder au module REU

    Click Link  css=#module_reu-onglet-synchronisation_liste_electorale
    WUX  Element Should Be Visible  css=#nb_electeur_oel
    ${nb_electeur_oel}=  Get Text  css=#nb_electeur_oel
    WUX  Element Should Contain  css=#nb_electeur_oel  ${nb_electeur_oel}
    WUX  Element Should Contain  css=#nb_electeur_reu  0
    WUX  Click Element  name:traitement.reu_sync_l_e_init.form.valid
    Handle Alert
    WUX  Element Should Contain  css=#nb_electeur_oel  ${nb_electeur_oel}
    WUX  Element Should Contain  css=#nb_electeur_reu  55
    WUX  Click Link  ETAPE SUIVANTE
    WUX  Click ELement  name:traitement.reu_sync_l_e_attach.form.valid
    Handle Alert
    WUX  Click Link  ETAPE SUIVANTE

    WUX  Element Should Be Visible  css=#exact
    WUX  Click Element  css=#exact

    WUX  Page Should Contain  AZERTYTOU
    Click On Back Button

    WUX  Element Should Be Visible  css=#approx
    WUX  Click Element  css=#approx

    WUX  Page Should Contain  BARANDO
    WUX  Page Should Contain  00/00/1994
    Click On Back Button
    WUX  Element Should Be Visible  name:traitement.reu_sync_l_e_valid.form.valid
    WUX  Click Element  name:traitement.reu_sync_l_e_valid.form.valid
    Handle Alert
    WUX  Click Link  RAFRAÎCHIR

    Désactiver le reu et mettre les fichiers par défaut

Vérification de l'inscription et de la radiation d'un électeur à partir du REU
    [Documentation]  ...

    # Positionnement de l'état de l'environnement
    Réinitialiser les bureaux de vote REU
    Modifier l'identifiant externe de la radiation INSEE  201
    Remettre l'identifiant électeur de la radiation INSEE par défaut
    Remettre le fichier de notifications par défaut
    Remettre les réponses par défaut
    Run  sed -i 's/"numeroElecteur": [0-9]*/"numeroElecteur": 123456/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales

    # Fonctionnement de l'inscription avec recherche par INE + abandonner
    Faire une recherche d'électeur REU par ine  123456
    WUX  Page Should Contain  TEST420NOMNAISSANCE
    La page ne doit pas contenir d'erreur
    WUX  Select Radio Button  choix_ine  TEST420NOMNAISSANCE;03/07/1950;123456
    Click Element  name:inscription_search_form.action.valid
    Vérifier que les champs sont static
    &{mouvement_inscription_reu} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  libelle_voie=RUE BASSE
    Saisir le mouvement d'inscription  ${mouvement_inscription_reu}
    WUX  Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur
    ${id_inscription} =  Get Value  css=#id
    Portlet Action Should Be In Form  inscription  modifier
    Portlet Action Should Not Be In Form  inscription  supprimer
    Portlet Action Should Not Be In Form  inscription  valider
    Portlet Action Should Not Be In Form  inscription  reu-viser
    Portlet Action Should Be In Form  inscription  reu-abandonner
    Portlet Action Should Be In Form  inscription  reu-attendre
    Portlet Action Should Be In Form  inscription  reu-completer
    WUX  Form Value Should Be  css=#id_demande  114
    WUX  Element Should Contain  css=#nom  TEST420NOMNAISSANCE
    WUX  Element Should Contain  css=#prenom  TEST420PRENOM
    WUX  Form Value Should Be  css=#libelle_departement_naissance  ROUMANIE
    WUX  Form Value Should Be  css=#libelle_lieu_de_naissance  IPATELE
    # Abandon du mouvement d'inscription
    Depuis le contexte de l'inscription  ${id_inscription}
    Abandonner le mouvement d'inscription REU
    Depuis le listing des inscriptions
    Rechercher en recherche avancée simple  TEST420NOMNAISSANCE
    Page Should Contain  abandonne

    # Fonctionnement de l'inscription avec recherche par état civil + attendre + abandonner
    Modifier l'identifiant externe de l'inscription  115
    &{etat_civil} =  Create Dictionary
    ...  nom=TEST420NOMNAISSANCE
    ...  prenom=TEST420PRENOM
    ...  date_naissance=03/07/1950
    ...  sexe=M
    Faire une recherche d'électeur REU par état civil  ${etat_civil}
    WUX  Select Radio Button  choix_ine  TEST420NOMNAISSANCE;03/07/1950;123456
    Click Element  name:inscription_search_form.action.valid
    Vérifier que les champs sont static
    Saisir le mouvement d'inscription  ${mouvement_inscription_reu}
    WUX  Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur
    ${id_inscription} =  Get Value  css=#id
    Portlet Action Should Be In Form  inscription  modifier
    Portlet Action Should Not Be In Form  inscription  supprimer
    Portlet Action Should Not Be In Form  inscription  valider
    Portlet Action Should Not Be In Form  inscription  reu-viser
    Portlet Action Should Be In Form  inscription  reu-completer
    Portlet Action Should Be In Form  inscription  reu-abandonner
    Portlet Action Should Be In Form  inscription  reu-attendre
    WUX  Form Value Should Be  css=#id_demande  115
    WUX  Element Should Contain  css=#nom  TEST420NOMNAISSANCE
    WUX  Element Should Contain  css=#prenom  TEST420PRENOM
    WUX  Form Value Should Be  css=#libelle_departement_naissance  ROUMANIE
    WUX  Form Value Should Be  css=#libelle_lieu_de_naissance  IPATELE
    # Mise en attente du mouvement d'inscription
    Mettre en attente le mouvement d'inscription REU
    Portlet Action Should Be In Form  inscription  modifier
    Portlet Action Should Not Be In Form  inscription  supprimer
    Portlet Action Should Not Be In Form  inscription  valider
    Portlet Action Should Be In Form  inscription  reu-abandonner
    Portlet Action Should Be In Form  inscription  reu-completer
    Portlet Action Should Not Be In Form  inscription  reu-attendre
    Portlet Action Should Not Be In Form  inscription  reu-viser
    # Abandon du mouvement d'inscription
    Depuis le contexte de l'inscription  ${id_inscription}
    Abandonner le mouvement d'inscription REU
    Portlet Action Should Not Be In Form  inscription  modifier
    Portlet Action Should Not Be In Form  inscription  supprimer
    Portlet Action Should Not Be In Form  inscription  valider
    Portlet Action Should Not Be In Form  inscription  reu-abandonner
    Portlet Action Should Not Be In Form  inscription  reu-completer
    Portlet Action Should Not Be In Form  inscription  reu-attendre
    Portlet Action Should Not Be In Form  inscription  reu-viser

    # Fonctionnement de l'inscription avec saisie manuelle + compléter + viser
    Modifier l'identifiant externe de l'inscription  116
    &{mouvement_inscription_reu_saisie_libre} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  libelle_voie=RUE BASSE
    ...  naissance_type_saisie=Né à l'étranger
    ...  pays_de_naissance=99114 - ROUMANIE
    ...  libelle_lieu_de_naissance=IPATELE
    Faire une recherche d'électeur REU par état civil  ${etat_civil}
    WUX  Select Radio Button  choix_ine  TEST420NOMNAISSANCE;03/07/1950;-1
    Click Element  name:inscription_search_form.action.valid
    Vérifier que les champs ne sont pas static
    Vérifier que les champs sont correct pour l'état civil  ${etat_civil}
    Saisir le mouvement d'inscription  ${mouvement_inscription_reu_saisie_libre}
    WUX  Form Value Should Be  css=#id_demande  116
    Compléter le mouvement d'inscription REU  ${DATE_FORMAT_DD/MM/YYYY}
    &{visa} =  Create Dictionary
    ...  select=accepté
    ...  date=${DATE_FORMAT_DD/MM/YYYY}
    Viser le mouvement d'inscription REU  ${visa}
    ${id_inscription} =  Get Value  css=#id

    # On vérifie que l'inscription apparaît bien dans le widget Tâches en cours du tableau de bord
    Go To Dashboard
    # WUX  Page Should Contain Link  1

    # On fait en sorte que l'inscription précédente soit traitée pour avoir un électeur maîtrisé à radier
    Synchroniser les notifications
    Traiter la notification  116
    WUX  Valid Message Should Contain  Traitement de la notification d'inscription terminé.

    Depuis le contexte de l'inscription  ${id_inscription}
    Click Link  css=#id_electeur_link
    La page ne doit pas contenir d'erreur

    # Fonctionnement de la radiation + abandonner
    Modifier l'identifiant externe de l'inscription  117
    &{mouvement_radiation_reu} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=DECES
    ...  electeur_nom=${etat_civil.nom}
    ${mouvement_radiation_reu_id} =  Ajouter le mouvement de radiation  ${mouvement_radiation_reu}
    La page ne doit pas contenir d'erreur
    WUX  Form Value Should Be  css=#id_demande  117
    Vérifier que les champs sont correct pour l'état civil  ${etat_civil}
    Abandonner le mouvement de radiation REU

    # Fonctionnement de la radiation + viser
    Modifier l'identifiant externe de l'inscription  118
    &{mouvement_radiation_reu} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=DECES
    ...  electeur_nom=${etat_civil.nom}
    ${mouvement_radiation_reu_id} =  Ajouter le mouvement de radiation  ${mouvement_radiation_reu}
    La page ne doit pas contenir d'erreur
    WUX  Form Value Should Be  css=#id_demande  118
    Vérifier que les champs sont correct pour l'état civil  ${etat_civil}
    Viser le mouvement de radiation REU  ${visa}

    # On vérifie que la radiation apparaît bien dans le widget Tâches en cours du tableau de bord
    Go To Dashboard
    # WUX  Page Should Contain Link  1

    # On fait en sorte que la radiation précédente soit traitée pour avoir un état identique à avant les tests (électeur non présent)
    Traiter la notification  118
    WUX  Valid Message Should Contain  Traitement de la notification de radiation terminé.

    # Positionnement de l'état de l'environnement
    Désactiver le reu et mettre les fichiers par défaut
    Remettre les réponses par défaut

Synchronisation referentiel
    Réinitialiser les bureaux de vote REU
    Modifier l'identifiant externe de la radiation INSEE  201
    Remettre l'identifiant électeur de la radiation INSEE par défaut
    Remettre le fichier de notifications par défaut
    Remettre les réponses par défaut
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence
    Désactiver le reu et mettre les fichiers par défaut

Traitement de notification insee
    Modifier l'identifiant externe de la radiation INSEE  201
    Remettre l'identifiant électeur de la radiation INSEE par défaut
    Remettre le fichier de notifications par défaut
    Remettre les réponses par défaut
    Run  sed -i 's/"etatCivil"/"etatCivilUgle"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_inscription.txt
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales

    Préparer les notifications pour le test des notifications INSEE
    Synchroniser les notifications

    &{motifvalue1} =  Create Dictionary
    ...  datedeb=${DATE_FORMAT_DD/MM/YYYY}
    ...  datefin=25/02/2020
    ...  categorie=Inscription
    ...  code=VOL1
    ...  libelle=Inscription volontaire
    ...  effet=Immédiat

    &{motifvalue2} =  Create Dictionary
    ...  datedeb=${DATE_FORMAT_DD/MM/YYYY}
    ...  datefin=25/02/2020
    ...  categorie=Radiation
    ...  code=DEC
    ...  libelle=DEC
    ...  effet=Immédiat

    Ajouter un motif dans les paramètres de mouvement  ${motifvalue1}
    Ajouter un motif dans les paramètres de mouvement  ${motifvalue2}

    Traiter la notification  200
    WUX  Valid Message Should Contain  Traitement de la notification d'inscription terminé.
    Run  sed -i 's/"etatCivilUgle"/"etatCivil"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_inscription.txt
    Valider le mouvement d'inscription INSEE  TEST420NOMNAISSANCEINSEE
    WUX  Valid Message Should Contain  Le mouvement a été correctement validé.


    Traiter la notification  201
    WUX  Valid Message Should Contain  Traitement de la notification de radiation terminé.
    Valider le mouvement de radiation INSEE  TEST420NOMNAISSANCEINSEE
    WUX  Valid Message Should Contain  Le mouvement a été correctement validé.

    Remettre le fichier de notifications par défaut
    Run  sed -i 's/"etatCivil"/"etatCivilUgle"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_inscription.txt
    Désactiver le reu et mettre les fichiers par défaut


Vérification du bon fonctionnement de l'api REST
    Préparer les notifications pour le test API
    Désactiver le reu et mettre les fichiers par défaut
    Réinitialiser les bureaux de vote REU
    #Seul la requête POST est disponible
    ${json} =  Set Variable  { "module": ""}
    Vérifier le code retour du web service et vérifier que son message est  Get  maintenance  ${json}  400  La méthode GET n'est pas disponible sur cette ressource.
    Vérifier le code retour du web service et vérifier que son message est  Put  maintenance  ${json}  400  La méthode PUT n'est pas disponible sur cette ressource.
    Vérifier le code retour du web service et vérifier que son message est  Delete  maintenance  ${json}  400  La méthode DELETE n'est pas disponible sur cette ressource.

    #Le fichier de configuration n'est pas présent
    ${json} =  Set Variable  { "module": "synchro_referentiels_lieu_naissance"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Le module REU n'est pas activé.
    ${json} =  Set Variable  { "module": "treat_all_notifications"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Le module REU n'est pas activé.
    ${json} =  Set Variable  { "module": "synchro_last_notifications"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Le module REU n'est pas activé.
    ${json} =  Set Variable  { "module": "synchro_referentiels_misc"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Le module REU n'est pas activé.
    ${json} =  Set Variable  { "module": "synchro_bureaux"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Le module REU n'est pas activé.

    #Vérification de la synchronisation des referentiels et des bureaux
    Remettre les réponses par défaut
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Non
    &{motifvalue1} =  Create Dictionary
    ...  datedeb=${DATE_FORMAT_DD/MM/YYYY}
    ...  datefin=25/02/2020
    ...  categorie=Inscription
    ...  code=VOL1
    ...  libelle=Inscription volontaire
    ...  effet=Immédiat

    &{motifvalue2} =  Create Dictionary
    ...  datedeb=${DATE_FORMAT_DD/MM/YYYY}
    ...  datefin=25/02/2020
    ...  categorie=Radiation
    ...  code=DEC
    ...  libelle=DEC
    ...  effet=Immédiat

    Ajouter un motif dans les paramètres de mouvement  ${motifvalue1}
    Ajouter un motif dans les paramètres de mouvement  ${motifvalue2}

    Préparer l'erreur des lieux de naissance REU
    ${json} =  Set Variable  { "module": "synchro_referentiels_lieu_naissance"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  500  Impossible de récupérer les lieux de naissance.
    #
    Réinitialiser les lieux de naissance REU
    ${json} =  Set Variable  { "module": "synchro_referentiels_lieu_naissance"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Synchronisation des lieux de naissance effectuée.
    #Changement du fichier text ressource_test_bureaux_de_vote.txt
    Retirer un bureau de vote REU
    ${json} =  Set Variable  { "module": "synchro_bureaux"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  500  Nombre de collectivités traitées : 1\rNombre de collectivités en erreurs : 1\r\rLIBREVILLE : Les bureaux de vote et/ou leurs codes ne sont pas identiques entre openElec et le REU.\rLe nombre de bureaux de vote est différent entre openElec (3) et le REU (2).\r
    #
    Réinitialiser les bureaux de vote REU
    ${json} =  Set Variable  { "module": "synchro_bureaux"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Nombre de collectivités traitées : 1\rNombre de collectivités en erreurs : 0\r\rLIBREVILLE : Les bureaux de vote et leurs codes sont identiques entre openElec et le REU.\r
    ${json} =  Set Variable  { "module": "synchro_referentiels_misc"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Synchronisation des référentiels effectuée.

    # #Synchro liste electoral faite
    Synchroniser les listes électorales

    ${json} =  Set Variable  { "module": "synchro_last_notifications"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Nombre de collectivités traitées : 1\rNombre de collectivités en erreurs : 0\r\rLIBREVILLE : 2 (2) notification(s) importées.

    ${json} =  Set Variable  { "module": "treat_all_notifications"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Nombre de collectivités traitées : 1\rNombre de collectivités en erreurs : 0\r\rLIBREVILLE : => notification n°9<br />Traitement de la notification d'inscription terminé.<br />=> notification n°10<br />Le champ <span class="bold">nom</span> est obligatoire<br />Le champ <span class="bold">Prénom</span> est obligatoire<br />Le champ <span class="bold">sexe</span> est obligatoire<br />Le champ <span class="bold">date de naissance</span> est obligatoire<br />Le champ <span class="bold">code de nationalité</span> est obligatoire<br /><br/>SAISIE NON ENREGISTRÉE<br/><br />Erreur lors de la création du mouvement de radiation.<br />Le mouvement n'a pas pu être ajouté.<br />Erreur lors de l'application du traitement. Contactez votre administrateur.<br />1 notification(s) traitée(s) sur un total de 2.
    Valider le mouvement d'inscription INSEE  TEST420NOMNAISSANCEINSEE
    WUX  Valid Message Should Contain  Le mouvement a été correctement validé.
    ${number} =  Get Value  css=#id
    ${id_mouvement_previous} =  Convert To Integer  ${number}
    ${id_mouvement} =  Evaluate  ${id_mouvement_previous} + 1
    ${id_electeur} =  WUX  Get Value  css=#electeur_id
    Modifier l'identifiant externe de la radiation INSEE  220
    Modifier l'identifiant électeur de la radiation INSEE  159753
    ${json} =  Set Variable  { "module": "treat_all_notifications"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Nombre de collectivités traitées : 1\rNombre de collectivités en erreurs : 0\r\rLIBREVILLE : => notification n°10<br />Traitement de la notification de radiation terminé.<br />1 notification(s) traitée(s) sur un total de 1.
    Valider le mouvement de radiation INSEE  vise_insee
    Modifier l'identifiant externe de la radiation INSEE  201
    Remettre l'identifiant électeur de la radiation INSEE par défaut

    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut

Vérification de la modification d'un électeur et du traitement par lot
    [Documentation]  Test la modification d'un électeur et l'affichage de
    ...  des doublons lors de l'inscription

    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Modifier l'identifiant externe de l'inscription  230
    #Changement de l'ine pour l'inscription
    Run  sed -i 's/"numeroElecteur": "[0-9]*"/"numeroElecteur": "794613"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Faire une recherche d'électeur REU par ine  794613
    &{etat_civil_modif} =  Create Dictionary
    ...  nom=TEST420NOMNAISSANCE
    ...  prenom=TEST420PRENOM
    ...  date_naissance=03/10/1950
    ...  sexe=M
    WUX  Page Should Contain  TEST420NOMNAISSANCE
    La page ne doit pas contenir d'erreur
    WUX  Select Radio Button  choix_ine  TEST420NOMNAISSANCE;03/07/1950;123456
    Click Element  name:inscription_search_form.action.valid
    # Vérifie l'affichage des informations dans le résultats de la recherche de doublon
    WUX  Page Should Contain  Résultats de la recherche de doublon
    # Accède au formulaire d'inscription pour tester les champs
    Click Element  name:inscription_doublon_form.action.valid
    Vérifier que les champs sont static
    &{mouvement_inscription_reu_modif} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  libelle_voie=RUE BASSE
    &{visa_modif} =  Create Dictionary
    ...  select=accepté
    ...  date=${DATE_FORMAT_DD/MM/YYYY}
    Saisir le mouvement d'inscription  ${mouvement_inscription_reu_modif}
    ${id_inscription_modif} =  Get Value  css=#id
    Préparer les notifications pour le test du modificatif
    Accéder au module REU
    Synchroniser les notifications
    Traiter la notification  230

    # Test du traitement par lot
    Go To Dashboard
    Element Should Not Contain  css=#task-mes_inscriptions_acceptees_insee-nb  0
    # Vérification de la présence de l'action de traitement sur le listing
    # des inscriptions
    Depuis le listing des inscriptions
    WUX  Page Should Contain Element  css=a#action-tab-inscription-corner-traitements_par_lot
    # Accès au formulaire des traitements par lot
    Click Element  css=a#action-tab-inscription-corner-traitements_par_lot
    WUX  Page Should Contain Element  css=select#lot_a_traiter
    La page ne doit pas contenir d'erreur
    # Sélection d'un lot sans mouvement à traiter
    Select From List By Label  lot_a_traiter  Modifications d'état civil
    WUX  Element Should Contain  css=.message.ui-state-warning  Aucun mouvement à traiter pour ce lot.
    # Traitement du lot
    Click On Submit Button
    WUX  Element Should Contain  css=.message.ui-state-valid  Aucun mouvement à traiter.
    # Sélection d'un lot avec un mouvement à traiter
    Select From List By Label  lot_a_traiter  Inscriptions acceptées par l'insee
    WUX  Element Should Contain  css=.message.ui-state-warning  mouvement seront traités pour ce lot.
    # Traitement du lot
    Click On Submit Button
    WUX  Element Should Contain  css=.message.ui-state-valid  mouvement(s) traité(s) sur un total de

    # Création d'un mouvement de modification
    Depuis le contexte de l'inscription  ${id_inscription_modif}
    Click Element  css=#id_electeur_link
    WUX  Click Element  css=#action-form-electeur-modifier-electeur
    WUX  Select From List By Label  css=#types  MODIFICATION ETAT CIVIL
    Input Text  css=#nom_usage  NOMUSAGE1
    Click On Submit Button
    WUX  Valid Message Should Be  Vos modifications ont bien été enregistrées.
    WUX  Page Should Contain  NOMUSAGE1
    La page ne doit pas contenir d'erreur
    Portlet Action Should Be In Form  modification  modifier
    Portlet Action Should Not Be In Form  modification  supprimer
    Portlet Action Should Be In Form  modification  valider
    Portlet Action Should Be In Form  modification  abandonner
    # Abandon du mouvement de modification
    Click On Form Portlet Action  modification  abandonner  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur
    Portlet Action Should Not Be In Form  modification  modifier
    Portlet Action Should Not Be In Form  modification  supprimer
    Portlet Action Should Not Be In Form  modification  valider
    Portlet Action Should Not Be In Form  modification  abandonner
    # Création d'un mouvement de modification
    Depuis le contexte de l'inscription  ${id_inscription_modif}
    Click Element  css=#id_electeur_link
    WUX  Click Element  css=#action-form-electeur-modifier-electeur
    WUX  Select From List By Label  css=#types  MODIFICATION ETAT CIVIL
    Input Text  css=#nom_usage  NOMUSAGE2
    Click On Submit Button
    WUX  Page Should Contain  NOMUSAGE2
    # Validation du mouvement de modification
    Click On Form Portlet Action  modification  valider  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Valid Message Should Contain  Le mouvement a été correctement validé.
    WUX  Valid Message Should Contain  > La modification a été appliquée correctement sur l'électeur.
    WUX  Valid Message Should Contain  > Le statut du mouvement a été correctement mis à jour.
    WUX  Valid Message Should Contain  > Les informations de l'électeur ont correctement transmises au REU.
    La page ne doit pas contenir d'erreur
    Portlet Action Should Not Be In Form  modification  modifier
    Portlet Action Should Not Be In Form  modification  supprimer
    Portlet Action Should Not Be In Form  modification  valider
    Portlet Action Should Not Be In Form  modification  abandonner
    #
    #Changement de l'ine pour l'inscription
    Run  sed -i 's/"numeroElecteur": "794613*"/"numeroElecteur": "123456"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut
    Remettre les réponses par défaut

Vérification du traitement des électeurs sans bureau de vote
    [Documentation]  Permet de vérifier le traitement des electeurs sans bureau de vote lorsque la ressource de test retourne une réponse positive.

    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales

    #Ajout des deux électeurs supposés sans bureau par notifications
    Préparer les notifications pour le test sans bureau
    Synchroniser les notifications
    Traiter la notification  240
    Valider le mouvement d'inscription INSEE  TEST420NOMNAISSANCEINSEE
    Traiter la notification  250
    Valider le mouvement d'inscription INSEE  TEST420NOMNAISSANCEINSEE
    # Accès à l'onglet de transmission par lots
    Accéder au module REU
    Click Element  css=#module_reu-onglet-transmission_par_lots

    # On vérifie que l'électeur est présent
    WUX  Page Should Contain  2

    # On clique sur le bouton permettant de confirmer le traitement et on vérifie le message
    WUX  Click Element  name:traitement.reu_sync_l_e_reu_sans_bureau.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    WUX  Valid Message Should Contain  2 modification(s) validée(s) sur un total de 2.

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut

Traitement des notifications MODEC
    [Documentation]  Permet de vérifier le traitement de la notification de type MODE lorsque la ressource de test retourne une réponse positive.

    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales

    #On créer une nouvelle inscription par notification insee
    Préparer les notifications d'inscription pour le test des notifications MODEC
    Synchroniser les notifications
    Traiter la notification  260
    Valider le mouvement d'inscription INSEE  TEST420NOMNAISSANCEINSEE

    #On récupère la notification modec puis on la traite
    Préparer les notifications pour le test des notifications MODEC
    Synchroniser les notifications
    Traiter la notification  260

    #On vérifie que le mouvement de modification est correctement ajouté et on le valide
    Depuis la page d'accueil  admin  admin
    ${nb-mes_modifications_d_etat_civil} =  Get Text  css=#task-mes_modifications_d_etat_civil-nb
    Should Be Equal As Strings  ${nb-mes_modifications_d_etat_civil}  1
    Click Element  css=#task-mes_modifications_d_etat_civil-nb
    Click On Link  TEST420NOMNAISSANCE
    WUX  Form Value Should Be  css=#types  MODEC
    Click On Form Portlet Action  modification  valider  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Valid Message Should Contain  Le mouvement a été correctement validé.
    WUX  Valid Message Should Contain  > Le statut du mouvement a été correctement mis à jour.

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut

Traitement de l'état civil des électeurs
    [Documentation]  Permet de vérifier que le traitement de l'état civil des électeurs s'effectue correctement lorsque la ressource de test envoie des réponses positives.

    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence

    #On vérifie la valeur de l'électeur à traiter
    Depuis la fiche de l'électeur  100068
    WUX  Page Should Not Contain  AZERTYTOU

    #On accède à l'onglet des synchronisation référentiel
    Accéder au module REU
    Click Element  css=#module_reu-onglet-synchronisation_referentiel

    #On traite les états civil et on vérifie que tous les électeurs sont traités
    WUX  Click Element  name:traitement.reu_sync_l_e_reu_etat_civil.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail

    #On vérifie un des électeurs traités
    Depuis la fiche de l'électeur  100068
    WUX  Page Should Contain  AZERTYTOU

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut

Traitement carte retour avec ine
    [Documentation]  Permet de vérifier que le traitement de la carte retour se fait bien avec l'ine
    ...  de l'électeur et non sur l'id.
    ...  Vérifie également que le traitement d'épuration fonctionne.
    ...  3 cas d'épuration des cartes testés :
    ...    1) Saisie d'une date de début et de fin : suppression des cartes en retour
    ...       ayant une date de saisie supérieure ou égale à la date de début et
    ...       inférieure ou égale à la date de fin.
    ...    2) Saisie d'une date de fin uniquement : suppression des cartes en retour
    ...       ayant une date de saisie inférieure ou égale à la date de fin ou n'ayant pas de
    ...       date de saisie
    ...    3) Saisie d'une date de début uniquement : suppression des cartes en retour
    ...       ayant une date de saisie supérieure ou égale à la date de début

    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence

    # On accède à l'onglet carte en retour et on saisie l'ine de l'électeur
    # ajouté lors de la synchronisation des listes électorales
    # Ainsi que la date du jour
    Go To Submenu In Menu  traitement  carteretour
    Input Text  css=#id_elec  51
    Input Datepicker  date_saisie_carte_retour  ${DATE_FORMAT_DD/MM/YYYY}
    Click Element  name:traitement.carteretour.form.valid
    WUX  Valid Message Should Contain  AZERTYTOUT FRANCOIS - né(e) le 31/03/1962 de la liste LP a maintenant une carte en retour. La date de saisie de la carte en retour a été enregistrée.
    WUX  Page Should Contain  Le traitement est terminé.

    # Vérification de l'affichage de la carte en retour en consultation de l'électeur
    Depuis la page d'accueil  admin  admin
    Depuis la fiche de l'électeur  100068
    WUX  Element Should Contain  css=.carteretour-32  Une carte en retour est enregistrée pour cet électeur.
    WUX  Element Should Contain  css=.date_saisie_carte_retour  date de saisie : ${DATE_FORMAT_DD/MM/YYYY}

    # Épuration de la carte en retour de l'électeur
    Épurer les cartes en retour

    # On accède à l'onglet carte en retour et on saisie l'ine des électeurs
    # ajouté avec les dates de saisie suivante :
    # 2 -> 02-09-2022
    # 7 -> 07-09-2022
    # 8 -> 08-09-2022
    # 9 -> pas de date
    Go To Submenu In Menu  traitement  carteretour
    Input Text  css=#id_elec  9
    Click Element  name:traitement.carteretour.form.valid
    WUX  Valid Message Should Contain  a maintenant une carte en retour.
    ${INE} =  Create List  2  7  8
    :FOR  ${id}  IN  @{INE}
    \    Input Text  css=#id_elec  ${id}
    \    Input Datepicker  date_saisie_carte_retour  0${id}/09/2022
    \    Click Element  name:traitement.carteretour.form.valid
    \    WUX  Valid Message Should Contain  a maintenant une carte en retour.

    # Épuration des cartes
    # Cas 1 : Date début : 29-08-2022, Date fin : 07-09-2022 -> 1 carte épurée
    Épurer les cartes en retour  29/08/2022  06/09/2022
    WUX  Valid Message Should Contain  1 carte(s) en retour supprimée(s)

    # Cas 2 : Date début : - , Date fin : 08-09-2022 -> 2 carte épurée
    Épurer les cartes en retour  ${EMPTY}  07/09/2022
    WUX  Valid Message Should Contain  2 carte(s) en retour supprimée(s)

    # Cas 3 : Date début : 29-08-2022 , Date fin : - -> 1 carte épurée
    Épurer les cartes en retour  29/08/2022
    WUX  Valid Message Should Contain  1 carte(s) en retour supprimée(s)

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut

Traitement de la refonte à partir du REU
    [Documentation]  Vérifie que le traitement de la refonte à partir du REU retourne les bons messages

    # On se connecte au REU et on initialise un statut valide
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales

    # On accède au traitement de la refonte et on lance le traitement
    Go To Submenu In Menu  traitement  refonte
    Click Element  name:traitement.refonte.form.valid
    Handle Alert

    # On vérifie que les messages sont tous positifs
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    WUX  Valid Message Should Contain  Tous les bureaux de vote ont été traités correctement dans le REU.
    WUX  Valid Message Should Contain  Tous les électeurs ont été resynchronisés.

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut

Synchronisation des bureaux de vote sans canton et sans circonscription
    [Documentation]  Vérifie que la synchronisation des bureaux sans canton et sans circonscription

    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    # On ne fait pas la synchro des bureaux
    Initialiser un statut valide  Non

    # On met en place les bureaux de vote sans canton et circon pour la synchronisation
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_bureaux_de_vote_sans_canton_circon.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_bureaux_de_vote_sans_canton_circon.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_bureaux_de_vote.txt

    #On accède à l'onglet des synchronisation référentiel
    Accéder au module REU
    WUX  Click Element  css=#module_reu-onglet-synchronisation_referentiel
    #Synchronisation des bureaux de vote
    WUX  Click Element  name:traitement.reu_sync_bureau.form.valid
    Handle Alert

    # Vérification du message de validation
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    WUX  Valid Message Should Contain  Les bureaux de vote et leurs codes sont identiques entre openElec et le REU.
    WUX  Valid Message Should Contain  Toutes les informations de cantons et de circonscriptions ne sont pas renseignées dans le REU.

    # Vérification du message dans le statut
    Click Element  css=#module_reu-onglet-main
    WUX  Element Should Contain  css=#bureaux-status  ERROR
    WUX  Page Should Contain  Toutes les informations de cantons et de circonscriptions ne sont pas renseignées dans le REU.

    # On remet le fichier contenant les bureaux de vote avec canton et circon
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_bureaux_de_vote.txt  ${PATH_REU_RESOURCE_TEST_WF}

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut

Synchronisation des scrutins
    [Documentation]  Vérifie que la synchronisation des scrutins fonctionne correctement

     # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence

    # On accède au module élection et on synchronise les scrutins
    Go To Submenu In Menu  traitement  module-election
    WUX  Click Element  css=#action-tab-reu_scrutin-corner-sync_scrutin
    WUX  Click On Submit Button
    La page ne doit pas contenir d'erreur

    # On vérifie que le scrutin 183 contient l'action COMJ-20 + emarge et pas J-5
    Click Link  Test COMJ-20 disponible AZE
    Portlet Action Should Be In Form  reu_scrutin  demander_j20_livrable
    Portlet Action Should Be In Form  reu_scrutin  demander_emarge_livrable
    Portlet Action Should Not Be In Form  reu_scrutin  demander_j5_livrable
    Page Should Contain  La période est propice (J-23 > J-7) pour demander l'arrêt des listes.
    Page Should Contain  La période n'est pas propice (J-6 > J-1) à la demande du tableau des mouvements j-5.
    Click On Back Button
    # On vérifie que le scrutin 184 contient l'action COMJ-5 + emarge et pas J-20
    Click Link  Test J-5 disponible
    Portlet Action Should Not Be In Form  reu_scrutin  demander_j20_livrable
    Portlet Action Should Be In Form  reu_scrutin  demander_emarge_livrable
    Portlet Action Should Be In Form  reu_scrutin  demander_j5_livrable
    Page Should Contain  La période n'est pas propice (J-23 > J-7) à la demande d'arrêt des listes.
    Page Should Contain  La période est propice (J-6 > J-1) pour demander le tableau des mouvements j-5.
    Click On Back Button
    # On vérifie que le scrutin 185 contient l'action emarge et pas J-20 et J-5
    Click Link  Test aucune action dispo
    Portlet Action Should Not Be In Form  reu_scrutin  demander_j20_livrable
    Portlet Action Should Be In Form  reu_scrutin  demander_emarge_livrable
    Portlet Action Should Not Be In Form  reu_scrutin  demander_j5_livrable
    Page Should Contain  La période n'est pas propice (J-23 > J-7) à la demande d'arrêt des listes.
    Page Should Contain  La période n'est pas propice (J-6 > J-1) à la demande du tableau des mouvements j-5.
    Click On Back Button

Traitement d'une demande de livrable 'arrêt des listes'
    # Envoie d'une demande de livrable J-20 au REU
    Click Link  Test COMJ-20 disponible AZE
    Click On Form Portlet Action  reu_scrutin  demander_j20_livrable  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    # Vérification des messages de l'interface
    WUX  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    WUX  Valid Message Should Contain  La demande de livrable COMJ-20 a été transmise au REU.
    WUX  Page Should Contain  L'arrêt des listes a été demandé (290)
    WUX  Page Should Contain  En attente du retour de l'INSEE.

    # Synchronisation des notifications pour récupérer la notification livrable
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_livrable_j20.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_j20.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  290
    WUX  Valid Message Should Be  Traitement de la notification de livrable terminé.

    # On vérifie que l'interface scrutin sur un J-20 a été mis à jour
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test COMJ-20 disponible AZE
    Page Should Contain  L'arrêt des listes a été réalisé le
    Page Should Contain  Fichiers produits : Télécharger

    # Vérification de la génération de la liste électorale pour une J-20
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test COMJ-20 disponible AZE
    WUX  Click Element  css=#edition-pdf-listeelectorale-generer
    Click Element  css=#edition-pdf-listeelectorale
    ${contenu_pdf} =  Create List  ADAM
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

Traitement d'une demande de livrable j-5
    # Envoie d'une demande de livrable J-5 au REU
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test J-5 disponible
    Click On Form Portlet Action  reu_scrutin  demander_j5_livrable  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    # Vérification des messages de l'interface
    WUX  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    WUX  Valid Message Should Contain  La demande de livrable MOUVJ-5 a été transmise au REU.
    WUX  Page Should Contain  Le tableau des mouvements j-5 a été demandé (291) le
    WUX  Page Should Contain  En attente du retour de l'INSEE.

    # Synchronisation des notifications pour récupérer la notification livrable
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_livrable_j5.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_j5.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  291
    WUX  Valid Message Should Be  Traitement de la notification de livrable terminé.

    # On vérifie que l'interface scrutin sur un J-5 a été mis à jour
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test J-5 disponible
    Page Should Contain  Le tableau des mouvements j-5 du
    Page Should Contain  est disponible ici : Télécharger

Traitement d'une demande de livrable 'liste d'émargement'
    # Envoie d'une demande de livrable emarge au REU
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test aucune action dispo
    Click On Form Portlet Action  reu_scrutin  demander_emarge_livrable  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    # Vérification des messages de l'interface
    WUX  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    WUX  Valid Message Should Contain  La demande de livrable EMARGE a été transmise au REU.
    WUX  Page Should Contain  Le livrable liste d'émargement a été demandé (292) le
    WUX  Page Should Contain  En attente du retour de l'INSEE.

    # Synchronisation des notifications pour récupérer la notification livrable
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_livrable_emarge.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_emarge.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  292
    Sleep  10
    WUX  Valid Message Should Be  Traitement de la notification de livrable terminé.

    # Edition de liste émargement par bureau
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test aucune action dispo
    WUX  Page Should Contain  Le livrable liste d'émargement a été produit le
    WUX  Page Should Contain   Liste d'émargement par bureau
    #
    WUX  Click Element  name:liste_emargement_par_bureau.submit
    ${contenu_souhaite} =  Create List  LISTE D'ÉMARGEMENT  ABEUDJE - ALEXANDRE EHYGSENE
    ${contenu_non_souhaite} =  Create List  PLOP  PLOP
    Vérifier Que Le PDF Contient Des Strings Mais Pas D'Autres  ${OM_PDF_TITLE}  ${contenu_souhaite}  ${contenu_non_souhaite}
    #


Traitement de notification demande de livrable ne provenant pas d'openElec
    [Documentation]  Vérifie si le bon déroulement du traitement de demande de livrable qui ne provient pas d'openElec

    # Synchronisation des notifications pour récupérer la notification livrable
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_livrable_out_oel.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_out_oel.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_demande_livrable.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  293
    # Il existe déjà un livrable plus récent
    WUX  Valid Message Should Contain  La demande n'a pas été faite sur openElec.
    WUX  Valid Message Should Contain  Un livrable plus récent existe dans le scrutin.
    WUX  Valid Message Should Contain  Traitement de la notification de livrable abandonné.

    # On fait une nouvelle notification avec une date plus récente
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_livrable_out_oel.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Run  sed -i 's/"id": 33/"id": 34/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_out_oel.txt
    Run  sed -i 's/"idDemande": "293"/"idDemande": "294"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_out_oel.txt
    Run  sed -i '4 c \"dateCreation": "01/05/2019T09:11:47",' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_out_oel.txt
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_out_oel.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_demande_livrable.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Run  sed -i 's/"idScrutin": 185/"idScrutin": 186/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_demande_livrable.txt
    Run  sed -i 's/"idLivrable": 355/"idLivrable": 345/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_demande_livrable.txt
    Run  sed -i 's/"code": "EMARGE"/"code": "COMJ-20"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_demande_livrable.txt
    Run  sed -i 's/"idDemandeLivrable": 293/"idDemandeLivrable": 294/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_demande_livrable.txt
    Synchroniser les notifications

    # On traite la notification
    Traiter la notification  294
    # On vérifie le message
    WUX  Valid Message Should Contain  La demande n'a pas été faite sur openElec.
    WUX  Valid Message Should Contain  Traitement de la notification de livrable terminé.
    # On vérifie que l'arrêt des listes a été faite et que l'action n'apparaît plus
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test COMJ-20 disponible demande out oel
    WUX  Page Should Contain  L'arrêt des listes a été réalisé le
    WUX  Page Should Contain  Fichiers produits : Télécharger
    WUX  Portlet Action Should Not Be In Form  reu_scrutin  demander_j20_livrable

Traitement d'une demande de livrable 'arrêt des listes sans scrutin'

    Depuis la page d'accueil  admin  admin
    # Acces au menu arret des listes
    Go To Submenu In Menu  traitement  module-arret_liste

    # Effectue la demande d'arret des listes sans scrutin
    Click On Add Button
    Click On Submit Button

    # Verification de la mise à jour de l'arrêt des listes
    Element Should Contain  css=#livrable_demande_id  3112
    Element Should Contain  css=#livrable_demande_date  ${DATE_FORMAT_DD/MM/YYYY}
    Element Should Contain  css=#livrable_date  ${EMPTY}
    ${arret_id} =  Get Value  css=input#arrets_liste

    # Traitement de la demande de livrable
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_livrable_com3112.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_livrable_com3112.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Synchroniser les notifications
    Traiter la notification  3112
    WUX  Valid Message Should Be  Traitement de la notification de livrable terminé.

    # Verification de la mise à jour de l'arrêt des listes
    Go To Submenu In Menu  traitement  module-arret_liste
    Click Link  ${arret_id}
    Wait Until Element Contains  css=#livrable_demande_id  3112
    Element Should Contain  css=#livrable_demande_date  ${DATE_FORMAT_DD/MM/YYYY}
    Element Should Contain  css=#livrable_date  30/04/2019

    # Test des éditions
    Page Should Contain  L'arrêt des listes a été réalisé le 2019-04-30.
    WUX  Click Element  css=#edition-pdf-listeelectorale-generer
    Go To Submenu In Menu  traitement  module-arret_liste
    Click Link  ${arret_id}
    Click Element  css=#edition-pdf-listeelectorale
    ${contenu_pdf} =  Create List  ADAM
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut


Vérification des mentions
    # Vérification des mentions sur la liste d'émargement
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test aucune action dispo
    Click Element  name:liste_emargement_par_bureau.submit
    ${contenu} =  Create List  *** ne vote pas dans la commune ***  *** ne vote pas au premier tour ***
    WUX  Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu}

    # TODO : gérer un jeu de données liste d'émargement avec procuration REU
    # ${date_validite} =  Get Text  css=#date_tour1
    # &{args_procuration} =  Create Dictionary
    # ...  nom_mandant=LOUIS
    # ...  nom_mandataire=BELLOZ
    # ...  debut_validite=${date_validite}
    # ...  fin_validite=${date_validite}
    # ...  date_accord=15/08/2013
    # Ajouter une procuration  ${args_procuration}
    # #Vérification des mentions procuration sur la liste d'émargement
    # Go To Submenu In Menu  traitement  module-election
    # Click Link  Test aucune action dispo
    # Select from List By Label  css=#liste_emargement_par_bureau #bureau  3 SALLE DES FETES
    # Click Element  name:liste_emargement_par_bureau.submit
    # ${contenu} =  Create List  - Mandant CAMILLE LOUIS (3)  - Mandataire BELLOZ PHILIPPE (3)
    # WUX  Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu}


Vérification d'une radiation d'électeur LC2
    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence
    #

    # On change la liste
    Click Link  ../app/changeliste.php
    Select From List By Label  name:liste  LCE - LISTE COMPLÉMENTAIRE EUROPÉENNE
    WUX  Click Button  name=changeliste.action.valid

    # Initialisation de l'identifiant externe / id de la demande (pour notification)
    ${idDemande} =  Set Variable  700
    ${idElecteur} =  Set Variable  40030
    ${liste} =  Set Variable  LC2
    ${libelle_liste} =  Set Variable  LC2 - LISTES COMPLÉMENTAIRES E&M
    Modifier l'identifiant externe de l'inscription  ${idDemande}

    # Ajout d'une inscription d'office
    Run  sed -i 's/"numeroElecteur": [0-9]*/"numeroElecteur": ${idElecteur}/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Run  sed -i 's/"code": "LP"/"code": "${liste}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Faire une recherche d'électeur REU par ine  ${idElecteur}
    WUX  Select Radio Button  choix_ine  TEST420NOMNAISSANCE;03/07/1950;40030
    Click Element  name:inscription_search_form.action.valid

    &{mouvement_inscription_electeur_lc2} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  liste=${libelle_liste}
    ...  types=INSCRIPTION D'OFFICE
    ...  libelle_voie=RUE BASSE
    Saisir le mouvement d'inscription  ${mouvement_inscription_electeur_lc2}
    ${id_inscription_electeur_lc2} =  Get Value  css=#id


    Depuis le contexte de l'inscription  ${id_inscription_electeur_lc2}
    Compléter le mouvement d'inscription REU  ${DATE_FORMAT_DD/MM/YYYY}
    &{visa} =  Create Dictionary
    ...  select=accepté
    ...  date=${DATE_FORMAT_DD/MM/YYYY}
    Viser le mouvement d'inscription REU  ${visa}

    # On prépare la notification d'inscription d'office afin de valider le mouvement d'inscription précédemment ajouté
    Préparer la notification d'inscription d'office  ${idDemande}  ${idElecteur}  50  ACCEPTEE

    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  ${idDemande}

    # On prépare la notification de radiation INSEE pour l'électeur LCM
    ${idDemande} =  Set Variable  701
    Modifier l'identifiant externe de la radiation INSEE  ${idDemande}
    Préparer la notification d'inscription d'office  ${idDemande}  ${idElecteur}  51  ACCEPTEE  RAD


    Run  sed -i 's/"id": 789456/"id": ${idElecteur}/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt
    Run  sed -i 's/"code": "LP"/"code": "LCM"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt
    # Viser le mouvement de radiation REU  ${visa}

    Synchroniser les notifications
    # On traite la notification de radiation insee de l'électeur en LCM
    Traiter la notification  ${idDemande}
    # On valide le mouvement de radiation
    Valider le mouvement de radiation INSEE  LCM
    # On vérifie que l'électeur LCM a été radié
    Click Link  ../app/changeliste.php
    Select From List By Label  name:liste  LCM - LISTE COMPLÉMENTAIRE MUNICIPALE
    WUX  Click Button  name=changeliste.action.valid

    Depuis le listing de la liste électorale
    Click Element  css=#toggle-advanced-display
    Input Text  css=#ine  ${idElecteur}
    Select From List By Label  css=#liste  LCM - LISTE COMPLÉMENTAIRE MUNICIPALE
    Click Element  adv-search-submit
    Page Should Contain  Aucun enregistrement.
    Input Text  css=#ine  ${idElecteur}
    Select From List By Label  css=#liste  LCE - LISTE COMPLÉMENTAIRE EUROPÉENNE
    Click Element  adv-search-submit
    Element Should Contain  css=td.col-10.lastcol  LCE

    Click Element  link:${idElecteur}
    Page Should Not Contain  Cet électeur a un mouvement en cours

    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut
    Run  sed -i 's/"id": ${idElecteur}/"id": 789456/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt
    Run  sed -i 's/"referenceExterne": "701"/"referenceExterne": "201"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt
    Run  sed -i 's/"numeroElecteur": ${idElecteur}/"numeroElecteur": 123456/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Run  sed -i 's/"code": "LC2"/"code": "LP"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Run  sed -i 's/"code": "LCM"/"code": "LP"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt
    Remettre les réponses par défaut


Vérifier qu'il y a une erreur lors du traitement d'une demande de livrable 'liste d'émargement' non valide
    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence
    # On prépare le mauvais livrable
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_livrable_emarge.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_livrable_emarge_old.txt
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_wrong_livrable_emarge.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_livrable_emarge.txt
    # Envoie d'une demande de livrable emarge au REU
    Go To Submenu In Menu  traitement  module-election
    Click Link  Test COMJ-20 disponible demande out oel
    Click On Form Portlet Action  reu_scrutin  demander_emarge_livrable  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    # Vérification des messages de l'interface
    WUX  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    WUX  Valid Message Should Contain  La demande de livrable EMARGE a été transmise au REU.
    WUX  Page Should Contain  Le livrable liste d'émargement a été demandé (300) le
    WUX  Page Should Contain  En attente du retour de l'INSEE.

    # Synchronisation des notifications pour récupérer la notification livrable
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_wrong_livrable_emarge.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_wrong_livrable_emarge.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  300
    Sleep  30
    WUX  Error Message Should Contain  Erreur lors de l'insertion des données du livrable.
    WUX  Error Message Should Contain  Erreur lors de l'application du traitement. Contactez votre administrateur.

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut
