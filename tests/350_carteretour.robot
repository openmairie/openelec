*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Module Carte En Retour.


*** Test Cases ***
Constitution d'un jeu de données
    [Documentation]
    Depuis la page d'accueil  admin  admin
    # Création d'un électeur pour lui positionner une carte en retour
    &{mouvement01} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST370ELECTEUR01
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE BASSE
    ...  bureauforce=Oui
    ...  bureau=1 HOTEL DE VILLE
    ${mouvement01_id} =  Ajouter le mouvement d'inscription  ${mouvement01}
    Appliquer le traitement de fin d'année
    Sleep  5
    Depuis le contexte de l'inscription  ${mouvement01_id}
    ${electeur01_id} =  Get Value  css=#electeur_id
    ${electeur01_bureau} =  Get Value  css=#bureau
    Set Suite Variable  ${electeur01_id}
    Set Suite Variable  ${mouvement01}
    Set Suite Variable  ${mouvement01_id}


Liste des électeurs avec une carte en retour
    [Documentation]
    Depuis la page d'accueil  admin  admin
    Épurer les cartes en retour
    Depuis l'onglet 'Liste des électeurs' du module 'Carte en retour'
    Element Should Contain  css=#sousform-electeur_carteretour table.tab-tab  Aucun enregistrement.
    Marquer l'électeur avec une carte en retour  ${electeur01_id}
    Depuis l'onglet 'Liste des électeurs' du module 'Carte en retour'
    Element Should Contain  css=#sousform-electeur_carteretour table.tab-tab  ${mouvement01.nom}


Épuration des cartes en retour
    [Documentation]
    Depuis la page d'accueil  admin  admin
    Épurer les cartes en retour
    Marquer l'électeur avec une carte en retour  ${electeur01_id}
    Depuis l'onglet 'Épuration' du module 'Carte en retour'
    Page should Contain  Le nombre d'électeurs étant marqués avec une carte en retour à la date du ${DATE_FORMAT_DD/MM/YYYY} est de 1.
    WUX  Click Button  Épuration des cartes en retour
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    La page ne doit pas contenir d'erreur
    Page should Contain  Le nombre d'électeurs étant marqués avec une carte en retour à la date du ${DATE_FORMAT_DD/MM/YYYY} est de 0.


Édition PDF - Listing des cartes retournées par bureau de vote
    [Documentation]  Test l'édition des cartes en retour dans l'onglet Registre du menu
    ...  Traitement > Cartes En Retour.

    Depuis la page d'accueil  admin  admin
    # Supprime toutes les carte en retour pour vérifier l'affichage du message si aucune
    # carte n'a été récupérée
    Épurer les cartes en retour
    # Accès à l'onglet des cartes en retour
    Depuis l'onglet 'Editions' du module 'Carte en retour'
    Page should Contain  Cet onglet vous permet d'accéder aux différentes éditions du module carte en retour.
    La page ne doit pas contenir d'erreur

    # Remplissage du formulaire
    Select from list by label  css=#liste  LP - LISTE PRINCIPALE
    Select from list by label  css=#bureau  1 HOTEL DE VILLE
    # Affichage du PDF
    Click Element  css=input[name="carte_retour_par_bureau.submit"]
    # Vérification du contenu du PDF
    ${contenu_pdf} =  Create List
    ...  LISTE DES CARTES RETOURNÉES
    ...  Aucun enregistrement selectionne pour le bureau 1
    ...  LP - LISTE PRINCIPALE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Test l'affichage du édition avec des résultats
    Marquer l'électeur avec une carte en retour  ${electeur01_id}
    # Accès à l'onglet des cartes en retour
    Depuis l'onglet 'Editions' du module 'Carte en retour'
    Page should Contain  Cet onglet vous permet d'accéder aux différentes éditions du module carte en retour.
    La page ne doit pas contenir d'erreur

    # Remplissage du formulaire
    Select from list by label  css=#liste  LP - LISTE PRINCIPALE
    Select from list by label  css=#bureau  1 HOTEL DE VILLE
    # Affichage du PDF
    Click Element  css=input[name="carte_retour_par_bureau.submit"]
    ${contenu_pdf} =  Create List
    ...  LISTE DES CARTES RETOURNÉES
    ...  Commune : LIBREVILLE
    ...  Bureau : 1 - HOTEL DE VILLE
    ...  Canton : CANTON DE LIBREVILLE
    ...  Circonscription : CIRCONSCRIPTION DE LIBREVILLE
    ...  LP - LISTE PRINCIPALE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

