<?php
/**
 * Ce script contient la définition de la classe 'GeneralPHP70Test'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "testGeneral_common.php";
final class GeneralPHP70Test extends GeneralCommon {
    public function setUp() {
        $this->common_setUp();
    }
    public function tearDown() {
        $this->common_tearDown();
    }
    public function onNotSuccessfulTest(Throwable $e) {
        $this->common_onNotSuccessfulTest($e);
    }
}
