*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Module Archivage.


*** Test Cases ***
Fonctionnement basique
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  archivage
    Page Title Should Be  Traitement > Module Archivage
    #
    WUX  Click Element  css=#archivage-edition-statistiques
    ${contenu_pdf} =  Create List  Statistiques des mouvements
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    WUX  Click Element  css=#archivage-edition-listing
    ${contenu_pdf} =  Create List  Liste des mouvements à archiver du tableau
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Button  Archivage des mouvements
    Handle Alert
    WUX  Valid Message Should Be  Le traitement est terminé. Voir le détail
