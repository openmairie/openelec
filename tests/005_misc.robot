*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Divers

*** Test Cases ***
Aide à la saisie sur les champs de type date
    [Documentation]  La saisie de la touche espace dans le champ insère la date
    ...  du jour si le champ est vide.
    ...
    ...  Tous les champs dates de l'application sont concernées, cependant on
    ...  teste ici que sur un formulaire.

    Depuis la page d'accueil  admin  admin
    Depuis le formulaire d'ajout d'une procuration
    # Touche espace sur un champ vide
    Press Key  css=#debut_validite  \\32
    ${text} =  Get Value  css=#debut_validite
    Should Be Equal  ${DATE_FORMAT_DD/MM/YYYY}  ${text}
    # Touche espace sur un champ non vide
    Press Key  css=#fin_validite  1
    Press Key  css=#fin_validite  \\32
    ${text} =  Get Value  css=#fin_validite
    Should Be Equal  1  ${text}

