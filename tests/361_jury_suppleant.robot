*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite des jurys d'assises


*** Test Cases ***
Activation du module de tirage au sort des suppléants au jury d'assise
    [Documentation]  Ajoute le paramètre option_module_jures_suppleants et lui donne pour valeur true

    Depuis la page d'accueil  admin  admin
    Ajouter le paramètre depuis le menu  option_module_jures_suppleants  true  null

Vérification du message d'erreur lors du tirage aléatoire
    [Documentation]  Permet de vérifier que lorsque nous avons un nombre de jurés suppléants
    ...  non remplis ou à 0 dans le paramétrage, un message d'erreur nous prévient.

    # Le nombre de suppléant n'est pas pré rempli, on teste donc en cliquant directement sur
    # le bouton de tirage au sort
    Depuis le module Jury D'assises
    Tirer au sort les jurés suppléants
    WUX  Error Message Should Contain  Le nombre d'électeurs à tirer au sort est à 0.

Tirage au sort des jurés suppléants
    [Documentation]  Modifie le paramétrage du tirage au sort pour que 5 titulaire et 10 suppléants
    ...  soient tirés au sort.
    ...  Cas 1 : Tirage au sort des suppléants, 10 jurés suppléants doivent apparaître dans la
    ...          liste préparatoire
    ...  Cas 2 : Tirage au sort des titulaire, les suppléants précedemment tirés au sort doivent
    ...          être déselectionné et 5 titulaires ajoutés. Puis tirage au sort des suppléants.
    ...          10 suppléants doivent être ajoutés à la liste préparatoire.

    # Remet le jury à 0
    Depuis le module Jury D'assises
    Accéder à l'onglet Épuration du module Jury D'assises
    Epurer les jurés
    Wait Until Page Contains  Le traitement est terminé
    # Paramètre le nombre de jurés
    &{values} =  Create Dictionary
    ...  nb_jures=5
    ...  nb_suppleants=10
    Modifier le paramétrage du nombre de jurés du canton  CANTON DE LIBREVILLE  ${values}

    # Cas 1 : Tirage au sort des suppléants et vérification du nombre dans la liste préparatoire
    Depuis le module Jury D'assises
    Tirer au sort les jurés suppléants
    WUX  Valid Message Should Contain  10/10 électeur(s) sélectionné(s) sur 1 canton(s)
    # Si la première ligne est un suppléant alors c'est qu'il n'y a que des suppléants dans le tableau
    # On vérifie donc la première ligne et le nombre de résultat
    Accéder à l'onglet Liste Préparatoire Courante du module Jury D'assises
    Element Should Contain  css=tbody tr:nth-child(1) td.lastcol  Suppléant
    Element Should Contain  css=.pagination-text  1 - 10 enregistrement(s) sur 10

    # Cas 2 : Tirage au sort des titulaire puis des suppléants
    Accéder à l'onglet Tirage Aléatoire du module Jury D'assises
    Tirer au sort les jurés titulaires
    WUX  Valid Message Should Contain  5/5 électeur(s) sélectionné(s) sur 1 canton(s)
    # Vérifie le nombre de résultat et qu'il n'y a pas de suppléants
    Accéder à l'onglet Liste Préparatoire Courante du module Jury D'assises
    Element Should Contain  css=.pagination-text  1 - 5 enregistrement(s) sur 5
    Page Should Not Contain  Suppléant
    # Tirage au sort des suppléants
    Accéder à l'onglet Tirage Aléatoire du module Jury D'assises
    Tirer au sort les jurés suppléants
    WUX  Valid Message Should Contain  10/10 électeur(s) sélectionné(s) sur 1 canton(s)
    # Vérification du nombre dans la liste et qu'a partir de la 6ème lignes on a bien des
    # suppléants
    Accéder à l'onglet Liste Préparatoire Courante du module Jury D'assises
    Element Should Contain  css=.pagination-text  1 - 12 enregistrement(s) sur 15
    Element Should Contain  css=tbody tr:nth-child(6) td.lastcol  Suppléant

Visualisation d'un jurés suppléants sur la fiche de l'électeur
    [Documentation]  Accède à la fiche d'électeur d'un suppléant et vérification qu'il est bien
    ...  mentionné qu'il s'agit d'un suppléant provisoire.
    ...  Remplis les informations du jurés et le marque comme effectif puis vérifie que ces information
    ...  sont bien mise à jour sur la fiche de l'électeur.

    # Recupération de l'id d'un suppléants
    Depuis le module Jury D'assises
    Accéder à l'onglet Liste Préparatoire Courante du module Jury D'assises
    ${id_suppleant} =  Get Text  css=tbody tr:nth-child(6) td:nth-child(2)
    # Accès à la fiche de l'électeur
    Depuis la fiche de l'électeur  ${id_suppleant}
    Element Should Contain  css=#jury_assise  L'electeur fait parti de la liste provisoire courante en tant que suppléant.
    # Saisi des informations du supléants
    &{values} =  Create Dictionary
    ...  jury_effectif=Oui
    ...  date_jeffectif=26/05/2023
    ...  profession=plop
    ...  motif_dispense_jury=plip
    Modifier les informations du juré  ${values}
    # Recharge la page et vérifie la mise à jour des informartions
    Reload Page
    Element Should Contain
    ...  css=#jury_assise
    ...  L'electeur fait parti de la liste provisoire courante en tant que suppléant.\nProfession de l'électeur : plop\nL'electeur est jure suppleant effectif le : 26/05/2023\nMotif de dispense : plip

    # Epuration du jury, le jure doit toujours être noté comme effectif
    Depuis le module Jury D'assises
    Accéder à l'onglet Épuration du module Jury D'assises
    Epurer les jurés
    Wait Until Page Contains  Le traitement est terminé
    Depuis la fiche de l'électeur  ${id_suppleant}
    Element Should Not Contain  css=#jury_assise  L'electeur fait parti de la liste provisoire courante en tant que suppléant.
    Element Should Contain  css=#jury_assise  L'electeur est jure suppleant effectif le : 26/05/2023

Édition de la liste préparatoire des jurés suppléants
    [Documentation]  Tire au sort de 1 titulaire et 5 suppléants et vérifie que sur l'édition de
    ...  la liste préparatoire les 5 suppléants sont visibles mais pas le titulaire

    # Paramètre le nombre de jurés
    Depuis la page d'accueil  admin  admin
    &{values} =  Create Dictionary
    ...  nb_jures=1
    ...  nb_suppleants=2
    Modifier le paramétrage du nombre de jurés du canton  CANTON DE LIBREVILLE  ${values}
    # Tirage au sort
    Depuis le module Jury D'assises
    Tirer au sort les jurés titulaires
    WUX  Valid Message Should Contain  1/1 électeur(s) sélectionné(s) sur 1 canton(s)
    Reload Page
    Tirer au sort les jurés suppléants
    WUX  Valid Message Should Contain  2/2 électeur(s) sélectionné(s) sur 1 canton(s)
    # Récupération de leurs id
    Accéder à l'onglet Liste Préparatoire Courante du module Jury D'assises
    ${id_titulaire} =    Get Text  css=tbody tr:nth-child(1) td:nth-child(2)
    ${nom_titulaire} =    Get Text  css=tbody tr:nth-child(1) td:nth-child(3)
    ${prenom_titulaire} =    Get Text  css=tbody tr:nth-child(1) td:nth-child(4)
    ${id_suppleant_1} =    Get Text  css=tbody tr:nth-child(2) td:nth-child(2)
    ${nom_suppleant_1} =  Get Text  css=tbody tr:nth-child(2) td:nth-child(3)
    ${prenom_suppleant_1} =  Get Text  css=tbody tr:nth-child(2) td:nth-child(4)
    ${id_suppleant_2} =    Get Text  css=tbody tr:nth-child(3) td:nth-child(2)
    ${nom_suppleant_2} =  Get Text  css=tbody tr:nth-child(3) td:nth-child(3)
    ${prenom_suppleant_2} =  Get Text  css=tbody tr:nth-child(3) td:nth-child(4)
    # Remplissage de leurs informations
    ${profession_suppleant_1} =  Set Variable  peuplier
    ${motif_dispense_suppleant_1} =  Set Variable  arbre
    Depuis la fiche de l'électeur  ${id_suppleant_1}
    &{values} =  Create Dictionary
    ...  profession=${profession_suppleant_1}
    ...  motif_dispense_jury=${motif_dispense_suppleant_1}
    Modifier les informations du juré  ${values}
    ${profession_suppleant_2} =  Set Variable  camembert
    ${motif_dispense_suppleant_2} =  Set Variable  odeur
    Depuis la fiche de l'électeur  ${id_suppleant_2}
    &{values} =  Create Dictionary
    ...  profession=${profession_suppleant_2}
    ...  motif_dispense_jury=${motif_dispense_suppleant_2}
    Modifier les informations du juré  ${values}

    # Vérification du contenu de l'édition
    Depuis le module Jury D'assises
    Accéder à l'onglet Tirage Aléatoire du module Jury D'assises
    Ouvrir l'édition Liste préparatoire des suppléants du jury d'assises
    @{suppleants} =  Create List  ${nom_suppleant_1}  ${prenom_suppleant_1}  ${nom_suppleant_2}  ${prenom_suppleant_2}
    @{professions_dispenses_suppleants} =  Create List  ${profession_suppleant_1}  ${motif_dispense_suppleant_1}  ${profession_suppleant_2}  ${motif_dispense_suppleant_2}
    # Vérification de l'affichage des noms et prénoms des suppléants
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${suppleants}
    # Vérification de l'affichage des professions et dispense des suppléants
    Ouvrir l'édition Liste préparatoire des suppléants du jury d'assises
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${professions_dispenses_suppleants}
    # Vérification que le titulaire n'est pas dans la liste
    Ouvrir l'édition Liste préparatoire des suppléants du jury d'assises
    Page Should Not Contain  ${nom_titulaire}
    Page Should Not Contain  ${prenom_titulaire}

    # Enregistrement des noms et prénoms pour les tests suivants
    @{var_to_init} =  Create List  nom_titulaire  prenom_titulaire
    Set Suite Variable  @{suppleants}
    Set Suite Variable  @{professions_dispenses_suppleants}
    Set Suite Variable  ${nom_titulaire}
    Set Suite Variable  ${prenom_titulaire}

Éditions de l'étiquette des jurés suppléants
    [Documentation]  Vérifie que l'édition de l'étiquette affiche bien les étiquettes des 5
    ...  suppléants précedemment tirés au sort et pas celle du titulaire.

    # Vérification du contenu de l'édition
    Depuis le module Jury D'assises
    Accéder à l'onglet Tirage Aléatoire du module Jury D'assises
    Ouvrir l'édition Étiquettes des jurés suppléants
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${suppleants}
    Ouvrir l'édition Liste préparatoire des suppléants du jury d'assises
    Page Should Not Contain  ${nom_titulaire}
    Page Should Not Contain  ${prenom_titulaire}

Exports csv des jurés suppléants
    [Documentation]  Vérifie que l'export contiens bien les informations des 5 suppléants
    ...  précedemment tirés au sort et pas celles du titulaire.

    # Vérification du contenu du csv
    Depuis le module Jury D'assises
    Accéder à l'onglet Tirage Aléatoire du module Jury D'assises
    Ouvrir le csv Export CSV des électeurs suppléants sélectionnés
    :FOR  ${suppleant}  IN  @{suppleants}
    \  Page Should Contain  ${suppleant}
    :FOR  ${profession_dispense}  IN  @{professions_dispenses_suppleants}
    \  Page Should Contain  ${profession_dispense}
    Page Should Not Contain  ${nom_titulaire}
    Page Should Not Contain  ${prenom_titulaire}

Désactivation du module de tirage au sort des suppléants au jury d'assise
    [Documentation]  Supprime le paramètre option_module_jures_suppleants

    Depuis la page d'accueil  admin  admin
    Supprimer le paramètre  option_module_jures_suppleants  true