*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Traitements notification par courriel.


*** Test Cases ***
Constitution d'un jeu de données
    # Positionnement de l'état de l'environnement (ajout de profils et des utilisateurs utilisant ces profils)
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  multi  admin

    Ajouter le profil depuis le menu  TEST430NOTIFICATIONPROFIL  0
    Ajouter le profil depuis le menu  TEST430NOTIFICATIONPROFIL1  0
    Ajouter le profil depuis le menu  TEST430NOTIFICATIONPROFIL2  0

    Depuis la page d'accueil  multi  admin
    Select From List By Label  collectivite  LIBREVILLE
    Initialiser un statut valide  Oui
    # On traite les notifications afin d'avoir une tâches en cours sur le tableau de bord
    Synchroniser les listes électorales

    # Synchronisation des notifications pour récupérer une tache en cours
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_taches_en_cours_courriel.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_taches_en_cours_courriel.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

    ${json} =  Set Variable  { "module": "synchro_last_notifications"}
    Appeler le web service  Post  maintenance  ${json}
    ${json} =  Set Variable  { "module": "treat_all_notifications"}
    Appeler le web service  Post  maintenance  ${json}


    Depuis la page d'accueil  multi  admin
    Ajouter l'utilisateur   mono1  testnotif1@openmairie.org  mono1  mono1  TEST430NOTIFICATIONPROFIL  LIBREVILLE
    Ajouter l'utilisateur   mono2  testnotif2@openmairie.org  mono2  mono2  TEST430NOTIFICATIONPROFIL1  LIBREVILLE


Vérification du message d'erreur lorsqu'il n'y a pas de tâches en cours

    # On ajoute une commune n'ayant pas fait de synchronisation avec le REU le but étant de ne pas avoir de tâches en cours
    Ajouter la collectivité depuis le menu  TEST430NOTIFPROFILCOLL  mono
    Ajouter l'utilisateur   test430user  nospam@openmairie.org  test430notifcoll  test430notifcoll  ADMINISTRATEUR  TEST430NOTIFPROFILCOLL
    Depuis la page d'accueil  test430notifcoll  test430notifcoll
    Changer la date de tableau  10/01/2014
    Depuis la page d'accueil  multi  admin
    ${bureau01} =  Create Dictionary
    ...  code=01
    ...  libelle=BUREAU
    ...  canton=01 CANTON DE LIBREVILLE
    ...  circonscription=CIRCONSCRIPTION DE LIBREVILLE
    ...  om_collectivite=TEST430NOTIFPROFILCOLL
    Ajouter le bureau de vote  ${bureau01}
    Ajouter le paramètre depuis le menu  ville  TEST430NOTIFPROFILCOLL  TEST430NOTIFPROFILCOLL
    Ajouter le paramètre depuis le menu  inseeville  14987  TEST430NOTIFPROFILCOLL
    Ajouter le paramètre depuis le menu  maire  Jacques ROBERT  TEST430NOTIFPROFILCOLL
    Ajouter le paramètre depuis le menu  maire_de  TEST430NOTIFPROFILCOLL  TEST430NOTIFPROFILCOLL

    Depuis la page d'accueil  test430notifcoll  test430notifcoll
    Accéder au controlpanel notification par courriel
    La page ne doit pas contenir d'erreur
    WUX  Element Should Be Visible  css=#bloc_profil
    Element Should Contain  css=#bloc_profil   Profil(s) sélectionné(s) dans cette commune
    Element Should Contain  css=#bloc_profil  Aucun profil sélectionné
    Element Should Contain  css=#bloc_message  Rien à notifier.

    @{profils} =  Create List
    ...  ADMINISTRATEUR
    Sélectionner les profils à paramétrer  ${profils}

    WUX  Click Element  name:traitement.notification_courriel.form.valid
    Handle Alert

    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    WUX  Valid Message Should Contain  Rien à notifier.


Affichage du bloc mono et vérification du traitement

    # En tant qu'admin de la collectivité mono LIBREVILLE
    Depuis la page d'accueil  admin  admin

    # On récupère les chiffres à jour sur le tableau de bord
    ${nb-mes_inscriptions_office_a_valider} =  Get Text  css=#task-mes_inscriptions_office_a_valider-nb
    ${nb-mes_inscriptions_a_instruire} =  Get Text  css=#task-mes_inscriptions_a_instruire-nb
    ${nb-mes_inscriptions_en_attente} =  Get Text  css=#task-mes_inscriptions_en_attente-nb
    ${nb-mes_inscriptions_en_attente_insee} =  Get Text  css=#task-mes_inscriptions_en_attente_insee-nb
    ${nb-mes_inscriptions_a_viser} =  Get Text  css=#task-mes_inscriptions_a_viser-nb
    ${nb-mes_inscriptions_acceptees_insee} =  Get Text  css=#task-mes_inscriptions_acceptees_insee-nb
    ${nb-mes_inscriptions_refusees_insee} =  Get Text  css=#task-mes_inscriptions_refusees_insee-nb
    ${nb-mes_radiations_office_a_valider} =  Get Text  css=#task-mes_radiations_office_a_valider-nb
    ${nb-mes_radiations_a_viser} =  Get Text  css=#task-mes_radiations_a_viser-nb
    ${nb-mes_radiation_en_attente_insee} =  Get Text  css=#task-mes_radiation_en_attente_insee-nb
    ${nb-mes_radiations_acceptees_insee} =  Get Text  css=#task-mes_radiations_acceptees_insee-nb
    ${nb-mes_radiations_refusees_insee} =  Get Text  css=#task-mes_radiations_refusees_insee-nb
    ${nb-mes_modifications_a_valider} =  Get Text  css=#task-mes_modifications_a_valider-nb
    ${nb-mes_modifications_d_etat_civil} =  Get Text  css=#task-mes_modifications_d_etat_civil-nb

    # On compose le complément attendu dans l'envoi de mail
    ${complement} =  Set Variable  \n
    ${complement} =  Set Variable If  ${nb-mes_inscriptions_office_a_valider} == 0  ${complement}  ${complement}Inscriptions d'office à valider : ${nb-mes_inscriptions_office_a_valider}\n
    ${complement} =  Set Variable If  ${nb-mes_inscriptions_acceptees_insee} == 0  ${complement}  ${complement}Inscriptions acceptées par l'INSEE : ${nb-mes_inscriptions_acceptees_insee}\n
    ${complement} =  Set Variable If  ${nb-mes_inscriptions_en_attente_insee} == 0  ${complement}  ${complement}Inscriptions en attente de validation INSEE : ${nb-mes_inscriptions_en_attente_insee}\n
    ${complement} =  Set Variable If  ${nb-mes_inscriptions_refusees_insee} == 0  ${complement}  ${complement}Inscriptions refusées par l'INSEE : ${nb-mes_inscriptions_refusees_insee}\n
    ${complement} =  Set Variable If  ${nb-mes_radiations_office_a_valider} == 0  ${complement}  ${complement}Radiations d'office à valider : ${nb-mes_radiations_acceptees_insee}\n
    ${complement} =  Set Variable If  ${nb-mes_radiations_acceptees_insee} == 0  ${complement}  ${complement}Radiations acceptées par l'INSEE : ${nb-mes_radiations_acceptees_insee}\n
    Set Suite Variable  ${complement}

    # On accède au controlpanel
    Accéder au controlpanel notification par courriel
    La page ne doit pas contenir d'erreur

    #
    WUX  Element Should Be Visible  css=#bloc_profil
    WUX  Element Should Contain  css=#bloc_profil  Profil(s) sélectionné(s) dans cette commune
    WUX  Element Should Contain  css=#bloc_profil  Aucun profil sélectionné

    # Vérification du message d'erreur lors lorsqu'aucun profil n'est paramétré
    WUX  Click Element  name:traitement.notification_courriel.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    WUX  Valid Message Should Contain  Aucun profil n'a été sélectionné.

    # Sélection des profils
    @{profils} =  Create List
    ...  TEST430NOTIFICATIONPROFIL
    Sélectionner les profils à paramétrer  ${profils}

    # Vérification de l'affichage de la liste des profils dans le bloc
    WUX  Element Should Be Visible  css=#bloc_profil
    WUX  Element Should Contain  css=#bloc_profil   Profil(s) sélectionné(s) dans cette commune
    WUX  Element Should Contain  css=#bloc_profil   TEST430NOTIFICATIONPROFIL
    # Vérification du message
    WUX  Element Should Contain   css=#bloc_message  Bonjour,\nVous avez à ce jour :${complement}--\nVotre assistant openElec.\nMerci de ne pas répondre à ce courriel envoyé à [ADRESSE_COURRIEL], par le logiciel openElec pour la commune 00000.

    WUX  Click Element  name:traitement.notification_courriel.form.valid
    Handle Alert

    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    Accéder à maildump
    Vérifier qu'un mail a été envoyé au destinataire  testnotif1@openmairie.org
    Vérifier le contenu du mail  testnotif1@openmairie.org  Bonjour,\nVous avez à ce jour :${complement}--\nVotre assistant openElec.\nMerci de ne pas répondre à ce courriel envoyé à testnotif1@openmairie.org, par le logiciel openElec pour la commune 00000.


Affichage du bloc multi et vérification du traitement

    # En tant qu'admin de la collectivité multi MULTI
    Depuis la page d'accueil  multi  admin
    Modifier l'utilisateur  mono1  testnotif3@openmairie.org  mono1  mono1  TEST430NOTIFICATIONPROFIL2  LIBREVILLE
    Accéder au controlpanel notification par courriel
    La page ne doit pas contenir d'erreur
    WUX  Element Should Be Visible  css=#bloc_profil
    Element Should Contain  css=#bloc_profil   Profil(s) sélectionné(s) dans la collectivité
    Element Should Contain  css=#bloc_profil  Aucun profil sélectionné
    Element Should Contain  css=#bloc_profil  Définir/Modifier
    # Sélection des profils
    @{profils} =  Create List
    ...  TEST430NOTIFICATIONPROFIL1
    ...  TEST430NOTIFICATIONPROFIL2
    Sélectionner les profils à paramétrer  ${profils}

    Depuis la page d'accueil  multi  admin
    Select From List By Label  collectivite  LIBREVILLE
    Accéder au controlpanel notification par courriel
    Désélectionner les profils paramétrés

    Depuis la page d'accueil  multi  admin
    Accéder au controlpanel notification par courriel
    WUX  Click Element  name:traitement.notification_courriel.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    WUX  Valid Message Should Contain  Notification(s) envoyée(s) avec succès pour la collectivité LIBREVILLE.
    WUX  Valid Message Should Contain  Rien à notifier pour la collectivité TEST430NOTIFPROFILCOLL.

    Vérifier qu'un mail a été envoyé au destinataire  testnotif2@openmairie.org
    Vérifier le contenu du mail  testnotif2@openmairie.org  Bonjour,\nVous avez à ce jour :${complement}--\nVotre assistant openElec.\nMerci de ne pas répondre à ce courriel envoyé à testnotif2@openmairie.org, par le logiciel openElec pour la commune 00000.
    Vérifier qu'un mail a été envoyé au destinataire  testnotif3@openmairie.org
    Vérifier le contenu du mail  testnotif3@openmairie.org  Bonjour,\nVous avez à ce jour :${complement}--\nVotre assistant openElec.\nMerci de ne pas répondre à ce courriel envoyé à testnotif3@openmairie.org, par le logiciel openElec pour la commune 00000.

    Depuis la page d'accueil  multi  admin
    Modifier l'utilisateur  mono1  testnotif4@openmairie.org  mono1  mono1  TEST430NOTIFICATIONPROFIL2  LIBREVILLE
    Modifier l'utilisateur   mono2  testnotif5@openmairie.org  mono2  mono2  TEST430NOTIFICATIONPROFIL  LIBREVILLE
    Depuis la page d'accueil  multi  admin
    Select From List By Label  collectivite  LIBREVILLE
    Accéder au controlpanel notification par courriel
     # Sélection des profils
    @{profils} =  Create List
    ...  TEST430NOTIFICATIONPROFIL
    ...  TEST430NOTIFICATIONPROFIL2
    Sélectionner les profils à paramétrer  ${profils}
    WUX  Click Element  name:traitement.notification_courriel.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    WUX  Valid Message Should Contain  La notification a été envoyée avec succès.

    Vérifier qu'un mail a été envoyé au destinataire  testnotif4@openmairie.org
    Vérifier le contenu du mail  testnotif4@openmairie.org  Bonjour,\nVous avez à ce jour :${complement}--\nVotre assistant openElec.\nMerci de ne pas répondre à ce courriel envoyé à testnotif4@openmairie.org, par le logiciel openElec pour la commune 00000.
    Vérifier qu'un mail a été envoyé au destinataire  testnotif5@openmairie.org
    Vérifier le contenu du mail  testnotif5@openmairie.org  Bonjour,\nVous avez à ce jour :${complement}--\nVotre assistant openElec.\nMerci de ne pas répondre à ce courriel envoyé à testnotif5@openmairie.org, par le logiciel openElec pour la commune 00000.

    # Vérifier lorsque les utilisateur ont le même profil
    Depuis la page d'accueil  multi  admin
    Modifier l'utilisateur  mono1  testnotif6@openmairie.org  mono1  mono1  TEST430NOTIFICATIONPROFIL  LIBREVILLE
    Modifier l'utilisateur   mono2  testnotif7@openmairie.org  mono2  mono2  TEST430NOTIFICATIONPROFIL  LIBREVILLE
    Depuis la page d'accueil  multi  admin
    Select From List By Label  collectivite  LIBREVILLE
    Accéder au controlpanel notification par courriel
    @{profils} =  Create List
    ...  TEST430NOTIFICATIONPROFIL
    Sélectionner les profils à paramétrer  ${profils}
    WUX  Click Element  name:traitement.notification_courriel.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    WUX  Valid Message Should Contain  La notification a été envoyée avec succès.

    Vérifier qu'un mail a été envoyé au destinataire  testnotif6@openmairie.org
    Vérifier le contenu du mail  testnotif6@openmairie.org  Bonjour,\nVous avez à ce jour :${complement}--\nVotre assistant openElec.\nMerci de ne pas répondre à ce courriel envoyé à testnotif6@openmairie.org, par le logiciel openElec pour la commune 00000.
    Vérifier qu'un mail a été envoyé au destinataire  testnotif7@openmairie.org
    Vérifier le contenu du mail  testnotif7@openmairie.org  Bonjour,\nVous avez à ce jour :${complement}--\nVotre assistant openElec.\nMerci de ne pas répondre à ce courriel envoyé à testnotif7@openmairie.org, par le logiciel openElec pour la commune 00000.

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut

Vérification du bon fonctionnement de l'api REST
    [Documentation]
    # On positionne un fichier de conf spécial pour s'assurer que c'est
    # avec un utilisateur MULTI que l'appel au WS va se faire
    Copy File  ${PATH_BIN_FILES}services_multi.inc.php  ..${/}dyn${/}services.inc.php

    # Cas n°1 :
    ${json} =  Set Variable  { "module": "notification_courriel"}
    Vérifier le code retour du web service et vérifier que son message est
    ...  Post  maintenance  ${json}
    ...  200  Notification(s) envoyée(s) avec succès pour la collectivité LIBREVILLE.\rRien à notifier pour la collectivité TEST430NOTIFPROFILCOLL.\r

    # Cas n°2 :
    Depuis la page d'accueil  test430notifcoll  test430notifcoll
    Accéder au controlpanel notification par courriel
    Désélectionner les profils paramétrés
    ${json} =  Set Variable  { "module": "notification_courriel"}
    Vérifier le code retour du web service et vérifier que son message est
    ...  Post  maintenance  ${json}
    ...  200  Notification(s) envoyée(s) avec succès pour la collectivité LIBREVILLE.\rAucun utilisateur à notifier pour la collectivité TEST430NOTIFPROFILCOLL.\r

    # Cas n°3 :
    Depuis la page d'accueil  multi  admin
    Accéder au controlpanel notification par courriel
    Désélectionner les profils paramétrés
    Arrêter maildump
    ${json} =  Set Variable  { "module": "notification_courriel"}
    Vérifier le code retour du web service et vérifier que son message est
    ...  Post  maintenance  ${json}
    ...  500  La notification n'a pas pu être envoyée à l'utilisateur se prénomant mono1 de la commune LIBREVILLE ayant l'adresse email testnotif6@openmairie.org.\rLa notification n'a pas pu être envoyée à l'utilisateur se prénomant mono2 de la commune LIBREVILLE ayant l'adresse email testnotif7@openmairie.org.\rLa collectivité TEST430NOTIFPROFILCOLL n'a pas de profil paramétré.\r

    # Cas n°4 :
    Démarrer maildump
    Depuis la page d'accueil  admin  admin
    Accéder au controlpanel notification par courriel
    Désélectionner les profils paramétrés
    ${json} =  Set Variable  { "module": "notification_courriel"}
    Vérifier le code retour du web service et vérifier que son message est
    ...  Post  maintenance  ${json}
    ...  200  La collectivité LIBREVILLE n'a pas de profil paramétré.\rLa collectivité TEST430NOTIFPROFILCOLL n'a pas de profil paramétré.\r

    # On repositionne le fichier de conf de base
    Copy File  ${PATH_BIN_FILES}dyn${/}services.inc.php  ..${/}dyn${/}
