*** Settings ***
Documentation     Ressources (librairies, keywords et variables)

# Mots-clefs framework
Library  openmairie.robotframework.Library

# Mots-clefs métier
Resource  app${/}common.robot
Resource  app${/}bureau.robot
Resource  app${/}canton.robot
Resource  app${/}carteretour.robot
Resource  app${/}circonscription.robot
Resource  app${/}electeur.robot
Resource  app${/}formulaire.robot
Resource  app${/}framework-override.robot
Resource  app${/}inscription.robot
Resource  app${/}jury.robot
Resource  app${/}radiation.robot
Resource  app${/}mouvements.robot
Resource  app${/}pdf.robot
Resource  app${/}procuration.robot
Resource  app${/}traitements.robot
Resource  app${/}voie.robot
Resource  app${/}reu.robot
Resource  app${/}maildump.robot
Resource  app${/}notification_courriel.robot
Resource  app${/}modification.robot

*** Variables ***
${ADMIN_PASSWORD}  admin
${ADMIN_USER}      admin
${BROWSER}         firefox
${DELAY}           0
${PATH_BIN_FILES}  ${EXECDIR}${/}binary_files${/}
${PROJECT_NAME}    openelec
${PROJECT_URL}     http://${SERVER}/${PROJECT_NAME}/
${SERVER}          localhost
${TITLE}           :: openMairie :: openElec - Gestion des listes électorales
${SESSION_COOKIE}  openelec
${PATH_REU_RESOURCE_TEST_REFERENCE}  ${EXECDIR}${/}binary_files${/}reu_resource_test${/}fichier_txt${/}
${PATH_REU_RESOURCE_TEST_PLUGIN}  ${EXECDIR}${/}binary_files${/}reu_resource_test${/}plugin${/}
${PATH_REU_RESOURCE_TEST_WF}  ${EXECDIR}${/}reu_resource_test_wf${/}


*** Keywords ***
For Suite Setup
    Reload Library  openmairie.robotframework.Library
    # Les keywords définit dans le resources.robot sont prioritaires
    Set Library Search Order  resources
    Ouvrir le navigateur
    Tests Setup

For Suite Teardown
    Fermer le navigateur
