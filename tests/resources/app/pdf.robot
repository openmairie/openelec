*** Settings ***
Documentation  Actions sur un PDF visionné depuis Firefox.

*** Keywords ***
Vérifier Que Le PDF Contient Des Strings
    [Tags]
    [Documentation]  Change de fenêtre, fait les verifs, et ferme la fenêtre.
    ...  Il faut donc avoir déclenché l'ouverture de la fenêtre avant.
    ...  L'argument window est {name, title, url} de la fenêtre sans le suffixe '.php'
    [Arguments]  ${window}  ${strings_to_find}  ${from_system}=False

    Run Keyword If  ${from_system}==False  Open PDF  ${window}
    :FOR  ${string}  IN  @{strings_to_find}
    \    WUX  Page Should Contain  ${string}

    La page ne doit pas contenir d'erreur
    Run Keyword If  ${from_system}==False  Close PDF

Vérifier Que Le PDF Contient Des Strings Mais Pas D'Autres
    [Tags]
    [Documentation]  Change de fenêtre, fait les verifs, et ferme la fenêtre.
    ...  Il faut donc avoir déclenché l'ouverture de la fenêtre avant.
    ...  L'argument window est {name, title, url} de la fenêtre sans le suffixe '.php'
    [Arguments]  ${window}  ${strings_to_find}  ${strings_not_to_find}

    Open PDF  ${window}
    :FOR  ${string}  IN  @{strings_to_find}
    \    WUX  Page Should Contain  ${string}

    :FOR  ${string}  IN  @{strings_not_to_find}
    \    WUX  Page Should Not Contain  ${string}

    La page ne doit pas contenir d'erreur
    Close PDF
