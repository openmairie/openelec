*** Keywords ***
Depuis le listing des bureaux
    Depuis le listing  bureau
    Page Title Should Be  Administration & Paramétrage > Découpage > Bureaux


Depuis le formulaire d'ajout d'un bureau de vote
    Go To  ${PROJECT_URL}/app/index.php?module=form&obj=bureau&action=0&retour=form
    WUX  Page Title Should Be  Administration & Paramétrage > Découpage > Bureaux


Ajouter le bureau de vote
    [Documentation]  ...
    [Arguments]  ${values}
    [Return]  ${bureau_id}
    Depuis le formulaire d'ajout d'un bureau de vote
    Saisir les informations dans le formulaire du bureau de vote  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${bureau_id} =  Get Text  css=div.form-content span#id


Saisir les informations dans le formulaire du bureau de vote
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "canton" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "circonscription" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

Modifier le bureau
    [Documentation]  Modifie le bureau
    [Arguments]  ${values}
    Depuis le menu 'Administration & Paramétrage'
    Input Text  css=#filter  bureau
    Click Link  css=a[href="../app/index.php?module=tab&obj=bureau"]
    Click Link  ${values.bureau}
    Click On Form Portlet Action  bureau  modifier
    Saisir les informations dans le formulaire du bureau de vote  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.