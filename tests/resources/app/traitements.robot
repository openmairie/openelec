*** Keywords ***
Appliquer le traitement de fin d'année
    [Tags]
    [Documentation]  Appliquer le traitement de fin d'année

    Go To  ${PROJECT_URL}/app/index.php?module=module_traitement_annuel
    WUX  Click Button  name=traitement.annuel.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé.

Appliquer l'archivage
    [Tags]
    [Documentation]  Appliquer le traitement d'archivage
    Go To Submenu In Menu  traitement  archivage
    Click Button  Archivage des mouvements
    Handle Alert
    WUX  Valid Message Should Be  Le traitement est terminé. Voir le détail


L'onglet de traitement doit être présent
    [Tags]
    [Documentation]
    [Arguments]    ${id}=null    ${libelle}=null

    #
    ${locator} =    Catenate    SEPARATOR=    css=#traitement-tabs ul.ui-tabs-nav li a#    ${id}
    #
    Element Text Should Be    ${locator}    ${libelle}


L'onglet de traitement doit être sélectionné
    [Tags]
    [Documentation]
    [Arguments]    ${id}=null    ${libelle}=null

    #
    ${locator} =    Catenate    SEPARATOR=    css=#traitement-tabs ul.ui-tabs-nav li.ui-tabs-selected a#    ${id}
    #
    Element Text Should Be    ${locator}    ${libelle}


On clique sur l'onglet de traitement
    [Tags]
    [Documentation]
    [Arguments]    ${id}=null    ${libelle}=null

    #
    ${locator} =    Catenate    SEPARATOR=    css=#traitement-tabs ul.ui-tabs-nav li a#    ${id}
    #
    L'onglet de traitement doit être présent    ${id}    ${libelle}
    #
    Click Element    ${locator}
    #
    L'onglet de traitement doit être sélectionné    ${id}    ${libelle}
    #
    Sleep    1
    #
    La page ne doit pas contenir d'erreur
