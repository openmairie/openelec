*** Settings ***
Documentation  Actions spécifiques au controlpanel de notification par courriel

*** Keywords ***

Accéder au controlpanel notification par courriel
    [Tags]
    [Documentation]  Accéder au module notification par courriel.
    Go To  ${PROJECT_URL}/app/index.php?module=module_notification_courriel
    WUX  Page Title Should Be  Paramétrage > Module Notification Par Courriel

Sélectionner les profils à paramétrer
    [Tags]
    [Documentation]  Permet de sélectionner les profils des utilisateurs qui recevront la notification.
    [Arguments]  ${profils}
    Désélectionner les profils paramétrés
    WUX  Click Element  css=#action-notification-select-profil
    Select Multiple By Label  profil_selection  ${profils}
    Click Element  selectprofil.action.valid
    Click Element  css=.ui-dialog .ui-dialog-titlebar-close

Désélectionner les profils paramétrés
    [Documentation]  Permet de Désélectionner les profils paramétrés
    Click Element  css=#action-notification-select-profil
    Click Element  selectprofil.action.valid
    Click Element  css=.ui-dialog .ui-dialog-titlebar-close