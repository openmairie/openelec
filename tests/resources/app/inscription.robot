*** Settings ***
Documentation  Actions spécifiques aux inscriptions

*** Keywords ***
Depuis le listing des inscriptions
    [Documentation]  ...
    [Tags]  inscription
    Depuis le listing  inscription


Depuis le contexte de l'inscription
    [Documentation]  ..
    [Arguments]  ${mouvement}
    [Tags]  inscription
    Depuis le listing des inscriptions
    Rechercher en recherche avancée simple  ${mouvement}
    Click Element  css=#action-tab-inscription-left-consulter-${mouvement}
    La page ne doit pas contenir d'erreur

Faire une recherche d'électeur REU par ine
    [Documentation]  Permet de faire une recherche d'électeur appartenant au reu avec son ine
    [Arguments]  ${ine}
    Go To Submenu In Menu  saisie  inscription
    Select From List By Value  css=#recherche_par  ine
    WUX  Element Should Be Visible  css=#ine
    Input Text  css=#ine  ${ine}
    CLick On Submit Button

Faire une recherche d'électeur REU par état civil
    [Documentation]  Permet de faire une recherche d'électeur appartenant au reu avec son état civil
    [Arguments]  ${etat_civil}
    Go To Submenu In Menu  saisie  inscription
    Select From List By Value  css=#recherche_par  etat_civil
    WUX  Input Text  css=#nom  ${etat_civil.nom}
    WUX  Input Text  css=#prenom  ${etat_civil.prenom}
    WUX  Input Datepicker  datenaissance  ${etat_civil.date_naissance}
    WUX  Select From List By Value  css=#sexe  ${etat_civil.sexe}
    CLick On Submit Button

Abandonner le mouvement d'inscription REU
    [Documentation]  Clique sur l'action abandonner du mouvement
    WUX  Click On Form Portlet Action  inscription  reu-abandonner  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Form Value Should Be  css=#statut  abandonne
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur

Mettre en attente le mouvement d'inscription REU
    [Documentation]  Clique sur l'action 'attendre' du mouvement
    [Arguments]  ${id_inscription}='null'
    [Tags]  inscription  REU
    Run keyword if  ${id_inscription} != 'null'
    ...  Depuis le contexte de l'inscription  ${id_inscription}
    Click On Form Portlet Action  inscription  reu-attendre  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Valid Message Should Be  L'inscription a été correctement mise en attente.
    La page ne doit pas contenir d'erreur
    WUX  Form Value Should Be  css=#statut  en_attente

Compléter le mouvement d'inscription REU
    [Documentation]  Clique sur l'action completer du mouvement et ajoute une date de complétude
    [Arguments]  ${date_completude}
    WUX  Click On Form Portlet Action  inscription  reu-completer
    Input Datepicker  date_complet  ${date_completude}
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    WUX  Form Value Should Be  css=#statut  complet
    WUX  Form Value Should Be  css=#date_complet  ${date_completude}
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur

Viser le mouvement d'inscription REU
    [Documentation]  Clique sur l'action completer du mouvement et ajoute une date de complétude
    [Arguments]  ${visa}
    WUX  Click On Form Portlet Action  inscription  reu-viser
    WUX  Select From List By Label  css=#visa  ${visa.select}
    Input Datepicker  date_visa  ${visa.date}
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    ${result} =  Evaluate  """${visa.select}""" == """accepté"""
    Run Keyword If  ${result} == True  WUX  Form Value Should Be  css=#visa  accepte
    ${result} =  Evaluate  """${visa.select}""" == """refusé"""
    Run Keyword If  ${result} == True  WUX  Form Value Should Be  css=#visa  refuse
    WUX  Form Value Should Be  css=#statut  vise_maire
    WUX  Form Value Should Be  css=#date_visa  ${visa.date}
        Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur

Vérifier que les champs sont static
    [Documentation]  Vérifie que les champs rempli lors d'une insciption à partir du REU sont static
    WUX  Element Should Not Be Visible  css=#ine
    WUX  Element Should Not Be Visible  css=#nom
    WUX  Element Should Not Be Visible  css=#prenom
    WUX  Element Should Not Be Visible  css=#sexe
    WUX  Element Should Not Be Visible  css=#date_naissance
    WUX  Element Should Not Be Visible  css=#libelle_lieu_de_naissance
    WUX  Element Should Be Visible  css=#code_nationalite

Vérifier que les champs ne sont pas static
    [Documentation]  Vérifie que les champs rempli lors d'une insciption à partir du REU sont static
    WUX  Element Should Not Be Visible  css=#ine
    WUX  Element Should Be Visible  css=#nom
    WUX  Element Should Be Visible  css=#prenom
    WUX  Element Should Be Visible  css=#sexe
    WUX  Element Should Be Visible  css=#date_naissance
    WUX  Element Should Be Visible  css=#code_nationalite

Acceder au formulaire d'ajout d'un mouvement
    [Documentation]  Accède au formulaire d'ajout d'un mouvement d'inscription.
    [Arguments]  ${mvt_name}

    Go To Submenu In Menu  saisie  inscription
    Page Title Should Be  Saisie > Nouvelle Inscription
    # Sélection du mouvement
    Input Text  css=#nom  ${mvt_name}
    Click On Submit Button

Ajouter le mouvement d'inscription
    [Documentation]  Accède au formulaire d'ajout d'un mouvement d'isncription, le rempli
    ...  et clique sur le bouton de validation.
    ...  Récupère l'identifiant du mouvement ajouter et le renvoie.
    [Arguments]  ${mouvement}
    [Return]  ${mouvement_id}

    Acceder au formulaire d'ajout d'un mouvement  ${mouvement.nom}
    Saisir le mouvement d'inscription  ${mouvement}
    # Récupération de l'identifiant du mouvement dans le formulaire
    ${mouvement_id} =  Get Element Attribute  css=.form-content #id  value

Saisir le mouvement d'inscription
    [Documentation]  Rempli le formulaire du mouvement d'inscription et clique sur le bouton
    ...  de validation.
    [Arguments]  ${mouvement}

    Remplir le formulaire de mouvement d'inscription  ${mouvement}
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.

Remplir le formulaire de mouvement d'inscription
    [Documentation]  Rempli le formulaire du mouvement d'inscription.
    [Arguments]  ${mouvement}

    Si "date_demande" existe dans "${mouvement}" on execute "Input Datepicker" dans le formulaire
    Si "liste" existe dans "${mouvement}" on execute "Select From List By Label" dans le formulaire
    Si "bureauforce" existe dans "${mouvement}" on execute "Select From List By Label" dans le formulaire
    Si "types" existe dans "${mouvement}" on execute "Select From List By Label" dans le formulaire
    Si "nom" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "nom_usage" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "date_naissance" existe dans "${mouvement}" on execute "Input Datepicker" dans le formulaire
    Si "bureau" existe dans "${mouvement}" on execute "Select From List By Label" dans le formulaire
    Si "naissance_type_saisie" existe dans "${mouvement}" on execute "Select From List By Label" dans le formulaire
    Si "pays_de_naissance" existe dans "${mouvement}" on sélectionne la valeur sur l'autocomplete "pays-naissance" dans le formulaire
    Si "commune_de_naissance" existe dans "${mouvement}" on sélectionne la valeur sur l'autocomplete "commune-naissance" dans le formulaire
    Si "ancien_departement_francais_algerie" existe dans "${mouvement}" on sélectionne la valeur sur l'autocomplete "ancien-departement-francais-algerie" dans le formulaire
    Si "libelle_lieu_de_naissance" existe dans "${mouvement}" on execute "Input Text" dans le formulaire

    Input Text  css=#autocomplete-voie-search  ${mouvement.libelle_voie}
    WUX  Click On Link  ${mouvement.libelle_voie}
    #
    Si "numero_habitation" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    # Adresse de rattachement
    Si "resident" existe dans "${mouvement}" on execute "Select From List By Label" dans le formulaire
    ${exist} =    Run Keyword and Return Status    Evaluate    "${mouvement.resident}"=="Oui"
    Run Keyword If  ${exist} == True
    ...  Saisir l'adresse de rattachement  ${mouvement}

    Si "telephone" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "courriel" existe dans "${mouvement}" on execute "Input Text" dans le formulaire

Saisir l'adresse de rattachement
    [Documentation]  Saisie de l'adresse de rattachement
    [Arguments]  ${mouvement}

    Wait Until Element Is Visible  css=#ville_resident
    Si "adresse_resident" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "complement_resident" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "cp_resident" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "ville_resident" existe dans "${mouvement}" on execute "Input Text" dans le formulaire

Préparer une réponse
    [Arguments]  ${reponse}
    ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key    ${reponse}    message

    ${result} =  Evaluate  """${reponse.mode}"""=="""add"""
    Run Keyword If  ${result} == True  Run  sed -i 's/"code" : "[0-9]*"/"code" : "${reponse.code}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_add.txt
    Run Keyword If  ${exist} == True and ${result} == True  Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "${reponse.message}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_add.txt

    ${result} =  Evaluate  """${reponse.mode}"""=="""simple"""
    Run Keyword If  ${result} == True   Run  sed -i 's/"code" : "[0-9]*"/"code" : "${reponse.code}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_response.txt
    Run Keyword If  ${exist} == True and ${result} == True  Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "${reponse.message}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_response.txt

    ${result} =  Evaluate  """${reponse.mode}"""=="""elec_ine"""
    Run Keyword If  ${result} == True   Run  sed -i 's/"code" : "[0-9]*"/"code" : "${reponse.code}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Run Keyword If  ${exist} == True and ${result} == True  Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "${reponse.message}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_ressource_test_electeur_ine.txt

    ${result} =  Evaluate  """${reponse.mode}"""=="""elec_etat_civil"""
    Run Keyword If  ${result} == True   Run  sed -i 's/"code" : "[0-9]*"/"code" : "${reponse.code}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_etat_civil.txt
    Run Keyword If  ${exist} == True and ${result} == True  Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "${reponse.message}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_ressource_test_electeur_etat_civil.txt

    ${result} =  Evaluate  """${reponse.mode}"""=="""notif"""
    Run Keyword If  ${result} == True   Run  sed -i 's/"code" : "[0-9]*"/"code" : "${reponse.code}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Run Keyword If  ${exist} == True and ${result} == True  Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "${reponse.message}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

    ${result} =  Evaluate  """${reponse.mode}"""=="""radiation"""
    Run Keyword If  ${result} == True   Run  sed -i 's/"code" : "[0-9]*"/"code" : "${reponse.code}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation.txt
    Run Keyword If  ${exist} == True and ${result} == True  Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "${reponse.message}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation.txt

    ${result} =  Evaluate  """${reponse.mode}"""=="""inscription_insee"""
    Run Keyword If  ${result} == True   Run  sed -i 's/"code" : "[0-9]*"/"code" : "${reponse.code}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_inscription.txt
    Run Keyword If  ${exist} == True and ${result} == True  Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "${reponse.message}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_inscription.txt

    ${result} =  Evaluate  """${reponse.mode}"""=="""radiation_insee"""
    Run Keyword If  ${result} == True   Run  sed -i 's/"code" : "[0-9]*"/"code" : "${reponse.code}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt
    Run Keyword If  ${exist} == True and ${result} == True  Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "${reponse.message}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt

Remettre les réponses par défaut
    Run  sed -i 's/"code" : "[0-9]*"/"code" : "201"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_add.txt
    Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "Created"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_add.txt
    #
    Run  sed -i 's/"code" : "[0-9]*"/"code" : "200"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_response.txt
    Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "OK"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_response.txt
    #
    Run  sed -i 's/"code" : "[0-9]*"/"code" : "200"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "OK"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    #
    Run  sed -i 's/"code" : "[0-9]*"/"code" : "200"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_etat_civil.txt
    Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "OK"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_etat_civil.txt
    #
    Run  sed -i 's/"X-App-params" : "[0-9]*"/"X-App-params" : "114"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_add.txt
    #
    Run  sed -i 's/"code" : "[0-9]*"/"code" : "200"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "OK"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt
    #
    Run  sed -i 's/"code" : "[0-9]*"/"code" : "200"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation.txt
    Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "OK"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation.txt
    #
    Run  sed -i 's/"code" : "[0-9]*"/"code" : "200"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_inscription.txt
    Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "OK"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_inscription.txt
    #
    Run  sed -i 's/"code" : "[0-9]*"/"code" : "200"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt
    Run  sed -i 's/"message" : "[a-zA-Z]* *[a-zA-Z]*"/"message" : "OK"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt

Modifier l'identifiant externe de l'inscription
    [Arguments]  ${id_externe}
    Run  sed -i 's/"X-App-params" : "[0-9]*"/"X-App-params" : "${id_externe}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_mouvement_add.txt

Vérifier que les champs sont correct pour l'état civil
    [Arguments]  ${etat_civil}
    WUX  Form Value Should Be  css=#nom  ${etat_civil.nom}
    WUX  Form Value Should Be  css=#prenom  ${etat_civil.prenom}
    WUX  Form Value Should Be  css=#sexe  ${etat_civil.sexe}
    WUX  Form Value Should Be  css=#date_naissance  ${etat_civil.date_naissance}

Valider le mouvement d'inscription INSEE
    [Arguments]  ${nom_electeur}
    Depuis le listing des inscriptions
    Rechercher en recherche avancée simple  ${nom_electeur}
    Click On Link  ${nom_electeur}
    Click On Form Portlet Action  inscription  valider  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only

Préparer la notification d'inscription d'office
    [Documentation]  Prépare la notification d'inscription d'office pour pouvoir la traiter.
    ...  Ce keyword prend différents paramètres permettant de modéliser la notification.
    ...  Paramètre indispensable pour que la notification soient traité : idDemande, idElecteur, id (de notification)
    ...  L'idDemande doit être identique à l'identifiant externe du mouvement d'inscription ajouté au préalable.
    ...  Si un de ces paramètres a déjà été saisi pour un autre mouvement alors la validation du mouvement risque de ne pas aboutir.
    [Arguments]  ${idDemande}  ${idElecteur}  ${idNotif}  ${accepte_refuse}=ACCEPTEE  ${code}=INS
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_notifications_inscription_office.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Run  sed -i 's/"id": [0-9]*/"id": ${idNotif}/' ${PATH_REU_RESOURCE_TEST_WF}ressource_notifications_inscription_office.txt
    Run  sed -i 's/"idElecteur": [0-9]*/"idElecteur": ${idElecteur}/' ${PATH_REU_RESOURCE_TEST_WF}ressource_notifications_inscription_office.txt
    Run  sed -i 's/"idDemande": "[0-9]*"/"idDemande": "${idDemande}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_notifications_inscription_office.txt
    Run  sed -i 's/"code": "ACCEPTEE"/"code": "${accepte_refuse}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_notifications_inscription_office.txt
    Run  sed -i 's/"libelle": "ACCEPTEE"/"libelle": "${accepte_refuse}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_notifications_inscription_office.txt
    Run  sed -i 's/"code": "INS"/"code": "${code}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_notifications_inscription_office.txt
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_notifications_inscription_office.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt


