*** Keywords ***
Depuis le listing des cantons
    Depuis le listing  canton
    Page Title Should Be  Administration & Paramétrage > Découpage > Cantons

Modifier le canton
    [Documentation]  Modifie le canton
    [Arguments]  ${values}
    Depuis le menu 'Administration & Paramétrage'
    Input Text  css=#filter  canton
    Click Link  css=a[href="../app/index.php?module=tab&obj=canton"]
    Click Link  ${values.code}
    Click On Form Portlet Action  canton  modifier
    Saisir les informations dans le formulaire du canton  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Saisir les informations dans le formulaire du canton
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire