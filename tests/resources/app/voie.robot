*** Keywords ***
Depuis le listing des voies
    Depuis le listing  voie
    Page Title Should Be  Administration & Paramétrage > Découpage > Voies


Depuis le formulaire d'ajout d'une voie
    [Documentation]
    [Tags]
    Depuis le listing des voies
    Click On Add Button

Depuis le contexte de la voie
    [Documentation]  Permet d'accéder à la voie
    [Arguments]  ${code_voie}
    Depuis le listing des voies
    Input Text  name:recherche  ${code_voie}
    Click Element  id:search-submit
    Click Link  ${code_voie}

Ajouter la voie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    [Tags]
    Depuis le formulaire d'ajout d'une voie
    Saisir les valeurs dans le formulaire de la voie  ${values}
    # On valide le formulaire
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${voie_id} =  Get Value  css=#code
    [Return]  ${voie_id}


Saisir les valeurs dans le formulaire de la voie
    [Documentation]  Remplit le formulaire
    ...  &{voie} =  Create Dictionary
    ...  ...  libelle_voie=RUE DE LA LOI
    ...  ...  cp=96125
    ...  ...  ville=LIBREVILLE
    [Arguments]  ${values}
    [Tags]
    Si "libelle_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "abrege" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

Depuis le listing des decoupages de la voie
    [Documentation]  Accède au sous onglet découpage d'une voie.
    [Arguments]  ${code_voie}

    Depuis le contexte de la voie  ${code_voie}
    Click Element  css=#decoupage

Ajouter le découpage
    [Documentation]  Crée le découpage sur la voie
    [Arguments]  ${values}

    Depuis le listing des decoupages de la voie  ${values.code_voie}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Add Button
    Saisir les valeurs dans le formulaire de découpage  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${decoupage_id} =  Get Text  css=#id
    [Return]  ${decoupage_id}

Saisir les valeurs dans le formulaire de découpage
    [Arguments]  ${values}
    Select From List By Label  css=#bureau  ${values.bureau}
    Si "premier_pair" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dernier_pair" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "premier_impair" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dernier_impair" existe dans "${values}" on execute "Input Text" dans le formulaire

Fusionner les voies
    [Documentation]  Fusionne les deux voies passées en paramètres. Si un label est donnée
    ...  la nouvelle voie aura ce label sinon elle prendra le nom de la première voie.
    [Arguments]  ${voie1}  ${voie2}  ${label}=${EMPTY}

    # Accès au formulaire de découpage des voies
    Go To Submenu In Menu    traitement    redecoupage
    Click Link    Initialisation
    Click Element    css=#normalisation_voies>legend
    Wait Until Element Is Visible  css=#${voie1}
    # Selection des voies à fusionner
    Select Checkbox    css=#${voie1}
    Select Checkbox    css=#${voie2}
    # Saisie du label de la nouvelle voie
    Run Keyword If  "${label}"!="${EMPTY}"
    ...  Input Text  name=new_label  ${label}
    ...  ELSE
    ...  Input Text  name=new_label  ${voie1}
    # Validation de la fusion
    Click Element    name=traitement.decoupage_normalisation_voies.form.valid
    Handle Alert
    # Vérification de la validation du traitement
    La page ne doit pas contenir d'erreur
    Valid Message Should Be    Le traitement est terminé. Voir le détail


