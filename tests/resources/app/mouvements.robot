*** Settings ***
Documentation  Actions spécifiques aux mouvements

*** Keywords ***
Ajouter une piece au mouvement
    [Documentation]  Ajouter la pièce au mouvement
    [Arguments]  ${values}
    [Return]  ${id}

    ${result_inscription} =  Evaluate  """${values.type_mouvement}""" == """inscription"""
    Run Keyword If  ${result_inscription} == True  Depuis le contexte de l'inscription  ${values.mouvement}

    ${result_modification} =  Evaluate  """${values.type_mouvement}""" == """modification"""
    #Keyword "Depuis le contexte" à ajouter
    Run Keyword If  ${result_modification} == True  Depuis le contexte de la modification  ${values.mouvement}

    ${result_radiation} =  Evaluate  """${values.type_mouvement}""" == """radiation"""
    #Keyword "Depuis le contexte" à ajouter
    Run Keyword If  ${result_radiation} == True  Depuis le contexte de la radiation  ${values.mouvement}

    # On accède au tableau
    On clique sur l'onglet  piece  Pièce(s)
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir la piece  ${values}
    # On valide le formulaire
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id


Saisir la piece
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "piece_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "fichier" existe dans "${values}" on execute "Add File" dans le formulaire


Récupérer l'uid de la pièce
    [Documentation]  Récupère l'identifiant unique de la pièce
    [Arguments]  ${values}
    [Return]  ${piece_uid}

    Depuis le formulaire de modification de la piece  ${values}
    ${piece_uid} =  Get Value  css=input#fichier


Depuis le formulaire de modification de la piece
    [Documentation]  Permet d'accèder au formulaire de modification de la pièce
    [Arguments]  ${values}

    ${result_inscription} =  Evaluate  """${values.type_mouvement}""" == """inscription"""
    Run Keyword If  ${result_inscription} == True  Depuis le contexte de l'inscription  ${values.mouvement}

    ${result_modification} =  Evaluate  """${values.type_mouvement}""" == """modification"""
    #Keyword "Depuis le contexte" à ajouter
    Run Keyword If  ${result_modification} == True  Depuis le contexte de la modification  ${values.mouvement}

    ${result_radiation} =  Evaluate  """${values.type_mouvement}""" == """radiation"""
    #Keyword "Depuis le contexte" à ajouter
    Run Keyword If  ${result_radiation} == True  Depuis le contexte de la radiation  ${values.mouvement}

    On clique sur l'onglet  piece  Pièce(s)
    Depuis le contexte de la pièce  ${values.piece_id}
    # On clique sur l'action modifier
    Click On SubForm Portlet Action    piece    modifier


Ajouter un motif dans les paramètres de mouvement
    [Documentation]  Ajoute un motif
    [Arguments]  ${values}
    Depuis le menu 'Administration & Paramétrage'
    Input Text  css=#filter  mouvement
    Click Link  css=a[href="../app/index.php?module=tab&obj=param_mouvement"]
    Wait Until Page Contains Element  css=select[name="selectioncol"]
    Select From List By Label  name:selectioncol  Code
    Input Text  name:recherche  ${values.code}
    Click Element  css=#search-submit
    ${expression} =  WUX  Get Text  css=.pagination-text
    Return from keyword If  """${expression}""" == """1 - 1 enregistrement(s) sur 1 = [${values.code}]"""
    Click On Add Button
    Input Text  css=#code  ${values.code}
    Input Text  css=#libelle  ${values.libelle}
    Select From List By Label  css=#effet  ${values.effet}
    Select From List By Label  css=#typecat  ${values.categorie}
    WUX  Input Datepicker  om_validite_debut  ${values.datedeb}
    WUX  Input Datepicker  om_validite_fin  ${values.datefin}
    Click On Submit Button

Depuis le contexte de la pièce
    [Documentation]  Permet d'accéder au formulaire du document généré.
    [Arguments]  ${piece_id}

    # On fait une recherche sur l'id de la piece
    Input Text  css=#recherchedyn  ${piece_id}
    # On accède à la visualisation de la piece
    Click On Link  ${piece_id}
    #
    Page Title Should Contain  ${piece_id}
