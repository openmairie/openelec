*** Settings ***
Documentation  Actions spécifiques aux procurations

*** Keywords ***
Ajouter ou modifier le paramètre
    [Tags]
    [Arguments]  ${values}
    # Accès au menu Administration & Paramétrage > Général > paramètre 
    Depuis le menu 'Administration & Paramétrage'
    Click Element  css=.parametre
    # Si on passe la clé om_collectivite alors on est un utilisateur multi
    ${multi} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${values}  om_collectivite
    Run Keyword If  ${multi} == True  Ajouter ou modifier le paramètre en sousformulaire / en multi  ${values}
    Run Keyword If  ${multi} == False  Ajouter ou modifier le paramètre en formulaire / en mono  ${values}


Ajouter ou modifier le paramètre en formulaire / en mono
    [Tags]
    [Arguments]  ${values}
    Use Simple Search  libellé  ${values.libelle}
    ${multi} =  Run Keyword And Return Status  Dictionary Should Contain Key  ${values}  om_collectivite
    ${notexists} =  Run Keyword And Return Status  Element Should Contain  css=.tab-data  Aucun enregistrement.
    Run Keyword If  ${notexists} == False  Click Link  ${values.libelle}
    Run Keyword If  ${notexists} == False  Click On Form Portlet Action  om_parametre  modifier
    Run Keyword If  ${notexists} == True  Click On Add Button
    Saisir les valeurs dans le formulaire 'om_parametre'  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Ajouter ou modifier le paramètre en sousformulaire / en multi
    [Tags]
    [Arguments]  ${values}
    Depuis l'onglet 'Paramètres' dans le contexte de la collectivité  ${values.om_collectivite}
    Input Text  css=#recherchedyn  ${values.libelle}
    # Vérifie que la barre de recherche a bien été remplie et effectuée car lorsque la recherche
    # est effective le texte saisie dans la barre est visible au niveau de l'encart de pagination
    WUX  Element Should Contain  css=.pagination-text  [${values.libelle}]
    # Vérifie si le paramètre existe déjà. Si oui, on le modifie pour lui donner la valeur voulue. Si non on l'ajoute.
    ${notexists} =  Run Keyword And Return Status  Element Should Contain  css=#sousform-om_parametre  Aucun enregistrement.
    Run Keyword If  ${notexists} == False  Acceder au formulaire de modification du parametre dans le contexte de la collectivite  ${values.libelle}
    Run Keyword If  ${notexists} == True  Click On Add Button
    Saisir les valeurs dans le sousformulaire 'om_parametre'  ${values}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.

Acceder au formulaire de modification du parametre dans le contexte de la collectivite
    [Documentation]  Depuis le contexte d'une collectivité, dans le sous-formulaire "Paramètre"
    ...  clique sur le paramètre voulu en l'identifiant avec son libellé. Une fois le paramètre
    ...  ouvert en consultation, clique sur l'action "Modifier".
    [Arguments]  ${parameter_label}

    Click Link  ${parameter_label}
    WUX  Element Should Contain  css=#sousform-om_parametre .form-content #libelle  ${parameter_label} 
    Click On SubForm Portlet Action  om_parametre  modifier

Saisir les valeurs dans le sousformulaire 'om_parametre'
    [Documentation]  Remplit le formulaire
    [Tags]
    [Arguments]  ${values}
    Si "libelle" existe dans "${values}" on execute "Input Text" dans "om_parametre"
    Si "valeur" existe dans "${values}" on execute "Input Text" dans "om_parametre"
    ${exists} =  Run Keyword And Return Status  Element Should Be Visible  css=#sousform-om_parametre select#om_collectivite
    Run Keyword If  ${exists} == True  Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans "om_parametre"

Saisir les valeurs dans le formulaire 'om_parametre'
    [Documentation]  Remplit le formulaire
    [Tags]
    [Arguments]  ${values}
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "valeur" existe dans "${values}" on execute "Input Text" dans le formulaire

Depuis l'onglet 'Paramètres' dans le contexte de la collectivité
    [Documentation]
    [Tags]
    [Arguments]  ${om_collectivite}
    Depuis le contexte de la collectivité  ${om_collectivite}
    On clique sur l'onglet  om_parametre  Paramètre


Rechercher en recherche avancée simple
    [Documentation]
    [Tags]
    [Arguments]  ${terme}
    ${passed} =     Run Keyword And Return Status  Element Should Contain  css=#advanced-form legend  Afficher la recherche simple
    Run Keyword If  ${passed}  Click Element  css=#toggle-advanced-display
    Input Text  css=#adv-search-classic-fields input  ${terme}
    Click Element  adv-search-submit

La recherche avancée simple doit être ouverte
    [Documentation]
    [Tags]
    WUX  Element Should Contain  css=#advanced-form legend  Afficher la recherche avancée

La recherche avancée doit être ouverte
    [Documentation]
    [Tags]
    WUX  Element Should Contain  css=#advanced-form legend  Afficher la recherche simple
