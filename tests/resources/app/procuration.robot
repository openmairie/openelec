*** Settings ***
Documentation  Actions spécifiques aux procurations

*** Keywords ***
Depuis le listing des procurations
    [Documentation]  Accède au listing des procurations
    [Tags]  procuration
    Depuis le listing  procuration


Depuis le formulaire d'ajout d'une procuration
    [Documentation]  Accède au formulaire d'ajout d'une procuration
    [Tags]  procuration
    Go To Submenu In Menu  saisie  procuration


Depuis le contexte de la procuration
    [Documentation]  Accède au formulaire d'ajout d'une procuration
    [Arguments]  ${args_procuration}
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=procuration&action=3&idx=${args_procuration.id}


Ajouter une procuration
    [Documentation]  Ajoute une procuration et retourne son identifiant.
    ...  En cas d'homonyme, le premier de la liste est choisi.
    ...
    ...  Clefs de &args_procuration avec exemples de valeurs :
    ...  nom_mandant=Li
    ...  nom_mandataire=Doe
    ...  debut_validite=02/04/2014
    ...  fin_validite=15/04/2014
    ...  date_accord=15/08/2013
    [Tags]  procuration
    [Arguments]  ${args_procuration}
    [Return]  ${procuration_id}

    Depuis le formulaire d'ajout d'une procuration
    Saisir les informations dans le formulaire *procuration*  ${args_procuration}
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${procuration_id} =  Get Text  css=div.form-procuration #id


Saisir les informations dans le formulaire *procuration*
    [Documentation]  ...
    [Tags]  procuration
    [Arguments]  ${args_procuration}
    #
    Si "statut" existe dans "${args_procuration}" on execute "Select From List By Label" dans le formulaire
    Si "motif_refus" existe dans "${args_procuration}" on execute "Input Text" dans le formulaire
    #
    Si "autorite_nom_prenom" existe dans "${args_procuration}" on execute "Input Text" dans le formulaire
    Si "autorite_type" existe dans "${args_procuration}" on execute "Select From List By Label" dans le formulaire
    Si "autorite_commune" existe dans "${args_procuration}" on sélectionne la valeur sur l'autocomplete "autorite_commune" dans le formulaire

    # Saisie mandant avec autocomplétion
    Input Text  css=#autocomplete-electeur-mandant-search  ${args_procuration.nom_mandant}
    WUX  Click On Link  css=[class*="autocomplete-electeur-mandant-result-"]
    # Saisie madataire avec autocomplétion
    Input Text  css=#autocomplete-electeur-mandataire-search  ${args_procuration.nom_mandataire}
    WUX  Click On Link  css=[class*="autocomplete-electeur-mandataire-result-"]
    #
    Input Text  debut_validite  ${args_procuration.debut_validite}
    Input Text  fin_validite  ${args_procuration.fin_validite}
    Input Text  date_accord   ${args_procuration.date_accord}


Saisir les informations dans le formulaire de recherche avancée des procurations
    [Arguments]  ${values}

    Si "id" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mandant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "bureau" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "mandataire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "debut_validite_min" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "debut_validite_max" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fin_validite_min" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fin_validite_max" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "statut" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "vu" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

Recherche avancée de procuration
    [Documentation]  Accède au formulaire de recherche avancé du menu Consultation > Procuration.
    ...  Saisis les valeurs fournies dans le formulaire puis valide la recherche.
    [Arguments]  ${args_search}

    ${is_advanced_search_displayed} =  Run Keyword And Return Status  Element Should Contain  css=#toggle-advanced-display  Afficher la recherche avancée
    Log  ${is_advanced_search_displayed}
    Run Keyword If  ${is_advanced_search_displayed}!="True"  Click Element  css=#toggle-advanced-display

    Wait Until Page Contains Element  css=.adv-search-fieldset input#id
    Saisir les informations dans le formulaire de recherche avancée des procurations  ${args_search}

    Click Button  adv-search-submit