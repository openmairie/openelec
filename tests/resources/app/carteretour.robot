*** Settings ***
Documentation  Librairie de mots clés autour de la fonctionnalité 'Carte en retour'.

*** Keywords ***
Marquer l'électeur avec une carte en retour
    [Documentation]
    [Arguments]  ${electeur_id}
    [Tags]  carteretour
    Depuis la fiche de l'électeur  ${electeur_id}
    Click On Form Portlet Action  electeur  add-carteretour  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Valid Message Should Be  La carte en retour est enregitrée.
    WUX  Element Should Contain  css=div.message.carteretour  Une carte en retour est enregistrée pour cet électeur.


Démarquer l'électeur avec une carte en retour
    [Documentation]
    [Arguments]  ${electeur_id}
    [Tags]  carteretour
    Depuis la fiche de l'électeur  ${electeur_id}
    Click On Form Portlet Action  electeur  remove-carteretour  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Valid Message Should Be  La carte en retour est supprimée.
    WUX  Element Should Not Be Visible  css=div.message.carteretour


Épurer les cartes en retour
    [Documentation]  Effectue le traitement d'épuration des cartes en retour.
    ...  Si des dates ont été saisie, remplie le formulaire d'épuration avec
    ...  les dates fournies avant de déclencher l'épuration.
    [Tags]  carteretour
    [Arguments]    ${date_debut}=${EMPTY}    ${date_fin}=${EMPTY}

    Depuis l'onglet 'Épuration' du module 'Carte en retour'
    Run Keyword If  "${date_debut}"!="${EMPTY}"  Input Datepicker  date_debut  ${date_debut}
    Run Keyword If  "${date_fin}"!="${EMPTY}"  Input Datepicker  date_fin  ${date_fin}
    WUX  Click Button  Épuration des cartes en retour
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    La page ne doit pas contenir d'erreur


Depuis l'onglet 'Editions' du module 'Carte en retour'
    [Documentation]
    [Tags]  carteretour
    Go To Submenu In Menu  traitement  carteretour
    Page Title Should Be  Traitement > Module Carte En Retour
    # On clique sur l'onglet 'Epuration'
    Click Link  Éditions
    WUX  Element Should Be Visible  css=#module-carte-retour-main-tab
    La page ne doit pas contenir d'erreur


Depuis l'onglet 'Épuration' du module 'Carte en retour'
    [Documentation]
    [Tags]  carteretour
    Go To Submenu In Menu  traitement  carteretour
    Page Title Should Be  Traitement > Module Carte En Retour
    # On clique sur l'onglet 'Epuration'
    Click Link  Épuration
    WUX  Element Should Be Visible  css=#traitement_carteretour_epuration_form
    La page ne doit pas contenir d'erreur


Depuis l'onglet 'Liste des électeurs' du module 'Carte en retour'
    [Documentation]
    [Tags]  carteretour
    Go To Submenu In Menu  traitement  carteretour
    Page Title Should Be  Traitement > Module Carte En Retour
    # On clique sur l'onglet 'Liste des électeurs'
    Click Element  css=a[href="#ui-tabs-2"]
    WUX  Element Should Be Visible  css=#sousform-electeur_carteretour table.tab-tab
    La page ne doit pas contenir d'erreur
