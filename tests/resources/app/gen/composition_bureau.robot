*** Settings ***
Documentation    CRUD de la table composition_bureau
...    @author  generated
...    @package openElec
...    @version 18/02/2022 03:02

*** Keywords ***

Depuis le contexte composition_bureau
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  composition_bureau
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter composition_bureau
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  composition_bureau
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir composition_bureau  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier composition_bureau
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte composition_bureau  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  composition_bureau  modifier
    # On saisit des valeurs
    Saisir composition_bureau  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer composition_bureau
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte composition_bureau  ${composition_bureau}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir composition_bureau
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "composition_scrutin" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "bureau" existe dans "${values}" on execute "Select From List By Label" dans le formulaire