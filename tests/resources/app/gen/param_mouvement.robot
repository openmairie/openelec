*** Settings ***
Documentation    CRUD de la table param_mouvement
...    @author  generated
...    @package openElec
...    @version 19/09/2017 07:09

*** Keywords ***

Depuis le contexte param_mouvement
    [Documentation]  Accède au formulaire
    [Arguments]  ${code}

    # On accède au tableau
    Go To Tab  param_mouvement
    # On recherche l'enregistrement
    Use Simple Search  code  ${code}
    # On clique sur le résultat
    Click On Link  ${code}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter param_mouvement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  param_mouvement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir param_mouvement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${code} =  Get Text  css=div.form-content span#code
    # On le retourne
    [Return]  ${code}

Modifier param_mouvement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${code}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte param_mouvement  ${code}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  param_mouvement  modifier
    # On saisit des valeurs
    Saisir param_mouvement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer param_mouvement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${code}

    # On accède à l'enregistrement
    Depuis le contexte param_mouvement  ${param_mouvement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  code  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir param_mouvement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "typecat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "effet" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cnen" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "codeinscription" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "coderadiation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "edition_carte_electeur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "insee_import_radiation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire