*** Settings ***
Documentation    CRUD de la table poste
...    @author  generated
...    @package openElec
...    @version 26/12/2019 10:12

*** Keywords ***

Depuis le contexte poste
    [Documentation]  Accède au formulaire
    [Arguments]  ${poste}

    # On accède au tableau
    Go To Tab  poste
    # On recherche l'enregistrement
    Use Simple Search  poste  ${poste}
    # On clique sur le résultat
    Click On Link  ${poste}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter poste
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  poste
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir poste  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${poste} =  Get Text  css=div.form-content span#poste
    # On le retourne
    [Return]  ${poste}

Modifier poste
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${poste}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte poste  ${poste}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  poste  modifier
    # On saisit des valeurs
    Saisir poste  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer poste
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${poste}

    # On accède à l'enregistrement
    Depuis le contexte poste  ${poste}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  poste  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir poste
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire