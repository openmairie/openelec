*** Settings ***
Documentation    CRUD de la table canton
...    @author  generated
...    @package openElec
...    @version 07/03/2019 13:03

*** Keywords ***

Depuis le contexte canton
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  canton
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter canton
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  canton
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir canton  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier canton
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte canton  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  canton  modifier
    # On saisit des valeurs
    Saisir canton  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer canton
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte canton  ${canton}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir canton
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire