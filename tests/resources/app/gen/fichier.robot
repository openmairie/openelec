*** Settings ***
Documentation    CRUD de la table fichier
...    @author  generated
...    @package openElec
...    @version 19/09/2017 07:09

*** Keywords ***

Depuis le contexte fichier
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  fichier
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter fichier
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  fichier
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir fichier  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier fichier
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte fichier  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  fichier  modifier
    # On saisit des valeurs
    Saisir fichier  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer fichier
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte fichier  ${fichier}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir fichier
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "creationdate" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "creationtime" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "uid" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "filename" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "size" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mimetype" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "type" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire