*** Settings ***
Documentation    CRUD de la table liste
...    @author  generated
...    @package openElec
...    @version 19/09/2017 07:09

*** Keywords ***

Depuis le contexte liste
    [Documentation]  Accède au formulaire
    [Arguments]  ${liste}

    # On accède au tableau
    Go To Tab  liste
    # On recherche l'enregistrement
    Use Simple Search  liste  ${liste}
    # On clique sur le résultat
    Click On Link  ${liste}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter liste
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  liste
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir liste  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${liste} =  Get Text  css=div.form-content span#liste
    # On le retourne
    [Return]  ${liste}

Modifier liste
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${liste}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte liste  ${liste}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  liste  modifier
    # On saisit des valeurs
    Saisir liste  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer liste
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${liste}

    # On accède à l'enregistrement
    Depuis le contexte liste  ${liste}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  liste  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir liste
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle_liste" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "liste_insee" existe dans "${values}" on execute "Input Text" dans le formulaire