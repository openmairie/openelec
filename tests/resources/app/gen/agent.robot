*** Settings ***
Documentation    CRUD de la table agent
...    @author  generated
...    @package openElec
...    @version 26/12/2019 10:12

*** Keywords ***

Depuis le contexte agent
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  agent
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter agent
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  agent
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir agent  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier agent
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte agent  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  agent  modifier
    # On saisit des valeurs
    Saisir agent  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer agent
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte agent  ${agent}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir agent
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "telephone_pro" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "grade" existe dans "${values}" on execute "Select From List By Label" dans le formulaire