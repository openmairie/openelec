*** Settings ***
Documentation    CRUD de la table decoupage
...    @author  generated
...    @package openElec
...    @version 29/05/2018 13:05

*** Keywords ***

Depuis le contexte decoupage
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  decoupage
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter decoupage
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  decoupage
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir decoupage  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier decoupage
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte decoupage  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  decoupage  modifier
    # On saisit des valeurs
    Saisir decoupage  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer decoupage
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte decoupage  ${decoupage}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir decoupage
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "bureau" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "code_voie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "premier_impair" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dernier_impair" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "premier_pair" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dernier_pair" existe dans "${values}" on execute "Input Text" dans le formulaire