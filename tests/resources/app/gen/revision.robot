*** Settings ***
Documentation    CRUD de la table revision
...    @author  generated
...    @package openElec
...    @version 19/09/2017 07:09

*** Keywords ***

Depuis le contexte revision
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  revision
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter revision
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  revision
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir revision  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier revision
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte revision  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  revision  modifier
    # On saisit des valeurs
    Saisir revision  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer revision
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte revision  ${revision}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir revision
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_effet" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_tr1" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_tr2" existe dans "${values}" on execute "Input Datepicker" dans le formulaire