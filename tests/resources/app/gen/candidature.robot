*** Settings ***
Documentation    CRUD de la table candidature
...    @author  generated
...    @package openElec
...    @version 18/02/2022 01:02

*** Keywords ***

Depuis le contexte candidature
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  candidature
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter candidature
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  candidature
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir candidature  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier candidature
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte candidature  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  candidature  modifier
    # On saisit des valeurs
    Saisir candidature  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer candidature
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte candidature  ${candidature}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir candidature
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "agent" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "composition_scrutin" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "periode" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "poste" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "bureau" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "note" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "decision" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "recuperation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "debut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fin" existe dans "${values}" on execute "Input Text" dans le formulaire