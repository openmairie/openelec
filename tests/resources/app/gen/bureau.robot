*** Settings ***
Documentation    CRUD de la table bureau
...    @author  generated
...    @package openElec
...    @version 05/06/2018 12:06

*** Keywords ***

Depuis le contexte Bureau
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  bureau
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter Bureau
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  bureau
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir Bureau  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier Bureau
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte Bureau  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  bureau  modifier
    # On saisit des valeurs
    Saisir Bureau  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer Bureau
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte Bureau  ${bureau}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir Bureau
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse3" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "canton" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "circonscription" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire