*** Settings ***
Documentation    CRUD de la table composition_scrutin
...    @author  generated
...    @package openElec
...    @version 22/11/2022 10:11

*** Keywords ***

Depuis le contexte composition du scrutin
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  composition_scrutin
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter composition du scrutin
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  composition_scrutin
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir composition du scrutin  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier composition du scrutin
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte composition du scrutin  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  composition_scrutin  modifier
    # On saisit des valeurs
    Saisir composition du scrutin  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer composition du scrutin
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte composition du scrutin  ${composition_scrutin}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir composition du scrutin
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "scrutin" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tour" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_tour" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "solde" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "convocation_agent" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "convocation_president" existe dans "${values}" on execute "Input Text" dans le formulaire