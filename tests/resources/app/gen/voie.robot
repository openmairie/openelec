*** Settings ***
Documentation    CRUD de la table voie
...    @author  generated
...    @package openElec
...    @version 15/05/2018 11:05

*** Keywords ***

Depuis le contexte voie
    [Documentation]  Accède au formulaire
    [Arguments]  ${code}

    # On accède au tableau
    Go To Tab  voie
    # On recherche l'enregistrement
    Use Simple Search  code  ${code}
    # On clique sur le résultat
    Click On Link  ${code}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter voie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  voie
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir voie  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${code} =  Get Text  css=div.form-content span#code
    # On le retourne
    [Return]  ${code}

Modifier voie
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${code}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte voie  ${code}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  voie  modifier
    # On saisit des valeurs
    Saisir voie  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer voie
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${code}

    # On accède à l'enregistrement
    Depuis le contexte voie  ${voie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  code  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir voie
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "abrege" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire