*** Settings ***
Documentation    CRUD de la table reu_scrutin
...    @author  generated
...    @package openElec
...    @version 25/09/2023 18:09

*** Keywords ***

Depuis le contexte reu_scrutin
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  reu_scrutin
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter reu_scrutin
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reu_scrutin
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir reu_scrutin  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier reu_scrutin
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte reu_scrutin  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reu_scrutin  modifier
    # On saisit des valeurs
    Saisir reu_scrutin  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer reu_scrutin
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte reu_scrutin  ${reu_scrutin}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir reu_scrutin
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "type" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "partiel" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "zone" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_tour1" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_tour2" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_l30" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "j5_livrable_demande_id" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "j5_livrable_demande_date" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "j5_livrable" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "j5_livrable_date" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emarge_livrable_demande_id" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emarge_livrable_demande_date" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emarge_livrable" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "emarge_livrable_date" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "referentiel_id" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "canton" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "circonscription_legislative" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "circonscription_metropolitaine" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "arrets_liste" existe dans "${values}" on execute "Select From List By Label" dans le formulaire