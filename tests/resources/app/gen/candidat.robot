*** Settings ***
Documentation    CRUD de la table candidat
...    @author  generated
...    @package openElec
...    @version 15/02/2022 09:02

*** Keywords ***

Depuis le contexte candidat
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  candidat
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter candidat
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  candidat
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir candidat  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier candidat
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte candidat  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  candidat  modifier
    # On saisit des valeurs
    Saisir candidat  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer candidat
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte candidat  ${candidat}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir candidat
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "composition_scrutin" existe dans "${values}" on execute "Select From List By Label" dans le formulaire