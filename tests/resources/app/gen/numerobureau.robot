*** Settings ***
Documentation    CRUD de la table numerobureau
...    @author  generated
...    @package openElec
...    @version 29/05/2018 13:05

*** Keywords ***

Depuis le contexte numerobureau
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  numerobureau
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter numerobureau
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  numerobureau
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir numerobureau  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier numerobureau
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte numerobureau  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  numerobureau  modifier
    # On saisit des valeurs
    Saisir numerobureau  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer numerobureau
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte numerobureau  ${numerobureau}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir numerobureau
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "liste" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "bureau" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dernier_numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dernier_numero_provisoire" existe dans "${values}" on execute "Input Text" dans le formulaire