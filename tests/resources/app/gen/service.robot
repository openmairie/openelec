*** Settings ***
Documentation    CRUD de la table service
...    @author  generated
...    @package openElec
...    @version 26/12/2019 10:12

*** Keywords ***

Depuis le contexte service
    [Documentation]  Accède au formulaire
    [Arguments]  ${service}

    # On accède au tableau
    Go To Tab  service
    # On recherche l'enregistrement
    Use Simple Search  service  ${service}
    # On clique sur le résultat
    Click On Link  ${service}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter service
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  service
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir service  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${service} =  Get Text  css=div.form-content span#service
    # On le retourne
    [Return]  ${service}

Modifier service
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${service}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte service  ${service}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  service  modifier
    # On saisit des valeurs
    Saisir service  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer service
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${service}

    # On accède à l'enregistrement
    Depuis le contexte service  ${service}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  service  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir service
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire