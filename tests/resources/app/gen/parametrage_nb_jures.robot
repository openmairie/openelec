*** Settings ***
Documentation    CRUD de la table parametrage_nb_jures
...    @author  generated
...    @package openElec
...    @version 15/05/2018 11:05

*** Keywords ***

Depuis le contexte parametrage_nb_jures
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  parametrage_nb_jures
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter parametrage_nb_jures
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  parametrage_nb_jures
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir parametrage_nb_jures  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier parametrage_nb_jures
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte parametrage_nb_jures  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  parametrage_nb_jures  modifier
    # On saisit des valeurs
    Saisir parametrage_nb_jures  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer parametrage_nb_jures
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte parametrage_nb_jures  ${parametrage_nb_jures}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir parametrage_nb_jures
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "canton" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nb_jures" existe dans "${values}" on execute "Input Text" dans le formulaire