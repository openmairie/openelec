*** Settings ***
Documentation    CRUD de la table periode
...    @author  generated
...    @package openElec
...    @version 26/12/2019 10:12

*** Keywords ***

Depuis le contexte periode
    [Documentation]  Accède au formulaire
    [Arguments]  ${periode}

    # On accède au tableau
    Go To Tab  periode
    # On recherche l'enregistrement
    Use Simple Search  periode  ${periode}
    # On clique sur le résultat
    Click On Link  ${periode}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter periode
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  periode
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir periode  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${periode} =  Get Text  css=div.form-content span#periode
    # On le retourne
    [Return]  ${periode}

Modifier periode
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${periode}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte periode  ${periode}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  periode  modifier
    # On saisit des valeurs
    Saisir periode  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer periode
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${periode}

    # On accède à l'enregistrement
    Depuis le contexte periode  ${periode}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  periode  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir periode
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "debut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fin" existe dans "${values}" on execute "Input Text" dans le formulaire