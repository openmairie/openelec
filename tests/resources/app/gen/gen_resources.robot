*** Settings ***
Documentation    Ressources de mots-clefs générés
...    @author  generated
...    @package openElec
...    @version 14/09/2023 12:09

Resource          affectation.robot
Resource          agent.robot
Resource          archive.robot
Resource          arrets_liste.robot
Resource          bureau.robot
Resource          candidat.robot
Resource          candidature.robot
Resource          canton.robot
Resource          circonscription.robot
Resource          commune.robot
Resource          composition_bureau.robot
Resource          composition_scrutin.robot
Resource          decoupage.robot
Resource          departement.robot
Resource          electeur.robot
Resource          elu.robot
Resource          fichier.robot
Resource          grade.robot
Resource          liste.robot
Resource          membre_commission.robot
Resource          mouvement.robot
Resource          nationalite.robot
Resource          numerobureau.robot
Resource          param_mouvement.robot
Resource          parametrage_nb_jures.robot
Resource          periode.robot
Resource          piece.robot
Resource          piece_type.robot
Resource          poste.robot
Resource          procuration.robot
Resource          reu_notification.robot
Resource          reu_scrutin.robot
Resource          revision.robot
Resource          service.robot
Resource          trace.robot
Resource          voie.robot