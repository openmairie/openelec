*** Settings ***
Documentation    CRUD de la table grade
...    @author  generated
...    @package openElec
...    @version 26/12/2019 10:12

*** Keywords ***

Depuis le contexte grade
    [Documentation]  Accède au formulaire
    [Arguments]  ${grade}

    # On accède au tableau
    Go To Tab  grade
    # On recherche l'enregistrement
    Use Simple Search  grade  ${grade}
    # On clique sur le résultat
    Click On Link  ${grade}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter grade
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  grade
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir grade  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${grade} =  Get Text  css=div.form-content span#grade
    # On le retourne
    [Return]  ${grade}

Modifier grade
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${grade}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte grade  ${grade}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  grade  modifier
    # On saisit des valeurs
    Saisir grade  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer grade
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${grade}

    # On accède à l'enregistrement
    Depuis le contexte grade  ${grade}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  grade  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir grade
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire