*** Settings ***
Documentation    CRUD de la table reu_notification
...    @author  generated
...    @package openElec
...    @version 30/01/2019 13:01

*** Keywords ***

Depuis le contexte reu_notification
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  reu_notification
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter reu_notification
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reu_notification
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir reu_notification  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier reu_notification
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte reu_notification  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reu_notification  modifier
    # On saisit des valeurs
    Saisir reu_notification  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer reu_notification
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte reu_notification  ${reu_notification}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir reu_notification
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "date_de_creation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "id_demande" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "id_electeur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "lien_uri" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "resultat_de_notification" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "type_de_notification" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_de_collecte" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Input Text" dans le formulaire