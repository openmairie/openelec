*** Settings ***
Documentation    CRUD de la table piece
...    @author  generated
...    @package openElec
...    @version 29/01/2019 09:01

*** Keywords ***

Depuis le contexte pièce
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  piece
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter pièce
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  piece
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir pièce  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier pièce
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte pièce  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  piece  modifier
    # On saisit des valeurs
    Saisir pièce  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer pièce
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte pièce  ${piece}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir pièce
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "mouvement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "piece_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "fichier" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire