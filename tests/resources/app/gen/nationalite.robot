*** Settings ***
Documentation    CRUD de la table nationalite
...    @author  generated
...    @package openElec
...    @version 09/01/2019 11:01

*** Keywords ***

Depuis le contexte nationalite
    [Documentation]  Accède au formulaire
    [Arguments]  ${code}

    # On accède au tableau
    Go To Tab  nationalite
    # On recherche l'enregistrement
    Use Simple Search  code  ${code}
    # On clique sur le résultat
    Click On Link  ${code}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter nationalite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  nationalite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir nationalite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${code} =  Get Text  css=div.form-content span#code
    # On le retourne
    [Return]  ${code}

Modifier nationalite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${code}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte nationalite  ${code}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  nationalite  modifier
    # On saisit des valeurs
    Saisir nationalite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer nationalite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${code}

    # On accède à l'enregistrement
    Depuis le contexte nationalite  ${nationalite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  code  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir nationalite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle_nationalite" existe dans "${values}" on execute "Input Text" dans le formulaire