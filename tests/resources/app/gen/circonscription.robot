*** Settings ***
Documentation    CRUD de la table circonscription
...    @author  generated
...    @package openElec
...    @version 05/06/2018 11:06

*** Keywords ***

Depuis le contexte circonscription
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  circonscription
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter circonscription
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  circonscription
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir circonscription  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier circonscription
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte circonscription  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  circonscription  modifier
    # On saisit des valeurs
    Saisir circonscription  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer circonscription
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte circonscription  ${circonscription}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir circonscription
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire