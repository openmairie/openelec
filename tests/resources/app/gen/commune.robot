*** Settings ***
Documentation    CRUD de la table commune
...    @author  generated
...    @package openElec
...    @version 19/09/2017 07:09

*** Keywords ***

Depuis le contexte commune
    [Documentation]  Accède au formulaire
    [Arguments]  ${code}

    # On accède au tableau
    Go To Tab  commune
    # On recherche l'enregistrement
    Use Simple Search  code  ${code}
    # On clique sur le résultat
    Click On Link  ${code}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter commune
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  commune
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir commune  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${code} =  Get Text  css=div.form-content span#code
    # On le retourne
    [Return]  ${code}

Modifier commune
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${code}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte commune  ${code}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  commune  modifier
    # On saisit des valeurs
    Saisir commune  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer commune
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${code}

    # On accède à l'enregistrement
    Depuis le contexte commune  ${commune}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  code  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir commune
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code_commune" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_commune" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code_departement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire