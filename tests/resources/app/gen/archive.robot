*** Settings ***
Documentation    CRUD de la table archive
...    @author  generated
...    @package openElec
...    @version 16/04/2019 12:04

*** Keywords ***

Depuis le contexte archive
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  archive
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter archive
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  archive
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir archive  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier archive
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte archive  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  archive  modifier
    # On saisit des valeurs
    Saisir archive  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer archive
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte archive  ${archive}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir archive
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "types" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "electeur_id" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "liste" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "bureau_de_vote_code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "bureauforce" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero_bureau" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_modif" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "utilisateur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "civilite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sexe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom_usage" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_naissance" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "code_departement_naissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_departement_naissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code_lieu_de_naissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_lieu_de_naissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code_nationalite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero_habitation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement_numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "provenance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_provenance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ancien_bureau_de_vote_code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "observation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement_resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tableau" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_tableau" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "envoi_cnen" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_cnen" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "mouvement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "typecat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_mouvement" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "etat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "telephone" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courriel" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "bureau_de_vote_libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ancien_bureau_de_vote_libelle" existe dans "${values}" on execute "Input Text" dans le formulaire