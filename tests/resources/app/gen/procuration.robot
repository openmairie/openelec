*** Settings ***
Documentation    CRUD de la table procuration
...    @author  generated
...    @package openElec
...    @version 17/03/2022 09:03

*** Keywords ***

Depuis le contexte procuration
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  procuration
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter procuration
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  procuration
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir procuration  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier procuration
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte procuration  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  procuration  modifier
    # On saisit des valeurs
    Saisir procuration  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer procuration
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte procuration  ${procuration}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir procuration
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "mandant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "mandataire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "debut_validite" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "fin_validite" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_accord" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "motif_refus" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "id_externe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "statut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mandant_ine" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mandataire_ine" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mandant_infos" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mandataire_infos" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mandant_resume" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mandataire_resume" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "vu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "historique" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "autorite_nom_prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "autorite_commune" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "autorite_consulat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "autorite_type" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "notes" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "annulation_autorite_nom_prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "annulation_autorite_commune" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "annulation_autorite_consulat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "annulation_autorite_type" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "annulation_date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire