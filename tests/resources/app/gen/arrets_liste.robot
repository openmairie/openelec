*** Settings ***
Documentation    CRUD de la table arrets_liste
...    @author  generated
...    @package openElec
...    @version 20/11/2023 10:11

*** Keywords ***

Depuis le contexte Arrêt des listes
    [Documentation]  Accède au formulaire
    [Arguments]  ${arrets_liste}

    # On accède au tableau
    Go To Tab  arrets_liste
    # On recherche l'enregistrement
    Use Simple Search  Arrêt des listes  ${arrets_liste}
    # On clique sur le résultat
    Click On Link  ${arrets_liste}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter Arrêt des listes
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  arrets_liste
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir Arrêt des listes  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${arrets_liste} =  Get Text  css=div.form-content span#arrets_liste
    # On le retourne
    [Return]  ${arrets_liste}

Modifier Arrêt des listes
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${arrets_liste}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte Arrêt des listes  ${arrets_liste}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  arrets_liste  modifier
    # On saisit des valeurs
    Saisir Arrêt des listes  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer Arrêt des listes
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${arrets_liste}

    # On accède à l'enregistrement
    Depuis le contexte Arrêt des listes  ${arrets_liste}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  arrets_liste  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir Arrêt des listes
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "livrable_demande_id" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "livrable_demande_date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "livrable" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "livrable_date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire