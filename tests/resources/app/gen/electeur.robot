*** Settings ***
Documentation    CRUD de la table electeur
...    @author  generated
...    @package openElec
...    @version 03/05/2019 22:05

*** Keywords ***

Depuis le contexte électeur
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  electeur
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter électeur
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  electeur
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir électeur  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier électeur
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte électeur  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  electeur  modifier
    # On saisit des valeurs
    Saisir électeur  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer électeur
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte électeur  ${electeur}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir électeur
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "liste" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "bureau" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "bureauforce" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero_bureau" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_modif" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "utilisateur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "civilite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sexe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom_usage" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_naissance" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "code_departement_naissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_departement_naissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code_lieu_de_naissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_lieu_de_naissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code_nationalite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "code_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numero_habitation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement_numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "provenance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_provenance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "complement_resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp_resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville_resident" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tableau" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_tableau" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "mouvement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_mouvement" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "typecat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "carte" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "procuration" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "jury" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_inscription" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "code_inscription" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "jury_effectif" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_jeffectif" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "profession" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "motif_dispense_jury" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courriel" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ine" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "reu_sync_info" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "a_transmettre" existe dans "${values}" on execute "Set Checkbox" dans le formulaire