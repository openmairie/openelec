*** Settings ***
Documentation    CRUD de la table elu
...    @author  generated
...    @package openElec
...    @version 13/05/2022 20:05

*** Keywords ***

Depuis le contexte élu
    [Documentation]  Accède au formulaire
    [Arguments]  ${id}

    # On accède au tableau
    Go To Tab  elu
    # On recherche l'enregistrement
    Use Simple Search  identifiant  ${id}
    # On clique sur le résultat
    Click On Link  ${id}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter élu
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  elu
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir élu  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${id} =  Get Text  css=div.form-content span#id
    # On le retourne
    [Return]  ${id}

Modifier élu
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${id}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte élu  ${id}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  elu  modifier
    # On saisit des valeurs
    Saisir élu  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer élu
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${id}

    # On accède à l'enregistrement
    Depuis le contexte élu  ${elu}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  id  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir élu
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nomjf" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_naissance" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "lieu_naissance" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire