*** Settings ***
Documentation    CRUD de la table departement
...    @author  generated
...    @package openElec
...    @version 19/09/2017 07:09

*** Keywords ***

Depuis le contexte departement
    [Documentation]  Accède au formulaire
    [Arguments]  ${code}

    # On accède au tableau
    Go To Tab  departement
    # On recherche l'enregistrement
    Use Simple Search  code  ${code}
    # On clique sur le résultat
    Click On Link  ${code}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter departement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  departement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir departement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${code} =  Get Text  css=div.form-content span#code
    # On le retourne
    [Return]  ${code}

Modifier departement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${code}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte departement  ${code}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  departement  modifier
    # On saisit des valeurs
    Saisir departement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer departement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${code}

    # On accède à l'enregistrement
    Depuis le contexte departement  ${departement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  code  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir departement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle_departement" existe dans "${values}" on execute "Input Text" dans le formulaire