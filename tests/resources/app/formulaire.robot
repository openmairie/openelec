*** Settings ***
Documentation  Actions dans un formulaire

*** Keywords ***
Attendre que le champ texte "${locator}" soit vide
    WUX  Run Keyword And Expect Error
    ...  Value of text field '${locator}' should have been '' but was*
    ...  Textfield Value Should Be  ${locator}  ${EMPTY}
