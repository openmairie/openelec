*** Settings ***
Documentation  Actions spécifiques au module REU

*** Keywords ***

Accéder au module REU
    [Tags]
    [Documentation]  Accéder au module REU
    Go To  ${PROJECT_URL}/app/index.php?module=module_reu
    WUX  Page Title Should Be  Traitement > Module REU

Saisir l'UGLE
    [Tags]
    [Documentation]  Saisir le UGLE dans le REU
    [Arguments]  ${ugle}
    Click On Link  action-reu-overlay-change-ugle
    Input Text  ugle  ${ugle}
    Click Element  changeugle.action.valid
    Click Element  css=.ui-dialog .ui-dialog-titlebar-close

Saisir le compte logiciel
    [Tags]
    [Documentation]  Saisir le compte logiciel dans le REU
    [Arguments]  ${username}  ${password}=${EMPTY}

    # If the password is not set use the username as a password instead
    ${password} =  Set Variable If  '${password}'=='${EMPTY}'
    ...  ${username}
    ...  ${password}
    Click On Link  action-reu-overlay-change-compte-logiciel
    WUX  Input Text  compte_logiciel_username  ${username}
    WUX  Input Text  compte_logiciel_password  ${password}
    Click Element  changecompte_logiciel.action.valid
    Click Element  css=.ui-dialog .ui-dialog-titlebar-close


Synchroniser les bureaux
    [Tags]
    [Documentation]  Synchronise les bureaux
    [Arguments]  ${code_commune}="null"

    # Liste des code de caton et de circonscription
    ${canton_circon} =  Create Dictionary
    ...  com84017_canton=84-12
    ...  com84017_circon=84-05
    ...  com59484_canton=59-29
    ...  com59484_circon=59-21
    ...  com57289_canton=57-22
    ...  com57289_circon=57-05
    ...  com95392_canton=95-17
    ...  com95392_circon=95-02
    ...  com95424_canton=95-13
    ...  com95424_circon=95-03
    ...  com95426_canton=95-15
    ...  com95426_circon=95-04
    ...  com95427_canton=95-06
    ...  com95427_circon=95-06
    ...  com11129_canton=11-14
    ...  com11129_circon=11-03
    ...  com31193_canton=31-06
    ...  com31193_circon=31-08
    ...  com34142_canton=34-11
    ...  com34142_circon=34-04
    ...  com38332_canton=38-25
    ...  com38332_circon=38-09
    ...  com66133_canton=66-05
    ...  com66133_circon=66-04
    ...  com73296_canton=73-05
    ...  com73296_circon=73-02
    ...  com78189_canton=78-19
    ...  com78189_circon=78-12
    ...  com83007_canton=83-04
    ...  com83007_circon=83-08
    ...  com93005_canton=93-02
    ...  com93005_circon=93-10

    Accéder au module REU
    Click Link  css=#module_reu-onglet-synchronisation_referentiel
    WUX  Click Element  name:traitement.reu_sync_bureau.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    Accéder au module REU
    ${has_canton_circon} =  Run Keyword And Return Status  WUX  Element Should Contain  css=#bureaux-status  OK
    # Si la synchronisation des bureaux est bonne on quitte le keyword
    Return from keyword If  ${has_canton_circon}
    # Si le code commune est vide on quitte le keyword (test non beta)
    Return from keyword If  ${code_commune} == "null"
    # Si les bureaux n'ont pas de canton et de circonscription alors on les modifie
    ${canton} =  Get From Dictionary  ${canton_circon}  com${code_commune}_canton
    ${circon} =  Get From Dictionary  ${canton_circon}  com${code_commune}_circon
    # On modifie le libellé du canton pour pouvoir le récupérer
    ${values_modif_canton}=  Create Dictionary
    ...  code=${canton}
    ...  libelle=CANTON
    Modifier le canton  ${values_modif_canton}
    REU-TEST Connexion logiciel standard  ${code_commune}
    # On modifie les bureaux
    ${values_bureau_1} =  Create Dictionary
    ...  bureau=1
    ...  canton=${canton} CANTON
    ...  circonscription=${circon}
    Modifier le bureau  ${values_bureau_1}
    #
    ${values bureau_2} =  Create Dictionary
    ...  bureau=2
    ...  canton=${canton} CANTON
    ...  circonscription=${circon}
    Modifier le bureau  ${values_bureau_2}
    #
    ${values bureau_3} =  Create Dictionary
    ...  bureau=3
    ...  canton=${canton} CANTON
    ...  circonscription=${circon}
    Modifier le bureau  ${values_bureau_3}
    # On resynchronise les bureaux et on vérifie que tout est bon
    Accéder au module REU
    Click Link  css=#module_reu-onglet-synchronisation_referentiel
    WUX  Click Element  name:traitement.reu_sync_bureau.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    Accéder au module REU
    WUX  Element Should Contain  css=#bureaux-status  OK


Initialiser un statut valide
    [Tags]
    [Arguments]  ${with_synch}
    [Documentation]  Applique un statut valide au REU
    Accéder au module reu
    ${config} =  WUX  Get Text  css=#config_system
    ${ugle} =  WUX  Get Text  css=#param_ugle
    ${compte logiciel} =  WUX  Get Text  css=#param_compte_logiciel
    Return from keyword If  """${config}""" == """OK""" and """${ugle}""" == """OK""" and """${compte_logiciel}""" == """OK"""
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_reu_all_false.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_reu_all_true.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_reu_config.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu_all_true.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu.txt

    ${ugle} =  Set Variable  ugle
    ${compte_logiciel} =  Set Variable  compte logiciel
    Saisir l'UGLE  ${ugle}
    Saisir le compte logiciel  ${compte_logiciel}
    Run Keyword If  """${with_synch}""" == """Oui"""  Synchroniser les bureaux

Synchroniser les listes électorales
    [Tags]
    [Documentation]  Fait la synchronisation des listes
    Accéder au module reu
    Click Link  css=#module_reu-onglet-synchronisation_liste_electorale
    ${passed} =  Run Keyword And Return Status  WUX  Element Should Contain  css=#ui-tabs-2 div.message  Initialisation terminée avec une extraction au
    Return from keyword If  ${passed}
    WUX  Click Element  name:traitement.reu_sync_l_e_init.form.valid
    Handle Alert
    WUX  Click Link  ETAPE SUIVANTE
    WUX  Click ELement  name:traitement.reu_sync_l_e_attach.form.valid
    Handle Alert
    WUX  Click Link  ETAPE SUIVANTE
    WUX  Click Element  name:traitement.reu_sync_l_e_valid.form.valid
    Handle Alert
    WUX  Click Link  RAFRAÎCHIR

Synchroniser les lieux de naissances
    [Documentation]  Fait la synchronisation des lieux de naissance
    Depuis La Page D'accueil  admin  admin
    Accéder au module reu
    Click Link  css=#module_reu-onglet-synchronisation_referentiel
    WUX  Click Element  name:traitement.reu_sync_referentiel_lieu_naissance.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    La page ne doit pas contenir d'erreur

Synchroniser les tables de référence
    [Documentation]  Fait la synchronisation des tables de références
    Depuis La Page D'accueil  admin  admin
    Accéder au module reu
    Click Link  css=#module_reu-onglet-synchronisation_referentiel
    WUX  Click Element  name:traitement.reu_sync_referentiel_misc.form.valid
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    la page ne doit pas contenir d'erreur

Désactiver le reu et mettre les fichiers par défaut
    [Tags]
    [Documentation]  Supprimer le fichier reu.inc.php et renomme les fichier text de la ressource de test
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_reu_all_false.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_reu_all_true.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_reu_config.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu_all_false.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_reu.txt
    Remove File  ..${/}dyn${/}reu.inc.php

Synchroniser les notifications
    Depuis La Page D'accueil  admin  admin
    Accéder au module reu
    WUX  Click Link  css=#module_reu-onglet-notification
    WUX  Click Element  css=#action-soustab-reu_notification-corner-sync_last_notifications
    WUX  Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

Traiter la notification
    [Arguments]  ${id_demande}
    Depuis La Page D'accueil  admin  admin
    Accéder au module reu
    WUX  Click Link  css=#module_reu-onglet-notification
    :FOR    ${i}    IN RANGE    5
    \    Log  ${i}
    \    ${passed} =  Run Keyword And Return Status  WUX  Click On Link  ${id_demande}
    \    Exit For Loop If  '${passed}' == 'True'
    \    ${passed} =  Run Keyword And Return Status  WUX  Click Element  css=a.pagination-next
    \    Exit For Loop If  '${passed}' == 'False'
    Click On SubForm Portlet Action  reu_notification  traiter  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only

Préparer les notifications pour le test API
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_api.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_api.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

Remettre le fichier de notifications par défaut
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_api.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

Préparer les notifications pour le test sans bureau
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_sans_bureau.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_sans_bureau.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

Préparer les notifications pour le test des notifications INSEE
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_insee.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_insee.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

Préparer les notifications pour le test des notifications MODEC
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_modec.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications_modec.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

Préparer les notifications pour le test du modificatif
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_notification_test_modificatif.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_notification_test_modificatif.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

Préparer les notifications d'inscription pour le test des notifications MODEC
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notifications_defaut.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_notification_inscription_modec.txt  ${PATH_REU_RESOURCE_TEST_WF}
    Move File  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notification_inscription_modec.txt  ${PATH_REU_RESOURCE_TEST_WF}ressource_test_notifications.txt

Retirer un bureau de vote REU
    Run  sed -i '27 s/},$/}/' binary_files/ressource_test_reu/ressource_test_bureaux_de_vote.txt ${PATH_REU_RESOURCE_TEST_WF}ressource_test_bureaux_de_vote.txt
    Run  sed -i '28,40d' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_bureaux_de_vote.txt

Réinitialiser les bureaux de vote REU
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}ressource_test_bureaux_de_vote.txt  ${PATH_REU_RESOURCE_TEST_WF}

Préparer l'erreur des lieux de naissance REU
    Copy File  ${PATH_REU_RESOURCE_TEST_REFERENCE}reponse_lieux_naissance_faux.txt  ${PATH_REU_RESOURCE_TEST_WF}

Réinitialiser les lieux de naissance REU
    Remove File  ${PATH_REU_RESOURCE_TEST_WF}reponse_lieux_naissance_faux.txt
