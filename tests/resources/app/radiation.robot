*** Settings ***
Documentation  Actions spécifiques aux radiations

*** Keywords ***
Ajouter le mouvement de radiation
    [Documentation]  ...
    [Arguments]  ${mouvement}
    [Return]  ${mouvement_id}
    #
    Go To Submenu In Menu  saisie  radiation
    Page Title Should Be  Saisie > Radiation
    #
    Input Text  nom  ${mouvement.electeur_nom}
    Click Button  Rechercher l'électeur à radier
    Click Element  css=[title="Creer un mouvement de radiation"]
    Sleep  2
    #
    Si "date_demande" existe dans "${mouvement}" on execute "Input Datepicker" dans le formulaire
    Si "types" existe dans "${mouvement}" on execute "Select From List By Label" dans le formulaire
    #
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${mouvement_id} =  Get Element Attribute  css=.form-content #id  value


Viser le mouvement de radiation REU
    [Arguments]  ${visa}
    WUX  Click On Form Portlet Action  radiation  reu-viser
    WUX  Select From List By Label  css=#visa  ${visa.select}
    Input Datepicker  date_visa  ${visa.date}
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    ${result} =  Evaluate  """${visa.select}""" == """accepté"""
    Run Keyword If  ${result} == True  WUX  Form Value Should Be  css=#visa  accepte
    ${result} =  Evaluate  """${visa.select}""" == """refusé"""
    Run Keyword If  ${result} == True  WUX  Form Value Should Be  css=#visa  refuse
    WUX  Form Value Should Be  css=#statut  vise_maire
    WUX  Form Value Should Be  css=#date_visa  ${visa.date}
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur


Abandonner le mouvement de radiation REU
    [Documentation]  Clique sur l'action abandonner du mouvement
    WUX  Click On Form Portlet Action  radiation  reu-abandonner  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Form Value Should Be  css=#statut  abandonne
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur


Depuis le listing des radiations
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  consultation  radiations
    Page Title Should Be  Consultation > Radiation
    First Tab Title Should Be  Radiation
    Element Should Be Visible  css=#tab-radiation

Valider le mouvement de radiation INSEE
    [Arguments]  ${nom_electeur}
    Depuis le listing des radiations
    Rechercher en recherche avancée simple  ${nom_electeur}
    Click On Link  ${nom_electeur}
    Click On Form Portlet Action  radiation  valider  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only

Modifier l'identifiant externe de la radiation INSEE
    [Arguments]  ${id_externe}
    Run  sed -i 's/"referenceExterne": "[0-9]*"/"referenceExterne": "${id_externe}"/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt

Modifier l'identifiant électeur de la radiation INSEE
    [Arguments]  ${id_externe}
    Run  sed -i 's/"id": 789456/"id": ${id_externe}/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt

Remettre l'identifiant électeur de la radiation INSEE par défaut
    Run  sed -i 's/"id": 159753/"id": 789456/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_radiation_insee.txt

