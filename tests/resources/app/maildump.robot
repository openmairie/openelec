*** Settings ***
Documentation  Actions dans maildump

*** Keywords ***

Démarrer maildump
    [Documentation]  Permet de démarrer maildump
    Run  om-tests -c startsmtp

Arrêter maildump
    [Documentation]  Permet d'arrêter maildump
    Run  om-tests -c stopsmtp

Accéder à maildump
    [Documentation]  Permet d'accéder à maildump
    Go To  http://127.0.0.1:1080/

Sélectionner le mail à afficher
    [Arguments]  ${mail_id}
    [Documentation]  Permet de sélectionner le mail à afficher
    Click Element  //tr[@data-message-id]/td[text()="${mail_id}"]


Vérifier le contenu du mail
    [Documentation]  Permet de vérifier le contenu du message reçu
    [Arguments]  ${mail_id}  ${message}
    Sélectionner le mail à afficher  ${mail_id}
    WUX  Click Element  link:HTML
    Select frame  //iframe
    WUX  Element Should Contain  //body  ${message}

Vérifier qu'un mail a été envoyé au destinataire
    [Arguments]  ${mail_id}
    Accéder à maildump
    WUX  Element Should Be Visible  //tr[@data-message-id]/td[text()="${mail_id}"]
