*** Settings ***
Documentation  Actions spécifiques aux inscriptions

*** Keywords ***
Depuis le listing de la liste électorale
    [Documentation]  Accède au listing de la liste électorale
    Depuis le listing  electeur


Depuis la fiche de l'électeur
    [Documentation]  Accède à la fiche de l'électeur
    [Arguments]  ${electeur}
    Depuis le listing de la liste électorale
    Rechercher en recherche avancée simple  ${electeur}
    Click Element  css=#action-tab-electeur-left-consulter-${electeur}
    La page ne doit pas contenir d'erreur


Saisir les informations dans le formulaire de recherche avancée
    [Documentation]  Remplit le formulaire de recherche avancée
    [Arguments]  ${values}

    Si "id" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ine" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenoms" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_naissance_min" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_naissance_max" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "bureau" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "adresse" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "liste" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "bureauforce" existe dans "${values}" on execute "Select From List By Label" dans le formulaire


Rechercher des electeurs avec la recherche avancée
    [Documentation]  Vérifie si le formulaire de recherche avancé est ouvert.
    ...  Si ce n'est pas le cas switch le formulaire de recherche. Saisie
    ...  les éléments voulus dans la recherche avancée et valide le formulaire.
    [Arguments]  ${values}

    # Vérifie si la recherche avancé est ouverte et l'ouvre si ce n'est pas le cas
    ${passed} =  Run Keyword And Return Status  La recherche avancée doit être ouverte
    Run Keyword If  ${passed} == False  Click Element  css=#toggle-advanced-display

    # Rempli le formulaire
    Saisir les informations dans le formulaire de recherche avancée  ${values}

    # Valide le formulaire
    Click Element  adv-search-submit
    La page ne doit pas contenir d'erreur
