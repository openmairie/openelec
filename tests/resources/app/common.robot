*** Settings ***
Documentation  Actions spécifiques aux inscriptions

*** Keywords ***
WUX
    [Documentation]  "WUX -> WUKS -> Wait Until Keyword Succeds" Alias pour
    ...  gagner en lisibilité.
    [Arguments]  @{args}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  @{args}


Changer la date de tableau
    [Documentation]  Positionne la date de tableau à la valeur de l'argument.
    [Arguments]  ${datetableau}
    Go To Dashboard
    Click Element  css=#dashboard p.datetableau a
    Page Title Should Be  Changement De La Date De Tableau
    Input Text  datetableau  ${datetableau}
    Click Element  css=input[name='changedatetableau.action.valid']
    Page Title Should Be  Changement De La Date De Tableau
    Valid Message Should Be  La date de tableau est changée.


Depuis le menu 'Administration & Paramétrage'
    [Documentation]
    WUX  Click Element  css=a.shortlinks-settings
    Page Title Should Be  Administration & Paramétrage > Menu
    La page ne doit pas contenir d'erreur
