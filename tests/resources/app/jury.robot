*** Keywords ***
Depuis le module Jury D'assises
    [Documentation]  Accède au menu Traitement > Jury D'assises

    Go To Submenu In Menu  traitement  jury

Accéder à l'onglet Tirage Aléatoire du module Jury D'assises
    [Documentation]  Clique sur l'onglet Tirage Aléatoire du menu Traitement > Jury D'assises

    # On clique sur l'onglet voulu et on attend que la page se charge correctement
    Click Element  css=#jury-tirage-aleatoire
    Wait Until Page Contains Element  css=#traitement_jury_form

Accéder à l'onglet Liste Préparatoire Courante du module Jury D'assises
    [Documentation]  Clique sur l'onglet Liste Préparatoire Courante du menu Traitement > Jury D'assises

    # On clique sur l'onglet voulu et on attend que la page se charge correctement
    Click Element  css=#jury-liste-preparatoire-courante
    Wait Until Page Contains Element  css=#sousform-electeur_jury

Accéder à l'onglet Épuration du module Jury D'assises
    [Documentation]  Clique sur l'onglet Épuration du menu Traitement > Jury D'assises

    # On clique sur l'onglet voulu et on attend que la page se charge correctement
    Click Element  css=#jury-epuration
    Wait Until Page Contains Element  css=#traitement_jury_epuration_form

Accéder à l'onglet Paramétrage du module Jury D'assises
    [Documentation]  Clique sur l'onglet Paramétrage du menu Traitement > Jury D'assises

    # On clique sur l'onglet voulu et on attend que la page se charge correctement
    Click Element  css=#jury-parametrage
    Wait Until Page Contains Element  css=#sousform-parametrage_nb_jures

Modifier le paramétrage du nombre de jurés du canton
    [Documentation]  Accéde au menu Traitement > Jury D'assises et clique sur l'onglet
    ...  Paramétrage. Sélectionne ensuite le canton voulu avant de remplir le formulaire
    ...  de modification et de le valider
    [Arguments]  ${canton}  ${values}

    Depuis le module Jury D'assises
    Accéder à l'onglet Paramétrage du module Jury D'assises
    Click Link  ${Canton}
    Wait Until Page Contains Element  css=a#action-sousform-parametrage_nb_jures-modifier
    Click On SubForm Portlet Action  parametrage_nb_jures  modifier
    Saisir les valeurs dans le formulaire de paramétrage du nombre de jurés  ${values}
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

Saisir les valeurs dans le formulaire de paramétrage du nombre de jurés
    [Documentation]  Remplit le formulaire de paramétrage du nombre de jurés
    [Arguments]  ${values}

    Si "nb_jures" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nb_suppleants" existe dans "${values}" on execute "Input Text" dans le formulaire

Tirer au sort les jurés titulaires
    [Documentation]  Clique sur le bouton Tirage aléatoire des titulaires

    Click Element  css=input[name="traitement.jury.form.valid"]
    Handle Alert

Tirer au sort les jurés suppléants
    [Documentation]  Clique sur le bouton Tirage aléatoire des suppléants

    Click Element  css=input[name="traitement.jury_suppleant.form.valid"]
    Handle Alert

Epurer les jurés
    [Documentation]  Clique sur le bouton Épuration des jurés d'assises de l'onglet Épuration

    Click Element  css=input[name="traitement.jury_epuration.form.valid"]
    Handle Alert

Modifier les informations du juré
    [Documentation]  Clique sur l'action Juré d'assise de la fiche de l'électeur. Remplis
    ...  le formulaire et le valide.
    [Arguments]  ${values}

    Click On Form Portlet Action  electeur  edit-jury  modale
    Saisir les valeurs dans le formulaire de modification du juré  ${values}
    Click On Submit Button In SubForm
    Wait Until Element Contains  css=#sousform-electeur_jury .message  Vos modifications ont bien été enregistrées.
    La Page Ne Doit Pas Contenir D'erreur
    Click Link  css=div[aria-labelledby="ui-dialog-title-sousform-electeur_jury"] .ui-dialog-titlebar-close

Saisir les valeurs dans le formulaire de modification du juré
    [Documentation]  Remplit le formulaire de paramétrage du nombre de jurés
    [Arguments]  ${values}

    Si "jury" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "jury_effectif" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_jeffectif" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "profession" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "motif_dispense_jury" existe dans "${values}" on execute "Input Text" dans le formulaire

Ouvrir l'édition Liste préparatoire des suppléants du jury d'assises
    [Documentation]  Clique sur le bouton Liste préparatoire des suppléants du jury d'assises

    Click Link  css=a#action-module-jury-pdf-liste-preparatoire-suppleants

Ouvrir l'édition Étiquettes des jurés suppléants
    [Documentation]  Clique sur le bouton Étiquettes des jurés suppléants

    Click Link  css=a#action-module-jury-pdf-edtiquettes-suppleants

Ouvrir le csv Export CSV des électeurs suppléants sélectionnés
    [Documentation]  Clique sur le bouton Export CSV des électeurs suppléants sélectionnés
    ...  et clique sur le bouton d'export

   
    Click Link  css=a#action-module-jury-exports-electeurs-suppleants
    Wait Until Page Contains Element  css=input[name="reqmo-form-valid"]
    WUX  Click On Submit Button In Reqmo
    Page Title Should Be  Export > Requêtes Mémorisées > List__electeur_suppleant_jury