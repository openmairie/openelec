*** Settings ***
Documentation  Actions spécifiques aux modifications

*** Keywords ***
Depuis le listing des modifications
    [Documentation]  ...
    [Tags]  modification
    Depuis le listing  modification


Depuis le contexte de la modificarion
    [Documentation]  ..
    [Arguments]  ${mouvement}
    [Tags]  modification
    Depuis le listing des modifications
    Rechercher en recherche avancée simple  ${mouvement}
    Click Element  css=#action-tab-modification-left-consulter-${mouvement}
    La page ne doit pas contenir d'erreur

Abandonner le mouvement de modification REU
    [Documentation]  Clique sur l'action abandonner du mouvement
    WUX  Click On Form Portlet Action  inscription  reu-abandonner  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Form Value Should Be  css=#statut  abandonne
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    La page ne doit pas contenir d'erreur

Valider le mouvement de modification
    [Arguments]  ${nom_electeur}
    Depuis le listing des modifications
    Rechercher en recherche avancée simple  ${nom_electeur}
    Click On Link  ${nom_electeur}
    Click On Form Portlet Action  modification  valider  modale
    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only

Ajouter le mouvement de modification
    [Documentation]  ...
    [Arguments]  ${mouvement}
    [Return]  ${mouvement_id}
    #
    Go To Submenu In Menu  saisie  modification
    Page Title Should Be  Saisie > Modification
    #
    Input Text  nom  ${mouvement.electeur_nom}
    Click Button  Rechercher l'électeur à modifier
    Click Element  css=[title="Creer un mouvement de modification"]
    Sleep  2
    #
    Si "types" existe dans "${mouvement}" on execute "Select From List By Label" dans le formulaire
    Si "nom" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "nom_usage" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "date_naissance" existe dans "${mouvement}" on execute "Input Datepicker" dans le formulaire
    Si "date_demande" existe dans "${mouvement}" on execute "Input Datepicker" dans le formulaire
    # #
    # Input Text  css=#autocomplete-commune-naissance-search  ${mouvement.code_commune_de_naissance}
    # WUX  Click On Link  ${mouvement.code_commune_de_naissance} - ${mouvement.libelle_commune_de_naissance}
    # #
    # Input Text  css=#autocomplete-voie-search  ${mouvement.libelle_voie}
    # WUX  Click On Link  ${mouvement.libelle_voie}
    #
    Si "numero_habitation" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Si "complement" existe dans "${mouvement}" on execute "Input Text" dans le formulaire
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    ${mouvement_id} =  Get Element Attribute  css=.form-content #id  value