*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Conultation > Traces.


*** Test Cases ***
Fonctionnement basique
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  consultation  traces
    Page Title Should Be   Consultation > Traces & Éditions

Vérification du bon fonctionnement de l'api REST
    ${json} =  Set Variable  { "module": "migrate_logfiles_to_database"}
    Vérifier le code retour du web service et vérifier que son message est  Post  maintenance  ${json}  200  Nombre de fichiers traités : 0\rNombre de fichiers en erreurs : 0\r

