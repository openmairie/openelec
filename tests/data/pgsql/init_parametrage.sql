-- Paramètres de la collectivité LIBREVILLE
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES 
(nextval('om_parametre_seq'), 'datetableau', '2022-12-31', (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE')),
(nextval('om_parametre_seq'), 'maire', 'Monsieur le Maire', (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE')),
(nextval('om_parametre_seq'), 'inseeville', '00000', (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE')),
(nextval('om_parametre_seq'), 'cp', '00000', (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE')),
(nextval('om_parametre_seq'), 'num_tel', '0101010101', (SELECT om_collectivite FROM om_collectivite WHERE libelle='LIBREVILLE'));

-- MULTI
INSERT INTO om_collectivite (om_collectivite, libelle, niveau) VALUES (nextval('om_collectivite_seq'), 'MULTI', '2');
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (nextval('om_parametre_seq'), 'ville', 'MULTI', (SELECT om_collectivite FROM om_collectivite WHERE libelle='MULTI'));
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (nextval('om_utilisateur_seq'), 'Administrateur', 'nospam@openmairie.org', 'multi', '21232f297a57a5a743894a0e4a801fc3', (SELECT om_collectivite FROM om_collectivite WHERE libelle='MULTI'), 'DB', (SELECT om_profil FROM om_profil WHERE libelle='ADMINISTRATEUR'));


--
-- Data for Name: liste; Type: TABLE DATA; Schema: public; Owner: -
--

COPY liste (liste, libelle_liste, liste_insee, officielle) FROM stdin;
01	LISTE PRINCIPALE	LP	t
02	LISTE COMPLÉMENTAIRE EUROPÉENNE	LCE	t
03	LISTE COMPLÉMENTAIRE MUNICIPALE	LCM	t
04	LISTES COMPLÉMENTAIRES E&M	LC2	f
\.


--
-- Data for Name: param_mouvement; Type: TABLE DATA; Schema: public; Owner: -
--

COPY param_mouvement (code, libelle, typecat, effet, cnen, codeinscription, coderadiation, edition_carte_electeur, insee_import_radiation, om_validite_debut, om_validite_fin) FROM stdin;
IN	INSCRIPTION NATIONALITE FRANCAISE	Inscription	Immediat	Oui	1	 	1	\N	\N	\N
IM	INSCRIPTION MAJEUR J-5 (L11-2)	Inscription	Immediat	Oui	8	 	1	\N	\N	\N
PI	PREMIERE INSCRIPTION 	Inscription	1erMars	Oui	1	 	1	\N	\N	\N
CC	VENANT D'UNE AUTRE COMMUNE 	Inscription	1erMars	Oui	1	 	1	\N	\N	\N
IO	INSCRIPTION D'OFFICE	Inscription	Immediat	Oui	8	 	1	\N	\N	\N
IJ	INSCRIPTION JUDICIAIRE	Inscription	Immediat	Oui	2	 	1	\N	\N	\N
EC	MODIFICATION ETAT CIVIL 	Modification	1erMars	Non	 	 	1	\N	\N	\N
CX	MODIFICATION ADRESSE ET ETAT CIVIL 	Modification	1erMars	Non	 	 	1	\N	\N	\N
CB	CHANGEMENT ADRESSE	Modification	1erMars	Non	 	 	1	\N	\N	\N
L5	MISE SOUS TUTELLE INSEE	Radiation	Immediat	Non	 	 	0	\N	\N	\N
RA	RADIATION DEMANDEE PAR INSEE 	Radiation	Immediat	Non	 	 	0	\N	\N	\N
DN	DECES INSEE	Radiation	Immediat	Non	 	 	0	\N	\N	\N
DC	DECES	Radiation	Immediat	Oui	 	D	0	\N	\N	\N
DM	DEPART COMMUNE INSEE	Radiation	Immediat	Non	 	 	0	\N	\N	\N
DI	DOUBLE INSCRIPTIONS	Radiation	Immediat	Oui	 	E	0	\N	\N	\N
DP	DEPART AUTRE COMMUNE	Radiation	Immediat	Oui	 	P	0	\N	\N	\N
BR	MODIFICATION REDECOUPAGE	Modification	1erMars	Non	 	 	1		\N	\N
I6J	IO 18 ANS 01/03 -> 22/03/2014 (L11-2)	Inscription	Election	Oui	8	 	1		\N	2014-04-01
I6M	IO TABLEAU DU 6 MARS (L11-2)	Inscription	Election	Oui	8	 	1		\N	2014-06-01
IOD	IO 18 ANS 01/03 -> 22/03/2015 (L11-2)	Inscription	Election	Oui	8	 	1		\N	2015-04-01
I6O	IO TABLEAU DU 6 OCTOBRE (L11-2 AL2)	Inscription	Election	Oui	8	 	1		\N	2015-12-14
VOL1	Inscription volontaire	Inscription	Immediat	   	 	 	1	\N	\N	\N
VOL_L30	Inscription volontaire L30	Inscription	Immediat	   	 	 	1	\N	\N	\N
VOL	Radiation volontaire sur liste complémentaire	Radiation	Immediat	   	 	 	0	\N	\N	\N
MAI	Radiation pour perte d'attache communale ou consulaire	Radiation	Immediat	   	 	 	0	\N	\N	\N
OFFJUDICIAIRE	Inscription d'office sur décision judiciaire	Inscription	Immediat	   	 	 	1	\N	\N	2019-01-01
PROP_OFFDJ	Proposition d'inscription d'office sur à décision judiciaire	Inscription	Immediat	   	 	 	1	\N	\N	2019-01-01
PROP_OFFJ	Proposition d'inscription d'office jeune	Inscription	Immediat	   	 	 	1	\N	\N	2019-01-01
PROP_OFFN	Proposition d'inscription d'office naturalisé	Inscription	Immediat	   	 	 	1	\N	\N	2019-01-01
OFFJ	Inscription d'office jeune	Inscription	Immediat	   	 	 	1	\N	\N	2019-01-01
OFFN	Inscription d'office entrée nationalité	Inscription	Immediat	   	 	 	1	\N	\N	2019-01-01
NVO	Radiation suite à nouveau rattachement	Radiation	Immediat	   	 	 	0	\N	\N	2019-01-01
DEC	Décès	Radiation	Immediat	   	 	 	0	\N	\N	2019-01-01
DJ1	Radiation suite à recours contentieux	Radiation	Immediat	   	 	 	0	\N	\N	2019-01-01
TUT	Radiation suite à mise sous tutelle	Radiation	Immediat	   	 	 	0	\N	\N	2019-01-01
CON	Radiation suite à condamnation	Radiation	Immediat	   	 	 	0	\N	\N	2019-01-01
NAT	Radiation suite à la perte de nationalité	Radiation	Immediat	   	 	 	0	\N	\N	2019-01-01
CCIN	Inscription sur décision de la commission de contrôle	Inscription	Immediat	   	 	 	1	\N	\N	\N
CCR	Radiation sur décision de la commission de contrôle	Radiation	Immediat	   	 	 	0	\N	\N	2019-01-01
INIT	Inscription pour initialisation du répertoire	Inscription	Immediat	   	 	 	1	\N	\N	2019-01-01
INIT_AJOUT	Ajout après l'initialisation du répertoire	Inscription	Immediat	   	 	 	1	\N	\N	2019-01-01
OEL_M_DIV	Modification divers	Modification	Immediat	   	 	 	1	\N	\N	\N
OEL_M_ADR	Changement d'adresse dans la commune	Modification	Immediat	   	 	 	1	\N	\N	\N
OEL_M_USAGE	Modification du nom d'usage	Modification	Immediat	   	 	 	1	\N	\N	\N
OEL_M_CONTACT	Modification des coordonnées de contact	Modification	Immediat	   	 	 	1	\N	\N	\N
INIT_SUPPR	Suppression par la commune	Radiation	Immediat	   	 	 	0	\N	\N	2019-01-01
MODEC	Modification d'état civil	Modification	Immediat	   	 	 	0	\N	\N	2019-01-01
\.

--
-- Data for Name: revision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY revision (id, libelle, date_debut, date_effet, date_tr1, date_tr2) FROM stdin;
2	Révision exceptionnelle pour le 01/12/2015	2015-03-01	2015-12-01	2015-10-10	2015-11-30
1	Révision traditionnelle pour le 01/03/2016	2015-12-01	2016-03-01	2016-01-10	2016-02-29
3	Révision traditionnelle pour le 01/03/2015	2014-03-01	2015-03-01	2015-01-10	2015-02-28
4	Révision traditionnelle pour le 01/03/2014	2013-03-01	2014-03-01	2014-01-10	2014-02-28
5	Révision traditionnelle pour le 01/03/2013	2012-03-01	2013-03-01	2013-01-10	2013-02-28
6	Révision traditionnelle pour le 01/03/2012	2011-03-01	2012-03-01	2012-01-10	2012-02-29
7	Révision traditionnelle pour le 01/03/2017	2016-03-01	2017-03-01	2017-01-10	2017-02-28
8	Révision traditionnelle pour le 01/03/2018	2017-03-01	2018-03-01	2018-01-10	2018-02-28
9	Révision exceptionnelle REU	2018-03-01	2025-01-01	2019-01-10	2024-12-31
\.

