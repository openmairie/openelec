--------------------------------------------------------------------------------
-- Script d'installation des jeux de données pour les tests
--
-- ATTENTION ce script est prévu pour être appliqué après le install.sql
--
-- @package openelec
-- @version SVN : $Id$
--------------------------------------------------------------------------------
--
START TRANSACTION;
\set ON_ERROR_STOP on
--
\set schema 'openelec'
--
SET search_path = :schema, public, pg_catalog;
--
\i 'tests/data/pgsql/init_parametrage.sql'
\i 'tests/data/pgsql/init_parametrage_widget_dashboard.sql'
\i 'tests/data/pgsql/init_parametrage_editions.sql'
\i 'tests/data/pgsql/init_parametrage_decoupage.sql'
\i 'tests/data/pgsql/init_data.sql'
--
\i 'tests/data/pgsql/update_sequences.sql'
--
\i 'tests/data/pgsql/openscrutin_parametrage_dev.sql'
\i 'tests/data/pgsql/openscrutin_data_dev.sql'
--
COMMIT;

