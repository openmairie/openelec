
-- =================================================================
-- PARAMÉTRAGE openScrutin fusionné dans openElec
-- =================================================================

--
-- Data for Name: om_collectivite; Type: TABLE DATA; Schema: openscrutin; Owner: -
--
-- INSERT INTO om_collectivite (om_collectivite, libelle, niveau) VALUES (nextval('om_collectivite_seq'), 'MULTI', '2');

--
-- Data for Name: om_profil; Type: TABLE DATA; Schema: openscrutin; Owner: -
--

-- Profil à ajouter ultérieurement si besoin
--INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'SUPER UTILISATEUR', 4);
--INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'UTILISATEUR', 3);
--INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'UTILISATEUR LIMITE', 2);
--INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (nextval('om_profil_seq'), 'CONSULTATION', 1);

--
-- Data for Name: om_droit; Type: TABLE DATA; Schema: openscrutin; Owner: -
--

-- Droits déjà défini dans 'om_permissions'
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (1, 'om_utilisateur', 1);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (2, 'om_droit', 1);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (3, 'om_profil', 1);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (4, 'om_collectivite', 1);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (9, 'gen', 1);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (14, 'directory', 1);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (15, 'import', 1);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (43, 'logout', 1);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (45, 'periode', 1);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (5, 'om_parametre', 2);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (6, 'om_etat', 2);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (7, 'om_sousetat', 2);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (8, 'om_lettretype', 2);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (10, 'om_sig_map', 2);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (11, 'om_sig_map_comp', 2);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (12, 'om_sig_map_wms', 2);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (13, 'om_sig_wms', 2);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (19, 'om_widget', 2);
-- INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (20, 'om_tdb', 2);

---- Droits pour le profil 'SUPER UTILISATEUR'
--DO $$
--DECLARE
--    current_profil om_profil.om_profil%type;
--BEGIN
--    SELECT om_profil INTO current_profil FROM om_profil WHERE libelle = 'SUPER UTILISATEUR';
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'scrutin', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'service', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'poste', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'grade', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'canton', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'bureau', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'composition_bureau', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'composition_bureau_edition', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'convocation_agent', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'convocation_president', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'recepisse', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'affectation_heure', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'transfert_candidature', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'transfert_affectation', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'scrutin_default', current_profil);
--END $$;
--
---- Droits pour le profil 'UTILISATEUR'
--DO $$
--DECLARE
--    current_profil om_profil.om_profil%type;
--BEGIN
--    SELECT om_profil INTO current_profil FROM om_profil WHERE libelle = 'UTILISATEUR';
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'elu', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'candidature', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'candidat', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'agent', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'affectation', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'valid_copie', current_profil);
--END $$;
--
---- Droits pour le profil 'CONSULTATION'
--DO $$
--DECLARE
--    current_profil om_profil.om_profil%type;
--BEGIN
--    SELECT om_profil INTO current_profil FROM om_profil WHERE libelle = 'CONSULTATION';
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'password', current_profil);
--    INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES (nextval('om_droit_seq'), 'logout', current_profil);
--END $$;

--
-- Data for Name: om_etat; Type: TABLE DATA; Schema: openscrutin; Owner: -
--

-- Pas ajouté car invalides et trop couteux à adapter manuellement,
-- possibilité de les exporter pour les ré-importer.

--
-- Data for Name: om_sousetat; Type: TABLE DATA; Schema: openscrutin; Owner: -
--

-- Pas ajouté car invalides et trop couteux à adapter manuellement,
-- possibilité de les exporter pour les ré-importer.

--
-- Data for Name: om_parametre; Type: TABLE DATA; Schema: openscrutin; Owner: -
--

DO $$
DECLARE
    collectivite_ville om_collectivite.om_collectivite%type;
BEGIN
    SELECT om_collectivite INTO collectivite_ville FROM om_collectivite WHERE libelle = 'LIBREVILLE';
    INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (nextval('om_parametre_seq'), 'prefixe_edition_substitution_vars', 'edi_', collectivite_ville);
    INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (nextval('om_parametre_seq'), 'edi_logo_composition_scrutin_marge', '<br/><br/><br/><br/><br/><br/><br/>', collectivite_ville);
    INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (nextval('om_parametre_seq'), 'edi_ville', 'LIBREVILLE', collectivite_ville);
    INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (nextval('om_parametre_seq'), 'option_composition_scrutin', 'true', collectivite_ville);
    INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (nextval('om_parametre_seq'), 'decision', 'oui', collectivite_ville);
    INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (nextval('om_parametre_seq'), 'signataire', 'Michèle DURAND', collectivite_ville);
    INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (nextval('om_parametre_seq'), 'adjoint', 'Danielle DUPONT', collectivite_ville);
END $$;

--
-- Data for Name: periode; Type: TABLE DATA; Schema: openscrutin; Owner: -
--

INSERT INTO periode (periode, libelle, debut, fin) VALUES ('matin', '    de  7H00  à  13H00', '07:00:00', '13:00:00');
INSERT INTO periode (periode, libelle, debut, fin) VALUES ('apres-midi', '    de  13H00  à  LA FIN', '13:00:00', '22:00:00');
INSERT INTO periode (periode, libelle, debut, fin) VALUES ('journee', '    de  7H00  à  LA FIN', '07:00:00', '23:00:00');


--
-- Data for Name: poste; Type: TABLE DATA; Schema: openscrutin; Owner: -
--

INSERT INTO poste (poste, libelle, nature, ordre) VALUES ('SECRETAIRE', 'SECRETAIRE', 'candidature', 0);
INSERT INTO poste (poste, libelle, nature, ordre) VALUES ('PLANTON', 'PLANTON', 'candidature', 0);
INSERT INTO poste (poste, libelle, nature, ordre) VALUES ('PRESIDENT', 'PRESIDENT', 'affectation', 1);
INSERT INTO poste (poste, libelle, nature, ordre) VALUES ('DELEGUE TITULAIRE', 'DELEGUE TITULAIRE', 'affectation', 5);
INSERT INTO poste (poste, libelle, nature, ordre) VALUES ('ASSESSEUR TITULAIRE', 'ASSESSEUR TITULAIRE', 'affectation', 3);
INSERT INTO poste (poste, libelle, nature, ordre) VALUES ('ASSESSEUR SUPPLEANT', 'ASSESSEUR SUPPLEANT', 'affectation', 4);
INSERT INTO poste (poste, libelle, nature, ordre) VALUES ('AGENT CENTRALISATION', 'AGENT CENTRALISATION', 'candidature', 0);
INSERT INTO poste (poste, libelle, nature, ordre) VALUES ('DELEGUE SUPPLEANT', 'DELEGUE SUPPLEANT', 'affectation', 6);
INSERT INTO poste (poste, libelle, nature, ordre) VALUES ('PRESIDENT SUPPLEANT', 'PRESIDENT SUPPLEANT', 'affectation', 2);

INSERT INTO om_logo (om_logo, id, libelle, description, fichier, resolution, actif, om_collectivite) VALUES 
(nextval('om_logo_seq'), 'logopdf.png', 'logopdf.png', NULL, 'ce8218f718f5224430da72be8bacbf69', 150, true, 1);


--
-- Data for Name: om_requete; Type: TABLE DATA; Schema: openelec; Owner: -
--

INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES 
(
    nextval('om_requete_seq'),
    'composition_bureau',
    'Contexte ''scrutin''', 'Tous les champs de fusion disponibles pour une composition de bureau',
    '
    SELECT
        composition_bureau.id,
        composition_scrutin.libelle as "composition_scrutin.libelle",
        to_char(composition_scrutin.date_tour ,''DD/MM/YYYY'') as "composition_scrutin.date_tour",
        bureau.libelle AS "libelle_bureau",
        bureau.id AS "bureau"
    FROM
        &DB_PREFIXEcomposition_bureau
            INNER join &DB_PREFIXEcomposition_scrutin
                ON composition_bureau.composition_scrutin=composition_scrutin.id
            INNER JOIN &DB_PREFIXEbureau
                ON bureau.id=composition_bureau.bureau
    WHERE
        composition_bureau.id=&idx',
        NULL, 'sql', NULL, NULL
);
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES 
(
    nextval('om_requete_seq'),
    'candidature',
    'convocation agent', 'Tous les champs de fusion disponibles pour une convocation d''agent',
    '
    SELECT
        nom,
        prenom,
        adresse,
        agent.cp as cpagent,
        agent.ville as villeagent,
        candidature.composition_scrutin,
        reu_scrutin.libelle as libellescrutin,
        composition_scrutin.libelle as "composition_scrutin.libelle",
        to_char(composition_scrutin.date_tour ,''DD/MM/YYYY'') as "composition_scrutin.date_tour",
        candidature.poste,
        composition_scrutin.convocation_agent,
        candidature.bureau,
        bureau.libelle as libellebureau,
        candidature.periode,
        bureau.id,
        adresse1,
        adresse2,
        periode.libelle as libelleperiode
    FROM
        &DB_PREFIXEagent
            INNER JOIN &DB_PREFIXEcandidature
                ON agent.id = candidature.agent
            INNER JOIN &DB_PREFIXEcomposition_scrutin
                ON candidature.composition_scrutin=composition_scrutin.id
            INNER JOIN &DB_PREFIXEreu_scrutin
                ON composition_scrutin.scrutin=reu_scrutin.id
            INNER JOIN &DB_PREFIXEbureau
                ON bureau.id = candidature.bureau
            LEFT JOIN &DB_PREFIXEperiode
                ON candidature.periode=periode.periode
    WHERE
        candidature.id=''&idx''',
    NULL, 'sql', NULL, NULL
);
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES 
(
    nextval('om_requete_seq'),
    'affectation',
    'convocation elu', 'Tous les champs de fusion disponibles pour une convocation de président ou autre',
    '
    SELECT
        elu.nom,
        elu.prenom,
        elu.adresse,
        elu.cp as cpelu,
        elu.ville as villeelu,
        affectation.composition_scrutin,
        reu_scrutin.libelle as libellescrutin,
        composition_scrutin.libelle as "composition_scrutin.libelle",
        to_char(composition_scrutin.date_tour ,''DD/MM/YYYY'') as "composition_scrutin.date_tour",
        composition_scrutin.convocation_president,
        affectation.poste,
        affectation.bureau,
        bureau.libelle as libellebureau,
        bureau.id,
        bureau.adresse1,
        bureau.adresse2,
        candidat.nom as candidat
    FROM
        &DB_PREFIXEelu
            INNER JOIN &DB_PREFIXEaffectation
                ON elu.id = affectation.elu
            INNER JOIN &DB_PREFIXEcomposition_scrutin
                ON affectation.composition_scrutin=composition_scrutin.id
            INNER JOIN &DB_PREFIXEreu_scrutin
                ON composition_scrutin.scrutin=reu_scrutin.id
            INNER JOIN &DB_PREFIXEbureau
                ON bureau.id = affectation.bureau
            LEFT JOIN &DB_PREFIXEcandidat
                ON candidat.id = affectation.candidat
    WHERE
        affectation.id=''&idx''',
    NULL, 'sql', NULL, NULL
);
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES 
(
    nextval('om_requete_seq'),
    'composition_scrutin',
    'Contexte ''composition_scrutin''', 'Tous les champs de fusion disponibles pour un enregistrement composition_scrutin',
    '
    SELECT
        composition_scrutin.libelle as "composition_scrutin.libelle",
        to_char(composition_scrutin.date_tour ,''DD/MM/YYYY'') as "composition_scrutin.date_tour"
    FROM
        &DB_PREFIXEcomposition_scrutin
    WHERE
        composition_scrutin.id = ''&idx''',
    NULL, 'sql', NULL, NULL
);

--
-- Data for Name: om_etat; Type: TABLE DATA; Schema: openelec; Owner: -
--

DO $$
DECLARE
    collectivite_libreville om_collectivite.om_collectivite%type;
    req_composition_bureau om_requete.om_requete%type;
    req_candidature om_requete.om_requete%type;
    req_recepisse om_requete.om_requete%type;
    req_affectation om_requete.om_requete%type;
    req_composition_scrutin om_requete.om_requete%type;
BEGIN
    SELECT om_collectivite INTO collectivite_libreville FROM om_collectivite WHERE libelle = 'LIBREVILLE';
    SELECT om_requete INTO req_composition_bureau FROM om_requete WHERE code = 'composition_bureau';
    SELECT om_requete INTO req_candidature FROM om_requete WHERE code = 'candidature';
    SELECT om_requete INTO req_recepisse FROM om_requete WHERE code = 'recepisse';
    SELECT om_requete INTO req_affectation FROM om_requete WHERE code = 'affectation';
    SELECT om_requete INTO req_composition_scrutin FROM om_requete WHERE code = 'composition_scrutin';
    
    INSERT INTO om_etat (om_etat, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, margeleft, margetop, margeright, margebottom, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES 
    (
        nextval('om_etat_seq'), collectivite_libreville,
        'composition_scrutin', 'composition du scrutin',
        true, 'P', 'A4', 'logopdf.png', 15, 15,
        15, 15, 15, 15,
        '
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">&logo_composition_scrutin_marge</td>
<td style="width: 50%;">
<p style="text-align: center;"><strong>[composition_scrutin.libelle] du [composition_scrutin.date_tour]</strong></p>
<p style="text-align: center;">Composition des bureaux de vote</p>
<p> </p>
<p style="text-align: center;">Composition</p>
</td>
</tr>
</tbody>
</table>
',
        15, 15, 180, 5, 0,
        '
<p><span id=''scrutin_bureau'' class=''mce_sousetat''>scrutin bureau</span></p>
',
        req_composition_scrutin,
        'helvetica', '0-0-0',
        '', 12,
        '<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">
<p style="font-size: 8px;">&ville</p>
</td>
<td style="width: 50%;">
<p style="text-align: right; font-size: 8px;">ÉLECTIONS - Édition du &aujourdhui_lettre</p>
</td>
</tr>
</tbody>
</table>', 10
    );
    INSERT INTO om_etat (om_etat, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, margeleft, margetop, margeright, margebottom, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES 
    (
        nextval('om_etat_seq'), collectivite_libreville,
        'composition_scrutin_liste_presidents', 'liste des presidents et suppleants',
        true, 'P', 'A4', 'logopdf.png', 15, 15,
        15, 15, 15, 15,
        '
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">&logo_composition_scrutin_marge</td>
<td style="width: 50%;">
<p style="text-align: center;"><strong>[composition_scrutin.libelle] du [composition_scrutin.date_tour]</strong></p>
<p style="text-align: center;">Composition des bureaux de vote</p>
<p> </p>
<p style="text-align: center;">Liste des présidents et des suppléants</p>
</td>
</tr>
</tbody>
</table>
',
        15, 15, 180, 5, 0,
        '
<p><span id=''scrutin_president'' class=''mce_sousetat''>scrutin president</span></p>
',
        req_composition_scrutin,
        'helvetica', '0-0-0',
        '', 12,
        '<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">
<p style="font-size: 8px;">&ville</p>
</td>
<td style="width: 50%;">
<p style="text-align: right; font-size: 8px;">ÉLECTIONS - Édition du &aujourdhui_lettre</p>
</td>
</tr>
</tbody>
</table>', 10
    );
    INSERT INTO om_etat (om_etat, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, margeleft, margetop, margeright, margebottom, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES 
    (
        nextval('om_etat_seq'), collectivite_libreville,
        'composition_bureau', 'Composition d''un bureau de vote',
        true,
        'L', 'A4', 'logopdf.png', 15, 15,
        15, 15, 15, 15,
        '
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">&logo_composition_scrutin_marge</td>
<td style="width: 50%;">
<p style="text-align: center;"><strong>[composition_scrutin.libelle] du [composition_scrutin.date_tour]</strong></p>
<p style="text-align: center;">Composition des bureaux de vote</p>
<p> </p>
<p style="text-align: center;">Composition du bureau de vote</p>
<p style="text-align: center;">[bureau] - [libelle_bureau]</p>
</td>
</tr>
</tbody>
</table>
',
        15, 15, 267, 10, 0,
        '
<p><span id=''composition_bureau'' class=''mce_sousetat''>composition bureau</span></p>
',
        req_composition_bureau,
        'helvetica', '0-0-0',
        '', 12,
        '<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">
<p style="font-size: 8px;">&ville</p>
</td>
<td style="width: 50%;">
<p style="text-align: right; font-size: 8px;">ÉLECTIONS - Édition du &aujourdhui_lettre</p>
</td>
</tr>
</tbody>
</table>', 10
    );
    INSERT INTO om_etat (om_etat, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, margeleft, margetop, margeright, margebottom, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES 
    (
        nextval('om_etat_seq'), collectivite_libreville,
        'candidature', 'convocation agent',
        true, 'P', 'A4', 'logopdf.png', 15, 15,
        15, 15, 15, 15,
        '
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">&logo_composition_scrutin_marge</td>
<td style="width: 50%;">
<p>&ville, le &aujourdhui_lettre</p>
<p> </p>
<p> </p>
<p>[nom] [prenom]<br/>
[adresse]<br/>
[cpagent] [villeagent]</p>
</td>
</tr>
</tbody>
</table>',
        15, 15, 180, 5, 0,
        '
<p> </p>
<p> </p>
<p><strong>Objet : Élection du [composition_scrutin.date_tour] - Affectation / Réunion préparatoire</strong></p>
<p> </p>
<p> </p>
<p>
Madame, Monsieur,<br/>
<br/>
J ai le plaisir de vous faire part de votre affectation en qualité de « [poste] »<br/>
<br/>
Pour les élections [composition_scrutin.libelle]<br/>
<br/>
Le DIMANCHE [composition_scrutin.date_tour]<br/>
<br/>
Au bureau de vote  « [bureau] [libellebureau] »  [adresse1] [adresse2]<br/>
<br/>
pour la periode : [periode]  [libelleperiode]<br/>
<br/>
En cas d empêchement de votre part, pour assurer ces fonctions, il convient de m en informer le plus rapidement possible au 04.90.49.35.48.<br/>
<br/>
Vous voudrez bien PARTICIPER IMPERATIVEMENT à la réunion préparatoire dont les horaires et le lieu sont précisés ci-dessous, par avance je vous en remercie.<br/>
<br/>
[convocation_agent]<br/>
<br/>
Cette réunion, destinée à l ensemble des secrétaires et plantons, est organisée afin de vous présenter les nouvelles dispositions arrêtées pour ces scrutins et vous donner toutes précisions utiles notamment sur l établissement du procès-verbal.<br/>
<br/>
Je vous prie de recevoir, l assurance de mes sentiments distingués.
</p>
<p style="text-align: right">&signataire</p>
', req_candidature, 'helvetica', '0-0-0',
        '', 12, 'candidature', 12
    );
    INSERT INTO om_etat (om_etat, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, margeleft, margetop, margeright, margebottom, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES 
    (
        nextval('om_etat_seq'), collectivite_libreville,
        'recepisse', 'recepisse assesseurs et delegues',
        true, 'P', 'A4', 'logopdf.png', 15, 15,
        15, 15, 15, 15,
        '
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">&logo_composition_scrutin_marge</td>
<td style="width: 50%;">
<p style="text-align: center;"><strong>[composition_scrutin.libelle] du [composition_scrutin.date_tour]</strong></p>
<p style="text-align: center;">Composition des bureaux de vote</p>
<p> </p>
<p style="text-align: center;">Récépissé</p>
</td>
</tr>
</tbody>
</table>
',
        15, 15, 180, 5, 0,
        '
<p> </p>
<p> </p>
<p style="text-align: justify">Vu l''article R.44 du Code électoral
et la circulaire ministérielle NOR/INT/A/07/000123/C du 20 décembre 2007,</p>
<p> </p>
<p>Le Maire donne récépissé à :</p>
<p> </p>
<p>[nom] [prenom]<br/>
[adresse]<br/>
[cpelu] [villeelu]</p>
<p> </p>
<p>De sa désignation en qualité de [poste]</p>
<p> </p>
<p>Au bureau de vote : [bureau] [libellebureau]</p>
<p> </p>
<p>Pour le candidat : [candidat]</p>
<p> </p>
<p> </p>
<p style="text-align: right">Fait à &ville, le &aujourdhui</p>
<p style="text-align: right">Le Maire</p>
',
        req_affectation,
        'helvetica', '0-0-0',
        '', 12,
        '<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">
<p style="font-size: 8px;">&ville</p>
</td>
<td style="width: 50%;">
<p style="text-align: right; font-size: 8px;">ÉLECTIONS - Édition du &aujourdhui_lettre</p>
</td>
</tr>
</tbody>
</table>', 10
    );
    INSERT INTO om_etat (om_etat, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, margeleft, margetop, margeright, margebottom, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, se_font, se_couleurtexte, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES 
    (
        nextval('om_etat_seq'), collectivite_libreville,
        'affectation', 'convocation elu',
        true, 'P', 'A4', 'logopdf.png', 15, 15,
        15, 15, 15, 15,
        '
<table style="width: 100%;" border="0">
<tbody>
<tr>
<td style="width: 50%;">&logo_composition_scrutin_marge</td>
<td style="width: 50%;">
<p>&ville, le &aujourdhui_lettre</p>
<p> </p>
<p> </p>
<p>[nom] [prenom]<br/>
[adresse]<br/>
[cpelu] [villeelu]</p>
</td>
</tr>
</tbody>
</table>',
        15, 15, 180, 5, 0,
        '
<p> </p>
<p> </p>
<p><strong>Objet : Élection du [composition_scrutin.date_tour] - Affectation</strong></p>
<p> </p>
<p> </p>
<p>Madame, Monsieur, Cher(e) Collègue,</p>
<p></p>
<p>Conformément aux dispositions de l''article R.43 du code
électoral "les bureaux de vote sont présidés par les Maires,
Adjoints et Conseillers Municipaux dans l’ordre du tableau".</p>
<p></p>
<p>Je vous confirme votre affectation en qualité de [poste] au bureau de vote :</p>
<p></p>
<p>N° [bureau] [libellebureau]<br/>
[adresse1]<br/>
[adresse2]</p>
<p></p>
<p>[convocation_president]</p>
<p></p>
<p>Dans cette attente, je vous prie de croire, Madame, Monsieur, Cher(e) Collègue, à l''expression de mes sentiments les meilleurs.</p>
<p></p>
<p style="text-align: right">Pour le Maire,<br/>
L Adjoint déléguée</p>
<p style="text-align: right">&adjoint</p>
',
        req_affectation,
        'helvetica', '0-0-0',
        '', 12, 'affectation', 12
    );
END $$;

--
-- Data for Name: om_sousetat; Type: TABLE DATA; Schema: openscrutin; Owner: -
--


DO $$
DECLARE
    collectivite_libreville om_collectivite.om_collectivite%type;
BEGIN
    SELECT om_collectivite INTO collectivite_libreville FROM om_collectivite WHERE libelle = 'LIBREVILLE';
    INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql) VALUES
    (
        nextval('om_sousetat_seq'), collectivite_libreville,
        'scrutin_bureau', 'composition des bureaux par scrutin', true,
        ' ',
        0, 'helvetica', 'B', 0, '0', 'L', '0', '255-255-255', '0-0-0', 0, 0,
        '1', '1', '0|0|0', 10, 'TLB|LTB|LTBR', 'C|C|C', '255-255-255', '0-0-0', 
        180, '1', 8, '0-0-0', '243-243-246', '255-255-255',
        '1', 10, '60|60|60', 'TBL|TBL|TRBL', 'TBL|TBL|TRBL', 'L|L|L',
        '1', 10, 15, '196-213-215', 'TBL|TBL|TRBL', 'L|L|L',
        '1', 10, 15, '196-213-215', 'TBL|TBL|TRBL', 'L|L|L',
        '1', 10, 15, '196-213-215', 'TBL|TBL|TRBL', 'L|L|L',
        '999|999|999', '0|0|0', '0|0|0', '0|0|0',
        '
        SELECT
            (CASE WHEN bureau.code != ''T'' THEN affectation.bureau||'' - ''||bureau.libelle ELSE bureau.libelle END) as bureau,
            affectation.poste,
            concat(nom||'' ''||prenom|| '' (''||adresse||'' ''||elu.cp||'' ''||elu.ville||'')'') as nom
        FROM
            &DB_PREFIXEaffectation
                INNER JOIN &DB_PREFIXEelu
                    ON elu.id=affectation.elu
                LEFT JOIN &DB_PREFIXEbureau
                    ON bureau.id = affectation.bureau
                INNER JOIN &DB_PREFIXEposte
                    ON affectation.poste = poste.poste
        WHERE
            affectation.composition_scrutin = ''&idx''
            AND decision IS TRUE
        ORDER BY
            affectation.bureau,
            poste.ordre
        '
    ),
    (
        nextval('om_sousetat_seq'), collectivite_libreville,
        'scrutin_president', 'liste presidents et suppleants', true,
        ' ',
        0, 'helvetica', 'B', 0, '0', 'L', '0', '255-255-255', '0-0-0', 0, 0,
        '1', '1', '0|0|0', 10, 'TLB|LTB|LTBR', 'C|C|C', '255-255-255', '0-0-0', 
        180, '1', 8, '0-0-0', '243-243-246', '255-255-255',
        '1', 10, '60|60|60', 'TBL|TBL|TRBL', 'TBL|TBL|TRBL', 'L|L|L',
        '1', 10, 15, '196-213-215', 'TBL|TBL|TRBL', 'L|L|L',
        '1', 10, 15, '196-213-215', 'TBL|TBL|TRBL', 'L|L|L',
        '1', 10, 15, '196-213-215', 'TBL|TBL|TRBL', 'L|L|L',
        '999|999|999', '0|0|0', '0|0|0', '0|0|0',
        '
        SELECT
            (affectation.bureau||'' - ''||bureau.libelle) as bureau,
            affectation.poste,
            (elu.nom||'' ''||elu.prenom||'' (''||elu.adresse||'' ''||elu.cp||'' ''||elu.ville||'')'') as nom
        FROM
            &DB_PREFIXEaffectation
                INNER JOIN &DB_PREFIXEelu
                    ON elu.id=affectation.elu
                INNER JOIN &DB_PREFIXEbureau
                    ON bureau.id = affectation.bureau
        WHERE
            affectation.composition_scrutin = ''&idx''
            AND (
                poste = ''PRESIDENT''
                OR poste =''PRESIDENT SUPPLEANT''
            )
            AND decision IS TRUE
        ORDER BY
            affectation.bureau,
            affectation.poste'
    ),
    (
        nextval('om_sousetat_seq'), collectivite_libreville,
        'composition_bureau', 'composition bureau', true,
        ' ',
        0, 'helvetica', 'B', 0, '0', 'L', '0', '243-246-246', '0-0-0', 0, 0,
        '1', '1', '0|0|0', 0, 'TBL|TBL|TBL|TBL|TBL|TRBL', 'C|C|C|C|C|C', '255-255-255', '0-0-0',
        267, '1', 9, '0-0-0', '243-243-246', '255-255-255',
        '1', 9, '50|50|40|50|50|27', 'TBL|TBL|TBL|TBL|TBL|TRBL', 'TBL|TBL|TBL|TBL|TBL|TRBL', 'L|L|L|L|L|L',
        '1', 10, 15, '196-213-215', 'TBL|TBL|TBL|TBL|TBL|TRBL', 'L|L|L|L|L|L',
        '1', 10, 15, '196-213-215', 'TBL|TBL|TBL|TBL|TBL|TRBL', 'L|L|L|L|L|L',
        '1', 10, 15, '196-213-215', 'TBL|TBL|TBL|TBL|TBL|TRBL', 'L|L|L|L|L|L',
        '999|999|999', '0|0|0', '0|0|0', '0|0|0',
        '
        SELECT 
            fonction,
            nom_prenoms,
            date_et_lieu_de_naissance,
            adresse,
            candidat_ou_liste,
            '''' as emargement
        FROM
        (
        SELECT * FROM (
        SELECT
            poste.ordre as hidden_ordre,
            poste.libelle as fonction,
            (elu.nom||'' ''||elu.prenom) as nom_prenoms,
            (to_char(elu.date_naissance, ''DD/MM/YYYY'')||'' ''||coalesce(elu.lieu_naissance,'''')) as date_et_lieu_de_naissance,
            (elu.adresse||'' ''||elu.cp||'' ''||elu.ville) as adresse,
            candidat.nom as candidat_ou_liste
        FROM
            &DB_PREFIXEcomposition_bureau
                INNER JOIN &DB_PREFIXEaffectation
                    ON affectation.composition_scrutin = composition_bureau.composition_scrutin
                LEFT JOIN &DB_PREFIXEelu
                    ON elu.id=affectation.elu
                LEFT JOIN &DB_PREFIXEcandidat
                    ON candidat.id=affectation.candidat
                LEFT JOIN &DB_PREFIXEbureau
                    ON bureau.id = affectation.bureau
                LEFT JOIN &DB_PREFIXEposte
                    ON affectation.poste = poste.poste

        WHERE
            composition_bureau.id=''&idx''
            AND (
                bureau.id = composition_bureau.bureau
                OR bureau.code = ''T''
            )
            AND affectation.decision IS TRUE
        ) ALIAS1

        UNION ALL

        SELECT * FROM
        (
        SELECT
            poste.ordre as hidden_ordre,
            poste.libelle as fonction,
            (agent.nom||'' ''||agent.prenom) as nom_prenoms,
            '''' as date_et_lieu_de_naissance,
            (agent.adresse||'' ''||agent.cp||'' ''||agent.ville) as adresse,
            '''' as candidat_ou_liste
        FROM
            &DB_PREFIXEcomposition_bureau
                INNER JOIN &DB_PREFIXEcandidature
                    ON candidature.composition_scrutin = composition_bureau.composition_scrutin
                LEFT JOIN &DB_PREFIXEagent
                    ON agent.id=candidature.agent
                LEFT JOIN &DB_PREFIXEbureau
                    ON bureau.id = candidature.bureau
                LEFT JOIN &DB_PREFIXEposte
                    ON candidature.poste = poste.poste

        WHERE
            composition_bureau.id=''&idx''
            AND (
                bureau.id = composition_bureau.bureau
                OR bureau.code = ''T''
            )
            AND candidature.decision IS TRUE
        ) ALIAS2

        ) ALIAS3

        ORDER BY hidden_ordre, fonction, candidat_ou_liste
        '
    );
END $$;

