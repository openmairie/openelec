--

--
-- Data for Name: canton; Type: TABLE DATA; Schema: openelec; Owner: -
--
--INSERT INTO canton (id, code, libelle) VALUES (nextval('canton_seq'), 'T', 'Tous');


--
-- Data for Name: bureau; Type: TABLE DATA; Schema: openelec; Owner: -
--

--DO $$
--DECLARE
--    canton_tous canton.id%type;
--    circonscription_libreville circonscription.id%type;
--    collectivite_libreville om_collectivite.om_collectivite%type;
--BEGIN
--    SELECT id INTO canton_tous FROM canton WHERE libelle = 'Tous';
--    SELECT id INTO circonscription_libreville FROM circonscription WHERE libelle = 'CIRCONSCRIPTION DE LIBREVILLE';
--    SELECT om_collectivite INTO collectivite_libreville FROM om_collectivite WHERE libelle = 'LIBREVILLE';
--    INSERT INTO bureau (id, code, libelle, canton, circonscription, om_collectivite) VALUES (nextval('bureau_seq'), 'C', 'Centralisation', canton_tous, circonscription_libreville, collectivite_libreville);
--    INSERT INTO bureau (id, code, libelle, canton, circonscription, om_collectivite) VALUES (nextval('bureau_seq'), 'T', 'Tous Bureaux', canton_tous, circonscription_libreville, collectivite_libreville);
--END $$;
INSERT INTO arrets_liste (arrets_liste, om_collectivite) VALUES (
    nextval('arrets_liste_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle = 'LIBREVILLE'));

INSERT INTO reu_scrutin (id, om_collectivite, libelle, date_tour1, date_tour2, date_debut, arrets_liste) VALUES 
(
    nextval('reu_scrutin_seq'),
    (SELECT om_collectivite FROM om_collectivite WHERE libelle = 'LIBREVILLE'),
    'Municipales 2008', '2008-10-19', '2008-12-29', '2008-10-19',
    currval('arrets_liste_seq')
);
INSERT INTO composition_scrutin (id, libelle, scrutin, tour, date_tour, solde, convocation_agent, convocation_president) VALUES 
(
    nextval('composition_scrutin_seq'),
    'Municipales 2008 1er Tour',
    (SELECT reu_scrutin.id FROM reu_scrutin WHERE libelle='Municipales 2008'),
    '1',
    '2008-10-19',
    false, 'Salle des Pas Perdus le 18/10/2008 à 15 heures', 'Salle du conseil le 18/10/2008 à 20 heures'
),
(
    nextval('composition_scrutin_seq'),
    'Municipales 2008 2eme Tour',
    (SELECT reu_scrutin.id FROM reu_scrutin WHERE libelle='Municipales 2008'),
    '2',
    '2008-12-29',
    false, '25 Octobre 2008 Salle des Pas Perdus', '25 Octobre 2008 Salle du conseil'
);

--
-- Data for Name: candidat; Type: TABLE DATA; Schema: openelec; Owner: -
--

DO $$
DECLARE
    composition_scrutin_1 composition_scrutin.id%type;
    composition_scrutin_2 composition_scrutin.id%type;
BEGIN
    SELECT id INTO composition_scrutin_1 FROM composition_scrutin WHERE libelle = 'Municipales 2008 1er Tour';
    SELECT id INTO composition_scrutin_2 FROM composition_scrutin WHERE libelle = 'Municipales 2008 2eme Tour';
    INSERT INTO candidat (id, nom, composition_scrutin) VALUES (1, 'Durant Paul', composition_scrutin_1);
    INSERT INTO candidat (id, nom, composition_scrutin) VALUES (2, 'Dupont Pierre', composition_scrutin_1);
    INSERT INTO candidat (id, nom, composition_scrutin) VALUES (3, 'Dupont Pierre', composition_scrutin_2);
    INSERT INTO candidat (id, nom, composition_scrutin) VALUES (4, 'Durant Paul', composition_scrutin_2);
END $$;


--
-- Data for Name: elu; Type: TABLE DATA; Schema: openelec; Owner: -
--

INSERT INTO elu (id, nom, prenom, nomjf, date_naissance, lieu_naissance, adresse, cp, ville) VALUES (nextval('elu_seq'), 'LEMAIRE', 'paul', '', '2012-01-01', '', 'rue de la charité', '13200', 'ARLES');
INSERT INTO elu (id, nom, prenom, nomjf, date_naissance, lieu_naissance, adresse, cp, ville) VALUES (nextval('elu_seq'), 'LADJOINT', 'josette', '', '2012-01-01', '', 'rue du temple', '13200', 'ARLES');
INSERT INTO elu (id, nom, prenom, nomjf, date_naissance, lieu_naissance, adresse, cp, ville) VALUES (nextval('elu_seq'), 'LECONSEILLER', 'jules', '', '2012-01-01', '', 'rue de la liberté', '13200', 'ARLES');
INSERT INTO elu (id, nom, prenom, nomjf, date_naissance, lieu_naissance, adresse, cp, ville) VALUES (nextval('elu_seq'), 'POLO', 'Walter', '', '2012-01-01', '', 'impasse de la révolution', '13200', 'ARLES');


--
-- Data for Name: affectation; Type: TABLE DATA; Schema: openelec; Owner: -
--

DO $$
DECLARE
    composition_scrutin_1 composition_scrutin.id%type;
    composition_scrutin_2 composition_scrutin.id%type;
    bureau_1 bureau.id%type;
    bureau_2 bureau.id%type;
    elu_1 elu.id%type;
    elu_2 elu.id%type;
    elu_3 elu.id%type;
    elu_4 elu.id%type;
BEGIN
    SELECT id INTO composition_scrutin_1 FROM composition_scrutin WHERE libelle = 'Municipales 2008 1er Tour';
    SELECT id INTO composition_scrutin_2 FROM composition_scrutin WHERE libelle = 'Municipales 2008 2eme Tour';
    SELECT id INTO bureau_1 FROM bureau WHERE libelle = 'HOTEL DE VILLE';
    SELECT id INTO bureau_2 FROM bureau WHERE libelle = 'ECOLE MATERNELLE';
    SELECT id INTO elu_1 FROM elu WHERE nom = 'LEMAIRE';
    SELECT id INTO elu_2 FROM elu WHERE nom = 'LADJOINT';
    SELECT id INTO elu_3 FROM elu WHERE nom = 'LECONSEILLER';
    SELECT id INTO elu_4 FROM elu WHERE nom = 'POLO';
    INSERT INTO affectation (id, elu, composition_scrutin, periode, poste, bureau, note, decision, candidat) VALUES (nextval('affectation_seq'), elu_2, composition_scrutin_1, 'journee', 'PRESIDENT', bureau_1, '', true, 2);
    INSERT INTO affectation (id, elu, composition_scrutin, periode, poste, bureau, note, decision, candidat) VALUES (nextval('affectation_seq'), elu_3, composition_scrutin_1, 'journee', 'PRESIDENT SUPPLEANT', bureau_1, '', true, 2);
    INSERT INTO affectation (id, elu, composition_scrutin, periode, poste, bureau, note, decision, candidat) VALUES (nextval('affectation_seq'), elu_1, composition_scrutin_1, 'journee', 'PRESIDENT', bureau_2, '', true, 2);
    INSERT INTO affectation (id, elu, composition_scrutin, periode, poste, bureau, note, decision, candidat) VALUES (nextval('affectation_seq'), elu_4, composition_scrutin_1, 'journee', 'DELEGUE TITULAIRE', bureau_1, '', true, 1);
    INSERT INTO affectation (id, elu, composition_scrutin, periode, poste, bureau, note, decision, candidat) VALUES (nextval('affectation_seq'), elu_1, composition_scrutin_2, 'journee', 'PRESIDENT', bureau_2, 'transfert MUN08-1', false, 3);
    INSERT INTO affectation (id, elu, composition_scrutin, periode, poste, bureau, note, decision, candidat) VALUES (nextval('affectation_seq'), elu_2, composition_scrutin_2, 'journee', 'PRESIDENT', bureau_1, 'transfert MUN08-1', false, 4);
    INSERT INTO affectation (id, elu, composition_scrutin, periode, poste, bureau, note, decision, candidat) VALUES (nextval('affectation_seq'), elu_4, composition_scrutin_2, 'journee', 'DELEGUE TITULAIRE', bureau_1, 'transfert MUN08-1', false, 3);
END $$;

--
-- Data for Name: grade; Type: TABLE DATA; Schema: openelec; Owner: -
--

INSERT INTO grade (grade, libelle) VALUES ('DT', 'Directeur Territorial');
INSERT INTO grade (grade, libelle) VALUES ('AB', 'Agent de bureau');

--
-- Data for Name: service; Type: TABLE DATA; Schema: openelec; Owner: -
--

INSERT INTO service (service, libelle) VALUES ('33500', 'Informatique');
INSERT INTO service (service, libelle) VALUES ('33510', 'Telecoms');

--
-- Data for Name: agent; Type: TABLE DATA; Schema: openelec; Owner: -
--

INSERT INTO agent (id, nom, prenom, adresse, cp, ville, telephone, service, telephone_pro, grade) VALUES (nextval('agent_seq'), 'MARTIN', 'Germaine', 'rue du temple', '13200', 'ARLES', '', '33500', '', 'AB');
INSERT INTO agent (id, nom, prenom, adresse, cp, ville, telephone, service, telephone_pro, grade) VALUES (nextval('agent_seq'), 'MAUSSAN', 'Pierre', 'rue de la mosquée', '13200', 'ARLES', '', '33500', '', 'AB');
INSERT INTO agent (id, nom, prenom, adresse, cp, ville, telephone, service, telephone_pro, grade) VALUES (nextval('agent_seq'), 'PINSON', 'Marie', 'rue de l eglise', '13200', 'ARLES', '', '33500', '', 'AB');
INSERT INTO agent (id, nom, prenom, adresse, cp, ville, telephone, service, telephone_pro, grade) VALUES (nextval('agent_seq'), 'GRAND', 'Paul', 'rue de la synagogue', '13200', 'ARLES', '', '33500', '', 'DT');

--
-- Data for Name: candidature; Type: TABLE DATA; Schema: openelec; Owner: -
--

DO $$
DECLARE
    composition_scrutin_1 composition_scrutin.id%type;
    composition_scrutin_2 composition_scrutin.id%type;
    agent_1 agent.id%type;
    agent_2 agent.id%type;
    agent_3 agent.id%type;
    agent_4 agent.id%type;
    bureau_1 bureau.id%type;
    bureau_2 bureau.id%type;
    periode_journee periode.periode%type;
    periode_matin periode.periode%type;
    periode_am periode.periode%type;
    poste_secretaire poste.poste%type;
    poste_planton poste.poste%type;
BEGIN
    SELECT id INTO composition_scrutin_1 FROM composition_scrutin WHERE libelle = 'Municipales 2008 1er Tour';
    SELECT id INTO composition_scrutin_2 FROM composition_scrutin WHERE libelle = 'Municipales 2008 2eme Tour';
    SELECT id INTO agent_1 FROM agent WHERE nom = 'MARTIN';
    SELECT id INTO agent_2 FROM agent WHERE nom = 'MAUSSAN';
    SELECT id INTO agent_3 FROM agent WHERE nom = 'PINSON';
    SELECT id INTO agent_4 FROM agent WHERE nom = 'GRAND';
    SELECT id INTO bureau_1 FROM bureau WHERE libelle = 'HOTEL DE VILLE';
    SELECT id INTO bureau_2 FROM bureau WHERE libelle = 'ECOLE MATERNELLE';
    SELECT periode INTO periode_journee FROM periode WHERE periode = 'journee';
    SELECT periode INTO periode_matin FROM periode WHERE periode = 'matin';
    SELECT periode INTO periode_am FROM periode WHERE periode = 'apres-midi';
    SELECT poste INTO poste_secretaire FROM poste WHERE poste = 'SECRETAIRE';
    SELECT poste INTO poste_planton FROM poste WHERE poste = 'PLANTON';
    INSERT INTO candidature (id, agent, composition_scrutin, periode, poste, bureau, recuperation, note, decision, debut, fin) VALUES (nextval('candidature_seq'), agent_4, composition_scrutin_1, periode_journee, poste_secretaire, bureau_1, true, '1ère demande : SECRETAIRE au bureau 01 pendant  journee', true, '07:00:00', '23:00:00');
    INSERT INTO candidature (id, agent, composition_scrutin, periode, poste, bureau, recuperation, note, decision, debut, fin) VALUES (nextval('candidature_seq'), agent_1, composition_scrutin_1, periode_am, poste_planton, bureau_1, true, '1ère demande : PLANTON au bureau 01 pendant  apres-midi', true, '13:00:00', '22:00:00');
    INSERT INTO candidature (id, agent, composition_scrutin, periode, poste, bureau, recuperation, note, decision, debut, fin) VALUES (nextval('candidature_seq'), agent_2, composition_scrutin_1, periode_am, poste_secretaire, bureau_2, true, '1ère demande : SECRETAIRE au bureau 02 pendant  apres-midi', true, '13:00:00', '22:00:00');
    INSERT INTO candidature (id, agent, composition_scrutin, periode, poste, bureau, recuperation, note, decision, debut, fin) VALUES (nextval('candidature_seq'), agent_3, composition_scrutin_1, periode_am, poste_planton, bureau_2, true, '1ère demande : PLANTON au bureau 02 pendant  apres-midi', true, '13:00:00', '22:00:00');
    INSERT INTO candidature (id, agent, composition_scrutin, periode, poste, bureau, recuperation, note, decision, debut, fin) VALUES (nextval('candidature_seq'), agent_2, composition_scrutin_2, periode_am, poste_secretaire, bureau_2, true, 'transfert MUN08-1', false, NULL, NULL);
    INSERT INTO candidature (id, agent, composition_scrutin, periode, poste, bureau, recuperation, note, decision, debut, fin) VALUES (nextval('candidature_seq'), agent_4, composition_scrutin_2, periode_journee, poste_secretaire, bureau_1, true, 'transfert MUN08-1', true, '07:00:00', '23:00:00');
    INSERT INTO candidature (id, agent, composition_scrutin, periode, poste, bureau, recuperation, note, decision, debut, fin) VALUES (nextval('candidature_seq'), agent_1, composition_scrutin_2, periode_am, poste_planton, bureau_1, true, 'transfert MUN08-1', true, '13:00:00', '22:00:00');
    INSERT INTO candidature (id, agent, composition_scrutin, periode, poste, bureau, recuperation, note, decision, debut, fin) VALUES (nextval('candidature_seq'), agent_3, composition_scrutin_2, periode_am, poste_planton, bureau_2, true, 'transfert MUN08-1', false, NULL, NULL);
END $$;

