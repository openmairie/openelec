*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Connexion REU initalisée - Procurations

*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    # On se connecte au REU et on initialise un statut valide
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence

Listing des procurations
    [Documentation]  Depuis le menu Consultation > Procuration, dans le formulaire de recherche avancée,
    ...  je sélectionne un bureau. En validant ma recherche, toutes les procurations rattachées à ce
    ...  bureau sont affichées.
    ...  En cliquant sur l'entête de la colonne bureau, mes procurations sont triées par ordre croissant.
    ...  En cliquant à nouveau, le tri est réalisé par ordre décroissant.
    ...  En cliquant sur l'icone csv je télécharge la liste des procurations affichées dans le tableau.


    Depuis la page d'accueil  admin  admin
    # Ajout de deux procuration sur des bureaux différents
    &{procuration} =  Create Dictionary
    ...  nom_mandant=BIG
    ...  nom_mandataire=DADOU
    ...  debut_validite=02/04/2014
    ...  fin_validite=15/04/2014
    ...  date_accord=15/08/2013
    ...  statut=demande_refusee
    ...  autorite_nom_prenom=Jean DUPONT
    ...  autorite_type=Police Nationale
    ...  autorite_commune=01011 - Apremont
    ...  motif_refus=Ceci est un motif de refus en bonne et due forme
    Ajouter une procuration  ${procuration}
    &{procuration} =  Create Dictionary
    ...  nom_mandant=ABEUDJE  #
    ...  nom_mandataire=DUPUY
    ...  debut_validite=02/04/2014
    ...  fin_validite=15/04/2014
    ...  date_accord=15/08/2013
    ...  statut=demande_refusee
    ...  autorite_nom_prenom=Jean DUPONT
    ...  autorite_type=Police Nationale
    ...  autorite_commune=01011 - Apremont
    ...  motif_refus=Ceci est un motif de refus en bonne et due forme
    Ajouter une procuration  ${procuration}

    #
    Go To Submenu In Menu  consultation  procurations
    Page Title Should Be  Électeur > Procuration
    Submenu In Menu Should Be Selected  consultation  procurations
    La page ne doit pas contenir d'erreur

    # Tri des colonnes
    Click Link  css=a[href*="tricol=3"]
    Wait Until Element Contains  css=tbody tr:nth-of-type(1)  1 HOTEL DE VILLE
    Wait Until Element Contains  css=tbody tr:nth-of-type(2)  3 SALLE DES FETES
    Click Link  css=a[href*="tricol=-3"]
    Wait Until Element Contains  css=tbody tr:nth-of-type(1)  3 SALLE DES FETES
    Wait Until Element Contains  css=tbody tr:nth-of-type(2)  1 HOTEL DE VILLE

    # Recherche simple
    Rechercher en recherche avancée simple  1 HOTEL DE VILLE
    Wait Until Element Does Not Contain  css=table.tab-tab  3 SALLE DES FETES
    Wait Until Element Contains  css=table.tab-tab  ABEUDJE - ALEXANDRE EHYGSENE - 04/08/1994 - 1 - LP - 2

    # Recherche avancé
    &{args_search} =  Create Dictionary
    ...  bureau=3 SALLE DES FETES
    Recherche avancée de procuration  ${args_search}
    Wait Until Element Contains  css=table.tab-tab  BIG - BOB - 01/01/1983 - 3 - LP - 7
    Wait Until Element Does Not Contain  css=table.tab-tab  1 HOTEL DE VILLE

    # Export csv
    ${link_export_listing}=  Get Element Attribute  css=.tab-export a  href
    ${output_dir}  ${output_name} =  Télécharger un fichier  ${SESSION_COOKIE}  ${link_export_listing}  ${EXECDIR}${/}binary_files${/}
    La page ne doit pas contenir d'erreur
    # Récupération du contenu du fichier pour vérifier si le filtre de la recherche avancée
    # est bien conservé
    ${full_path_to_file} =  Catenate  SEPARATOR=  ${output_dir}  ${output_name}
    ${content_file} =  Get File  ${full_path_to_file}
    Should Contain  ${content_file}   1;EF;"BIG - BOB - 01/01/1983 - 3 - LP - 7";"3 SALLE DES FETES";
    Should Not Contain  ${content_file}   2;EF;"ABEUDJE - ALEXANDRE EHYGSENE - 04/08/1994 - 1 - LP - 2";"1 HOTEL DE VILLE";

Refus procuration
    [Documentation]  Ce test case ajoute une procuration puis la refuse.
    ...  Prérequis: Dans la base on compte sur le fait qu'il y a
    ...  déjà une procuration avec les dates 01/04/2014 à 16/04/2014 pour les
    ...  mêmes personnes que celles utilisées ici.

    Depuis la page d'accueil  admin  admin

    # Création d'une procuration refusée
    &{procuration01} =  Create Dictionary
    ...  nom_mandant=BIG
    ...  nom_mandataire=DADOU
    ...  debut_validite=02/04/2014
    ...  fin_validite=15/04/2014
    ...  date_accord=15/08/2013
    ...  statut=demande_refusee
    ...  autorite_nom_prenom=Jean DUPONT
    ...  autorite_type=Police Nationale
    ...  autorite_commune=01011 - Apremont
    ...  motif_refus=Ceci est un motif de refus en bonne et due forme
    ${procuration01.id} =  Ajouter une procuration  ${procuration01}

    # Vérification de l'attestation de refus
    Depuis le contexte de la procuration  ${procuration01}
    Sleep  5
    Click On Form Portlet Action  procuration  edition-pdf-refusprocuration  new_window
    ${contenu_pdf} =  Create List  Objet : Vote par procuration
    ...  BIG - BOB
    ...  DADOU - DAVID
    ...  Ceci est un motif de refus en bonne et due forme
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # # Vérification des éditions par bureau
    # Go To Submenu In Menu  edition  menu-editions-par-bureau

    # WUX  Click Element  css=#action-edition-pdf-listeemargement-bureau-3
    # ${contenu_pdf} =  Create List  LISTE D'ÉMARGEMENT
    # dates de la 1re procuration qui était déjà en base
    # c'est celle qui faisait conflit avec celle qu'on à voulu créer
    # ...  01/04/2014  16/04/2014
    # ${ne_doit_pas_contenir} =  Create List  02/04/2014  # 2e procuration
    # Vérifier Que Le PDF Contient Des Strings Mais Pas D'Autres  ${OM_PDF_TITLE}  ${contenu_pdf}  ${ne_doit_pas_contenir}

    Go To Submenu In Menu  traitement  module-procuration
    Page Title Should Be  Traitement > Module 'Procurations'
    La page ne doit pas contenir d'erreur

    # Édition Registre Sur La Commune
    WUX  Click Element  css=#edition-pdf-registre-des-procurations-classees-par-mandant-01
    ${contenu_pdf} =  Create List  REGISTRE DES PROCURATIONS
    # on veut qu'il y ait les deux procurations dont celle refusée
    ...  Ceci est un motif
    ...  02/04/2014  15/04/2014
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Édition Listing Par Bureau
    Select from List By Label  css=#edition_pdf_listing_procurations_par_bureau #bureau  3 SALLE DES FETES
    Click Element  css=input[name="edition_pdf_listing_procurations_par_bureau.submit"]
    ${contenu_pdf} =  Create List  LISTE DES PROCURATIONS
    ${ne_doit_pas_contenir} =  Create List  02/04/2014  15/04/2014
    Vérifier Que Le PDF Contient Des Strings Mais Pas D'Autres  ${OM_PDF_TITLE}  ${contenu_pdf}  ${ne_doit_pas_contenir}

    # Édition Registre Par Bureau
    Select from List By Label  css=#edition_pdf_registre_procurations_par_bureau #bureau  3 SALLE DES FETES
    Click Element  css=input[name="edition_pdf_registre_procurations_par_bureau.submit"]
    ${contenu_pdf} =  Create List  REGISTRE DES PROCURATIONS
    ...  Ceci est un motif
    ...  02/04/2014  15/04/2014
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

Intégration
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  module-procuration
    Page Title Should Be  Traitement > Module 'Procurations'
    La page ne doit pas contenir d'erreur

Éditions
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  module-procuration
    Page Title Should Be  Traitement > Module 'Procurations'
    La page ne doit pas contenir d'erreur

    # Édition Registre Sur La Commune
    Click Element Until New Window  css=#edition-pdf-registre-des-procurations-classees-par-mandant-01
    ${contenu_pdf} =  Create List
    ...  REGISTRE DES PROCURATIONS
    ...  Commune : LIBREVILLE
    ...  LP - LISTE PRINCIPALE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Cette édition n'existe plus
    # # Édition Statistiques Sur La Commune
    # Click Element Until New Window  css=#edition-pdf-statistiques-procurations-01
    # ${contenu_pdf} =  Create List  Nombre total des procurations
    # Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Édition Registre Par Bureau
    Select from List By Label  css=#edition_pdf_registre_procurations_par_bureau #bureau  3 SALLE DES FETES
    Click Element Until New Window  css=input[name="edition_pdf_registre_procurations_par_bureau.submit"]
    ${contenu_pdf} =  Create List
    ...  REGISTRE DES PROCURATIONS
    ...  Commune : LIBREVILLE
    ...  Bureau : 3 - SALLE DES FETES
    ...  Canton : CANTON DE LIBREVILLE
    ...  Circonscription : CIRCONSCRIPTION DE LIBREVILLE
    ...  LP - LISTE PRINCIPALE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Édition Listing Par Bureau
    Select from List By Label  css=#edition_pdf_listing_procurations_par_bureau #bureau  2 ECOLE MATERNELLE
    Click Element Until New Window  css=input[name="edition_pdf_listing_procurations_par_bureau.submit"]
    ${contenu_pdf} =  Create List
    ...  LISTE DES PROCURATIONS
    ...  Liste LP - LISTE PRINCIPALE
    ...  Aucun enregistrement selectionne pour le bureau 2
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


Épuration
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  module-procuration
    Page Title Should Be  Traitement > Module 'Procurations'
    La page ne doit pas contenir d'erreur

    #
    Click Link  Épuration
    WUX  Click Element Until New Window  css=#action-traitement-election_epuration_procuration-statistiques
    ${contenu_pdf} =  Create List  Statistiques - Procurations à épurer  Épuration des procurations au
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Input Text  css=#dateelection  ${DATE_FORMAT_DD/MM/YYYY}
    Click Button  Épuration des procurations
    Handle Alert
    WUX  Valid Message Should Contain  Le traitement est terminé. Voir le détail
    La page ne doit pas contenir d'erreur

Erreurs PHP lors de l'utilisation des actions sur une procuration non existante
    [Documentation]  Test toutes les actions liées aux procurations sur une procuration non
    ...  existante pour vérifier que cela ne provoque pas d'erreur PHP.

    # TODO : les actions 302 et 304 ne sont pas testées car la vérification de l'existance de
    # la procuration provoque des erreurs dans les tests précédents. A tester et corriger plus tard.
    @{actions} =  Create List  0  1  2  3  21  22  23  63  201  401  301  303
    :FOR  ${action}  IN  @{actions}
    \    Go To  http://localhost/openelec/app/index.php?module=form&obj=procuration&idx=0&action=${action}&premier=0&retour=form
    \    La Page Ne Doit Pas Contenir D'erreur