*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Radiation.


*** Test Cases ***
Listing des radiations
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  consultation  radiations
    Page Title Should Be  Consultation > Radiation
    First Tab Title Should Be  Radiation
    Element Should Be Visible  css=#tab-radiation

Vérification de la présence du libelle lieu de naissance dans ajout de radiations
    # Création de trois électeurs pour faire des mouvements de radition
    # Pour la vérification de libelle de naissance il y a trois cas : commune francaise, étrangère identifiée
    # étrangère non identifié
    &{mouvement01} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST130ELECTEUR01
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    # ...  ne_en_france=13 105 - SENAS
    # ...  code_commune_de_naissance=13 105
    ...  libelle_commune_de_naissance=SENAS
    ...  libelle_voie=RUE BASSE
    ${mouvement01_id} =  Ajouter le mouvement d'inscription  ${mouvement01}
    #
    &{mouvement02} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST130ELECTEUR02
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né à l'étranger
    ...  pays_de_naissance=99134 - ROYAUME D'ESPAGNE
    ...  libelle_lieu_de_naissance=MADRID
    ...  libelle_voie=RUE BASSE
    ${mouvement02_id} =  Ajouter le mouvement d'inscription  ${mouvement02}

    &{mouvement03} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST130ELECTEUR03
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né à l'étranger
    ...  pays_de_naissance=99 - ETRANGER
    ...  libelle_lieu_de_naissance=MEXICO
    ...  libelle_voie=RUE BASSE
    ${mouvement03_id} =  Ajouter le mouvement d'inscription  ${mouvement03}
    #
    Appliquer le traitement de fin d'année

    #Radiation du premier electeur : commune francaise
    &{mouvement14} =  Create Dictionary
    ...  electeur_nom=${mouvement01.nom}
    ...  types=DECES
    #
    Go To Submenu In Menu  saisie  radiation
    Page Title Should Be  Saisie > Radiation
    #
    Input Text  nom  ${mouvement14.electeur_nom}
    Click Button  Rechercher l'électeur à radier
    Click Element  css=[title="Creer un mouvement de radiation"]
    Sleep  2
    #
    Input Text  css=#date_demande  ${DATE_FORMAT_DD/MM/YYYY}
    #
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement01.libelle_commune_de_naissance}
    Si "types" existe dans "${mouvement14}" on execute "Select From List By Label" dans le formulaire
    #
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Form Portlet Action  radiation  modifier
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement01.libelle_commune_de_naissance}

    #Radiation du deuxième electeur : commune étrangère identifiée
    &{mouvement15} =  Create Dictionary
    ...  electeur_nom=${mouvement02.nom}
    ...  types=DECES
    #
    Go To Submenu In Menu  saisie  radiation
    Page Title Should Be  Saisie > Radiation
    #
    Input Text  nom  ${mouvement15.electeur_nom}
    Click Button  Rechercher l'électeur à radier
    Click Element  css=[title="Creer un mouvement de radiation"]
    Sleep  2
    #
    Input Text  css=#date_demande  ${DATE_FORMAT_DD/MM/YYYY}
    #
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement02.libelle_lieu_de_naissance}
    Si "types" existe dans "${mouvement15}" on execute "Select From List By Label" dans le formulaire
    #
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Form Portlet Action  radiation  modifier
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement02.libelle_lieu_de_naissance}

    #Radiation du troisième electeur : commune étrangère non identifié
    &{mouvement16} =  Create Dictionary
    ...  electeur_nom=${mouvement03.nom}
    ...  types=DECES
    #
    Go To Submenu In Menu  saisie  radiation
    Page Title Should Be  Saisie > Radiation
    #
    Input Text  nom  ${mouvement16.electeur_nom}
    Click Button  Rechercher l'électeur à radier
    Click Element  css=[title="Creer un mouvement de radiation"]
    Sleep  2
    #
    Input Text  css=#date_demande  ${DATE_FORMAT_DD/MM/YYYY}
    #
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement03.libelle_lieu_de_naissance}
    Si "types" existe dans "${mouvement16}" on execute "Select From List By Label" dans le formulaire
    #
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Form Portlet Action  radiation  modifier
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement03.libelle_lieu_de_naissance}
    #

Recherche par date de naissance dans le listing des radiations
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  consultation  radiations
    # Aucun résultat
    Rechercher en recherche avancée simple  01/01/1754
    Page Should Contain  Aucun enregistrement.
    # Recherche générale avec une date complète
    Rechercher en recherche avancée simple  04/10/1994
    Page Should Contain  04/10/1994 à ABANCOURT
    Page Should Not Contain  18/03/1994 à MARSEILLE
    # Recherche générale avec une date tronquée
    Rechercher en recherche avancée simple  10/1994
    Page Should Contain  04/10/1994 à ABANCOURT
    Page Should Not Contain  18/03/1994 à MARSEILLE
    # Recherche générale avec l'année
    Rechercher en recherche avancée simple  1994
    Page Should Contain  04/10/1994 à ABANCOURT
    Page Should Contain  18/03/1994 à MARSEILLE
    Page Should Not Contain  14/03/1945 à LIMOGES
    # Recherche sur date de naissance avec une date tronquée
    Rechercher en recherche avancée simple  10/1994
    Page Should Contain  04/10/1994 à ABANCOURT
    Page Should Not Contain  18/03/1994 à MARSEILLE

