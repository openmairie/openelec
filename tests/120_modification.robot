*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Modification.

*** Test Cases ***
Saisie d'une modification de base
    [Documentation]  Saisie d'une modification dans le but de tester le cas de
    ...  base et aussi l'autocomplétion.

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  saisie  modification
    Page Title Should Be  Saisie > Modification
    Input Text  nom  MALBRANQUE
    Click Button  Rechercher l'électeur à modifier

    Click Element  css=[title="Creer un mouvement de modification"]

    # Type de mouvement/modification
    WUX  Select From List By Label  types  CHANGEMENT ADRESSE
    Input Text  css=#date_demande  ${DATE_FORMAT_DD/MM/YYYY}
    # Saisie de voie avec autocomplétion
    Click Element  css=#autocomplete-voie-empty
    Input Text  css=#autocomplete-voie-search  Rue basse
    WUX   Click On Link  css=[class*="autocomplete-voie-result-"]

    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    ${mouvement_id} =  Get Element Attribute  css=.form-content #id  value
    Page Title Should Be  Consultation > Modification > ${mouvement_id} - Mme MALBRANQUE BERNADETTE
    First Tab Title Should Be  Modification
    Submenu In Menu Should Be Selected  consultation  modifications
    Portlet Action Should Be In Form  modification  modifier
    Portlet Action Should Be In Form  modification  supprimer


Vérification de la présence du libelle lieu de naissance lors de l'ajout et modif de modifications

    [Documentation]  ...

    Depuis la page d'accueil  admin  admin

    # Lors de la création d'un moiuvement de modification à partir d'un électeur
    # les informations à saisir sur le lieu de naissance peuvent ne pas correspondre
    # aux standards d'aujourd'hui. Il existe donc une proposition "autre (Lieu de
    # naissance non référencé)" qui permet d'afficher les 4 champs bruts non
    # modifiables.
    Go To Submenu In Menu  saisie  modification
    Page Title Should Be  Saisie > Modification
    Input Text  nom  LACROIX
    Input Text  prenom  SOPHIE
    Click Button  Rechercher l'électeur à modifier
    Click Element  css=[title="Creer un mouvement de modification"]
    WUX  Selected List Label Should Be  css=#naissance_type_saisie  Lieu de naissance non référencé

    # Création de quatre électeurs (via quatre mouvements d'inscription) pour vérifier
    # le comportement du widget de formulaire des lieux de naissance (sélection du
    # cas + autocomplétion)
    &{mouvement01} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST120ELECTEUR01
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né en France
    ...  commune_de_naissance=13 105 - SENAS
    ...  libelle_voie=RUE BASSE
    ${mouvement01_id} =  Ajouter le mouvement d'inscription  ${mouvement01}
    &{mouvement02} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST120ELECTEUR02
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né à l'étranger
    ...  pays_de_naissance=99134 - ROYAUME D'ESPAGNE
    ...  libelle_lieu_de_naissance=MADRID
    ...  libelle_voie=RUE BASSE
    ${mouvement02_id} =  Ajouter le mouvement d'inscription  ${mouvement02}
    &{mouvement03} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST120ELECTEUR03
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né à l'étranger
    ...  pays_de_naissance=99 - ETRANGER
    ...  libelle_lieu_de_naissance=MEXICO
    ...  libelle_voie=RUE BASSE
    ${mouvement03_id} =  Ajouter le mouvement d'inscription  ${mouvement03}
    &{mouvement04} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=PREMIERE INSCRIPTION
    ...  nom=DURANDTEST120ELECTEUR04
    ...  prenom=JACQUES
    ...  date_naissance=10/01/1975
    ...  naissance_type_saisie=Né dans un ancien département français d'Algérie
    ...  ancien_departement_francais_algerie=91352 - ALGER
    ...  libelle_lieu_de_naissance=EL MADANIA
    ...  libelle_voie=RUE BASSE
    ${mouvement04_id} =  Ajouter le mouvement d'inscription  ${mouvement04}
    Appliquer le traitement de fin d'année

    #
    &{mouvement17} =  Create Dictionary
    ...  electeur_nom=${mouvement01.nom}
    ...  types=CHANGEMENT ADRESSE
    #
    Go To Submenu In Menu  saisie  modification
    Page Title Should Be  Saisie > Modification
    Input Text  nom  ${mouvement17.electeur_nom}
    #
    Click Button  Rechercher l'électeur à modifier
    Click Element  css=[title="Creer un mouvement de modification"]
    Sleep  2
    #
    Input Text  css=#date_demande  ${DATE_FORMAT_DD/MM/YYYY}
    #
    Form Value Should Be  css=#autocomplete-commune-naissance-search  ${mouvement01.commune_de_naissance}
    Si "types" existe dans "${mouvement17}" on execute "Select From List By Label" dans le formulaire
    #
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Form Portlet Action  modification  modifier
    Form Value Should Be  css=#autocomplete-commune-naissance-search  ${mouvement01.commune_de_naissance}
    #
    &{mouvement18} =  Create Dictionary
    ...  electeur_nom=${mouvement02.nom}
    ...  types=CHANGEMENT ADRESSE

    Go To Submenu In Menu  saisie  modification
    Page Title Should Be  Saisie > Modification
    Input Text  nom  ${mouvement18.electeur_nom}
    #
    Click Button  Rechercher l'électeur à modifier
    Click Element  css=[title="Creer un mouvement de modification"]
    Sleep  2
    #
    Input Text  css=#date_demande  ${DATE_FORMAT_DD/MM/YYYY}
    #
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement02.libelle_lieu_de_naissance}
    Si "types" existe dans "${mouvement18}" on execute "Select From List By Label" dans le formulaire
    #
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Form Portlet Action  modification  modifier
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement02.libelle_lieu_de_naissance}

    &{mouvement19} =  Create Dictionary
    ...  electeur_nom=${mouvement03.nom}
    ...  types=CHANGEMENT ADRESSE

    Go To Submenu In Menu  saisie  modification
    Page Title Should Be  Saisie > Modification
    Input Text  nom  ${mouvement19.electeur_nom}
    #
    Click Button  Rechercher l'électeur à modifier
    Click Element  css=[title="Creer un mouvement de modification"]
    Sleep  2
    #
    Input Text  css=#date_demande  ${DATE_FORMAT_DD/MM/YYYY}
    #
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement03.libelle_lieu_de_naissance}
    Si "types" existe dans "${mouvement19}" on execute "Select From List By Label" dans le formulaire
    #
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Form Portlet Action  modification  modifier
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement03.libelle_lieu_de_naissance}

    &{mouvement20} =  Create Dictionary
    ...  electeur_nom=DURANDTEST120ELECTEUR04
    ...  types=CHANGEMENT ADRESSE

    Go To Submenu In Menu  saisie  modification
    Page Title Should Be  Saisie > Modification
    Input Text  nom  ${mouvement20.electeur_nom}
    #
    Click Button  Rechercher l'électeur à modifier
    Click Element  css=[title="Creer un mouvement de modification"]
    Sleep  2
    #
    Input Text  css=#date_demande  ${DATE_FORMAT_DD/MM/YYYY}
    #
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement04.libelle_lieu_de_naissance}
    Si "types" existe dans "${mouvement20}" on execute "Select From List By Label" dans le formulaire
    #
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Form Portlet Action  modification  modifier
    Form Value Should Be  css=#libelle_lieu_de_naissance  ${mouvement04.libelle_lieu_de_naissance}

    Appliquer le traitement de fin d'année

Listing des modifications
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  consultation  modifications
    Page Title Should Be  Consultation > Modification
    First Tab Title Should Be  Modification
    Element Should Be Visible  css=#tab-modification


Recherche par date de naissance dans le listing des modifications
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  consultation  modifications
    # Aucun résultat
    Rechercher en recherche avancée simple  01/01/1754
    Page Should Contain  Aucun enregistrement.
    # Recherche générale avec une date complète
    Rechercher en recherche avancée simple  02/09/1990
    Page Should Contain  02/09/1990 à MARSEILLE 16
    Page Should Not Contain  30/10/1990 à MARTIGUES
    # Recherche générale avec une date tronquée
    Rechercher en recherche avancée simple  10/1990
    Page Should Contain  30/10/1990 à MARTIGUES
    Page Should Not Contain  02/09/1990 à MARSEILLE 16
    # Recherche générale avec l'année
    Rechercher en recherche avancée simple  1990
    Page Should Contain  30/10/1990 à MARTIGUES
    Page Should Contain  02/09/1990 à MARSEILLE 16
    Page Should Not Contain  12/01/1956 à ARQUES
    # Recherche sur date de naissance avec une date tronquée
    Rechercher en recherche avancée simple  10/1990
    Page Should Contain  30/10/1990 à MARTIGUES
    Page Should Not Contain  02/09/1990 à MARSEILLE 16

