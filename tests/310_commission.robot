*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite des commissions

*** Test Cases ***
Message d'erreur convocation commission sans membre

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  commission
    Click Link  Convocation
    WUX  Page Should Contain
    ...  Veuillez ajouter les membres participant à la commission (onglet "Membres").


Ajout membre

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  commission
    Click Link  Membres

    WUX  Click Link  Ajouter  # plus(+) vert dans le tableau
    WUX  Element Should Be Visible  css=#sousform-membre_commission input[name='nom']
    Input Text  nom  Engström
    Input Text  prenom  Joline
    Input Text  adresse  43 Place Charles de Gaulle
    Input Text  complement  Apt 25
    Input Text  cp  59650
    Input Text  ville  VILLENEUVE-D'ASCQ
    Click Button  css=#sousform-membre_commission input[name='submit']
    WUX  Page Should Contain  Vos modifications ont bien été enregistrées.

    Click On Back Button In Subform
    Page Should Not Contain  Apt 25  # check qu'on est bien sur la liste
    Page Should Contain  Joline

    WUX  Click Link  Ajouter  # plus(+) vert dans le tableau
    WUX  Element Should Be Visible  css=#sousform-membre_commission input[name='nom']
    Input Text  nom  Jankovic
    Input Text  prenom  Kazimir
    Input Text  adresse  72, boulevard Amiral Courbet
    Input Text  cp  69600
    Input Text  ville  OULLINS
    Click Button  css=#sousform-membre_commission input[name='submit']
    WUX  Page Should Contain  Vos modifications ont bien été enregistrées.


Génération PDF convocation

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  traitement  commission
    Click Link  Convocation

    WUX  Click Button  css=[value="Télécharger l'édition des courriers de convocation"]

    ${contenu_pdf} =  Create List  Joline  Kazimir  le ... à ...
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
