*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Module REU

*** Test Cases ***
Constitution du jeu de données
    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence
    # Initialisation de l'identifiant externe / id de la demande (pour notification)
    ${idDemande} =  Set Variable  600
    Set Suite Variable  ${idDemande}
    ${idElecteur} =  Set Variable  40023
    Set Suite Variable  ${idElecteur}
    Modifier l'identifiant externe de l'inscription  ${idDemande}

    # Ajout d'une inscription d'office
    Run  sed -i 's/"numeroElecteur": [0-9]*/"numeroElecteur": ${idElecteur}/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt
    Faire une recherche d'électeur REU par ine  ${idElecteur}
    WUX  Select Radio Button  choix_ine  TEST420NOMNAISSANCE;03/07/1950;${idElecteur}
    Click Element  name:inscription_search_form.action.valid
    # Si on se trouve sur le formulaire de vérification des doublons
    # clique sur le bouton permettant d'accéder au formulaire d'inscription
    ${recherche_doublon} =  Run Keyword And Return status  Page Should Contain  Électeur(s) avec inscription en cours
    Run Keyword If  ${recherche_doublon} == True  Click Element  name:inscription_doublon_form.action.valid

    &{mouvement_inscription_badge_warning} =  Create Dictionary
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  types=INSCRIPTION D'OFFICE
    ...  libelle_voie=RUE BASSE
    Saisir le mouvement d'inscription  ${mouvement_inscription_badge_warning}
    ${id_inscription_badge_warning} =  Get Value  css=#id
    Set Suite Variable  ${id_inscription_badge_warning}

Vérification du badge de warning et du badge danger sur une tache en cours

    Depuis la page d'accueil  admin  admin
    Page Should Contain Element  xpath://a//span[@id="task-mes_inscriptions_a_instruire-nb"][contains(@class, 'badge-warning')]

    Depuis le contexte de l'inscription  ${id_inscription_badge_warning}
    Compléter le mouvement d'inscription REU  ${DATE_FORMAT_DD/MM/YYYY}
    &{visa} =  Create Dictionary
    ...  select=accepté
    ...  date=${DATE_FORMAT_DD/MM/YYYY}
    Viser le mouvement d'inscription REU  ${visa}

    # On prépare la notification d'inscription d'office afin de valider le mouvement d'inscription précédemment ajouté
    Préparer la notification d'inscription d'office  ${idDemande}  ${idElecteur}  55  REFUSEE
    # On synchronise les notification pour récupérer celle que l'on vient de préparer
    Synchroniser les notifications
    # On traite la notification
    Traiter la notification  ${idDemande}
    #
    Depuis la page d'accueil  admin  admin
    # Le badge warning ne doit pas apparaître
    Page Should Not Contain Element  xpath://a//span[@id="task-mes_inscriptions_a_instruire-nb"][contains(@class, 'badge-warning')]
    Page Should Contain Element  xpath://a//span[@id="task-mes_inscriptions_a_instruire-nb"]
    # Vérification de la présence du badge danger
    Page Should Contain Element  xpath://a//span[@id="task-mes_inscriptions_refusees_insee-nb"][contains(@class, 'badge-danger')]


Vérification du marquage des taches acceptées et réfusées par INSEE comme vu
    #
    Depuis la page d'accueil  admin  admin
    Click Element  css=#task-mes_inscriptions_refusees_insee-nb

    WUX  Element Should Be Visible  css=#action-tab-inscription-left-consulter-${id_inscription_badge_warning}

    Click Element  css=#action-tab-inscription-left-consulter-${id_inscription_badge_warning}

    WUX  Portlet Action Should Be In Form  inscription  marquer_comme_vu

    Click On Form Portlet Action  inscription  marquer_comme_vu  modale

    WUX  Click Element  css=.ui-dialog .ui-dialog-buttonset .ui-button-text-only
    WUX  Valid Message Should Be  Vos modifications ont bien été enregistrées.
    Click On Back Button

    WUX  Element Should Not Be Visible  css=#action-tab-inscription-left-consulter-${id_inscription_badge_warning}


Déconsitution du jeu de données

    Run  sed -i 's/"numeroElecteur": [0-9]*/"numeroElecteur": 123456/' ${PATH_REU_RESOURCE_TEST_WF}ressource_test_electeur_ine.txt

    # Positionnement de l'état de l'environnement
    Remettre le fichier de notifications par défaut
    Modifier l'identifiant externe de l'inscription  114
    Désactiver le reu et mettre les fichiers par défaut


