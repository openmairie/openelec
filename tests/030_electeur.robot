*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite de recherche d'électeurs

*** Test Cases ***
Widget "Recherche dans la liste électorale"
    [Documentation]  Ce widget de tableau de bord est un raccourci pour chercher
    ...  rapidement un électeur dans la liste électorale. Les caractéristiques
    ...  sont :
    ...   - le focus au chargement de la page
    ...   - si on ne saisit aucune valeur et qu'on clique sur le bouton Recherche
    ...     on accède au listing
    ...   - le résultat de la recherche doit être chargé
    Depuis la page d'accueil  admin  admin
    #
    Element Should Be Focused  css=#dashboard_recherche
    La page ne doit pas contenir d'erreur
    # On clique sur le bouton sans saisir de valeur
    #
    Click Button  css=div.widget_recherche_listeelectorale #search-submit
    Page Title Should Be  Consultation > Liste Électorale
    First Tab Title Should Be  Électeur
    Submenu In Menu Should Be Selected  consultation  electeurs
    La recherche avancée simple doit être ouverte
    Element Should Be Focused  css=#adv-search-classic-fields input[name="recherche"]
    La page ne doit pas contenir d'erreur
    #
    Go To Dashboard
    Input Text  css=#dashboard_recherche  BIG
    Click Button  css=div.widget_recherche_listeelectorale #search-submit
    Page Title Should Be  Consultation > Liste Électorale
    First Tab Title Should Be  Électeur
    Submenu In Menu Should Be Selected  consultation  electeurs
    La recherche avancée simple doit être ouverte
    Element Should Be Focused  css=#adv-search-classic-fields input[name="recherche"]
    Element Should Be Visible  css=#action-tab-electeur-left-consulter-100034
    La page ne doit pas contenir d'erreur


Intégration "Consultation > Liste Électorale + Fiche électeur"
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    # En cliquant sur l'entrée de menu "Consultation > Liste Électorale"
    # on charge le listing des électeurs avec la recherche avancée ouverte
    # et la liste de travail courante sélectionnée
    Go To Submenu In Menu  consultation  electeurs
    Page Title Should Be  Consultation > Liste Électorale
    First Tab Title Should Be  Électeur
    Submenu In Menu Should Be Selected  consultation  electeurs
    La recherche avancée doit être ouverte
    Form Value Should Be  css=#liste  01
    La page ne doit pas contenir d'erreur
    # En accédant au listing depuis un autre lien, on chare le listing des
    # électeurs avec la recherche avancée simple ouverte et le focus sur
    # le champ de recherche
    Depuis le listing  electeur
    Page Title Should Be  Consultation > Liste Électorale
    First Tab Title Should Be  Électeur
    Submenu In Menu Should Be Selected  consultation  electeurs
    La recherche avancée simple doit être ouverte
    Element Should Be Focused  css=#adv-search-classic-fields input[name="recherche"]
    La page ne doit pas contenir d'erreur
    # Recherche l'électeur BIG
    Rechercher en recherche avancée simple  BIG
    Click Element  css=#action-tab-electeur-left-consulter-100034
    Page Title Should Be  Consultation > Liste Électorale > Électeur - Id : 100034
    First Tab Title Should Be  Électeur
    Submenu In Menu Should Be Selected  consultation  electeurs
    La page ne doit pas contenir d'erreur
    Click On Back Button
    Page Title Should Be  Consultation > Liste Électorale
    First Tab Title Should Be  Électeur
    Submenu In Menu Should Be Selected  consultation  electeurs
    La recherche avancée simple doit être ouverte

    Depuis la page d'accueil  multi  admin
    # Consultation → Liste électorale
    # Go To Submenu In Menu  consultation  electeurs
    Depuis le listing  electeur
    Page Title Should Be  Consultation > Liste Électorale
    First Tab Title Should Be  Électeur
    # Recherche l'électeur BIG
    Rechercher en recherche avancée simple  BIG
    Click Element  css=#action-tab-electeur-left-consulter-100034
    Page Title Should Be  Consultation > Liste Électorale > Électeur - Id : 100034
    First Tab Title Should Be  Électeur
    Submenu In Menu Should Be Selected  consultation  electeurs
    La page ne doit pas contenir d'erreur


Recherche électeurs
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    # Consultation → Liste électorale
    Depuis le listing  electeur
    # Recherche générale avec une date complète
    Rechercher en recherche avancée simple  10/03/1980
    Page Should Contain  HANANE INNOVA
    # Recherche générale avec une date tronquée
    Rechercher en recherche avancée simple  05/1968
    Page Should Contain  GEORGES
    Page Should Contain  JEAN
    # Recherche générale avec l'année
    Rechercher en recherche avancée simple  1994
    Page Should Contain  ABAY
    Page Should Contain  ABEUDJE
    Page Should Contain  DURAND
    Page Should Contain  FABRE
    # Recherche sur date de naissance avec une date tronquée
    Rechercher en recherche avancée simple  05/1968
    Page Should Contain  GEORGES
    Page Should Contain  JEAN

    # Recherche avancé sur la voie du bureau de rattachement de l'électeur
    &{args_recherche} =  Create Dictionary
    ...  adresse=RUE DE L'HOTEL DE VILLE
    Rechercher des electeurs avec la recherche avancée  ${args_recherche}
    Page Should Contain  ABEUDJE
    Page Should Contain  JEAN
    Page Should Contain  CARINE
    Page Should Contain  GEORGES

Recherche électeurs archivés
    [Documentation]  ...
    Depuis la page d'accueil  admin  admin
    # Go To Submenu In Menu  consultation  archive
    Depuis le listing  archive
    # Recherche générale avec une date tronquée
    Use Simple Search  Tous  03/1981
    Page Should Contain  TRAWINSKI
    # Recherche sur date de naissance avec une date tronquée
    Use Simple Search  Date de naissance  05/1984
    Page Should Contain  TESTOU

