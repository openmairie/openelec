*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suite des découpages

*** Test Cases ***
Créer une nouvelle circonscription

    Depuis la page d'accueil    admin    admin
    Depuis le listing des circonscriptions
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Découpage > Circonscriptions

    Input Text    xpath=//input[@name='code']       124
    Input Text    xpath=//input[@name='libelle']       CIRCONSCRIPTION DE TEST

    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be    Vos modifications ont bien été enregistrées.


Créer un nouveau canton

    Depuis la page d'accueil    admin    admin
    Depuis le listing des cantons
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Découpage > Cantons

    Input Text    xpath=//input[@name='code']       02
    Input Text    xpath=//input[@name='libelle']       CANTON DE TEST

    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    # Seul endroit dans les tests où on teste la déconnexion
    Go To    ${PROJECT_URL}
    Se déconnecter

Ajouter un bureau dans le nouveau canton et la nouvelle circonscription

    Depuis la page d'accueil    admin    admin
    # Ajout d'un bureau au nouveau canton
    Depuis le listing des bureaux
    Use Simple Search    Tous    HOTEL DE VILLE
    Click On Link  HOTEL DE VILLE
    Click On Form Portlet Action  bureau  modifier
    WUX  Select From List By Label    name=canton    02 CANTON DE TEST
    WUX  Select From List By Label    name=circonscription    CIRCONSCRIPTION DE TEST
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # Vérification de la création d'un paramétrage de jury supplémentaire
    Go To Submenu In Menu    traitement    jury
    WUX  Element Should Contain     css=#traitement_jury_status ul    Canton 01 - CANTON DE LIBREVILLE
    Element Should Contain     css=#traitement_jury_status ul    Canton 02 - CANTON DE TEST
    Page Should Contain Element    css=div[id="traitement_jury_status"] ul li    limit=2


Supprimer le bureau du nouveau canton

    Depuis la page d'accueil    admin    admin
    # Changement de canton
    Depuis le listing des bureaux
    Use Simple Search    Tous    HOTEL DE VILLE
    Click On Link  HOTEL DE VILLE
    Click On Form Portlet Action  bureau  modifier
    Select From List By Label    name=canton    01 CANTON DE LIBREVILLE
    Select From List By Label    name=circonscription    CIRCONSCRIPTION DE LIBREVILLE
    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # Vérification de la suppression du paramétrage de jury supplémentaire
    Go To Submenu In Menu    traitement    jury
    Element Should Contain     css=#traitement_jury_status ul    Canton 01 - CANTON DE LIBREVILLE
    Page Should Contain Element    css=div[id="traitement_jury_status"] ul li    limit=1


Créer une nouvelle voie

    Depuis la page d'accueil    admin    admin
    ${param} =  Create Dictionary
    ...  libelle=cp
    ...  valeur=35210
    Ajouter ou modifier le paramètre  ${param}
    Depuis le listing des voies
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Découpage > Voies

    Input Text    name=libelle_voie       AVENUE DU 8 MAI
    # Vérifie que le code postale et la voie sont automatiquement rempli
    # en récupérant les valeur issues des paramètres cp et ville
    Textfield Value Should Be    css=#cp       35210
    Textfield Value Should Be    css=#ville    LIBREVILLE

    Click On Submit Button Until Message  Vos modifications ont bien été enregistrées.
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    # Réinitialisation des paramètres
    ${param} =  Create Dictionary
    ...  libelle=cp
    ...  valeur=00000
    Ajouter ou modifier le paramètre  ${param}


Fusionner des voies
    [Documentation]  Le module redécoupage permet la fusion de voies existantes.
    Depuis la page d'accueil    admin    admin

    # Depuis l'onglet 'Initialisation' du module 'Redécoupage'
    Go To Submenu In Menu  traitement  redecoupage
    Page Title Should Be    Traitement > Module Redecoupage
    On clique sur l'onglet de traitement  decoupage_initialisation  Initialisation

    # Cas d'erreur n°1 : le champ libellé est obligatoire
    Click Element    name=traitement.decoupage_normalisation_voies.form.valid
    Handle Alert
    Error Message Should Contain    Veuillez renseigner le champ : Libelle de la nouvelle voie.

    # Cas d'erreur n°2 : la sélection de deux vois est obligatoire
    Click Element    css=#normalisation_voies>legend
    Sleep    1
    Input Text    name=new_label    AVENUE DU HUIT MAI
    Click Element    name=traitement.decoupage_normalisation_voies.form.valid
    Handle Alert
    Error Message Should Contain    Veuillez sélectionner 2 voies à fusionner.

    # Cas typique
    Click Element    css=#normalisation_voies>legend
    Sleep    1
    Select Checkbox    css=#AVENUE_DU_HUIT_MAI
    Select Checkbox    css=#AVENUE_DU_8_MAI
    Input Text    name=new_label    AVENUE DU HUIT MAI
    Click Element    name=traitement.decoupage_normalisation_voies.form.valid
    Handle Alert
    La page ne doit pas contenir d'erreur
    Valid Message Should Be    Le traitement est terminé. Voir le détail

Vérification de la suppression des découpages en double lors de la fusion des voies

    Depuis la page d'accueil    admin    admin
    # On ajoute deux voies
    &{voie_3} =  Create Dictionary
    ...  libelle_voie=TEST010DECOUPAGEDOUBLON1
    ...  cp=35210
    ...  ville=LIBREVILLE
    ${voie_id_3} =  Ajouter la voie  ${voie_3}

    &{voie_4} =  Create Dictionary
    ...  libelle_voie=TEST010DECOUPAGEDOUBLON2
    ...  cp=35210
    ...  ville=LIBREVILLE
    ${voie_id_4} =  Ajouter la voie  ${voie_4}

    # On ajoute un découpage identique sur chaque voie
    &{decoupage_1} =  Create Dictionary
    ...  bureau=3 SALLE DES FETES
    ...  code_voie=${voie_id_3}
    ${decoupage_id_1} =  Ajouter le découpage  ${decoupage_1}

    &{decoupage_2} =  Create Dictionary
    ...  bureau=3 SALLE DES FETES
    ...  code_voie=${voie_id_4}
    ${decoupage_id_2} =  Ajouter le découpage  ${decoupage_2}
    # On fusionne les voies
    Depuis la page d'accueil    admin    admin
    Fusionner les voies  TEST010DECOUPAGEDOUBLON1  TEST010DECOUPAGEDOUBLON2  VOIEFUSIONNÉE

    # On vérifie qu'il y a qu'un seul découpage
    Depuis le listing des decoupages de la voie  VOIEFUSIONNÉE
    Wait Until Element Contains  css=#sousform-decoupage  ${decoupage_id_2}
    Element Should Not Contain  css=#sousform-decoupage  ${decoupage_id_1}

Electeur bureau force

    Depuis la page d'accueil    admin    admin
    # Accès à l'initialisation du bureau forcé
    Go To Submenu In Menu    traitement    redecoupage
    Page Title Should Be    Traitement > Module Redecoupage
    Click Link    Initialisation
    La page ne doit pas contenir d'erreur
    Click Element    name=traitement.decoupage_init_bureauforce.form.valid
    Handle Alert
    La page ne doit pas contenir d'erreur
    Valid Message Should Contain    Le traitement est terminé. Voir le détail
    Valid Message Should Contain    6 électeur(s) modifié(s)
    Valid Message Should Contain    2 mouvement(s) modifié(s)


Listing des voies
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des voies
    First Tab Title Should Be  Voie
    Element Should Be Visible  css=#tab-voie


Édition PDF - Listing des voies
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des voies
    Click Element  css=div.tab-edition a span.print-25
    ${contenu_pdf} =  Create List  Liste des voies
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


Édition PDF - Listing des cantons
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des cantons
    Click Element  css=div.tab-edition a span.print-25
    ${contenu_pdf} =  Create List  Liste des cantons
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


Édition PDF - Listing des circonscriptions
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des circonscriptions
    Click Element  css=div.tab-edition a span.print-25
    ${contenu_pdf} =  Create List  Listing des circonscriptions  CIRCONSCRIPTION DE TEST
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


Édition PDF - Listing des bureaux
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des bureaux
    Click Element  css=div.tab-edition a span.print-25
    ${contenu_pdf} =  Create List  Liste des bureaux
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

Édition PDF - Listing des électeurs d'une voie
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des voies
    Click Link  AVENUE DE LA LIBERATION
    Page Title Should Be  Administration & Paramétrage > Découpage > Voies > AVENUE DE LA LIBERATION
    Portlet Action Should Be In Form  voie  edition-pdf-electeurparvoie
    Click On Form Portlet Action  voie  edition-pdf-electeurparvoie  new_window
    ${contenu_pdf} =  Create List  Electeurs sur la voie  AVENUE DE LA LIBERATION
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


Édition PDF - Listing des électeurs d'une entrée de découpage
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Depuis le listing des voies
    Click Link  AVENUE DE LA LIBERATION
    Page Title Should Be  Administration & Paramétrage > Découpage > Voies > AVENUE DE LA LIBERATION
    On clique sur l'onglet  decoupage  Decoupage
    Click Link  90001
    WUX  Portlet Action Should Be In SubForm  decoupage  edition-pdf-electeurpardecoupage
    Click On SubForm Portlet Action  decoupage  edition-pdf-electeurpardecoupage  new_window
    ${contenu_pdf} =  Create List  Electeurs sur le decoupage  AVENUE DE LA LIBERATION
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

