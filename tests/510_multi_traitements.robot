*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Traitements MULTI.


*** Test Cases ***
Constitution d'un jeu de données
    Remettre les réponses par défaut
    Réinitialiser les bureaux de vote REU
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    # Commune LIBREVILLE
    Depuis la page d'accueil  admin  admin
    Changer la date de tableau  10/01/2014
    # Commune MULTI
    Depuis la page d'accueil  multi  admin
    Changer la date de tableau  10/01/2014
    # Commune GRANVILLE
    Ajouter la collectivité depuis le menu  GRANVILLE  mono
    Ajouter l'utilisateur   granville  nospam@openmairie.org  granville  admin  ADMINISTRATEUR  GRANVILLE
    Depuis la page d'accueil  granville  admin
    Changer la date de tableau  10/01/2014
    Depuis la page d'accueil  multi  admin
    ${bureau01} =  Create Dictionary
    ...  code=01
    ...  libelle=BUREAU
    ...  canton=01 CANTON DE LIBREVILLE
    ...  circonscription=CIRCONSCRIPTION DE LIBREVILLE
    ...  om_collectivite=GRANVILLE
    Ajouter le bureau de vote  ${bureau01}
    Ajouter le paramètre depuis le menu  ville  GRANVILLE  GRANVILLE
    Ajouter le paramètre depuis le menu  inseeville  14987  GRANVILLE
    Ajouter le paramètre depuis le menu  maire  Jacques ROBERT  GRANVILLE
    Ajouter le paramètre depuis le menu  maire_de  GRANVILLE  GRANVILLE


Date de tableau
    [Documentation]  ...
    #
    Depuis la page d'accueil  multi  admin
    #
    Element Should Contain  css=#dashboard p.datetableau  10/01/2014
    #
    Depuis la page d'accueil  admin  admin
    #
    Element Should Contain  css=#dashboard p.datetableau  10/01/2014
    Click Element  css=#dashboard p.datetableau a
    Page Title Should Be  Changement De La Date De Tableau
    Input Text  datetableau  10/01/2016
    Click Element  css=input[name='changedatetableau.action.valid']
    Page Title Should Be  Changement De La Date De Tableau
    Valid Message Should Be  La date de tableau est changée.
    Click On Back Button
    Element Should Contain  css=#dashboard p.datetableau  10/01/2016
    #
    Depuis la page d'accueil  multi  admin
    #
    Element Should Contain  css=#dashboard p.datetableau  10/01/2014
    Click Element  css=#dashboard p.datetableau a
    Page Title Should Be  Changement De La Date De Tableau
    Input Text  datetableau  10/01/2015
    Click Element  css=input[name='changedatetableau.action.valid']
    Page Title Should Be  Changement De La Date De Tableau
    Valid Message Should Be  La date de tableau est changée.
    Click On Back Button
    Element Should Contain  css=#dashboard p.datetableau  10/01/2015
    Depuis la page d'accueil  admin  admin
    #
    Element Should Contain  css=#dashboard p.datetableau  10/01/2015
    #
    Depuis la page d'accueil  multi  admin
    Click Element  css=#dashboard p.datetableau a
    Page Title Should Be  Changement De La Date De Tableau
    Input Text  datetableau  10/01/2014
    Click Element  css=input[name='changedatetableau.action.valid']
    Page Title Should Be  Changement De La Date De Tableau
    Valid Message Should Be  La date de tableau est changée.


Traitements Multi Collectivités
    [Documentation]  ...
    #
    Depuis la page d'accueil  multi  admin
    #
    Go To Submenu In Menu  traitement  menu-traitements-multi
    Page Title Should Be  Traitement > Module Multi Collectivités

    # Refonte
    Click Element  css=#trtmulti_form #selectall
    Click Button  css=#trtmulti_form input[name='trtmulti_form_0.action.valid']
    WUX  Select From List By Label  css=#trtmulti_form select[name='traitement']  Refonte
    WUX  Click Button  css=#trtmulti_form input[name='trtmulti_form_1.action.valid']
    WUX  Click Button  css=#trtmulti_form input[name='trtmulti_form_2.action.valid']
    WUX  Element Should Contain  css=#trtmulti_form div.message  Le traitement est terminé.


Module facturation
    [Documentation]  ...
    #
    Depuis la page d'accueil  multi  admin
    #
    Go To Submenu In Menu  traitement  menu-traitements-multi
    Page Title Should Be  Traitement > Module Multi Collectivités
    Click Link  Facturation
    #
    WUX  Page Should Contain  Ce module permet de générer les documents nécessaires à la facturation des communes. Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs.
    La page ne doit pas contenir d'erreur

    # Titres de recettes
    #
    Click Element  css=#facturation-titres-recettes-link
    ${contenu_pdf} =  Create List  DEMANDE DE TITRE DE RECETTE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Transfert des informations budgétaires
    #
    Go To Submenu In Menu  traitement  menu-traitements-multi
    Page Title Should Be  Traitement > Module Multi Collectivités
    Click Link  Facturation
    #
    WUX  Page Should Contain  Ce module permet de générer les documents nécessaires à la facturation des communes. Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs.
    La page ne doit pas contenir d'erreur
    Click Element  css=#facturation-transfert-budget-link
    WUX  Valid Message Should Contain  Le fichier a été exporté

    # Publipostage
    #
    Go To Submenu In Menu  traitement  menu-traitements-multi
    Page Title Should Be  Traitement > Module Multi Collectivités
    Click Link  Facturation
    #
    WUX  Page Should Contain  Ce module permet de générer les documents nécessaires à la facturation des communes. Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs.
    La page ne doit pas contenir d'erreur
    Click Element  css=#facturation-publipostage-link
    WUX  Valid Message Should Contain  Le fichier a été exporté


Module statistiques
    [Documentation]  ...
    #
    Depuis la page d'accueil  multi  admin
    #
    Go To Submenu In Menu  traitement  menu-traitements-multi
    Page Title Should Be  Traitement > Module Multi Collectivités
    Click Element  css=#module-multi-tab-statistiques

    WUX  Page Should Contain  Ce module permet de générer les documents nécessaires aux statistiques sur la saisie. Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs.
    La page ne doit pas contenir d'erreur

    # Statistique sur la saisie
    Click Element  css=#multi-statistiques-sur-la-saisie-link
    ${contenu_pdf} =  Create List  SAISIES SUR LE TABLEAU EN COURS
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Statistique sur la saisie par utilisateur
    Click Element  css=#multi-statistiques-sur-la-saisie-par-utilisateur-link
    ${contenu_pdf} =  Create List  STATISTIQUE À PROPOS DES SAISIES UTILISATEURS SUR LE TABLEAU EN COURS
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Statistique sur les inscriptions par sexe
    Click Element  css=#multi-statistiques-sur-les-inscriptions-par-sexe-link
    ${contenu_pdf} =  Create List  STATISTIQUES INSCRIPTIONS D'OFFICE ET INSCRIPTIONS PAR SEXE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Statistiques générales des mouvements
    Click Element  css=#multi-statistiques-generales-des-mouvements-link
    ${contenu_pdf} =  Create List  STATISTIQUES GÉNÉRALES DES MOUVEMENTS
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


Module inscriptions: ségregation voies
    [Documentation]  L'autocompeltion de la voie dans l'adresse de l'électeur
    ...  ne doit pas montrer des voies d'une autre commune.
    #
    Depuis la page d'accueil  multi  admin
    #
    Select From List By Label  collectivite  GRANVILLE

    Go To Submenu In Menu  saisie  inscription
    Input Text  css=#nom  Nakamoto
    Click On Submit Button

    # Numéro de voie pour être sûr de chercher celle de LIBREVILLE.
    # Dernière lettre du nom manquante pour que l'autocompeltion s'affiche.
    Input Text  css=#autocomplete-voie-search  5 BOULEVARD MAQUIS MARCEA
    WUX  Element Should Contain  css=.autocomplete-voie-noresult  Aucun résultat


Modules procuration : cloisonnement des électeurs
    [Documentation]  L'autocomplétion de l'électeur ne doit pas montrer des
    ...  électeurs d'une autre commune.

    Depuis la page d'accueil  multi  admin
    Select From List By Label  collectivite  GRANVILLE

    # Module procuration
    Go To Submenu In Menu  saisie  procuration
    Input Text  css=#autocomplete-electeur-mandant-search  DUPONT THOMAS
    WUX  Element Should Contain  css=.autocomplete-electeur-mandant-noresult  Aucun résultat
    Input Text  css=#autocomplete-electeur-mandataire-search  DUPONT THOMAS
    WUX  Element Should Contain  css=.autocomplete-electeur-mandataire-noresult  Aucun résultat


    Remettre le fichier de notifications par défaut
    Désactiver le reu et mettre les fichiers par défaut
    Remettre les réponses par défaut


Edition carte électoral à partir d'une date en mode multi
    [Documentation]  ...
    # Positionnement de l'état de l'environnement
    Copy File  ${PATH_REU_RESOURCE_TEST_PLUGIN}reu.inc.php  ..${/}dyn${/}
    Depuis la page d'accueil  admin  admin
    Initialiser un statut valide  Oui
    Synchroniser les listes électorales
    Synchroniser les lieux de naissances
    Synchroniser les tables de référence

    Run  rm -r ..${/}app${/}pdf
    Depuis la page d'accueil  multi  admin
    #
    Go To Submenu In Menu  traitement  menu-traitements-multi
    Page Title Should Be  Traitement > Module Multi Collectivités

    # Traitement d'édition des cartes électorales multi
    Click Element  css=#trtmulti_form #selectall
    Click Button  css=#trtmulti_form input[name='trtmulti_form_0.action.valid']
    WUX  Select From List By Label  css=#trtmulti_form select[name='traitement']  Nouvelles cartes electorales
    WUX  Click Button  css=#trtmulti_form input[name='trtmulti_form_1.action.valid']
    WUX  Select From List By Label  css=#liste  LCM - LISTE COMPLÉMENTAIRE MUNICIPALE
    WUX  Input Datepicker  date  01/01/2014
    WUX  Set Checkbox  css=#sans_changement_de_bureau  true
    WUX  Click Button  css=#trtmulti_form input[name='trtmulti_form_2.action.valid']
    WUX  Element Should Contain  css=#trtmulti_form div.message  Le traitement est terminé.
    La page ne doit pas contenir d'erreur
    ${href} =  Get Element Attribute  css=.om-prev-icon  href
    ${uid} =  Remove String  ${href}  http://localhost/openelec/app/index.php?module=form&snippet=file&uid=
    ${dossier1} =  Get Substring  ${uid}  0  2
    ${dossier2} =  Get Substring  ${uid}  0  4
    Run  mkdir ..${/}app${/}pdf
    Run  unzip ..${/}var${/}filestorage${/}${dossier1}${/}${dossier2}${/}${uid} -d ..${/}app${/}pdf
    @{file_list} =  List Files In Directory  ..${/}app${/}pdf  *LIBREVILLE-liste03.pdf
    ${file_pdf} =  Set Variable  @{file_list}
    # Run  firefox -new-window binary_files${/}pdf${/}${file_pdf}
    Go To  ${PROJECT_URL}${/}app${/}pdf${/}${file_pdf}
    # @{liste_window} =  Get Window Titles
    ${contenu} =  Create List  MARTINEZ
    Vérifier Que Le PDF Contient Des Strings  ${file_pdf}  ${contenu}  True
    Run  rm -r ..${/}app${/}pdf
    Depuis la page d'accueil  admin  admin
    Désactiver le reu et mettre les fichiers par défaut
    Remettre les réponses par défaut


Widget de statistiques electeurs en multi
    [Documentation]  On vérifie qu'en mode multi, les chiffres globaux apparaissent
    #Récu
    Depuis la page d'accueil  admin  admin
    ${nb_electeur_mono} =  Get Text  xpath://div[@class="card-body bloc-lp"]//div[@class="card-title nb-electeurs"]

    Depuis la page d'accueil  multi  admin
    Element Should Contain  xpath://div[@class="card-body bloc-lp"]//div[@class="card-title nb-electeurs"]  ${nb_electeur_mono}

