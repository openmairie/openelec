<?php
/**
 * Ce script contient la définition de la classe 'GeneralCommon'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Cette classe permet de tester unitairement les fonctions de l'application.
 */
abstract class GeneralCommon extends PHPUnit\Framework\TestCase {

    /**
     * Méthode lancée en début de traitement
     */
    function common_setUp() {
        date_default_timezone_set('Europe/Paris');
        echo ' = '.get_called_class().'.'.str_replace('test_', '', $this->getName())."\r\n";
    }

    /**
     * Méthode lancée en fin de traitement
     */
    function common_tearDown() {
    }

    /**
     *
     */
    protected function get_inst_om_application($login = null, $password = null) {
        if ($login === null && $password === null) {
            @$f = new openelec("anonym");
            $GLOBALS['f'] = $f;
            return $f;
        }
        $_POST["login"] = $login;
        $_POST["password"] = $password;
        $_POST["login_action_connect"] = true;
        @$f = new openelec("login_and_nohtml");
        $GLOBALS['f'] = $f;
        return $f;
    }

    /**
     * Méthode étant appelée lors du fail d'un test.
     *
     * @param $e Exception remontée lors du test
     * @return void
     */
    public function common_onNotSuccessfulTest(Throwable $e) {
        echo 'Line '.$e->getLine().' : '.$e->getMessage()."\r\n";
        parent::onNotSuccessfulTest($e);
    }

    /**
     * Test template
     */
    public function test_template() {
        // Instanciation de la classe *om_application*
        require_once "../obj/openelec.class.php";
        $_POST["login"] = "admin";
        $_POST["password"] = "admin";
        $_POST["login_action_connect"] = true;
        $f = new openelec("login_and_nohtml");
        $f->disableLog();
        // Test
        $this->assertEquals($f->authenticated, true);
        // Destruction de la classe *om_application*
        $f->logout();
        $f->__destruct();
    }

    /**
     * Test de la méthode reu_notification::add_procuration().
     */
    public function test_01_add_procuration() {
        // Instanciation de la classe *om_application*
        $f = $this->get_inst_om_application();
    
        // PARAMETRAGE
        // Connexion au REU de test
        copy('binary_files/reu_resource_test/plugin/reu.inc.php', '../dyn/reu.inc.php');
        // Rend la connexion au logiciel de test valide
        copy('binary_files/reu_resource_test/wf/ressource_test_reu_all_true.txt', 'reu_resource_test_wf/ressource_test_reu.txt');
        // Paramétrage des données de la session
        $_SESSION['collectivite'] = 1;
        $_SESSION['niveau'] = 1;
        $_SESSION['login'] = 'test_unit';
        // Instanciation du REU de test et utilisation de la méthode d'ajout des procurations
        $inst_reu_notif = $f->get_inst__om_dbform(array(
            "obj" => "reu_notification",
            "idx" => "]",
        ));
    
        // TEST
        $add_proc = $inst_reu_notif->add_procuration();
        // Test le retour de l'ajout pour s'assurer qu'il s'est bien effectué
        $this->assertTrue($add_proc);
    
        // RÉTABLISSEMENT DU PARAMÉTRAGE
        // Deconnexion du REU de test
        unlink('../dyn/reu.inc.php');
        // Rend la connexion au logiciel de test non valide
        copy('binary_files/reu_resource_test/fichier_txt/ressource_test_reu_all_false.txt', 'reu_resource_test_wf/ressource_test_reu.txt');
    
        // Destruction de la classe *om_application*
        $f->__destruct();
    }

    /**
     * Test de la méthode om_lettretype::activer().
     *
     * Ajoute une nouvelle lettretype avec la méthode om_lettretype::ajouter() et
     * vérifie que l'ajout s'est correctement effectué, c'est à dire que la méthode renvoie true.
     * Instancie cette lettretype, utilise la méthode om_lettretype::activer() et
     * vérifie que l'activation s'est bien faite, c'est à dire que la méthode renvoie true.
     */
    public function test_03_om_lettretype_activer() {
        // Instanciation de la classe *om_application*
        $f = $this->get_inst_om_application();
    
        // PARAMETRAGE
        // Ajout d'une lettretype
        $lettretype = $f->get_inst__om_dbform(array(
            "obj" => "om_lettretype",
            "idx" => "]",
        ));
        $has_succeeded = $lettretype->ajouter(array(
            'om_lettretype' =>  '100001',
            'om_collectivite' =>  1,
            'id' => "d'inscription",
            'libelle' => "d'inscription",
            'actif' => false,
            'orientation' => 'P',
            'format' => 'A4',
            'logo' =>  null,
            'logoleft' => 10,
            'logotop' => 28,
            'titre_om_htmletat' => '<p>Texte du titre</p>,',
            'titreleft' => 105,
            'titretop' => 25,
            'titrelargeur' => 95,
            'titrehauteur' => 10,
            'titrebordure' => 0,
            'corps_om_htmletatex' => '<p>Texte du corps</p>',
            'om_sql' => 1,
            'margeleft' => 10,
            'margetop' => 25,
            'margeright' => 10,
            'margebottom' => 25,
            'se_font' => 'helvetica',
            'se_couleurtexte' => '0-0-0',
            'header_om_htmletat' => '',
            'header_offset' => 10,
            'footer_om_htmletat' => "<p style='text-align: center; font-size: 8pt;'><em>Page &amp;numpage/&amp;nbpages</em></p>",
            'footer_offset' => 12
        ));
        $this->assertTrue($has_succeeded);

        // TEST
        // Activation de la lettretype
        $lettretype = $f->get_inst__om_dbform(array(
            "obj" => "om_lettretype",
            "idx" => "100001",
        ));
        $is_activated = $lettretype->activer(array());
        $this->assertTrue($is_activated);
    
        // RÉTABLISSEMENT DU PARAMÉTRAGE    
        // Destruction de la classe *om_application*
        $f->__destruct();
    }

    /**
     * Test de la méthode mouvement_electeur::getDataSubmit().
     *
     * TODO
     */
    public function test_04_getDataSubmit() {
        // Instanciation de la classe *om_application*
        $f = $this->get_inst_om_application();
    
        // PARAMETRAGE
        // Instanciation d'un mouvement_electeur
        $mvt_electeur = $f->get_inst__om_dbform(array(
            "obj" => "mouvement_electeur",
            "idx" => "]",
        ));
        // Préparation des paramètres
        $mvt_electeur->parameters = array(
            'idx' => ']',
            'nom' => 'test_nom',
            'exact' => 't',
            'prenom' => 'test_prenom',
            'datenaissance' => '01-01-1990',
            'marital' => 'marital_nom',
            'origin' => 'test_unitaire',
            'idxelecteur' => ''
        );


        // TEST
        // TNR : vérifie que l'appel de getDataSubmit avec un idxelecteur null et un idx n'étant
        // pas un nombre ne provoque pas d'erreur de base de données.
        $submitted_url = $mvt_electeur->getDataSubmit();
        $this->assertEquals(
            OM_ROUTE_FORM.'&obj=mouvement&amp;maj=&amp;nom=test_nom&amp;exact=t&amp;prenom=test_prenom&amp;datenaissance=01-01-1990&amp;marital=marital_nom&amp;origin=test_unitaire&amp;idxelecteur=',
            $submitted_url
        );

        // RÉTABLISSEMENT DU PARAMÉTRAGE    
        // Destruction de la classe *om_application*
        $f->__destruct();
    }
}
