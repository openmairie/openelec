{  
  "result" :[
    {
      "id": 14,
      "dateCreation": "26/03/2019T18:00:38",
      "libelle": "Modification état civil TEST",
      "idDemande": "260",
      "idElecteur": 852963,
      "lu": false,
      "lienUri": "test",
      "resultatNotification": {
        "code": "ACCEPTEE",
        "libelle": "ACCEPTEE"
      },
      "typeNotification": {
        "code": "MODEC",
        "libelle": "Modification d'état civil"
      },
      "ugle": {
        "code": "ugle",
        "libelle": "ugle"
      }
    }
  ],
  "code" : "200",
  "message" : "OK",
  "content_type": "application/json;charset=UTF-8"
}