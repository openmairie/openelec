{
  "result":[
    {
      "dateCollecte": "18/02/2019",
      "dateCreation": "18/02/2019T08:30:35",
      "id": 6,
      "idDemande": "230",
      "idElecteur": 794613,
      "libelle": "Inscription pour modificatif",
      "lienUri": "test",
      "lu": false,
      "resultatNotification": {
        "code": "ACCEPTEE",
        "libelle": "ACCEPTEE"
      },
      "typeNotification": {
        "code": "INS",
        "libelle": "INS"
      },
      "ugle": {
        "code": "ugle",
        "libelle": "ugle"
      }
    }
  ],
  "code" : "200",
  "message" : "OK",
  "content_type": "application/json;charset=UTF-8"
}