<?php
/**
 * Ce script définit la classe 'reu'.
 *
 * @package openelec
 * @version SVN : $Id$
 */


require_once "../obj/reu.class.php";
/**
 * Définition de la classe 'reu'.
 */
class reu_test extends reu_base  {

    /**
     *
     */
    var $path_reu_resource_test_wf = "../tests/reu_resource_test_wf/";

    /**
     * @return string|null
     */
    public function __construct(array $conf) {
        parent::__construct($conf);
    }

    /**
     * @return boolean
     */
    public function is_config_valid() {
        $is_config_valid = 'is_config_valid';
        return $this->read_ressources_tests_file($is_config_valid);
    }

    /**
     * @return boolean
     */
    public function is_api_up() {
        $is_api_up = 'is_api_up';
        return $this->read_ressources_tests_file($is_api_up);
    }

    /**
     * @return boolean
     */
    public function is_connexion_logicielle_valid() {
        $is_connexion_logicielle_valid = 'is_connexion_logicielle_valid';
        return $this->read_ressources_tests_file($is_connexion_logicielle_valid);
    }

    /**
     * @return boolean
     */
    public function logout_connexion_logicielle() {
        //
        if (isset($_SESSION['reu_logiciel_access_token']) === true) {
            unset($_SESSION['reu_logiciel_access_token']);
        }
        if (isset($_SESSION['reu_logiciel_access_exp_time']) === true) {
            unset($_SESSION['reu_logiciel_access_exp_time']);
        }
        if (isset($_SESSION['reu_logiciel_refresh_token']) === true) {
            unset($_SESSION['reu_logiciel_refresh_token']);
        }
        if (isset($_SESSION['reu_logiciel_refresh_exp_time']) === true) {
            unset($_SESSION['reu_logiciel_refresh_exp_time']);
        }
        return true;
    }

    public function get_bureaux_de_vote(){
        return $this->read_ressource_tests_file_tabs();
    }

    /**
     * @return array
     */
    public function get_actualites() {
        if($this->is_connexion_logicielle_valid() === true
            && $this->is_api_up() === true
            && $this->is_config_valid() === true
            && $this->get_ugle()!=null
            && $this->get_compte_logiciel()!=null){
            //
            if (isset($this->_actualites) === true) {
                return $this->_actualites;
            }
            $this->_actualites = array (
                                    'result' => 
                                        array (
                                            0 => 
                                                array (
                                                    'id' => 4,
                                                    'titre' => 'Test d\'actualité pour l\'interface reu',
                                                    'dateDebut' => '09/04/2018',
                                                    'texte' => 'Test d\'actualité pour l\'interface reu Test d\'actualité pour l\'interface reu Test d\'actualité pour l\'interface reu Test d\'actualité pour l\'interface reu')),
                                    'code' => 200,
                                    'content_type' => 'application/json;charset=UTF-8');
            return $this->_actualites;
        }
    }

    /**
     * @return boolean
     */
    public function read_ressources_tests_file($line_param){
        $lines = file($this->path_reu_resource_test_wf."ressource_test_reu.txt");
        foreach($lines as $line) {
            $line = explode(' ',$line); 
            if($line[0] == $line_param){
                $return_value = ($line[1] === 'true');
                return $return_value;
            }
        }
        return false;
    }


    public function get_liste_in_csv($liste = "LP") {
        if($liste === "LP"){
            $path = $this->path_reu_resource_test_wf."LP.csv";
            $file = file_get_contents($path);
            // Écriture du fichier sur le disque
            $nom_fichier = "export_".time().".csv"; 
            $metadata = array(
                "filename" => $nom_fichier,
                "size" => strlen($file),
                "mimetype" => "application/vnd.ms-excel",
            );
            return $this->f->storage->create_temporary($file, $metadata);
        }
        if($liste === "LCM"){
            $path = $this->path_reu_resource_test_wf."LCM.csv";
            $file = file_get_contents($path);
            // Écriture du fichier sur le disque
            $nom_fichier = "export_".time().".csv"; 
            $metadata = array(
                "filename" => $nom_fichier,
                "size" => strlen($file),
                "mimetype" => "application/vnd.ms-excel",
            );
            return $this->f->storage->create_temporary($file, $metadata);
        }
        if($liste === "LCE"){
            $path = $this->path_reu_resource_test_wf."LCE.csv";
            $file = file_get_contents($path);
            // Écriture du fichier sur le disque
            $nom_fichier = "export_".time().".csv"; 
            $metadata = array(
                "filename" => $nom_fichier,
                "size" => strlen($file),
                "mimetype" => "application/vnd.ms-excel",
            );
            return $this->f->storage->create_temporary($file, $metadata);
        }
    }

    public function get_nationalites(){
        $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_nationalites.txt");
        $file_decode = json_decode($file,true);
        return $file_decode;
    }


    public function read_ressource_tests_file_tabs(){
        $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_bureaux_de_vote.txt");
        $file_decode = json_decode($file,true);
        return $file_decode;
    }

    /**
     *
     */
    public function get_notifications($params = array()) {
        //
        $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_notifications.txt");
        $params = json_decode($file,true);
        return $params;
    }

    public function handle_inscription($params = array()) {
        if ($params['mode'] === 'add') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_add.txt");
            $response = json_decode($file,true);
        } else if($params['mode'] === 'edit') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        } else if($params['mode'] === 'abandonner'){
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        } else if($params['mode'] === 'attendre') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        } else if($params['mode'] === 'completer'){
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        } else if($params['mode'] === 'instruire'){
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        } else if ($params['mode'] === 'viser') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        } elseif ($params["mode"] == "get") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_inscription.txt");
            $response = json_decode($file,true);
        } 
        return $response;
    }

    public function handle_radiation($params = array()) {
        if ($params['mode'] === 'add') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_add.txt");
            $response = json_decode($file,true);
        } else if($params['mode'] === 'batch') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_add.txt");
            $response = json_decode($file,true);
        } else if($params['mode'] === 'abandonner'){
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        }else if ($params['mode'] === 'viser') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        } elseif ($params["mode"] == "get" && $params["id"] != "201" && $params["id"]!="220" && $params["id"]!="701") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_radiation.txt");
            $response = json_decode($file,true);
        } elseif ($params["mode"]== "get" && $params["id"] == "201" || $params["id"]=="220" || $params["id"]=="701"){
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_radiation_insee.txt");
            $response = json_decode($file,true);
        }
        return $response;
    }

    public function handle_electeur($params = array()) {
        if ($params['mode'] === 'search_by_ine') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_electeur_ine.txt");
            $response = json_decode($file,true);
        } else if($params['mode'] === 'search_by_etat_civil') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_electeur_etat_civil.txt");
            $response = json_decode($file,true);
        } else if($params["mode"] === "get" && $params["ine"] != "789456") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_electeur_ine.txt");
            $response = json_decode($file,true);
        } else if($params["mode"] === "get" && $params["ine"] == "789456") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_inscription.txt");
            $response = json_decode($file,true);
        } else if($params["mode"] === "edit") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        }
        return $response;
     }

    /**
     *
     */
    public function handle_scrutin($params = array()) {
        if($params["mode"] === "get") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_scrutin_get.txt");
            $response = json_decode($file,true);
        } else if($params["mode"] === "list") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_scrutin_list.txt");
            $response = json_decode($file,true);
            $dtime = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
            $date_tour1_j20 = date('d/m/Y',strtotime("+23 days", strtotime($dtime->format('Y-m-d'))));
            $date_tour1_j5 = date('d/m/Y',strtotime("+6 days", strtotime($dtime->format('Y-m-d'))));
            $date_tour1_no_action = date('d/m/Y',strtotime("+7 days", strtotime($dtime->format('Y-m-d'))));
            $response[0]["result"][0]["tour1"] = $date_tour1_j20;
            $response[0]["result"][1]["tour1"] = $date_tour1_j5;
            $response[0]["result"][1]["tour2"] = $date_tour1_j5;
            $response[0]["result"][2]["tour1"] = $date_tour1_no_action;
        }
        return $response;
     }

     /**
     *
     */
    public function handle_livrable($params = array()) {
        if($params["mode"] === "add" && $params["type_livrable"] == "COMJ-20") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_livrable_j20.txt");
            $response = json_decode($file,true);
        } elseif($params["mode"] === "get" && array_key_exists("id", $params)) {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_demande_livrable.txt");
            $response = json_decode($file,true);
        } elseif($params["mode"] === "add" && $params["type_livrable"] == "MOUVJ-5") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_livrable_j5.txt");
            $response = json_decode($file,true);
        } elseif($params["mode"] === "add" && $params["type_livrable"] == "EMARGE") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_livrable_emarge.txt");
            $response = json_decode($file,true);
        } elseif($params["mode"] === "add" && $params["type_livrable"] == "COM3112") {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_livrable_com3112.txt");
            $response = json_decode($file,true);
        } elseif($params["mode"] === "fichier" && $params["id_livrable"] == "345") {
            $file_zip = file_get_contents($this->path_reu_resource_test_wf."ressource_test_liste_arretee.zip");
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_reponse_liste_arretee.txt");
            $response = json_decode($file,true);
            $response["result"] = $file_zip;
        } elseif($params["mode"] === "fichier" && $params["id_livrable"] == "350") {
            $file_zip = file_get_contents($this->path_reu_resource_test_wf."ressource_test_liste_j5.zip");
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_reponse_liste_j5.txt");
            $response = json_decode($file,true);
            $response["result"] = $file_zip;
        } elseif($params["mode"] === "fichier" && $params["id_livrable"] == "355") {
            $file_zip = file_get_contents($this->path_reu_resource_test_wf."ressource_test_liste_emarge.zip");
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_reponse_liste_emarge.txt");
            $response = json_decode($file,true);
            $response["result"] = $file_zip;
        } elseif($params["mode"] === "fichier" && $params["id_livrable"] == "400") {
            $file_zip = file_get_contents($this->path_reu_resource_test_wf."ressource_test_wrong_liste_emarge.zip");
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_reponse_wrong_liste_emarge.txt");
            $response = json_decode($file,true);
            $response["result"] = $file_zip;
        } elseif($params["mode"] === "fichier" && $params["id_livrable"] == "3112") {
            $file_zip = file_get_contents($this->path_reu_resource_test_wf."ressource_test_liste_arretee_com3112.zip");
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_reponse_liste_arretee_com3112.txt");
            $response = json_decode($file,true);
            $response["result"] = $file_zip;
        }
        return $response;
    }

    /**
     * Simule la réponse de la méthode handle_procuration du connecteur du REU.
     * Renvoie un tableau. Ce tableau est récupérer à partir d'un fichier json pré-rempli.
     *
     * @param array tableau des paramètres envoyé à la méthode contenant notamment le mode
     * de gestion des procurations (get, add ou cancel).
     * Actuellement seul le get est implémenté.
     * @return array tableau des valeurs avec un format similaire à celui du REU
     */
    public function handle_procuration($params = array()) {
        $response = array();
        if ($params['mode'] === 'get') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_procuration_get.json");
            $response = json_decode($file, true);
        } elseif (array_key_exists("mode", $params) === true
            && $params["mode"] == "add") {
            // TODO
            $response = array();
        } elseif (array_key_exists("mode", $params) === true
            && $params["mode"] == "cancel"
            && array_key_exists("id", $params) === true) {
            // TODO
            $response = array();
        }

        return $response;
    }

    /**
     *
     */
    public function get_referentiel_lieu_in_csv($referentiel_type) {
        if(file_exists($this->path_reu_resource_test_wf."reponse_lieux_naissance_faux.txt")) {
            $path = $this->path_reu_resource_test_wf."reponse_lieux_naissance_faux.txt";
            $file = file_get_contents($path);
            $response = json_decode($file,true);
            return $response;
        }
        if($referentiel_type == "commune") {
            $path = $this->path_reu_resource_test_wf."lieux_naissance_commune.csv";
            $file = file_get_contents($path);
            $nom_fichier = "export_".time().".csv"; 
            $metadata = array(
                "filename" => $nom_fichier,
                "size" => strlen($file),
                "mimetype" => "application/vnd.ms-excel",
            );
            return $this->f->storage->create_temporary($file, $metadata);
        }
        if($referentiel_type == "pays") {
            $path = $this->path_reu_resource_test_wf."lieux_naissance_pays.csv";
            $file = file_get_contents($path);
            $nom_fichier = "export_".time().".csv"; 
            $metadata = array(
                "filename" => $nom_fichier,
                "size" => strlen($file),
                "mimetype" => "application/vnd.ms-excel",
            );
            return $this->f->storage->create_temporary($file, $metadata);
        }
        if($referentiel_type == "ugle") {
            $path = $this->path_reu_resource_test_wf."reu_referentiel_ugle.csv";
            $file = file_get_contents($path);
            $nom_fichier = "export_".time().".csv"; 
            $metadata = array(
                "filename" => $nom_fichier,
                "size" => strlen($file),
                "mimetype" => "application/vnd.ms-excel",
            );
            return $this->f->storage->create_temporary($file, $metadata);
        }
    }

    /**
     *
     */
    public function get_referentiel_reference($type) {
        if($type == "typejustificatif"){
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_type_justificatif.txt");
            $params = json_decode($file,true);
            return $params;   
        }
    }

    /**
     *
     */
    public function get_referentiel_ugle() {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_referentiel_ugle.txt");
            $params = json_decode($file,true);
            return $params;   
    }

    /**
     *
     */
    public function handle_bureau_de_vote($params = array()) {
        if ($params['mode'] === 'add') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_add.txt");
            $response = json_decode($file,true);
        } elseif ($params['mode'] === 'renumerotation') {
            $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_mouvement_response.txt");
            $response = json_decode($file,true);
        }
        return $response;
    }

    /**
     *
     */
    public function is_connexion_standard_valid() {
        return true;
    }

    public function get_liste_electeurs_sans_bureau() {
        $file = file_get_contents($this->path_reu_resource_test_wf."ressource_test_electeur_sans_bureau.txt");
        $response = json_decode($file,true);
        return $response;
    }
}
