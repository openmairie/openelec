*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Test suites de génération des PDF


*** Test Cases ***
PDFs fiche électeur

    Depuis la page d'accueil  admin  admin

    # Consultation → Liste électorale
    Go To Submenu In Menu  consultation  electeurs
    Click Link  ABAY

    Click Link  Attestation d'électeur
    ${contenu_pdf} =  Create List  ATTESTATION D'INSCRIPTION  M. ABAY BARAN
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    Click Link  Carte électorale
    ${contenu_pdf} =  Create List  M. ABAY BARAN
    # À part le nom de la fenêtre et la personne, il ne semble pas possible
    # d'arriver à vérifier que le PDF est ce celui de la carte d'électeur.
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


PDFs liste d'inscription

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  consultation  inscriptions

    Click Link  BONNET

    # Ouverture de l'attestation de la 1ere ligne
    WUX  Click On Form Portlet Action  inscription  edition-pdf-attestationinscription  new_window
    ${contenu_pdf} =  Create List  ATTESTATION D'INSCRIPTION  M. BONNET HERVE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Ouverture du refus
    WUX  Click On Form Portlet Action  inscription  edition-pdf-refusinscription  new_window
    ${contenu_pdf} =  Create List  REFUS D'INSCRIPTION  M. BONNET HERVE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


PDFs modifications

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  consultation  modifications

    #
    Click Link  DUPONT

    # Ouverture de l'attestation de la 1ere ligne
    WUX  Click On Form Portlet Action  modification  edition-pdf-attestationmodification  new_window
    ${contenu_pdf} =  Create List  AVIS DE MODIFICATION  M. DUPONT GILBERT
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Ouverture du refus de modification
    WUX  Click On Form Portlet Action  modification  edition-pdf-refusmodification  new_window
    ${contenu_pdf} =  Create List  REFUS DE MODIFICATION  M. DUPONT GILBERT
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


PDFs radiations

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  consultation  radiations

    #
    Click Link  AUBAGNE

    # Ouverture de l'attestation de la 1ere ligne
    WUX  Click On Form Portlet Action  radiation  edition-pdf-attestationradiation  new_window
    ${contenu_pdf} =  Create List  ATTESTATION DE RADIATION  M. AUBAGNE JEAN
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    # Ouverture du refus
    WUX  Click On Form Portlet Action  radiation  edition-pdf-refusradiation  new_window
    ${contenu_pdf} =  Create List  REFUS DE RADIATION  M. AUBAGNE JEAN
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

