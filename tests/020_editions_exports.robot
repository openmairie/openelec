*** Settings ***
Resource          resources/resources.robot
Suite Setup       For Suite Setup
Suite Teardown    For Suite Teardown
Documentation     Éditions & Exports

*** Test Cases ***
Éditions par bureau 	 
    
    Depuis la page d'accueil  admin  admin 	 
    # 	 
    Go To Submenu In Menu  edition  menu-editions-par-bureau 	 
    Page Title Should Be  Éditions Par Bureau 	 
    
    # On vérifie tout les PDFs du bureau 1 	 
    
    # WUX  Click Element  css=#action-edition-pdf-listeemargement-bureau-1 	 
    # ${contenu_pdf} =  Create List 	 
    # ...  LISTE D'ÉMARGEMENT 	 
    # ...  Commune : LIBREVILLE 	 
    # ...  Liste : LP - LISTE PRINCIPALE 	 
    # ...  Bureau : 1 - HOTEL DE VILLE 	 
    # ...  Canton : CANTON DE LIBREVILLE 	 
    # ...  Circonscription : CIRCONSCRIPTION DE LIBREVILLE 	 
    # Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf} 	 
    
    # Click Element  css=#action-edition-pdf-listeelectorale-bureau-1 	 
    # ${contenu_pdf} =  Create List 	 
    # ...  LISTE ÉLECTORALE 	 
    # ...  Commune : LIBREVILLE 	 
    # ...  Liste : LP - LISTE PRINCIPALE 	 
    # ...  Bureau : 1 - HOTEL DE VILLE 	 
    # ...  Canton : CANTON DE LIBREVILLE 	 
    # ...  Circonscription : CIRCONSCRIPTION DE LIBREVILLE 	 
    # Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf} 	 
    
    Click Element  css=#action-edition-pdf-etiquetteelecteur-bureau-1 	 
    # on check le numéro d'ordre, seule chose discriminante avec les autres PDFs 	 
    ${contenu_pdf} =  Create List  1 - 29 	 
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf} 	 
    
    # Click Element  css=#action-edition-pdf-commission-bureau-1 	 
    # ${contenu_pdf} =  Create List 	 
    # ...  ÉTAT POUR LA COMMISSION 	 
    # ...  Commune : LIBREVILLE 	 
    # ...  Bureau : 1 - HOTEL DE VILLE 	 
    # ...  Canton : CANTON DE LIBREVILLE 	 
    # ...  Circonscription : CIRCONSCRIPTION DE LIBREVILLE 	 
    # Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf} 	 



Éditions générales
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  edition  menu-editions-generales
    Page Title Should Be  Éditions Générales

    # Génération en avance pour ne pas avoir à attendre
    # WUX  Click Element  css=#edition-pdf-listeelectorale-generer
    WUX  Click Element  css=#edition-pdf-etiquetteselecteur-generer

    # #
    # WUX  Click Element  css=#edition-pdf-listeelectorale
    # ${contenu_pdf} =  Create List
    # ...  LISTE ÉLECTORALE
    # ...  Commune : LIBREVILLE
    # ...  Liste : LP - LISTE PRINCIPALE
    # Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    WUX  Click Element  css=#edition-pdf-etiquetteselecteur
    ${contenu_pdf} =  Create List
    # Pour être sûr que c'est la liste générale:
    ...  1 - 24   # numéro d'un electeur du bureau 1
    ...  2 - 23   # numéro d'un electeur du bureau 2
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Element  css=#edition-pdf-statistiques-generales-des-mouvements
    ${contenu_pdf} =  Create List  STATISTIQUES GÉNÉRALES DES MOUVEMENTS
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}



Cartes électorales
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    # Activation de l'affichage des codes barres sur les cartes d'électeur
    ${param} =  Create Dictionary
    ...  libelle=option_code_barre
    ...  valeur=true\npos_x=35\npos_y=35\nshow_bureau_code=true
    Ajouter ou modifier le paramètre  ${param}
    Go To Submenu In Menu  edition  carte_electorale
    Page Title Should Be  Éditions > Cartes Électorales
    #
    WUX  Click Element  css=#edition-pdf-carteelecteur-generer
    WUX  Click Element  css=#edition-pdf-carteelecteur
    ${contenu_pdf} =  Create List
    ...  CARTES ÉLECTORALES
    ...  Commune : LIBREVILLE
    ...  Liste : LP - LISTE PRINCIPALE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Element  css=input[name="carte_electorale_par_bureau.submit"]
    ${contenu_pdf} =  Create List
    ...  CARTES ÉLECTORALES
    ...  Commune : LIBREVILLE
    ...  Liste : LP - LISTE PRINCIPALE
    ...  Bureau : 1 - HOTEL DE VILLE
    ...  Canton : CANTON DE LIBREVILLE
    ...  Circonscription : CIRCONSCRIPTION DE LIBREVILLE
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}

    # Désactivation de l'affichage des codes barres sur les cartes d'électeur
    ${param} =  Create Dictionary
    ...  libelle=option_code_barre
    ...  valeur=false
    Ajouter ou modifier le paramètre  ${param}


Cartes électorales - Édition par bureau de vote
    [Documentation]  Depuis le menu Éditions > Cartes électorales, dans la partie Édition par bureau de vote,
    ...  récupère une édition triée par ordre alphabétique, une par date de naissance et une par voie de l'électeur.
    ...  Vérifie pour chacune des éditions qu'il n'y a pas de problème sur la page.

    Depuis la page d'accueil  admin  admin
    Go To Submenu In Menu  edition  carte_electorale
    Page Title Should Be  Éditions > Cartes Électorales

    Select From List By Label  tri  ORDRE ALPHABÉTIQUE
    WUX  Click Element  css=input[name="carte_electorale_par_bureau.submit"]
    La page ne doit pas contenir d'erreur
    Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur
    Close PDF

    Select From List By Label  tri  DATE DE NAISSANCE
    WUX  Click Element  css=input[name="carte_electorale_par_bureau.submit"]
    La page ne doit pas contenir d'erreur
    Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur
    Close PDF


    Select From List By Label  tri  VOIE
    WUX  Click Element  css=input[name="carte_electorale_par_bureau.submit"]
    La page ne doit pas contenir d'erreur
    Open PDF  ${OM_PDF_TITLE}
    La page ne doit pas contenir d'erreur
    Close PDF


Vérification de l'option d'affichage du lieu de naissance

    Depuis la page d'accueil  admin  admin
    ${param} =  Create Dictionary
    ...  libelle=option_carte_electorale_lieu_naissance
    ...  valeur=false
    Ajouter ou modifier le paramètre  ${param}
    Changer la date de tableau  10/01/2015

    Go To Submenu In Menu  consultation  electeurs
    Click Element  link:ABAY

    ${lieu_naissance} =  Create List
    ...  28
    ...  DREUX

    ${nom_electeur} =  Create List
    ...  ABAY

    Click On Form Portlet Action  electeur  edition-pdf-carteelecteur  new_window
    Vérifier Que Le PDF Contient Des Strings Mais Pas D'Autres  ${OM_PDF_TITLE}  ${nom_electeur}  ${lieu_naissance}

    # On met la valeur à true pour afficher le lieu de naissance
    ${param} =  Create Dictionary
    ...  libelle=option_carte_electorale_lieu_naissance
    ...  valeur=true

    Depuis la page d'accueil  admin  admin
    Ajouter ou modifier le paramètre  ${param}
    Go To Submenu In Menu  consultation  electeurs
    Click Element  link:ABAY
    Click On Form Portlet Action  electeur  edition-pdf-carteelecteur  new_window
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${lieu_naissance}


Statistiques
    [Documentation]
    #
    Depuis la page d'accueil  admin  admin
    #
    Go To Submenu In Menu  edition  statistiques
    Page Title Should Be  Statistiques
    #
    Click Link  Mouvements détails par bureau
    ${contenu_pdf} =  Create List  Statistiques
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Link  Mouvements détails par voie
    ${contenu_pdf} =  Create List  Statistiques
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Link  Électeurs par tranche d'âge / détails par bureau
    ${contenu_pdf} =  Create List  Statistiques
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Link  Électeurs par sexe / détails par bureau
    ${contenu_pdf} =  Create List  Statistiques
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Link  Électeurs par sexe / détails par nationalité
    ${contenu_pdf} =  Create List  Statistiques
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Link  Électeurs par sexe / détails par département de naissance
    ${contenu_pdf} =  Create List  Statistiques
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Link  Électeurs par sexe / détails par voie
    ${contenu_pdf} =  Create List  Statistiques
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}
    #
    Click Link  Mouvements inscrits office (8) par sexe / détails par bureau
    ${contenu_pdf} =  Create List  Statistiques
    Vérifier Que Le PDF Contient Des Strings  ${OM_PDF_TITLE}  ${contenu_pdf}


Requêtes Mémorisées
    [Documentation]  Ce test permet de vérifier qu'aucune requête mémorisée reqmo du menu
    ...  "Export" -> "Requêtes mémorisées" ne produit d'erreur de base de données.
    #
    Depuis la page d'accueil  admin  admin
    # On accède au listing des reqmo
    Depuis l'écran principal du module 'Reqmo'
    Page Title Should Be  Export > Requêtes Mémorisées
    Submenu In Menu Should Be Selected  edition  reqmo
    La page ne doit pas contenir d'erreur
    #
    @{reqmo_standards}  Create List  ${EMPTY}
    @{reqmo_specifics}  Create List
    ...  stat__nb_procuration_detail_sexe__par_bureau__pour_une_liste__a_une_date
    ...  stat__nb_procuration_detail_sexe__par_bureau__pour_une_liste__entre_deux_dates
    @{reqmo_filenames} =  List Directory  ../sql/pgsql/  *.reqmo.inc.php
    :FOR  ${reqmo_name}  IN  @{reqmo_filenames}
    \    ${reqmo_name}  ${_} =  Split Extension  ${reqmo_name}
    \    ${reqmo_name}  ${_} =  Split Extension  ${reqmo_name}
    \    ${reqmo_name}  ${_} =  Split Extension  ${reqmo_name}
    \    ${index} =  Get Index From List  ${reqmo_specifics}  ${reqmo_name}
    \    Run Keyword If  ${index} == -1
    \    ...  Append To List  ${reqmo_standards}  ${reqmo_name}

    # reqmo standards
    :FOR  ${ELEMENT}  IN  @{reqmo_standards}
        \  Log  ${ELEMENT}
        \  Depuis l'écran principal du module 'Reqmo'
        \  Run Keyword If  '${ELEMENT}' == '${EMPTY}'
        \  ...  Continue For Loop
        \  Click Element  css=#action-reqmo-${ELEMENT}-exporter
        \  Click On Submit Button In Reqmo

    # reqmo specifics
    # - stat_procuration_election_bureau
    #   (la non saisie de dates provoque une erreur de base de données)
    Depuis l'écran principal du module 'Reqmo'
    Click Element  css=#action-reqmo-stat__nb_procuration_detail_sexe__par_bureau__pour_une_liste__a_une_date-exporter
    WUX  Input Text  xpath=//input[@id='dateelection(YYYY-MM-JJ)']  2017-01-01
    Click On Submit Button In Reqmo
    # - stat_procuration_sexe
    #   (la non saisie de dates provoque une erreur de base de données)
    Depuis l'écran principal du module 'Reqmo'
    Click Element  css=#action-reqmo-stat__nb_procuration_detail_sexe__par_bureau__pour_une_liste__entre_deux_dates-exporter
    WUX  Input Text  xpath=//input[@id='datedebut(YYYY-MM-JJ)']  2017-01-01
    WUX  Input Text  xpath=//input[@id='datefin(YYYY-MM-JJ)']  2018-01-01
    Click On Submit Button In Reqmo


Éditions PDF
    [Documentation]  Ce test permet de vérifier qu'aucune édition PDF
    ...  '../sql/pgsql/*.pdf.inc.php' ne produit d'erreur de base de données.
    #
    Depuis la page d'accueil  admin  admin
    #
    @{pdf_standards}  Create List  ${EMPTY}
    @{pdf_specifics}  Create List  ${EMPTY}
    @{pdf_filenames} =  List Directory  ../sql/pgsql/  *.pdf.inc.php
    :FOR  ${pdf_name}  IN  @{pdf_filenames}
    \    ${pdf_name}  ${_} =  Split Extension  ${pdf_name}
    \    ${pdf_name}  ${_} =  Split Extension  ${pdf_name}
    \    ${pdf_name}  ${_} =  Split Extension  ${pdf_name}
    \    ${index} =  Get Index From List  ${pdf_specifics}  ${pdf_name}
    \    Run Keyword If  ${index} == -1
    \    ...  Append To List  ${pdf_standards}  ${pdf_name}

    # pdf standards
    :FOR  ${ELEMENT}  IN  @{pdf_standards}
        \  Log  ${ELEMENT}
        \  Run Keyword If  '${ELEMENT}' == '${EMPTY}'
        \  ...  Continue For Loop
        \  Go To  ${PROJECT_URL}/app/index.php?module=edition&obj=${ELEMENT}
        \  La page ne doit pas contenir d'erreur
        \  WUX  Page Should Contain  Page 1

