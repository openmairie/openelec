openElec
========

Avertissement
-------------

openElec est une application sensible, nécessitant un paramétrage précis.
Un mauvais paramétrage peut entrainer une révision incorrecte de
la liste électorale, des erreurs d'éditions ou de lien avec l'INSEE.
Ni l'équipe du projet openElec ni le chef de projet ne peuvent être tenus
pour responsables d'un éventuel dysfonctionnement comme ceci est précisé dans
la licence jointe. Vous pouvez, si vous le souhaitez, faire appel a un
prestataire spécialisé qui peut fournir support, hot-line, maintenance, et
garantir le fonctionnement en environnement de production.


Description
-----------

openElec est un logiciel de gestion des listes électorales. Il permet la
gestion complète des élections politiques, de l'inscription d'un électeur,
jusqu'à l'édition de sa carte électorale, l'édition des listes d'émargement,
le transfert à l'insee des nouvelles inscription et bien plus encore.


Documentation
-------------

Toutes les instructions pour l'installation, l'utilisation et la prise en main 
du logiciel dans la documentation en ligne :
http://docs.openmairie.org/?project=openelec&format=html


Licence
-------

Voir le fichier LICENSE.txt.

