<?php
/**
 * REQMO - stat__nb_carte_en_retour__par_bureau__pour_une_liste
 *
 * Export statistiques du nombre d'électeurs marqués avec une carte en retour par bureau de vote sur une liste en particulier.
 *
 * Contexte MONO uniquement.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

//
$reqmo['sql'] = sprintf(
    'SELECT electeur.bureau as bureau_id, bureau.code as bureau_code, bureau.libelle as bureau_libelle, count(*) FROM %1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id WHERE electeur.carte=1 AND electeur.liste=\'[choix_liste]\' AND electeur.om_collectivite=%2$s GROUP BY electeur.bureau, bureau.code, bureau.libelle ORDER BY bureau.code',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
