<?php
//$Id$ 
//gen openMairie le 29/04/2019 14:53

include "../gen/sql/pgsql/reu_scrutin.inc.php";

$tab_title = __("Scrutin");

//
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}
$tab_actions['corner']['sync_scrutin'] = array(
    'lien' => OM_ROUTE_FORM.'&amp;obj='.$obj.'&amp;action=101&amp;retour=tab',
    'id' => '',
    'lib' => sprintf(
        '<span class="om-icon om-icon-16 om-icon-fix loop-16" title="%s">%s</span>',
        __("Synchronisation des scrutins du REU."),
        __("synchroniser")
    ),
    'rights' => array('list' => array($obj, $obj.'_synchroniser'), 'operator' => 'OR'),
    'type' => 'action-direct',
    'ordre' => 10,
    "parameters" => array(
        "reu_sync_l_e_valid" => "done",
    ),
);

$tri = " ORDER BY reu_scrutin.date_debut DESC, reu_scrutin.id DESC";

// On positionne la couleur de fond en grisée pour les élections dans le passé
$case_election_in_the_past = "CASE WHEN reu_scrutin.date_fin < CURRENT_DATE THEN 'past' ELSE 'future' END";
$champAffiche[] = $case_election_in_the_past." as election_in_time ";
if (isset($options) !== true) {
    $options = array();
}
$options[] = array(
    "type" => "condition",
    "field" => $case_election_in_the_past,
    "case" => array(
        "0" => array(
            "values" => array("past", ),
            "style" => "election-in-the-past",
        ),
        "1" => array(
            "values" => array("future", ),
            "style" => "election-in-the-future",
        ),
    ),
);
