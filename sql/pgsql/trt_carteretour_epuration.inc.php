<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// CARTE RETOUR
$sql_nbcarteretour = sprintf(
    'SELECT * FROM %1$selecteur WHERE electeur.carte=1 AND electeur.om_collectivite=%2$s',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);
$sql_carteretour = sprintf(
    'UPDATE %1$selecteur SET carte=0, date_saisie_carte_retour = NULL WHERE carte=1 AND om_collectivite=%2$s',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);
