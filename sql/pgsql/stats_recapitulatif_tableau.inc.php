<?php
/**
 * $Id$
 *
 */

if (isset($row['code'])) {
    //
    $sqlB = sprintf(
        'SELECT
            count(electeur.id)
        FROM
            %1$selecteur
            LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
        WHERE
            electeur.liste=\'%3$s\'
            AND bureau.code=\'%4$s\'
            AND electeur.om_collectivite=%2$s
        ',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $row['code']
    );

    // ins j5
    $sqlIj5 = sprintf(
        'SELECT
            count(mouvement.id)
        FROM
            %1$smouvement
            INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
        WHERE
            param_mouvement.typecat=\'Inscription\'
            AND mouvement.liste=\'%3$s\'
            AND mouvement.date_tableau=\'%4$s\'
            AND mouvement.etat=\'trs\'
            AND mouvement.bureau_de_vote_code=\'%5$s\'
            and mouvement.om_collectivite=%2$s
        ',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $f->getParameter("datetableau"),
        $row["code"]
    );

    //  rad j5
    $sqlRj5 = sprintf(
        'SELECT
            count(mouvement.id)
        FROM
            %1$smouvement
            INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
        WHERE
            lower(param_mouvement.typecat)=\'radiation\'
            AND mouvement.liste=\'%3$s\'
            AND mouvement.date_tableau=\'%4$s\'
            AND mouvement.etat=\'trs\'
            AND mouvement.bureau_de_vote_code=\'%5$s\'
            AND mouvement.om_collectivite=%2$s',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $f->getParameter("datetableau"),
        $row["code"]
    );

    // ***
    $sqlI = sprintf(
        'SELECT
            count(mouvement.id)
        FROM
            %1$smouvement
            INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
        WHERE
            lower(param_mouvement.typecat)=\'inscription\'
            AND mouvement.liste=\'%3$s\'
            AND mouvement.date_tableau=\'%4$s\'
            AND mouvement.etat=\'actif\'
            AND mouvement.bureau_de_vote_code=\'%5$s\'
            AND mouvement.om_collectivite=%2$s',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $f->getParameter("datetableau"),
        $row["code"]
    );

    //
    $sqlM1 = sprintf(
        'SELECT 
            count(mouvement.id)
        FROM
            %1$smouvement
            INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
        WHERE 
            lower(param_mouvement.typecat)=\'modification\'
            AND mouvement.etat=\'actif\'
            AND mouvement.om_collectivite=%2$s
            AND mouvement.liste=\'%3$s\'
            AND mouvement.date_tableau=\'%4$s\'
            AND mouvement.bureau_de_vote_code <> mouvement.ancien_bureau_de_vote_code
            AND mouvement.bureau_de_vote_code=\'%5$s\'
        ',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $f->getParameter("datetableau"),
        $row["code"]
    );

    //
    $sqlM2 = sprintf(
        'SELECT
            count(mouvement.id)
        FROM
            %1$smouvement
            INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
        WHERE
            lower(param_mouvement.typecat)=\'modification\'
            AND mouvement.etat=\'actif\'
            AND mouvement.om_collectivite=%2$s
            AND mouvement.liste=\'%3$s\'
            AND mouvement.date_tableau=\'%4$s\'
            AND mouvement.bureau_de_vote_code <> mouvement.ancien_bureau_de_vote_code
            AND mouvement.ancien_bureau_de_vote_code=\'%5$s\'
        ',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $f->getParameter("datetableau"),
        $row["code"]
    );

    //
    $sqlR = sprintf(
        'SELECT
            count(mouvement.id)
        FROM
            %1$smouvement
            INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
        WHERE
            lower(param_mouvement.typecat)=\'radiation\'
            AND mouvement.liste=\'%3$s\'
            AND mouvement.date_tableau=\'%4$s\'
            AND mouvement.etat=\'actif\'
            AND mouvement.bureau_de_vote_code=\'%5$s\'
            AND mouvement.om_collectivite=%2$s
        ',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $f->getParameter("datetableau"),
        $row["code"]
    );
}

//
$query_electeurs_actuels_groupby_bureau = sprintf(
    'SELECT
        bureau.code as bureau,
        electeur.sexe as sexe,
        count(electeur.id) as total
    FROM
        %1$selecteur
        LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
    WHERE
        electeur.om_collectivite=%2$s
        AND electeur.liste=\'%3$s\'
    GROUP BY
        bureau.code,
        electeur.sexe
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

//
$query_inscriptions_radiations_stats_groupby_bureau = sprintf(
    'SELECT
        mouvement.bureau_de_vote_code as bureau,
        mouvement.date_tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe as sexe,
        count(*) as total
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        mouvement.om_collectivite=%2$s
        AND mouvement.liste=\'%3$s\'
        AND 
        (
            lower(param_mouvement.typecat)=\'inscription\'
            OR lower(param_mouvement.typecat)=\'radiation\'
        )
    GROUP BY
        mouvement.bureau_de_vote_code,
        mouvement.date_tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe
    ORDER BY
        mouvement.bureau_de_vote_code,
        mouvement.date_tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

//
$query_transferts_plus_stats_groupby_bureau = sprintf(
    'SELECT
        mouvement.bureau_de_vote_code as bureau,
        mouvement.date_tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe as sexe,
        count(*) as total
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'modification\'
        AND mouvement.om_collectivite=%2$s
        AND mouvement.liste=\'%3$s\'
        AND mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
    GROUP BY
        mouvement.bureau_de_vote_code,
        mouvement.date_tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

//
$query_transferts_moins_stats_groupby_bureau = sprintf(
    'SELECT
        mouvement.ancien_bureau_de_vote_code as bureau,
        mouvement.date_tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe as sexe,
        count(*) as total
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'modification\'
        AND mouvement.om_collectivite=%2$s
        AND mouvement.liste=\'%3$s\'
        AND mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
    GROUP BY
        mouvement.ancien_bureau_de_vote_code,
        mouvement.date_tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

//
$query_inscriptions_radiations_groupby_bureau = sprintf(
    'SELECT
        mouvement.bureau_de_vote_code as bureau,
        param_mouvement.typecat,
        mouvement.sexe as sexe,
        count(mouvement.id) as total
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        mouvement.om_collectivite=%2$s
        AND mouvement.liste=\'%3$s\'
        AND mouvement.date_tableau=\'%4$s\'
        AND
        (
            lower(param_mouvement.typecat)=\'inscription\'
            OR lower(param_mouvement.typecat)=\'radiation\'
        )
    GROUP BY
        mouvement.bureau_de_vote_code,
        param_mouvement.typecat,
        mouvement.sexe
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"],
    $datetableau
);

//
$query_transferts_plus_groupby_bureau = sprintf(
    'SELECT
        mouvement.bureau_de_vote_code as bureau,
        mouvement.sexe as sexe,
        count(mouvement.id) as total
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'modification\'
        AND mouvement.om_collectivite=%2$s
        AND mouvement.liste=\'%3$s\'
        AND mouvement.date_tableau=\'%4$s\'
        AND mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
    GROUP BY
        mouvement.bureau_de_vote_code,
        mouvement.sexe
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"],
    $datetableau
);

//
$query_transferts_moins_groupby_bureau = sprintf(
    'SELECT
        mouvement.ancien_bureau_de_vote_code as bureau,
        mouvement.sexe as sexe,
        count(mouvement.id) as total
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'modification\'
        AND mouvement.om_collectivite=%2$s
        AND mouvement.liste=\'%3$s\'
        AND mouvement.date_tableau=\'%4$s\'
        AND mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
    GROUP BY
        mouvement.ancien_bureau_de_vote_code,
        mouvement.sexe
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"],
    $datetableau
);
