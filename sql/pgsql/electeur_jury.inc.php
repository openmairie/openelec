<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Titre
$ent = " -> ".__("Jury d'assises");

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 12;

// Critere FROM de la requete
$table = sprintf(
    '%1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id LEFT JOIN %1$scanton ON bureau.canton=canton.id', // FROM
    DB_PREFIXE
);

// Critere select de la requete
$champAffiche = array(
    "electeur.id as \"".__("Id")."\"",
    "nom as \"".__("Nom")."\"",
    "prenom as \"".__("Prenom")."\"",
    "nom_usage as \"".__("Nom d'usage")."\"",
    "(to_char(date_naissance,'DD/MM/YYYY')||' ".__("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \"".__("Date et lieu de naissance")."\"",
    "(numero_habitation||' '||complement_numero||' '||libelle_voie) as \"".__("Adresse")."\"",
    "bureau.code as \"".__("Bureau")."\"",
    "canton.libelle as \"".__("Canton")."\"",
    "typecat as \"".__("En cours")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
   "electeur.id",
   "nom",
   "prenom"
);

// Critere where de la requete
$selection = " where electeur.om_collectivite=".intval($_SESSION["collectivite"])." ";
$selection .= " and electeur.liste = '".$_SESSION["liste"]."' ";
// Sélection de tous les jurés
$selection .= " AND electeur.jury != 0";

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

// Modifications de la requête d'affichage liées à l'activation du module de tirage au sort des
// jurés suppléants.
if ($f->is_option_enabled('option_module_jures_suppleants') === true) {
    // Affichage de la colonne jury pour distinguer les titulaires des suppléants
    $champAffiche[] = sprintf(
        "CASE
            WHEN jury = 1 THEN 'Titulaire'
            WHEN jury = 2 THEN 'Suppléant'
            ELSE CONCAT('', electeur.jury)
        END AS \"%s\"",
        __("Jury")
    );
    // Modification du tri pour organiser en premier par type de juré
    $tri = " order by jury, withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
}

if (isset($retourformulaire) && isset($idxformulaire)) {
    $tab_actions["corner"] = array();
    $tab_actions["left"] = array(
        "modifier" => array(
            "lien" => OM_ROUTE_SOUSFORM."&obj=electeur_jury&amp;idx=",
            "id" => "&amp;action=54&amp;objsf=".$obj."&amp;retourformulaire=".$retourformulaire."&amp;idxformulaire=".$idxformulaire,
            "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix edit-16\" title=\"".__("Modifier")."\">".__("Modifier")."</span>",
        ),
    );
    $tab_actions["content"] = $tab_actions["left"]["modifier"];
}

//
$formulaire = $table.".form";

/**
 * Options
 */
//
$options = array();
$option = array(
    "type" => "search",
    "display" => false,
);
array_push($options, $option);
//
$option = array(
    "type" => "condition",
    "field" => "typecat",
    "case" => array(
        "0" => array(
            "values" => array("modification", ),
            "style" => "modification-encours",
        ),
        "1" => array(
            "values" => array("radiation", ),
            "style" => "radiation-encours",
        ),
    ),
);
array_push($options, $option);
