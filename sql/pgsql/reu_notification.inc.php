<?php
/**
 * ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/reu_notification.inc.php";

//
if (isset($tab_actions) === true
    && is_array($tab_actions) === true
    && array_key_exists("corner", $tab_actions) === true
    && is_array($tab_actions["corner"]) === true
    && array_key_exists("ajouter", $tab_actions["corner"]) === true) {
    //
    unset($tab_actions["corner"]["ajouter"]);
}
$tab_actions["corner"]['sync_last_notifications'] = array(
    'lien' => OM_ROUTE_SOUSFORM.'&amp;obj='.$obj.'&amp;action=101&amp;advs_id=&amp;premiersf=0&amp;trisf=&amp;valide=&amp;retourformulaire=reu&amp;idxformulaire=reu&amp;retour=tab',
    'id' => '',
    'lib' => sprintf(
        '<span class="om-icon om-icon-16 om-icon-fix loop-16" title="%s">%s</span>',
        __("Synchronisation des dernières notifications du REU."),
        __("synchroniser")
    ),
    'rights' => array('list' => array($obj, $obj.'_synchroniser'), 'operator' => 'OR'),
    'type' => 'action-direct',
    'ordre' => 10,
    "parameters" => array(
        "reu_sync_l_e_valid" => "done",
    ),
);

$tab_actions['corner']['treat_all_notifications'] = array(
    'lien' => OM_ROUTE_SOUSFORM.'&amp;obj='.$obj.'&amp;action=102&amp;advs_id=&amp;premiersf=0&amp;trisf=&amp;valide=&amp;retourformulaire=reu&amp;idxformulaire=reu&amp;retour=tab',
    'id' => '',
    'lib' => sprintf(
        '<span class="om-icon om-icon-16 om-icon-fix treat-16" title="%s">%s</span>',
        __("Traitement de toutes les notifications."),
        __("traiter toutes les notifications")
    ),
    'rights' => array('list' => array($obj, $obj.'_traiter_tout'), 'operator' => 'OR'),
    'type' => 'action-direct',
    'ordre' => 20,
    "parameters" => array(
        "reu_sync_l_e_valid" => "done",
    ),
);

// FROM
// XXX pourquoi le générateur n'a pas fait ça ?
$table = sprintf(
    '%1$sreu_notification
        LEFT JOIN %1$som_collectivite
            ON reu_notification.om_collectivite=om_collectivite.om_collectivite',
    DB_PREFIXE
);

//
$tri = " ORDER BY reu_notification.id DESC ";


if (in_array($retourformulaire, array("procuration", )) === true) {
    $tab_actions["corner"] = array();
    $table .= sprintf(
        ' INNER JOIN %1$sprocuration ON reu_notification.id_demande=procuration.id_externe ',
        DB_PREFIXE
    );
    $selection = sprintf(
        ' WHERE procuration.id = %1$s',
        intval($idxformulaire)
    );
}
