<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
//
require "../sql/pgsql/procuration_common_pdf.inc.php";

/**
 *
 */
//-------------------------- titre----------------------------------------------
$libtitre = "Procuration(s) non active(s) le";
//
if ($dateelection1 != NULL && $dateelection2 != NULL) {
    $libtitre .= " ".$f->formatdate($dateelection1);
    $libtitre .= " ".__("et le");
    $libtitre .= " ".$f->formatdate($dateelection2);
} else {
    if ($dateelection1 != NULL) {
        $libtitre .= " ".$f->formatdate($dateelection1);
    } else {
        $libtitre .= " ".$f->formatdate($dateelection2);
    }
}

//--------------------------SQL-------------------------------------------------
//
$liste = $_SESSION["liste"];
$libelle_liste = $_SESSION["libelle_liste"];
if (array_key_exists("liste", $params) === true) {
    $liste = $params["liste"];
    $ret = $this->f->get_one_result_from_db_query(sprintf(
        'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
        DB_PREFIXE,
        $this->f->db->escapesimple($liste)
    ));
    $libelle_liste = $ret["result"];
}
//
$sql = sprintf(
    'SELECT 
        to_char(procuration.debut_validite,\'DD/MM/YYYY\')||\' au \'||to_char(procuration.fin_validite,\'DD/MM/YYYY\') as validite, 
        (mandant.nom||\' \'|| mandant.prenom) as mandant, 
        bureau_mandant.code as bv1, 
        liste_mandant.liste_insee as liste1, 
        CASE WHEN procuration.mandataire IS NOT NULL
            THEN (mandataire.nom||\' \'|| mandataire.prenom) 
            ELSE concat(procuration.mandataire_infos::json->\'etatCivil\'->>\'nomNaissance\', \' \', procuration.mandataire_infos::json->\'etatCivil\'->>\'prenoms\')
        END as mandataire, 
        CASE WHEN procuration.mandataire IS NOT NULL
            THEN bureau_mandataire.code
            ELSE \'HC\'
        END as bv2, 
        CASE WHEN procuration.mandataire IS NOT NULL
            THEN liste_mandataire.liste_insee
            ELSE \'HC\'
        END as liste2 
    FROM 
        %1$sprocuration
        INNER JOIN %1$selecteur AS mandant ON procuration.mandant=mandant.id 
        LEFT JOIN %1$sbureau AS bureau_mandant ON mandant.bureau=bureau_mandant.id
        LEFT JOIN %1$sliste as liste_mandant ON mandant.liste=liste_mandant.liste
        LEFT JOIN %1$selecteur AS mandataire ON procuration.mandataire=mandataire.id
        LEFT JOIN %1$sbureau AS bureau_mandataire ON mandataire.bureau=bureau_mandataire.id
        LEFT JOIN %1$sliste as liste_mandataire ON mandataire.liste=liste_mandataire.liste
    WHERE
        procuration.om_collectivite=%2$s
        AND mandant.liste=\'%3$s\'
        AND procuration.statut = \'procuration_confirmee\'
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $liste
);
// Gestion des dates de validite
$sql .= " AND ";
$sql .= " ( ";
//
if ($dateelection1 != NULL && $dateelection2 != NULL) {
    //
    $sql .= " (procuration.debut_validite >'".$dateelection1."' ";
    $sql .= " AND ";
    $sql .= " procuration.debut_validite >'".$dateelection2."') ";
    $sql .= " OR ";
    $sql .= " (procuration.fin_validite <'".$dateelection1."' ";
    $sql .= " AND ";
    $sql .= " procuration.fin_validite <'".$dateelection2."') ";
} elseif ($dateelection1 != NULL) {
    //
    $sql .= " (procuration.debut_validite >'".$dateelection1."' ";
    $sql .= " OR ";
    $sql .= " procuration.fin_validite <'".$dateelection1."') ";
} elseif ($dateelection2 != NULL) {
    //
    $sql .= " (procuration.debut_validite >'".$dateelection2."' ";
    $sql .= " OR ";
    $sql .= " procuration.fin_validite <'".$dateelection2."') ";
}
$sql .= " ) ";
//
$sql .= " order by withoutaccent(lower(mandant.nom)), withoutaccent(lower(mandant.prenom)) ";
