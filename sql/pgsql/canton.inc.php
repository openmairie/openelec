<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/canton.inc.php";

// Critere select de la requete
$champAffiche = array(
    "id as \"".__("Id")."\"",
    "code as \"".__("Code")."\"",
    "libelle as \"".__("Libelle")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "code as \"".__("Code")."\"",
    "libelle as \"".__("Libelle")."\"",
);
