<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// CARTE RETOUR
$sql_nbcarteretour = sprintf(
    'SELECT * FROM %1$selecteur WHERE electeur.carte=1 AND electeur.liste=\'%3$s\' AND electeur.om_collectivite=%2$s',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);
$sql_carteretour = sprintf(
    'UPDATE %1$selecteur SET electeur.carte=0 WHERE electeur.carte=1 AND electeur.liste=\'%3$s\' AND electeur.om_collectivite=%2$s',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);
