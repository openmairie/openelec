<?php
/**
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../sql/pgsql/reu_sync_l_e_oel.inc.php";

$tab_title = __("Électeurs openElec non rattachés");

$selection .= sprintf(
    ' AND electeur.ine IS NULL '
);

$tab_actions['left']['consulter'] = array();
$tab_actions['content'] = $tab_actions['left']['consulter'];
