<?php

$serie = 100;
$ent = __("Traitement")." -> ".__("Module REU");
$tab_description = sprintf(
    '<a class="retour" href="../app/index.php?module=module_reu#ui-tabs-2">Retour</a>'
);
// $table = sprintf(
//     '%1$selecteur left join %1$sreu_sync_listes on (electeur.nom=reu_sync_listes.nom and electeur.date_naissance=to_date(reu_sync_listes.date_de_naissance, \'DD/MM/YYYY\'))',
//     DB_PREFIXE
// );
$table = sprintf(
    '%1$selecteur inner join %1$sliste on electeur.liste=liste.liste left join %1$sreu_sync_listes on (electeur.ine=reu_sync_listes.numero_d_electeur::integer and liste.liste_insee=reu_sync_listes.code_type_liste) ',
    DB_PREFIXE
);
$champAffiche = array(
    "electeur.id",

    "case when withoutaccent(lower(trim(electeur.nom)))=withoutaccent(lower(trim(reu_sync_listes.nom)))
        then
            electeur.nom 
        else
            case when position(' NOM EXACT' IN reu_sync_info) > 0 
                THEN
                    electeur.nom
                else
                    concat('<span class=''diff''>', electeur.nom, '</span>') 
            end
    end as \"nom OEL\"",
    "case when withoutaccent(lower(trim(electeur.nom)))=withoutaccent(lower(trim(reu_sync_listes.nom)))
        then 
            reu_sync_listes.nom
        else
            case when position(' NOM EXACT' IN reu_sync_info) > 0 
                THEN
                    reu_sync_listes.nom 
                else
                    concat('<span class=''diff''>', reu_sync_listes.nom, '</span>') 
            end
    end as \"nom REU\"",
    //
    "case when electeur.nom_usage=electeur.nom
        then 
            ''
        else
            electeur.nom_usage
    end as \"usage OEL\"",
    "case when reu_sync_listes.nom_d_usage=reu_sync_listes.nom 
        then 
            '' 
        else
            case when (withoutaccent(lower(trim(electeur.nom_usage)))=withoutaccent(lower(trim(reu_sync_listes.nom_d_usage))) or withoutaccent(lower(trim(electeur.nom)))=withoutaccent(lower(trim(reu_sync_listes.nom_d_usage))))
                then
                    upper(reu_sync_listes.nom_d_usage)
                else 
                    concat('<span class=''diff''>', upper(reu_sync_listes.nom_d_usage), '</span>')
        end
    end as \"usage REU\"",
    //
    "case when withoutaccent(lower(trim(electeur.prenom)))=withoutaccent(lower(trim(reu_sync_listes.prenoms)))
        then 
            upper(electeur.prenom)
        else
            case when position('PRENOM EXACT' IN reu_sync_info) > 0 
                THEN 
                    upper(electeur.prenom) 
                else 
                    concat('<span class=''diff''>', upper(electeur.prenom), '</span>')
            end
    end as \"prenom OEL\"",
    "case when withoutaccent(lower(trim(electeur.prenom)))=withoutaccent(lower(trim(reu_sync_listes.prenoms)))
        then 
            upper(reu_sync_listes.prenoms)
        else
            case when position('PRENOM EXACT' IN reu_sync_info) > 0 
                THEN 
                    upper(reu_sync_listes.prenoms) 
                else 
                    concat('<span class=''diff''>', upper(reu_sync_listes.prenoms), '</span>')
            end
    end as \"prenom REU\"",
    //
    "case when to_char(electeur.date_naissance,'DD/MM/YYYY')=reu_sync_listes.date_de_naissance
        then
            to_char(electeur.date_naissance,'DD/MM/YYYY')
        else
            case when position('DATENAISSANCE EXACT' IN reu_sync_info) > 0 
                THEN 
                    to_char(electeur.date_naissance,'DD/MM/YYYY')
                else
                    concat('<span class=''diff''>', to_char(electeur.date_naissance,'DD/MM/YYYY'), '</span>') 
            end
    end as \"naissance OEL\"",
    "case when to_char(electeur.date_naissance,'DD/MM/YYYY')=reu_sync_listes.date_de_naissance
        then
            reu_sync_listes.date_de_naissance
        else
            case when position('DATENAISSANCE EXACT' IN reu_sync_info) > 0 
                THEN 
                    reu_sync_listes.date_de_naissance
                else
                    concat('<span class=''diff''>', reu_sync_listes.date_de_naissance, '</span>') 
            end
    end as \"naissance REU\"",
    //
    "electeur.liste",
    "reu_sync_listes.numero_d_electeur",
    "electeur.reu_sync_info",
);
$champRecherche = array(
    "electeur.nom",
    "electeur.prenom",
);
$tri = "";

$selection = sprintf(
    ' WHERE (om_collectivite=%1$s) ',
    intval($_SESSION["collectivite"])
);

/**
 *
 */
$champs = array();
$champs['nom'] = array(
    'colonne' => array('nom', 'prenom'),
    'table' => 'electeur',
    'type' => 'text',
    'libelle' => _('nom'),
    'taille' => 10,
    'max' => 8,
);
require_once "../obj/traitement.reu_sync_l_e_attach.class.php";
$trt = new reuSyncLEAttachTraitement();
if ($obj == "reu_sync_l_e_oel_rattaches_exact") {
    $conditions = $trt->get_conditions("labels_exact");
} elseif ($obj == "reu_sync_l_e_oel_rattaches_approx") {
    $conditions = $trt->get_conditions("labels_approx");
} else {
    $conditions = $trt->get_conditions("labels");
}
$args = array(
    0 => array_merge(array('', ), $conditions),
    1 => array_merge(array(__("Tous"), ), $conditions),
);
$champs['info'] = array(
    'colonne' => 'reu_sync_info',
    'table' => 'electeur',
    'type' => 'select',
    'libelle' => _('condition'),
    'subtype' => 'manualselect',
    'args' => $args,

);
$options[] =  array(
    'type' => 'search',
     'display' => true,
     'advanced' => $champs,
     'default_form' => 'advanced',
     'absolute_object' => 'electeur'
);

$tab_actions["corner"] = array();
