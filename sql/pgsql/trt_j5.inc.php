<?php
/**
 *
 */

/**
 *
 */
$query_j5_where = "and mouvement.liste='".$_SESSION["liste"]."' ";
$query_j5_where .= "and mouvement.om_collectivite=".intval($_SESSION["collectivite"])." ";
$query_j5_where .= "and mouvement.date_tableau='".$datetableau."' ";
if (isset($mode_edition) && $mode_edition == "recapitulatif") {
    //
    $query_j5_where .= "and mouvement.etat='trs' ";
    $query_j5_where .= "and mouvement.tableau='j5' ";
    $query_j5_where .= "and mouvement.date_j5='".$datej5."' ";
} else {
    //
    $query_j5_where .= "and mouvement.etat='actif' ";
    //
    if (isset($cinqjours) && $cinqjours == true || isset($additions) && count($additions) != 0) {
        //
        $query_j5_where .= "and (";
        if (isset($cinqjours) && $cinqjours == true) {
            $query_j5_where .= "param_mouvement.effet='Immediat' ";
        }
        foreach ($additions as $key => $elem) {
            if ((isset($cinqjours) && $cinqjours == true && $key == 0) || $key > 0 && $key < count($additions)) {
                $query_j5_where .= "or ";
            }
            $query_j5_where .= " mouvement.types='".$elem."' ";
        }
        $query_j5_where .= ")";
    } else {
        $query_j5_where .= "and mouvement.etat='impossible' ";
    }
}

/**
 * Ces requetes permettent l'une de lister et l'autre de compter toutes les
 * inscriptions non traitees a effet immediat a la date de tableau donnee en
 * fonction de la collectivite en cours et de la liste en cours
 *
 * @param string $datetableau
 * @param string $_SESSION["collectivite"]
 * @param string $_SESSION["liste"]
 */
$query_inscription = sprintf(
    'FROM %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'inscription\' ',
    DB_PREFIXE
);
$query_inscription .= $query_j5_where;
$query_select_inscription = sprintf(
    'SELECT
        mouvement.id as mouvement_id,
        mouvement.bureau_de_vote_code as bureau_code,
        mouvement.*
    %1$s
    ORDER BY
        withoutaccent(lower(mouvement.nom)),
        withoutaccent(lower(mouvement.prenom))',
    $query_inscription
);
$query_count_inscription = "select count(mouvement.id) ".$query_inscription;

/**
 * Ces requetes permettent l'une de lister et l'autre de compter toutes les
 * radiations non traitees a effet immediat a la date de tableau donnee en
 * fonction de la collectivite en cours et de la liste en cours
 *
 * @param string $datetableau
 * @param string $_SESSION["collectivite"]
 * @param string $_SESSION["liste"]
 */
$query_radiation = sprintf(
    'FROM %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'radiation\' ',
    DB_PREFIXE
);
$query_radiation .= $query_j5_where;
$query_select_radiation = sprintf(
    'SELECT
        mouvement.id as mouvement_id,
        mouvement.bureau_de_vote_code as bureau_code,
        mouvement.*
    %1$s
    ORDER BY
        withoutaccent(lower(mouvement.nom)),
        withoutaccent(lower(mouvement.prenom))',
    $query_radiation
);
$query_count_radiation = "select count(mouvement.id) ".$query_radiation;

/**
 * Ces requetes permettent l'une de lister et l'autre de compter toutes les
 * modifications non traitees a effet immediat a la date de tableau donnee en
 * fonction de la collectivite en cours et de la liste en cours
 *
 * @param string $datetableau
 * @param string $_SESSION["collectivite"]
 * @param string $_SESSION["liste"]
 */
$query_modification = sprintf(
    'FROM %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'modification\' ',
    DB_PREFIXE
);
$query_modification .= $query_j5_where;
$query_select_modification = sprintf(
    'SELECT
        mouvement.id as mouvement_id,
        mouvement.bureau_de_vote_code as bureau_code,
        mouvement.*
    %1$s
    ORDER BY
        withoutaccent(lower(mouvement.nom)),
        withoutaccent(lower(mouvement.prenom))',
    $query_modification
);
$query_count_modification = "select count(mouvement.id) ".$query_modification;
