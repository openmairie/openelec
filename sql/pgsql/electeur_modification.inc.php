<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Titre
$ent = __("Saisie")." -> ".__("Modification");

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 12;

// Critere FROM de la requete
$table = sprintf('%1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id', DB_PREFIXE); // FROM

// Critere select de la requete
$champAffiche = array(
   "electeur.id as \"".__("Id")."\"",
    "nom as \"".__("Nom")."\"",
    "prenom as \"".__("Prenom")."\"",
    "nom_usage as \"".__("Nom d'usage")."\"",
        "(to_char(date_naissance,'DD/MM/YYYY')||' ".__("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \"".__("Date et lieu de naissance")."\"",
   "(numero_habitation||' '||complement_numero||' '||libelle_voie) as \"".__("Adresse")."\"",
   "bureau.code as \"".__("Bureau")."\"",
   "typecat as \"".__("En cours")."\""
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
   "electeur.id",
   "nom",
   "prenom"
);

// Critere where de la requete
$selection = sprintf(
    ' WHERE electeur.om_collectivite=%1$s AND electeur.liste=\'%2$s\' ',
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

/**
 *
 */
//
$nom = mb_strtolower($nom, 'UTF-8');
$nom = str_replace(
    array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$nom
);
if (isset($nom) and $nom != "") {
   if (substr($nom,strlen($nom)-1,1) == '*') {
      $selection .= " and lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
      .substr($nom,0,strlen($nom)-1)."%' ";
   } else {
      if( isset($exact) and $exact != true ) {
         $selection .= " and lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
         .$nom."%' ";
      } else {
         $selection .= " and lower(translate(nom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ='"
         .$nom."' ";
      }
   }
}
//
$prenom = mb_strtolower($prenom, 'UTF-8');
$prenom = str_replace(
    array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$prenom
);
if (isset($prenom) and $prenom != ""){
   if (substr($prenom,strlen($prenom)-1,1) == '*') {
      $selection .= " and lower(translate(prenom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
      .substr($prenom,0,strlen($prenom)-1)."%' ";
   } else {
      if( isset($exact) and $exact != true ) {
         $selection .= " and lower(translate(prenom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
         .$prenom."%' ";
      } else {
         $selection .= " and lower(translate(prenom".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ='"
         .$prenom."' ";
      }
   }
}

//
$marital = mb_strtolower($marital, 'UTF-8');
$marital = str_replace(
    array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
$marital
);
if (isset($marital) and $marital != "") {
   if (substr($marital,strlen($marital)-1,1) == '*') {
      $selection .= " and lower(translate(nom_usage".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
      .substr($marital,0,strlen($marital)-1)."%' ";
   } else {
      if( isset($exact) and $exact != true ) {
         $selection .= " and lower(translate(nom_usage".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%"
         .$marital."%' ";
      } else {
         $selection .= " and lower(translate(nom_usage".iconv("UTF-8",HTTPCHARSET,"::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ='"
         .$marital."' ";
      }
   }
}

//
if (isset($datenaissance) and $datenaissance != "") {
   $selection .= " and date_naissance ='".$datenaissance."' ";
}

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

//
$formulaire = $table.".form";

/**
 * Options
 */
//
$options = array();
$option = array(
    "type" => "search",
    "display" => false,
);
array_push($options, $option);
//
$option = array(
    "type" => "condition",
    "field" => "typecat",
    "case" => array(
        "0" => array(
            "values" => array("modification", ),
            "style" => "modification-encours mouvement-encours",
        ),
        "1" => array(
            "values" => array("radiation", ),
            "style" => "radiation-encours mouvement-encours",
        ),
    ),
);
array_push($options, $option);
