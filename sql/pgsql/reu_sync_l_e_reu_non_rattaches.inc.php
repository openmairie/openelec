<?php

$tab_title = __("Électeurs REU non rattachés");
$serie = 100;
$tab_description = sprintf(
    '<a class="retour" href="../app/index.php?module=module_reu#ui-tabs-2">Retour</a>'
);
$ent = __("Traitement")." -> ".__("Module REU");
$table = sprintf(
    '%1$sreu_sync_listes inner join %1$sliste on reu_sync_listes.code_type_liste=liste.liste_insee left join %1$selecteur on (reu_sync_listes.numero_d_electeur::integer=electeur.ine AND electeur.liste=liste.liste AND electeur.om_collectivite=%2$s ) ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
$case_bureau = "case when (code_bureau_de_vote is null or code_bureau_de_vote = '') then '0' else '1' end";
$champAffiche = array(
    "CONCAT(reu_sync_listes.code_type_liste, reu_sync_listes.numero_d_electeur) as id",
    "reu_sync_listes.code_type_liste",
    "reu_sync_listes.numero_d_electeur",
    "reu_sync_listes.nom",
    "reu_sync_listes.nom_d_usage",
    "reu_sync_listes.prenoms",
    "reu_sync_listes.date_de_naissance",
    $case_bureau." as \"avec/sans bureau\"",
);
$champRecherche = $champAffiche;
unset($champRecherche[0]);
$tri = "";
$inst_reu = $f->get_inst__reu();
$selection = " where code_ugle='".$inst_reu->get_ugle()."' AND electeur.nom IS NULL";

$lien = sprintf(
    "%s&amp;obj=%s&amp;action=%s&amp;advs_id=%s&amp;premier=%s&amp;tricol=%s&amp;valide=%s&amp;retour=%s&amp;idx=0&amp;numero_d_electeur=",
    OM_ROUTE_FORM,
    $obj,
    '502',
    $advs_id,
    $premier,
    $tricol,
    $valide,
    'tab'
);

$tab_actions['left']['consulter']['lien'] = $lien;
$tab_actions['content'] = $tab_actions['left']['consulter'];
$tab_actions["corner"] = array();

$lien = sprintf(
    "%s&amp;obj=%s&amp;action=%s&amp;advs_id=%s&amp;premier=%s&amp;tricol=%s&amp;valide=%s&amp;retour=%s&amp;idx=0&amp;numero_d_electeur=",
    OM_ROUTE_FORM,
    $obj,
    '503',
    $advs_id,
    $premier,
    $tricol,
    $valide,
    'tab'
);
// Actions a gauche : consulter
$tab_actions['left']['bureau'] = array(
    'lien' => $lien,
    'id' => '',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix edit-16" title="'.__('Bureau').'">'.__('Bureau').'</span>',
    'rights' => array('list' => array($obj.'_affecter_bureau', ), 'operator' => 'OR'),
    'ordre' => 10,
);

// COLOR ETAT
$options = array();
$options[] = array(
    "type" => "condition",
    "field" => $case_bureau,
    "case" => array(
        "0" => array(
            "values" => array("0", ),
            "style" => "sans-bureau",
        ),
        "1" => array(
            "values" => array("1", ),
            "style" => "avec-bureau",
        ),
    ),
);
