<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/revision.inc.php";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere select de la requete
$champAffiche = array(
    "revision.id as \"".__("Id")."\"",
    "revision.libelle as \"".__("Revision")."\"",
    "to_char(revision.date_debut ,'DD/MM/YYYY') as \"".__("Date de debut")."\"",
    "to_char(revision.date_effet ,'DD/MM/YYYY') as \"".__("Date d'effet")."\"",
    "to_char(revision.date_tr1 ,'DD/MM/YYYY') as \"".__("Date TR1")."\"",
    "to_char(revision.date_tr2 ,'DD/MM/YYYY') as \"".__("Date TR2")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
);

// Critere order by ou group by de la requete
$tri = "order by revision.date_effet DESC";
