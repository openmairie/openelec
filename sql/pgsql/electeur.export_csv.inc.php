<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

$champAffiche = array(
    "electeur.id as \"id\"",
    "electeur.ine as \"".__("ine")."\"",
    "nom as \"".__("Nom")."\"",
    "prenom as \"".__("Prenom")."\"",
    "nom_usage as \"".__("Nom d'usage")."\"",
    "to_char(date_naissance,'DD/MM/YYYY') as \"".__("Date de naissance")."\"",
    "libelle_lieu_de_naissance||' ('||libelle_departement_naissance||')' as \"".__("Lieu de naissance")."\"",
    "case resident
        when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || electeur.libelle_voie)
                when False then electeur.libelle_voie
            end
        )
        when 'Oui' then 
            adresse_resident
    end as adresse_ligne_1",
    "case resident
        when 'Non' then 
            electeur.complement
        when 'Oui' then 
            trim(complement_resident)
    end as adresse_ligne_2",
    "case resident
        when 'Non' then 
            trim(voie.cp || ' ' || voie.ville)
        when 'Oui' then 
            trim(cp_resident || ' ' || ville_resident)
    end as adresse_ligne_3",
    "bureau.code as \"".__("Bureau")."\"",
    "typecat as \"".__("En cours")."\"",
    "liste.liste_insee as \"".__("liste")."\"",
);
//
if ($f->getParameter("is_collectivity_multi") === true) {
    $champAffiche[] = "om_collectivite.libelle as \"".__("om_collectivite")."\"";
}
