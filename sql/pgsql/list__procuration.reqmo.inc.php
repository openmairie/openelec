<?php
/**
 * REQMO - list__procuration
 *
 * Export de la liste des procurations de la commune de l'utilisateur connecté.
 *
 * Contexte MONO uniquement.
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$reqmo["sql"] = sprintf(
    'SELECT 
        procuration.id as procuration_id,
        procuration.id_externe as procuration_id_externe,
        procuration.mandant_ine as mandant_ine,
        mandant.id as mandant_id,
        mandant.nom as mandant_nom_patronymique,
        mandant.nom_usage as mandant_nom_usage,
        mandant.prenom as mandant_prenom,
        to_char(mandant.date_naissance,\'DD/MM/YYYY\') as mandant_date_naissance,
        bureau_mandant.code as mandant_bureau_code,
        liste_mandant.liste_insee as mandant_liste_code,
        procuration.mandant_resume as mandant_resume,
        procuration.mandataire_ine as mandataire_ine,
        mandataire.id as mandataire_id,
        mandataire.nom as mandataire_nom_patronymique,
        mandataire.nom_usage as mandataire_nom_usage,
        mandataire.prenom as mandataire_prenom,
        to_char(mandataire.date_naissance,\'DD/MM/YYYY\') as mandataire_date_naissance,
        bureau_mandataire.code as mandataire_bureau_code,
        liste_mandataire.liste_insee as mandataire_liste_code,
        procuration.mandataire_resume as mandataire_resume,
        to_char(procuration.debut_validite,\'DD/MM/YYYY\') as debut_validite,
        to_char(procuration.fin_validite,\'DD/MM/YYYY\') as fin_validite,
        procuration.autorite_type as autorite_type,
        procuration.autorite_nom_prenom as autorite_nom_prenom,
        CASE
            WHEN procuration.autorite_type = \'CSL\'
                THEN procuration.autorite_consulat
            WHEN procuration.autorite_type IN (\'GN\', \'PN\', \'TRIB\')
                THEN procuration.autorite_commune
            ELSE
                \'\'
        END as autorite_ugle,
        to_char(procuration.date_accord,\'DD/MM/YYYY\') as date_etablissement,
        procuration.statut as statut,
        procuration.motif_refus as motif_refus,
        om_collectivite.om_collectivite as collectivite_id,
        om_collectivite.libelle as collectivite_libelle
    FROM
        %1$sprocuration
            LEFT JOIN %1$som_collectivite
                ON procuration.om_collectivite=om_collectivite.om_collectivite
            LEFT JOIN %1$selecteur AS mandant 
                ON procuration.mandant=mandant.id 
            LEFT JOIN %1$sbureau AS bureau_mandant
                ON mandant.bureau=bureau_mandant.id
            LEFT JOIN %1$sliste as liste_mandant
                ON mandant.liste=liste_mandant.liste
            LEFT JOIN %1$selecteur AS mandataire
                ON procuration.mandataire=mandataire.id
            LEFT JOIN %1$sbureau AS bureau_mandataire
                ON mandataire.bureau=bureau_mandataire.id
            LEFT JOIN %1$sliste as liste_mandataire
                ON mandataire.liste=liste_mandataire.liste
    WHERE
        procuration.om_collectivite=%2$s
    ORDER BY
        procuration.id',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
