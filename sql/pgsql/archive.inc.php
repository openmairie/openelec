<?php
//$Id$
//gen openMairie le 27/07/2017 14:41

include "../gen/sql/pgsql/archive.inc.php";


// Nombre d'enregistrements par page
$serie = 20;

// Titre
$ent = __("Consultation")." -> ".__("Archive");

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere FROM de la requete
$table = sprintf(
    '%1$sarchive INNER JOIN %1$sparam_mouvement ON archive.types=param_mouvement.code INNER JOIN %1$som_collectivite ON archive.om_collectivite=om_collectivite.om_collectivite', // FROM
    DB_PREFIXE
);

if ($f->isMulti() == true) {
    // Critere select de la requete
    $champAffiche = array (
        "archive.id as \"".__("Id")."\"",
        "archive.electeur_id as \"".__("Id Electeur")."\"",
        "nom as \"".__("Nom")."\"",
        "prenom as \"".__("Prenom")."\"",
        "nom_usage as \"".__("Nom d'usage")."\"",
        "to_char(date_naissance,'DD/MM/YYYY') as \"".__("Date de naissance")."\"",
        "libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')' as \"".__("Lieu de naissance")."\"",
        "case resident
                when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || archive.libelle_voie)
                when False then archive.libelle_voie
            end)
                when 'Oui' then adresse_resident
            end || '<br />' ||case resident
                when 'Non' then archive.complement
                when 'Oui' then (complement_resident || ' ' || cp_resident || ' - ' || ville_resident)
            end as \"".__("Adresse")."\"",
        "om_collectivite.libelle as \"".__("om_collectivite")."\""
    );

} else {

    // Critere select de la requete
    $champAffiche = array (
        "archive.id as \"".__("Id")."\"",
        "archive.electeur_id as \"".__("Id Electeur")."\"",
        "nom as \"".__("Nom")."\"",
        "prenom as \"".__("Prenom")."\"",
        "nom_usage as \"".__("Nom d'usage")."\"",
        "to_char(date_naissance,'DD/MM/YYYY') as \"".__("Date de naissance")."\"",
        "libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')' as \"".__("Lieu de naissance")."\"",
        "case resident
                when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || archive.libelle_voie)
                when False then archive.libelle_voie
            end)
                when 'Oui' then adresse_resident
            end || '<br />' ||case resident
                when 'Non' then archive.complement
                when 'Oui' then (complement_resident || ' ' || cp_resident || ' - ' || ville_resident)
            end as \"".__("Adresse")."\"",
        "archive.bureau_de_vote_code as \"".__("Bureau")."\"",
        "(param_mouvement.typecat||' ['||param_mouvement.libelle||' ('||types||')]<br/>".__("valide lors du traitement")." '||archive.tableau||'<br/>".__("a la date de tableau du")." '||to_char(date_tableau,'DD/MM/YYYY')) as \"".__("Archive")."\""
    );

}

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "archive.electeur_id as \"".__("Id Electeur")."\"",
    "nom as \"".__("Nom")."\"",
    "prenom as \"".__("Prenom")."\"",
    "nom_usage as \"".__("Nom d'usage")."\"",
    "to_char(date_naissance,'DD/MM/YYYY') as \"".__("Date de naissance")."\"",
);

// Critere where de la requete
if ($f->isMulti() == true) {
    $selection = "";

} else {
    $selection = "where  archive.om_collectivite=".intval($_SESSION["collectivite"])." ";
}

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)), date_tableau, date_mouvement";

// Aucune action dans les listings
$tab_actions = array(
    'corner' => array(),
    'left' => array(),
    'content' => array(),
    'specific_content' => array(),
);
