<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$query_electeurs_actuels_groupby_bureau = sprintf(
    'SELECT
        bureau.code as bureau,
        electeur.sexe as sexe,
        count(electeur.id) as total
    FROM
        %1$selecteur
        LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
    WHERE
        electeur.om_collectivite=%2$s
        AND electeur.liste=\'%3$s\'
    GROUP BY
         bureau.code,
         electeur.sexe',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

//
$query_inscriptions_radiations_stats_groupby_bureau = sprintf(
    'SELECT
        mouvement.bureau_de_vote_code as bureau,
        mouvement.date_tableau,
        mouvement.date_j5,
        mouvement.tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe as sexe,
        count(*) as total
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        mouvement.om_collectivite=%2$s
        AND mouvement.liste=\'%3$s\'
        AND 
        (
            lower(param_mouvement.typecat)=\'inscription\'
            OR lower(param_mouvement.typecat)=\'radiation\'
        )
    GROUP BY
        mouvement.bureau_de_vote_code,
        mouvement.date_tableau,
        mouvement.date_j5,
        mouvement.tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe
    ORDER BY
        mouvement.bureau_de_vote_code,
        mouvement.date_tableau,
        mouvement.date_j5,
        mouvement.tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

//
$query_transferts_plus_stats_groupby_bureau = sprintf(
    'SELECT
        mouvement.bureau_de_vote_code as bureau,
        mouvement.date_tableau,
        mouvement.date_j5,
        mouvement.tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe as sexe,
        count(*) as total
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        mouvement.om_collectivite=%2$s
        AND mouvement.liste=\'%3$s\'
        AND mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
        AND lower(param_mouvement.typecat)=\'modification\'
        AND mouvement.ancien_bureau_de_vote_code<>\'\'
    GROUP BY
        mouvement.bureau_de_vote_code,
        mouvement.date_tableau,
        mouvement.date_j5,
        mouvement.tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

//
$query_transferts_moins_stats_groupby_bureau = sprintf(
    'SELECT
        mouvement.ancien_bureau_de_vote_code as bureau,
        mouvement.date_tableau,
        mouvement.date_j5,
        mouvement.tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe as sexe,
        count(*) as total
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        mouvement.om_collectivite=%2$s
        AND mouvement.liste=\'%3$s\'
        AND mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
        AND lower(param_mouvement.typecat)=\'modification\'
        AND mouvement.bureau_de_vote_code<>\'\'
    GROUP BY
        mouvement.ancien_bureau_de_vote_code,
        mouvement.date_tableau,
        mouvement.date_j5,
        mouvement.tableau,
        param_mouvement.typecat,
        mouvement.etat,
        mouvement.sexe
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);
