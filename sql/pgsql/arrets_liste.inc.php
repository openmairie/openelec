<?php
//$Id$ 
//gen openMairie le 14/09/2023 16:22

include "../gen/sql/pgsql/arrets_liste.inc.php";

$ent = __("Traitement")." -> ".__("Arrêts des listes");
$tab_title = __("Arrêts des listes");
// Désaffichage du sous-onglet montrant l'élection
$sousformulaire = array();

$champAffiche = array(
    'arrets_liste.arrets_liste as "'.__("arrêts des listes").'"',
    'CASE
        WHEN reu_scrutin IS NULL THEN \'Arrêt des listes hors élections\'
        ELSE reu_scrutin.libelle
    END as "'.__("Arrêt").'"',
    'arrets_liste.livrable_demande_date as "'.__("Date de demande").'"',
    'arrets_liste.livrable_date as "'.__("Date de réception").'"',
    'CASE
        -- livrable demandé et reçu
        WHEN arrets_liste.livrable IS NOT NULL
            THEN \'<img src=../app/img/arrive.png title="Livrable reçu">\'
        -- livrable demandé et attente de reception
        WHEN arrets_liste.livrable_demande_id IS NOT NULL
            THEN \'<img src=../app/img/nonarrive.png title="En attente de livrable">\'
        -- trop tôt pour demander le livrable
        WHEN CURRENT_DATE < (date_tour1 - integer \'23\')
            THEN \'<img src=../app/img/listeemargement-25x25.png class="arret_liste_indisponible" title="Trop tôt pour la demande">\'
        -- livrable non demandé et dont la date a dépassé la date de demande
        WHEN CURRENT_DATE >= (date_tour1 - integer \'7\')
            THEN \'<img src=../app/img/erreur.png title="Hors délai">\'
        -- livrable non demandé
        ELSE \'<img src=../app/img/listeemargement-25x25.png title="En attente de demande">\'
    END as "'.__("Statut").'"'
);

$table = sprintf(
    '%1$sarrets_liste
    LEFT JOIN %1$sreu_scrutin
        ON arrets_liste.arrets_liste = reu_scrutin.arrets_liste',
    DB_PREFIXE
);
// Actions d'ajout d'arrêt hors élection

$arret_liste = $this->get_inst__om_dbform(array(
    'obj' => 'arrets_liste',
    'idx' => ']'
));
$tab_actions['corner']['ajouter']['lib'] = sprintf(
    '<span class="om-icon om-icon-16 om-icon-fix add-16" title="%1$s">%1$s</span>',
    __("Arrêt des listes hors élection")
);

// Action de demande du livrable
// $tab_actions['left']['demander_arret_liste_livrable'] = array(
//     'lien' => OM_ROUTE_FORM.'&amp;obj='.$obj.'&amp;action=5&amp;retour=tab&amp;idx=',
//     'id' => '',
//     'lib' =>
//         "<span class=\"om-icon om-icon-16 om-icon-fix demander_j20_livrable-16\" title=\"".
//             __("Demander l'arrêt des listes")."\">".
//             __("Demander l'arrêt des listes").
//         "</span>",
//     'ajax' => false,
//     "type" => "action-direct"
// );

$tri="ORDER BY arrets_liste.arrets_liste ASC";
// Affichage de l'action de demande de livrable uniquement si le livrable n'a pas déjà été demandé.

// Si la date de demande de livrable n'est pas enregistrée alors la classe
// *display_demande_emarge_livrable* est appliquée sur la ligne du tableau.
// Cette classe permet de masquer les actions *demander_emarge_livrable* sur
// toutes les lignes où elle n'est pas appliquée (Voir : app.css).
$options[] = array(
    'type' => 'condition',
    'field' => "arrets_liste.livrable_demande_date",
    'case' => array(
        "0" => array(
            'values' => array('', ),
            'style' => 'display_demande_arret_liste_livrable',
        ),
    ),
);
