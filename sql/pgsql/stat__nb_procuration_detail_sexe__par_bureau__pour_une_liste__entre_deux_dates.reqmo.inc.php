<?php
/**
 * REQMO - stat__nb_procuration_detail_sexe__par_bureau__pour_une_liste__entre_deux_dates
 *
 * Contexte MONO uniquement.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

//
$reqmo['sql'] = sprintf(
    'SELECT electeur.bureau as bureau_id, bureau.code as bureau_code, bureau.libelle as bureau_libelle, sum(CASE when electeur.sexe=\'M\' then 1 else 0 end) as masculin, sum(CASE when electeur.sexe=\'F\' then 1 else 0 end) as feminin, count(*) as tous FROM %1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id INNER JOIN %1$sprocuration ON procuration.mandant=electeur.id WHERE electeur.liste=\'[choix_liste]\' AND electeur.om_collectivite=%2$s AND procuration.debut_validite<=\'[datedebut(YYYY-MM-JJ)]\' AND procuration.fin_validite>=\'[datefin(YYYY-MM-JJ)]\' GROUP BY electeur.bureau, bureau.code, bureau.libelle ORDER BY bureau.code',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
