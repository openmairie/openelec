<?php

/*
Cette requete a pour objet de calculer les heures supplementaires sur les 2 tours (non soldes)
heure de nuit apres 22 heures
heure de jour avant 22 heures
Attention ne gere pas le décalage sur la journée suivante (apres 24 h)
*/

$reqmo['libelle'] = "État des heures supplémentaires regroupées sur les 2 tours";
$reqmo['sql'] = "
    SELECT
        A.nom||' '||A.prenom as agent, SUM(C.fin - C.debut) as total,
        SUM((CASE WHEN C.fin > '22:00:00' THEN '22:00:00' ELSE C.fin END)-(CASE WHEN C.debut < '07:00:00' THEN '07:00:00' ELSE C.debut END)) as jour,
        SUM((CASE WHEN C.fin < '22:00:00' THEN '22:00:00' ELSE C.fin END)-'22:00:00') +
        SUM('07:00:00'-(CASE WHEN C.debut > '07:00:00' THEN '07:00:00' ELSE C.debut END)) as nuit
    FROM
        ".DB_PREFIXE."composition_scrutin AS S
        INNER JOIN ".DB_PREFIXE."candidature AS C ON C.composition_scrutin=S.id
        INNER JOIN ".DB_PREFIXE."agent AS A ON A.id=C.agent
    WHERE
        (S.id = '[Tour_1]' OR S.id = '[Tour_2]')
        AND C.decision IS True
    GROUP BY
        A.nom||' '||A.prenom
    ORDER BY
        A.nom||' '||A.prenom
";
$reqmo['Tour_1'] =
    "SELECT
        id,
        libelle
    FROM
        ".DB_PREFIXE."composition_scrutin
    WHERE 
        solde IS NOT True
    -- Résultat par défaut pour le cas où il n y a pas de scrutin
    UNION
        SELECT 0, 'Aucun'";
$reqmo['Tour_2'] =
    "SELECT
        id,
        libelle
    FROM
        ".DB_PREFIXE."composition_scrutin
    WHERE 
        solde IS NOT True
    -- Résultat par défaut pour le cas où il n y a pas de scrutin
    UNION
        SELECT 0, 'Aucun'";
