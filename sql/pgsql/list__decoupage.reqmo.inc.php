<?php
/**
 * REQMO - list__decoupage
 *
 * Contexte MONO uniquement.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

//
$reqmo['sql'] = sprintf(
    'SELECT
        [bureau.code as bureau_code],
        [bureau.libelle as bureau_libelle],
        [canton.code as canton_code],
        [libelle_voie as voie_libelle],
        [premier_pair],
        [dernier_pair],
        [premier_impair],
        [dernier_impair],
        bureau.id as bureau_id
    FROM
        %1$sbureau
        INNER JOIN %1$scanton ON bureau.canton=canton.id
        INNER JOIN %1$sdecoupage ON decoupage.bureau=bureau.id
        INNER JOIN %1$svoie ON decoupage.code_voie=voie.code
    WHERE
        bureau.om_collectivite=%2$s
    ORDER BY
        bureau.code',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
//
$reqmo['bureau_code']="checked";
$reqmo['bureau_libelle']="checked";
$reqmo['canton_code']="checked";
$reqmo['voie_libelle']="checked";
$reqmo['premier_pair']="checked";
$reqmo['dernier_pair']="checked";
$reqmo['premier_impair']="checked";
$reqmo['dernier_impair']="checked";
