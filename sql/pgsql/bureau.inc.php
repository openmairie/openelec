<?php
/**
 * ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/bureau.inc.php";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 *
 * On ne souhaite pas avoir electeur, mouvement et archive en onglet.
 */
$sousformulaire = array(
    'decoupage',
    'numerobureau',
);
$sousformulaire_parameters = array(
    "numerobureau" => array(
        "title" => __("Séquences - Numéro Bureau"),
    ),
);
