<?php
/**
 * Configuration du tableau de membres de commission
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/membre_commission.inc.php";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere FROM de la requete
$table = sprintf(
    '%1$smembre_commission INNER JOIN %1$som_collectivite ON membre_commission.om_collectivite=om_collectivite.om_collectivite', // FROM
    DB_PREFIXE
);

// Critere select de la requete
$champAffiche = array(
    "membre_commission.id as \"".__("Id")."\"",
    "membre_commission.nom as \"".__("Nom")."\"",
    "membre_commission.prenom as \"".__("Prenom")."\"",
    "membre_commission.cp as \"".__("Code postal")."\"",
    "membre_commission.ville as \"".__("Ville")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "membre_commission.id as \"".__("Id")."\"",
    "membre_commission.nom as \"".__("Nom")."\"",
    "membre_commission.prenom as \"".__("Prenom")."\"",
    "membre_commission.cp as \"".__("Code postal")."\"",
    "membre_commission.ville as \"".__("Ville")."\"",
);

// Critere where de la requete
$selection = " where membre_commission.om_collectivite=".intval($_SESSION["collectivite"])."";

// Critere order by ou group by de la requete
$tri = "order by withoutaccent(lower(membre_commission.nom)) ";

/**
 * MULTI
 */
if ($f->isMulti() == true) {
    // Critere select de la requete
    array_push($champAffiche, "om_collectivite.libelle as \"".__("Collectivite")."\"");
    
    // Champ sur lesquels la recherche est active
    array_push($champRecherche, "om_collectivite.libelle as \"".__("Collectivite")."\"");
    
    // Critere where de la requete
    $selection = "";
}
