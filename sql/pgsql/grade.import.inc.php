<?php
//$Id$ 
//gen openMairie le 15/02/2022 15:29

$import= "Insertion dans la table grade voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."grade";
$id=''; // numerotation non automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "grade" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "10",
    ),
    "libelle" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "40",
    ),
);
