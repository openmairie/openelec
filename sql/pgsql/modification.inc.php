<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/mouvement_electeur.inc.php";

// Titre
$ent = __("Consultation")." -> ".__("Modification");
if (isset($maj)) {
    if ($maj == 101 || $maj == 102 || $maj == 0) {
        $ent = __("Saisie")." -> ".__("Modification");
        if ($maj == 101) {
            $tab_title = "<span class=\"om-icon ui-icon ui-icon-search\"><!-- --></span>".__("Recherche d'electeur");
        } elseif ($maj == 102) {
            $tab_title = "<span class=\"om-icon ui-icon ui-icon-search\"><!-- --></span>".__("Resultats de recherche d'electeur");
        }
    }
}

//
if (trim($selection) == "") {
    $selection = " WHERE ";
} else {
    $selection .= " AND ";
}
$selection .= " lower(param_mouvement.typecat)='modification' ";

//
$tab_actions["corner"]["ajouter"]["lien"] = OM_ROUTE_FORM."&obj=modification&action=101&idx=0";

$tab_actions["corner"]["traitements_par_lot"] = array(
    'lien' => OM_ROUTE_FORM.'&obj='.$obj.'&action=91&idx=0',
    'id' => '&advs_id='.$advs_id.'&premier='.$premier.'&tricol='.$tricol.'&valide='.$valide.'&retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix traitement-16" title="'.__('Traiter par lot').'">'.__('Traiter par lot').'</span>',
    'rights' => array('list' => array($obj, $obj.'_traiter_par_lot'), 'operator' => 'OR'),
    'ordre' => 20,
);

if ($f->getParameter("is_collectivity_multi") === true) {
    $tab_actions["corner"]["ajouter"] = array();
    $tab_actions["corner"]["traitements_par_lot"] = array();
}
