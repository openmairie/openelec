<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
if (!isset($params)) {
    $params = array();
}
//
$params["filename_more"] = "-liste".$_SESSION["liste"];

/**
 *
 */
//******************************************************************************
//                  DIFFERENTES ZONES A AFFICHER                              //
//******************************************************************************
//------------------------------------------------------------------------------
//                  COMPTEUR                                                  //
//------------------------------------------------------------------------------
//(0) 1 -> affichage compteur ou 0 ->pas d'affichage
// (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$champs_compteur=array();
//------------------------------------------------------------------------------
//                  IMAGE                                                     //
//------------------------------------------------------------------------------
//  (0) nom image (1) x  (2) y  (3) width (4) hauteue  (5) type
// $img=array(array('../img/arles.png',1,1,17.6,12.6,'png')
$img=array();
//------------------------------------------------------------------------------
//                  TEXTE                                                     //
//------------------------------------------------------------------------------
// (0) texte (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$texte=array();
//------------------------------------------------------------------------------
//                  DATA                                                      //
//------------------------------------------------------------------------------
// (0) affichage avant data
// (1) affichage apres data
// (2) tableau X Y Width bold(0 ou 1),size ou 0
// (3) 1 = number_format(champs,0) : 0002->2  /  ou 0
$champs = array(
    'ligne1'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 0),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne2'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 1),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne3'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 2),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne4'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 3),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne5'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 4),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne6'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 5),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0)
);

// Récupération du type de jury (titulaire / suppléant) à afficher selon l'action utilisée
// Par défaut ce sont les jurés titulaire qui seront affichés
$jury = 1;
if ($this->f->get_submitted_get_value('action') === '307') {
    $jury = 2;
}

//******************************************************************************
//                        SQL                                                 //
//******************************************************************************
$sql = sprintf(
    "SELECT
        '' AS ligne1,
        CASE
            WHEN (electeur.nom_usage <> '' and electeur.nom_usage <> electeur.nom)
                THEN (upper(electeur.civilite) ||' '||electeur.nom||' - '||electeur.nom_usage||' '||electeur.prenom )
            ELSE (upper(electeur.civilite) ||' '||electeur.nom||' '||electeur.prenom)
        END AS ligne2,
        '' AS ligne3,
        CASE resident
            WHEN 'Non' THEN (
                CASE numero_habitation
                    WHEN 0 THEN ''
                    ELSE (numero_habitation||' ')
                END
                ||
                CASE complement_numero
                    WHEN '' THEN ''
                    ELSE (complement_numero||' ')
                END
                || electeur.libelle_voie)
            WHEN 'Oui' THEN adresse_resident
        END AS ligne4,
        CASE resident
            WHEN 'Non' THEN electeur.complement
            WHEN 'Oui' THEN complement_resident
        END AS ligne5,
        CASE resident
            WHEN 'Non' THEN (voie.cp||' '||voie.ville)
            WHEN 'Oui' THEN (cp_resident||' '||ville_resident)
        END AS ligne6
    FROM
        %1\$svoie
        RIGHT JOIN %1\$selecteur ON voie.code = electeur.code_voie 
    WHERE
        electeur.liste = '%2\$s'
        AND electeur.om_collectivite = %3\$d
        AND jury = %4\$d
    ORDER BY
        withoutaccent(lower(nom)),
        withoutaccent(lower(prenom))",
    DB_PREFIXE,
    $_SESSION["liste"],
    intval($_SESSION["collectivite"]),
    $jury
);
