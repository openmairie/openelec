<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/voie.inc.php";

// Objet d'une eventuelle edition .pdf.inc
$edition = "voie";

// Nombre d'enregistrements par page
$serie = 20;

// Critere FROM de la requete
$table = sprintf(
    '%1$svoie INNER JOIN %1$som_collectivite ON voie.om_collectivite=om_collectivite.om_collectivite', // FROM
    DB_PREFIXE
);

// Critere select de la requete
$champAffiche = array(
    "voie.code as \"".__("Code")."\"",
    "voie.libelle_voie as \"".__("Libelle")."\"",
    "voie.cp as \"Code Postal\"",
    "voie.ville as \"".__("Ville")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "voie.code as \"".__("Code")."\"",
    "voie.libelle_voie as \"".__("Libelle")."\"",
    "voie.cp as \"Code Postal\"",
    "voie.ville as \"".__("Ville")."\"",
);

// Critere where de la requete
$selection = "where voie.om_collectivite=".intval($_SESSION["collectivite"])." ";

// Critere order by ou group by de la requete
$tri = "order by voie.om_collectivite, voie.libelle_voie";

/**
 * Tableau de liens
 */
$href[3]['lien'] = OM_ROUTE_MODULE_EDITION."&idx=";
$href[3]['id'] = "&amp;obj=electeurparvoie";
$href[3]['lib'] = "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\"".__("Electeurs par voie")."\">".__("Electeurs par voie")."</span>";
$href[3]['target'] = "_blank";

/**
 * Sous formulaires
 */
$sousformulaire = array(
    "decoupage",
);

/**
 * MULTI
 */
if ($f->isMulti() == true) {
    // Critere select de la requete
    array_push($champAffiche, "om_collectivite.libelle as \"".__("Collectivite")."\"");

    // Champ sur lesquels la recherche est active
    array_push($champRecherche, "om_collectivite.libelle as \"".__("Collectivite")."\"");

    // Critere where de la requete
    $selection = "";

    // Tableau de liens inutiles
    unset($href[3]);
}

/**
 * Options
 */
//
$options = array();
//
$option = array(
    "type" => "pagination_select",
    "display" => false,
);
array_push($options, $option);
