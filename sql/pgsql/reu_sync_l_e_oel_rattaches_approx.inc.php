<?php
/**
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../sql/pgsql/reu_sync_l_e_oel.inc.php";

$tab_title = __("Électeurs rattachés ~approx (à vérifier)");

$selection .= sprintf(
    '
    AND electeur.ine IS NOT NULL 
    AND (
        electeur.reu_sync_info LIKE \'3%%\'
        OR electeur.reu_sync_info LIKE \'4%%\'
        OR electeur.reu_sync_info LIKE \'5%%\'
    )
    '
);
