<?php
/**
 * REQMO - list__electeur__pour_un_ine__pour_une_liste
 *
 * Contexte MONO uniquement.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

// Pour Ã©viter les erreurs de base de donnÃ©es on vÃ©rifie Ã quel moment
// du processus nous nous toruvons pour intialiser la variable
// $identifiant en consÃ©quence. Si le formulaire a Ã©tÃ© postÃ© et que
// l'identifiant postÃ© n'est pas numÃ©rique alors on l'initialise Ã -1 pour que
// la requÃªte ne renvoi aucun rÃ©sultat
$identifiant = "[identifiant]";
if (isset($_POST["identifiant"]) && !is_numeric($_POST["identifiant"])) {
    $identifiant = -1;
}

//
$reqmo['sql'] = sprintf(
    'SELECT
        electeur.id,
        numero_bureau as no_dans_bureau,
        electeur.ine,
        [civilite],
        [nom],
        [prenom],
        [nom_usage],
        [sexe],
        [to_char(date_naissance, \'DD/MM/YYYY\') as date_de_naissance],
        [jury],
        liste
    FROM
        %1$selecteur
    WHERE
        electeur.ine=%3$s
        AND electeur.liste=\'[choix_liste]\'
        AND electeur.om_collectivite=%2$s
    ORDER BY
        [TRI]',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $identifiant
);
//
$reqmo['TRI']= array(
    'nom',
    'nom_usage',
    'prenom',
);
//
$reqmo['civilite']="checked";
$reqmo['nom']="checked";
$reqmo['prenom']="checked";
$reqmo['nom_usage']="checked";
$reqmo['sexe']="checked";
$reqmo['date_de_naissance']="checked";
$reqmo['code_departement_naissance']="checked";
$reqmo['libelle_departement_naissance']="checked";
$reqmo['code_lieu_de_naissance']="checked";
$reqmo['libelle_lieu_de_naissance']="checked";
$reqmo['numero_habitation']="checked";
$reqmo['libelle_voie']="checked";
$reqmo['complement']="checked";
$reqmo['provenance']="checked";
$reqmo['libelle_provenance']="checked";
$reqmo['carte']="checked";
$reqmo['jury']="checked";
