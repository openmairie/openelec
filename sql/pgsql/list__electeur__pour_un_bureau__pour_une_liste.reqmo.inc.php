<?php
/**
 * REQMO - list__electeur__pour_un_bureau__pour_une_liste
 *
 * Export de la liste des électeurs d'un bureau de vote et d'une liste en particulier.
 *
 * Contexte MONO uniquement.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

//
$reqmo['sql'] = sprintf(
    'SELECT
        numero_bureau as no_dans_bureau,
        [civilite],
        [nom],
        [prenom],
        [nom_usage],
        [sexe],
        [to_char(date_naissance,
        \'DD/MM/YYYY\') as date_de_naissance],
        [code_departement_naissance],
        [libelle_departement_naissance],
        [code_lieu_de_naissance],
        [libelle_lieu_de_naissance],
        [numero_habitation],
        [libelle_voie],
        [complement],
        [provenance],
        [libelle_provenance],
        [carte],
        [jury],
        liste
    FROM
        %1$selecteur
    WHERE
        electeur.bureau=[choix_bureau]
        AND electeur.liste=\'[choix_liste]\'
        AND electeur.om_collectivite=%2$s
    ORDER BY
        [tri]',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
//
$reqmo["tri"] = array(
    "nom",
    "nom_usage",
    "prenom",
);
//
$reqmo['civilite']="checked";
$reqmo['nom']="checked";
$reqmo['prenom']="checked";
$reqmo['nom_usage']="checked";
$reqmo['sexe']="checked";
$reqmo['date_de_naissance']="checked";
$reqmo['code_departement_naissance']="checked";
$reqmo['libelle_departement_naissance']="checked";
$reqmo['code_lieu_de_naissance']="checked";
$reqmo['libelle_lieu_de_naissance']="checked";
$reqmo['numero_habitation']="checked";
$reqmo['libelle_voie']="checked";
$reqmo['complement']="checked";
$reqmo['provenance']="checked";
$reqmo['libelle_provenance']="checked";
$reqmo['carte']="checked";
$reqmo['jury']="checked";
//
$reqmo["choix_bureau"] = sprintf(
    'SELECT bureau.id, (bureau.code || \' \' || bureau.libelle) FROM %1$sbureau WHERE bureau.om_collectivite=%2$s ORDER BY bureau.code',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
