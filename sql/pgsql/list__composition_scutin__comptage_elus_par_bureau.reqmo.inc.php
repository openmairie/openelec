<?php

$reqmo['libelle'] = "Comptage des élus par bureau";

$reqmo['sql'] = "
    SELECT
        B.libelle as bureau, A.poste as poste, COUNT(A.id) as \"nombre d'élus\"
    FROM
        ".DB_PREFIXE."composition_scrutin AS S
        INNER JOIN ".DB_PREFIXE."affectation AS A ON A.composition_scrutin=S.id
        INNER JOIN ".DB_PREFIXE."elu AS E ON E.id=A.elu
        INNER JOIN ".DB_PREFIXE."bureau AS B ON B.id=A.bureau
    WHERE
        S.id = '[SCRUTIN]'
    GROUP BY
        B.libelle, A.poste
    ORDER BY
        B.libelle, A.poste
";
$reqmo['SCRUTIN'] =
    "SELECT
        id,
        libelle
    FROM
        ".DB_PREFIXE."composition_scrutin
    WHERE 
        solde IS NOT True
    -- Résultat par défaut pour le cas où il n y a pas de scrutin
    UNION
        SELECT 0, 'Aucun'";
