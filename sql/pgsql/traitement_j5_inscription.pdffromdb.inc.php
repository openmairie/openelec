<?php
/**
 * 
 */

//
include "../sql/pgsql/traitement_common_pdf.inc.php";

//
if ($mode_edition == "recapitulatif") {
    //
    $libtitre = "Inscription(s) appliquée(s) lors du traitement J-5 du ".$f->formatdate($datej5)." [Tableau du ".$f->formatdate($datetableau)."]";
} else {
    //
    $libtitre = "Inscription(s) à appliquer au traitement J-5 du ".date("d/m/Y")." [Tableau du ".$f->formatdate($f->getParameter("datetableau"))."]";
}

//--------------------------SQL-------------------------------------------------
$sql = " SELECT ";
$sql .= " mouvement.bureau_de_vote_code as \"Bureau\", ";
$sql .= " mouvement.nom, ";
$sql .= " mouvement.prenom as \"Prenom(s)\", ";
$sql .= " to_char(mouvement.date_naissance, 'DD/MM/YYYY') as \"Naissance\", ";
$sql .= " param_mouvement.libelle as \"Motif\" ";
$sql .= $query_inscription;
$sql .= " ORDER BY mouvement.bureau_de_vote_code, withoutaccent(lower(mouvement.nom)), withoutaccent(lower(mouvement.prenom)) ";
