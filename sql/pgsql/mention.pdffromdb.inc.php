<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id: procuration_common_pdf.inc.php 1715 2019-02-19 15:28:11Z fmichon $
 */

/**
 *
 */
//
$dateelection = null;
$dateelection1 = null;
$dateelection2 = null;
//
if (isset($params) === true
    && is_array($params) === true
    && array_key_exists("dateelection1", $params) === true) {
    //
    $date = explode("/", $params['dateelection1']);
    if (sizeof($date) == 3
        && (checkdate($date[1], $date[0], $date[2]))) {
        $dateelection1 = $date[2]."-".$date[1]."-".$date[0];
    }
}
//
if (isset($params) === true
    && is_array($params) === true
    && array_key_exists("dateelection2", $params) === true) {
    //
    $date = explode("/", $params['dateelection2']);
    if (sizeof($date) == 3 && (checkdate($date[1], $date[0], $date[2]))) {
        $dateelection2 = $date[2]."-".$date[1]."-".$date[0];
    }
}
//
if ($dateelection1 === null
    && $dateelection2 === null) {
    //
    $dateelection1 = date("Y-m-d");
}
//
if ($dateelection1 === null) {
    $dateelection = $dateelection2;
} else {
    $dateelection = $dateelection1;
}

/**
 *
 */
//
$DEBUG=0;
// ------------------------document---------------------------------------------
$orientation="L";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=10;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B
//-------------------------LIGNE tableau----------------------------------------
$size=8; //taille POLICE
$height=5; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="255";// couleur fond  R
$C2fond1="255";// couleur fond  V
$C3fond1="255";// couleur fond  B
$C1fond2="241";// couleur fond  R
$C2fond2="241";// couleur fond  V
$C3fond2="241";// couleur fond  B
//-------------------------- titre----------------------------------------------
$libtitre = "Mention(s)";
$flagsessionliste=0;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="B";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre=11;
//--------------------------libelle entete colonnes-----------------------------
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=5;//hauteur ligne entete colonne
$C1fondentete="180";// couleur fond  R
$C2fondentete="180";// couleur fond  V
$C3fondentete="180";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
$entete_style="";
$entete_size=8;
//------ Border entete colonne $be0  Ã $be.. ( $be OBLIGATOIRE )
$be0="TLB";
$be1="TLB";
$be2="TLB";
$be3="TLBR";
// ------ couleur border--------------------------------------------------------
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
//------ Border cellule colonne $b0  Ã $b.. ( $b OBLIGATOIRE )
$b0="BL";
$b1="BL";
$b2="BL";
$b3="BLR";
//------ ALIGNEMENT entete colonne $ae0  Ã $ae.. ( $ae OBLIGATOIRE )
$ae0="L";
$ae1="C";
$ae2="C";
$ae3="C";
//------ largeur de chaque colonne $l0  Ã $l.. ( $l OBLIGATOIRE )---------------
$widthtableau=277;// -> ajouter $l0 Ã $lxx
$l0=140;
$l1=40;
$l2=14;
$l3=$widthtableau-$l0-$l1-$l2;
$bt=0;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//------ Affichage d'une ligne de compteur du nombre d'enregistrements----------
$counter_flag = true;
$counter_prefix = "";
$counter_suffix = "enregistrement(s)";
//------ ALIGNEMENT de chaque colonne $l0  Ã $a.. ( $a OBLIGATOIRE )------------
$a0="L";
$a1="C";
$a2="C";
$a3="C";
//--------------------------SQL-------------------------------------------------
$sql = "";
/**
 *
 */
//-------------------------- titre----------------------------------------------
$libtitre = "Liste des mentions(s)";

//--------------------------SQL-------------------------------------------------
$liste = $_SESSION["liste"];
$libelle_liste = $_SESSION["libelle_liste"];
if (array_key_exists("liste", $params) === true) {
    $liste = $params["liste"];
    $ret = $this->f->get_one_result_from_db_query(sprintf(
        'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
        DB_PREFIXE,
        $this->f->db->escapesimple($liste)
    ));
    $libelle_liste = $ret["result"];
}
//
$sql = sprintf(
    'SELECT 
        concat(
            nom_de_naissance, 
            case
                when nom_d_usage <> \'\' and nom_d_usage <> nom_de_naissance
                    then concat(\' (\', nom_d_usage, \')\')
                else \'\'
            end,
            \' \',
            prenoms) as electeur,
        date_de_naissance as naissance,
        code_du_bureau_de_vote as bv,
        case mention_code 
        when \'1\' then \'*** ne vote pas au premier tour ***\'
        when \'2\' then \'*** ne vote pas dans la commune ***\'
        else \'???\'
        end as mention
    FROM 
        %1$sreu_livrable
    WHERE 
        reu_livrable.om_collectivite=\'%2$s\'
        AND reu_livrable.liste=\'%3$s\'
        AND reu_livrable.demande_id=%4$s
        AND reu_livrable.scrutin_id=%5$s
        AND reu_livrable.mention_code<>\'\'
    ORDER BY 
        nom_de_naissance, nom_d_usage, prenoms',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $liste,
    $params["livrable_demande_id"],
    $params["scrutin"]
);

