<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/numerobureau.inc.php";

// SELECT
$champAffiche = array(
    'numerobureau.id as "'.__("id").'"',
    '(liste.liste || \' \' || liste.libelle_liste) as "'.__("liste").'"',
    '(bureau.code || \' \' || bureau.libelle) as "'.__("bureau").'"',
    'numerobureau.dernier_numero as "'.__("dernier numero attribué").'"',
    'numerobureau.dernier_numero_provisoire as "'.__("dernier numero provisoire attribué").'"',
);
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}

// Champ sur lesquels la recherche est active
// On ne souhaite pas pouvoir rechercher par les numéros de séquence
$champRecherche = array(
    'numerobureau.id as "'.__("id").'"',
    '(liste.liste || \' \' || liste.libelle_liste) as "'.__("liste").'"',
    '(bureau.code || \' \' || bureau.libelle) as "'.__("bureau").'"',
);
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}

// Critere order by de la requête
$tri = "ORDER BY om_collectivite.libelle ASC NULLS LAST, liste.liste, bureau.code ";
