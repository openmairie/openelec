<?php
/**
 *
 */

//
$electeur_search_filter  = "";
$mouvement_search_filter  = "";
//
$nom = mb_strtolower($nom, 'UTF-8');
$nom = str_replace(
    array(
        'à', 'â', 'ä', 'á', 'ã', 'å',
        'î', 'ï', 'ì', 'í',
        'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
        'ù', 'û', 'ü', 'ú',
        'é', 'è', 'ê', 'ë',
        'ç', 'ÿ', 'ñ',
        ),
    array(
        'a', 'a', 'a', 'a', 'a', 'a',
        'i', 'i', 'i', 'i',
        'o', 'o', 'o', 'o', 'o', 'o',
        'u', 'u', 'u', 'u',
        'e', 'e', 'e', 'e',
        'c', 'y', 'n',
        ),
    $nom
);

//
if ($nom != "") {
    // Echappement des caractères spéciaux pour éviter de l'injection SQL
    $nom = $this->f->db->escapesimple($nom);
    //
    if (substr($nom, strlen($nom) - 1, 1) == '*') {
        $electeur_search_filter .= " lower(translate(electeur.nom".iconv("UTF-8", HTTPCHARSET, "::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%".substr($nom, 0, strlen($nom)-1)."%' ";
    } else {
        if ($exact == 1) {
            $electeur_search_filter .= " lower(translate(electeur.nom".iconv("UTF-8", HTTPCHARSET, "::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $electeur_search_filter .= " = '".$nom."' ";
        } else {
            $electeur_search_filter .= " lower(translate(electeur.nom".iconv("UTF-8", HTTPCHARSET, "::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $electeur_search_filter .= " like '%".$nom."%' ";
        }
    }
    $electeur_search_filter .= " AND ";
    //
    if (substr($nom, strlen($nom)-1, 1) == '*') {
        $mouvement_search_filter .= " lower(translate(mouvement.nom".iconv("UTF-8", HTTPCHARSET, "::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) like '%".substr($nom, 0, strlen($nom)-1)."%' ";
    } else {
        if ($exact == 1) {
            $mouvement_search_filter .= " lower(translate(mouvement.nom".iconv("UTF-8", HTTPCHARSET, "::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $mouvement_search_filter .= " = '".$nom."' ";
        } else {
            $mouvement_search_filter .= " lower(translate(mouvement.nom".iconv("UTF-8", HTTPCHARSET, "::varchar,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'").")) ";
            $mouvement_search_filter .= " like '%".$nom."%' ";
        }
    }
    $mouvement_search_filter .= " AND ";
}
//
if ($datenaissance != "") {
    //
    $datenaissance_format_yyyymmdd = substr($datenaissance, 6, 4).'-'.substr($datenaissance, 3, 2).'-'.substr($datenaissance, 0, 2);
    //
    $electeur_search_filter .= sprintf(
        ' electeur.date_naissance=\'%s\' AND ',
        $datenaissance_format_yyyymmdd
    );
    $mouvement_search_filter .= sprintf(
        ' mouvement.date_naissance=\'%s\' AND ',
        $datenaissance_format_yyyymmdd
    );
}

/**
 * Cette requete permet de lister les electeurs correspondants au nom (d'une
 * maniere exacte ou non) et/ou a la date de naissance donnee en fonction de
 * la collectivite en cours et de la liste en cours
 *
 * @param string $nom Nom patronymique
 * @param boolean $exact true ou false
 * @param string $datenaissance format DD/MM/YYYY
 * @param string $_SESSION["collectivite"]
 * @param string $_SESSION["liste"]
 */
$query_doublon_electeur = sprintf(
    'SELECT
        electeur.nom,
        electeur.prenom,
        bureau.code,
        to_char(electeur.date_naissance, \'DD/MM/YYYY\') AS Naissance
    FROM
        %1$selecteur
        LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
    WHERE
        %4$s
        electeur.om_collectivite=%2$s AND
        electeur.liste=\'%3$s\'
    ORDER BY
        withoutaccent(lower(electeur.nom)),
        withoutaccent(lower(electeur.prenom))',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"],
    $electeur_search_filter
);

/**
 * Cette requete permet de lister les inscriptions correspondantes au nom (d'une
 * maniere exacte ou non) et/ou a la date de naissance donnee en fonction de
 * la collectivite en cours et de la liste en cours
 *
 * @param string $nom Nom patronymique
 * @param boolean $exact true ou false
 * @param string $datenaissance format DD/MM/YYYY
 * @param string $_SESSION["collectivite"]
 * @param string $_SESSION["liste"]
 */
$query_doublon_mouvement = sprintf(
    'SELECT
        mouvement.nom,
        mouvement.prenom,
        mouvement.bureau_de_vote_code,
        to_char(mouvement.date_naissance,\'DD/MM/YYYY\') AS Naissance
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        %4$s
        mouvement.etat=\'actif\' AND
        param_mouvement.typecat=\'Inscription\' AND
        mouvement.om_collectivite=%2$s AND
        mouvement.liste=\'%3$s\'
    ORDER BY
        withoutaccent(lower(mouvement.nom)),
        withoutaccent(lower(mouvement.prenom))',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"],
    $mouvement_search_filter
);
