<?php
/**
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../sql/pgsql/reu_sync_l_e_oel.inc.php";

$tab_title = __("Électeurs rattachés ~exact");

$selection .= sprintf(
    '
    AND electeur.ine IS NOT NULL 
    AND (
        electeur.reu_sync_info LIKE \'1%%\'
        OR electeur.reu_sync_info LIKE \'2%%\'
    )
    '
);
