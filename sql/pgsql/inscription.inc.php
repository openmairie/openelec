<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/mouvement.inc.php";

// Titre
$ent = __("Consultation")." -> ".__("Inscription");
if (isset($maj) && ($maj == 0 || $maj == 101 || $maj ==102)) {
    $ent = __("Saisie")." -> ".__("Nouvelle inscription");
    if ($maj == 101) {
        $tab_title = "<span class=\"om-icon ui-icon ui-icon-search\"><!-- --></span>".__("Recherche de doublon");
    } elseif ($maj == 102) {
        $tab_title = "<span class=\"om-icon ui-icon ui-icon-search\"><!-- --></span>".__("Resultats de la recherche de doublon");
    }
}

//
if (trim($selection) == "") {
    $selection = " WHERE ";
} else {
    $selection .= " AND ";
}
$selection .= " lower(param_mouvement.typecat)='inscription' ";

//
$tab_actions["corner"]["ajouter"]["lien"] = OM_ROUTE_FORM."&obj=inscription&action=101&idx=0";


$tab_actions["corner"]["traitements_par_lot"] = array(
    'lien' => OM_ROUTE_FORM.'&obj='.$obj.'&action=91&idx=0',
    'id' => '&advs_id='.$advs_id.'&premier='.$premier.'&tricol='.$tricol.'&valide='.$valide.'&retour=tab',
    'lib' => '<span class="om-icon om-icon-16 om-icon-fix traitement-16" title="'.__('Traiter par lot').'">'.__('Traiter par lot').'</span>',
    'rights' => array('list' => array($obj, $obj.'_traiter_par_lot'), 'operator' => 'OR'),
    'ordre' => 20,
);

if ($f->getParameter("is_collectivity_multi") === true) {
    $tab_actions["corner"]["ajouter"] = array();
    $tab_actions["corner"]["traitements_par_lot"] = array();
}

// Extra Parameters
if (isset($obj)
    && $obj == "inscription"
    && isset($maj)
    && $maj == 0) {
    //
    $exact = "";
    if (isset($_GET['exact']) && $_GET['exact']==true) {
        $exact = $_GET['exact'];
    }
    $nom = "";
    if (isset($_GET['nom'])) {
        $nom = $_GET['nom'];
    }
    $prenom = "";
    if (isset($_GET['prenom'])) {
        $prenom = $_GET['prenom'];
    }
    $datenaissance = "";
    if (isset($_GET['datenaissance'])) {
        $datenaissance = $_GET['datenaissance'];
    }
    $sexe = "";
    if (isset($_GET['sexe'])) {
        $sexe = $_GET['sexe'];
    }
    $ine = "";
    if (isset($_GET['ine'])) {
        $ine = $_GET['ine'];
    }
    $choix_ine = "";
    if (isset($_GET['choix_ine'])) {
        $choix_ine = $_GET['choix_ine'];
    }
    $recherche_par = "";
    if (isset($_GET['recherche_par'])) {
        $recherche_par = $_GET['recherche_par'];
    }
    $extra_parameters = array(
        "recherche_par" => $recherche_par,
        "nom" => $nom,
        "prenom" => $prenom,
        "datenaissance" => $datenaissance,
        "sexe" => $sexe,
        "ine" => $ine,
        "choix_ine" => $choix_ine,
        "exact" => $exact,
    );
}
