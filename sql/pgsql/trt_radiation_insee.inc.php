<?php
/**
 * $Id$
 * Ce fichier est appelé par le fichier obj/insee_import.class.php

 */
$radiationmax = count($personne);
$i = 0;

while ($i<$radiationmax) {
    if (isset ($personne[$i]["code_lieu_de_naissance"]))$divisionter = $personne[$i]['code_lieu_de_naissance'];
    if (isset ($personne[$i]['code_lieu_de_deces'])) $divisionterdece = $personne[$i]['code_lieu_de_deces'];
    
    if (isset ($divisionter)) {
        $sqlc = sprintf(
            'SELECT libelle_departement FROM %1$sdepartement WHERE code=\'%2$s\'',
            DB_PREFIXE,
            $divisionter
        );
        $result = $f->db->getone($sqlc);
        $f->addToLog(__METHOD__."(): db->getone(\"".$sqlc."\");", VERBOSE_MODE);
        $f->isDatabaseError($result);
        $personne[$i]['libelle_lieu_de_naissance'] = $result;
    }
    
    if (isset ($divisionterdece)) {
        $sqlc = sprintf(
            'SELECT libelle_departement FROM %1$sdepartement WHERE code=\'%2$s\'',
            DB_PREFIXE,
            $divisionterdece
        );
        $result = $f->db->getone($sqlc);
        $f->addToLog(__METHOD__."(): db->getone(\"".$sqlc."\");", VERBOSE_MODE);
        $f->isDatabaseError($result);
        $personne[$i]['libelle_lieu_de_deces'] = $result;
    }
    $i++;
}
