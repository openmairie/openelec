<?php

$reqmo['libelle'] = "Liste des affectations des élus";
$reqmo['sql'] = "
    SELECT
         [S.libelle as scrutin], [E.nom||' '||E.prenom as nom], [B.libelle as bureau], [A.poste as poste], [A.periode as periode], [CASE A.decision WHEN 't' THEN 'Oui' ELSE 'Non' END as decision], [C.nom as candidat]
    FROM
        ".DB_PREFIXE."composition_scrutin AS S
        INNER JOIN ".DB_PREFIXE."affectation AS A ON A.composition_scrutin=S.id
        INNER JOIN ".DB_PREFIXE."elu AS E ON E.id=A.elu
        INNER JOIN ".DB_PREFIXE."bureau AS B ON B.id=A.bureau
        INNER JOIN ".DB_PREFIXE."candidat AS C ON C.id=A.candidat
    WHERE
        S.id = '[SCRUTIN]'
    ORDER BY
        [TRI]
";
$reqmo['TRI'] = array('bureau', 'poste', 'periode');
foreach(array('scrutin', 'nom', 'bureau', 'poste', 'periode', 'decision', 'candidat') as $key) {
    $reqmo[$key] = "checked";
}
$reqmo['SCRUTIN'] =
    "SELECT
        id,
        libelle
    FROM
        ".DB_PREFIXE."composition_scrutin
    WHERE 
        solde IS NOT True
    -- Résultat par défaut pour le cas où il n y a pas de scrutin
    UNION
        SELECT 0, 'Aucun'";
