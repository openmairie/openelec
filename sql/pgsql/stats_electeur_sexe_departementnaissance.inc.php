<?php
/**
 * $Id$
 *
 */

//
$sql_departement=sprintf(
    'SELECT * FROM %1$sdepartement ORDER BY departement.code',
    DB_PREFIXE
);

if (isset ($row ['code'])) {
    //
    $sqlB = sprintf(
        'SELECT count(code_departement_naissance) FROM %1$selecteur WHERE electeur.liste=\'%3$s\' and electeur.code_departement_naissance=\'%4$s\' and electeur.om_collectivite=%2$s',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $row['code']
    );
    //
    $sql1 = sprintf(
        'SELECT count(sexe) FROM %1$selecteur WHERE electeur.sexe=\'M\' and  electeur.code_departement_naissance=\'%4$s\' and  electeur.liste=\'%3$s\' and electeur.om_collectivite=%2$s',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $row['code']
    );
    //
    $sql2 = sprintf(
        'SELECT count(sexe) FROM %1$selecteur WHERE electeur.sexe=\'F\' and  electeur.code_departement_naissance=\'%4$s\' and  electeur.liste=\'%3$s\' and electeur.om_collectivite=%2$s',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $row['code']
    );
}
//
$sqlB_a = sprintf(
    'SELECT count(code_departement_naissance) FROM %1$selecteur WHERE electeur.liste=\'%3$s\' and electeur.code_departement_naissance not in (SELECT departement.code FROM %1$sdepartement) and electeur.om_collectivite=%2$s',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);
//
$sql1_a = sprintf(
    'SELECT count(sexe) FROM %1$selecteur WHERE electeur.sexe=\'M\' and  electeur.code_departement_naissance not in (SELECT departement.code FROM %1$sdepartement) and  electeur.liste=\'%3$s\' and electeur.om_collectivite=%2$s',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);
//
$sql2_a = sprintf(
    'SELECT count(sexe) FROM %1$selecteur WHERE electeur.sexe=\'F\' and  electeur.code_departement_naissance not in (SELECT departement.code FROM %1$sdepartement) and  electeur.liste=\'%3$s\' and electeur.om_collectivite=%2$s',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);
