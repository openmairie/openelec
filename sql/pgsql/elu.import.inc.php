<?php
//$Id$ 
//gen openMairie le 15/02/2022 15:29

$import= "Insertion dans la table elu voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."elu";
$id='id'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "id" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
    ),
    "nom" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "40",
    ),
    "prenom" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "40",
    ),
    "nomjf" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "date_naissance" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "lieu_naissance" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "adresse" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "80",
    ),
    "cp" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "5",
    ),
    "ville" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "40",
    ),
);
