<?php
/**
 * $Id$
 * Ce fichier est appelé par le fichier app/jury.php
 */

//
$query_nb_jures_canton = sprintf(
    'SELECT
        canton.id as canton_id,
        canton.code as canton_code,
        canton.libelle as canton_libelle,
        parametrage_nb_jures.%3$s AS nb_jures
    FROM
        %1$sparametrage_nb_jures
        LEFT JOIN %1$scanton ON parametrage_nb_jures.canton=canton.id
    WHERE
        parametrage_nb_jures.om_collectivite = %2$s',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $this->jury === 1 ? 'nb_jures' : 'nb_suppleants'
);

$sql_jury = sprintf(
    'SELECT * FROM %1$selecteur WHERE electeur.liste=\'%3$s\' AND electeur.jury='.$this->jury.' AND electeur.om_collectivite=%2$s',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

if (isset($row['id'])) {
    $sql_jury_1 = sprintf(
        'UPDATE %1$selecteur SET electeur.jury=0 WHERE electeur.om_collectivite=%2$s AND electeur.id=%3$s',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $row['id']
    );
}
