<?php
/**
 *
 */

/**
 *
 */
//
$error = false;

/**
 *
 */
if ($traitement == "j5") {
    //
    require "../sql/pgsql/trt_j5.inc.php";
} else {
    //
    require "../sql/pgsql/trt_annuel.inc.php";
}

/**
 *
 */
//
$DEBUG=0;
// ------------------------document---------------------------------------------
$orientation="L";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=10;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B
//-------------------------LIGNE tableau----------------------------------------
$size=8; //taille POLICE
$height=5; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="255";// couleur fond  R
$C2fond1="255";// couleur fond  V
$C3fond1="255";// couleur fond  B
$C1fond2="241";// couleur fond  R
$C2fond2="241";// couleur fond  V
$C3fond2="241";// couleur fond  B
//-------------------------- titre----------------------------------------------
$libtitre="Listing des mouvements appliqués ou à appliquer lors d'un traitement "; // libelle titre
$flagsessionliste=0;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="B";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre=11;
//--------------------------libelle entete colonnes-----------------------------
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=5;//hauteur ligne entete colonne
$C1fondentete="180";// couleur fond  R
$C2fondentete="180";// couleur fond  V
$C3fondentete="180";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
$entete_style="";
$entete_size=8;
//------ Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
$be0="TLB";
$be1="TLB";
$be2="TLB";
$be3="TLB";
$be4="TLBR";
// ------ couleur border--------------------------------------------------------
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
//------ Border cellule colonne $b0  à $b.. ( $b OBLIGATOIRE )
$b0="BL";
$b1="BL";
$b2="BL";
$b3="BL";
$b4="BLR";
//------ ALIGNEMENT entete colonne $ae0  à $ae.. ( $ae OBLIGATOIRE )
$ae0="C";
$ae1="C";
$ae2="C";
$ae3="C";
$ae4="C";
$ae5="C";
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$l0=27;
$l1=60;
$l2=60;
$l3=30;
$l4=100;
$widthtableau=277;// -> ajouter $l0 à $lxx
$bt=0;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//------ Affichage d'une ligne de compteur du nombre d'enregistrements----------
$counter_flag = true;
$counter_prefix = "";
$counter_suffix = "enregistrement(s)";
//------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )------------
$a0="C";
$a1="L";
$a2="L";
$a3="C";
$a4="L";
//--------------------------SQL-------------------------------------------------
$sql = "";
