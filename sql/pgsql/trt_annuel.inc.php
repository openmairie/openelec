<?php
/**
 *
 */

/**
 *
 */
$query_annuel_where = "and mouvement.liste='".$_SESSION["liste"]."' ";
$query_annuel_where .= "and mouvement.om_collectivite=".intval($_SESSION["collectivite"])." ";
$query_annuel_where .= "and mouvement.date_tableau='".$datetableau."' ";
$query_annuel_where .= "and (param_mouvement.effet='1erMars' or param_mouvement.effet='Immediat') ";
if (isset($mode_edition) && $mode_edition == "recapitulatif") {
    //
    $query_annuel_where .= "and mouvement.etat='trs' ";
    $query_annuel_where .= "and mouvement.tableau='annuel' ";
} else {
    //
    $query_annuel_where .= "and mouvement.etat='actif' ";
}

/**
 * Ces requetes permettent l'une de lister et l'autre de compter toutes les
 * inscriptions non traitees a la date de tableau donnee en fonction de la
 * collectivite en cours et de la liste en cours
 *
 * @param string $datetableau
 * @param string $_SESSION["collectivite"]
 * @param string $_SESSION["liste"]
 */
$query_inscription = sprintf(
    'FROM %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'inscription\' ',
    DB_PREFIXE
);
$query_inscription .= $query_annuel_where;
$query_select_inscription = sprintf(
    'SELECT
        mouvement.id as mouvement_id,
        mouvement.bureau_de_vote_code as bureau_code,
        mouvement.*
    %1$s
    ORDER BY
        withoutaccent(lower(mouvement.nom)),
        withoutaccent(lower(mouvement.prenom))',
    $query_inscription
);
$query_count_inscription = "select count(mouvement.id) ".$query_inscription;

/**
 * Ces requetes permettent l'une de lister et l'autre de compter toutes les
 * radiations non traitees a la date de tableau donnee en fonction de la
 * collectivite en cours et de la liste en cours
 *
 * @param string $datetableau
 * @param string $_SESSION["collectivite"]
 * @param string $_SESSION["liste"]
 */
$query_radiation = sprintf(
    'FROM %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'radiation\' ',
    DB_PREFIXE
);
$query_radiation .= $query_annuel_where;
$query_select_radiation = sprintf(
    'SELECT
        mouvement.id as mouvement_id,
        mouvement.bureau_de_vote_code as bureau_code,
        mouvement.*
    %1$s
    ORDER BY
        withoutaccent(lower(mouvement.nom)),
        withoutaccent(lower(mouvement.prenom))',
    $query_radiation
);
$query_count_radiation = "select count(mouvement.id) ".$query_radiation;

/**
 * Ces requetes permettent l'une de lister et l'autre de compter toutes les
 * modifications non traitees a la date de tableau donnee en fonction de la
 * collectivite en cours et de la liste en cours
 *
 * @param string $datetableau
 * @param string $_SESSION["collectivite"]
 * @param string $_SESSION["liste"]
 */
$query_modification = sprintf(
    'FROM %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        lower(param_mouvement.typecat)=\'modification\' ',
    DB_PREFIXE
);
$query_modification .= $query_annuel_where;
$query_select_modification = sprintf(
    'SELECT
        mouvement.id as mouvement_id,
        mouvement.bureau_de_vote_code as bureau_code,
        mouvement.*
    %1$s
    ORDER BY
        withoutaccent(lower(mouvement.nom)),
        withoutaccent(lower(mouvement.prenom))',
    $query_modification
);
$query_count_modification = "select count(mouvement.id) ".$query_modification;
