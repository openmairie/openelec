<?php
/**
 * REQMO - list__bureau
 *
 * Contexte MONO et MULTI.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

//
$reqmo['sql'] = sprintf(
    'SELECT
        [bureau.code as bureau_code],
        [bureau.libelle as bureau_libelle],
        [adresse1],
        [adresse2],
        [adresse3],
        [canton.code as canton_code],
        [canton.libelle as canton_libelle],
        [circonscription.code as circonscription_code],
        [circonscription.libelle as circonscription_libelle],
        [om_collectivite.om_collectivite as collectivite_id],
        [om_collectivite.libelle as collectivite_libelle],
        bureau.id as bureau_id
    FROM
        %1$sbureau
        LEFT JOIN %1$scanton on bureau.canton=canton.id
        LEFT JOIN %1$scirconscription on bureau.circonscription=circonscription.id
        LEFT JOIN %1$som_collectivite on bureau.om_collectivite=om_collectivite.om_collectivite
        %2$s
    ORDER BY
        [tri]',
    DB_PREFIXE,
    (!isset($f) || !$f->isMulti() ? "WHERE bureau.om_collectivite=".intval($_SESSION["collectivite"])."" : "")
);
//
$reqmo["tri"]= array(
    "collectivite_libelle",
    "ville",
    "bureau_code",
    "canton_code",
);
//
$reqmo['bureau_code']="checked";
$reqmo['bureau_libelle']="checked";
$reqmo['adresse1']="checked";
$reqmo['adresse2']="checked";
$reqmo['adresse3']="checked";
$reqmo['canton_code']="checked";
$reqmo['canton_libelle']="checked";
$reqmo['circonscription_code']="checked";
$reqmo['circonscription_libelle']="checked";
$reqmo['collectivite_id']="checked";
$reqmo['collectivite_libelle']="checked";
