<?php

include "../gen/sql/pgsql/composition_bureau.inc.php";

// suppression du bouton d'ajout
$tab_actions["corner"]["ajouter"] = null;

// ajout de l'action de consultation de l'édition de composition du bureau
$tab_actions['left']['edition'] = array(
    'lien' => OM_ROUTE_FORM.'&amp;obj='.$obj.'&amp;action=201&amp;retour=tab&amp;idx=',
    'id' => '',
    'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\"".__("Composition du bureau")."\">".__("Scrutin bureau")."</span>",
    'ajax' => false,
    "target" => "_blank",
);

$champAffiche = array(
    'composition_bureau.id as "'.__("id").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    '(bureau.code || \' \' || bureau.libelle) as "'.__("bureau").'"',
);

$tri = "ORDER BY composition_scrutin.libelle ASC NULLS LAST, bureau.code";
