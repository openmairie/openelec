<?php
/**
 * 
 */

//
include "../sql/pgsql/traitement_common_pdf.inc.php";

//
if ($mode_edition == "recapitulatif") {
    //
    $libtitre = "Modification(s) appliquée(s) lors du traitement J-5 du ".$f->formatdate($datej5)." [Tableau du ".$f->formatdate($datetableau)."]";
} else {
    //
    $libtitre = "Modification(s) à appliquer au traitement J-5 du ".date("d/m/Y")." [Tableau du ".$f->formatdate($f->getParameter("datetableau"))."]";
}

//------ Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
$be4=$be3;
$be5=$be4."R";
//------ Border cellule colonne $b0  à $b.. ( $b OBLIGATOIRE )
$b4=$b3;
$b5=$b4."R";
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$l5=$l4;
$l4=$l3;
$l3=$l2;
$l2=$l1;
$l1=$l0/2;
$l0=$l0/2;
//------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )------------
$a5=$a4;
$a4=$a3;
$a3=$a2;
$a2=$a1;
$a1=$a0;
$a0=$a0;

//--------------------------SQL-------------------------------------------------
$sql = sprintf(
    'SELECT
        mouvement.bureau_de_vote_code as "Bureau",
        CASE 
            WHEN mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
            THEN mouvement.ancien_bureau_de_vote_code
            ELSE \'-\' 
        END AS "Ancien", 
        mouvement.nom, 
        mouvement.prenom as "Prenom(s)", 
        to_char(mouvement.date_naissance, \'DD/MM/YYYY\') as "Naissance", 
        param_mouvement.libelle as "Motif" 
    %1$s
    ORDER BY
        mouvement.bureau_de_vote_code,
        withoutaccent(lower(mouvement.nom)),
        withoutaccent(lower(mouvement.prenom))
    ',
    $query_modification
);
