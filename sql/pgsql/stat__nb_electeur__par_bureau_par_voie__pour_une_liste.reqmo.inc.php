<?php
/**
 * REQMO - stat__nb_electeur__par_bureau_par_voie__pour_une_liste
 *
 * Contexte MONO uniquement.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

//
$reqmo['sql'] = sprintf(
    'SELECT electeur.bureau as bureau_id, bureau.code as bureau_code, bureau.libelle as bureau_libelle, voie.code as voie_code, voie.libelle_voie as voie_libelle, count(electeur.id) as nb_electeur FROM %1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id INNER JOIN %1$svoie ON electeur.code_voie=voie.code WHERE electeur.liste=\'[choix_liste]\' AND electeur.om_collectivite=%2$s GROUP BY electeur.bureau, bureau.code, bureau.libelle, voie.code, voie.libelle_voie ORDER BY bureau.code',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
