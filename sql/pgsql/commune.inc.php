<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/commune.inc.php";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere select de la requete
$champAffiche = array(
    "commune.code as \"".__("Code")."\"",
    "commune.libelle_commune as \"".__("Libelle")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "commune.code as \"".__("Code")."\"",
    "commune.libelle_commune as \"".__("Libelle")."\"",
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = "order by commune.code";

/**
 * Options
 */
//
$options = array();
//
$option = array(
    "type" => "pagination_select",
    "display" => false,
);
array_push($options, $option);
