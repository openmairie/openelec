<?php

$reqmo['libelle'] = "Liste des candidats acceptés";
$reqmo['sql'] = "
    SELECT
         [S.libelle as scrutin], [A.nom||' '||A.prenom as nom], [A.adresse as adresse], [A.cp||' '||A.ville as cpville], [A.telephone as telephone], [B.libelle as bureau], [C.poste as poste], [C.periode as periode]
    FROM
        ".DB_PREFIXE."composition_scrutin AS S
        INNER JOIN ".DB_PREFIXE."candidature AS C ON C.composition_scrutin=S.id
        INNER JOIN ".DB_PREFIXE."agent AS A ON A.id=C.agent
        INNER JOIN ".DB_PREFIXE."bureau AS B ON B.id=C.bureau
    WHERE
        S.id = '[SCRUTIN]'
        AND C.decision IS True
    ORDER BY
        [TRI]
";
$reqmo['TRI']= array('bureau','poste','periode');
foreach(array('scrutin', 'nom', 'adresse', 'cpville', 'telephone', 'bureau', 'poste', 'periode') as $key) {
    $reqmo[$key] = "checked";
}
$reqmo['SCRUTIN'] =
    "SELECT
        id,
        libelle
    FROM
        ".DB_PREFIXE."composition_scrutin
    WHERE 
        solde IS NOT True
    -- Résultat par défaut pour le cas où il n y a pas de scrutin
    UNION
        SELECT 0, 'Aucun'";
