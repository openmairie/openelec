<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Titre
$ent = __("Consultation")." -> ".__("Carte en retour");

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Nombre d'enregistrements par page
$serie = 12;

// Critere FROM de la requete
$table = sprintf('%1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id', DB_PREFIXE); // FROM

// Critere select de la requete
$champAffiche = array(
    "electeur.id as \"".__("Id")."\"",
    "nom as \"".__("Nom")."\"",
    "prenom as \"".__("Prenom")."\"",
    "nom_usage as \"".__("Nom d'usage")."\"",
    "(to_char(date_naissance,'DD/MM/YYYY')||' ".__("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \"".__("Date et lieu de naissance")."\"",
    "(numero_habitation||' '||complement_numero||' '||libelle_voie) as \"".__("Adresse")."\"",
    "bureau.code as \"".__("Bureau")."\"",
    "typecat as \"".__("En cours")."\""
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
   "electeur.id",
   "nom",
   "prenom"
);

// Critere where de la requete
$selection = sprintf(
    ' WHERE electeur.om_collectivite=%1$s AND electeur.liste=\'%2$s\' AND carte=1 ',
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

/**
 * Tableau de liens
 */
$tab_actions["corner"] = array();
$tab_actions["content"] = null;
$tab_actions["left"] = array();

//
$formulaire = $table.".form";

/**
 * Options
 */
//
$options = array();
$option = array(
    "type" => "search",
    "display" => false,
);
array_push($options, $option);
//
$option = array(
    "type" => "condition",
    "field" => "typecat",
    "case" => array(
        "0" => array(
            "values" => array("modification", ),
            "style" => "modification-encours",
        ),
        "1" => array(
            "values" => array("radiation", ),
            "style" => "radiation-encours",
        ),
    ),
);
array_push($options, $option);
