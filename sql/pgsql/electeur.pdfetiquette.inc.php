<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
if (!isset($params)) {
    $params = array();
}
//
$params["filename_more"] = "-liste".$_SESSION["liste"];
$mode_edition = "";
$offset = 0;
if (isset($params["mode_edition"])) {
    if ($params["mode_edition"] == "commune") {
        $mode_edition = "commune";
    } elseif ($params["mode_edition"] == "parbureau") {
        $mode_edition = "parbureau";
        if (isset($params["bureau_code"])) {
            $bureau_code = $params["bureau_code"];
            $params["filename_more"] .= "-".$bureau_code;
            if (isset($f)) {
                //
                $sql_offset = sprintf(
                    'SELECT count(*) FROM %1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id WHERE bureau.code::integer<%4$s AND electeur.om_collectivite=%2$s AND electeur.liste=\'%3$s\'',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $_SESSION["liste"],
                    intval($bureau_code)
                );
                $offset = $f->db->getone($sql_offset);
                $f->addToLog(
                    __METHOD__."(): db->getone(\"".$sql_offset."\");",
                    VERBOSE_MODE
                );
                $f->isDatabaseError($offset);
            }
        } else {
            $bureau_code = "-1";
        }
    }
}

/**
 *
 */
//******************************************************************************
//                  DIFFERENTES ZONES A AFFICHER                              //
//******************************************************************************
//------------------------------------------------------------------------------
//                  COMPTEUR                                                  //
//------------------------------------------------------------------------------
//(0) 1 -> affichage compteur ou 0 ->pas d'affichage
// (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$champs_compteur = array(
    1,
    $parametrage_etiquettes["etiquette_marge_int_gauche"]+($parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"])*(2/3),
    $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 0),
    ($parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"])*(1/3),
    0,
    8,
    $offset,
    "R"
);
//------------------------------------------------------------------------------
//                  IMAGE                                                     //
//------------------------------------------------------------------------------
//  (0) nom image (1) x  (2) y  (3) width (4) hauteue  (5) type
// $img=array(array('../img/arles.png',1,1,17.6,12.6,'png')
$img=array();
//------------------------------------------------------------------------------
//                  TEXTE                                                     //
//------------------------------------------------------------------------------
// (0) texte (1) x  (2) y  (3) width (4) bold 1 ou 0  (5) size ou 0
$texte=array();
//------------------------------------------------------------------------------
//                  DATA                                                      //
//------------------------------------------------------------------------------
// (0) affichage avant data
// (1) affichage apres data
// (2) tableau X Y Width bold(0 ou 1),size ou 0
// (3) 1 = number_format(champs,0) : 0002->2  /  ou 0
$champs = array(
    'ligne1'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 0),
                              ($parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"])*(2/3),
                              0,
                              8),0),
    'ligne2'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 1),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne3'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 2),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne4'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 3),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne5'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 4),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0),
    'ligne6'   => array('', '',
                        array($parametrage_etiquettes["etiquette_marge_int_gauche"],
                              $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 5),
                              $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
                              1,
                              0),0)
);

//******************************************************************************
//                        SQL                                                 //
//******************************************************************************
$sql = " SELECT ";
//
$sql .= " (bureau.code||' - '|| numero_bureau)  AS ligne1, ";
//
$sql .= " CASE ";
    $sql .= " when (electeur.nom_usage <> '' and electeur.nom_usage <> electeur.nom) ";
        $sql .= " THEN (upper(electeur.civilite) ||' '||electeur.nom||' - '||electeur.nom_usage||' '||electeur.prenom ) ";
    $sql .= " ELSE (upper(electeur.civilite) ||' '||electeur.nom||' '||electeur.prenom) ";
$sql .= " END AS ligne2, ";
//
$sql .= " '' AS ligne3, ";
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN (";
        $sql .= " CASE numero_habitation ";
            $sql .= " WHEN 0 THEN '' ";
            $sql .= " ELSE (numero_habitation||' ') ";
        $sql .= " END ";
        $sql .= " || ";
        $sql .= " CASE complement_numero ";
            $sql .= " WHEN '' THEN '' ";
            $sql .= " ELSE (complement_numero||' ') ";
        $sql .= " END ";
    $sql .= " ||electeur.libelle_voie) ";
    $sql .= " WHEN 'Oui' THEN adresse_resident ";
$sql .= " END AS ligne4, ";
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN electeur.complement ";
    $sql .= " WHEN 'Oui' THEN complement_resident ";
$sql .= " END AS ligne5, ";
//
$sql .= " CASE resident ";
    $sql .= " WHEN 'Non' THEN (voie.cp||' '||voie.ville) ";
    $sql .= " WHEN 'Oui' THEN (cp_resident||' '||ville_resident) ";
$sql .= " END AS ligne6 ";
//
$sql .= sprintf(
    ' FROM %1$svoie RIGHT JOIN %1$selecteur ON voie.code = electeur.code_voie LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id ',
    DB_PREFIXE
);
//
$sql .= " WHERE ";
$sql .= " electeur.liste='".$_SESSION["liste"]."' ";
$sql .= " AND electeur.om_collectivite=".intval($_SESSION["collectivite"])." ";
//
if ($mode_edition == "") {
    $sql .= " AND 1=2 ";
}
//
if ($mode_edition == "parbureau") {
    $sql .= " AND bureau.code='".$f->db->escapesimple($bureau_code)."'";
}
//
if ($mode_edition == "parbureau") {
    $sql .= " ORDER BY bureau.code, withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
} elseif ($mode_edition == "commune") {
    $sql .= " ORDER BY withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
}
