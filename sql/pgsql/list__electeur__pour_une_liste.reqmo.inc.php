<?php
/**
 * REQMO - list__electeur__pour_une_liste
 *
 * Export de la liste des électeurs d'une liste en particulier.
 *
 * Contexte MONO et MULTI.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

//
$reqmo['sql'] = sprintf(
    'SELECT numero_bureau as no_dans_bureau, [electeur.civilite as electeur_civilite], [nom], [prenom], [nom_usage], [sexe], [to_char(date_naissance, \'DD/MM/YYYY\') as date_de_naissance], [code_departement_naissance], [libelle_departement_naissance], [code_lieu_de_naissance], [libelle_lieu_de_naissance], [code_nationalite], [libelle_nationalite], [numero_habitation], [complement_numero], [libelle_voie], [complement], [resident], [adresse_resident], [complement_resident], [cp_resident], [ville_resident], [provenance], [bureau.code as bureau_code], [libelle_provenance], [carte], [jury], [om_collectivite.libelle as ville], [om_collectivite.om_collectivite as collectivite], liste FROM %1$selecteur LEFT JOIN %1$som_collectivite ON electeur.om_collectivite=om_collectivite.om_collectivite LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id LEFT JOIN %1$snationalite ON electeur.code_nationalite=nationalite.code WHERE electeur.liste=\'[choix_liste]\' %2$s ORDER BY [tri]',
    DB_PREFIXE,
    (!isset($f) || !$f->isMulti() ? "AND electeur.om_collectivite=".intval($_SESSION["collectivite"])."" : "")
);
//
$reqmo["tri"]= array(
    "nom",
    "nom_usage",
    "prenom",
    "date_naissance",
    "collectivite",
    "ville",
);
//
$reqmo['electeur_civilite']="checked";
$reqmo['nom']="checked";
$reqmo['prenom']="checked";
$reqmo['nom_usage']="checked";
$reqmo['sexe']="checked";
$reqmo['date_de_naissance']="checked";
$reqmo['code_departement_naissance']="checked";
$reqmo['libelle_departement_naissance']="checked";
$reqmo['code_lieu_de_naissance']="checked";
$reqmo['libelle_lieu_de_naissance']="checked";
$reqmo['numero_habitation']="checked";
$reqmo['libelle_voie']="checked";
$reqmo['complement']="checked";
$reqmo['provenance']="checked";
$reqmo['libelle_provenance']="checked";
$reqmo['bureau_code']="checked";
$reqmo['carte']="checked";
$reqmo['jury']="checked";
$reqmo['collectivite']="checked";
$reqmo['ville']="checked";
$reqmo['resident']="checked";
$reqmo['adresse_resident']="checked";
$reqmo['complement_resident']="checked";
$reqmo['cp_resident']="checked";
$reqmo['ville_resident']="checked";
$reqmo['complement_numero']="checked";
$reqmo['code_nationalite']="checked";
$reqmo['libelle_nationalite']="checked";
//
$reqmo["choix_liste"] = $query_select_liste_officielle;
