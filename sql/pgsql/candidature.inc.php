<?php

include "../gen/sql/pgsql/candidature.inc.php";

/*$tab_actions['left']['candidature'] = array(
    'lien' => OM_ROUTE_FORM.'&amp;obj='.$obj.'&amp;action=201&amp;retour=tab&amp;idx=',
    'id' => '',
    'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\"".__("Candidature")."\">".__("Candidature")."</span>",
    'ajax' => false,
    "target" => "_blank",
);*/

// Filtre listing sous formulaire - composition_scrutin
if (in_array($retourformulaire, $foreign_keys_extended["composition_scrutin"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array('composition_scrutin.libelle as "'.__("composition_scrutin").'"', )
    );
}

$champAffiche = array(
    'candidature.id as "'.__("id").'"',
    'agent.nom as "'.__("agent").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    'periode.libelle as "'.__("periode").'"',
    'poste.libelle as "'.__("poste").'"',
    '(bureau.code || \' \' || bureau.libelle) as "'.__("bureau").'"',
    "case candidature.decision when 't' then 'Oui' else 'Non' end as \"".__("decision")."\"",
    "case candidature.recuperation when 't' then 'Oui' else 'Non' end as \"".__("recuperation")."\"",
    'candidature.debut as "'.__("debut").'"',
    'candidature.fin as "'.__("fin").'"',
);
