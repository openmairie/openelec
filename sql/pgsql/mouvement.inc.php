<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../gen/sql/pgsql/mouvement.inc.php";


// Nombre d'enregistrements par page
$serie = 30;

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere FROM de la requete
$table = sprintf(
    '%1$sparam_mouvement RIGHT JOIN %1$smouvement ON param_mouvement.code=mouvement.types LEFT JOIN %1$som_collectivite ON mouvement.om_collectivite=om_collectivite.om_collectivite LEFT JOIN %1$svoie ON mouvement.code_voie=voie.code LEFT JOIN %1$sliste ON mouvement.liste=liste.liste', // FROM
    DB_PREFIXE
);

// Critere select de la requete
$champAffiche = array(
    "mouvement.id as \"id\"",
    "to_char(date_demande,'DD/MM/YYYY') as \"".__("date de demande")."\"",
    "nom as \"".__("Nom")."\"",
    "prenom as \"".__("Prenom")."\"",
    "nom_usage as \"".__("Nom d'usage")."\"",
    "(to_char(date_naissance,'DD/MM/YYYY')||' ".__("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \"".__("Date et lieu de naissance")."\"",
    "param_mouvement.libelle as \"".__("motif")."\"",
    "mouvement.bureau_de_vote_code as \"".__("Bureau")."\"",
    "to_char(date_tableau,'DD/MM/YYYY') as \"".__("Tableau du")."\"",
    "etat as \"".__("état")."\"",
    "statut as \"".__("Statut")."\"",
    "case mouvement.vu when 't' then 'Oui' else 'Non' end as \"".__("Vu")."\"",
    "to_char(date_j5,'DD/MM/YYYY') as \"".__("Traite le")."\"",
    "liste.liste_insee as \"".__("liste")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "mouvement.id as \"id\"",
    "to_char(date_demande,'DD/MM/YYYY') as \"".__("date de demande")."\"",
    "nom as \"".__("Nom")."\"",
    "prenom as \"".__("Prenom")."\"",
    "nom_usage as \"".__("Nom d'usage")."\"",
    "param_mouvement.libelle as \"".__("motif")."\"",
    "to_char(date_naissance,'DD/MM/YYYY') as \"".__("Date de naissance")."\"",
    "etat as \"".__("état")."\"",
    "statut as \"".__("Statut")."\"",
    "case mouvement.vu when 't' then 'Oui' else 'Non' end as \"".__("Vu")."\"",
    "liste.liste_insee as \"".__("liste")."\"",
);

// Critere order by ou group by de la requete
$tri = "order by date_demande DESC NULLS LAST, id DESC, date_tableau, withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";

// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended["electeur"] = array("electeur");
// Filtre listing sous formulaire - bureau
if (in_array($retourformulaire, $foreign_keys_extended["electeur"])) {
    // Critere select de la requete
    $champAffiche = array(
        "mouvement.id as \"id\"",
        "electeur_id as \"electeur_id\"",
        "to_char(date_demande,'DD/MM/YYYY') as \"".__("date de demande")."\"",
        "param_mouvement.libelle as \"".__("motif")."\"",
        "mouvement.bureau_de_vote_code as \"".__("Bureau")."\"",
        "to_char(date_tableau,'DD/MM/YYYY') as \"".__("Tableau du")."\"",
        "etat as \"".__("état")."\"",
        "statut as \"".__("Statut")."\"",
        "case mouvement.vu when 't' then 'Oui' else 'Non' end as \"".__("Vu")."\"",
        "to_char(date_j5,'DD/MM/YYYY') as \"".__("Traite le")."\"",
        "liste.liste_insee as \"".__("liste")."\"",
    );
    $tab_actions["corner"] = array();
    $tab_actions["content"]["lien"] = OM_ROUTE_FORM."&obj=mouvement&action=3&idx=";
    $tab_actions["content"]["ajax"] = false;
    $tab_actions["content"]["rights"] = array('list' => array($obj, $obj.'_dispatch'), 'operator' => 'OR');
    $tab_actions["left"]["consulter"]["lien"] = OM_ROUTE_FORM."&obj=mouvement&action=3&idx=";
    $tab_actions["left"]["consulter"]["ajax"] = false;
    $tab_actions["left"]["consulter"]["rights"] = array('list' => array($obj, $obj.'_dispatch'), 'operator' => 'OR');
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (mouvement.electeur_id = ".intval($idxformulaire)." OR mouvement.ine=(SELECT ine FROM ".DB_PREFIXE."electeur WHERE id=".intval($idxformulaire).")) ";
    } else {
        // Filtre MONO
        $selection = " WHERE (mouvement.om_collectivite = '".$_SESSION["collectivite"]."') AND (mouvement.electeur_id = ".intval($idxformulaire)." OR mouvement.ine=(SELECT ine FROM ".DB_PREFIXE."electeur WHERE id=".intval($idxformulaire).")) ";
    }
}

//
if ($f->getParameter("is_collectivity_multi") === true) {
    $champAffiche[] = "om_collectivite.libelle as \"".__("om_collectivite")."\"";
    $champRecherche[] = "om_collectivite.libelle as \"".__("om_collectivite")."\"";
} else {
    if (trim($selection) == "") {
        $selection = " WHERE ";
    } else {
        $selection .= " AND ";
    }
    $selection .= " mouvement.om_collectivite=".intval($_SESSION["collectivite"])." ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'piece',
);
$sousformulaire_parameters = array(
    "piece" => array(
        "title" => _("Pièce(s)")
    ),
);

/**
 * Options du LISTING
 */
$options = array();
// ADVS
$args = array(
    0 => array('', 't', 'f', ),
    1 => array(__('Tous'), __('Oui'), __('Non'), ),
);
$champs = array();
$champs['id'] = array(
    'colonne' => 'id',
    'table' => 'mouvement',
    'type' => 'text',
    'libelle' => 'id',
    'help' => __("identifiant numérique du mouvement interne à openElec"),
    'taille' => 8,
    "max" => 60,
);
$champs['ine'] = array(
    'colonne' => 'ine',
    'table' => 'mouvement',
    'type' => 'text',
    'libelle' => 'ine',
    'help' => __("identifiant national de l'électeur (REU)"),
    'taille' => 8,
    "max" => 60,
);
$champs['nom'] = array(
    'colonne' => array('nom', 'nom_usage'),
    'table' => 'mouvement',
    'type' => 'text',
    'libelle' => __('nom'),
    'help' => __("nom patronymique / nom d'usage de l'électeur"),
    'taille' => 20,
    "max" => 60,
);
$champs['prenoms'] = array(
    'colonne' => 'prenom',
    'table' => 'mouvement',
    'type' => 'text',
    'libelle' => __('prénoms'),
    'taille' => 20,
    "max" => 60,
);
$champs['date_naissance'] = array(
    'colonne' => 'date_naissance',
    'table' => 'mouvement',
    'type' => 'date',
    'libelle' => __('date de naissance'),
    'taille' => 8,
    'where' => 'intervaldate',
);
$champs['date_demande'] = array(
    'colonne' => 'date_demande',
    'table' => 'mouvement',
    'type' => 'date',
    'libelle' => __("date de demande"),
    'taille' => 8,
    'where' => 'intervaldate',
);
$champs['motif_'.$obj] = array(
    'colonne' => 'types',
    'table' => 'mouvement',
    'type' => 'select',
    'libelle' => __('motif'),
);
$champs['statut'] = array(
    'colonne' => 'statut',
    'table' => 'mouvement',
    'type' => 'select',
    'libelle' => __('statut'),
);
$champs['etat'] = array(
    'colonne' => 'etat',
    'table' => 'mouvement',
    'type' => 'select',
    'libelle' => __('état'),
);
$champs['vu'] = array(
    'colonne' => 'vu',
    'table' => 'mouvement',
        'libelle' => __('vu ?'),
    "type" => "select",
    "subtype" => "manualselect",
    "args" => $args,
);
$champs['liste'] = array(
    'colonne' => 'liste',
    'table' => 'mouvement',
    'type' => 'select',
    'libelle' => __('liste'),
);
if ($f->getParameter("is_collectivity_multi") === true) {
    $champs['om_collectivite'] = array(
        'colonne' => 'om_collectivite',
        'table' => 'mouvement',
        'type' => 'select',
        'libelle' => __('om_collectivite'),
    );
}
//
if ($obj == "inscription") {
    $exports = array("csv", "etiquettes", );
    $options[] =  array(
        'type' => 'search',
        'display' => true,
        'advanced' => $champs,
        'export' => $exports,
        'default_form' => 'simple',
        'absolute_object' => 'mouvement'
    );
    $options[] = array(
        "type" => "export",
        "export" => array(
            "csv" => array(
                "right" => $obj."_exporter",
            ),
            "etiquettes" => array(
                "right" => $obj."_etiquettes_from_listing",
                "url" => OM_ROUTE_FORM."&obj=".$obj."&action=367&idx=0",
            ),
        ),
    );
} else {
    $exports = array("csv", );
    $options[] =  array(
        'type' => 'search',
        'display' => true,
        'advanced' => $champs,
        'export' => $exports,
        'default_form' => 'simple',
        'absolute_object' => 'mouvement'
    );
}
// COLOR ETAT
$options[] = array(
    "type" => "condition",
    "field" => "etat",
    "case" => array(
        "0" => array(
            "values" => array("trs", ),
            "style" => "etat-trs",
        ),
        "1" => array(
            "values" => array("na", ),
            "style" => "etat-na",
        ),
    ),
);
