<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include PATH_OPENMAIRIE."sql/pgsql/om_parametre.inc.php";

if (isset($f) === true) {
    $inst_om_parametre = $f->get_inst__om_dbform(array("obj" => "om_parametre",));
    if (trim($selection) === "") {
        $selection = ' WHERE ';
    } else {
        $selection .= ' AND ';
    }
    $selection .= sprintf(
        'om_parametre.libelle NOT IN (%s)',
        sprintf(
            '\'%s\'',
            implode("','", $inst_om_parametre->_hidden_and_locked_elements)
        )
    );
}
