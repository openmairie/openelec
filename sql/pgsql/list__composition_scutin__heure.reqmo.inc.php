<?php

$reqmo['libelle'] = "État des heures supplémentaires";
$reqmo['sql'] = "
    SELECT
        A.nom||' '||A.prenom as agent,
        C.poste as poste,
        B.libelle as bureau,
        C.periode as periode,
        C.debut as debut,
        C.fin as fin,
        CASE C.recuperation WHEN 't' THEN 'Oui' ELSE 'Non' END as recuperation
    FROM
        ".DB_PREFIXE."composition_scrutin AS S
        INNER JOIN ".DB_PREFIXE."candidature AS C ON C.composition_scrutin=S.id
        INNER JOIN ".DB_PREFIXE."agent AS A ON A.id=C.agent
        INNER JOIN ".DB_PREFIXE."bureau AS B ON B.id=C.bureau
    WHERE
        S.id = '[SCRUTIN]'
        AND C.decision IS True
    ORDER BY
        [TRI]
";
$reqmo['TRI'] = array('bureau', 'poste', 'periode');
foreach(array('nom', 'poste', 'bureau', 'periode', 'debut', 'fin', 'recuperation') as $key) {
    $reqmo[$key] = "checked";
}
$reqmo['SCRUTIN'] =
    "SELECT
        id,
        libelle
    FROM
        ".DB_PREFIXE."composition_scrutin
    WHERE 
        solde IS NOT True
    -- Résultat par défaut pour le cas où il n y a pas de scrutin
    UNION
        SELECT 0, 'Aucun'";