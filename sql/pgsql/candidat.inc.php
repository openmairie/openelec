<?php
/**
 * ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/candidat.inc.php";

// Filtre listing sous formulaire - composition_scrutin
if (in_array($retourformulaire, $foreign_keys_extended["composition_scrutin"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array('composition_scrutin.libelle as "'.__("composition_scrutin").'"', )
    );
}
