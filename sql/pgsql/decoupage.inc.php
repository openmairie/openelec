<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/decoupage.inc.php";

// SELECT
// L'objet principal de la surcharge est d'afficher le code du bureau en plus
// de son libellé. On en profite aussi pour cacher la colonne liée lorsque nous
// nous trouvons dans son contexte. Dans le contexte d'un bureau on n'affiche
// pas la colonne bureau, ...
$champAffiche_id = array(
    'decoupage.id as "'.__("id").'"',
);
$champAffiche_bureau = array(
    '(bureau.code || \' \' || bureau.libelle) as "'.__("bureau").'"',
);
$champAffiche_voie = array(
    'voie.libelle_voie as "'.__("voie").'"',
);
$champAffiche_numeros = array(
    'decoupage.premier_impair as "'.__("impair - début").'"',
    'decoupage.dernier_impair as "'.__("impair - fin").'"',
    'decoupage.premier_pair as "'.__("pair - début").'"',
    'decoupage.dernier_pair as "'.__("pair - fin").'"',
);
if (in_array($retourformulaire, $foreign_keys_extended["bureau"])) {
    $champAffiche = array_merge(
        $champAffiche_id,
        $champAffiche_voie,
        $champAffiche_numeros
    );
} elseif (in_array($retourformulaire, $foreign_keys_extended["voie"])) {
    $champAffiche = array_merge(
        $champAffiche_id,
        $champAffiche_bureau,
        $champAffiche_numeros
    );
} else {
    $champAffiche = array_merge(
        $champAffiche_id,
        $champAffiche_bureau,
        $champAffiche_voie,
        $champAffiche_numeros
    );
}
//
$champRecherche = $champAffiche;

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

/**
 * Tableau de liens
 */
$href[3]['lien'] = OM_ROUTE_MODULE_EDITION."&idx=";
$href[3]['id'] = "&amp;obj=electeurpardecoupage";
$href[3]['lib'] = "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\"".__("Electeurs par decoupage")."\">".__("Electeurs par decoupage")."</span>";
$href[3]['target'] = "_blank";
