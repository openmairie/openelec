<?php
/**
 * ...
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/trace.inc.php";

/**
 * Critère ORDER BY ou GROUP BY de la requête.
 * On souhaite un tri par date et heure décroissant pour toujours avoir la
 * dernière trace en premier.
 */
$tri = "ORDER BY trace.creationdate DESC, trace.creationtime DESC, type";

// Il y a trop d'enregistrements possibles sur cet objet, on ne souhaite pas
// avoir un select pour la pagination
if (isset($options) !== true) {
    $options = array();
}
$options[] = array(
    "type" => "pagination_select",
    "display" => false,
);
