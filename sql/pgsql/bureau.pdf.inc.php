<?php
/**
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
$DEBUG=0;

// document
$orientation="L";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=5;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B

// LIGNE tableau
$size=8; //taille POLICE
$height=4.6; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="234";// couleur fond  R 241
$C2fond1="240";// couleur fond  V 241
$C3fond1="245";// couleur fond  B 241
$C1fond2="255";// couleur fond  R
$C2fond2="255";// couleur fond  V
$C3fond2="255";// couleur fond  B

// titre
$libtitre = __("Liste des bureaux de vote"); // libelle titre
$flagsessionliste=0;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre="13";
// libelle entete colonnes
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=10;//hauteur ligne entete colonne
$C1fondentete="210";// couleur fond  R
$C2fondentete="216";// couleur fond  V
$C3fondentete="249";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
// couleur border
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
// largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )
$l0=15;
$l1=60;
$l2=100;
$l3=92;
$widthtableau=272;// -> ajouter $l0 à $lxx
$bt=1;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//-------
if ($_SESSION["niveau"] == "2") {
    $nbcol = 5;
} else {
    $nbcol = 4;
}
//-------
for ($i = 0; $i < $nbcol; $i++) {
    // Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
    ${"be".$i} = "L";
    //- Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
    ${"b".$i} = "L";
    // ALIGNEMENT entete colonne $ae0  à $ae.. ( $ae OBLIGATOIRE )
    ${"ae".$i} = "C";
    // ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )
    ${"a".$i} = "L";
    // ch(amps)n(umerique)d(ecimale)0 à nn / 999->non, 0 à nn->nbr decimale(s)
    ${"chnd".$i} = 999;
    // calcul et affichage totaux 1->Totaux 0->non
    ${"chtot".$i} = 0;
    // calcul et affichage moyenne 1->moyenne 0->non
    ${"chmoy".$i} = 0;
    //------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )
    ${"l".$i} = $widthtableau / $nbcol;
}
//
${"be".($nbcol-1)} = "LR";
${"b".($nbcol-1)} = "LR";

$l0 = 50;
${"l".($nbcol-4)} = 10;
${"l".($nbcol-3)} = 60;
${"l".($nbcol-1)} = 65;
${"l".($nbcol-2)} = ($widthtableau) - ${"l".($nbcol-4)} - ${"l".($nbcol-3)} - ${"l".($nbcol-1)};
if ($_SESSION["niveau"] == "2") {
    ${"l".($nbcol-2)} -= $l0;
}

// SQL
if ($_SESSION["niveau"] == "2") {
    //
    $sql = sprintf(
        'SELECT om_collectivite.libelle AS "%2$s", bureau.code AS "%3$s", bureau.libelle AS "%4$s", (bureau.adresse1||\' / \'||bureau.adresse2||\' / \'||bureau.adresse3) AS "%5$s", (canton.code||\' - \'||canton.libelle) as "%6$s" FROM %1$sbureau INNER JOIN %1$scanton ON bureau.canton=canton.id INNER JOIN %1$som_collectivite ON bureau.om_collectivite=om_collectivite.om_collectivite ORDER BY bureau.om_collectivite, bureau.code',
        DB_PREFIXE,
        __("Commune"),
        __("Code"),
        "Libelle",
        __("Adresse"),
        __("Canton")
    );
} else {
    //
    $sql = sprintf(
        'SELECT bureau.code AS "%3$s", bureau.libelle AS "%4$s", (bureau.adresse1||\' / \'||bureau.adresse2||\' / \'||bureau.adresse3) AS "%5$s", (canton.code||\' - \'||canton.libelle) as "%6$s" FROM %1$sbureau INNER JOIN %1$scanton ON bureau.canton=canton.id INNER JOIN %1$som_collectivite ON bureau.om_collectivite=om_collectivite.om_collectivite WHERE bureau.om_collectivite=%2$s ORDER BY bureau.om_collectivite, bureau.code',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        __("Code"),
        "Libelle",
        __("Adresse"),
        __("Canton")
    );
}
