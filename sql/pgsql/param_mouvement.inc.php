<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/param_mouvement.inc.php";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere select de la requete
$champAffiche = array(
    "code as \"".__("Code")."\"",
    "libelle as \"".__("Libelle")."\"",
    "typecat as \"".__("Categorie")."\"",
    "effet as \"".__("Date d'effet")."\"",
    "(codeinscription || coderadiation) as \"".__("Transfert INSEE")."\"",
    "case edition_carte_electeur 
        when '0' then 'Jamais' 
        when '1' then 'Toujours'
        when '2' then 'Lors de changement de bureau'
        else '' 
    end as \"".__("Impression")."\"",
);

// Gestion OMValidité - On affiche/cache les colonnes de validité si nécessaire
$champAffiche_datevalidite = array(
    "to_char(param_mouvement.om_validite_debut ,'DD/MM/YYYY') as \"".__("om_validite_debut")."\"",
    "to_char(param_mouvement.om_validite_fin ,'DD/MM/YYYY') as \"".__("om_validite_fin")."\"",
);
if ((isset($_GET["valide"]) && $_GET["valide"] == "false")) {
    $champAffiche = array_merge($champAffiche, $champAffiche_datevalidite);
}

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "code as \"".__("Code")."\"",
    "libelle as \"".__("Libelle")."\"",
    "typecat as \"".__("Categorie")."\"",
    "effet as \"".__("Date d'effet")."\"",
    "cnen as \"".__("Transfert INSEE")."\"",
    "codeinscription as \"".__("INSEE - Code INS")."\"",
    "coderadiation as \"".__("INSEE - Code RAD")."\"",
);
