<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/procuration.inc.php";

// Titre
$ent = __("Electeur")." -> ".__("Procuration");

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere FROM de la requete
$table = sprintf(
    '%1$sprocuration
        LEFT JOIN %1$som_collectivite 
            ON procuration.om_collectivite=om_collectivite.om_collectivite
        LEFT JOIN %1$selecteur AS mandant
            ON procuration.mandant=mandant.id 
        LEFT JOIN %1$sbureau AS bureau_mandant
            ON mandant.bureau=bureau_mandant.id
        LEFT JOIN %1$sliste as liste_mandant
            ON mandant.liste=liste_mandant.liste
        LEFT JOIN %1$selecteur AS mandataire
            ON procuration.mandataire=mandataire.id
        LEFT JOIN %1$sbureau AS bureau_mandataire
            ON mandataire.bureau=bureau_mandataire.id
        LEFT JOIN %1$sliste as liste_mandataire
            ON mandataire.liste=liste_mandataire.liste', // FROM
    DB_PREFIXE
);

// Critere select de la requete
$champAffiche = array(
    "procuration.id as \"".__("id")."\"",
    "CASE
        WHEN procuration.autorite_type = 'CSL' 
            THEN 'HF'
        WHEN procuration.autorite_type IN ('TRIB', 'PN', 'GN')
            THEN 'EF'
        ELSE '-'
    END as \"".__("types")."\"",
    "CASE
        WHEN mandant IS NOT NULL
            THEN (mandant.nom||' - '||mandant.prenom||' - '||to_char(mandant.date_naissance,'DD/MM/YYYY')|| ' - '||bureau_mandant.code||' - '||liste_mandant.liste_insee||' - '||mandant.ine)
        ELSE CONCAT(mandant_resume, ' - ', mandant_ine)
    END as \"".__("mandant")."\"",
    'CONCAT_WS(\' \', bureau_mandant.code, bureau_mandant.libelle) as "'.__('bureau du mandant').'"',
    "CASE
        WHEN mandataire IS NOT NULL
            THEN (mandataire.nom||' - '||mandataire.prenom||' - '||to_char(mandataire.date_naissance,'DD/MM/YYYY')|| ' - '||bureau_mandataire.code||' - '||liste_mandataire.liste_insee||' - '||mandataire.ine)
        ELSE CONCAT(mandataire_resume, ' - ', mandataire_ine)
    END as \"".__("mandataire")."\"",
    "to_char(procuration.debut_validite,'DD/MM/YYYY') as \"".__("debut")."\"",
    "to_char(procuration.fin_validite,'DD/MM/YYYY') as \"".__("fin")."\"",
    "procuration.statut as \"".__("statut")."\"",
    "id_externe",
);
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("collectivite")."\"");
}
// Champ sur lesquels la recherche est active
$champRecherche = array(
    "mandant.nom",
    "mandataire.nom",
    "mandant.prenom",
    "mandataire.prenom",
    "CONCAT_WS(' ', bureau_mandant.code, bureau_mandant.libelle) as \"bureau du mandant\"",
    "bureau_mandataire.code",
    "*mandant",
    "*mandataire"
);

// Critere where de la requete
/*$selection = "where mandant.om_collectivite=".intval($_SESSION["collectivite"])." and mandataire.om_collectivite=".intval($_SESSION["collectivite"])." ";*/

// Critere order by ou group by de la requete
$tri = " order by withoutaccent(lower(mandant.nom)), withoutaccent(lower(mandant.prenom)) ";

// L'action ajouter n'est pas disponible en mode multi
if ($f->getParameter("is_collectivity_multi") === true) {
    $tab_actions["corner"]["ajouter"] = null;
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    "reu_notification",
);
$sousformulaire_parameters = array(
    "reu_notification" => array(
        "title" => _("Notification(s)")
    ),
);

/**
 * Options
 */
//
$options = array();
$option = array(
    "type" => "condition",
    "field" => "procuration.statut",
    "case" => array(
        "0" => array(
            "values" => array("demande_a_transmettre", ),
            "style" => "procuration-demande_a_transmettre",
        ),
        "1" => array(
            "values" => array("demande_refusee", ),
            "style" => "procuration-demande_refusee",
        ),
        "2" => array(
            "values" => array("procuration_confirmee", ),
            "style" => "procuration-procuration_confirmee",
        ),
        "3" => array(
            "values" => array("procuration_annulee", ),
            "style" => "procuration-procuration_annulee",
        ),
    ),
);
array_push($options, $option);
//
$args = array(
    0 => array('', 't', 'f', ),
    1 => array(__('Tous'), __('Oui'), __('Non'), ),
);
$champs["id"] = array(
    "colonne" => array("id", "id_externe", ),
    "table" => "procuration",
    "type" => "text",
    "libelle" => __("id"),
    "help" => __("Id / Id externe"),
    "taille" => 8,
    "max" => 60,
);
$champs["mandant"] = array(
    "colonne" => array("mandant_resume", "mandant_ine"),
    "table" => "procuration",
    "type" => "text",
    "libelle" => __("mandant"),
    "help" => __("Mandant nom / prénoms / INE"),
    "taille" => 20,
    "max" => 60,
);
$champs["bureau"] = array(
    "colonne" => "id",
    "table" => "bureau_mandant",
    "libelle" => __("bureau du mandant"),
    "type" => "select",
    "subtype" => "sqlselect",
    "sql" => "SELECT
                id,
                CONCAT_WS(' ', code, libelle)
            FROM
                ".DB_PREFIXE."bureau"
);
$champs["mandataire"] = array(
    "colonne" => array("mandataire_resume", "mandataire_ine"),
    "table" => "procuration",
    "type" => "text",
    "libelle" => __("mandataire"),
    "help" => __("Mandataire nom / prénoms / INE"),
    "taille" => 20,
    "max" => 60,
);
$champs['debut_validite'] = array(
    'colonne' => 'debut_validite',
    'table' => 'procuration',
    'type' => 'date',
    'libelle' => __('début'),
    'taille' => 8,
    'where' => 'intervaldate',
);
$champs['fin_validite'] = array(
    'colonne' => 'fin_validite',
    'table' => 'procuration',
    'type' => 'date',
    'libelle' => __('fin'),
    'taille' => 8,
    'where' => 'intervaldate',
);
$champs['statut'] = array(
    'colonne' => 'statut',
    'table' => 'procuration',
    'type' => 'select',
    'libelle' => __('statut'),
);
$champs['vu'] = array(
    'colonne' => 'vu',
    'table' => 'procuration',
    'libelle' => __('vu ?'),
    "type" => "select",
    "subtype" => "manualselect",
    "args" => $args,
);
if ($f->getParameter("is_collectivity_multi") === true) {
    $champs['om_collectivite'] = array(
        'colonne' => 'om_collectivite',
        'table' => 'procuration',
        'type' => 'select',
        'libelle' => __('om_collectivite'),
    );
}
$options[] =  array(
    'type' => 'search',
    'display' => true,
    'advanced' => $champs,
    'default_form' => 'simple',
    'absolute_object' => 'procuration'
);
$options[] = array(
    "type" => "export",
    "export" => array(
        "csv" => array(
            "right" => $obj."_exporter",
        ),
    ),
);
