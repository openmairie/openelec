<?php

include "../gen/sql/pgsql/affectation.inc.php";

/*$tab_actions['left']['affectation'] = array(
    'lien' => OM_ROUTE_FORM.'&amp;obj='.$obj.'&amp;action=201&amp;retour=tab&amp;idx=',
    'id' => '',
    'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix pdf-16\" title=\"".__("Affectation")."\">".__("Affectation")."</span>",
    'ajax' => false,
    "target" => "_blank",
);*/

// Filtre listing sous formulaire - composition_scrutin
if (in_array($retourformulaire, $foreign_keys_extended["composition_scrutin"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array('composition_scrutin.libelle as "'.__("composition_scrutin").'"', )
    );
}

$champAffiche = array(
    'affectation.id as "'.__("id").'"',
    'elu.nom as "'.__("elu").'"',
    'composition_scrutin.libelle as "'.__("composition_scrutin").'"',
    'periode.libelle as "'.__("periode").'"',
    'poste.libelle as "'.__("poste").'"',
    '(bureau.code || \' \' || bureau.libelle) as "'.__("bureau").'"',
    "case affectation.decision when 't' then 'Oui' else 'Non' end as \"".__("decision")."\"",
    'candidat.nom as "'.__("candidat").'"',
);
