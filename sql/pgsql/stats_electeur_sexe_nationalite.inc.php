<?php
/**
 * $Id$
 *
 */

//
$sql_nationalite = sprintf(
    'SELECT * FROM %1$snationalite ORDER BY nationalite.libelle_nationalite',
    DB_PREFIXE
);

if (isset($row['code'])) {
    //
    $sqlB = sprintf(
        'SELECT count(electeur.code_nationalite) FROM %1$selecteur WHERE electeur.liste=\'%3$s\' and electeur.code_nationalite=\'%4$s\' and electeur.om_collectivite=%2$s',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $row['code']
    );
    //
    $sql1 = sprintf(
        'SELECT count(electeur.sexe) FROM %1$selecteur WHERE sexe=\'M\' AND electeur.liste=\'%3$s\' and electeur.code_nationalite=\'%4$s\' and electeur.om_collectivite=%2$s',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $row['code']
    );
    //
    $sql2 = sprintf(
        'SELECT count(electeur.sexe) FROM %1$selecteur WHERE sexe=\'F\' AND electeur.liste=\'%3$s\' and electeur.code_nationalite=\'%4$s\' and electeur.om_collectivite=%2$s',
        DB_PREFIXE,
        intval($_SESSION["collectivite"]),
        $_SESSION["liste"],
        $row['code']
    );
}
