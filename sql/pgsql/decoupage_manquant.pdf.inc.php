<?php
$DEBUG=0;
// ------------------------document---------------------------------------------
$orientation="P";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=5;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B
//-------------------------LIGNE tableau----------------------------------------
$size=9; //taille POLICE
$height=4; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="241";// couleur fond  R
$C2fond1="241";// couleur fond  V
$C3fond1="241";// couleur fond  B
$C1fond2="255";// couleur fond  R
$C2fond2="255";// couleur fond  V
$C3fond2="255";// couleur fond  B
//-------------------------- titre----------------------------------------------
$libtitre = utf8_decode(__("Listing des voies sans découpage")); // libelle titre
$flagsessionliste=0;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre="12";
//--------------------------libelle entete colonnes-----------------------------
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=10;//hauteur ligne entete colonne
$C1fondentete="210";// couleur fond  R
$C2fondentete="216";// couleur fond  V
$C3fondentete="249";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
//------ Borderentete colonne $be0  à $be.. ( $be OBLIGATOIRE )
$be0="L";
$be1="L";
$be2="L";
$be3="LR";
// ------ couleur border--------------------------------------------------------
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
//------ Border cellule colonne $b0  à $b.. ( $b OBLIGATOIRE )
$b0="L";
$b1="L";
$b2="L";
$b3="LR";
//------ ALIGNEMENT entete colonne $ae0  à $ae.. ( $ae OBLIGATOIRE )
$ae0="C";
$ae1="C";
$ae2="C";
$ae3="R";
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$l0=10;
$l1=70;
$l2=97;
$l3=10;
$widthtableau=187;// -> ajouter $l0 à $lxx
$bt=1;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )------------
$a0="C";
$a1="L";
$a2="L";
$a3="R";
//--------------------------SQL-------------------------------------------------
$sql = sprintf(
    'SELECT
        voie.code,
        voie.libelle_voie as voie,
        (bureau.code||\' - \'||bureau.libelle) as bureau,
        count(electeur.id) as nb
    FROM
        %1$svoie
        JOIN %1$selecteur ON electeur.code_voie=voie.code
        JOIN %1$sbureau ON electeur.bureau=bureau.id
        LEFT JOIN %1$sdecoupage ON voie.code=decoupage.code_voie
    WHERE
        decoupage.code_voie IS NULL AND
        voie.om_collectivite=%2$s AND
        bureau.om_collectivite=%2$s AND
        electeur.om_collectivite=%2$s
    GROUP BY
        voie.code,
        voie.libelle_voie,
        bureau.code,
        bureau.libelle
    ORDER BY
        voie.libelle_voie,
        nb DESC
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
