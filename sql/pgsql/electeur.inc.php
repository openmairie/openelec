<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/electeur.inc.php";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

//
if ($f->getParameter("is_collectivity_multi") === true) {
    // Critere FROM de la requete
    $table = sprintf(
        '%1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id INNER JOIN %1$som_collectivite ON electeur.om_collectivite=om_collectivite.om_collectivite LEFT JOIN %1$svoie ON electeur.code_voie=voie.code LEFT JOIN %1$sliste ON electeur.liste=liste.liste', // FROM
        DB_PREFIXE
    );
    // Critere select de la requete
    $champAffiche = array (
        "electeur.id as \"id\"",
        "electeur.ine as \"".__("ine")."\"",
        "nom as \"".__("Nom")."\"",
        "prenom as \"".__("Prenom")."\"",
        "nom_usage as \"".__("Nom d'usage")."\"",
        "(to_char(date_naissance,'DD/MM/YYYY')||'<br />".__("a")." '||libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')') as \"".__("Date et lieu de naissance")."\"",
        "case resident
                when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || electeur.libelle_voie)
                when False then electeur.libelle_voie
            end)
                when 'Oui' then adresse_resident
            end || '<br />' ||case resident
                when 'Non' then electeur.complement
                when 'Oui' then (complement_resident || ' ' || cp_resident || ' - ' || ville_resident)
            end as \"".__("Adresse")."\"",
        "liste.liste_insee as \"".__("liste")."\"",
        "om_collectivite.libelle as \"".__("om_collectivite")."\"",
    );
    // Champ sur lesquels la recherche est active
    $champRecherche = array(
        "electeur.id as \"id\"",
        "nom as \"".__("Nom")."\"",
        "prenom as \"".__("Prenom")."\"",
        "nom_usage as \"".__("Nom d'usage")."\"",
        "to_char(date_naissance,'DD/MM/YYYY') as \"".__("Date de naissance")."\"",
        "bureauforce as \"".__("bureau force")."\"",
        "liste.liste_insee as \"".__("liste")."\"",
        "om_collectivite.libelle as \"".__("om_collectivite")."\"",
    );
    // Critere where de la requete
    $selection = "";
    // Critere order by ou group by de la requete
    $tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
} else {
    // Critere FROM de la requete
    $table = sprintf(
        '%1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id LEFT JOIN %1$svoie ON electeur.code_voie=voie.code LEFT JOIN %1$sliste ON electeur.liste=liste.liste', // FROM
        DB_PREFIXE
    );
    // Critere select de la requete
    $champAffiche = array(
        "electeur.id as \"id\"",
        "electeur.ine as \"".__("ine")."\"",
        "nom as \"".__("Nom")."\"",
        "prenom as \"".__("Prenom")."\"",
        "nom_usage as \"".__("Nom d'usage")."\"",
        "to_char(date_naissance,'DD/MM/YYYY') as \"".__("Date de naissance")."\"",
        "libelle_lieu_de_naissance||' <br />('||libelle_departement_naissance||')' as \"".__("Lieu de naissance")."\"",
        "case resident
                when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || electeur.libelle_voie)
                when False then electeur.libelle_voie
            end)
                when 'Oui' then adresse_resident
            end || '<br />' ||case resident
                when 'Non' then electeur.complement
                when 'Oui' then (complement_resident || ' ' || cp_resident || ' - ' || ville_resident)
            end as \"".__("Adresse")."\"",
        "bureau.code as \"".__("Bureau")."\"",
        "typecat as \"".__("En cours")."\"",
        "liste.liste_insee as \"".__("liste")."\"",
    );
    // Champ sur lesquels la recherche est active
    $champRecherche = array(
        "electeur.id as \"id\"",
        "electeur.ine as \"".__("ine")."\"",
        "nom as \"".__("Nom")."\"",
        "prenom as \"".__("Prenom")."\"",
        "nom_usage as \"".__("Nom d'usage")."\"",
        "bureau.code as \"".__("Bureau")."\"",
        "to_char(date_naissance,'DD/MM/YYYY') as \"".__("Date de naissance")."\"",
        "bureauforce as \"".__("bureau force")."\"",
        "liste.liste_insee as \"".__("liste")."\"",
    );
    // Critere where de la requete
    $selection = " where electeur.om_collectivite=".intval($_SESSION["collectivite"])." ";
    // Critere order by ou group by de la requete
    $tri = " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
}

//
$tab_actions["left"]["consulter"] = array(
    'lien' => OM_ROUTE_FORM."&obj=electeur&action=3&idx=",
    'id' => "&amp;premier=".$premier."&amp;tricol=".$tricol."&amp;advs_id=".$advs_id."&amp;valide=".$valide,
    'lib' => "<span class=\"om-icon om-icon-16 om-icon-fix ficheelecteur-16\" title=\"".__("Fiche de l'electeur")."\">".__("Fiche de l'electeur")."</span>",
    "right" => array("electeur_consulter", "electeur"),
);
$tab_actions['content'] = $tab_actions["left"]["consulter"];
$tab_actions['corner'] = array();

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    "mouvement",
);
$sousformulaire_parameters = array(
    "mouvement" => array(
        "title" => _("Mouvement(s)")
    ),
);

/**
 * Options
 */
//
$options = array();
//
$option = array(
    "type" => "condition",
    "field" => "typecat",
    "case" => array(
        "0" => array(
            "values" => array("modification", ),
            "style" => "modification-encours",
        ),
        "1" => array(
            "values" => array("radiation", ),
            "style" => "radiation-encours",
        ),
    ),
);
array_push($options, $option);
//
$option = array(
    "type" => "pagination_select",
    "display" => false,
);
array_push($options, $option);
//
$args = array(
    0 => array('', 'Oui', 'Non', ),
    1 => array(__('Tous'), __('Oui'), __('Non'), ),
);
$advs_fields = array(
    "id" => array(
        "colonne" => "id",
        "table" => "electeur",
        "type" => "text",
        "libelle" => "id",
        'help' => __("identifiant numérique de l'électeur interne à openElec"),
        "taille" => 8,
        "max" => 60,
    ),
    "ine" => array(
        "colonne" => "ine",
        "table" => "electeur",
        "type" => "text",
        "libelle" => __("ine"),
        'help' => __("identifiant national de l'électeur (REU)"),
        "taille" => 8,
        "max" => 60,
    ),
    "nom" => array(
        "colonne" => array("nom", "nom_usage"),
        "table" => "electeur",
        "type" => "text",
        "libelle" => __("nom"),
        "help" => __("nom patronymique / nom d'usage de l'électeur"),
        "taille" => 20,
        "max" => 60,
    ),
    "prenoms" => array(
        "colonne" => "prenom",
        "table" => "electeur",
        "type" => "text",
        "libelle" => __("prénoms"),
        "taille" => 20,
        "max" => 60,
    ),
    "date_naissance" => array(
        "colonne" => "date_naissance",
        "table" => "electeur",
        "type" => "date",
        "libelle" => __("date de naissance"),
        "taille" => 8,
        "where" => "intervaldate",
    ),
    "bureau" => array(
        "colonne" => "bureau",
        "table" => "electeur",
        "libelle" => __("bureau"),
        "type" => "select",
    ),
    "adresse" => array(
        'table' => 'electeur',
        'colonne' => 'libelle_voie',
        'type' => 'text',
        'taille' => 30,
        'max' => 60,
        'libelle' => __('adresse'),
        "help" => __("Nom de la voie du bureau de rattachement de l'électeur")
    ),
    "liste" => array(
        "colonne" => "liste",
        "table" => "electeur",
        "type" => "select",
        "libelle" => __("liste"),
    ),
    "bureauforce" => array(
        "colonne" => "bureauforce",
        "table" => "electeur",
        "libelle" => __("bureau forcé ?"),
        "type" => "select",
        "subtype" => "manualselect",
        "args" => $args
    ),
);
if ($f->getParameter("is_collectivity_multi") === true) {
    $advs_fields['om_collectivite'] = array(
        'colonne' => 'om_collectivite',
        'table' => 'electeur',
        'type' => 'select',
        'libelle' => __('om_collectivite'),
    );
}
$options[] = array(
    'type' => 'search',
    'display' => true,
    'advanced' => $advs_fields,
    'export' => array('csv', 'etiquettes', ),
    'default_form' => 'simple',
    'absolute_object' => 'electeur',
);
$options[] = array(
    "type" => "export",
    "export" => array(
        "csv" => array(
            "right" => $obj."_exporter",
        ),
        "etiquettes" => array(
            "right" => $obj."_etiquettes_from_listing",
            "url" => OM_ROUTE_FORM."&obj=".$obj."&action=367&idx=0",
        ),
    ),
);

// Extra Parameters
if (isset($obj)
    && $obj == "electeur") {
    //
    $exact = "";
    if (isset($_GET['exact']) && $_GET['exact'] == true) {
        $exact = $_GET['exact'];
    }
    $nom = "";
    if (isset($_GET['nom'])) {
        $nom = $_GET['nom'];
    }
    $datenaissance = "";
    if (isset($_GET['datenaissance'])) {
        $datenaissance = $_GET['datenaissance'];
    }
    $idxelecteur = "]";
    if (isset($_GET['idxelecteur'])) {
        $idxelecteur = $_GET['idxelecteur'];
    }
    $marital = "";
    if (isset($_GET['marital'])) {
        $marital = $_GET['marital'];
    }
    $prenom = "";
    if (isset($_GET['prenom'])) {
        $prenom = $_GET['prenom'];
    }
    $origin = "electeur";
    if (isset($_GET['origin'])) {
        $origin = $_GET['origin'];
    }
    $retour = "";
    if (isset($_GET['retour'])){
        $retour = $_GET ['retour'];
    }
    $extra_parameters = array(
        "retour" => $retour,
        "nom" => $nom,
        "prenom" => $prenom,
        "exact" => $exact,
        "datenaissance" => $datenaissance,
        "marital" => $marital,
        "origin" => $origin,
        "idxelecteur" => $idxelecteur,
    );
}
