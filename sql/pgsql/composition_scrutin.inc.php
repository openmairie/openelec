<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id: composition_scrutin.inc.php 1715 2019-02-19 15:28:11Z fmichon $
 */

//
include "../gen/sql/pgsql/composition_scrutin.inc.php";

//
$tab_title = __("Composition");

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'candidat',
    'affectation',
    'candidature',
    'composition_bureau',
);
$sousformulaire_parameters = array(
    "candidat" => array(
        "title" => _("Candidat(s) / Liste(s)")
    ),
    "affectation" => array(
        "title" => _("Affectation(s) d'élu")
    ),
    "candidature" => array(
        "title" => _("Candidature(s) d'agent")
    ),
    "composition_bureau" => array(
        "title" => _("Bureaux")
    ),
);
