<?php
/**
 * REQMO - list__mouvement__pour_une_categorie
 *
 * Contexte MONO uniquement.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

//
$reqmo['sql'] = sprintf(
    'SELECT
        numero_bureau as no_dans_bureau,
        [typecat],
        types as type_mouvt,
        [date_modif],
        [civilite],
        [nom],
        [prenom],
        [nom_usage],
        [sexe],
        [to_char(date_naissance, \'DD/MM/YYYY\') as date_de_naissance],
        [code_departement_naissance],
        [libelle_departement_naissance],
        [code_lieu_de_naissance],
        [libelle_lieu_de_naissance],
        [numero_habitation],
        [complement_numero],
        [libelle_voie],
        [complement],
        [provenance],
        [libelle_provenance],
        [bureau_de_vote_code],
        [ancien_bureau_de_vote_code],
        [observation],
        [tableau],
        [to_char(date_j5, \'DD/MM/YYYY\') as date_j5],
        [to_char(date_tableau, \'DD/MM/YYYY\') as date_du_tableau],
        [envoi_cnen],
        [date_cnen],
        liste
    FROM
        %1$smouvement
        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
    WHERE
        param_mouvement.typecat=\'[CHOIX_TYPECAT]\'
        AND mouvement.om_collectivite=%2$s
    ORDER BY
        [tri]
    ',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
//
$reqmo["tri"]= array(
    "nom",
    "nom_usage",
    "prenom",
    "date_modif",
);
//
$reqmo['typecat']="checked";
$reqmo['civilite']="checked";
$reqmo['nom']="checked";
$reqmo['prenom']="checked";
$reqmo['nom_usage']="checked";
$reqmo['sexe']="checked";
$reqmo['date_modif']="checked";
$reqmo['date_de_naissance']="checked";
$reqmo['code_departement_naissance']="checked";
$reqmo['libelle_departement_naissance']="checked";
$reqmo['code_lieu_de_naissance']="checked";
$reqmo['libelle_lieu_de_naissance']="checked";
$reqmo['numero_habitation']="checked";
$reqmo['libelle_voie']="checked";
$reqmo['complement']="checked";
$reqmo['provenance']="checked";
$reqmo['libelle_provenance']="checked";
$reqmo['ancien_bureau_de_vote_code']="checked";
$reqmo['observation']="checked";
$reqmo['tableau']="checked";
$reqmo['date_j5']="checked";
$reqmo['date_du_tableau']="checked";
$reqmo['envoi_cnen']="checked";
$reqmo['date_cnen']="checked";
$reqmo['bureau_de_vote_code']="checked";
//
$reqmo['CHOIX_TYPECAT'] = sprintf(
    'SELECT param_mouvement.typecat, (param_mouvement.typecat) FROM %1$sparam_mouvement GROUP BY param_mouvement.typecat ORDER BY param_mouvement.typecat',
    DB_PREFIXE
);
