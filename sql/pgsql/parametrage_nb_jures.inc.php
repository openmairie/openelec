<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/parametrage_nb_jures.inc.php";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere FROM de la requete
$table = sprintf(
    '%1$sparametrage_nb_jures LEFT JOIN %1$scanton ON parametrage_nb_jures.canton=canton.id LEFT JOIN %1$som_collectivite ON parametrage_nb_jures.om_collectivite=om_collectivite.om_collectivite', // FROM
    DB_PREFIXE
);

// Critere select de la requete
$champAffiche = array(
    "parametrage_nb_jures.id as \"".__("Id")."\"",
    "canton.libelle as \"".__("Canton")."\"",
    "parametrage_nb_jures.nb_jures as \"".__("Nombre de jures")."\"",
);
if ($f->is_option_enabled('option_module_jures_suppleants')) {
    $champAffiche[] = sprintf('parametrage_nb_jures.nb_suppleants as "%s"', __("Nombre de suppléants"));
}
if ($f->isMulti() == true) {
    array_push($champAffiche, "om_collectivite.libelle as \"".__("Collectivite")."\"");
}

// Champ sur lesquels la recherche est active
$champRecherche = array(
);

// Critere where de la requete
if ($f->isMulti() == true) {
    $selection = "";
} else {
    $selection = "WHERE parametrage_nb_jures.om_collectivite=".intval($_SESSION["collectivite"])."";
}

// Critere order by ou group by de la requete
$tri = "order by om_collectivite.libelle, canton.libelle";

/**
 * Tableau de liens
 */
$tab_actions["corner"] = array();
