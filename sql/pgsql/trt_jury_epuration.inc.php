<?php
/**
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Requete pour compter le nombre d'electeur(s) selectionne(s) comme jure
 */
$query_count_jury = sprintf(
    "SELECT
        count(*)
    FROM
        %selecteur
    WHERE
        electeur.om_collectivite = %d
        AND electeur.jury = 1
        AND electeur.liste = '%s'",
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

$query_count_jury_sup = sprintf(
    "SELECT
        count(*)
    FROM
        %selecteur
    WHERE
        electeur.om_collectivite = %d
        AND electeur.jury = 2
        AND electeur.liste = '%s'",
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);

/**
 * Requete pour de-selectionner tous les electeurs selectionnes comme jure
 */
$query_update_jury = sprintf(
    "UPDATE %selecteur
    SET
        jury = 0,
        profession = '',
        motif_dispense_jury = ''
    WHERE
        electeur.om_collectivite = %d
        AND (electeur.jury = 1
            OR electeur.jury = 2)
        AND electeur.liste = '%s'",
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"]
);
