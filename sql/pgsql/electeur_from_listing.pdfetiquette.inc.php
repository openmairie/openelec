<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id: electeur.pdfetiquette.inc.php 1771 2019-02-27 11:25:18Z fmichon $
 */

/**
 *
 */
if (!isset($params)) {
    $params = array();
}
$from_where = "";
if (isset($params["query"])) {
    $query = explode("FROM", $params["query"]);
    $from_where = $query[1];
}

/**
 *
 */
$champs_compteur = array();
$img = array();
$texte = array();
$champs = array(
    'ligne1' => array(
        '',
        '',
        array(
            $parametrage_etiquettes["etiquette_marge_int_gauche"],
            $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 0),
            ($parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"])*(2/3),
            0,
            8
        ),
        0,
    ),
    'ligne2' => array(
        '',
        '',
        array(
            $parametrage_etiquettes["etiquette_marge_int_gauche"],
            $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 1),
            $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
            1,
            0,
        ),
        0,
    ),
    'ligne3' => array(
        '',
        '',
        array(
            $parametrage_etiquettes["etiquette_marge_int_gauche"],
            $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 2),
            $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
            1,
            0,
        ),
        0,
    ),
    'ligne4' => array(
        '',
        '',
        array(
            $parametrage_etiquettes["etiquette_marge_int_gauche"],
            $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 3),
            $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
            1,
            0,
        ),
        0,
    ),
    'ligne5' => array(
        '',
        '',
        array(
            $parametrage_etiquettes["etiquette_marge_int_gauche"],
            $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 4),
            $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
            1,
            0,
        ),
        0,
    ),
    'ligne6' => array(
        '',
        '',
        array(
            $parametrage_etiquettes["etiquette_marge_int_gauche"],
            $parametrage_etiquettes["etiquette_marge_int_haut"] + ($parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"] * 5),
            $parametrage_etiquettes["etiquette_largeur"] - $parametrage_etiquettes["etiquette_marge_int_gauche"] - $parametrage_etiquettes["etiquette_marge_int_droite"],
            1,
            0,
        ),
        0,
    ),
);

$sql = sprintf(
    'SELECT 
        \'\'
            AS ligne1, 
        CASE 
            WHEN (electeur.nom_usage <> \'\' and electeur.nom_usage <> electeur.nom)
                THEN (upper(electeur.civilite) ||\' \'||electeur.nom||\' - \'||electeur.nom_usage||\' \'||electeur.prenom ) 
            ELSE (upper(electeur.civilite) ||\' \'||electeur.nom||\' \'||electeur.prenom) 
        END
            AS ligne2,
        \'\'
            AS ligne3, 
        CASE resident 
            WHEN \'Non\' THEN (
                CASE numero_habitation 
                    WHEN 0 THEN \'\' 
                    ELSE (numero_habitation||\' \') 
                END 
                || 
                CASE complement_numero 
                    WHEN \'\' THEN \'\' 
                    ELSE (complement_numero||\' \') 
                END 
            ||electeur.libelle_voie) 
            WHEN \'Oui\' THEN adresse_resident 
        END
            AS ligne4, 
        CASE resident 
            WHEN \'Non\' THEN electeur.complement 
            WHEN \'Oui\' THEN complement_resident 
        END
            AS ligne5, 
        CASE resident 
            WHEN \'Non\' THEN (voie.cp||\' \'||voie.ville) 
            WHEN \'Oui\' THEN (cp_resident||\' \'||ville_resident) 
        END
            AS ligne6 
    FROM %1$s',
    $from_where
);
