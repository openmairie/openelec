<?php
$DEBUG=0;
// ------------------------document---------------------------------------------
$orientation="L";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=5;// marge gauche
$margetop=5;// marge haut
$margeright=5;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B
//-------------------------LIGNE tableau----------------------------------------
$size=10; //taille POLICE
$height=4.6; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="241";// couleur fond  R
$C2fond1="241";// couleur fond  V
$C3fond1="241";// couleur fond  B
$C1fond2="255";// couleur fond  R
$C2fond2="255";// couleur fond  V
$C3fond2="255";// couleur fond  B
//-------------------------- titre----------------------------------------------
$libtitre="Liste des mouvements à archiver du tableau du ".$datetableau_formatted_for_display." de la liste ".$nolibliste; // libelle titre
$flagsessionliste=1;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="B";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre="15";
//--------------------------libelle entete colonnes-----------------------------
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=10;//hauteur ligne entete colonne
$C1fondentete="210";// couleur fond  R
$C2fondentete="216";// couleur fond  V
$C3fondentete="249";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
//------ Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
$be0="L";
$be1="L";
$be2="L";
$be3="L";
$be4="L";
$be5="L";
$be6="LR";
// ------ couleur border--------------------------------------------------------
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
//------ Border cellule colonne $b0  à $b.. ( $b OBLIGATOIRE )
$b0="L";
$b1="L";
$b2="L";
$b3="L";
$b4="L";
$b5="L";
$b6="LR";
//------ ALIGNEMENT entete colonne $ae0  à $ae.. ( $ae OBLIGATOIRE )
$ae0="C";
$ae1="C";
$ae2="C";
$ae3="C";
$ae4="C";
$ae5="C";
$ae6="C";
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$l0=20;
$l1=60;
$l2=60;
$l3=30;
$l4=50;
$l5=30;
$l6=30;
$widthtableau=280;// -> ajouter $l0 à $lxx
$bt=1;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )------------
$a0="C";
$a1="L";
$a2="L";
$a3="C";
$a4="L";
$a5="C";
$a6="L";
//--------------------------SQL-------------------------------------------------
$sql = sprintf(
    'SELECT mouvement.bureau_de_vote_code AS bureau, mouvement.nom, mouvement.prenom, to_char(date_naissance, \'DD/MM/YYYY\') AS Naissance, param_mouvement.libelle, (mouvement.tableau || \' \' || to_char(date_j5, \'DD/MM/YYYY\')) AS tableau, (mouvement.envoi_cnen || \' \' || to_char(date_cnen, \'DD/MM/YYYY\')) AS insee FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.date_tableau=\'%2$s\' AND mouvement.etat=\'trs\' AND mouvement.liste=\'%3$s\' AND mouvement.om_collectivite=%4$s ORDER BY withoutaccent(lower(mouvement.nom)), withoutaccent(lower(mouvement.prenom))',
    DB_PREFIXE,
    $datetableau_formatted_for_database,
    $_SESSION["liste"],
    intval($_SESSION["collectivite"])
);
