<?php
/**
 * Ce fichier contient les requêtes utilisées lors de la normalisation des voies
 *
 * @package openelec
 * @version SVN : $Id$
 */

$sql_voies = sprintf(
    'SELECT * FROM %1$svoie WHERE voie.om_collectivite=%2$s ORDER BY voie.libelle_voie ASC',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);

$sql_voie_infos = sprintf(
    'SELECT * FROM %1$svoie WHERE voie.code=\'<id>\'',
    DB_PREFIXE
);

$sql_next_voie_id = sprintf(
    'SELECT max(to_number(voie.code, \'999999\')) FROM %1$svoie;',
    DB_PREFIXE
);

$sql_update_decoupages = sprintf(
    'SELECT decoupage.id FROM %1$sdecoupage WHERE decoupage.code_voie=\'<old_code>\'',
    DB_PREFIXE
);

$sql_update_electeurs = sprintf(
    'SELECT electeur.id FROM %1$selecteur WHERE electeur.code_voie=\'<old_code>\'',
    DB_PREFIXE
);

$sql_update_mouvements = sprintf(
    'SELECT mouvement.id, mouvement.electeur_id FROM %1$smouvement WHERE mouvement.code_voie=\'<old_code>\'',
    DB_PREFIXE
);

$sql_select_old_voies = sprintf(
    'SELECT voie.code FROM %1$svoie WHERE voie.code=\'<old_code>\'',
    DB_PREFIXE
);

$sql_delete_old_voies = sprintf(
    'DELETE FROM %1$svoie WHERE voie.code=\'<old_code>\'',
    DB_PREFIXE
);
