<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/liste.inc.php";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere select de la requete
$champAffiche = array(
    "liste as \"".__("Code")."\"",
    "libelle_liste as \"".__("Libelle")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "liste as \"".__("Code")."\"",
    "libelle_liste as \"".__("Libelle")."\"",
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = "order by liste";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 *
 * On ne souhaite pas avoir electeur, mouvement et archive en onglet.
 */
$sousformulaire = array(
    'numerobureau',
);
$sousformulaire_parameters = array(
    "numerobureau" => array(
        "title" => __("Séquences - Numéro Bureau"),
    ),
);
