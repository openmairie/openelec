<?php
/**
 * REQMO - list__electeur_jury
 *
 * Export de la liste des électeurs marqués comme sélectionnés comme juré d'assises.
 *
 * Contexte MONO uniquement.
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/reqmo_common_queries.inc.php";

//
$reqmo['sql'] = sprintf(
    'SELECT [civilite], [nom], [prenom], [nom_usage], [sexe], [to_char(date_naissance, \'DD/MM/YYYY\') as date_de_naissance], [code_departement_naissance], [libelle_departement_naissance], [code_lieu_de_naissance], [libelle_lieu_de_naissance], [numero_habitation], [libelle_voie], [complement], [resident], [adresse_resident], [complement_resident], [cp_resident], [ville_resident], [provenance], [libelle_provenance], [numero_bureau], [liste], [canton.code as canton_code], [canton.libelle as canton_libelle], [profession], [motif_dispense_jury] FROM %1$selecteur LEFT JOIN %1$sbureau on electeur.bureau=bureau.id LEFT JOIN %1$scanton on bureau.canton=canton.id WHERE electeur.jury=1 AND electeur.om_collectivite=%2$s ORDER BY [tri]',
    DB_PREFIXE,
    intval($_SESSION["collectivite"])
);
//
$reqmo["tri"]= array(
    "nom",
    "nom_usage",
    "prenom",
);
//
$reqmo['civilite']="checked";
$reqmo['nom']="checked";
$reqmo['prenom']="checked";
$reqmo['nom_usage']="checked";
$reqmo['sexe']="checked";
$reqmo['date_de_naissance']="checked";
$reqmo['code_departement_naissance']="checked";
$reqmo['libelle_departement_naissance']="checked";
$reqmo['code_lieu_de_naissance']="checked";
$reqmo['libelle_lieu_de_naissance']="checked";
$reqmo['numero_habitation']="checked";
$reqmo['libelle_voie']="checked";
$reqmo['complement']="checked";
$reqmo['resident']="checked";
$reqmo['adresse_resident']="checked";
$reqmo['complement_resident']="checked";
$reqmo['cp_resident']="checked";
$reqmo['ville_resident']="checked";
$reqmo['provenance']="checked";
$reqmo['libelle_provenance']="checked";
$reqmo['numero_bureau']="checked";
$reqmo['liste']="checked";
$reqmo['canton_code']="checked";
$reqmo['canton_libelle']="checked";
$reqmo['profession']="checked";
$reqmo['motif_dispense_jury']="checked";
