<?php
/**
 *  
 *
 * @package openelec
 * @version SVN : $Id$
 */

include "../sql/pgsql/mouvement.inc.php";

if ($obj == "modification" || $obj == "radiation") {
    // //
    // if (isset($_GET['idx'])) {
    //     $idx = $_GET['idx'];
    //     if (isset($_GET['ids'])) {
    //         $maj = 2;
    //     } else {
    //         $maj = 1;
    //     }
    // } else {
    //     if (isset($_GET['maj'])) {
    //         $maj = $_GET['maj'];
    //         $idx = "]";
    //     } else {
    //         $idx = "]";
    //         $maj = 0;
    //     }
    // }
    //
    $exact = "";
    if (isset($_GET['exact']) && $_GET['exact'] == true) {
        $exact = $_GET['exact'];
    }
    $nom = "";
    if (isset($_GET['nom'])) {
        $nom = $_GET['nom'];
    }
    $datenaissance = "";
    if (isset($_GET['datenaissance'])) {
        $datenaissance = $_GET['datenaissance'];
    }
    $idxelecteur = "]";
    if (isset($_GET['idxelecteur'])) {
        $idxelecteur = $_GET['idxelecteur'];
    }
    $marital = "";
    if (isset($_GET['marital'])) {
        $marital = $_GET['marital'];
    }
    $prenom = "";
    if (isset($_GET['prenom'])) {
        $prenom = $_GET['prenom'];
    }
    $idrad = "";
    if (isset($_GET['idrad'])) {
        $idrad = $_GET['idrad'];
    }
    if ($obj == "modification") {
        $origin = "electeur_modification";
        if (isset($_GET['origin'])) {
            $origin = $_GET['origin'];
        }
    } elseif ($obj == "radiation") {
        $origin = "electeur_radiation";
        if (isset($_GET['origin'])) {
            $origin = $_GET['origin'];
        }
    }
    $extra_parameters = array(
        "nom" => $nom,
        "prenom" => $prenom,
        "exact" => $exact,
        "datenaissance" => $datenaissance,
        "marital" => $marital,
        "origin" => $origin,
        "idxelecteur" => $idxelecteur,
        "idrad" => $idrad,
    );
}
