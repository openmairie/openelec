<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

$champAffiche = array(
    "mouvement.id as \"".__("Id")."\"",
    "to_char(date_demande,'DD/MM/YYYY') as \"".__("date de demande")."\"",
    "nom as \"".__("Nom")."\"",
    "prenom as \"".__("Prenom")."\"",
    "nom_usage as \"".__("Nom d'usage")."\"",
    "to_char(date_naissance,'DD/MM/YYYY') as \"".__("Date de naissance")."\"",
    "(libelle_lieu_de_naissance||' ('||libelle_departement_naissance||')') as \"".__("Lieu de naissance")."\"",
    "case resident
        when 'Non' then (
            case numero_habitation > 0
                when True then (numero_habitation || ' ' || complement_numero || ' ' || mouvement.libelle_voie)
                when False then mouvement.libelle_voie
            end
        )
        when 'Oui' then 
            adresse_resident
    end as adresse_ligne_1",
    "case resident
        when 'Non' then 
            mouvement.complement
        when 'Oui' then 
            trim(complement_resident)
    end as adresse_ligne_2",
    "case resident
        when 'Non' then 
            trim(voie.cp || ' ' || voie.ville)
        when 'Oui' then 
            trim(cp_resident || ' ' || ville_resident)
    end as adresse_ligne_3",
    "param_mouvement.libelle as \"".__("Mouvement")."\"",
    "mouvement.bureau_de_vote_code as \"".__("Bureau")."\"",
    "to_char(date_tableau,'DD/MM/YYYY') as \"".__("Tableau du")."\"",
    "etat as \"".__("état")."\"",
    "statut as \"".__("Statut")."\"",
    "to_char(date_j5,'DD/MM/YYYY') as \"".__("Traite le")."\"",
    "liste.liste_insee as \"".__("liste")."\"",
);
//
if ($f->getParameter("is_collectivity_multi") === true) {
    $champAffiche[] = "om_collectivite.libelle as \"".__("om_collectivite")."\"";
}
