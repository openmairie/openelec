<?php
/**
 * $Id$
 * Ce fichier est appelé par le fichier app/archivage.php
 */

//1
$sql = sprintf(
    'SELECT * FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.liste=\'%3$s\' AND mouvement.om_collectivite=%2$s AND mouvement.date_tableau=\'%4$s\' AND mouvement.etat=\'trs\' ORDER BY withoutaccent(lower(mouvement.nom)), withoutaccent(lower(mouvement.prenom))',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"],
    $datetableau
);


$query_select_piece = sprintf(
    'SELECT piece.* FROM %1$spiece INNER JOIN %1$smouvement ON mouvement.id=piece.mouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.liste=\'%3$s\' AND mouvement.om_collectivite=%2$s AND mouvement.date_tableau=\'%4$s\' AND  mouvement.etat=\'trs\' AND param_mouvement.cnen=\'Oui\' AND mouvement.envoi_cnen IS NULL',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"],
    $datetableau
);

/**
 * Cette requete permet de compter tous les mouvements qui n'ont pas ete
 * envoyes a l'insee en fonction de la collectivite en cours et de la liste en
 * cours et de la date de tableau
 * 
 * @param string $_SESSION["collectivite"]
 * @param string $_SESSION["liste"]
 * @param date $datetableau
 */
$query_count_mouvement = sprintf(
    'SELECT count(*) FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.liste=\'%3$s\' AND mouvement.om_collectivite=%2$s AND mouvement.date_tableau=\'%4$s\' AND mouvement.etat=\'trs\' AND param_mouvement.cnen=\'Oui\' AND mouvement.envoi_cnen IS NULL',
    DB_PREFIXE,
    intval($_SESSION["collectivite"]),
    $_SESSION["liste"],
    $datetableau
);

if (isset($row['id'])) {
    $sql1 = sprintf(
        'DELETE FROM %1$smouvement WHERE id=%2$s',
        DB_PREFIXE,
        $row['id']
    );
}
