<?php

$reqmo['libelle'] = 'Liste des candidatures des agents';
$reqmo['sql'] = "
    SELECT
         [S.libelle as scrutin], [A.nom||' '||A.prenom as nom], [B.libelle as bureau], [C.poste as poste], [C.periode as periode], [CASE C.decision WHEN 't' THEN 'Oui' ELSE 'Non' END as decision]
    FROM
        ".DB_PREFIXE."composition_scrutin AS S
        INNER JOIN ".DB_PREFIXE."candidature AS C ON C.composition_scrutin=S.id
        INNER JOIN ".DB_PREFIXE."agent AS A ON A.id=C.agent
        INNER JOIN ".DB_PREFIXE."bureau AS B ON B.id=C.bureau
    WHERE
        S.id = '[SCRUTIN]'
    ORDER BY
        [TRI]
";
$reqmo['TRI'] = array('bureau', 'poste', 'periode');
foreach(array('scrutin', 'nom', 'bureau', 'poste', 'periode', 'decision') as $key) {
    $reqmo[$key] = "checked";
}
$reqmo['SCRUTIN'] =
    "SELECT
        id,
        libelle
    FROM
        ".DB_PREFIXE."composition_scrutin
    WHERE 
        solde IS NOT True
    -- Résultat par défaut pour le cas où il n y a pas de scrutin
    UNION
        SELECT 0, 'Aucun'";
