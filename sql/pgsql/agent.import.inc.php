<?php
//$Id$ 
//gen openMairie le 15/02/2022 15:30

$import= "Insertion dans la table agent voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."agent";
$id='id'; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "id" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
    ),
    "nom" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "40",
    ),
    "prenom" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "40",
    ),
    "adresse" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "80",
    ),
    "cp" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "5",
    ),
    "ville" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "40",
    ),
    "telephone" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "14",
    ),
    "service" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "10",
        "fkey" => array(
            "foreign_table_name" => "service",
            "foreign_column_name" => "service",
            "sql_exist" => "select * from ".DB_PREFIXE."service where service = '",
        ),
    ),
    "telephone_pro" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "14",
    ),
    "grade" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "10",
        "fkey" => array(
            "foreign_table_name" => "grade",
            "foreign_column_name" => "grade",
            "sql_exist" => "select * from ".DB_PREFIXE."grade where grade = '",
        ),
    ),
);
