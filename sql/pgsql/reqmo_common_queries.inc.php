<?php
/**
 * Ce script permet de factoriser certaines configurations communes.
 *
 * @package openelec
 * @version SVN : $Id$
 */

$query_select_liste_officielle = sprintf(
    'SELECT liste.liste, (liste.liste_insee || \' - \' || liste.libelle_liste) AS lib FROM %1$sliste WHERE liste.officielle IS TRUE ORDER BY liste.liste',
    DB_PREFIXE
);
$reqmo["choix_liste"] = $query_select_liste_officielle;
