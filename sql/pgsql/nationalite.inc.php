<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/nationalite.inc.php";

// Objet d'une eventuelle edition .pdf.inc
$edition = "";

// Critere select de la requete
$champAffiche = array(
    "code as \"".__("Code")."\"",
    "libelle_nationalite as \"".__("Libelle")."\"",
);

// Champ sur lesquels la recherche est active
$champRecherche = array(
    "code as \"".__("Code")."\"",
    "libelle_nationalite as \"".__("Libelle")."\"",
);

// Critere where de la requete
$selection = "";

// Critere order by ou group by de la requete
$tri = " order by libelle_nationalite";
