<?php
/**
 *
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
//
$canton_id = NULL;
if (isset($params["canton"])) {
    $canton_id = $params["canton"];
}
//
$sql = sprintf(
    'SELECT canton.libelle FROM %1$scanton WHERE canton.id=\'%2$s\'',
    DB_PREFIXE,
    $canton
);
$canton_libelle = $f->db->getone($sql);
$f->addToLog("jury_liste_preparatoire.pdf.inc: db->getone(\"".$sql."\");", VERBOSE_MODE);
$f->isDatabaseError($canton_libelle);


/**
 *
 */
//
$DEBUG=0;
// ------------------------document---------------------------------------------
$orientation="L";// orientation P-> portrait L->paysage
$format="A4";// format A3 A4 A5
$police='arial';
$margeleft=10;// marge gauche
$margetop=5;// marge haut
$margeright=10;//  marge droite
$border=1; // 1 ->  bordure 0 -> pas de bordure
$C1="0";// couleur texte  R
$C2="0";// couleur texte  V
$C3="0";// couleur texte  B
//-------------------------LIGNE tableau----------------------------------------
$size=7; //taille POLICE
$height=5; // -> hauteur ligne tableau
$align='L';
$fond=1;// 0- > FOND transparent 1 -> fond
$C1fond1="255";// couleur fond  R
$C2fond1="255";// couleur fond  V
$C3fond1="255";// couleur fond  B
$C1fond2="241";// couleur fond  R
$C2fond2="241";// couleur fond  V
$C3fond2="241";// couleur fond  B
//-------------------------- titre----------------------------------------------
$libtitre =  ! empty($jury) && $jury === '2' ?
    "Liste préparatoire des suppléants du jury d'assises" :
    "Liste préparatoire du jury d'assises";
$libtitre .= ($canton_libelle != "" ? " - ".$canton_libelle : "");
$libtitre .= " - Année ".date("Y")."/".(date("Y")+1);
$flagsessionliste=0;// 1 - > affichage session liste ou 0 -> pas d'affichage
$bordertitre=0; // 1 ->  bordure 0 -> pas de bordure
$aligntitre='L'; // L,C,R
$heightitre=10;// hauteur ligne titre
$grastitre="B";//$gras="B" -> BOLD OU $gras=""
$fondtitre=0; //0- > FOND transparent 1 -> fond
$C1titrefond="181";// couleur fond  R
$C2titrefond="182";// couleur fond  V
$C3titrefond="188";// couleur fond  B
$C1titre="75";// couleur texte  R
$C2titre="79";// couleur texte  V
$C3titre="81";// couleur texte  B
$sizetitre=11;
//--------------------------libelle entete colonnes-----------------------------
$flag_entete=1;//entete colonne : 0 -> non affichage , 1 -> affichage
$fondentete=1;// 0- > FOND transparent 1 -> fond
$heightentete=5;//hauteur ligne entete colonne
$C1fondentete="180";// couleur fond  R
$C2fondentete="180";// couleur fond  V
$C3fondentete="180";// couleur fond  B
$C1entetetxt="0";// couleur texte R
$C2entetetxt="0";// couleur texte V
$C3entetetxt="0";// couleur texte B
$entete_style="";
$entete_size=8;
//------ Border entete colonne $be0  à $be.. ( $be OBLIGATOIRE )
$be0="TLB";
$be1="TLB";
$be2="TLB";
$be3="TLB";
$be4="TLB";
$be5="TLB";
$be6="TLBR";
// ------ couleur border--------------------------------------------------------
$C1border="159";// couleur texte  R
$C2border="160";// couleur texte  V
$C3border="167";// couleur texte  B
//------ Border cellule colonne $b0  à $b.. ( $b OBLIGATOIRE )
$b0="BL";
$b1="BL";
$b2="BL";
$b3="BL";
$b4="BL";
$b5="BL";
$b6="BLR";
//------ ALIGNEMENT entete colonne $ae0  à $ae.. ( $ae OBLIGATOIRE )
$ae0="C";
$ae1="C";
$ae2="C";
$ae3="C";
$ae4="C";
$ae5="C";
$ae6="C";
//------ largeur de chaque colonne $l0  à $l.. ( $l OBLIGATOIRE )---------------
$l0=12;
$l1=50;
$l2=50;
$l3=75;
$l4=30;
$l5=30;
$l6=30;
$widthtableau=277;// -> ajouter $l0 à $lxx
$bt=0;// border 1ere  et derniere ligne  dutableau par page->0 ou 1
//------ Affichage d'une ligne de compteur du nombre d'enregistrements----------
$counter_flag = true;
$counter_prefix = "";
$counter_suffix = "enregistrement(s)";
//------ ALIGNEMENT de chaque colonne $l0  à $a.. ( $a OBLIGATOIRE )------------
$a0="C";
$a1="L";
$a2="L";
$a3="L";
$a4="L";
$a5="L";
$a6="L";

/**
 *
 */
//--------------------------SQL-------------------------------------------------
$sql = sprintf(
    "SELECT
        bureau.code AS %2\$s,
        CASE
            WHEN (electeur.nom_usage <> '' AND electeur.nom_usage <> electeur.nom)
                THEN (electeur.civilite ||' '|| electeur.nom ||' - '|| electeur.nom_usage ||' '|| electeur.prenom)
            ELSE (electeur.civilite ||' '|| electeur.nom ||' '|| electeur.prenom)
        END AS \"%3\$s\",
        (to_char(date_naissance,'DD/MM/YYYY' )||' à '||libelle_lieu_de_naissance) AS %4\$s,
        CASE electeur.resident
            WHEN 'Non' THEN (
                CASE electeur.numero_habitation
                    WHEN 0 THEN ''
                    ELSE (electeur.numero_habitation||' ')
                END
                ||
                CASE electeur.complement_numero
                    WHEN '' THEN ''
                    ELSE (electeur.complement_numero||' ')
                END
                ||
                electeur.libelle_voie
                ||
                CASE electeur.complement
                    WHEN '' THEN ''
                    ELSE (' / '||electeur.complement)
                END
            )
            WHEN 'Oui' THEN (
                electeur.adresse_resident
                ||
                CASE electeur.complement_resident
                    WHEN '' THEN ''
                    ELSE (' / '||electeur.complement_resident)
                END
            )
            END AS \"%5\$s\",
        CASE resident
            WHEN 'Non'
                THEN (voie.cp||' '||voie.ville)
            WHEN 'Oui'
                THEN (cp_resident||' ' ||ville_resident)
            END AS \"%6\$s\",
        profession AS %7\$s,
        motif_dispense_jury AS %8\$s
    FROM
        %1\$selecteur
        LEFT JOIN %1\$svoie ON electeur.code_voie = voie.code
        LEFT JOIN %1\$sbureau ON electeur.bureau = bureau.id
        LEFT JOIN %1\$scanton ON bureau.canton = canton.id
    WHERE
        jury = %11\$d
        AND electeur.om_collectivite = %9\$d
        %10\$s
    ORDER BY
        withoutaccent(lower(nom)),
        withoutaccent(lower(prenom))",
    DB_PREFIXE,
    __("bureau"),
    __("Nom(s) Prenom(s)"),
    __("naissance"),
    __("Adresse / Complement"),
    __("CP Ville"),
    __("profession"),
    __("dispense"),
    intval($_SESSION["collectivite"]),
    isset($canton_id) && $canton_id != null ?
        sprintf("AND bureau.canton = '%s'", $canton_id) :
        '',
    ! empty($jury) ? intval($jury) : 1
);
