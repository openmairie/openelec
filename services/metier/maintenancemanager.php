<?php
/**
 * Ce fichier permet de déclarer la classe MaintenanceManager, qui effectue les
 * traitements pour la ressource 'maintenance'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

// Inclusion de la classe de base MetierManager
require_once("./metier/metiermanager.php");

/**
 * Cette classe hérite de la classe MetierManager. Elle permet d'effectuer des
 * traitements pour la ressource 'maintenance' qui ont vocation à être
 * déclenché de manière automatique ou par une application externe. Par
 * exemple, un cron permet chaque soir d'exécuter la synchronisation des
 * utilisateurs de l'application avec l'annuaire LDAP.
 */
class MaintenanceManager extends MetierManager {

    /**
     * @var mixed Ce tableau permet d'associer un module a une méthode,
     * le module (key) est celui reçu dans la requête REST, la méthode (value)
     * est la méthode de cette classe qui va exécuter le traitement
     */
    var $fptrs = array(
        'user' => 'synchronizeUsers',
        'synchro_last_notifications' => 'synchro_last_notifications',
        'treat_all_notifications' => 'treat_all_notifications',
        'synchro_referentiels_lieu_naissance' => 'synchro_referentiels_lieu_naissance',
        'synchro_referentiels_misc' => 'synchro_referentiels_misc',
        'synchro_bureaux' => 'synchro_bureaux',
        'synchro_etat_civil' => 'synchro_etat_civil',
        'synchro_l_e_reg' => 'synchro_l_e_reg',
        'reu_sync_check_status' => 'reu_sync_check_status',
        'reu_sync_mouvement_provenance_demande' => 'reu_sync_mouvement_provenance_demande',
        'reu_sync_scrutin' => 'reu_sync_scrutin',
        'notification_courriel' => 'notification_courriel',
        'migrate_logfiles_to_database' => 'migrate_logfiles_to_database',
    );

    /**
     * Cette méthode permet de gérer l'appel aux différentes méthodes
     * correspondantes au module appelé.
     *
     * @param string $module Le module à exécuter reçu dans la requête
     * @param mixed $data Les données reçues dans la requête
     * @return string Le résultat du traitement
     */
    public function performMaintenance($module, $data) {

        // Si le module n'existe pas dans la liste des modules disponibles
        // alors on ajoute un message d'informations et on retourne un
        // un résultat d'erreur
        if (!in_array($module, array_keys($this->fptrs))) {
            $this->setMessage("Le module demandé n'existe pas");
            return $this->BAD_DATA;
        }

        // Si le module existe dans la liste des modules disponibles
        // alors on appelle la méthode en question et on retourne son résultat
        return call_user_func(array($this, $this->fptrs[$module]), $data);

    }

    /**
     * Cette méthode permet d'effectuer la synchronisation des utilisateurs
     * de l'application avec l'annuaire LDAP paramétré dans l'application.
     *
     * @param mixed $data Les données reçues dans la requête
     * @return string Le résultat du traitement
     */
    public function synchronizeUsers($data) {
        // Si aucune configuration d'annuaire n'est présente, on retourne que
        // tout est OK et que la configuration 'annuaire n'est pas activée.
        if ($this->f->is_option_directory_enabled() !== true) {
            $this->setMessage("L'option 'annuaire' n'est pas configurée.");
            return $this->OK;
        }
        // Initialisation de la synchronisation des utilisateurs LDAP
        $results = $this->f->initSynchronization();
        // Si l'initialisation ne se déroule pas correctement alors on retourne
        // un résultat d'erreur
        if (is_null($results) || $results == false) {
            $this->setMessage("Erreur interne");
            return $this->KO;
        }
        // Application du traitement de synchronisation
        $ret = $this->f->synchronizeUsers($results);
        // Si l'a synchronisation ne se déroule pas correctement alors on
        // retourne un résultat d'erreur
        if ($ret == false) {
            $this->setMessage("Erreur interne");
            return $this->KO;
        }
        // Si l'a synchronisation ne se déroule correctement alors on
        // retourne un résultat OK
        $this->setMessage("Synchronisation terminée.");
        return $this->OK;
    }

    /**
     * [synchro_last_notifications description]
     * @return [type] [description]
     */
    public function synchro_last_notifications() {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        //
        $count_collectivites = $res->numrows();
        $count_errors = 0;
        $return_msg = "";
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            //
            $return_msg .= "\r".$row['libelle']." : ";
            //
            $reu_notification = $this->f->get_inst__om_dbform(array(
                "obj" => "reu_notification",
                "idx" => 0
            ));
            $sync = $reu_notification->sync_last_notifications();
            $return_msg .= $reu_notification->msg;
            //
            if ($sync !== true) {
                $count_errors++;
            }

            //
            $inst_reu->logout_connexion_logicielle();
            $inst_reu->__destruct();
            unset($inst_reu);
        }

        //
        $return_msg_header = __("Nombre de collectivités traitées"). " : ".'%s';
        $return_msg_header .= "\r".__("Nombre de collectivités en erreurs"). " : ".'%s';
        $return_msg_header = sprintf(
            $return_msg_header,
            $count_collectivites,
            $count_errors
        );
        $this->setMessage($return_msg_header."\r".$return_msg);

        //
        if ($count_errors !== 0) {
            return $this->KO;
        }
        return $this->OK;
    }

    /**
     * [treat_all_notifications description]
     * @return [type] [description]
     */
    public function treat_all_notifications() {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        //
        $count_collectivites = $res->numrows();
        $count_errors = 0;
        $return_msg = "";
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            //
            $return_msg .= "\r".$row['libelle']." : ";
            //
            $reu_notification = $this->f->get_inst__om_dbform(array(
                "obj" => "reu_notification",
                "idx" => 0
            ));
            $sync = $reu_notification->treat_all_notifications();
            $return_msg .= $reu_notification->msg;
            //
            if ($sync !== true) {
                $count_errors++;
            }

            //
            $inst_reu->logout_connexion_logicielle();
            $inst_reu->__destruct();
            unset($inst_reu);
        }

        //
        $return_msg_header = __("Nombre de collectivités traitées"). " : ".'%s';
        $return_msg_header .= "\r".__("Nombre de collectivités en erreurs"). " : ".'%s';
        $return_msg_header = sprintf(
            $return_msg_header,
            $count_collectivites,
            $count_errors
        );
        $this->setMessage($return_msg_header."\r".$return_msg);

        //
        if ($count_errors !== 0) {
            return $this->KO;
        }
        return $this->OK;
    }

    public function synchro_referentiels_lieu_naissance() {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            //
            require_once "../obj/traitement.reu_sync_referentiel_lieu_naissance.class.php";
            $trt = new reuSyncReferentielLieuNaissanceTraitement();
            $trt->execute();
            // Stop le traitement dès qu'une collectivité retourne un résultat
            if ($trt->error === false) {
                $this->setMessage(__("Synchronisation des lieux de naissance effectuée."));
                return $this->OK;
            }
        }
        $this->setMessage(__("Impossible de récupérer les lieux de naissance."));
        return $this->KO;
    }

    public function synchro_referentiels_misc() {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            require_once "../obj/traitement.reu_sync_referentiel_misc.class.php";
            $trt = new reuSyncReferentielMiscTraitement();
            $trt->execute();
            // Stop le traitement dès qu'une collectivité retourne un résultat
            if ($trt->error === false) {
                $this->setMessage(__("Synchronisation des référentiels effectuée."));
                return $this->OK;
            }
        }
        $this->setMessage(__("Impossible de récupérer la liste des référentiels."));
        return $this->KO;
    }

    public function synchro_bureaux() {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        //
        $count_collectivites = $res->numrows();
        $count_errors = 0;
        $return_msg = "";
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            //
            $return_msg .= "\r".$row['libelle']." : ";
            require_once "../obj/traitement.reu_sync_bureau.class.php";
            $trt = new reuSyncBureauTraitement();
            $trt->execute();
            $return_msg .= $trt->message;
            //
            if ($trt->error !== false) {
                $count_errors++;
            }

            //
            $inst_reu->logout_connexion_logicielle();
            $inst_reu->__destruct();
            unset($inst_reu);
        }

        //
        $return_msg_header = __("Nombre de collectivités traitées"). " : ".'%s';
        $return_msg_header .= "\r".__("Nombre de collectivités en erreurs"). " : ".'%s';
        $return_msg_header = sprintf(
            $return_msg_header,
            $count_collectivites,
            $count_errors
        );
        $this->setMessage($return_msg_header."\r".str_replace("<br/>", "\r", $return_msg));

        //
        if ($count_errors !== 0) {
            return $this->KO;
        }
        return $this->OK;
    }

    public function synchro_etat_civil() {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        //
        $count_collectivites = $res->numrows();
        $count_errors = 0;
        $return_msg = "";
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            //
            $return_msg .= "\r".$row['libelle']." : ";
            require_once "../obj/traitement.reu_sync_l_e_reu_etat_civil.class.php";
            $trt = new reuSyncLEREUEtatCivilTraitement();
            $trt->execute();
            $return_msg .= $trt->message;
            //
            if ($trt->error !== false) {
                $count_errors++;
            }

            //
            $inst_reu->logout_connexion_logicielle();
            $inst_reu->__destruct();
            unset($inst_reu);
        }

        //
        $return_msg_header = __("Nombre de collectivités traitées"). " : ".'%s';
        $return_msg_header .= "\r".__("Nombre de collectivités en erreurs"). " : ".'%s';
        $return_msg_header = sprintf(
            $return_msg_header,
            $count_collectivites,
            $count_errors
        );
        $this->setMessage($return_msg_header."\r".str_replace("<br/>", "\r", $return_msg));

        //
        if ($count_errors !== 0) {
            return $this->KO;
        }
        return $this->OK;
    }

    public function synchro_l_e_reg() {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        //
        $count_collectivites = $res->numrows();
        $count_errors = 0;
        $return_msg = "";
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            //
            $return_msg .= "\r".$row['libelle']." : ";
            require_once "../obj/traitement.reu_sync_l_e_reg.class.php";
            $trt = new reuSyncLERegTraitement();
            $trt->execute();
            $return_msg .= $trt->message;
            //
            if ($trt->error !== false) {
                $count_errors++;
            }

            //
            $inst_reu->logout_connexion_logicielle();
            $inst_reu->__destruct();
            unset($inst_reu);
        }

        //
        $return_msg_header = __("Nombre de collectivités traitées"). " : ".'%s';
        $return_msg_header .= "\r".__("Nombre de collectivités en erreurs"). " : ".'%s';
        $return_msg_header = sprintf(
            $return_msg_header,
            $count_collectivites,
            $count_errors
        );
        $this->setMessage($return_msg_header."\r".str_replace("<br/>", "\r", $return_msg));

        //
        if ($count_errors !== 0) {
            return $this->KO;
        }
        return $this->OK;
    }

    /**
     *
     */
    public function reu_sync_check_status($data) {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        //
        $count_collectivites = $res->numrows();
        $count_errors = 0;
        $return_msg = "";
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            //
            $return_msg .= "\r".$row['libelle']." : ";
            require_once "../obj/traitement.reu_sync_l_e_check.class.php";
            $trt = new reuSyncLECheckTraitement();
            ob_start();
            $trt->execute();
            ob_clean();
            $return_msg .= $trt->message;
            //
            if ($trt->error !== false) {
                $count_errors++;
            }

            //
            $inst_reu->logout_connexion_logicielle();
            $inst_reu->__destruct();
            unset($inst_reu);
        }

        //
        $return_msg_header = __("Nombre de collectivités traitées"). " : ".'%s';
        $return_msg_header .= "\r".__("Nombre de collectivités en erreurs"). " : ".'%s';
        $return_msg_header = sprintf(
            $return_msg_header,
            $count_collectivites,
            $count_errors
        );
        $this->setMessage($return_msg_header."\r".str_replace("<br/>", "\r", $return_msg));

        //
        if ($count_errors !== 0) {
            return $this->KO;
        }
        return $this->OK;
    }

    /**
     *
     */
    public function reu_sync_mouvement_provenance_demande($data) {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        //
        $count_collectivites = $res->numrows();
        $count_errors = 0;
        $return_msg = "";
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            //
            $return_msg .= "\r".$row['libelle']." : ";
            require_once "../obj/traitement.reu_sync_l_e_mouvement_provenance_demande.class.php";
            $trt = new reuSyncLEMouvementProvenanceDemandeTraitement();
            ob_start();
            $trt->execute();
            ob_clean();
            $return_msg .= $trt->message;
            //
            if ($trt->error !== false) {
                $count_errors++;
            }

            //
            $inst_reu->logout_connexion_logicielle();
            $inst_reu->__destruct();
            unset($inst_reu);
        }

        //
        $return_msg_header = __("Nombre de collectivités traitées"). " : ".'%s';
        $return_msg_header .= "\r".__("Nombre de collectivités en erreurs"). " : ".'%s';
        $return_msg_header = sprintf(
            $return_msg_header,
            $count_collectivites,
            $count_errors
        );
        $this->setMessage($return_msg_header."\r".str_replace("<br/>", "\r", $return_msg));

        //
        if ($count_errors !== 0) {
            return $this->KO;
        }
        return $this->OK;
    }

    function reu_sync_scrutin() {
        // Récupère la liste des collectivités
        $sql = sprintf(
            'SELECT om_collectivite, libelle
            FROM %1$som_collectivite
            WHERE niveau = \'%2$s\'
            ORDER BY om_collectivite',
            DB_PREFIXE,
            '1'
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des collectivités."));
            return $this->KO;
        }
        //
        $count_collectivites = $res->numrows();
        $count_errors = 0;
        $return_msg = "";
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            // Changement de contexte de la collectivité
            $_SESSION['collectivite'] = $row['om_collectivite'];
            $_SESSION['libelle_collectivite'] = $row['libelle'];
            $this->f->getCollectivite();

            //
            $inst_reu = $this->f->get_inst__reu(true);
            if ($inst_reu == null) {
                $this->setMessage(__("Le module REU n'est pas activé."));
                return $this->OK;
            }

            //
            $return_msg .= "\r".$row['libelle']." : ";
            //
            $reu_scrutin = $this->f->get_inst__om_dbform(array(
                "obj" => "reu_scrutin",
                "idx" => 0
            ));
            $sync = $reu_scrutin->sync_scrutin();
            $return_msg .= $reu_scrutin->msg;
            //
            if ($sync !== true) {
                $count_errors++;
            }

            //
            $inst_reu->logout_connexion_logicielle();
            $inst_reu->__destruct();
            unset($inst_reu);
        }

        //
        $return_msg_header = __("Nombre de collectivités traitées"). " : ".'%s';
        $return_msg_header .= "\r".__("Nombre de collectivités en erreurs"). " : ".'%s';
        $return_msg_header = sprintf(
            $return_msg_header,
            $count_collectivites,
            $count_errors
        );
        $this->setMessage($return_msg_header."\r".$return_msg);

        //
        if ($count_errors !== 0) {
            return $this->KO;
        }
        return $this->OK;
    }

    /**
     *
     */
    public function notification_courriel() {
        require_once "../obj/traitement.notification_courriel.class.php";
        $trt = new notificationCourrielTraitement();
        ob_start();
        $trt->execute();
        ob_clean();
        $this->setMessage(str_replace("<br/>", "\r", $trt->message));
        if ($trt->error !== false) {
            return $this->KO;
        }
        return $this->OK;
    }

    /**
     * 
     * 
     * @return 
     */
    function migrate_logfiles_to_database() {
        // Récupère la liste des fichiers de type trace
        $sql = sprintf(
            'SELECT *
            FROM %1$sfichier
            WHERE fichier.type = \'trace\'
            ORDER BY fichier.id',
            DB_PREFIXE
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) === true) {
            $this->setMessage(__("Impossible de récupérer la liste des fichiers à migrer."));
            return $this->KO;
        }
        //
        $count_fichier = $res->numrows();
        $count_errors = 0;
        $return_msg = "";
        $this->f->db->autoCommit (false);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $inst_fichier = $this->f->get_inst__om_dbform(array(
                "obj" => "fichier",
                "idx" => $row["id"],
            ));
            $ret = $inst_fichier->migrate();
            $return_msg .= $inst_fichier->msg;
            if ($ret !== true) {
                $count_errors++;
                $this->f->db->rollback() ;
            } else {
                $this->f->db->commit() ;
            }
        }
        //
        $return_msg_header = __("Nombre de fichiers traités"). " : ".'%s';
        $return_msg_header .= "\r".__("Nombre de fichiers en erreurs"). " : ".'%s';
        $return_msg_header = sprintf(
            $return_msg_header,
            $count_fichier,
            $count_errors
        );
        $this->setMessage($return_msg_header."\r".$return_msg);
        //
        if ($count_errors !== 0) {
            return $this->KO;
        }
        return $this->OK;
    }
}
