<?php
/**
 * Ce script définit la classe 'canton'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/canton.class.php";

/**
 * Définition de la classe 'canton' (om_dbform).
 */
class canton extends canton_gen {

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("id", __("Id"));
        $form->setLib("code", __("Code"));
        $form->setLib("libelle", __("Libelle"));
    }

    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange("code", "this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle", "this.value=this.value.toUpperCase()");
    }

    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // verification de la suppression
        if(isset($val['code']) && $val['code'] == 'T') {
            $this->msg .= sprintf(__("SUPPRESSION NON AUTORISÉE pour le canton '%s' (utilisé dans les traitements)"), $id);
            $this->correct=false;
        }
    }
}
