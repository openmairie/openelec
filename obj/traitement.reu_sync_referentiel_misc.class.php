<?php
/**
 * Ce script définit la classe 'reuSyncReferentielMiscTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'reuSyncReferentielMiscTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncReferentielMiscTraitement extends traitement {
    var $fichier = "reu_sync_referentiel_misc";
    /**
     *
     */
    function treatment () {
        //
        $this->LogToFile("start reu_sync_referentiel_misc");
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            $this->LogToFile($message);
            $this->LogToFile("end reu_sync_referentiel_misc");
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            $this->LogToFile($message);
            $this->LogToFile("end reu_sync_referentiel_misc");
            return;
        }
        //
        $nationalites = $inst_reu->get_nationalites();
        foreach ($nationalites["result"] as $key => $value) {
            $query = sprintf(
                'INSERT INTO %1$snationalite(code, libelle_nationalite) SELECT \'%2$s\', \'%3$s\' WHERE NOT EXISTS (SELECT code FROM %1$snationalite WHERE code=\'%2$s\');',
                DB_PREFIXE,
                $value["code"],
                $value["libelle"]
            );
            $res = $this->f->db->query($query);
            if ($this->f->isDatabaseError($res, true) !== false) {
                $this->error = true;
                return;
            }
        }
        //
        $typejustificatif = $inst_reu->get_referentiel_reference("typejustificatif");
        foreach ($typejustificatif["result"] as $key => $value) {
            $query = sprintf(
                'INSERT INTO %1$spiece_type(id, code, libelle) SELECT nextval(\'openelec.piece_type_seq\'), \'%2$s\', \'%3$s\' WHERE NOT EXISTS (SELECT code FROM %1$spiece_type WHERE code=\'%2$s\');',
                DB_PREFIXE,
                $value["code"],
                $this->f->db->escapeSimple($value["libelle"])
            );
            $res = $this->f->db->query($query);
            if ($this->f->isDatabaseError($res, true) !== false) {
                $this->LogToFile("database error");
                $this->error = true;
                return;
            }
        }
        //
        $this->LogToFile("end reu_sync_referentiel_misc");
    }
}
