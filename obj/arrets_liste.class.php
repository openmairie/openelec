<?php
//$Id$ 
//gen openMairie le 18/09/2023 12:01

require_once "../gen/obj/arrets_liste.class.php";

class arrets_liste extends arrets_liste_gen {

    /**
     *
     */
    function init_class_actions() {
        parent::init_class_actions();
        // Non affichage des actions de modification et de suppression
        $this->class_actions[1] = array();
        $this->class_actions[2] = array();
        // Demande de livrable d'arrêt des listes
        $this->class_actions[5] = array(
            "identifier" => "demander_j20_livrable",
            "portlet" => array(
               "type" => "action-direct-with-confirmation",
               "libelle" => __("Demander l'arrêt des listes"),
               "class" => "demander_j20_livrable-16",
               "order" => 51,
            ),
            "method" => "demander_j20_livrable",
            "permission_suffix" => "demander_j20_livrable",
            "condition" => array(
                "is_j20_period_valid",
                "has_no_demande",

            ),
            "button" => __("demander_j20_livrable"),
        );
    }

    /**
     * CONDITION - is_j20_period_valid
     *
     * @return boolean
     */
    function is_j20_period_valid() {
        $scrutin = $this->get_related_scrutin();
        return empty($scrutin) || (! empty($scrutin) && $scrutin->is_j20_period_valid());
    }

    function has_no_demande() {
        return empty($this->getVal('livrable_demande_id'));
    }

    function setType(&$form, $maj) {
        // Type des champs par défaut
        foreach ($this->champs as $champs) {
            $form->setType($champs, "hidden");
        }
        parent::setType($form, $maj);
        $form->setType("arrets_liste", "hidden");
        $form->setType("livrable", "hidden");
        if ($maj == 0) {
            $form->setType("livrable_demande_id", "hidden");
            $form->setType("livrable_demande_date", "hiddenstatic");
            $form->setType("livrable_date", "hidden");
        }
    }

    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        $form->setLib("livrable_demande_id", "Identifiant de la demande");
        $form->setLib("livrable_demande_date", "Date de demande");
        $form->setLib("livrable", "Livrable");
        $form->setLib("livrable_date", "Date de réception");
    }

    /**
     * Configuration du formulaire (VIEW formulaire et VIEW sousformulaire).
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * @param integer $validation Marqueur de validation du formulaire.
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        if ($maj === '0') {
            $form->setVal('livrable_demande_date', date("Y-m-d"));
        }
    }


    /**
     * Effectue différent traitement suite à l'ajout d'un nouvel arrets_listes.
     *
     * Les traitements effectués sont :
     *  - Réalise un arrêtes des listes hors scrutin
     *
     * @param string id : identifiant de l'objet courant
     * @param string dnu1 : ???
     * @param array val : tableau contenant les valeurs de l'objet courant
     * @param string dnu2 : ???
     *
     * @return boolean
     */
    function triggerajouter($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        parent::triggerajouterapres($id, $dnu1, $val, $dnu2);
        // Demande de livrable d'arrêt des listes si l'ajout n'est pas lié au
        // traitement d'une notification d'arrêt des listes sans scrutin dont
        // la demande ne proviens pas d'openElec.
        if (empty($val['non_openelec']) || $val['non_openelec'] !== true) {
            if ($this->demander_livrable_arret_liste_hors_election() === false) {
                $this->correct = false;
            }
        } else {
            unset($val['non_openelec']);
        }
        return $this->end_treatment(__METHOD__, $this->correct);
    }

    /**
     * Réalise un arrêts des listes hors élection.
     *
     * Récupère l'instance du REU puis envoie la demande de livrable d'arrêts des listes
     * hors élection.
     * Si la demande échoue affiche un message explicatif et renvoie false.
     * Sinon, set dans la propriété valF l'id de la demande de livrable.
     *
     * Remarque : cette méthode étant appellé dans la méthode this::triggerajouter()
     * en settant $this->valF['livrable_demande_id'] la valeur sera enregistré lors de
     * l'ajout de l'arret des listes en base de données
     *
     * @return boolean
     *
     */
    function demander_livrable_arret_liste_hors_election() {
        $inst_reu = $this->f->get_inst__reu();
        $ret = $inst_reu->handle_livrable(array(
            "mode" => "add",
            "type_livrable" => "COM3112",
            "datas" => array(
                "code" => "PDF"
            ),
        ));
        if (isset($ret["code"]) !== true || $ret["code"] != 201) {
            $this->correct = false;
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }

        $this->valF["livrable_demande_id"] = trim($ret["headers"]["X-App-params"]);
        $this->addToMessage("La demande de livrable COM3112 a été transmise au REU.");
        return $this->end_treatment(__METHOD__, true);
    }

    function demander_j20_livrable() {
        $scrutin = $this->get_related_scrutin();
        if (empty($scrutin)) {
            $this->addToMessage("Le scrutin n'a pas pu être récupéré. Veuillez contactez votre administrateur.");
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Récupération et affichage des messages issus du traitement de la demande
        // de livrable
        $is_correct = $scrutin->demander_j20_livrable();
        $this->addToMessage($scrutin->msg);
        return $is_correct;
    }

    function bloc_livrable_arret_liste_hors_election_content() {
        $com3112_content = '';
        if ($this->getVal("livrable") != "") {
            $com3112_content = sprintf(
                '<img src="../app/img/arrive.png" /> L\'arrêt des listes a été réalisé le %s. Fichiers produits : <a href="%s">Télécharger</a>',
                $this->getVal("livrable_date"),
                sprintf(
                    '%s&snippet=file&uid=%s',
                    OM_ROUTE_FORM,
                    $this->getVal("livrable")
                )
            );
        } elseif ($this->getVal("livrable_demande_id") != "") {
            $com3112_content = sprintf(
                '<img src="../app/img/nonarrive.png" /> L\'arrêt des listes a été demandé (%s) le %s. En attente du retour de l\'INSEE.',
                $this->getVal("livrable_demande_id"),
                $this->getVal("livrable_demande_date")
            );
        }
        return $com3112_content;
    }

    function bloc_generation_listes_electorales() {
        $bloc__generation_listes_electorales = "";
        if ($this->getVal("livrable") != "") {
            ob_start();
            printf(
                '<div id="edition_arret_listes" class="card">
                <div class="card-header">
                    Listes électorales
                </div>
                <div class="card-body">
                '
            );
            $this->view__generation_listes_electorales();
            printf(
                '</div>
                </div>
                '
            );
            $bloc__generation_listes_electorales = ob_get_clean();
        }
        return $bloc__generation_listes_electorales;
    }

    /**
     *
     */
    function view__generation_listes_electorales() {
        echo "<table class=\"table\">\n";
        // Gestion de l'url et du nom de fichier en fonction du contexte. Si il s'agit d'un arrêt des
        // listes lié à un scrutin on utilise les information du scrutin pour générer l'édition.
        // Si c'est un arrêt des listes hors scrutin ce sont les informations de la demande qui
        // sont utilisées
        $template_url = "../app/index.php?module=form&obj=electeur&action=302&mode_edition=commune&stockage=true&origin=%s&%s";
        $url = sprintf(
            $template_url,
            'demande',
            sprintf(
                'demande_id=%s&date_demande=%s&contexte=arrets_liste&obj_id=%s',
                $this->getVal('livrable_demande_id'),
                $this->getVal('livrable_demande_date'),
                $this->getVal($this->clePrimaire)
            )
        );
        $filename_template = "listeelectorale-%s-%s-%s%s.pdf";
        $filename = sprintf(
            $filename_template,
            $_SESSION["collectivite"],
            "liste%s",
            "demande",
            $this->getVal('livrable_demande_id')
        );
        $scrutin = $this->get_related_scrutin();
        if (! empty($scrutin)) {
            $url = sprintf(
                $template_url,
                'scrutin',
                sprintf('scrutin=%s', $scrutin->getVal($scrutin->clePrimaire))
            );
            $filename = sprintf(
                $filename_template,
                $_SESSION["collectivite"],
                "liste%s",
                "scrutin",
                $scrutin->getVal($scrutin->clePrimaire)
            );
        }
        foreach ($this->f->get_all__listes(true) as $key => $value) {
            //
            $edition = array (
                'pdf' => $url."&liste=".$value["liste"],
                'generer' => "OUI",
                'filename' => sprintf($filename, $value["liste"]),
                'id' => "edition-pdf-listeelectorale",
                "right" => "electeur_edition_pdf__liste_electorale",
            );
            //
            
            echo "\t<tr class=\"editions\">";
            echo "<td class=\"title\">".$value["libelle_liste"]."</td>";
            echo "<td class=\"icone\">";
            //
            $sql = sprintf(
                'SELECT * FROM %1$sfichier WHERE fichier.om_collectivite=%2$s AND fichier.filename=\'%3$s\' ORDER BY creationdate DESC, creationtime DESC LIMIT 1',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $edition['filename']
            );
            $res = $this->f->db->query($sql);
            $this->f->addToLog(
                __METHOD__."(): db->query(\"".$sql."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            //
            $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
            //
            if ($row != null) {
                echo "<a class=\"lien\" id=\"".$edition["id"]."\" href=\"".OM_ROUTE_FORM."&snippet=file&uid=".$row["uid"]."\" target=\"_blank\">";
                echo "<span class=\"om-icon om-icon-25 om-icon-fix pdf-25\"><!-- --></span>";
                echo "<br />";
                echo __("Télécharger");
                echo "</a>";
                echo "</td><td>";
                echo " <a class=\"lien\" href=\"".$edition ['pdf']."\" id=\"".$edition["id"]."-generer\">".__("Generer")."</a><br />";
                echo " Taille : [".number_format($row["size"], 0, ',', ' ')." Octets]<br />";
                echo " Date : [". $this->f->formatDate($row["creationdate"])." à ".$row["creationtime"]."]";
            } else {
                echo "&nbsp;</td><td>";
                echo " <a class=\"lien\" href=\"".$edition ['pdf']."\" id=\"".$edition["id"]."-generer\">".__("Generer")."</a><br />";
                echo __("Aucun fichier n'a ete genere");
            }
            echo "</td>";
            echo "</tr>\n";
        }
        echo "</table>";
    }

    function display_infos_livrable_scrutin() {
        $scrutin = $this->get_related_scrutin();
        $arret_liste = empty($scrutin) ?
            $this->bloc_livrable_arret_liste_hors_election_content():
            $scrutin->bloc_livrable_j20_content();
        printf(
            '<div class="container-fluid" id="module-election-scrutin">
                <h2>Livrables \'Répertoire Électorale Unique\'</h2>
                <div class="row">
                    <div id="arret_listes" class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                Arrêt des listes
                            </div>
                            <div class="card-body">
                                %1$s
                            </div>
                        </div>
                    </div>
                <div class="visualClear"></div>
                <h2>Éditions</h2>
                <div class="row">
                    <div class="col-sm-6">
                        %2$s
                    </div>
                </div>
                <div class="visualClear"></div>
            </div>
            <div class="visualClear"></div>',
            $arret_liste,
            $this->bloc_generation_listes_electorales()
        );
    }

    /**
     *
     */
    function afterFormSpecificContent() {
        if ($this->getParameter("maj") == 3) {
            $this->display_infos_livrable_scrutin();
        }
    }

    /**
     * Récupère le scrutin lié à l'arrêt des listes.
     *
     * @return reu_scrutin|null renvoie le scrutin lié ou null si aucun
     * scrutin n'est associé à l'arrêt des listes.
     */
    function get_related_scrutin() {
        if (! empty($this->reu_scrutin)) {
            return $this->reu_scrutin;
        }

        $reu_scrutin_id = $this->f->get_one_result_from_db_query(sprintf(
            'SELECT reu_scrutin.id   
            FROM %1$sreu_scrutin
            WHERE reu_scrutin.arrets_liste = \'%2$d\'',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        ));
        if (empty($reu_scrutin_id['result'])) {
            return null;
        }
        $this->reu_scrutin = $this->f->get_inst__om_dbform(array(
            "obj" => "reu_scrutin",
            "idx" => $reu_scrutin_id['result']));
        return $this->reu_scrutin;
    }
}
