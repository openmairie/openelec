<?php
/**
 * Ce script définit la classe 'modification'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/mouvement_electeur.class.php";

/**
 * Définition de la classe 'modification' (om_dbform).
 *
 * Surcharge de la classe 'mouvement_electeur'.
 */
class modification extends mouvement_electeur {

    /**
     *
     */
    protected $_absolute_class_name = "modification";

    /**
     * Type de mouvement correspondant à la valeur de typecat dans la table
     * param_mouvement
     * @var string Type de mouvement
     */
    var $typeCat = "modification";

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        mouvement_gen::init_class_actions();
        //
        $this->class_actions[0]["condition"] = array(
            "is_collectivity_mono",
            "is_idxelecteur_exists",
        );
        //
        $this->class_actions[1]["condition"] = array(
            "is_collectivity_mono",
            "is_not_treated",
            "is_not_in_status__abandonne",
        );
        //
        $this->class_actions[2]["condition"] = array(
            "is_collectivity_mono",
            "is_not_treated",
            "is_not_in_status__ouvert",
            "is_not_in_status__abandonne",
        );
        //
        $this->class_actions[4] = array(
            "identifier" => "consulter-from-electeur",
            "view" => "view_consulter_from_idxelecteur",
            "permission_suffix" => "consulter",
        );
        $this->class_actions[61] = array(
            "identifier" => "valider",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("valider"),
                "class" => "valider-16",
            ),
            "permission_suffix" => "valider",
            "method" => "valider",
            "condition" => array(
                "is_collectivity_mono",
                "is_in_status__ouvert__or__vise_insee",
            ),
        );
        //
        $this->class_actions[101] = array(
            "identifier" => "add-search-form",
            "view" => "view_add_search_form",
            "permission_suffix" => "ajouter",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[102] = array(
            "identifier" => "add-search-form-results",
            "view" => "view_add_search_form_results",
            "permission_suffix" => "ajouter",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-attestationmodification",
            "view" => "view_edition_pdf__attestationmodification",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Attestation de modification"),
               "class" => "pdf-16",
               "order" => 52,
            ),
            "permission_suffix" => "edition_pdf__attestationmodification",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[202] = array(
            "identifier" => "edition-pdf-refusmodification",
            "view" => "view_edition_pdf__refusmodification",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Refus de modification"),
               "class" => "courrierrefus-16",
               "order" => 53,
            ),
            "permission_suffix" => "edition_pdf__refusmodification",
            "condition" => array(
                "is_collectivity_mono",
                "is_option_refus_mouvement_enabled",
            ),
        );
        //
        $this->class_actions[204] = array(
            "identifier" => "edition-pdf-recepissemodification",
            "view" => "view_edition_pdf__recepissemodification",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Récépissé"),
               "class" => "pdf-16",
               "order" => 51,
            ),
            "permission_suffix" => "edition_pdf__recepissemodification",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[251] = array(
            "identifier" => "decoupage",
            "view" => "view_decoupage_json",
            "permission_suffix" => "consulter",
        );
        //
        $this->class_actions[401] = array(
            "identifier" => "consulter-historique",
            "view" => "view_consulter_historique",
            "permission_suffix" => "consulter_historique",
            "condition" => array(
                "exists",
            ),
        );
        //
        $this->class_actions[501] = array(
            "identifier" => "abandonner",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("abandonner"),
                "class" => "abandonner-16",
            ),
            "permission_suffix" => "abandonner",
            "method" => "abandonner",
            "condition" => array(
                "is_collectivity_mono",
                "is_in_status__ouvert",
                "is_not_treated",
            ),
        );
        $this->class_actions[91] = array(
            "identifier" => "traitements_par_lot",
            "view" => "view_traitements_par_lot",
            "permission_suffix" => "traiter_par_lot",
        );
    }

    /**
     * VIEW - view_edition_pdf__recepissemodification.
     *
     * @return void
     */
    public function view_edition_pdf__recepissemodification() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "recepissemodification"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'recepissemodification-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__attestationmodification.
     *
     * @return void
     */
    public function view_edition_pdf__attestationmodification() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "attestationmodification"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'attestationmodification-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__refusmodification.
     *
     * @return void
     */
    public function view_edition_pdf__refusmodification() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "refusmodification"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'refusmodification-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * Configuration du formulaire (VIEW formulaire et VIEW sousformulaire).
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        //
        if ($maj == 0 && $validation == 0) {
            $form->setVal("date_demande", date("Y-m-d"));
        }
    }

    /**
     * Cette methode permet de remplir le tableau val attribut de
     * l'objet contenu dans $valE.
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        parent::setVal($form, $maj, $validation);
        if ($validation == 0
            && $maj == 0
            && $this->f->getParameter("reu_sync_l_e_valid") == "done"
            && is_array($this->electeur) === true
            && array_key_exists("ine", $this->electeur) === true) {
            // Modifie tous les électeurs ayant le même INE
            // Le cas doit se présenter seulement dans le cas des listes LCE et LCM
            $elec_w_same_ine = $this->get_electeurs_with_same_ine($this->electeur['ine']);
            if ($elec_w_same_ine !== false
                && count($elec_w_same_ine) > 1) {
                // LC2
                $form->setVal('liste', '04');
            }
        }
    }

    /**
     *
     */
    function setValFAjout($val = array()) {
        //
        $this->valF['electeur_id'] = $val['electeur_id'];
        $this->valF['numero_bureau'] = ($val['numero_bureau'] !== '' ? $val['numero_bureau'] : null);
        if (array_key_exists("provenance_demande", $val) === true
            && $val["provenance_demande"] !== "") {
            //
            $this->valF["provenance_demande"] = $val["provenance_demande"];
        } else {
            $this->valF["provenance_demande"] = "MAIRIE";
        }
    }

    /**
     * TREATMENT - apply.
     *
     * Ce traitement permet d'appliquer le mouvement sur la liste électorale.
     *
     * @return boolean
     */
    function apply($val = array()) {
        $this->begin_treatment(__METHOD__);
        // Il y a deux modes :
        // - soit nous sommes sur une instance d'une modification existante
        //   et on applique donc le traitement sur celle là
        // - soit on reçoit une liste d'identifiants de modifications à traiter
        //   et on applique donc le traitement sur celles là
        $modifications_ids = array();
        if ($this->exists() === true) {
            $modifications_ids = array($this->getVal($this->clePrimaire), );
        } elseif (array_key_exists("modifications_ids", $val) === true
            && is_array($val["modifications_ids"]) === true) {
            $modifications_ids = $val["modifications_ids"];
        }
        $nb_modifications_to_apply = count($modifications_ids);
        if ($nb_modifications_to_apply == 0) {
            $this->correct = false;
            $this->addToLog("Mauvais paramètres. Aucune modification à appliquer.", DEBUG_MODE);
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // On récupère toutes les informations dont on a besoin sur le ou les
        // mouvements pour pouvoir appliquer le traitement
        $query_select_modification = sprintf(
            'SELECT
                *
            FROM
                %1$smouvement
                INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
            WHERE
                lower(param_mouvement.typecat)=\'modification\'
                AND mouvement.etat=\'actif\'
                AND mouvement.id IN (%2$s)',
            DB_PREFIXE,
            implode(',', $modifications_ids)
        );
        $res_select_modification = $this->f->db->query($query_select_modification);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query_select_modification."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_select_modification, true)) {
            $this->correct = false;
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        if ($nb_modifications_to_apply != $res_select_modification->numrows()) {
            $this->correct = false;
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        //
        while ($row =& $res_select_modification->fetchRow(DB_FETCHMODE_ASSOC)) {
            // Gestion des LC2
            if ($row["liste"] == "04") {
                $message = "-> Cas particulier LC2";
                $this->addToMessage($message);
                $listes = array("02", "03", );
            } else {
                $listes = array($row["liste"], );
            }
            foreach ($listes as $liste) {
                $electeur_id = $this->get_electeur_id_by_ine_and_list(
                    $row['ine'],
                    $liste
                );
                // maj ELECTEUR
                $enr = $this->f->get_inst__om_dbform(array(
                    "obj" => "electeur",
                    "idx" => $electeur_id,
                ));
                $enr->valF['tableau'] = 'annuel';
                $row["liste"] = $liste;
                $row["numero_bureau"] = null;
                $ret = $enr->modifierTraitement($row, $this->f->getParameter("datetableau"));
                if ($ret !== true) {
                    $this->correct = false;
                    $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
                    return $this->end_treatment(__METHOD__, false);
                }
            }
            // maj MOUVEMENT
            $fields_values = array(
                'etat'    => 'trs',
                'tableau' => 'annuel',
                'date_j5' => ''.$enr->dateSystemeDB().'',
            );
            $cle = "id=".$row['id'];
            //
            $res1 = $this->f->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, "mouvement"),
                $fields_values,
                DB_AUTOQUERY_UPDATE,
                $cle
            );
            $this->f->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "mouvement")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res1, true) !== false) {
                $this->correct = false;
                $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
                return $this->end_treatment(__METHOD__, false);
            }
        }
        $res_select_modification->free();
        if ($this->exists() === true) {
            // Gestion de l'historique
            $val_histo = array(
                'action' => 'apply',
                'message' => __("Mouvement appliqué sur la liste électorale.")
            );
            $histo = $this->handle_historique($val_histo);
            if ($histo !== true) {
                $this->correct = false;
                $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $this->addToMessage(_("La modification a été appliquée correctement sur l'électeur."));
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - On active le verrou sur l'électeur
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Gestion de l'archive de l'électeur
        $val_archive_e = $val;
        $val_archive_e['id'] = $id;
        $handle_archive_electeur = $this->handle_archive_electeur($val_archive_e);
        if ($handle_archive_electeur !== true) {
            return false;
        }
        //
        if (array_key_exists("origin", $val) === true
            && $val["origin"] === "modec") {
            return true;
        }
        if ($this->manageVerrouElecteur($id, $val, true) !== true) {
            return false;
        }
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done") {
            $valF = array(
                "statut" => "ouvert",
            );
            $ret = $this->update_autoexecute($valF, $id, false);
            if ($ret !== true) {
                return $this->end_treatment(__METHOD__, false);
            }
            // Gestion de l'historique du mouvement
            $val_histo = array(
                'id' => $id,
                'action' => __METHOD__,
                'message' => __('Statut ouvert.'),
                'datas' => $valF,
            );
            $histo = $this->handle_historique($val_histo);
            if ($histo !== true) {
                $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
                return $this->end_treatment(__METHOD__, false);
            }
        }
        return true;
    }

    /**
     * TRIGGER - triggersupprimerapres.
     *
     * - On désactive le verrou sur l'électeur
     *
     * @return boolean
     */
    function triggersupprimerapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($this->manageVerrouElecteur($id, $val, false) !== true) {
            return false;
        }
        return true;
    }

    /**
     *
     */
    function setValFNumeroBureau() {
        //
        if ($this->valF['bureau'] != $this->electeur['bureau']) {
            //
            $this->valF['numero_bureau'] = null;
            $this->addToMessage(__("Le mouvement de l'electeur entraine un changement de bureau de vote.")."<br/>");
        } else {
            //
            $this->valF['numero_bureau'] = ($this->electeur['numero_bureau'] !== '' ? $this->electeur['numero_bureau'] : null);
        }
    }

    /**
     *
     */
    function handle_archive_electeur($val = array()) {
        //
        $inst_electeur = $this->f->get_inst__om_dbform(array(
            "obj" => "electeur",
            "idx" => $val['electeur_id'],
        ));
        //
        $etat_civil = array();
        $etat_civil['id'] = $inst_electeur->getVal('id');
        $etat_civil['ine'] = $inst_electeur->getVal('ine');
        $etat_civil['nom'] = $inst_electeur->getVal('nom');
        $etat_civil['nom_usage'] = $inst_electeur->getVal('nom_usage');
        $etat_civil['prenom'] = $inst_electeur->getVal('prenom');
        $etat_civil['bureau'] = $inst_electeur->getVal('bureau');
        $inst_bureau = $inst_electeur->get_inst__bureau();
        $etat_civil['bureau_de_vote_code'] = $inst_bureau->getVal('code');
        $etat_civil['bureau_de_vote_libelle'] = $inst_bureau->getVal('libelle');
        $etat_civil['bureauforce'] = $inst_electeur->getVal('bureauforce');
        $etat_civil['civilite'] = $inst_electeur->getVal('civilite');
        $etat_civil['sexe'] = $inst_electeur->getVal('sexe');
        $etat_civil['date_naissance'] = $inst_electeur->getVal('date_naissance');
        $etat_civil['code_departement_naissance'] = $inst_electeur->getVal('code_departement_naissance');
        $etat_civil['libelle_departement_naissance'] = $inst_electeur->getVal('libelle_departement_naissance');
        $etat_civil['code_lieu_de_naissance'] = $inst_electeur->getVal('code_lieu_de_naissance');
        $etat_civil['libelle_lieu_de_naissance'] = $inst_electeur->getVal('libelle_lieu_de_naissance');
        $etat_civil['code_nationalite'] = $inst_electeur->getVal('code_nationalite');
        $inst_nationalite = $inst_electeur->get_inst__nationalite();
        $etat_civil['libelle_nationalite'] = $inst_nationalite->getVal('libelle_nationalite');
        $etat_civil['code_voie'] = $inst_electeur->getVal('code_voie');
        $etat_civil['libelle_voie'] = $inst_electeur->getVal('libelle_voie');
        $inst_voie = $inst_electeur->get_inst__voie();
        $etat_civil['cp_voie'] = $inst_voie->getVal("cp");
        $etat_civil['ville_voie'] = $inst_voie->getVal("ville");
        $etat_civil['numero_habitation'] = $inst_electeur->getVal('numero_habitation');
        $etat_civil['complement_numero'] = $inst_electeur->getVal('complement_numero');
        $etat_civil['complement'] = $inst_electeur->getVal('complement');
        $etat_civil['resident'] = $inst_electeur->getVal('resident');
        $etat_civil['adresse_resident'] = $inst_electeur->getVal('adresse_resident');
        $etat_civil['complement_resident'] = $inst_electeur->getVal('complement_resident');
        $etat_civil['cp_resident'] = $inst_electeur->getVal('cp_resident');
        $etat_civil['ville_resident'] = $inst_electeur->getVal('ville_resident');
        $etat_civil['courriel'] = $inst_electeur->getVal('courriel');
        $etat_civil['telephone'] = $inst_electeur->getVal('telephone');
        //
        $valF = array(
            "archive_electeur" => json_encode($etat_civil),
        );
        $ret = $this->update_autoexecute($valF, $val['id'], false);
        if ($ret !== true) {
            return false;
        }
        return true;
    }
}
