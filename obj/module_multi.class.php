<?php
/**
 * Ce script définit la classe 'module_multi'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_multi' (module).
 */
class module_multi extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_multi",
            "title" => __("Traitement")." -> ".__("Module Multi Collectivités"),
            "elems" => array(
                array(
                    "right" => "module_multi-onglet_traitement_par_lot",
                    "href" => "../app/index.php?module=module_multi&view=traitement_par_lot",
                    "title" => __("Traitements par lot"),
                    "view" => "traitement_par_lot",
                ),
                array(
                    "right" => "module_multi-onglet_facturation",
                    "id" => "module-multi-tab-facturation",
                    "href" => "../app/index.php?module=module_multi&view=facturation",
                    "title" => __("Facturation"),
                    "view" => "facturation",
                ),
                array(
                    "right" => "module_multi-onglet_statistiques",
                    "id" => "module-multi-tab-statistiques",
                    "href" => "../app/index.php?module=module_multi&view=statistiques",
                    "title" => __("Statistiques"),
                    "view" => "statistiques",
                ),
                array(
                    "type" => "edition_pdf",
                    "right" => "module_multi-edition_pdf__facturation_commune_titre_de_recette",
                    "view" => "edition_pdf__facturation_commune_titre_de_recette",
                ),
                array(
                    "type" => "edition_pdf",
                    "right" => "module_multi-edition_pdf__statistiques_mouvements_par_commune",
                    "view" => "edition_pdf__statistiques_mouvements_par_commune",
                ),
                array(
                    "type" => "edition_pdf",
                    "right" => "module_multi-edition_pdf__statistiques_saisie_par_utilisateur",
                    "view" => "edition_pdf__statistiques_saisie_par_utilisateur",
                ),
                array(
                    "type" => "edition_pdf",
                    "right" => "module_multi-edition_pdf__statistiques_inscriptions",
                    "view" => "edition_pdf__statistiques_inscriptions",
                ),
                array(
                    "type" => "export",
                    "right" => "module_multi-export__facturation_publipostage",
                    "view" => "export__facturation_publipostage",
                ),
                array(
                    "type" => "export",
                    "right" => "module_multi-export__facturation_transfert_budget",
                    "view" => "export__facturation_transfert_budget",
                ),
            ),
        );
    }

    function view__traitement_par_lot() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_multi");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");
        $f=$this->f;
        printf('<div id="sousform-multi_traitement_par_lot">');

        /**
         * GESTION DES TABS
         */
        //
        $tab = 0;
        if (isset($_GET['tab'])) {
            $tab = $_GET['tab'];
        } elseif (isset($_POST['return']) && $_POST['return'] == 1) {
            $tab = 0;
        } elseif (isset($_POST['return']) && $_POST['return'] == 2) {
            $tab = 1;
        } elseif (isset($_POST['return']) && $_POST['return'] == 3) {
            $tab = 2;
        } elseif (isset($_POST['trtmulti_form_0_action_valid'])
            && isset($_POST['posted_from_tab']) && $_POST['posted_from_tab'] == "0") {
            if (!isset ($_POST ['collectivites'])) {
                $tab = 0;
            } else {
                $tab = 1;
            }
        } elseif (isset ($_POST['trtmulti_form_1_action_valid'])
            && isset($_POST['posted_from_tab']) && $_POST['posted_from_tab'] == "1") {
            $tab = 2;
        } else if (isset ($_POST['trtmulti_form_2_action_valid'])
            && isset($_POST['posted_from_tab']) && $_POST['posted_from_tab'] == "2") {
            $tab = 3;
        }
        /**
         * JAVASCRIPT
         */
        printf(
            '<script type="text/javascript">
        $(document).ready(function(){
            var $multitabs = $("#multi-traitement-par-lot").tabs();
            $("#multi-traitement-par-lot").tabs("select", %s);
            $("#multi-traitement-par-lot").tabs("option", "disabled", [0, 1, 2, 3]);
            $("a#unselectall").click(function(){
            // make the element selectable again
            $("#collectivites").removeAttr("disabled");
            // deselect all options
            $("#collectivites").each(function(){
                    $("#collectivites option").removeAttr("selected");
            });
            });
            $("a#selectall").click(function(){
                $("#collectivites").each(function(){
                    $("#collectivites option").attr("selected", "selected")
                });
            });
        });
        </script>',
            $tab
        );

        /**
         * ONGLETS
         */
        // start #tabs
        echo "<div id=\"multi-traitement-par-lot\">\n";

        // onglets
        echo "\n<ul>\n";
        echo "\t<li><a href=\"#tabs-0\">".__("Etape 1")."</a></li>\n";
        echo "\t<li><a href=\"#tabs-1\">".__("Etape 2")."</a></li>\n";
        echo "\t<li><a href=\"#tabs-2\">".__("Etape 3")."</a></li>\n";
        echo "\t<li><a href=\"#tabs-3\">".__("Etape 4")."</a></li>\n";
        echo "</ul>\n";

        //
        $liste = (isset($_POST["liste"]) ? $_POST["liste"] : "");
        $date = (isset($_POST["date"]) ? $_POST["date"] : "");
        $sans_changement_de_bureau = (isset($_POST["sans_changement_de_bureau"]) && $_POST["sans_changement_de_bureau"] != '' ? true : false);
        $order_by_datenaissance = (isset($_POST["order_by_datenaissance"]) && $_POST["order_by_datenaissance"] != '' ? true : false);

        /**
         * TRAITEMENTS
         */
        $traitements = array(
            // TRT
            "annuel" => array(
                "type" => "trt",
                "name" => "Traitement annuel",
                "selected" => false,
                "group" => "trt",
            ),
            "j5" => array(
                "type" => "trt",
                "name" => "Traitement J-5",
                "params" => array(
                    "mouvementatraiter" => (isset($_POST["mouvementatraiter"]) ? $_POST["mouvementatraiter"] : ""),
                ),
                "confirmation" => "mouvementatraiter",
                "selected" => false,
                "group" => "trt",
            ),
            "archivage" => array(
                "type" => "trt",
                "name" => "Archivage",
                "selected" => false,
                "group" => "trt",
            ),
            "refonte" => array(
                "type" => "trt",
                "name" => "Refonte",
                "selected" => false,
                "group" => "trt",
            ),
            "election_mention" => array(
                "type" => "trt",
                "class" => "electionMentionTraitement",
                "name" => "Election Mentions",
                "confirmation" => "election_mention",
                "selected" => false,
                "group" => "trt",
            ),
            // PDF
            "listeelectorale" => array(
                "type" => "pdf",
                "name" => "Liste generale",
                "method" => "handle_pdf_output__liste_electorale",
                "get" => array(
                    "mode_edition" => "commune",
                ),
                "confirmation" => "date_election",
                "livrable" => "j20",
                "selected" => false,
                "group" => "pdf",
            ),
            "etiquetteelecteur" => array(
                "type" => "pdf",
                "get" => array(
                    "obj" => "electeur",
                    "mode_edition" => "commune",
                ),
                "name" => "Etiquettes",
                "method" => "handle_pdf_output__etiquette",
                "selected" => false,
                "group" => "pdf",
            ),
            "etiquetteadditions" => array(
                "type" => "pdf",
                "name" => "Etiquettes Jeunes",
                "get" => array(
                    "obj" => "additions_des_jeunes",
                    "type" => (isset($_POST["mouvementatraiter"]) ? $_POST["mouvementatraiter"] : ""),
                ),
                "method" => "handle_pdf_output__etiquette",
                "selected" => false,
                "confirmation" => "additions_des_jeunes",
                "group" => "pdf",
            ),
            "emargement" => array(
                "type" => "pdf",
                "name" => "Liste d'emargement",
                "method" => "handle_pdf_output__liste_emargement",
                "bureau" => true,
                "get" => array(
                    "mode_edition" => "parbureau",
                ),
                "confirmation" => "scrutin",
                "livrable" => "emarge",
                "selected" => false,
                "group" => "pdf",
            ),
            "liste_electorale_par_bureau_pour_un_scrutin" => array(
                "type" => "pdf",
                "name" => "Liste électorale par bureau pour un scrutin",
                "method" => "handle_pdf_output__liste_electorale",
                "bureau" => true,
                "get" => array(
                    "mode_edition" => "parbureau",
                ),
                "confirmation" => "scrutin",
                "livrable" => "emarge",
                "selected" => false,
                "group" => "pdf",
            ),
            "carteelecteur" => array(
                "type" => "pdf",
                "name" => "Cartes electorales",
                "method" => "handle_pdf_output__carte_electorale",
                "get" => array(
                    "mode_edition" => "commune",
                    "order_by_datenaissance" => $order_by_datenaissance,
                ),
                "selected" => false,
                "confirmation" => "options_cartes_electorales",
                "group" => "pdf",
            ),
            "nouvellecarteelecteur" => array(
                "type" => "pdf",
                "name" => "Nouvelles cartes electorales",
                "method" => "handle_pdf_output__carte_electorale",
                "get" => array(
                    "mode_edition" => "lastone",
                    "multi" => true,
                    "liste" => $liste,
                    "date" => $date,
                    "modifications_sans_changement_bureau" => $sans_changement_de_bureau,
                ),
                "selected" => false,
                "confirmation" => "selection_traitement",
                "group" => "pdf",
            ),
            "commission" => array(
                "type" => "pdf",
                "name" => "Commission par bureau",
                "method" => "handle_pdf_output__listing_mouvements_tableau_commission_multi",
                "selected" => false,
                "group" => "pdf",
            ),
            "tableau-commune" => array(
                "type" => "pdf",
                "name" => "Tableau rectificatif (commune)",
                "method" => "handle_pdf_output__listing_mouvements_tableau_commission",
                "get" => array(
                    "mode_edition" => "tableau",
                    "tableau" => "tableau_rectificatif",
                    "globale" => "true",
                ),
                "selected" => false,
                "group" => "pdf",
            ),
            "tableau-bureau" => array(
                "type" => "pdf",
                "name" => "Tableau rectificatif (bureau)",
                "method" => "handle_pdf_output__listing_mouvements_tableau_commission",
                "get" => array(
                    "mode_edition" => "tableau",
                    "tableau" => "tableau_rectificatif",
                ),
                "selected" => false,
                "group" => "pdf",
            ),
            // MISC
            "email" => array(
                "type" => "email",
                "name" => "Envoi de courriels",
                "selected" => false,
                "confirmation" => "email",
                "group" => "misc",
            ),
        );
        $optgroup_trt = array("annuel", "j5", "archivage", "refonte", "election_mention");
        $optgroup_pdf = array("listeelectorale", "etiquetteelecteur", "etiquetteadditions", "carteelecteur", "nouvellecarteelecteur", "emargement", "liste_electorale_par_bureau_pour_un_scrutin", "commission", "tableau-commune", "tableau-bureau");
        $optgroup_misc = array("email");


        /**
         * COLLECTIVITES
         */
        //
        $query_collectivites = sprintf(
            'SELECT om_collectivite.om_collectivite AS id, om_collectivite.libelle AS ville FROM %1$som_collectivite WHERE niveau=\'1\' ORDER BY om_collectivite.libelle',
            DB_PREFIXE
        );
        $res_collectivites = $this->f->db->query($query_collectivites);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query_collectivites."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res_collectivites);
        //
        $collectivites = array();
        while ($row =& $res_collectivites->fetchrow(DB_FETCHMODE_ASSOC)) {
            $collectivites [$row['id']] = array('ville' => $row['ville'], 'selected' => false);
        }

        /**
         *
         */
        // start .formulaire
        echo "<div class=\"formulaire\">\n";
        echo "<form method=\"post\" id=\"trtmulti_form\" name=\"trtmulti\" action=\"../app/index.php?module=module_multi&view=traitement_par_lot\">\n";
        echo "\t\t<input name=\"posted_from_tab\" type=\"hidden\" value=\"".$tab."\" />\n";
        echo "\t\t<input name=\"return\" type=\"hidden\" />\n";

        /**
         * ETAPE 1
         */
        // start #tabs-0
        echo "<div id=\"tabs-0\">";
        echo "<h2>".__("Etape 1: Choix de la ou des collectivite(s)")."</h2>\n";
        //
        if (isset ($_POST['trtmulti_form_0_action_valid'])) {
            if (!isset ($_POST ['collectivites'])) {
                $this->f->displayMessage ("erreur", __("Il faut selectionner au moins une collectivite."));
            }
        }
        //
        $tabindex = 1;
        // SELECT COLLECTIVITES
        echo "\n<div class=\"field\">\n";
        echo "\t<label for=\"collectivites\">".__("Collectivites")." - ";
        echo "<a id=\"selectall\">".__("Tout selectionner")."</a> - ";
        echo "<a id=\"unselectall\">".__("Tout deselectionner")."</a>";
        echo "</label>\n";
        if (isset ($_POST['collectivites'])) {
            foreach ($_POST['collectivites'] as $selected) {
                $collectivites[$selected]['selected'] = true;
            }
        }
        echo "<select id=\"collectivites\" name=\"collectivites[]\" multiple=\"multiple\" size=\"15\">\n";
        foreach ($collectivites as $key => $collectivite) {
            echo "<option ".($collectivite['selected']==true?"selected=\"selected\"":"")." value=\"".$key."\">";
            echo $collectivite['ville'];
            echo "</option>";
        }
        echo "</select>\n";
        echo "\n</div>\n";

        // ETAPE SUIVANTE - RETOUR
        echo "\t<div class=\"formControls\">\n";
        echo "\t\t<input name=\"trtmulti_form_0.action.valid\" tabindex=\"".$tabindex++."\" value=\"".__("Etape suivante")."\" type=\"submit\" />\n";
        echo "\t\t<a class=\"retour\" title=\"".__("Retour")."\" ";
        echo "href=\"".OM_ROUTE_DASHBOARD."\">";
        echo __("Retour");
        echo "</a>\n";
        echo "\t</div>\n";
        // end #tabs-0
        echo "</div>\n";

        /**
         * ETAPE 2
         */
        // start #tabs-1
        echo "<div id=\"tabs-1\">";
        echo "<h2>".__("Etape 2: Choix du traitement")."</h2>\n";
        //
        $tabindex = 1;
        // SELECT TRAITEMENT
        echo "\n<div class=\"field\">\n";
        echo "\t<label for=\"traitement\">".__("Traitements")."</label>\n";
        if (isset ($_POST['traitement'])) {
            $traitements[$_POST['traitement']]['selected'] = true;
        }
        echo "<select name=\"traitement\">\n";
        echo "<optgroup label=\"Traitements\">";
        foreach ($optgroup_trt as $trt) {
            echo "<option ".($traitements[$trt]['selected']==true?"selected=\"selected\"":"")." value=\"".$trt."\">";
            echo $traitements[$trt]['name'];
            echo "</option>";
        }
        echo "</optgroup>";
        echo "<optgroup label=\"Editions\">";
        foreach ($optgroup_pdf as $pdf) {
            echo "<option ".($traitements[$pdf]['selected']==true?"selected=\"selected\"":"")." value=\"".$pdf."\">";
            echo $traitements[$pdf]['name'];
            echo "</option>";
        }
        echo "</optgroup>";
        echo "<optgroup label=\"Divers\">";
        foreach ($optgroup_misc as $misc) {
            echo "<option ".($traitements[$misc]['selected']==true?"selected=\"selected\"":"")." value=\"".$misc."\">";
            echo $traitements[$misc]['name'];
            echo "</option>";
        }
        echo "</optgroup>";
        /*foreach ($traitements as $key => $traitement) {
            echo "<option ".($traitement['selected']==true?"selected=\"selected\"":"")." value=\"".$key."\">";
            echo $traitement['name'];
            echo "</option>";
        }*/
        echo "</select>\n";
        echo "\n</div>\n";
        // ETAPE SUIVANTE - RETOUR
        echo "\t<div class=\"formControls\">\n";
        echo "\t\t<input name=\"trtmulti_form_1.action.valid\" tabindex=\"".$tabindex++."\" value=\"".__("Etape suivante")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
        echo "\t\t<a class=\"retour retour-submit\" title=\"".__("Retour")."\" onclick=\"window.document.trtmulti.return.value='1';\" ";
        echo "href=\"#\">";
        echo __("Retour");
        echo "</a>\n";
        echo "\t</div>\n";
        // end #tabs-1
        echo "</div>\n";

        /**
         * ETAPE 3
         */
        // start #tabs-2
        echo "<div id=\"tabs-2\">";
        echo "<h2>".__("Etape 3: Confirmation")."</h2>\n";
        if (isset($_POST['traitement'])
            && isset($traitements[$_POST['traitement']]['confirmation'])) {
            //
            if ($traitements[$_POST['traitement']]['confirmation'] == "2dates") {
                $form_entre2dates = " ".__("Date debut");
                $form_entre2dates .= " <input type=\"text\" name=\"datedebut\" tabindex=\"1\" size=\"15\" class=\"datepicker champFormulaire\" id=\"datedebut\" onchange=\"fdate(this)\" ";
                $form_entre2dates .= (isset($_POST['datedebut'])?"value=\"".$_POST['datedebut']."\" ":"");
                $form_entre2dates .= "/>";
                $form_entre2dates .= " ".__("Date fin");
                $form_entre2dates .= " <input type=\"text\" name=\"datefin\" tabindex=\"2\" size=\"15\" class=\"datepicker champFormulaire\" id=\"datefin\" onchange=\"fdate(this)\" ";
                $form_entre2dates .= (isset($_POST['datefin'])?"value=\"".$_POST['datefin']."\" ":"");
                $form_entre2dates .= "/>";

                echo $form_entre2dates;
            } elseif ($traitements[$_POST['traitement']]['confirmation'] == "scrutin") {
                //
                $validation = 0;
                $maj = 0;
                $champs = array("scrutin_referentiel_id");
                //
                $form = $this->f->get_inst__om_formulaire(array(
                    "validation" => $validation,
                    "maj" => $maj,
                    "champs" => $champs,
                ));
                //
                $form->setLib("scrutin_referentiel_id", __("Selectionner le scrutin concerné"));
                $form->setType("scrutin_referentiel_id", "select");
                //
                $scrutins = $this->f->get_all_results_from_db_query(sprintf(
                    'SELECT referentiel_id, libelle, date_tour1, date_tour2 FROM %1$sreu_scrutin GROUP BY referentiel_id, libelle, date_tour1, date_tour2 ORDER BY date_tour1 DESC',
                    DB_PREFIXE
                ));
                //
                $contenu = array();
                $contenu[0] = array();
                $contenu[1] = array();
                foreach ($scrutins["result"] as $key => $value) {
                    array_push($contenu[0], $value["referentiel_id"]);
                    array_push($contenu[1], $value["date_tour1"]. " & ".$value["date_tour2"]." - ".$value["libelle"]);
                }
                $form->setSelect("scrutin_referentiel_id", $contenu);
                //
                if (isset($_POST["scrutin_referentiel_id"])) {
                    $form->setVal("scrutin_referentiel_id", $_POST["scrutin_referentiel_id"]);
                }
                //
                $form->entete();
                $form->afficher($champs, 0, false, false);
                $form->enpied();
            } elseif ($traitements[$_POST['traitement']]['confirmation'] == "date_election") {
                $form_date_election = " ".__("Date scrutin");
                $form_date_election .= " <input type=\"text\" name=\"datescrutin\" tabindex=\"1\" size=\"15\" class=\"datepicker champFormulaire\" id=\"datescrutin\" onchange=\"fdate(this)\" ";
                $form_date_election .= (isset($_POST['datescrutin'])?"value=\"".$_POST['datescrutin']."\" ":"");
                $form_date_election .= "/>";

                echo $form_date_election;
            } elseif ($traitements[$_POST['traitement']]['confirmation'] == "email") {
                //
                $validation = 0;
                $maj = 0;
                $champs = array("object", "message");
                //
                $form = $this->f->get_inst__om_formulaire(array(
                    "validation" => $validation,
                    "maj" => $maj,
                    "champs" => $champs,
                ));
                //
                $form->setLib("object", __("Objet"));
                $form->setType("object", "text");
                $form->setTaille("object", 100);
                $form->setMax("object", 100);
                $form->setVal("object", (isset($_POST['object'])?$_POST['object']:""));
                //
                $form->setLib("message", __("Message"));
                $form->setType("message", "textarea");
                $form->setTaille("message", 100);
                $form->setMax("message", 20);
                $form->setVal("message", (isset($_POST['message'])?$_POST['message']:""));
                //
                $form->entete();
                $form->afficher($champs, $validation, false, false);
                $form->enpied();
            } elseif ($traitements[$_POST['traitement']]['confirmation'] == "additions_des_jeunes") {
                //
                $validation = 0;
                $maj = 0;
                $champs = array("mouvementatraiter");
                //
                $form = $this->f->get_inst__om_formulaire(array(
                    "validation" => $validation,
                    "maj" => $maj,
                    "champs" => $champs,
                ));
                //
                $form->setLib("mouvementatraiter", __("Selectionner ici le tableau dont vous souhaitez imprimer les etiquettes"));
                $form->setType("mouvementatraiter", "select");
                //
                $mouvements_io = sprintf(
                    'SELECT code, libelle FROM %1$sparam_mouvement WHERE effet=\'Election\' GROUP BY code, libelle ORDER BY code',
                    DB_PREFIXE
                );
                $res = $this->f->db->query($mouvements_io);
                $this->f->addToLog(
                    __METHOD__."(): db->query(\"".$mouvements_io."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                //
                $contenu = array();
                $contenu[0] = array();
                $contenu[1] = array();
                while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                    array_push($contenu[0], $row["code"]);
                    array_push($contenu[1], __("Tableau des additions")." - ".$row["libelle"]);
                }
                $form->setSelect("mouvementatraiter", $contenu);
                //

                $additions = array();
                if (isset($_POST["mouvementatraiter"])) {
                    $form->setVal("mouvementatraiter", $_POST["mouvementatraiter"]);
                }
                //
                $form->entete();
                $form->afficher($champs, 0, false, false);
                $form->enpied();
            } elseif ($traitements[$_POST['traitement']]['confirmation'] == "selection_traitement") {
                //
                $validation = 0;
                $maj = 0;
                $champs = array("liste", "date", "sans_changement_de_bureau");
                //
                $form = $this->f->get_inst__om_formulaire(array(
                    "validation" => $validation,
                    "maj" => $maj,
                    "champs" => $champs,
                ));
                //
                $form->setLib("date", __("Date"));
                $form->setType("date", "date");
                $form->setOnchange("date", "fdate(this);");
                $form->setTaille("date", 12);
                $form->setMax("date", 12);
                //
                $form->setLib("sans_changement_de_bureau", __("Prendre en compte les modifications sans changement de bureau ?"));
                $form->setType("sans_changement_de_bureau", "checkbox");
                $form->setTaille("sans_changement_de_bureau", 1);
                $form->setMax("sans_changement_de_bureau", 1);

                $form->setLib("liste", __("Liste"));
                $form->setType("liste", "select");
                $contenu = array(
                    0 => array(),
                    1 => array(),
                );
                foreach ($this->f->get_all__listes() as $key => $liste) {
                    array_push($contenu[0], $liste["liste"]);
                    array_push($contenu[1], $liste["libelle_liste"]);
                }
                $form->setSelect("liste", $contenu);

                // if (isset($_POST["selection_traitement"])) {
                //     $form->setVal("selection_traitement", $_POST["selection_traitement"]);
                // }
                if (isset($_POST["liste"])) {
                    $form->setVal("liste", $_POST["liste"]);
                }
                if (isset($_POST["date"])) {
                    $form->setVal("date", $_POST["date"]);
                }
                
                $form->entete();
                $form->afficher($champs, 0, false, false);
                $form->enpied();
            } elseif ($traitements[$_POST['traitement']]['confirmation'] == "options_cartes_electorales") {
                //
                $validation = 0;
                $maj = 0;
                $champs = array("order_by_datenaissance", );
                //
                $form = $this->f->get_inst__om_formulaire(array(
                    "validation" => $validation,
                    "maj" => $maj,
                    "champs" => $champs,
                ));
                //
                $form->setLib("order_by_datenaissance", __("Trier les cartes par date de naissance ?"));
                $form->setType("order_by_datenaissance", "checkbox");
                $form->setTaille("order_by_datenaissance", 1);
                $form->setMax("order_by_datenaissance", 1);
                if ($order_by_datenaissance === true) {
                    $form->setVal("order_by_datenaissance", 't');
                }
                //
                $form->entete();
                $form->afficher($champs, 0, false, false);
                $form->enpied();
            } elseif ($traitements[$_POST['traitement']]['confirmation'] == "mouvementatraiter") {
                //
                $validation = 0;
                $maj = 0;
                $champs = array("mouvementatraiter");
                //
                $form = $this->f->get_inst__om_formulaire(array(
                    "validation" => $validation,
                    "maj" => $maj,
                    "champs" => $champs,
                ));
                //
                $form->setLib("mouvementatraiter", __("Selectionner ici le ou les tableau(x) ".
                                                       "qui entre(nt) en vigueur a la date de l'election"));
                $form->setType("mouvementatraiter", "checkbox_multiple");
                //
                $mouvements_io = sprintf(
                    'SELECT code, libelle, effet FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE (effet=\'Election\' or effet=\'Immediat\') and date_tableau=\'%2$s\' and etat=\'actif\' GROUP BY code, libelle, effet ORDER BY effet DESC, code',
                    DB_PREFIXE,
                    $this->f->getParameter("datetableau")
                );
                $res = $this->f->db->query($mouvements_io);
                $this->f->addToLog(
                    __METHOD__."(): db->query(\"".$mouvements_io."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                //
                $contenu = array();
                $contenu[0] = array("Immediat",);
                $contenu[1] = array(__("Tableau des cinq jours"),);
                while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                    $this->f->addToLog(__METHOD__."(): ".print_r($row, true), EXTRA_VERBOSE_MODE);
                    if ($row["effet"] == "Election") {
                        array_push($contenu[0], $row["code"]);
                        array_push($contenu[1], __("Tableau des additions")." - ".$row["libelle"]);
                    }
                }
                $form->setSelect("mouvementatraiter", $contenu);
                //
                $cinqjours = false;
                $additions = array();
                $mouvementatraiter = "";
                if (isset($_POST["mouvementatraiter"])) {
                    $val = "";
                    foreach($_POST["mouvementatraiter"] as $elem) {
                        if ($elem == "Immediat") {
                            $cinqjours = true;
                        } else {
                            array_push($additions, $elem);
                        }
                        $val .= $elem.";";
                    }
                    $mouvementatraiter = $val;
                    $form->setVal("mouvementatraiter", $val);
                }
                //
                $form->entete();
                $form->afficher($champs, 0, false, false);
                $form->enpied();
            } elseif ($traitements[$_POST['traitement']]['confirmation'] == "election_mention") {
                //
                require_once "../obj/traitement.".$_POST['traitement'].".class.php";
                //
                if (isset($traitements[$_POST['traitement']]['class'])) {
                    $traitement_class = $traitements[$_POST['traitement']]['class'];
                } else {
                    $traitement_class = $_POST['traitement']."Traitement";
                }
                //
                $trt = new $traitement_class();
                $trt->displayFormSection(true);

            }
        }

        // ETAPE SUIVANTE - RETOUR
        echo "\t<div class=\"formControls\">\n";
        echo "\t\t<input name=\"trtmulti_form_2.action.valid\" tabindex=\"".$tabindex++."\" value=\"".__("Confirmer le traitement")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
        echo "\t\t<a class=\"retour retour-submit\" title=\"".__("Retour")."\" name=\"retour\" value=\"test\" onclick=\"window.document.trtmulti.return.value='2';\" ";
        echo "href=\"#\">";
        echo __("Retour");
        echo "</a>\n";
        echo "\t</div>\n";
        // end #tabs-2
        echo "</div>\n";

        /**
         * ETAPE 4
         */
        // start #tabs-3
        echo "<div id=\"tabs-3\">";
        echo "<h2>".__("Etape 4: Resultat")."</h2>\n";
        //
        if ($tab == 3) {
            //
            $collectivite_tmp = $_SESSION["collectivite"];
            $libelle_collectivite_tmp = $this->f->collectivite['ville'];

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            // TRT TYPE
            if ($traitements[$_POST['traitement']]['type'] == "trt") {
                //
                require_once "../obj/traitement.".$_POST['traitement'].".class.php";
                //
                foreach ($_POST ['collectivites'] as $collectivite) {
                    //
                    $_SESSION["collectivite"] = $collectivite;
                    $_SESSION['libelle_collectivite'] = $collectivites[$collectivite]['ville'];
                    //
                    if (isset($traitements[$_POST['traitement']]['class'])) {
                        $traitement_class = $traitements[$_POST['traitement']]['class'];
                    } else {
                        $traitement_class = $_POST['traitement']."Traitement";
                    }
                    echo $traitement_class." - ".$_SESSION["collectivite"]." - ".$_SESSION['libelle_collectivite']."<br/>";
                    //
                    $trt = new $traitement_class();
                    if (isset($traitements[$_POST['traitement']]['params'])) {
                        $trt->setParams($traitements[$_POST['traitement']]['params']);
                    }
                    $trt->execute();
                }
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            // PDF TYPE
            // Le traitement de type PDF permet en fonction de l'option 'option_generation_multi_as_an_archive' :
            // - soit de générer des éditions PDF, de les compresser dans une archive ZIP stockée sur le disque et téléchargeable par l'utilisateur qui fait le traitement
            // - soit de générer des éditions PDF, de les stocker sur le disque et d'envoyer le/les liens par courriel à tous les utilisateurs de la collectivité
            if ($traitements[$_POST['traitement']]['type'] == "pdf") {
                //
                $traitement_class = $_POST['traitement']."Edition";
                echo $traitement_class;
                //
                // option_generation_as_an_archive
                //
                if ($this->f->is_option_generation_multi_as_an_archive_enabled() === true) {
                    //
                    require_once "../app/php/misc/zipfile.class.php";
                    $zip = new zipfile();
                    //
                    foreach ($_POST ['collectivites'] as $collectivite) {
                        // On se positionne sur la bonne collectivité pour faire le traitement
                        $_SESSION["collectivite"] = $collectivite;
                        $_SESSION['libelle_collectivite'] = $collectivites[$collectivite]['ville'];
                        $this->f->getCollectivite();
                        //
                        $pdf_editions_params = array();
                        //
                        $scrutin = "";
                        $scrutin_demande_id = "";
                        $scrutin_livrable_date = "";
                        if ($traitements[$_POST['traitement']]['confirmation'] == "date_election"
                            && isset($_POST['datescrutin'])) {
                            //
                            $livrable = $traitements[$_POST['traitement']]['livrable'];
                            $scrutin_res = $this->f->get_one_result_from_db_query(sprintf(
                                'select id from %1$sreu_scrutin where om_collectivite=%2$s and date_tour1=\'%3$s\' and %4$s_livrable<>\'\' LIMIT 1;',
                                DB_PREFIXE,
                                intval($_SESSION["collectivite"]),
                                $this->f->formatDate($_POST['datescrutin'], false),
                                $livrable
                            ), true);
                            if ($scrutin_res["code"] != "KO") {
                                $scrutin = $scrutin_res["result"];
                                $inst__scrutin = $this->f->get_inst__om_dbform(array(
                                    "obj" => "reu_scrutin",
                                    "idx" => intval($scrutin),
                                ));
                                $scrutin_demande_id = $inst__scrutin->getVal($livrable."_livrable_demande_id");
                                $scrutin_livrable_date = $inst__scrutin->getVal($livrable."_livrable_date");
                                $scrutin_libelle = $inst__scrutin->getVal("libelle");
                                $scrutin_tour1 = $inst__scrutin->getVal("date_tour1");
                                $scrutin_tour2 = $inst__scrutin->getVal("date_tour2");
                            }
                        } elseif ($traitements[$_POST['traitement']]['confirmation'] == "scrutin"
                            && isset($_POST['scrutin_referentiel_id'])) {
                            //
                            $livrable = $traitements[$_POST['traitement']]['livrable'];
                            $scrutin_res = $this->f->get_one_result_from_db_query(sprintf(
                                'select id from %1$sreu_scrutin where om_collectivite=%2$s and referentiel_id=\'%3$s\' and %4$s_livrable<>\'\' LIMIT 1;',
                                DB_PREFIXE,
                                intval($_SESSION["collectivite"]),
                                intval($_POST['scrutin_referentiel_id']),
                                $livrable
                            ), true);
                            if ($scrutin_res["code"] != "KO") {
                                $scrutin = $scrutin_res["result"];
                                $inst__scrutin = $this->f->get_inst__om_dbform(array(
                                    "obj" => "reu_scrutin",
                                    "idx" => intval($scrutin),
                                ));
                                $scrutin_demande_id = $inst__scrutin->getVal($livrable."_livrable_demande_id");
                                $scrutin_livrable_date = $inst__scrutin->getVal($livrable."_livrable_date");
                                $scrutin_libelle = $inst__scrutin->getVal("libelle");
                                $scrutin_tour1 = $inst__scrutin->getVal("date_tour1");
                                $scrutin_tour2 = $inst__scrutin->getVal("date_tour2");
                            }
                        }
                        if (isset($traitements[$_POST['traitement']]["bureau"])) {
                            //
                            foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
                                $params = array();
                                //
                                if (isset($traitements[$_POST['traitement']]['get'])) {
                                    foreach($traitements[$_POST['traitement']]['get'] as $key => $value) {
                                        $params[$key] = $value;
                                    }
                                }
                                //
                                $params['multi'] = true;
                                $params['id'] = "bureau";
                                $params['idx'] = $bureau["code"];
                                //$params['obj'] = "electeur";
                                $params['bureau_code'] = $bureau["code"];
                                $params['mode'] = "chaine";
                                if ($scrutin_demande_id != "") {
                                    $params["scrutin"] = $scrutin;
                                    $params["scrutin_libelle"] = $scrutin_libelle;
                                    $params["scrutin_demande_id"] = $scrutin_demande_id;
                                    $params["scrutin_livrable_date"] = $scrutin_livrable_date;
                                    $params["scrutin_tour1"] = $scrutin_tour1;
                                    $params["scrutin_tour2"] = $scrutin_tour2;
                                }
                                $pdf_editions_params[] = $params;
                            }
                        } else {
                            $params = array();
                            //
                            if (isset($traitements[$_POST['traitement']]['get'])) {
                                foreach($traitements[$_POST['traitement']]['get'] as $key => $value) {
                                    $params[$key] = $value;
                                }
                            }
                            $params['multi'] = true;
                            //$params['obj'] = "electeur";
                            $params['mode'] = "chaine";
                            if ($scrutin_demande_id != "") {
                                $params["scrutin"] = $scrutin;
                                $params["scrutin_libelle"] = $scrutin_libelle;
                                $params["scrutin_demande_id"] = $scrutin_demande_id;
                                $params["scrutin_livrable_date"] = $scrutin_livrable_date;
                                $params["scrutin_tour1"] = $scrutin_tour1;
                                $params["scrutin_tour2"] = $scrutin_tour2;
                            }
                            if (isset($traitements[$_POST['traitement']]['id'])) {
                                $params['id'] = $traitements[$_POST['traitement']]['id'];
                            }
                            $pdf_editions_params[] = $params;
                        }
                        //
                        foreach ($pdf_editions_params as $pdf_edition_params) {
                            //
                            if (isset($traitements[$_POST['traitement']]['method'])) {
                                $method_name = $traitements[$_POST['traitement']]['method'];
                                $pdf_edition = $this->$method_name($pdf_edition_params);
                            }
                            //
                            $zip->addfile($pdf_edition["pdf_output"], $pdf_edition["filename"]);
                        }
                    }
                    // On revient à notre collectivité d'origine (multi)
                    $_SESSION["collectivite"] = $collectivite_tmp;
                    $_SESSION['libelle_collectivite'] = $libelle_collectivite_tmp;
                    $this->f->getCollectivite();
                    //
                    $archive = $zip->file();
                    //
                    $filemetadata = array(
                        "filename" => date("Ymd-Gis")."-".$_POST['traitement'].".zip",
                        "size" => strlen($archive),
                        "mimetype" => "application/zip",
                    );
                    $uid = $this->f->store_file(
                        $archive,
                        $filemetadata,
                        "archive"
                    );
                    if ($uid == false) {
                        //
                        $this->f->displayMessage("error", __("Erreur lors du stockage/enregistrement du fichier."));
                    } else {
                        //
                        $this->f->displayMessage("valid", __("Le traitement est terminé."));
                        //
                        echo "\t<div class=\"formControls\">\n";
                        echo "\t\t<a class=\"om-prev-icon zip-16\" target=\"_blank\" href=\"".OM_ROUTE_FORM."&snippet=file&uid=".$uid."\"> ";
                        echo __("Telecharger l'archive resultat");
                        echo "</a>\n";
                        echo "</div>";
                    }
                } else {
                    // GESTION DU MAIL
                    //
                    $sql = sprintf(
                        'SELECT om_collectivite, email FROM %1$som_utilisateur WHERE om_collectivite IN (%2$s)',
                        DB_PREFIXE,
                        implode(",", $_POST['collectivites'])
                    );
                    $res = $this->f->db->query($sql);
                    $this->f->addToLog(
                        __METHOD__."(): db->query(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($res);
                    //
                    $emails = array();
                    foreach ($_POST['collectivites'] as $collectivite_id) {
                        $emails[$collectivite_id] = array();
                    }
                    while ($row=&$res->fetchrow(DB_FETCHMODE_ASSOC)) {
                        if (!isset($emails[$row['om_collectivite']])) {
                            $emails[$row['om_collectivite']] = array();
                        }
                        $emails[$row['om_collectivite']][] = $row['email'];
                    }
                    //
                    $cpt = array("sended" => 0, "ok" => 0, "error" => 0);
                    //
                    $root_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on" ? "https://":"http://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                    $root_url = explode("?", $root_url);
                    $root_url = $root_url[0];
                    //
                    $this->f->addToLog(__METHOD__."(): root_url=".$root_url, EXTRA_VERBOSE_MODE);
                    //
                    foreach ($_POST['collectivites'] as $collectivite) {
                        // On se positionne sur la bonne collectivité pour faire le traitement
                        $_SESSION["collectivite"] = $collectivite;
                        $_SESSION['libelle_collectivite'] = $collectivites[$collectivite]['ville'];
                        $this->f->getCollectivite();
                        //
                        $pdf_editions_params = array();
                        if (isset($traitements[$_POST['traitement']]["bureau"])) {
                            foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
                                $params = array();
                                //
                                if (isset($traitements[$_POST['traitement']]['get'])) {
                                    foreach($traitements[$_POST['traitement']]['get'] as $key => $value) {
                                        $params[$key] = $value;
                                    }
                                }
                                //
                                $_GET['mode'] = "save";
                                $_GET['id'] = "bureau";
                                $_GET['idx'] = $bureau["code"];
                                $_GET['obj'] = "electeur";
                                $_GET['bureau_code'] = $bureau["code"];
                                $pdf_editions_params[] = $params;
                            }
                        } else {
                            $params = array();
                            //
                            if (isset($traitements[$_POST['traitement']]['get'])) {
                                foreach($traitements[$_POST['traitement']]['get'] as $key => $value) {
                                    $params[$key] = $value;
                                }
                            }
                            //
                            $params['mode'] = "save";
                            $params['obj'] = "electeur";
                            if (isset($traitements[$_POST['traitement']]['id'])) {
                                $params['id'] = $traitements[$_POST['traitement']]['id'];
                            }
                            $pdf_editions_params[] = $params;
                        }
                        //
                        $generated_files = array();
                        foreach ($pdf_editions_params as $pdf_edition_params) { 
                            //
                            if (isset($traitements[$_POST['traitement']]['method'])) {
                                $method_name = $traitements[$_POST['traitement']]['method'];
                                $pdf_edition = $this->$method_name($pdf_edition_params);
                            }
                            //
                            $generated_files[] = $pdf_edition;
                        }

                        //
                        $links = "";
                        foreach ($generated_files as $generated_file) {
                            $links .= sprintf(
                                '<li><a href="%s?%s">%s</a></li>',
                                $root_url,
                                "module=form&snippet=file&uid=".$generated_file["uid"],
                                $generated_file["filename"]
                            );
                        }

                        //
                        $message = sprintf(
                            '<p>%s</p><ul>%s</ul><p>%s</p>',
                            __("Bonjour, veuillez trouver ci-dessous le ou les liens vers la ou les editions nouvellement disponible(s) dans le logiciel openElec."),
                            $links,
                            __("Votre administrateur openElec.")
                        );
                        $object = __("Nouvelle édition disponible dans openElec");
                        //
                        foreach ($emails[$collectivite] as $email) {
                            $sended =false;
                            $sended = $this->f->sendMail($object, $message, $email);
                            $cpt["sended"]++;
                            if ($sended) {
                                $cpt["ok"]++;
                            } else {
                                $cpt["error"]++;
                            }
                        }
                    }
                    // On revient à notre collectivité d'origine (multi)
                    $_SESSION["collectivite"] = $collectivite_tmp;
                    $_SESSION['libelle_collectivite'] = $libelle_collectivite_tmp;
                    $this->f->getCollectivite();
                    //
                    $this->f->addToLog(__METHOD__."(): cpt ".print_r($cpt, true), EXTRA_VERBOSE_MODE);
                    //
                    if ($cpt["error"] == 0) {
                        //
                        $this->f->displayMessage("valid", __("Le traitement est terminé. Les courriels ont été transmis."));
                    } else {
                        //
                        $this->f->displayMessage("error", __("Il y a eu des erreurs pendant l'envoi de mail. Contactez votre administrateur."));
                    }
                }
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////
            // TXT TYPE
            if ($traitements[$_POST['traitement']]['type'] == "txt") {
                //
                $collectivites = $_POST['collectivites'];
                //
                if (isset($traitements[$_POST['traitement']]['method'])) {
                    $params = array(
                        "collectivites" => $collectivites,
                    );
                    if (isset($_POST['datedebut'])) {
                        $params["datedebut"] = $_POST['datedebut'];
                    }
                    if (isset($_POST['datefin'])) {
                        $params["datefin"] = $_POST['datefin'];
                    }
                    $method_name = $traitements[$_POST['traitement']]['method'];
                    $results = $this->$method_name($params);
                }
                //
                if (isset($results)
                    && isset($results["lines"])
                    && $results["lines"] != 0
                    && isset($results["uid"])
                    && $results["uid"] != false) {
                    //
                    $this->f->displayMessage("valid", __("Le traitement est termine."));
                    //
                    echo "\t<div class=\"formControls\">\n";
                    echo "\t\t<a class=\"om-prev-icon txt-16\" target=\"_blank\" href=\"".OM_ROUTE_FORM."&snippet=file&uid=".$results["uid"] ."\"> ";
                    echo __("Telecharger le fichier resultat");
                    echo "</a>\n";
                    echo "</div>";
                } else {
                    //
                    $this->f->displayMessage("error", __("Une erreur s'est produite. Contactez votre administrateur."));
                }
            }

            // EMAIL TYPE
            if ($traitements[$_POST['traitement']]['type'] == "email") {
                // GESTION DU MAIL
                //
                $sql = sprintf(
                    'SELECT email FROM %1$som_utilisateur WHERE om_collectivite IN (%2$s)',
                    DB_PREFIXE,
                    implode(",", $_POST['collectivites'])
                );
                $res = $this->f->db->query($sql);
                $this->f->addToLog(
                    __METHOD__."(): db->query(\"".$sql."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                //
                $emails = array();
                while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                    $emails[] = $row['email'];
                }
                //
                $cpt = array("sended" => 0, "ok" => 0, "error" => 0);
                //
                foreach ($emails as $email) {
                    $sended =false;
                    $sended = $this->f->sendMail($_POST['object'], $_POST['message'], $email);
                    $cpt["sended"]++;
                    if ($sended) {
                        $cpt["ok"]++;
                    } else {
                        $cpt["error"]++;
                    }
                }
                //
                if ($cpt["error"] == 0) {
                    //
                    $this->f->displayMessage("valid", __("Le traitement est termine."));
                } else {
                    //
                    $this->f->displayMessage("error", __("Il y a eu des erreurs pendant l'envoi. Contactez votre administrateur."));
                }
            }

            //
            $_SESSION["collectivite"] = $collectivite_tmp;
            $_SESSION['libelle_collectivite'] = $libelle_collectivite_tmp;
            $this->f->getCollectivite();

            //
            echo "\t<div class=\"formControls\">\n";
            echo "\t\t<a class=\"retour retour-submit\" title=\"".__("Retour")."\" name=\"retour\" value=\"test\" onclick=\"window.document.trtmulti.return.value='3';\" ";
            echo "href=\"#\">";
            echo __("Retour");
            echo "</a>\n";
            echo "</div>";
        }
        // end #tabs-3
        echo "</div>\n";

        // end .formulaire
        echo "</form>\n";
        echo "</div>\n";

        // end #tabs
        echo "</div>";
        printf('</div>');
    }

    function view__facturation() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_multi");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");
        /**
         *
         */
        $facturations = array(
            array(
                "title" => __("Titres de recettes"),
                "description" => __("Ce fichier est destine a la division des finances. C'est un document PDF recapitulant le montant du par chaque commune ainsi que le total."),
                "id" => "facturation-titres-recettes",
                "class" => "pdf",
                "target" => "_blank",
                "href" => "../app/index.php?module=module_multi&view=edition_pdf__facturation_commune_titre_de_recette",
                "help" => __("Telecharger le fichier"),
            ),
            array(
                "title" => __("Transfert des informations budgetaires"),
                "description" => __("Ce fichier est un fichier txt de transfert des informations budgetaires pour l'application astre."),
                "id" => "facturation-transfert-budget",
                "class" => "traitement",
                "href" => "../app/index.php?module=module_multi&view=export__facturation_transfert_budget",
                "help" => __("Generer le fichier"),
            ),
            array(
                "title" => __("Publipostage"),
                "description" => __("Ce fichier est destine au publipostage pour la generation du courrier aux differentes communes."),
                "id" => "facturation-publipostage",
                "class" => "traitement",
                "href" => "../app/index.php?module=module_multi&view=export__facturation_publipostage",
                "help" => __("Generer le fichier"),
            ),
        );

        /**
         *
         */
        $listing = "";
        foreach ($facturations as $key => $value) {
            $listing .= sprintf(
                '<div class="#">
                    <h3>%1$s</h3>
                    <p>%2$s</p>
                    <p><a class="om-prev-icon %3$s-16" id="%4$s-link" href="%5$s"%6$s>%7$s</a></p>
                    <span id="%4$s" class="both"><!-- --></span>
                </div><br/>',
                $value["title"],
                $value["description"],
                $value["class"],
                $value["id"],
                $value["href"],
                (isset($value["target"]) ? ' target="'.$value["target"].'"' : ''),
                $value["help"]
            );
        }
        printf(
            '<div id="module-facturation">

        <script type="text/javascript">
        // <![CDATA[
        $(document).ready(function(){
            function ajax_call(id, href) {
                $("#"+id).html(msg_loading);
                $.ajax({
                    type: "GET",
                    url: href,
                    dataType: "html",
                    error:function(msg){
                        alert( "Error !: " + msg );
                    },
                    success:function(data){
                        $("#"+id).html("<div id=\"message\"><p>"+data+"</p></div>");
                    }
                });
            }
            compose_data_href_on_link("#facturation-transfert-budget-link");
            $("#facturation-transfert-budget-link").click(
                function() {
                    ajax_call("facturation-transfert-budget", $(this).attr("data-href"));
                }
            );
            compose_data_href_on_link("#facturation-publipostage-link");
            $("#facturation-publipostage-link").click(
                function() {
                    ajax_call("facturation-publipostage", $(this).attr("data-href"));
                }
            );
        });
        // ]]>
        </script>

            <div class="instructions">
                <p>%s</p>
            </div>
            %s
        </div>',
            __("Ce module permet de generer les documents necessaires a la facturation des communes.")." ".__("Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs."),
            $listing
        );
    }

    function view__statistiques() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_multi");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");
        /**
         *
         */
        $statistiques = array(
            array(
                "title" => __("Statistique sur la saisie"),
                "description" => __("Nombre d'electeurs au dernier tableau, et nombre d'inscriptions, de modifications et de radiations par commune a la date de tableau en cours."),
                "id" => "multi-statistiques-sur-la-saisie",
                "class" => "pdf",
                "target" => "_blank",
                "href" => "../app/index.php?module=module_multi&view=edition_pdf__statistiques_mouvements_par_commune",
                "help" => __("Telecharger le fichier"),
            ),
            array(
                "title" => __("Statistique sur la saisie par utilisateur"),
                "description" => __("Nombre d'inscriptions, de modifications et de radiations par utilisateur a la date de tableau en cours."),
                "id" => "multi-statistiques-sur-la-saisie-par-utilisateur",
                "class" => "pdf",
                "target" => "_blank",
                "href" => "../app/index.php?module=module_multi&view=edition_pdf__statistiques_saisie_par_utilisateur",
                "help" => __("Telecharger le fichier"),
            ),
            array(
                "title" => __("Statistique sur les inscriptions par sexe"),
                "description" => __("Nombre d'inscriptions d'office, d'inscriptions par sexe a la date de tableau en cours."),
                "id" => "multi-statistiques-sur-les-inscriptions-par-sexe",
                "class" => "pdf",
                "target" => "_blank",
                "href" => "../app/index.php?module=module_multi&view=edition_pdf__statistiques_inscriptions",
                "help" => __("Telecharger le fichier"),
            ),
            array(
                "title" => __("Statistiques generales des mouvements"),
                "description" => __("Nombre d'inscriptions, de modifications et de radiations par collectivite a la date de tableau en cours."),
                "id" => "multi-statistiques-generales-des-mouvements",
                "class" => "pdf",
                "target" => "_blank",
                "href" => "../app/index.php?module=form&obj=mouvement&action=301",
                "help" => __("Telecharger le fichier"),
            ),
        );

        /**
         *
         */
        $listing = "";
        foreach ($statistiques as $key => $value) {
            $listing .= sprintf(
                '<div class="#">
                    <h3>%1$s</h3>
                    <p>%2$s</p>
                    <p><a class="om-prev-icon %3$s-16" id="%4$s-link" href="%5$s"%6$s>%7$s</a></p>
                    <span id="%4$s" class="both"><!-- --></span>
                </div><br/>',
                $value["title"],
                $value["description"],
                $value["class"],
                $value["id"],
                $value["href"],
                (isset($value["target"]) ? ' target="'.$value["target"].'"' : ''),
                $value["help"]
            );
        }
        printf(
            '<div id="module-statistiques">
            <div class="instructions">
                <p>%s</p>
            </div>
            %s
        </div>',
            __("Ce module permet de generer les documents necessaires aux statistiques sur la saisie.")." ".__("Ces traitements portant sur toutes les communes, les temps de chargement peuvent etre longs."),
            $listing
        );
    }


    /**
     * VIEW - view__edition_pdf__facturation_commune_titre_de_recette.
     *
     * @return void
     */
    function view__edition_pdf__facturation_commune_titre_de_recette() {
        //
        require_once "../obj/edition_pdf__facturation_commune_titre_de_recette.class.php";
        $inst_edition_pdf = new edition_pdf__facturation_commune_titre_de_recette();
        $pdf_edition = $inst_edition_pdf->compute_pdf__facturation_commune_titre_de_recette(array());
        $inst_edition_pdf->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
        return;
    }

    /**
     *
     */
    function view__export__facturation_publipostage() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_multi");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        //// Class chiffre en lettre
        require_once "../app/php/misc/chiffreenlettre.class.php";
        $lettre = new ChiffreEnLettre();

        //// Recupere l'entier d'un nombre decimal sans arrondir
        function decompose($somme)
        {
            $pos = array("","");
            $virgule = 0;
            for($cp=0;$cp<strlen($somme);$cp++)
            {
                $lettre = substr($somme,$cp,1);
                if($lettre=="."){ $virgule = 1; $lettre=""; }
                $pos[$virgule] .= $lettre;
            }
            return $pos;
        }

        //
        $periode = intval(substr($this->f->getParameter("datetableau"), 0, 4)) - 2;
        //
        $sqlcom = sprintf(
            'SELECT om_collectivite.om_collectivite as id, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'ville\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as ville, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'inseeville\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as inseeville, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'maire\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as maire, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'adresse\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as ad1, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'complement_adresse\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as ad2, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'civilite\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as civilite, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'maire_de\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as maire_de, (SELECT count(*) FROM %1$selecteur WHERE electeur.om_collectivite=om_collectivite.om_collectivite) AS total FROM %1$som_collectivite WHERE om_collectivite.niveau=\'1\' ORDER BY ville',
            DB_PREFIXE
        );
        $rescom = $this->f->db->query($sqlcom);
        $this->f->isDatabaseError($rescom);
        //
        $commune = array();
        while ($rowcom =& $rescom->fetchRow(DB_FETCHMODE_ASSOC)) {
            $commune[] = $rowcom;
        }
        // Création du fichier CSV
        $filecontent = "COMMUNE;ADRESSE;BP;INSEE COMMUNE;TITRE;MAIRE 2;MAIRE;TITRE DE CIVILITE;Periode;Electeurs;Px unitaire;Px total;Somme\n";
        $parametrage_facturation = $this->f->get_parametrage_facturation();
        foreach ($commune as $c) {
            //
            $sommeDue = $parametrage_facturation["fact_electeur_unitaire"] * $c['total'];
            if ($sommeDue < $parametrage_facturation["fact_forfait"]) {
                $sommeDue = $parametrage_facturation["fact_forfait"];
            }
            $sommeDue = number_format($sommeDue, 2, '.', '');
            //
            $lg_xls = $c['ville'].";";
            $lg_xls .= $c['ad1'].";";
            $lg_xls .= $c['ad2'].";";
            $lg_xls .= $c['inseeville'].";";
            $lg_xls .= $c['civilite'].";";
            $lg_xls .= $c['maire_de'].";";
            $lg_xls .= $c['maire'].";";
            $lg_xls .= $c['civilite']." le maire;";
            $lg_xls .= $periode.";";
            $lg_xls .= $c['total'].";";
            $lg_xls .= str_replace(".", ",", $parametrage_facturation["fact_electeur_unitaire"]).";";
            $lg_xls .= str_replace(".", ",", $sommeDue).";";
            $somme = decompose($sommeDue);
            $lettre->init();
            $lg_xls .= $lettre->Conversion($somme[0])."EURO";
            ($somme[1])? $lg_xls.=" ".$somme[1]." CENTIMES\n" : $lg_xls.=";\n";
            $filecontent .= utf8_encode($lg_xls);
        }

        //
        $filemetadata = array(
            "filename" => "factot_".$periode.".csv",
            "size" => strlen($filecontent),
            "mimetype" => "text/plain",
        );
        $uid = $this->f->store_file(
            $filecontent,
            $filemetadata,
            "export"
        );
        if ($uid === false) {
            $this->f->displayMessage(
                "error",
                __("Erreur lors du stockage/enregistrement du fichier.")
            );
            return;
        }

        //
        $msg = "Le fichier a été exporté, vous pouvez l'ouvrir immédiatement en cliquant sur : ";
        $msg .= "<a target=\"_blank\" href=\"".OM_ROUTE_FORM."&snippet=file&uid=".$uid."\" class=\"om-prev-icon reqmo-16\">".__("Télécharger le fichier Export")."</a>.<br />";
        $msg .= "Il est également accessible dans les traces, son nom est : \"".$filemetadata["filename"]."\".<br />";
        $this->f->displayMessage("valid", $msg);

    }

    /**
     *
     */
    function view__export__facturation_transfert_budget() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_multi");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        //
        $periode = intval(substr($this->f->getParameter("datetableau"), 0, 4)) - 1;
        $parametrage_facturation = $this->f->get_parametrage_facturation();
        //
        $sqlcom = sprintf(
            'SELECT om_collectivite.om_collectivite as id, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'inseeville\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as ville, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'inseeville\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as inseeville, (SELECT count(*) FROM %1$selecteur WHERE electeur.om_collectivite=om_collectivite.om_collectivite) as total FROM %1$som_collectivite WHERE om_collectivite.niveau=\'1\' ORDER BY ville',
            DB_PREFIXE
        );
        $rescom = $this->f->db->query($sqlcom);
        $this->f->isDatabaseError($rescom);
        //
        $commune = array();
        while ($rowcom =& $rescom->fetchRow(DB_FETCHMODE_ASSOC)) {
            $commune[] = $rowcom;
        }
        // Création du fichier CSV
        $filecontent = "";
        foreach ($commune as $c) {
            //
            $sommeDue = $parametrage_facturation["fact_electeur_unitaire"] * $c['total'];
            if ($sommeDue < $parametrage_facturation["fact_forfait"]) {
                $sommeDue = $parametrage_facturation["fact_forfait"];
            }
            //
            $lg_astre = $this->f->collectivite['ville'].";".$periode.";01;;;0;GF;C;FRAIS DE TENUE INFORMATIQUE; DU FICHIER ELECTORAL";
            $lg_astre .= ";;;LE00;".str_replace(".", ",", $sommeDue).";000000;;;I;D;S;;2455285;;;1;;;002237;01;70;70878;;;;;;;";
            $lg_astre .= " COMMUNE ".$c['ville']."; ".$c['ville'].";;;;; ".$c['ville'].";".$c['inseeville'];
            $lg_astre .= ";;;;;;;;;O;;;;;;;;;;;;;;;;;;;E;E;;;;;;;;\n";
            //
            $filecontent .= $lg_astre;
        }

        //
        $filemetadata = array(
            "filename" => "export_astre.txt",
            "size" => strlen($filecontent),
            "mimetype" => "text/plain",
        );
        $uid = $this->f->store_file(
            $filecontent,
            $filemetadata,
            "export"
        );
        if ($uid === false) {
            $this->f->displayMessage(
                "error",
                __("Erreur lors du stockage/enregistrement du fichier.")
            );
            return;
        }

        //
        $msg = "Le fichier a été exporté, vous pouvez l'ouvrir immédiatement en cliquant sur : ";
        $msg .= "<a target=\"_blank\" href=\"".OM_ROUTE_FORM."&snippet=file&uid=".$uid."\" class=\"om-prev-icon reqmo-16\">".__("Télécharger le fichier Export")."</a>.<br />";
        $msg .= "Il est également accessible dans les traces, son nom est : \"".$filemetadata["filename"]."\".<br />";
        $this->f->displayMessage("valid", $msg);
    }

    /**
     * VIEW - view__edition_pdf__statistiques_mouvements_par_commune.
     *
     * @return void
     */
    function view__edition_pdf__statistiques_mouvements_par_commune() {
        //
        require_once "../obj/edition_pdf__statistiques_mouvements_par_commune.class.php";
        $inst_edition_pdf = new edition_pdf__statistiques_mouvements_par_commune();
        $pdf_edition = $inst_edition_pdf->compute_pdf__statistiques_mouvements_par_commune(array());
        $inst_edition_pdf->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
        return;
    }

    /**
     * VIEW - view__edition_pdf__statistiques_saisie_par_utilisateur.
     *
     * @return void
     */
    function view__edition_pdf__statistiques_saisie_par_utilisateur() {
        //
        require_once "../obj/edition_pdf__statistiques_saisie_par_utilisateur.class.php";
        $inst_edition_pdf = new edition_pdf__statistiques_saisie_par_utilisateur();
        $pdf_edition = $inst_edition_pdf->compute_pdf__statistiques_saisie_par_utilisateur(array());
        $inst_edition_pdf->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
        return;
    }


    /**
     * VIEW - view__edition_pdf__statistiques_inscriptions.
     *
     * @return void
     */
    function view__edition_pdf__statistiques_inscriptions() {
        //
        require_once "../obj/edition_pdf__statistiques_inscriptions.class.php";
        $inst_edition_pdf = new edition_pdf__statistiques_inscriptions();
        $pdf_edition = $inst_edition_pdf->compute_pdf__statistiques_inscriptions(array());
        $inst_edition_pdf->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
        return;
    }

    /**
     *
     */
    protected function handle_pdf_output__carte_electorale($params) {
        //
        require_once "../obj/edition_pdf__carte_electorale.class.php";
        $inst_edition_pdf = new edition_pdf__carte_electorale();
        $pdf_edition = $inst_edition_pdf->compute_pdf__carte_electorale($params);
        //
        if ($params["mode"] == "save") {
            // Composition des métadonnées du fichier
            $filemetadata = array(
                "filename" => $pdf_edition["filename"],
                "size" => strlen($pdf_edition["pdf_output"]),
                "mimetype" => "application/pdf",
            );
            // Écriture du fichier
            $uid = $this->f->store_file(
                $pdf_edition["pdf_output"],
                $filemetadata,
                "pdf"
            );
            if ($uid === false) {
                $this->f->addToMessage(
                    "error",
                    __("Erreur lors du stockage/enregistrement du fichier.")
                );
                $this->f->setFlag(null);
                $this->f->display();
                return;
            }
            $pdf_edition["uid"] = $uid;
        }
        return $pdf_edition;
    }

    /**
     *
     */
    protected function handle_pdf_output__liste_electorale($params) {
        //
        require_once "../obj/edition_pdf__liste_electorale.class.php";
        $inst_edition_pdf = new edition_pdf__liste_electorale();
        $pdf_edition = $inst_edition_pdf->compute_pdf__liste_electorale($params);
        //
        if ($params["mode"] == "save") {
            // Composition des métadonnées du fichier
            $filemetadata = array(
                "filename" => $pdf_edition["filename"],
                "size" => strlen($pdf_edition["pdf_output"]),
                "mimetype" => "application/pdf",
            );
            // Écriture du fichier
            $uid = $this->f->store_file(
                $pdf_edition["pdf_output"],
                $filemetadata,
                "pdf"
            );
            if ($uid === false) {
                $this->f->addToMessage(
                    "error",
                    __("Erreur lors du stockage/enregistrement du fichier.")
                );
                $this->f->setFlag(null);
                $this->f->display();
                return;
            }
            $pdf_edition["uid"] = $uid;
        }
        return $pdf_edition;
    }

    /**
     *
     */
    protected function handle_pdf_output__liste_emargement($params) {
        //
        require_once "../obj/edition_pdf__liste_emargement.class.php";
        $inst_edition_pdf = new edition_pdf__liste_emargement();
        $pdf_edition = $inst_edition_pdf->compute_pdf__liste_emargement($params);
        //
        if ($params["mode"] == "save") {
            // Composition des métadonnées du fichier
            $filemetadata = array(
                "filename" => $pdf_edition["filename"],
                "size" => strlen($pdf_edition["pdf_output"]),
                "mimetype" => "application/pdf",
            );
            // Écriture du fichier
            $uid = $this->f->store_file(
                $pdf_edition["pdf_output"],
                $filemetadata,
                "pdf"
            );
            if ($uid === false) {
                $this->f->addToMessage(
                    "error",
                    __("Erreur lors du stockage/enregistrement du fichier.")
                );
                $this->f->setFlag(null);
                $this->f->display();
                return;
            }
            $pdf_edition["uid"] = $uid;
        }
        return $pdf_edition;
    }

    /**
     *
     */
    protected function handle_pdf_output__listing_mouvements_tableau_commission_multi($params) {
        //
        require_once "../obj/edition_pdf__listing_mouvements_tableau_commission_multi.class.php";
        $inst_edition_pdf = new edition_pdf__listing_mouvements_tableau_commission_multi();
        $pdf_edition = $inst_edition_pdf->compute_pdf__listing_mouvements_tableau_commission_multi($params);
        //
        if ($params["mode"] == "save") {
            // Composition des métadonnées du fichier
            $filemetadata = array(
                "filename" => $pdf_edition["filename"],
                "size" => strlen($pdf_edition["pdf_output"]),
                "mimetype" => "application/pdf",
            );
            // Écriture du fichier
            $uid = $this->f->store_file(
                $pdf_edition["pdf_output"],
                $filemetadata,
                "pdf"
            );
            if ($uid === false) {
                $this->f->addToMessage(
                    "error",
                    __("Erreur lors du stockage/enregistrement du fichier.")
                );
                $this->f->setFlag(null);
                $this->f->display();
                return;
            }
            $pdf_edition["uid"] = $uid;
        }
        return $pdf_edition;
    }

    /**
     *
     */
    protected function handle_pdf_output__listing_mouvements_tableau_commission($params) {
        //
        require_once "../obj/edition_pdf__listing_mouvements_tableau_commission.class.php";
        $inst_edition_pdf = new edition_pdf__listing_mouvements_tableau_commission();
        $pdf_edition = $inst_edition_pdf->compute_pdf__listing_mouvements_tableau_commission($params);
        //
        if ($params["mode"] == "save") {
            // Composition des métadonnées du fichier
            $filemetadata = array(
                "filename" => $pdf_edition["filename"],
                "size" => strlen($pdf_edition["pdf_output"]),
                "mimetype" => "application/pdf",
            );
            // Écriture du fichier
            $uid = $this->f->store_file(
                $pdf_edition["pdf_output"],
                $filemetadata,
                "pdf"
            );
            if ($uid === false) {
                $this->f->addToMessage(
                    "error",
                    __("Erreur lors du stockage/enregistrement du fichier.")
                );
                $this->f->setFlag(null);
                $this->f->display();
                return;
            }
            $pdf_edition["uid"] = $uid;
        }
        return $pdf_edition;
    }

    /**
     *
     */
    protected function handle_pdf_output__etiquette($params) {
        //
        require_once "../obj/edition_pdf__etiquette.class.php";
        $inst_edition_pdf = new edition_pdf__etiquette();
        $pdf_edition = $inst_edition_pdf->compute_pdf__etiquette($params);
        //
        if ($params["mode"] == "save") {
            // Composition des métadonnées du fichier
            $filemetadata = array(
                "filename" => $pdf_edition["filename"],
                "size" => strlen($pdf_edition["pdf_output"]),
                "mimetype" => "application/pdf",
            );
            // Écriture du fichier
            $uid = $this->f->store_file(
                $pdf_edition["pdf_output"],
                $filemetadata,
                "pdf"
            );
            if ($uid === false) {
                $this->f->addToMessage(
                    "error",
                    __("Erreur lors du stockage/enregistrement du fichier.")
                );
                $this->f->setFlag(null);
                $this->f->display();
                return;
            }
            $pdf_edition["uid"] = $uid;
        }
        return $pdf_edition;
    }

}
