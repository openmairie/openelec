<?php
/**
 * Ce script définit la classe 'edition_pdf'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_base.class.php";

/**
 * Définition de la classe 'edition_pdf' (om_base).
 */
class edition_pdf extends om_base {

    /**
     *
     */
    public function __construct() {
        $this->init_om_application();
    }

    /**
     * Expose le fichier PDF à l'utilisateur.
     *
     * @param string $pdf_output PDF sous forme de chaîne de caractères.
     * @param string $filename Nom du fichier.
     *
     * @return void
     */
    public function expose_pdf_output($pdf_output, $filename) {
        //
        $om_edition = $this->f->get_inst__om_edition();
        $om_edition->expose_pdf_output($pdf_output, $filename);
    }

    /**
     *
     */
    protected function get_vars_from_config_file_pdffromdb($obj, $params = array()) {
        $multiplicateur = 1;
        foreach ($params as $key => $value) {
            ${$key} = $value;
        }
        unset($key);
        unset($value);
        $f = $this->f;
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$obj.".pdffromdb.inc.php")) {
            require "../sql/".OM_DB_PHPTYPE."/".$obj.".pdffromdb.inc.php";
        }
        unset($f);
        foreach ($params as $key => $value) {
            unset(${$key});
        }
        unset($params);
        unset($obj);
        $defined_vars = get_defined_vars();
        unset($defined_vars["this"]);
        return $defined_vars;
    }

    /**
     *
     */
    public function compute_pdf__pdffromdb($params) {
        /**
         *
         */
        // Obligatoire - objet du .pdf.inc.php à récupérer.
        $obj = "";
        if (isset($params["obj"])) {
            $obj = $params["obj"];
        }
        //
        $nolibliste = strtolower($_SESSION["libelle_liste"]);
        //
        if (array_key_exists("datetableau", $params) == true) {
            $datetableau = $params["datetableau"];
        } else {
            $datetableau = $this->f->getParameter("datetableau");
        }
        $datetableau_formatted_for_display = $this->f->formatDate($datetableau, true);
        $datetableau_formatted_for_database = $this->f->formatDate($datetableau);
        //
        $ville = $this->f->collectivite['ville'];

        /**
         *
         */
        //
        require_once "../obj/fpdf_fromdb_fromarray.class.php";
        $params = array_merge(
            $params,
            array(
                //"f" => $this->f,
                "obj" => $obj,
                "nolibliste" => $nolibliste,
                "datetableau" => $datetableau,
                "datetableau_formatted_for_database" => $datetableau_formatted_for_database,
                "datetableau_formatted_for_display" => $datetableau_formatted_for_display,
                "ville" => $ville,
            )
        );
        $conf = $this->get_vars_from_config_file_pdffromdb($obj, $params);
        //
        if (!isset($pdf)) {
            $pdf = new PDF(
                $conf["orientation"],
                'mm',
                $conf["format"]
            );
            $pdf->SetMargins(
                $conf["margeleft"],
                $conf["margetop"],
                $conf["margeright"]
            );
            $pdf->AliasNbPages();
            $pdf->SetDisplayMode(
                'real',
                'single'
            );
            $pdf->SetDrawColor(
                $conf["C1border"],
                $conf["C2border"],
                $conf["C3border"]
            );
        }
        $pdf->set_global_conf($conf);
        $pdf->init_pdffromdb(
            $conf["sql"],
            $this->f->db,
            $conf["height"],
            $conf["border"],
            $conf["align"],
            $conf["fond"],
            $conf["police"],
            $conf["size"],
            $conf["multiplicateur"],
            $conf["flag_entete"]
        );
        $pdf->init_header_pdffromdb();
        $pdf->AddPage();
        $pdf->Table(
            $conf["sql"],
            $this->f->db,
            $conf["height"],
            $conf["border"],
            $conf["align"],
            $conf["fond"],
            $conf["police"],
            $conf["size"],
            $conf["multiplicateur"],
            $conf["flag_entete"]
        );

        /**
         * OUTPUT
         */
        //
        $filename = date("Ymd-His");
        $filename .= "-".$obj;
        $filename .= "-".$_SESSION["collectivite"];
        $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
        $filename .= "-liste".$_SESSION["liste"];
        $filename .= ".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }

    /**
     *
     */
    public function compute_pdf__pdffromarray($params) {
        $tableau = $params["tableau"];
        //
        require_once "../obj/fpdf_fromdb_fromarray.class.php";
        $pdf = new PDF($tableau['format'], 'mm', 'A4');
        $pdf->generic_document_header_left = $this->f->collectivite["ville"];
        $pdf->init_header_pdffromarray();
        $pdf->tableau($tableau);

        /**
         * OUTPUT
         */
        $aujourdhui = date("Ymd-His");
        if (array_key_exists("output", $tableau) === true) {
            $filename = $tableau['output']."-".$aujourdhui.".pdf";
        } else {
            $filename = "stats-".$aujourdhui.".pdf";
        }
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }

    /**
     *
     */
    function get_page_header_config($params) {
        return array(
            array(
                gavoe($params, array("titre_libelle_ligne1")),
                gavoe($params, array("page_content_width"), 0)/2,
                gavoe($params, array("page_header_line_height"), 0),
                0,
                (gavoe($params, array("allborders")) == true ? 1 : 0),
                'L','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
            array(
                "<PAGE>",
                gavoe($params, array("page_content_width"), 0)/2,
                gavoe($params, array("page_header_line_height"), 0),
                1,
                (gavoe($params, array("allborders")) == true ? 1 : 0),
                'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                gavoe($params, array("titre_libelle_ligne2")),
                gavoe($params, array("page_content_width"), 0)/2,
                gavoe($params, array("page_header_line_height"), 0),
                0,
                (gavoe($params, array("allborders")) == true ? 1 : 0),
                'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                "Édition du ".date("d/m/Y H:i:s"),
                gavoe($params, array("page_content_width"), 0)/2,
                gavoe($params, array("page_header_line_height"), 0),
                1,
                (gavoe($params, array("allborders")) == true ? 1 : 0),
                'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                gavoe($params, array("libelle_liste")),
                gavoe($params, array("page_content_width"), 0),
                gavoe($params, array("page_header_line_height"), 0),
                1,
                (gavoe($params, array("allborders")) == true ? 1 : 0),
                'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                sprintf(__("Commune : %s"), gavoe($params, array("libelle_commune"))),
                gavoe($params, array("page_content_width"), 0)/2,
                gavoe($params, array("page_header_line_height"), 0),
                0,
                (gavoe($params, array("allborders")) == true ? 1 : 0),
                'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                (gavoe($params, array("canton_libelle")) != "" ? sprintf(__("Canton : %s"), gavoe($params, array("canton_libelle"))) : ""),
                gavoe($params, array("page_content_width"), 0)/2,
                gavoe($params, array("page_header_line_height"), 0),
                1,
                (gavoe($params, array("allborders")) == true ? 1 : 0),
                'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                (gavoe($params, array("bureau_code")) != "ALL" ? sprintf(__("Bureau : %s - %s"), gavoe($params, array("bureau_code")), gavoe($params, array("bureau_libelle"))) : ""),
                gavoe($params, array("page_content_width"), 0)/2,
                gavoe($params, array("page_header_line_height"), 0),
                0,
                (gavoe($params, array("allborders")) == true ? 1 : 0),
                'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                (gavoe($params, array("circonscription_libelle")) != "" ? sprintf(__("Circonscription : %s"), gavoe($params, array("circonscription_libelle"))) : ""),
                gavoe($params, array("page_content_width"), 0)/2,
                gavoe($params, array("page_header_line_height"), 0),
                1,
                (gavoe($params, array("allborders")) == true ? 1 : 0),
                'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        );
    }
}
