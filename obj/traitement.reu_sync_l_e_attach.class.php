<?php
/**
 * Ce script définit la classe 'juryEpurationTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_l_e.class.php";

/**
 * Définition de la classe 'reuSyncLEAttachTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLEAttachTraitement extends reuSyncLETraitement {

    var $fichier = "reu_sync_l_e_attach";
    var $form_class = "hide_button_on_success";
    function getValidButtonValue() {
        //
        return __("Rattacher automatiquement les électeurs");
    }

    function is_done() {
        if ($this->f->getParameter("reu_sync_l_e_attach") == "done") {
            return true;
        }
        return false;
    }

    function get_conditions($type = null) {
        /**
         * CONDITIONS
         */
        // NOM
        $nom_oe_clean = '
replace(withoutaccent(
    lower(
        trim(electeur.nom)
    )
),\'-\', \' \')
        ';
        $nom_reu_clean = '
replace(withoutaccent(
    lower(
        trim(reu_sync_listes.nom)
    )
),\'-\', \' \')
        ';
        $condition__nom__exact = sprintf(
            '%1$s=%2$s',
            $nom_oe_clean,
            $nom_reu_clean
        );
        $condition__nom__oe_in_reu_debut_exact = sprintf(
            '%1$s=left(%2$s, length(%1$s))',
            $nom_oe_clean,
            $nom_reu_clean
        );
        $condition__nom__reu_in_oe_debut_exact = sprintf(
            'left(%1$s, length(%2$s))=%2$s',
            $nom_oe_clean,
            $nom_reu_clean
        );
        $condition__nom__oe_in_reu = sprintf(
            'position(%1$s IN %2$s) > 0',
            $nom_oe_clean,
            $nom_reu_clean
        );
        $condition__nom__reu_in_oe = sprintf(
            'position(%2$s IN %1$s) > 0',
            $nom_oe_clean,
            $nom_reu_clean
        );
        $condition__nom__oe_in_reu_premier_nom = sprintf(
            '(position(\' \' IN %1$s) > 0 AND position(substr(%1$s, 1, position(\' \' IN %1$s)) IN %2$s) > 0)',
            $nom_oe_clean,
            $nom_reu_clean
        );
        $condition__nom__reu_in_oe_premier_nom = sprintf(
            '(position(\' \' IN %2$s) > 0 AND position(substr(%2$s, 1, position(\' \' IN %2$s)) IN %1$s) > 0)',
            $nom_oe_clean,
            $nom_reu_clean
        );
        $condition__nom__x_premiers_caracteres =  sprintf(
            'left(%1$s, %%1$s)=left(%2$s, %%1$s)',
            $nom_oe_clean,
            $nom_reu_clean
        );
        $condition__nom__3_premiers_caracteres = sprintf(
            $condition__nom__x_premiers_caracteres,
            3
        );
        $condition__nom__2_premiers_caracteres = sprintf(
            $condition__nom__x_premiers_caracteres,
            2
        );
        $condition__nom__1_premiers_caracteres = sprintf(
            $condition__nom__x_premiers_caracteres,
            1
        );
        $condition__nom__approx_n1 = sprintf(
            '(%s OR %s OR %s OR %s OR %s OR %s)',
            $condition__nom__oe_in_reu_debut_exact,
            $condition__nom__reu_in_oe_debut_exact,
            $condition__nom__oe_in_reu,
            $condition__nom__reu_in_oe,
            $condition__nom__oe_in_reu_premier_nom,
            $condition__nom__reu_in_oe_premier_nom
        );
        // PRENOM
        $prenom_oe_clean = 'replace(withoutaccent(
    lower(
        trim(electeur.prenom)
    )
), \'-\', \' \')';
        $prenom_reu_clean = 'replace(withoutaccent(
    lower(
        trim(reu_sync_listes.prenoms)
    )
), \'-\', \' \')';
        $condition__prenom__exact = sprintf(
            '%1$s=%2$s',
            $prenom_oe_clean,
            $prenom_reu_clean
        );
        $condition__prenom__oe_in_reu_debut_exact = sprintf(
            '%1$s=left(%2$s, length(%1$s))',
            $prenom_oe_clean,
            $prenom_reu_clean
        );
        $condition__prenom__oe_in_reu = sprintf(
            'position(%1$s IN %2$s) > 0',
            $prenom_oe_clean,
            $prenom_reu_clean
        );
        $condition__prenom__reu_in_oe = sprintf(
            'position(%2$s IN %1$s) > 0',
            $prenom_oe_clean,
            $prenom_reu_clean
        );
        $condition__prenom__x_premiers_caracteres = sprintf(
            'left(%1$s, %%1$s)=left(%2$s, %%1$s)',
            $prenom_oe_clean,
            $prenom_reu_clean
        );
        $condition__prenom__3_premiers_caracteres = sprintf(
            $condition__prenom__x_premiers_caracteres,
            3
        );
        $condition__prenom__2_premiers_caracteres = sprintf(
            $condition__prenom__x_premiers_caracteres,
            2
        );
        $condition__prenom__1_premiers_caracteres = sprintf(
            $condition__prenom__x_premiers_caracteres,
            1
        );
        $condition__prenom__approx_n1 = sprintf(
            '(%s OR %s OR %s)',
            $condition__prenom__oe_in_reu_debut_exact,
            $condition__prenom__oe_in_reu,
            $condition__prenom__reu_in_oe
        );
        // DATENAISSANCE
        $condition__datenaissance__exact = '
electeur.date_naissance=to_date(reu_sync_listes.date_de_naissance, \'DD/MM/YYYY\')
';

        $condition__datenaissance__m_ma__m_jm__m_ja = '
(
(
    extract(\'month\' from electeur.date_naissance) = extract(\'month\' from to_date(reu_sync_listes.date_de_naissance, \'DD/MM/YYYY\'))
    AND
    extract(\'year\' from electeur.date_naissance) = extract(\'year\' from to_date(reu_sync_listes.date_de_naissance, \'DD/MM/YYYY\'))
)
OR 
(
    extract(\'month\' from electeur.date_naissance) = extract(\'month\' from to_date(reu_sync_listes.date_de_naissance, \'DD/MM/YYYY\'))
    AND
    extract(\'day\' from electeur.date_naissance) = extract(\'day\' from to_date(reu_sync_listes.date_de_naissance, \'DD/MM/YYYY\'))
)
OR 
(
    extract(\'year\' from electeur.date_naissance) = extract(\'year\' from to_date(reu_sync_listes.date_de_naissance, \'DD/MM/YYYY\'))
    AND
    extract(\'day\' from electeur.date_naissance) = extract(\'day\' from to_date(reu_sync_listes.date_de_naissance, \'DD/MM/YYYY\'))
)
)
';
        $condition__datenaissance__m_a = '
(
extract(\'year\' from electeur.date_naissance) = extract(\'year\' from to_date(reu_sync_listes.date_de_naissance, \'DD/MM/YYYY\'))
)
';
        //
        $conditions = array(
            1 => array(
                "motif" => "NOM EXACT / PRENOM EXACT / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__exact,
                    $condition__datenaissance__exact
                ),
            ),
            2 => array(
                "motif" => "RATTACHEMENT MANUEL",
            ),
            10 => array(
                "motif" => "NOM EXACT / PRENOM APPROX N1 / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__approx_n1,
                    $condition__datenaissance__exact
                ),
            ),
            11 => array(
                "motif" => "NOM EXACT / PRENOM APPROX N2 / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__3_premiers_caracteres,
                    $condition__datenaissance__exact
                ),
            ),
            20 => array(
                "motif" => "NOM APPROX N1 / PRENOM EXACT / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__approx_n1,
                    $condition__prenom__exact,
                    $condition__datenaissance__exact
                ),
            ),
            31 => array(
                "motif" => "NOM EXACT / PRENOM EXACT / DATENAISSANCE APPROX N1",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__exact,
                    $condition__datenaissance__m_ma__m_jm__m_ja
                ),
            ),
            32 => array(
                "motif" => "NOM EXACT / PRENOM APPROX N1 / DATENAISSANCE APPROX N1",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__approx_n1,
                    $condition__datenaissance__m_ma__m_jm__m_ja
                ),
            ),
            33 => array(
                "motif" => "NOM APPROX N1 / PRENOM EXACT / DATENAISSANCE APPROX N1",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__approx_n1,
                    $condition__prenom__exact,
                    $condition__datenaissance__m_ma__m_jm__m_ja
                ),
            ),
            34 => array(
                "motif" => "NOM EXACT / PRENOM APPROX N2 / DATENAISSANCE APPROX N1",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__3_premiers_caracteres,
                    $condition__datenaissance__m_ma__m_jm__m_ja
                ),
            ),

            41 => array(
                "motif" => "NOM EXACT / PRENOM EXACT / DATENAISSANCE APPROX N2",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__exact,
                    $condition__datenaissance__m_a
                ),
            ),
            42 => array(
                "motif" => "NOM EXACT / PRENOM APPROX N1 / DATENAISSANCE APPROX N2",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__approx_n1,
                    $condition__datenaissance__m_a
                ),
            ),
            43 => array(
                "motif" => "NOM APPROX N1 / PRENOM EXACT / DATENAISSANCE APPROX N2",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__approx_n1,
                    $condition__prenom__exact,
                    $condition__datenaissance__m_a
                ),
            ),
            44 => array(
                "motif" => "NOM EXACT / PRENOM APPROX N2 / DATENAISSANCE APPROX N2",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__3_premiers_caracteres,
                    $condition__datenaissance__m_a
                ),
            ),
            51 => array(
                "motif" => "NOM APPROX N1 / PRENOM APPROX N1 / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__approx_n1,
                    $condition__prenom__approx_n1,
                    $condition__datenaissance__exact
                ),
            ),
            52 => array(
                "motif" => "NOM EXACT / PRENOM INDIFFERENT / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '%s AND %s',
                    $condition__nom__exact,
                    $condition__datenaissance__exact
                ),
            ),
            53 => array(
                "motif" => "NOM APPROX N1 / PRENOM INDIFFERENT / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '%s AND %s',
                    $condition__nom__approx_n1,
                    $condition__datenaissance__exact
                ),
            ),
            400 => array(
                "motif" => "NOM EXACT / PRENOM APPROX N2 / DATENAISSANCE APPROX N1",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__exact,
                    $condition__prenom__1_premiers_caracteres,
                    $condition__datenaissance__m_ma__m_jm__m_ja
                ),
            ),
            410 => array(
                "motif" => "NOM APPROX N2 / PRENOM APPROX N2 / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__3_premiers_caracteres,
                    $condition__prenom__3_premiers_caracteres,
                    $condition__datenaissance__exact
                ),
            ),
            411 => array(
                "motif" => "NOM APPROX N2 OU PRENOM APPROX N2 / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '(%s OR %s) AND %s',
                    $condition__nom__3_premiers_caracteres,
                    $condition__prenom__3_premiers_caracteres,
                    $condition__datenaissance__exact
                ),
            ),
            420 => array(
                "motif" => "NOM APPROX N2 / PRENOM APPROX N2 / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__2_premiers_caracteres,
                    $condition__prenom__2_premiers_caracteres,
                    $condition__datenaissance__exact
                ),
            ),
            421 => array(
                "motif" => "NOM APPROX N2 OR PRENOM APPROX N2 / DATENAISSANCE EXACT",
                "sql" => sprintf(
                    '(%s OR %s) AND %s',
                    $condition__nom__2_premiers_caracteres,
                    $condition__prenom__2_premiers_caracteres,
                    $condition__datenaissance__exact
                ),
            ),
            440 => array(
                "motif" => "NOM APPROX N1 / PRENOM APPROX N1 / DATENAISSANCE APPROX N1",
                "sql" => sprintf(
                    '%s AND %s AND %s',
                    $condition__nom__approx_n1,
                    $condition__prenom__approx_n1,
                    $condition__datenaissance__m_ma__m_jm__m_ja
                ),
            ),
        );
        //
        if ($type === "labels"
            || $type === "labels_exact"
            || $type === "labels_approx") {
            $conditions_labels = array();
            foreach ($conditions as $key => $value) {
                if (($type === "labels_exact" && $key < 30)
                    || ($type === "labels_approx" && $key >= 30)
                    || $type === "labels") {
                    //
                    $conditions_labels[] = $key." - ".$value["motif"];
                }
            }
            return $conditions_labels;
        }
        return $conditions;
    }

    function treatment() {
        //
        $this->LogToFile("start reu_sync_l_e_attach");
        //
        $inst_reu = $this->f->get_inst__reu();
        //
        $template_sql = '
UPDATE %1$selecteur SET 
    ine=(
    SELECT
        numero_d_electeur::integer
    FROM 
        %1$sreu_sync_listes
    WHERE
        %5$s
        AND code_type_liste=\'%2$s\'
        AND reu_sync_listes.numero_d_electeur::integer NOT IN (
            SELECT ine FROM %1$selecteur WHERE ine IS NOT NULL AND liste=\'%3$s\' AND om_collectivite=%7$s
        )
        AND code_ugle=\'%6$s\'
    ),
    reu_sync_info = \'%4$s\'
WHERE 
    ine IS NULL 
    AND liste=\'%3$s\'
    AND om_collectivite=%7$s
    AND (
        SELECT
            count(numero_d_electeur::integer)
        FROM 
            %1$sreu_sync_listes
        WHERE
            %5$s
            AND code_type_liste=\'%2$s\'
            AND reu_sync_listes.numero_d_electeur::integer NOT IN (
                SELECT ine FROM %1$selecteur WHERE ine IS NOT NULL AND liste=\'%3$s\' AND om_collectivite=%7$s
            )
            AND code_ugle=\'%6$s\'
    ) = 1
;
';

        //
        foreach ($this->listes as $liste_key => $liste_value) {
            $this->LogToFile($liste_value["reu"]." > begin");
            foreach ($this->get_conditions() as $condition_key => $condition_value) {
                if (!isset($condition_value["sql"])) {
                    // Le type de consition est manuel, on ne fait pas de traitement
                    // automatique
                    continue;
                }
                //
                $sql = sprintf(
                    $template_sql,
                    DB_PREFIXE,
                    $liste_value["reu"],
                    $liste_value["oe"],
                    $condition_key ." - ".$condition_value["motif"],
                    $condition_value["sql"],
                    $inst_reu->get_ugle(),
                    intval($_SESSION["collectivite"])
                );
                $res = $this->f->db->query($sql);
                $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
                if ($this->f->isDatabaseError($res, true) === true) {
                    $this->error = true;
                    $this->LogToFile($liste_value["reu"]." > ".$condition_key." : DB ERROR");
                    $this->LogToFile($liste_value["reu"]." > end");
                    return;
                }
                $this->LogToFile($liste_value["reu"]." > ".$condition_key." : ".$this->f->db->affectedRows());
            }
            $this->LogToFile($liste_value["reu"]." > end");
        }

        /**
         * Mise à jour du flag
         * Le traitement automatique a été fait
         */
        $ret = $this->f->insert_or_update_parameter("reu_sync_l_e_attach", "done");
        if ($ret !== true) {
            $this->error = true;
            $this->LogToFile("error lors de la création/mise à jour du flag");
            $this->LogToFile("end reu_sync_l_e_attach");
            return;
        }
        $this->LogToFile("création/mise à jour du flag ok");
        $this->addToMessage(sprintf(
            __("Cliquez ici pour accéder à l'étape suivante : %s"),
            '<a onclick="reload_my_tab(\'#traitement-tabs\');return false;" href="#">ETAPE SUIVANTE</a>'
        ));
        //
        $this->LogToFile("end reu_sync_l_e_attach");
    }


    function displayBeforeContentForm() {
        if ($this->is_done() !== true) {
            return false;
        }
        $inst_reu = $this->f->get_inst__reu();
        //
        $electeurs_oel_rattaches = $this->f->db->getone(
            sprintf(
                'select count(*) from %1$selecteur WHERE om_collectivite=%2$s AND ine is not null',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        $this->f->isDatabaseError($electeurs_oel_rattaches);
        $electeurs_oel_rattaches_exact = $this->f->db->getone(
            sprintf(
                'select count(*) from %1$selecteur WHERE om_collectivite=%2$s AND ine is not null
                AND (
                    electeur.reu_sync_info LIKE \'1%%\'
                    OR electeur.reu_sync_info LIKE \'2%%\'
                )',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        $this->f->isDatabaseError($electeurs_oel_rattaches_exact);
        $electeurs_oel_rattaches_approx = $this->f->db->getone(
            sprintf(
                'select count(*) from %1$selecteur WHERE om_collectivite=%2$s AND ine is not null
                AND (
                    electeur.reu_sync_info LIKE \'3%%\'
                    OR electeur.reu_sync_info LIKE \'4%%\'
                    OR electeur.reu_sync_info LIKE \'5%%\'
                )',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        $this->f->isDatabaseError($electeurs_oel_rattaches_approx);
        //
        printf(
            '
            <div class="container-fluid" id="reu-sync-tab">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card text-center">
                            <div class="card-header">
                                Électeurs openElec rattachés
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="card text-center text-white bg-success">
                                        <div class="card-body">
                                            <div class="card-title">%s</div>
                                            <div>rattachés</div>
                                        </div>
                                    </div>
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%s</div>
                                            <div>~exact</div>
                                            <a id="exact" href="../app/index.php?module=tab&obj=reu_sync_l_e_oel_rattaches_exact">Accéder au listing</a> 
                                        </div>
                                    </div>
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%s</div>
                                            <div>~approx (à vérifier)</div>
                                            <a id="approx" href="../app/index.php?module=tab&obj=reu_sync_l_e_oel_rattaches_approx">Accéder au listing</a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-center">
                            <div class="card-header">
                                Électeurs REU non rattachés
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%s</div>
                                                <a href="../app/index.php?module=tab&obj=reu_sync_l_e_reu_non_rattaches">Accéder au listing</a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-center">
                            <div class="card-header">
                                Électeurs openElec non rattachés
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%s</div>
                                            <a href="../app/index.php?module=tab&obj=reu_sync_l_e_oel_non_rattaches">Accéder au listing</a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="visualClear"></div>
            ',
            $electeurs_oel_rattaches,
            $electeurs_oel_rattaches_exact,
            $electeurs_oel_rattaches_approx,
            $this->get_nb_electeurs_reu_total()-intval($electeurs_oel_rattaches),
            $this->get_nb_electeurs_oel_non_rattaches()
        );
        //
        $detail_par_liste = array();
        foreach ($this->listes as $key => $value) {
            //
            $electeurs_oel_total = $this->f->db->getone(
                sprintf(
                    'select count(*) from %1$selecteur where om_collectivite=%2$s AND liste=\'%3$s\'',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $value["oe"]
                )
            );
            $this->f->isDatabaseError($electeurs_oel_total);
            $electeurs_reu_total = $this->f->db->getone(
                sprintf(
                    'select count(*) from %1$sreu_sync_listes where code_ugle=\'%2$s\' AND code_type_liste=\'%3$s\'',
                    DB_PREFIXE,
                    $inst_reu->get_ugle(),
                    $value["reu"]
                )
            );
            $this->f->isDatabaseError($electeurs_reu_total);
            $electeurs_oel_rattaches = $this->f->db->getone(
                sprintf(
                    'select count(*) from %1$selecteur where om_collectivite=%2$s AND liste=\'%3$s\' and ine is not null',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $value["oe"]
                )
            );
            $this->f->isDatabaseError($electeurs_oel_rattaches);
            $electeurs_oel_rattaches_exact = $this->f->db->getone(
                sprintf(
                    'select count(*) from %1$selecteur WHERE om_collectivite=%2$s AND liste=\'%3$s\' AND ine is not null
                    AND (
                        electeur.reu_sync_info LIKE \'1%%\'
                        OR electeur.reu_sync_info LIKE \'2%%\'
                    )',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $value["oe"]
                )
            );
            $this->f->isDatabaseError($electeurs_oel_rattaches_exact);
            $electeurs_oel_rattaches_approx = $this->f->db->getone(
                sprintf(
                    'select count(*) from %1$selecteur WHERE om_collectivite=%2$s AND liste=\'%3$s\' AND ine is not null
                    AND (
                        electeur.reu_sync_info LIKE \'3%%\'
                        OR electeur.reu_sync_info LIKE \'4%%\'
                        OR electeur.reu_sync_info LIKE \'5%%\'
                    )',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $value["oe"]
                )
            );
            $this->f->isDatabaseError($electeurs_oel_rattaches_approx);
            $electeurs_oel_non_rattaches = $this->f->db->getone(
                sprintf(
                    'select count(*) from %1$selecteur where om_collectivite=%2$s AND liste=\'%3$s\' and ine is null',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $value["oe"]
                )
            );
            $this->f->isDatabaseError($electeurs_oel_non_rattaches);
            //
            $detail_par_liste[$value["reu"]] = array(
                "electeurs_oel_total" => $electeurs_oel_total,
                "electeurs_reu_total" => $electeurs_reu_total,
                "electeurs_reu_non_rattaches" => intval($electeurs_reu_total)-intval($electeurs_oel_rattaches),
                "electeurs_oel_rattaches_exact" => $electeurs_oel_rattaches_exact,
                "electeurs_oel_rattaches_approx" => $electeurs_oel_rattaches_approx,
                "electeurs_oel_non_rattaches" => $electeurs_oel_non_rattaches,
            );
        }
        //
        printf('<table class="table table-sm table-striped">');
        $lines = array(
            array(
                "id" => "electeurs_oel_total",
                "title" => __("Électeurs openElec total"),
            ),
            array(
                "id" => "electeurs_reu_total",
                "title" => __("Électeurs REU total"),
            ),
            array(
                "id" => "electeurs_reu_non_rattaches",
                "title" => __("Électeurs REU non rattachés"),
            ),
            array(
                "id" => "electeurs_oel_rattaches_exact",
                "title" => __("Électeurs openElec rattachés exact"),
            ),
            array(
                "id" => "electeurs_oel_rattaches_approx",
                "title" => __("Électeurs openElec rattachés approx"),
            ),
            array(
                "id" => "electeurs_oel_non_rattaches",
                "title" => __("Électeurs openElec non rattachés"),
            ),
        );
        printf('<tr><th></th><th>LP</th><th>LCE</th><th>LCM</th></tr>');
        foreach ($lines as $line) {
            printf(
                '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                $line["title"],
                $detail_par_liste["LP"][$line["id"]],
                $detail_par_liste["LCE"][$line["id"]],
                $detail_par_liste["LCM"][$line["id"]]
            );
        }
        printf('</table>');
    }
}
