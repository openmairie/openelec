<?php
/**
 * Ce script définit la classe 'reuSyncLECheckTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_l_e.class.php";

/**
 * Définition de la classe 'reuSyncLECheckTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLECheckTraitement extends reuSyncLETraitement {
    /**
     * @var string
     */
    var $fichier = "reu_sync_l_e_check";

    /**
     * @return void
     */
    function displayBeforeContentForm() {

    }

    /**
     * @return void
     */
    function treatment () {
        //
        $this->LogToFile("start ".$this->fichier);
        //
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done") {
            $this->error = true;
            $message = __("Cet écran n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.");
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        $reu_sync_check_status = array(
            "check_date" => date("d/m/Y H:i:s"),
        );
        //
        $this->LogToFile("-- Récupération des listes électorales");
        $date_extraction = $this->init_reu_sync_listes();
        $reu_sync_check_status["date_extraction"] = $date_extraction;
        $date_extraction_procurations = $this->init_reu_sync_procurations();
        $reu_sync_check_status["date_extraction_procurations"] = $date_extraction_procurations;
        // On commite la récupération des listes
        $this->commitTransaction();
        $nb_electeurs_oel_total = $this->get_nb_electeurs_oel_total();
        $reu_sync_check_status["nb_electeurs_oel_total"] = $nb_electeurs_oel_total;
        $nb_electeurs_reu_total = $this->get_nb_electeurs_reu_total();
        $reu_sync_check_status["nb_electeurs_reu_total"] = $nb_electeurs_reu_total;
        //
        $nb_procurations_oel_total = $this->get_nb_procurations_oel_total();
        $reu_sync_check_status["nb_procurations_oel_total"] = $nb_procurations_oel_total;
        $nb_procurations_reu_total = $this->get_nb_procurations_reu_total();
        $reu_sync_check_status["nb_procurations_reu_total"] = $nb_procurations_reu_total;
        //
        $this->LogToFile("Date d'extraction : ".$date_extraction);
        $this->addToMessage("Date d'extraction : ".$date_extraction);
        $this->LogToFile("Électeurs openElec total : ".$nb_electeurs_oel_total);
        $this->LogToFile("Électeurs REU total : ".$nb_electeurs_reu_total);
        $this->LogToFile("Date d'extraction procurations: ".$date_extraction_procurations);
        $this->addToMessage("Date d'extraction procurations : ".$date_extraction_procurations);
        $this->LogToFile("Procurations openElec total : ".$nb_procurations_oel_total);
        $this->LogToFile("Procurations REU total : ".$nb_procurations_reu_total);
        //
        printf(
            '
            <div class="container-fluid" id="reu-sync-tab">
                <div class="row">
                    <div class="col-sm-3 ">
                    </div>
                    <div class="col-sm-3 ">
                        <div class="card text-center text-white bg-info">
                            <div class="card-header">
                                Électeurs openElec total
                            </div>
                            <div class="card-body">
                                <div id="nb_electeur_oel" class="card-title">%s</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-center text-white bg-success">
                            <div class="card-header">
                                Électeurs REU total
                            </div>
                            <div class="card-body">
                                <div id="nb_electeur_reu" class="card-title">%s</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 ">
                </div>
            </div>
            <div class="visualClear"></div>
            <h2 class="text-center">%s</h2>
            ',
            $nb_electeurs_oel_total,
            $nb_electeurs_reu_total,
            $date_extraction
        );
        $this->LogToFile("-- Liste des électeurs openElec absents du REU");
        $this->f->displaySubtitle(
            __("Liste des électeurs openElec absents du REU")
        );
        $get_electeurs_oel_absents_in_reu = $this->get_electeurs_oel_absents_in_reu();
        $nb_electeurs_oel_absents_in_reu = count($get_electeurs_oel_absents_in_reu);
        $reu_sync_check_status["nb_electeurs_oel_absents_in_reu"] = $nb_electeurs_oel_absents_in_reu;
        $this->LogToFile($nb_electeurs_oel_absents_in_reu);
        printf(
            '<p>%s</p>',
            $nb_electeurs_oel_absents_in_reu
        );
        foreach ($get_electeurs_oel_absents_in_reu as $key => $value) {
            $infos = sprintf(
                '- %s - %s - %s %s %s - INE : %s',
                $value["liste_insee"],
                $value["id"],
                $value["nom"],
                $value["prenom"],
                $this->f->formatDate($value["date_naissance"]),
                $value["ine"]
            );
            $this->LogToFile($infos);
            printf(
                '<p>%s</p>',
                $infos
            );
        }
        //
        $this->LogToFile("-- Liste des électeurs REU absents dans openElec");
        $this->f->displaySubtitle(
            __("Liste des électeurs REU absents dans openElec")
        );
        $get_electeurs_reu_non_rattaches = $this->get_electeurs_reu_non_rattaches();
        $nb_electeurs_reu_non_rattaches = count($get_electeurs_reu_non_rattaches);
        $reu_sync_check_status["nb_electeurs_reu_non_rattaches"] = $nb_electeurs_reu_non_rattaches;
        $this->LogToFile($nb_electeurs_reu_non_rattaches);
        printf(
            '<p>%s</p>',
            $nb_electeurs_reu_non_rattaches
        );
        foreach ($get_electeurs_reu_non_rattaches as $key => $value) {
            $infos = sprintf(
                '- %s - %s %s - INE : %s',
                $value["code_type_liste"],
                $value["nom"],
                $value["prenoms"],
                $value["numero_d_electeur"]
            );
            $this->LogToFile($infos);
            printf(
                '<p>%s</p>',
                $infos
            );
        }
        // //
        $this->LogToFile("-- Liste des électeurs openElec avec un état civil différent du REU");
        $this->f->displaySubtitle(
            __("Liste des électeurs openElec avec un état civil différent du REU")
        );
        $get_electeurs_oel_diffetatcivil = $this->get_electeurs_oel_diffetatcivil();
        $nb_electeurs_oel_diffetatcivil = count($get_electeurs_oel_diffetatcivil);
        $reu_sync_check_status["nb_electeurs_oel_diffetatcivil"] = $nb_electeurs_oel_diffetatcivil;
        $this->LogToFile($nb_electeurs_oel_diffetatcivil);
        printf(
            '<p>%s</p>',
            $nb_electeurs_oel_diffetatcivil
        );
        foreach ($get_electeurs_oel_diffetatcivil as $key => $value) {
            $infos = sprintf(
                '- %s - %s - %s %s %s %s - %s %s %s %s %s %s %s %s - INE : %s',
                $value["liste_insee"],
                $value["id"],
                $value["oel_nom"],
                $value["oel_prenom"],
                $value["oel_date_naissance"],
                $value["oel_sexe"],
                $value["reu_nom"],
                $value["reu_prenom"],
                $value["reu_date_de_naissance"],
                $value["reu_sexe"],
                $value["reu_clean_code_commune_de_naissance"],
                $value["reu_clean_libelle_commune_de_naissance"],
                $value["reu_clean_code_pays_de_naissance"],
                $value["reu_clean_libelle_pays_de_naissance"],
                $value["ine"]
            );
            $this->LogToFile($infos);
            printf(
                '<p>%s</p>',
                $infos
            );
        }
        //
        $this->LogToFile("-- Liste des électeurs openElec avec bureau de vote différent du REU");
        $this->f->displaySubtitle(
            __("Liste des électeurs openElec avec bureau de vote différent du REU")
        );
        $get_electeurs_oel_diffbureau = $this->get_electeurs_oel_diffbureau();
        $nb_electeurs_oel_diffbureau = count($get_electeurs_oel_diffbureau);
        $reu_sync_check_status["nb_electeurs_oel_diffbureau"] = $nb_electeurs_oel_diffbureau;
        $this->LogToFile($nb_electeurs_oel_diffbureau);
        printf(
            '<p>%s</p>',
            $nb_electeurs_oel_diffbureau
        );
        foreach ($get_electeurs_oel_diffbureau as $key => $value) {
            $infos = sprintf(
                '- %s - %s - %s %s %s %s BV %s (%s) - %s %s %s %s BV %s (%s) - INE : %s',
                $value["liste_insee"],
                $value["id"],
                $value["oel_nom"],
                $value["oel_prenom"],
                $value["oel_date_naissance"],
                $value["oel_sexe"],
                $value["oel_bureau_de_vote_code"],
                $value["oel_bureau_de_vote_referentiel_id"],
                $value["reu_nom"],
                $value["reu_prenom"],
                $value["reu_date_de_naissance"],
                $value["reu_sexe"],
                $value["reu_bureau_de_vote_code"],
                $value["reu_bureau_de_vote_id"],
                $value["ine"]
            );
            $this->LogToFile($infos);
            printf(
                '<p>%s</p>',
                $infos
            );
        }
        //
        $this->LogToFile("-- Liste des électeurs openElec avec numéro d'ordre différent du REU");
        $this->f->displaySubtitle(
            __("Liste des électeurs openElec avec numéro d'ordre différent du REU")
        );
        $get_electeurs_oel_diffnumeroordre = $this->get_electeurs_oel_diffnumeroordre();
        $nb_electeurs_oel_diffnumeroordre = count($get_electeurs_oel_diffnumeroordre);
        $reu_sync_check_status["nb_electeurs_oel_diffnumeroordre"] = $nb_electeurs_oel_diffnumeroordre;
        $this->LogToFile($nb_electeurs_oel_diffnumeroordre);
        printf(
            '<p>%s</p>',
            $nb_electeurs_oel_diffnumeroordre
        );
        foreach ($get_electeurs_oel_diffnumeroordre as $key => $value) {
            $infos = sprintf(
                '- %s - %s - %s %s %s %s BV %s (%s) n° %s - %s %s %s %s BV %s (%s) n° %s - INE : %s',
                $value["liste_insee"],
                $value["id"],
                $value["oel_nom"],
                $value["oel_prenom"],
                $value["oel_date_naissance"],
                $value["oel_sexe"],
                $value["oel_bureau_de_vote_code"],
                $value["oel_bureau_de_vote_referentiel_id"],
                $value["oel_numero_bureau"],
                $value["reu_nom"],
                $value["reu_prenom"],
                $value["reu_date_de_naissance"],
                $value["reu_sexe"],
                $value["reu_bureau_de_vote_code"],
                $value["reu_bureau_de_vote_id"],
                $value["reu_numero_bureau"],
                $value["ine"]
            );
            $this->LogToFile($infos);
            printf(
                '<p>%s</p>',
                $infos
            );
        }
        //
        $this->LogToFile("-- Liste des procurations openElec absentes du REU");
        $this->f->displaySubtitle(
            __("Liste des procurations openElec absentes du REU")
        );
        $get_procurations_oel_absents_in_reu = $this->get_procurations_oel_absents_in_reu();
        $nb_procurations_oel_absents_in_reu = count($get_procurations_oel_absents_in_reu);
        $reu_sync_check_status["nb_procurations_oel_absents_in_reu"] = $nb_procurations_oel_absents_in_reu;
        $this->LogToFile($nb_procurations_oel_absents_in_reu);
        printf(
            '<p>%s</p>',
            $nb_procurations_oel_absents_in_reu
        );
        foreach ($get_procurations_oel_absents_in_reu as $key => $value) {
            $infos = sprintf(
                '- %s - %s - %s - %s',
                $value["id"],
                $value["mandant_ine"],
                $value["mandataire_ine"],
                $value["statut"]
            );
            $this->LogToFile($infos);
            printf(
                '<p>%s</p>',
                $infos
            );
        }
        //
        $this->LogToFile("-- Liste des procurations REU absentes dans openElec");
        $this->f->displaySubtitle(
            __("Liste des procurations REU absentes dans openElec")
        );
        $get_procurations_reu_absents_in_oel = $this->get_procurations_reu_absents_in_oel();
        $nb_procurations_reu_absents_in_oel = count($get_procurations_reu_absents_in_oel);
        $reu_sync_check_status["nb_procurations_reu_absents_in_oel"] = $nb_procurations_reu_absents_in_oel;
        $this->LogToFile($nb_procurations_reu_absents_in_oel);
        printf(
            '<p>%s</p>',
            $nb_procurations_reu_absents_in_oel
        );
        foreach ($get_procurations_reu_absents_in_oel as $key => $value) {
            $infos = sprintf(
                '- %s - %s - %s - %s',
                $value["id"],
                $value["electeur_mandant"],
                $value["electeur_mandataire"],
                $value["etat_procuration_code"]
            );
            $this->LogToFile($infos);
            printf(
                '<p>%s</p>',
                $infos
            );
        }
        //
        $current_reu_sync_check_status = json_decode($this->f->getParameter("reu_sync_check_status"), true);
        if (is_array($current_reu_sync_check_status) !== true) {
            $current_reu_sync_check_status = array();
        }
        $new_reu_sync_check_status = array_merge($current_reu_sync_check_status, $reu_sync_check_status);
        $this->f->insert_or_update_parameter("reu_sync_check_status", json_encode($new_reu_sync_check_status));
        $this->commitTransaction();
        //
        $this->LogToFile("end ".$this->fichier);
    }
}
