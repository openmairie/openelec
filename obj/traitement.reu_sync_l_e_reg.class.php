<?php
/**
 * Ce script définit la classe 'reuSyncLERegTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_l_e.class.php";

/**
 * Définition de la classe 'reuSyncLERegTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLERegTraitement extends reuSyncLETraitement {
    /**
     * @var string
     */
    var $fichier = "reu_sync_l_e_reg";

    /**
     *
     */
    function treatment() {
        set_time_limit(180);
        $this->LogToFile("begin ".$this->fichier);
        //
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done") {
            $this->error = true;
            $message = __("Cet écran n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.");
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }

        $ret = $this->reload_reu_sync_listes();
        if ($ret !== true) {
            return;
        }

        // Liste des électeurs du REU absents dans openElec
        $ins_to_treat = $this->get_electeurs_reu_non_rattaches();
        $this->LogToFile(sprintf(
            __("Nombre d'électeurs du REU absent dans openElec : %s"),
            count($ins_to_treat)
        ));
        $this->LogToFile(sprintf(
            __("Liste des électeurs du REU absent dans openElec : %s"),
            print_r($ins_to_treat, true)
        ));

        // Liste des électeurs openElec absents dans le REU
        $rad_to_treat = $this->get_electeurs_oel_absents_in_reu();
        $this->LogToFile(sprintf(
            __("Nombre d'électeurs d'openElec absent dans le REU : %s"),
            count($rad_to_treat)
        ));
        $this->LogToFile(sprintf(
            __("Liste des électeurs d'openElec absent dans le REU : %s"),
            print_r($rad_to_treat, true)
        ));

        // Synchronisation des notifications
        require_once "../obj/reu_notification.class.php";
        $inst_reu_notif = $this->f->get_inst__om_dbform(array(
            "obj" => "reu_notification",
            "idx" => 0,
        ));
        $sync_last_notifications = $inst_reu_notif->sync_last_notifications();
        if ($sync_last_notifications === false) {
            $this->addToMessage($inst_reu_notif->msg);
            $this->LogToFile($inst_reu_notif->msg);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        //
        $query_notif = sprintf('
            SELECT id_electeur, type_de_notification
            FROM %1$sreu_notification
            WHERE traitee IS FALSE
                AND id_electeur IS NOT NULL
                AND om_collectivite = %2$s
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res_notif = $this->f->get_all_results_from_db_query($query_notif, true);
        if ($res_notif['code'] !== "OK") {
            $message = __("Erreur de base de données lors de la récupération des notifications non traitées.");
            $this->addToMessage($message);
            return;
        }
        $this->LogToFile(sprintf(
            __("Nombre de notification non traitées : %s"),
            count($res_notif['result'])
        ));
        $notif_no_treat = array(
            'INS' => array(),
            'INS_OFF' => array(),
            'RAD' => array(),
        );
        foreach ($res_notif['result'] as $value) {
            if (isset($notif_no_treat[$value['type_de_notification']]) !== true) {
                $notif_no_treat[$value['type_de_notification']] = array();
            }
            array_push($notif_no_treat[$value['type_de_notification']], $value['id_electeur']);
        }
        $this->LogToFile(sprintf(
            __("Liste des INE dans des notifications non traitées : %s"),
            print_r($notif_no_treat, true)
        ));

        //
        $query_ins_actif = sprintf('
            SELECT ine
            FROM %1$smouvement
            INNER JOIN %1$sparam_mouvement
                ON mouvement.types=param_mouvement.code
                AND lower(param_mouvement.typecat)=\'inscription\'
            WHERE etat = \'actif\'
                AND statut = \'vise_insee\'
            ',
            DB_PREFIXE
        );
        $res_ins_actif = $this->f->get_all_results_from_db_query($query_ins_actif, true);
        if ($res_ins_actif['code'] !== "OK") {
            $message = __("Erreur de base de données lors de la récupération des mouvements d'inscription encore actif.");
            $this->addToMessage($message);
            return;
        }
        $this->LogToFile(sprintf(
            __("Nombre de mouvement d'inscription encore actif : %s"),
            count($res_ins_actif['result'])
        ));
        $ins_actif = array();
        foreach ($res_ins_actif['result'] as $value) {
            $ins_actif[] = $value['ine'];
        }
        $this->LogToFile(sprintf(
            __("Liste des INE ayant un mouvement d'inscription encore actif : %s"),
            print_r($ins_actif, true)
        ));

        // Gestion du bureau unique
        // Si la commune n'a qu'un seul bureau stocke le code et l'id du bureau
        // openElec auquel on va rattacher toutes les nouvelles inscriptions
        $bureaux = $this->f->get_all__bureau__by_my_collectivite();
        $oo_bureau_ref_id = null;
        if (count($bureaux) == 1) {
            $oo_bureau_ref_id = $bureaux[0]['referentiel_id'];
        }
        if ($oo_bureau_ref_id !== null) {
            $this->LogToFile(sprintf(
                __("Collectivité possédant un bureau de vote unique dont l'identifiant référentiel est : %s"),
                $oo_bureau_ref_id
            ));
        }

        //
        $cpt_ins = 0;
        $cpt_ins_wo_bur = 0;
        $ins_treat = array();
        $ins_treat_wo_bur = array();
        // Pour chaque électeur présent dans le REU mais absent d'openElec
        foreach ($ins_to_treat as $ins) {
            $bureau_ok = false;
            // S'il n'y a pas de notification d'une demande d'inscription concernant
            // l'électeur
            // Ou un mouvement d'inscription encore actif
            if (in_array($ins['numero_d_electeur'], $notif_no_treat['INS']) !== true
                && in_array($ins['numero_d_electeur'], $notif_no_treat['INS_OFF']) !== true
                && in_array($ins['numero_d_electeur'], $ins_actif) !== true) {
                //
                if ($ins['id_bureau_de_vote'] !== ''
                    && $ins['id_bureau_de_vote'] !== null) {
                    //
                    $bureau_ok = true;
                }
                // Si le bureau de vote de l'électeur n'est pas renseigné mais que la
                // collectivité possède un unique bureau de vote
                if ($bureau_ok === false
                    && $oo_bureau_ref_id !== null) {
                    //
                    $ins['id_bureau_de_vote'] = $oo_bureau_ref_id;
                    $bureau_ok = true;
                }
                // Si l'électeur ne possède pas de bureau et qu'il n'y a pas un unique
                // bureau de vote sur la collectivité
                if ($bureau_ok === false) {
                    // Recherche dans les potentielles anciens mouvements d'inscriptions de
                    // l'électeur, l'information du bureau de vote
                    $ins['bureau_oel_id'] = $this->get_last_bureau_in_list_ins_electeur($ins['numero_d_electeur']);
                    if ($ins['bureau_oel_id'] === false) {
                        $this->LogToFile(sprintf(
                            "=> ECHEC Impossible de récupérer le dernier bureau de l'électeur dont l'INE est %s",
                            intval($ins["numero_d_electeur"])
                        ));
                        continue;
                    }
                    //
                    if ($ins['bureau_oel_id'] !== null
                        && $ins['bureau_oel_id'] !== '') {
                        //
                        $bureau_ok = true;
                    }
                }

                if ($bureau_ok === true) {
                    // Inscription de l'électeur
                    $electeur_reu = $inst_reu->handle_electeur(array(
                        "mode" => "get",
                        "ine" => $ins['numero_d_electeur'],
                    ));
                    $add_ins = $this->add_and_apply_mouvement_inscription($electeur_reu, $ins);
                    if ($add_ins === false) {
                        $this->LogToFile(sprintf(
                            "=> ECHEC Impossible d'inscrire l'électeur dont l'INE est %s",
                            intval($ins["numero_d_electeur"])
                        ));
                        $this->rollbackTransaction();
                        continue;
                    }
                    //
                    $cpt_ins++;
                    $ins_treat[] = $ins['numero_d_electeur'];
                } else {
                    //
                    $cpt_ins_wo_bur++;
                    $ins_treat_wo_bur[] = $ins['numero_d_electeur'];
                }
            }
        }
        $message = sprintf(
            __("Nombre d'électeurs inscrits : %s"),
            $cpt_ins
        );
        $this->addToMessage($message);
        $this->LogToFile($message);
        $message = sprintf(
            __("Nombre d'électeurs à inscrire qui n'ont pas de bureau de vote : %s"),
            $cpt_ins_wo_bur
        );
        if ($cpt_ins_wo_bur != 0) {
            $this->addToMessage($message);
        }
        $this->LogToFile($message);
        $this->LogToFile(sprintf(
            __("Liste des INE inscrits : %s"),
            print_r($ins_treat, true)
        ));
        $this->LogToFile(sprintf(
            __("Liste des INE  à inscrire qui n'ont pas de bureau de vote : %s"),
            print_r($ins_treat_wo_bur, true)
        ));

        //
        $cpt_rad = 0;
        $rad_treat = array();
        foreach ($rad_to_treat as $rad) {
            if (in_array($rad['ine'], $notif_no_treat['RAD']) !== true) {
                // Radiation de l'électeur
                $electeur_oel = $this->f->get_inst__om_dbform(array(
                    "obj" => "electeur",
                    "idx" => $rad['id'],
                ));
                $add_rad = $this->add_and_apply_mouvement_radiation($electeur_oel);
                if ($add_rad === false) {
                    $this->LogToFile(sprintf(
                        "=> ECHEC Impossible de radier l'électeur dont l'identifiant est %s",
                        intval($rad["id"])
                    ));
                    $this->rollbackTransaction();
                    continue;
                }
                //
                $cpt_rad++;
                $rad_treat[] = $rad['id'];
            }
        }
        $message = sprintf(
            __("Nombre d'électeurs radiés : %s"),
            $cpt_rad
        );
        $this->addToMessage($message);
        $this->LogToFile($message);
        $this->LogToFile(sprintf(
            __("Liste des identifiant d'électeur radiés : %s"),
            print_r($rad_treat, true)
        ));

        //
        $this->LogToFile("end ".$this->fichier);
    }

    function add_and_apply_mouvement_inscription($electeur_reu, $val = array()) {
        //
        $inst_inscription = $this->f->get_inst__om_dbform(array(
            "obj" => "inscription",
            "idx" => "]",
        ));
        // Lieu de naissance
        $birth_place = $this->f->handle_birth_place(array(
            "code_commune_de_naissance" => $val['code_commune_de_naissance'],
            "libelle_commune_de_naissance" => $val['libelle_commune_de_naissance'],
            "code_pays_de_naissance" => $val['code_pays_de_naissance'],
            "libelle_pays_de_naissance" => $val['libelle_pays_de_naissance'],
        ));
        $naissance_type_saisie = $inst_inscription->get_naissance_type_saisie(
            $birth_place["code_departement_naissance"],
            $birth_place["libelle_departement_naissance"],
            $birth_place["code_lieu_de_naissance"],
            $birth_place["libelle_lieu_de_naissance"]
        );
        $live_pays_de_naissance = '';
        $live_ancien_departement_francais_algerie = '';
        $live_commune_de_naissance = '';
        if ($naissance_type_saisie === "Etranger") {
            $live_pays_de_naissance = $birth_place["code_departement_naissance"];
        } elseif ($naissance_type_saisie === "Ancien_dep_franc") {
            $live_ancien_departement_francais_algerie = $birth_place["code_departement_naissance"];
        } elseif ($naissance_type_saisie === "France") {
            $live_commune_de_naissance = $birth_place["code_lieu_de_naissance"];
        }
        // Adresse de rattachement
        $adresse_rattachement = sprintf(
            '%1$s %2$s %3$s %4$s %5$s %6$s %7$s %8$s',
            trim($val['numero_de_voie']),
            trim($val['type_et_libelle_de_voie']),
            trim($val['premier_complement_adresse']),
            trim($val['second_complement_adresse']),
            trim($val['lieu_dit']),
            trim($val['code_postal']),
            trim($val['libelle_de_commune']),
            trim($val['libelle_du_pays'])
        );
        // Adresse résident
        $adresse_resident = trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "numVoie", )))." ".trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "libVoie", )));
        $complement_resident = trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "complement1", )))." ".trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "complement2", )));
        $cp_resident = (trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "codePostal", ))) !== '' ? trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "codePostal", ))) : '-');
        $ville_resident = (trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "commune", ))) !== '' ? trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "commune", ))) : '-');
        if ($adresse_resident === "" || $adresse_resident === null) {
            $adresse_resident = $complement_resident;
            $complement_resident = trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "lieuDit", )));
        }
        if ($adresse_resident === "" || $adresse_resident === null) {
            $adresse_resident = trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "lieuDit", )));
            $complement_resident = '-';
        }
        if ($ville_resident !== '-'
            && trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "pays", ))) !== ''
            && strtolower(trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "pays", )))) !== 'france') {
            //
            $ville_resident .= " ".trim(gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", "pays", )));
        }
        // Liste
        $liste = $this->f->db->getOne(
            sprintf(
                'SELECT liste FROM %1$sliste WHERE liste_insee=\'%2$s\'',
                DB_PREFIXE,
                $val['liste_insee']
            )
        );
        $this->f->isDatabaseError($liste);
        // Bureau de vote
        if (isset($val['bureau_oel_id']) === false) {
            $res_bureau = $this->f->db->query(
                sprintf(
                    'SELECT id, code FROM %1$sbureau WHERE referentiel_id=%3$s AND om_collectivite=%2$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $val['id_bureau_de_vote']
                )
            );
            $this->f->isDatabaseError($res_bureau);
            $row_bureau = $res_bureau->fetchRow(DB_FETCHMODE_ASSOC);
            $bureau = $row_bureau['id'];
        } else {
            $bureau = $val['bureau_oel_id'];
        }
        
        //
        $valF = array(
            //
            'origin' => 'reu_regularisation',
            //
            'electeur_id' => 0,
            'numero_bureau' => '',
            'bureau' => $bureau,
            'ancien_bureau' => '',
            'bureauforce' => ($bureau !== null ? 'Oui' : 'Non'),
            // liste et traitement
            'liste' => $liste,
            'date_tableau' => '2024-12-31',
            'tableau' => 'annuel',
            'etat' => 'actif',
            // etat civil
            'civilite' => ($val['sexe'] == "M" ? "M." : "Mme"),
            'sexe' => $val['sexe'],
            'nom' => $val['nom'],
            'nom_usage' => $val['nom_d_usage'],
            'prenom' => $val['prenoms'],
            //naissance
            'date_naissance' => $this->f->handle_birth_date($val['date_de_naissance']),
            'code_departement_naissance' => $birth_place["code_departement_naissance"],
            'libelle_departement_naissance' => $birth_place["libelle_departement_naissance"],
            'code_lieu_de_naissance' => $birth_place["code_lieu_de_naissance"],
            'libelle_lieu_de_naissance' => $birth_place["libelle_lieu_de_naissance"],
            'naissance_type_saisie' => $naissance_type_saisie,
            'live_pays_de_naissance' => $live_pays_de_naissance,
            'live_ancien_departement_francais_algerie' => $live_ancien_departement_francais_algerie,
            'live_commune_de_naissance' => $live_commune_de_naissance,
            'code_nationalite' => $val['code_nationalite'],
            'numero_habitation' => 0,
            'libelle_voie' => 'NON COMMUNIQUEE',
            'code_voie' => "NC".intval($_SESSION["collectivite"]),
            'complement_numero' => '',
            'complement' => '',
            // provenance
            'provenance' => '',
            'libelle_provenance' => '',
            // recuperation adresse resident
            'resident' => (gavoe($electeur_reu["result"], array("coordonneesContact", "adresse", )) === "" ? "Non" : "Oui"),
            'adresse_resident' => $adresse_resident,
            'complement_resident' => $complement_resident,
            'cp_resident' => $cp_resident,
            'ville_resident' => $ville_resident,
            //
            'telephone' => gavoe($electeur_reu["result"], array("coordonneesContact", "telephone1", )),
            'courriel' => gavoe($electeur_reu["result"], array("coordonneesContact", "mail", )),
            //initialiser a vide
            'id' => '',
            'utilisateur' => '',
            'date_modif' => '',
            'envoi_cnen' => '',
            'date_cnen' => '',
            //
            'om_collectivite' => intval($_SESSION["collectivite"]),
            'date_j5' => '',
            'types' => 'SRI',
            'observation' => '',
            'ine' => $val['numero_d_electeur'],
            'id_demande' => null,
            'visa' => null,
            'date_visa' => null,
            'date_complet' => null,
            'date_demande' => date("Y-m-d"),
            'statut' => 'ouvert',
            'adresse_rattachement_reu' => $adresse_rattachement,
            'historique' => null,
            'archive_electeur' => null,
        );
        //
        $add_ins = $inst_inscription->ajouter($valF);
        if ($add_ins === false) {
            $this->addToMessage($inst_inscription->msg);
            $this->addToMessage(__("Erreur lors de la création du mouvement d'inscription."));
            return false;
        }
        $inst_inscription->init_record_data($inst_inscription->valF[$inst_inscription->clePrimaire]);
        //
        $apply = $inst_inscription->apply();
        if ($apply === false) {
            $this->addToMessage($inst_inscription->msg);
            $this->addToMessage(__("Erreur lors de l'application du mouvement d'inscription."));
            $this->LogToFile(sprintf(
                "=> ECHEC Impossible d'appliquer l'inscription dont l'identifiant est %s",
                $inst_inscription->valF[$inst_inscription->clePrimaire]
            ));
            return false;
        }
        //
        $valF = array(
            "statut" => 'accepte',
        );
        $ret = $inst_inscription->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->addToMessage("Erreur lors du changement de statut du mouvement. Contactez votre administrateur.");
            return false;
        }
        // Gestion de l'historique du mouvement
        $val_histo = array(
            'action' => 'update_autoexecute',
            'message' => __("Mise à jour du statut depuis le traitement de synchronisation de la liste électorale après les notifications."),
            'datas' => $valF,
        );
        $histo = $inst_inscription->handle_historique($val_histo);
        if ($histo !== true) {
            $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
            return false;
        }
        //
        return true;
    }

    function add_and_apply_mouvement_radiation($electeur_oel) {
        //
        $inst_radiation = $this->f->get_inst__om_dbform(array(
            "obj" => "radiation",
            "idx" => "]",
        ));
        // Naissance
        $naissance_type_saisie = $inst_radiation->get_naissance_type_saisie(
            $electeur_oel->getVal('code_departement_naissance'),
            $electeur_oel->getVal('libelle_departement_naissance'),
            $electeur_oel->getVal('code_lieu_de_naissance'),
            $electeur_oel->getVal('libelle_lieu_de_naissance')
        );
        //
        $val = array(
            'origin' => 'reu_regularisation',
            //
            'electeur_id' => $electeur_oel->getVal('id'),
            'numero_bureau' => $electeur_oel->getVal('numero_bureau'),
            'bureau' => $electeur_oel->getVal('bureau'),
            'ancien_bureau' => $electeur_oel->getVal('bureau'),
            'bureauforce' => $electeur_oel->getVal('bureauforce'),
            // liste et traitement
            'liste' => $electeur_oel->getVal('liste'),
            'date_tableau' => '2024-12-31',
            'tableau' => "annuel",
            'etat' => "actif",
            // etat civil
            'civilite' => $electeur_oel->getVal('civilite'),
            'sexe' => $electeur_oel->getVal('sexe'),
            'nom' => $electeur_oel->getVal('nom'),
            'nom_usage' => $electeur_oel->getVal('nom_usage'),
            'prenom' => $electeur_oel->getVal('prenom'),
            //naissance
            'date_naissance' => $electeur_oel->getVal('date_naissance'),
            'code_departement_naissance' => $electeur_oel->getVal('code_departement_naissance'),
            'libelle_departement_naissance' => $electeur_oel->getVal('libelle_departement_naissance'),
            'code_lieu_de_naissance' => $electeur_oel->getVal('code_lieu_de_naissance'),
            'libelle_lieu_de_naissance' => $electeur_oel->getVal('libelle_lieu_de_naissance'),
            'naissance_type_saisie' => $naissance_type_saisie,
            'code_nationalite' => $electeur_oel->getVal('code_nationalite'),
            // adresse dans la commune
            'numero_habitation' => $electeur_oel->getVal('numero_habitation'),
            'libelle_voie' => $electeur_oel->getVal('libelle_voie'),
            'code_voie' => $electeur_oel->getVal('code_voie'),
            'complement_numero' => $electeur_oel->getVal('complement_numero') ,
            'complement' => $electeur_oel->getVal('complement'),
            // provenance
            'provenance' => $electeur_oel->getVal('provenance'),
            'libelle_provenance' => $electeur_oel->getVal('libelle_provenance'),
            // recuperation adresse resident
            'resident' => $electeur_oel->getVal('resident'),
            'adresse_resident' => $electeur_oel->getVal('adresse_resident'),
            'complement_resident' => $electeur_oel->getVal('complement_resident'),
            'cp_resident' => $electeur_oel->getVal('cp_resident'),
            'ville_resident' => $electeur_oel->getVal('ville_resident'),
            //
            'telephone' => $electeur_oel->getVal('telephone'),
            'courriel' => $electeur_oel->getVal('courriel'),
            //initialiser a vide
            'id' => '',
            'utilisateur' => '',
            'date_modif' => '',
            'envoi_cnen' => '',
            'date_cnen' => '',
            //
            'om_collectivite' => $electeur_oel->getVal('om_collectivite'),
            'date_j5' => '',
            'types' => 'SRR',
            'observation' => '',
            'ine' => $electeur_oel->getVal('ine'),
            'id_demande' => null,
            'visa' => null,
            'date_visa' => null,
            'date_complet' => null,
            'date_demande' => date("Y-m-d"),
            "statut" => 'ouvert',
            'adresse_rattachement_reu' => null,
            'historique' => null,
            'archive_electeur' => null,
        );
        //
        $add_ins = $inst_radiation->ajouter($val);
        if ($add_ins === false) {
            $this->addToMessage($inst_radiation->msg);
            $this->addToMessage(__("Erreur lors de la création du mouvement de radiation."));
            return false;
        }
        $inst_radiation->init_record_data($inst_radiation->valF[$inst_radiation->clePrimaire]);
        //
        $apply = $inst_radiation->apply();
        if ($apply === false) {
            $this->addToMessage($inst_radiation->msg);
            $this->addToMessage(__("Erreur lors de l'application du mouvement de radiation."));
            $this->LogToFile(sprintf(
                "=> ECHEC Impossible d'appliquer la radiation dont l'identifiant est %s",
                $inst_radiation->valF[$inst_radiation->clePrimaire]
            ));
            return false;
        }
        //
        $valF = array(
            "statut" => 'accepte',
        );
        $ret = $inst_radiation->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->addToMessage("Erreur lors du changement de statut du mouvement. Contactez votre administrateur.");
            return false;
        }
        // Gestion de l'historique du mouvement
        $val_histo = array(
            'action' => 'update_autoexecute',
            'message' => __("Mise à jour du statut depuis le traitement de synchronisation de la liste électorale après les notifications."),
            'datas' => $valF,
        );
        $histo = $inst_radiation->handle_historique($val_histo);
        if ($histo !== true) {
            $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
            return false;
        }
        //
        return true;
    }

    function get_last_bureau_in_list_ins_electeur($ine) {
        $query = sprintf('
            SELECT bureau.id
            FROM %1$smouvement
                INNER JOIN %1$sbureau
                    ON mouvement.bureau_de_vote_code = bureau.code
                INNER JOIN %1$sreu_notification
                    ON mouvement.id_demande = reu_notification.id_demande
                    AND reu_notification.id_electeur = %3$s
            WHERE mouvement.om_collectivite = %2$s
                AND mouvement.bureau_de_vote_code IS NOT NULL
                AND mouvement.bureau_de_vote_code != \'\'
            ORDER BY mouvement.id DESC
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $ine
        );
        $res = $this->f->get_one_result_from_db_query($query, true);
        if ($res['code'] !== "OK") {
            $message = __("Erreur de base de données lors de la récupération des mouvements d'inscription de l'électeur sans bureau de vote.");
            $this->addToMessage($message);
            return false;
        }
        return $res['result'];
    }
}
