<?php
/**
 * Ce script définit la classe 'carteretourTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'carteretourTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class carteretourTraitement extends traitement {

    var $fichier = "carteretour";

    var $champs = array(
        "id_elec",
        "date_saisie_carte_retour"
    );
    var $form_class = "no_confirmation no_status_update";

    function setContentForm() {
        // identifiant de l'électeur
        $this->form->setLib("id_elec", __("Presenter un code barre ou saisir un identifiant electeur"));
        $this->form->setType("id_elec", "text");
        $this->form->setTaille("id_elec", 15);
        $this->form->setMax("id_elec", 15);
        // date de saisie de la carte en retour
        $this->form->setLib("date_saisie_carte_retour", __("Date de saisie"));
        $this->form->setType("date_saisie_carte_retour", "date");
        $this->form->setTaille("date_saisie_carte_retour", 10);
        $this->form->setMax("date_saisie_carte_retour", 10);
    }

    function getValidButtonContent() {
        //
        return __("Saisie de la carte en retour");
    }

    function treatment () {
        //
        $this->LogToFile("start carteretour");
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_carteretour.inc.php";
        // Vérifie si le formulaire a été validé
        if (isset($_POST["id_elec"]) and $_POST["id_elec"] != "") {
            //
            $id_elec = $_POST["id_elec"];
            // Si l'identifiant de l'électeur n'est pas correct, affiche un message
            // d'erreur sur le formulaire.
            if (!is_numeric($id_elec)) {
                //
                $this->error = true;
                //
                $message = "[".$id_elec."] ".__("L'identifiant saisi est incorrect.");
                //
                $this->LogToFile($message);
                $this->addToMessage($message);
            } else {
                // Si l'identifiant saisi est correct, vérifie si cet identifiant existe.
                $query_select = sprintf(
                    'SELECT
                        *
                    FROM
                        %1$selecteur
                        INNER JOIN %1$sliste ON electeur.liste = liste.liste 
                    WHERE
                        electeur.ine=%3$s AND
                        electeur.om_collectivite=%2$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $id_elec
                );
                $res_select = $this->f->db->query($query_select);
                $this->f->addToLog(
                    __METHOD__."(): db->query(\"".$query_select."\");",
                    VERBOSE_MODE
                );
                if ($this->f->isDatabaseError($res_select, true)) {
                    // Gestion des erreurs de base de données
                    $this->error = true;
                    //
                    $message = $res_select->getMessage()." - ".$res_select->getUserInfo();
                    $this->LogToFile($message);
                    //
                    $this->addToMessage(__("Contactez votre administrateur."));
                } else {
                    //
                    $nb = $res_select->numRows();
                    // Si l'identifiant d'électeur n'a pas été trouvé dans la base
                    // affiche un message a destination de l'utilisateur pour l'avertir
                    // que l'identifiant saisi n'existe pas.
                    if ($nb == 0) {
                        //
                        $this->error = true;
                        //
                        $message = __("Cet identifiant d'electeur n'existe pas.");
                        $this->LogToFile($message);
                        $this->addToMessage($message);
                    } else {
                        //
                        //$row_select = $res_select->fetchRow(DB_FETCHMODE_ASSOC);
                        //
                        $cpt_error = 0;
                        //
                        while ($row_select =& $res_select->fetchRow(DB_FETCHMODE_ASSOC)) {
                            $infos = $row_select['nom']." ".$row_select['prenom']." - ".$row_select['nom_usage'];
                            $infos .= " ".__("ne(e) le")." ";
                            $infos .= substr($row_select['date_naissance'],8,2)."/".substr($row_select['date_naissance'],5,2)."/".substr($row_select['date_naissance'],0,4)." ";
                            $infos .= " de la liste ".$row_select['liste_insee']." ";
                            // Vérifie si l'électeur à déjà une carte en retour associée
                            if ($row_select["carte"] == 1) {
                                // Si c'est le cas averti l'utilisateur que l'électeur à déjà une carte
                                // en retour.
                                $message = __("a deja une carte en retour.");
                                // Si une date est enregistré alors la date de saisie est précisée dans le message
                                if (! empty($row_select['date_saisie_carte_retour'])) {
                                    $message = sprintf(
                                        '%1$s %2$s.',
                                        __("a deja une carte en retour en date du "),
                                        $this->f->formatDate($row_select['date_saisie_carte_retour'], true)
                                    );
                                }
                                $this->LogToFile($infos.$message);
                                $this->f->displayMessage("error", $infos.$message);
                                $cpt_error++;
                            } else {
                                // Sinon prépare la requête permettant d'ajouter la carte à l'électeur
                                // dans la base.
                                // Vérifie si une date de saisie a été saisie
                                $query_date_retour = '';
                                $date_saisie = $_POST["date_saisie_carte_retour"];
                                if (! empty($date_saisie)) {
                                    // Vérifie le format de la date. Si il n'est pas correct arrête le traitement
                                    // et informe l'utilisateur que la date n'est pas correct.
                                    if (! $this->f->formatDate($date_saisie, false)) {
                                        $message = __("Le format de la date de saisie est incorrect.");
                                        $this->f->displayMessage("error", $message);
                                        // Arrête le traitement et le marque en erreur.
                                        $this->error = true;
                                        return;
                                    }
                                    // Prépare la requête servant à mettre à jour la date de saisie.
                                    // Note : la "," ne doiyt pas être enlevé car c'est elle qui
                                    //        permet d'ajouter un nouvel argument dans la requête !
                                    $query_date_retour = sprintf(
                                        ',
                                        date_saisie_carte_retour = \'%s\'',
                                        $this->f->db->escapeSimple($date_saisie)
                                    );
                                }
                                $query_update = sprintf(
                                    'UPDATE
                                        %1$selecteur
                                    SET
                                        carte=\'1\'%4$s
                                    WHERE
                                        electeur.ine=%3$s AND
                                        electeur.om_collectivite=%2$s',
                                    DB_PREFIXE,
                                    intval($_SESSION["collectivite"]),
                                    $id_elec,
                                    $query_date_retour
                                );
                                //
                                $res_update = $this->f->db->query($query_update);
                                //
                                if ($this->f->isDatabaseError($res_update, true)) {
                                    //
                                    $message = $res_update->getMessage()." - ".$res_update->getUserInfo();
                                    $this->LogToFile($message);
                                    //
                                    $this->addToMessage(__("Contactez votre administrateur."));
                                } else {
                                    //
                                    $message = $infos.__("a maintenant une carte en retour.");
                                    // Message informant que la date de saisie à également
                                    // été mis à jour.
                                    if (! empty($query_date_retour)) {
                                        $message .= ' '.__('La date de saisie de la carte en retour a été enregistrée.');
                                    }
                                    $this->LogToFile($message);
                                    $this->f->displayMessage("valid", $message);
                                }
                            }
                            if ($cpt_error == 2){
                                $this->error = true;
                            }
                        }
                    }
                }
            }
        } else {
            //
            $this->error = true;
            //
            $message = __("L'identifiant saisi est incorrect.");
            //
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end carteretour");
    }
}


