<?php
/**
 * Ce script définit la classe 'reuSyncMouvementModificationTraitement'.
 *
 * @package openelec
 * @version SVN : $Id: traitement.reu_sync_ancien_mouvement_modification.class.php 1766 2019-02-26 14:55:01Z fmichon $
 */

require_once "../obj/traitement.reu_sync_mouvement.class.php";

/**
 * Définition de la classe 'reuSyncMouvementModificationTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncMouvementModificationTraitement extends reuSyncMouvementTraitement {
    /**
     *
     */
    var $fichier = "reu_sync_mouvement_modification";
    /**
     *
     */
    function displayBeforeContentForm() {
        //
        $modifications_to_treat = $this->get_modifications(array(
            "type" => "nb",
            "mode" => "a_transmettre",
        ));
        //
        printf(
            '
            <div class="container-fluid" id="reu-sync-tab">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="card text-center">
                            <div class="card-header">
                                modification(s) à transmettre
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%s</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="visualClear"></div>
            ',
            $modifications_to_treat["result"]
        );
    }

    /**
     *
     */
    function treatment() {
        $this->LogToFile("begin ".$this->fichier);
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->error = true;
            $this->addToMessage("Erreur de connexion REU.");
            $this->LogToFile("Erreur de connexion REU.");
            $this->LogToFile("end reu_sync_ancien_mouvement_modification");
            return;
        }
        if ($inst_reu->is_connexion_standard_valid() !== true) {
            $this->error = true;
            $this->addToMessage(sprintf(
                '%s : %s',
                __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                sprintf(
                    '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                    __("cliquez ici pour vérifier")
                )
            ));
            $this->LogToFile("La connexion standard n'est pas valide.");
            $this->LogToFile("end reu_sync_ancien_mouvement_modification");
            return;
        }
        /**
         *
         */
        $modifications = $this->get_modifications(array(
            "type" => "list",
            "mode" => "a_transmettre",
        ));
        $modifications_to_treat = $modifications['result'];
        $nb_modifications_to_treat = count($modifications['result']);
        $this->LogToFile(sprintf(
            __("Nombre de modifications à valider : %s"),
            $nb_modifications_to_treat
        ));
        $nb_res_vis = 0;
        foreach ($modifications['result'] as $key => $value) {
            $this->startTransaction();
            $inst_modification = $this->f->get_inst__om_dbform(array(
                "obj" => "modification",
                "idx" => intval($value["id"]),
            ));
            if ($inst_modification->exists() !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC La modification %s n'existe pas",
                    intval($value["id"])
                ));
                $this->rollbackTransaction();
                continue;
            }
            $ret = $inst_modification->valider();
            if ($ret !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC Validation de la modification %s échouée : %s",
                    intval($value["id"]),
                    $inst_modification->msg
                ));
                $this->rollbackTransaction();
                break;
            }
            $nb_res_vis++;
            $this->commitTransaction();
            $this->LogToFile(sprintf(
                "Mouvement %s - INE %s - Modification appliquée au REU avec succès",
                intval($value["id"]),
                $inst_modification->getVal("ine")
            ));
        }
        //
        if ($nb_modifications_to_treat != 0) {
            $message = sprintf(
                __('%1$s modification(s) validée(s) sur un total de %2$s.'),
                $nb_res_vis,
                $nb_modifications_to_treat
            );
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        if ($nb_modifications_to_treat == 0) {
            $message = __("Aucune modification à traiter.");
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        // Si il reste des modifications à traiter, on affiche un message
        // d'erreur
        $nb_modifications_to_treat = $this->get_modifications(array(
            "type" => "nb",
            "mode" => "a_transmettre",
        ));
        if ($nb_modifications_to_treat["result"] != 0) {
            $this->error = true;
        }
        //
        $this->LogToFile("end ".$this->fichier);
    }
}
