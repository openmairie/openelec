<?php
/**
 * Ce script définit la classe 'module'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_base.class.php";

/**
 * Définition de la classe 'module' (om_base).
 */
class module extends om_base {

    /**
     *
     */
    public function __construct() {
        $this->init_om_application();
        $this->init_module();
    }

    function init_module() {
        $this->module = array();
    }


    /**
     * CONDITION - is_liste_principale
     *
     * @return boolean
     */
    function is_liste_principale() {
        if ($_SESSION["liste"] == "01") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_collectivity_mono
     *
     * @return boolean
     */
    function is_collectivity_mono() {
        return $this->f->getParameter("is_collectivity_mono");
    }

    /**
     *
     */
    public function view_main() {
        $module = $this->module;

        //
        $module_condition_satisfied = true;
        if (isset($module["condition"])) {
            foreach ($module["condition"] as $condition) {
                if (method_exists($this, $condition)) {
                    $ret = $this->$condition();
                    if ($ret !== true) {
                        $module_condition_satisfied = false;
                        break;
                    }
                }
            }
        }

        //
        $available_views = array();
        if ($module_condition_satisfied === true) {
            foreach ($this->module["elems"] as $key => $value) {
                if (array_key_exists("view", $value) === true) {
                    $available_views[$value["view"]] = $value["right"];
                }
            }
        }
        if (in_array($this->f->get_submitted_get_value("view"), array_keys($available_views)) === true) {
            $method_name = "view__".$this->f->get_submitted_get_value("view");
            if (method_exists($this, $method_name) === true) {
                if ($this->f->isAccredited($module["right"]) !== true
                    || $this->f->isAccredited($available_views[$this->f->get_submitted_get_value("view")]) !== true) {
                    // Si ce n'est pas une requete Ajax
                    if ($this->f->isAjaxRequest() !== true) {
                        $this->f->setFlag(null);
                        $this->f->display();
                    } else {
                        // Definition du charset de la page
                        header("Content-type: text/html; charset=".HTTPCHARSET."");
                    }
                    $this->f->displayMessage("error", __("Droits insuffisants. Contactez votre administrateur."));
                    return;
                }
                $this->$method_name();
                return;
            } else {
                // Si ce n'est pas une requete Ajax
                if ($this->f->isAjaxRequest() !== true) {
                    $this->f->setFlag(null);
                    $this->f->display();
                    return;
                } else {
                    // Definition du charset de la page
                    header("Content-type: text/html; charset=".HTTPCHARSET."");
                }
                $this->f->displayMessage("error", __("Une erreur s'est produite. Contactez votre administrateur."));
                return;
            }
        }


        /**
         * Affichage de la structure de la page
         */
        $this->f->setFlag(null);
        $this->f->setTitle($module["title"]);
        $this->f->isAuthorized($module["right"]);
        $this->f->display();
        if (isset($module["description"])) {
            $this->f->displayDescription($module["description"]);
        }

        /**
         *
         */
        if ($module_condition_satisfied !== true) {
            // Affichage du message
            $message_class= "error";
            $message = __("Ce module n'est pas disponible.");
            $this->f->displayMessage($message_class, $message);
            // Fin du script
            die();
        }

        /**
         * Affichage des éléments en onglet
         */
        // Ouverture de la balise - Conteneur d'onglets
        echo "<div id=\"traitement-tabs\">\n";
        // Affichage de la liste des onglets
        echo "\n<ul>\n";
        foreach ($module["elems"] as $elem) {
            //
            if (isset($elem["type"])
                && $elem["type"] != "tab") {
                continue;
            }
            //
            if ($this->f->isAccredited($elem["right"]) !== true) {
                continue;
            }
            if (isset($elem["condition"])) {
                $ok = true;
                foreach ($elem["condition"] as $condition) {
                    if (method_exists($this, $condition)) {
                        $ret = $this->$condition();
                        if ($ret !== true) {
                            $ok = false;
                        }
                    }
                }
                if ($ok !== true) {
                    continue;
                }
            }
            //
            $id = '';
            if (isset($elem["id"])) {
                $id = ' id="'.$elem["id"].'"';
            }
            //
            echo "\t<li><a href=\"".$elem["href"]."\"".$id.">".$elem["title"]."</a></li>\n";
        }
        //
        echo "</ul>\n";
        // Fermeture de la balise - Conteneur d'onglets
        echo "</div>";
    }
}
