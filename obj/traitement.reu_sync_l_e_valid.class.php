<?php
/**
 * Ce script définit la classe 'reuSyncLEValidTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_l_e.class.php";

/**
 * Définition de la classe 'reuSyncLEValidTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLEValidTraitement extends reuSyncLETraitement {

    /**
     *
     */
    var $fichier = "reu_sync_l_e_valid";
    var $form_class = "hide_button_on_success";

    /**
     *
     */
    function getValidButtonValue() {
        return __("Valider la synchronisation");
    }

    function is_done() {
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done") {
            return true;
        }
        return false;
    }

    function apply_mouvement_inscription($inscriptions_ids = array()) {
        if (count($inscriptions_ids) == 0) {
            return;
        }
        // Traitement RADIATIONS
        $query_select_inscription = sprintf(
            'SELECT * FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE lower(param_mouvement.typecat)=\'inscription\' and mouvement.etat=\'actif\' and mouvement.id IN (%2$s)',
            DB_PREFIXE,
            implode(',', $inscriptions_ids)
        );
        // Traitement INSCRIPTIONS
        $res_select_inscription = $this->f->db->query($query_select_inscription);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query_select_inscription."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_select_inscription, true)) {
            $this->error = true;
            $message = $res_select_inscription->getMessage()." erreur sur ".$query_select_inscription."";
            $this->LogToFile($message);
        } else {
            $this->LogToFile("TRAITEMENT DES INSCRIPTIONS");
            $this->LogToFile($query_select_inscription);
            while ($row =& $res_select_inscription->fetchRow(DB_FETCHMODE_ASSOC)) {
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom'];
                // ajout ELECTEUR
                $enr = $this->f->get_inst__om_dbform(array(
                    "obj" => "electeur",
                    "idx" => "]",
                ));
                $enr->valF['tableau'] = 'annuel';
                $ret = $enr->ajouterTraitement($row, $row["date_tableau"]);
                if ($ret !== true) {
                    $this->error = true;
                    $message .= " -> Erreur lors de l'ajout de l'électeur";
                    $this->LogToFile($message);
                    break;
                }
                $message .= " -> ".$enr->msg;
                $this->LogToFile($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'annuel',
                    'electeur_id' => $enr->valF["id"],
                    'date_j5' => ''.$enr->dateSystemeDB().'',
                );
                $cle = "id=".$row['id'];
                //
                $res1 = $this->f->db->autoexecute(
                    sprintf('%1$s%2$s', DB_PREFIXE, "mouvement"),
                    $fields_values,
                    DB_AUTOQUERY_UPDATE,
                    $cle
                );
                $this->f->addToLog(
                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "mouvement")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                    VERBOSE_MODE
                );
                //
                if ($this->f->isDatabaseError($res1, true) !== false) {
                    $this->error = true;
                    $message = $res1->getMessage()." - ".$res1->getUserInfo();
                    $this->LogToFile($message);
                    break;
                }
                //
                $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                $this->LogToFile($message);
            }
            $res_select_inscription->free();
        }
    }


    function apply_mouvement_modification($mouvement_id) {
        $inst_modification = $this->f->get_inst__om_dbform(array(
            "obj" => "modification",
            "idx" => intval($mouvement_id),
        ));
        if ($inst_modification->exists() !== true) {
            $this->error = true;
            $message = sprint(
                "La modification %s n'existe pas.",
                intval($mouvement_id)
            );
            $this->LogToFile($message);
            return false;
        }
        $ret = $inst_modification->apply();
        if ($ret !== true) {
            $this->error = true;
            $message = sprint(
                "Echec de l'application de la modification %s.",
                intval($mouvement_id)
            );
            $this->LogToFile($message);
            return false;
        }
        return true;
    }

    function apply_mouvement_radiation($radiations_ids = array()) {
        if (count($radiations_ids) == 0) {
            return;
        }
        // Traitement RADIATIONS
        $query_select_radiation = sprintf(
            'SELECT * FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE lower(param_mouvement.typecat)=\'radiation\' and mouvement.etat=\'actif\' and mouvement.id IN (%2$s)',
            DB_PREFIXE,
            implode(',', $radiations_ids)
        );
        $res_select_radiation = $this->f->db->query($query_select_radiation);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query_select_radiation."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_select_radiation, true)) {
            $this->error = true;
            $message = $res_select_radiation->getMessage()." erreur sur ".$query_select_radiation."";
            $this->LogToFile($message);
        } else {
            $this->LogToFile("TRAITEMENT DES RADIATIONS");
            $this->LogToFile($query_select_radiation);
            while ($row =& $res_select_radiation->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom'];
                // suppression ELECTEUR
                $enr = $this->f->get_inst__om_dbform(array(
                    "obj" => "electeur",
                    "idx" => $row['electeur_id'],
                ));
                $ret = $enr->supprimerTraitement();
                if ($ret !== true) {
                    $this->error = true;
                    $message .= "-> Erreur lors de la suppresion de l'électeur";
                    $this->LogToFile($message);
                    break;
                }
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom']." - ".$enr->msg."";
                $this->LogToFile($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'annuel',
                    'date_j5' => ''.$enr->dateSystemeDB().'',
                );
                $cle = "id=".$row['id'];
                //
                $res1 = $this->f->db->autoexecute(
                    sprintf('%1$s%2$s', DB_PREFIXE, "mouvement"),
                    $fields_values,
                    DB_AUTOQUERY_UPDATE,
                    $cle
                );
                $this->f->addToLog(
                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "mouvement")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                    VERBOSE_MODE
                );
                if ($this->f->isDatabaseError($res1)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage()." - ".$res1->getUserInfo();
                    $this->LogToFile($message);
                    //
                    break;
                } else {
                    //
                    $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                    $this->LogToFile($message);
                }
            }
            $res_select_radiation->free();
        }
    }

    /**
     *
     */
    function handle_and_treat_radiation() {
        $radiations_to_treat = array();
        $nouvelles_radiations = 0;
        $radiations_existantes = 0;
        $electeurs_oel_non_rattaches = $this->get_electeurs_oel_non_rattaches();
        foreach ($electeurs_oel_non_rattaches as $key => $value) {
            $inst_electeur = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => $value["id"],
            ));
            if ($inst_electeur->getVal("typecat") !== "") {
                if (strtolower($inst_electeur->getVal("typecat")) == "radiation") {
                    $inst_radiation = $this->f->get_inst__om_dbform(array(
                        "obj" => "radiation",
                        "idx" => $inst_electeur->getVal("mouvement"),
                    ));
                    if ($inst_radiation->getVal("date_tableau") != "2019-01-10") {
                        $this->LogToFile("mouvement supprimé : ".print_r($inst_radiation->val, true));
                        $val = array(
                            "id" => $inst_electeur->getVal("mouvement"),
                            "electeur_id" => $inst_electeur->getVal("id"),
                        );
                        $ret = $inst_radiation->supprimer($val);
                        if ($ret !== true) {
                            $this->erreur = true;
                        }
                    } else {
                        $radiations_existantes += 1;
                        $radiations_to_treat[] = $inst_electeur->getVal("mouvement");
                        continue;
                    }
                }
                if (strtolower($inst_electeur->getVal("typecat")) == "modification") {
                    $inst_modification = $this->f->get_inst__om_dbform(array(
                        "obj" => "modification",
                        "idx" => $inst_electeur->getVal("mouvement"),
                    ));
                    if ($inst_modification->getVal("date_tableau") != "2019-01-10") {
                        $this->LogToFile("mouvement supprimé : ".print_r($inst_modification->val, true));
                        $val = array(
                            "id" => $inst_electeur->getVal("mouvement"),
                            "electeur_id" => $inst_electeur->getVal("id"),
                        );
                        $ret = $inst_modification->supprimer($val);
                        if ($ret !== true) {
                            $this->erreur = true;
                        }
                    } else {
                        $this->apply_mouvement_modification($inst_electeur->getVal("mouvement"));
                    }
                }
            }
            $inst_radiation = $this->f->get_inst__om_dbform(array(
                "obj" => "radiation",
                "idx" => "]",
            ));
            $inst_bureau = $this->f->get_inst__om_dbform(array(
                "obj" => "bureau",
                "idx" => $inst_electeur->getVal("bureau"),
            ));
            //
            $naissance_type_saisie = $inst_radiation->get_naissance_type_saisie(
                $inst_electeur->getVal('code_departement_naissance'),
                $inst_electeur->getVal('libelle_departement_naissance'),
                $inst_electeur->getVal('code_lieu_de_naissance'),
                $inst_electeur->getVal('libelle_lieu_de_naissance')
            );
            //
            $val = array(
                'electeur_id' => $inst_electeur->getVal('id'),
                'numero_bureau' => $inst_electeur->getVal('numero_bureau'),
                'bureau' => $inst_electeur->getVal('bureau'),
                'ancien_bureau' => $inst_electeur->getVal('bureau'),
                'bureauforce' => $inst_electeur->getVal('bureauforce'),
                // liste et traitement
                'liste' => $inst_electeur->getVal('liste'),
                'date_tableau' => '2024-12-31',
                'tableau' => "annuel",
                'etat' => "actif",
                // etat civil
                'civilite' => $inst_electeur->getVal('civilite'),
                'sexe' => $inst_electeur->getVal('sexe'),
                'nom' => $inst_electeur->getVal('nom'),
                'nom_usage' => $inst_electeur->getVal('nom_usage'),
                'prenom' => $inst_electeur->getVal('prenom'),
                //naissance
                'date_naissance' => $inst_electeur->getVal('date_naissance'),
                'code_departement_naissance' => $inst_electeur->getVal('code_departement_naissance'),
                'libelle_departement_naissance' => $inst_electeur->getVal('libelle_departement_naissance'),
                'code_lieu_de_naissance' => $inst_electeur->getVal('code_lieu_de_naissance'),
                'libelle_lieu_de_naissance' => $inst_electeur->getVal('libelle_lieu_de_naissance'),
                'naissance_type_saisie' => $naissance_type_saisie,
                'code_nationalite' => $inst_electeur->getVal('code_nationalite'),
                // adresse dans la commune
                'numero_habitation' => $inst_electeur->getVal('numero_habitation'),
                'libelle_voie' => $inst_electeur->getVal('libelle_voie'),
                'code_voie' => $inst_electeur->getVal('code_voie'),
                'complement_numero' => $inst_electeur->getVal('complement_numero') ,
                'complement' => $inst_electeur->getVal('complement'),
                // provenance
                'provenance' => $inst_electeur->getVal('provenance'),
                'libelle_provenance' => $inst_electeur->getVal('libelle_provenance'),
                // recuperation adresse resident
                'resident' => $inst_electeur->getVal('resident'),
                'adresse_resident' => $inst_electeur->getVal('adresse_resident'),
                'complement_resident' => $inst_electeur->getVal('complement_resident'),
                'cp_resident' => $inst_electeur->getVal('cp_resident'),
                'ville_resident' => $inst_electeur->getVal('ville_resident'),
                //
                'telephone' => $inst_electeur->getVal('telephone'),
                'courriel' => $inst_electeur->getVal('courriel'),
                //initialiser a vide
                'id' => '',
                'observation' => '',
                'utilisateur' => '',
                'types' => '',
                'date_modif' => '',
                'envoi_cnen' => '',
                'date_cnen' => '',
                //
                'om_collectivite' => $inst_electeur->getVal('om_collectivite'),
                'date_j5' => '',
                'types' => 'SRR',
                'observation' => 'Radiation synchronisation REU',
                'ine' => null,
                'id_demande' => null,
                'visa' => null,
                'date_visa' => null,
                'adresse_rattachement_reu' => null,
                'date_complet' => null,
                'date_demande' => date("Y-m-d"),
                "statut" => null,
                'historique' => null,
                'archive_electeur' => null,
            );
            if ($naissance_type_saisie === "Etranger") {
                $val['live_pays_de_naissance'] = $inst_electeur->getVal('code_departement_naissance');
            } elseif ($naissance_type_saisie === "Ancien_dep_franc") {
                $val['live_ancien_departement_francais_algerie'] = $inst_electeur->getVal('code_departement_naissance');
            } elseif ($naissance_type_saisie === "France") {
                $val['live_commune_de_naissance'] = $inst_electeur->getVal('code_lieu_de_naissance');
            }
            $ret = $inst_radiation->ajouter($val);
            if ($ret == true) {
                $nouvelles_radiations += 1;
                $radiations_to_treat[] = $inst_radiation->valF["id"];
            } else {
                $this->error = true;
                echo $inst_radiation->msg;
                $this->addToMessage("non ok");
            }
        }
        $this->addToMessage(sprintf('%s radiations nouvelles.', $nouvelles_radiations));
        $this->addToMessage(sprintf('%s radiations existantes au 10/01/2019.', $radiations_existantes));
        $this->apply_mouvement_radiation($radiations_to_treat);
    }

    /**
     *
     */
    function handle_and_treat_inscription() {
        $inscriptions_to_treat = array();
        $nouvelles_inscriptions = 0;
        $electeurs_reu_non_rattaches = $this->get_electeurs_reu_non_rattaches();
        // Gestion du bureau unique
        // Si la commune n'a qu'un seul bureau stocke le code et l'id du bureau
        // openElec auquel on va rattacher toutes les nouvelles inscriptions
        $bureaux = $this->f->get_all__bureau__by_my_collectivite();
        $oo_bureau_id = null;
        $oo_bureau_code = null;
        if (count($bureaux) == 1) {
            $oo_bureau_id = $bureaux[0]['id'];
            $oo_bureau_code = $bureaux[0]['code'];
        }
        foreach ($electeurs_reu_non_rattaches as $key => $value) {
            $inst_inscription = $this->f->get_inst__om_dbform(array(
                "obj" => "inscription",
                "idx" => "]",
            ));
            //
            $code_departement_naissance = "";
            $libelle_departement_naissance = "";
            $code_lieu_de_naissance = "";
            $libelle_lieu_de_naissance = "";
            // Né à l'étrageer
            if ($value["code_commune_de_naissance"] == "") {
                $code_lieu_de_naissance = $value["code_pays_de_naissance"];
                if ($value["libelle_commune_de_naissance"] == "") {
                    $libelle_lieu_de_naissance = $value["libelle_pays_de_naissance"];
                } else {
                    $libelle_lieu_de_naissance = $value["libelle_commune_de_naissance"];
                }
                $code_departement_naissance = $value["code_pays_de_naissance"];
                $libelle_departement_naissance = $value["libelle_pays_de_naissance"];
            } else {// Né en france
                $libelle_lieu_de_naissance = $value["libelle_commune_de_naissance"];
                $code_lieu_de_naissance = $value["code_commune_de_naissance"];
                if ($this->f->starts_with($value["code_commune_de_naissance"], "98") === true
                    || $this->f->starts_with($value["code_commune_de_naissance"], "97") === true) {
                    $code_departement_naissance = substr($value["code_commune_de_naissance"], 0, 3);
                    $code_lieu_de_naissance = $code_departement_naissance." ".substr($value["code_commune_de_naissance"], 3, 2);
                } else {
                    $code_departement_naissance = substr($value["code_commune_de_naissance"], 0, 2);
                    $code_lieu_de_naissance = $code_departement_naissance." ".substr($value["code_commune_de_naissance"], 2, 3);
                }
                $libelle_departement_naissance = $this->f->db->getone(
                    sprintf(
                        'SELECT libelle_departement from %1$sdepartement where code=\'%2$s\'',
                        DB_PREFIXE,
                        $code_departement_naissance
                    )
                );
            }
            $naissance_type_saisie= $inst_inscription->get_naissance_type_saisie(
                $code_departement_naissance,
                $libelle_departement_naissance,
                $code_lieu_de_naissance,
                $libelle_lieu_de_naissance
            );
            //
            $date_naissance = $this->f->formatDate($value["date_de_naissance"], false);
            if ($date_naissance == false) {
                $date_naissance_elems = explode("/", $value["date_de_naissance"]);
                $date_naissance_elems[0] = str_replace("00", "01", $date_naissance_elems[0]);
                $date_naissance_elems[1] = str_replace("00", "01", $date_naissance_elems[1]);
                $date_naissance = implode("/", $date_naissance_elems);
            }
            //
            if ($oo_bureau_id === null) {
                $res = $this->f->db->query(
                    sprintf(
                        'SELECT id, code FROM %1$sbureau WHERE code::integer=%3$s AND om_collectivite=%2$s',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        intval($value["code_bureau_de_vote"])
                    )
                );
                $this->f->isDatabaseError($res);
                $row = $res->fetchRow(DB_FETCHMODE_ASSOC);
                $bureau = $row['id'];
            } else {
                $bureau = $oo_bureau_id;
            }
            //
            $adresse_resident = "";
            $complement_resident = "";
            $cp_resident = "";
            $ville_resident = "";
            //
            if (trim($value["numero_de_voie"]." ".$value["type_et_libelle_de_voie"]) != "") {
                $adresse_resident = trim($value["numero_de_voie"]." ".$value["type_et_libelle_de_voie"]);
                $complement_resident = trim($value["premier_complement_adresse"]." ".$value["second_complement_adresse"]);
            } elseif (trim($value["premier_complement_adresse"]." ".$value["second_complement_adresse"]) != "") {
                $adresse_resident = trim($value["premier_complement_adresse"]." ".$value["second_complement_adresse"]);
                $complement_resident = trim($value["lieu_dit"]);
            } elseif (trim($value["lieu_dit"]) != "") {
                $adresse_resident = trim($value["lieu_dit"]);
            } else {
                $adresse_resident = "-";
            }
            if (trim($value["code_postal"]) != "") {
                $cp_resident = trim($value["code_postal"]);
            } else {
                $cp_resident = "-";
            }
            $ville_resident = trim($value["libelle_de_commune"]);
            if (strtoupper(trim($value["libelle_du_pays"])) != "FRANCE") {
                $ville_resident .= " ".$value["libelle_du_pays"];
            }
            if (trim($ville_resident) == "") {
                $ville_resident = "-";
            }
            //
            $val = array(
                'electeur_id' => 0,
                'numero_bureau' => "",
                'bureau' => $bureau,
                'ancien_bureau' => "",
                'bureauforce' => "Oui",
                // liste et traitement
                'liste' => $value['liste'],
                'date_tableau' => '2024-12-31',
                'tableau' => "annuel",
                'etat' => "actif",
                // etat civil
                'civilite' => ($value["sexe"] == "M" ? "M." : "Mme"),
                'sexe' => $value["sexe"],
                'nom' => $value["nom"],
                'nom_usage' => $value["nom_d_usage"],
                'prenom' => ($value["prenoms"] != "" ? $value["prenoms"] : "-"),
                //naissance
                'date_naissance' => $date_naissance,
                'code_departement_naissance' => $code_departement_naissance,
                'libelle_departement_naissance' => $libelle_departement_naissance,
                'code_lieu_de_naissance' => $code_lieu_de_naissance,
                'libelle_lieu_de_naissance' => $libelle_lieu_de_naissance,
                'naissance_type_saisie' => $naissance_type_saisie,
                'code_nationalite' => $value["code_nationalite"],
                // adresse dans la commune
                'numero_habitation' => 0,
                'libelle_voie' => "NON COMMUNIQUEE",
                'code_voie' => "NC".intval($_SESSION["collectivite"]),
                'complement_numero' => "",
                'complement' => "",
                // provenance
                'provenance' => "",
                'libelle_provenance' => "",
                // recuperation adresse resident
                'resident' => "Oui",
                'adresse_resident' => $adresse_resident,
                'complement_resident' => $complement_resident,
                'cp_resident' => $cp_resident,
                'ville_resident' => $ville_resident,
                //
                'telephone' => "",
                'courriel' => "",
                //initialiser a vide
                'id' => '',
                'utilisateur' => '',
                'types' => '',
                'date_modif' => '',
                'envoi_cnen' => '',
                'date_cnen' => '',
                //
                'om_collectivite' => intval($_SESSION["collectivite"]),
                'date_j5' => '',
                'types' => 'SRI',
                'observation' => 'Inscription synchronisation REU',
                'ine' => intval($value["numero_d_electeur"]),
                'id_demande' => null,
                'visa' => null,
                'adresse_rattachement_reu' => null,
                'date_visa' => null,
                'date_complet' => null,
                'date_demande' => date("Y-m-d"),
                "statut" => null,
                'historique' => null,
                'archive_electeur' => null,
            );
            if ($naissance_type_saisie === "Etranger") {
                $val['live_pays_de_naissance'] = $code_departement_naissance;
            } elseif ($naissance_type_saisie === "Ancien_dep_franc") {
                $val['live_ancien_departement_francais_algerie'] = $code_departement_naissance;
            } elseif ($naissance_type_saisie === "France") {
                $val['live_commune_de_naissance'] = $code_lieu_de_naissance;
            }
            $ret = $inst_inscription->ajouter($val);
            if ($ret == true) {
                $nouvelles_inscriptions += 1;
                $inscriptions_to_treat[] = $inst_inscription->valF["id"];
            } else {
                $this->error = true;
                echo $inst_inscription->msg;
                $this->addToMessage("non ok");
            }
        }
        $this->addToMessage(sprintf('%s inscriptions nouvelles.', $nouvelles_inscriptions));
        $this->apply_mouvement_inscription($inscriptions_to_treat);
    }

    /**
     *
     */
    function prerequis() {
        // On vérifie si il existe des électeur à inscrire sans bureau de vote
        // si c'est le cas on n'empêche la validation du traitement
        $electeurs_reu_non_rattaches = $this->get_electeurs_reu_non_rattaches();
        foreach ($electeurs_reu_non_rattaches as $key => $value) {
            if ($value["code_bureau_de_vote"] == "" || $value["code_bureau_de_vote"] == null) {
                $this->addToMessage(__("Il y a des électeurs REU non rattachés à inscrire sans bureau de vote. Il faut leur affecter un bureau de vote avant de valider la synchronisation."));
                $this->error = true;
                return;
            }
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        $nationalites = $inst_reu->get_nationalites();
        foreach ($nationalites["result"] as $key => $value) {
            $query = sprintf(
                'INSERT INTO %1$snationalite(code, libelle_nationalite) SELECT \'%2$s\', \'%3$s\' WHERE NOT EXISTS (SELECT code FROM %1$snationalite WHERE code=\'%2$s\');',
                DB_PREFIXE,
                $value["code"],
                $value["libelle"]
            );
            $res = $this->f->db->query($query);
            $this->f->isDatabaseError($res);
        }
        //
        $query = sprintf(
            'INSERT INTO  %1$svoie(code, libelle_voie, cp, ville, abrege, om_collectivite)
            SELECT  \'NC%2$s\', \'NON COMMUNIQUEE\', \'\', \'\', \'\', %2$s
            WHERE NOT EXISTS (SELECT code FROM %1$svoie WHERE code=\'NC%2$s\' AND om_collectivite=%2$s);',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->query($query);
        $this->f->isDatabaseError($res);
        //
        $query = sprintf(
            'INSERT INTO  %1$sparam_mouvement(code, libelle, typecat, effet, cnen, codeinscription, coderadiation, edition_carte_electeur, insee_import_radiation, om_validite_debut, om_validite_fin)
            SELECT  \'SRI\', \'INSCRIPTION SYNCHRONISATION REU\', \'Inscription\', \'1erMars\', \'Non\', \'\', \'\', 0, NULL, NULL, NULL 
            WHERE NOT EXISTS (SELECT code FROM %1$sparam_mouvement WHERE code=\'SRI\');',
            DB_PREFIXE
        );
        $res = $this->f->db->query($query);
        $this->f->isDatabaseError($res);
        //
        $query = sprintf(
            'INSERT INTO  %1$sparam_mouvement(code, libelle, typecat, effet, cnen, codeinscription, coderadiation, edition_carte_electeur, insee_import_radiation, om_validite_debut, om_validite_fin)
            SELECT  \'SRR\', \'RADIATION SYNCHRONISATION REU\', \'Radiation\', \'1erMars\', \'Non\', \'\', \'\', 0, NULL, NULL, NULL 
            WHERE NOT EXISTS (SELECT code FROM %1$sparam_mouvement WHERE code=\'SRR\');',
            DB_PREFIXE
        );
        $res = $this->f->db->query($query);
        $this->f->isDatabaseError($res);
    }

    /**
     *
     */
    function treatment() {
        //
        $this->LogToFile("start reu_sync_l_e_valid");
        //
        $this->prerequis();
        if ($this->error === true) {
            $this->LogToFile("end reu_sync_l_e_valid");
            return;
        }
        //
        $this->handle_and_treat_radiation();
        $this->handle_and_treat_inscription();
        /**
         * Mise à jour du flag
         * Le traitement automatique a été fait
         */
        $ret = $this->f->insert_or_update_parameter("reu_sync_l_e_valid", "done");
        if ($ret !== true) {
            $this->error = true;
            $this->LogToFile("error lors de la création/mise à jour du flag");
            $this->LogToFile("end reu_sync_l_e_valid");
            return;
        }
        $this->LogToFile("création/mise à jour du flag ok");
        $this->addToMessage(sprintf(
            __("Cliquez ici pour rafraîchir les chiffres : %s"),
            '<a onclick="reload_my_tab(\'#traitement-tabs\');return false;" href="#">RAFRAÎCHIR</a>'
        ));
        //
        $this->LogToFile("end reu_sync_l_e_valid");
    }

    /**
     *
     */
    function displayBeforeContentForm() {
        printf(
            'radiations : %s / inscriptions : %s',
            $this->get_nb_electeurs_oel_non_rattaches(),
            $this->get_nb_electeurs_reu_non_rattaches()
        );
    }
}
