<?php
/**
 * Ce script définit la classe 'reuSyncLEREUEtatCivilTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_l_e.class.php";

/**
 * Définition de la classe 'reuSyncLEREUEtatCivilTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLEREUEtatCivilTraitement extends reuSyncLETraitement {
    /**
     * @var string
     */
    var $fichier = "reu_sync_l_e_reu_etat_civil";

    /**
     *
     */
    function treatment() {
        set_time_limit(180);
        $this->LogToFile("begin ".$this->fichier);
        //
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done") {
            $this->error = true;
            $message = __("Cet écran n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.");
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }

        /**
         *
         */
        $elems_to_treat = $this->get_electeurs_oel_diffetatcivil();
        if (is_array($elems_to_treat) === false) {
            $this->addToMessage(sprintf(
                '%s',
                __("Une erreur est survenue lors de la récupération des informations. Réessayez plus tard.")
            ));
            $this->LogToFile("Une erreur est survenue lors de la récupération des informations. Réessayez plus tard.");
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        $nb_elems_to_treat = count($elems_to_treat);
        $this->LogToFile(sprintf(
            __("Nombre d'électeurs à mettre à jour : %s"),
            $nb_elems_to_treat
        ));
        $nb_elems_valid = 0;
        foreach ($elems_to_treat as $key => $value) {
            $this->startTransaction();
            $inst_electeur = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => intval($value["id"]),
            ));
            if ($inst_electeur->exists() !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC L'électeur %s n'existe pas",
                    intval($value["id"])
                ));
                $this->rollbackTransaction();
                continue;
            }
            $ret = $inst_electeur->handle_modec(array(
                "reu_datas" => array(
                    "nom" => $value["reu_nom"],
                    "prenoms" => $value["reu_prenom"],
                    "sexe" => $value["reu_sexe"],
                    "date_de_naissance" => $value["reu_date_de_naissance"],
                    "code_commune_de_naissance" => $value["reu_clean_code_commune_de_naissance"],
                    "libelle_commune_de_naissance" => $value["reu_clean_libelle_commune_de_naissance"],
                    "code_pays_de_naissance" => $value["reu_clean_code_pays_de_naissance"],
                    "libelle_pays_de_naissance" => $value["reu_clean_libelle_pays_de_naissance"],
                ),
            ));
            if ($ret !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC MODEC de l'électeur %s échouée : %s",
                    intval($value["id"]),
                    $inst_electeur->msg
                ));
                $this->rollbackTransaction();
                continue;
            }
            $nb_elems_valid++;
            $this->commitTransaction();
            $this->LogToFile(sprintf(
                "Electeur %s - INE %s - MODEC avec succès",
                intval($value["id"]),
                $inst_electeur->getVal("ine")
            ));
        }
        //
        if ($nb_elems_to_treat != 0) {
            $message = sprintf(
                __('%1$s mise à jour(s) validée(s) sur un total de %2$s.'),
                $nb_elems_valid,
                $nb_elems_to_treat
            );
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        if ($nb_elems_to_treat == 0) {
            $message = __("Aucun électeur avec état civil différent à traiter.");
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        //
        $this->LogToFile("end ".$this->fichier);
    }
}
