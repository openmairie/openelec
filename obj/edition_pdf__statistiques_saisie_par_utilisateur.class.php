<?php
/**
 * Ce script définit la classe 'edition_pdf__statistiques_saisie_par_utilisateur'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__statistiques_saisie_par_utilisateur' (edition_pdf).
 */
class edition_pdf__statistiques_saisie_par_utilisateur extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * @return array
     */
    public function compute_pdf__statistiques_saisie_par_utilisateur($params = array()) {
        //
        require_once "fpdf.php";
        $aujourdhui = date("d/m/Y");
        $nolibliste = $_SESSION["libelle_liste"];
        // Saisie utilisateurs (Interface 1)
        $sqlsaisie0 = sprintf(
            'SELECT utilisateur, typecat FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.date_tableau=\'%2$s\' ORDER BY utilisateur',
            DB_PREFIXE,
            $this->f->getParameter("datetableau")
        );

        /////////////////////////// Requete Mouvement
        $mouvements = array();
        $resmov =& $this->f->db->query($sqlsaisie0);
        $this->f->isDatabaseError($resmov);
        while ($rowmov =& $resmov->fetchrow(DB_FETCHMODE_ASSOC))
            array_push ($mouvements, $rowmov);
        //////////////////////////

        $utilisateurs = array();
        //////////////////////////

        /**
         *
         */
        function pdf_utilisateur_entete($utils, $pdf, $aujourdhui) {
            $pdf->AddPage();
            // entete
            $pdf->SetFont('courier','B',11);
            $pdf->Cell(200,7,iconv(HTTPCHARSET,"CP1252",__('STATISTIQUE A PROPOS DES SAISIES UTILISATEURS SUR LE TABLEAU EN COURS DU ').$utils->formatDate($utils->getParameter("datetableau"))),'0',0,'L',0);
            $pdf->SetFont('courier','',11);
            $pdf->Cell(40,7, iconv(HTTPCHARSET,"CP1252",$aujourdhui),'0',0,'C',0);
            $pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",__(' Page  :  ').$pdf->PageNo()."/{nb} "),'0',1,'R',0);
            $pdf->ln();
            // Tableau
            $pdf->Cell(105,7,iconv(HTTPCHARSET,"CP1252",__('UTILISATEURS')),1,'0','C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",__('NOMBRES INSCRIPTIONS')),1,0,'C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",__('NOMBRES RADIATIONS')),1,0,'C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",__('NOMBRES MODIFICATIONS')),1,1,'C',0);
            return $pdf;
        }


        $pdf=new FPDF('L','mm','A4');
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(true);
        $pdf->SetFont('courier','',11);
        $pdf->SetDrawColor(30,7,146);
        $pdf->SetMargins(5,5,5);
        $pdf->SetDisplayMode('real','single');


        foreach( $mouvements as $mouvement  ){

            $existant = false;
            foreach ($utilisateurs as $utilisateur){
                if ($utilisateur[0] == $mouvement['utilisateur']){
                    $existant = true;
                    break;
                }
            }

            if($existant){
                $nbr_user = count($utilisateurs);
                $compteur = 0;
                for ($compteur = 0; $compteur < $nbr_user; $compteur++){
                    if ($utilisateurs[$compteur][0] == $mouvement['utilisateur']){
                        switch($mouvement['typecat']){
                            case "Inscription":
                                $utilisateurs[$compteur][1]++;
                                break;
                            case "Modification":
                                $utilisateurs[$compteur][2]++;
                                break;
                            case "Radiation":
                                $utilisateurs[$compteur][3]++;
                                break;
                        }
                        break;
                    }
                }
            }else{
                $ins = 0;
                $mod = 0;
                $rad = 0;
                switch($mouvement['typecat']){
                    case "Inscription":
                        $ins++;
                        break;
                    case "Modification":
                        $mod++;
                        break;
                    case "Radiation":
                        $rad++;
                        break;
                }
                $uarray = array($mouvement['utilisateur'],$ins,$mod,$rad);
                array_push ($utilisateurs,  $uarray);
            }

        }

        /// mise en tableau - mouvement utilisateurs
        $pdf = pdf_utilisateur_entete($this->f, $pdf, $aujourdhui);
        $nb_radiation = 0;
        $nb_modification = 0;
        $nb_inscription = 0;
        $cpt = 0;
        foreach ( $utilisateurs as $utilisateur){
            $cpt++;
            if($cpt >= 22){
                $cpt=1;
                $pdf = pdf_utilisateur_entete($this->f, $pdf, $aujourdhui);
            }

            $pdf->Cell(105,7,'    '.iconv(HTTPCHARSET,"CP1252",$utilisateur[0]),0,'0','L',0);
            $pdf->Cell(60,7,''.iconv(HTTPCHARSET,"CP1252",$utilisateur[1]),0,0,'C',0);
            $pdf->Cell(60,7,''.iconv(HTTPCHARSET,"CP1252",$utilisateur[3]),0,0,'C',0);
            $pdf->Cell(60,7,''.iconv(HTTPCHARSET,"CP1252",$utilisateur[2]),0,1,'C',0);
            $nb_radiation = $nb_radiation + $utilisateur[3];
            $nb_modification = $nb_modification + $utilisateur[2];
            $nb_inscription =  $nb_inscription + $utilisateur[1];
        }
            $pdf->Cell(105,7,iconv(HTTPCHARSET,"CP1252",__('TOTAL')),1,'0','C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$nb_inscription),1,0,'C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$nb_radiation),1,0,'C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$nb_modification),1,1,'C',0);


        /**
         * OUTPUT
         */
        //
        $filename = "statistiques_saisie_par_utilisateur-".date('Ymd-His').".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
