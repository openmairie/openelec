<?php
/**
 * Ce script définit la classe 'j5Traitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'j5Traitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class j5Traitement extends traitement {

    var $fichier = "j5";

    var $champs = array("liste", "mouvementatraiter", );

    function setContentForm() {
        //
        $this->form->setLib("mouvementatraiter", __("Le traitement J-5 applique le(s) tableau(x) selectionne(s) :"));
        $this->form->setType("mouvementatraiter", "checkbox_multiple");
        //
        $mouvements_io = sprintf(
            'SELECT code, libelle, effet FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE (effet=\'Election\' OR effet=\'Immediat\') AND date_tableau=\'%2$s\' AND etat=\'actif\' GROUP BY code, libelle, effet ORDER BY effet DESC, code',
            DB_PREFIXE,
            $this->f->getParameter("datetableau")
        );
        $res = $this->f->db->query($mouvements_io);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$mouvements_io."\")",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $contenu = array();
        $contenu[0] = array("Immediat",);
        $contenu[1] = array(__("Tableau des cinq jours"),);
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $this->f->addToLog(__METHOD__."(): ".print_r($row, true), EXTRA_VERBOSE_MODE);
            if ($row["effet"] == "Election") {
                array_push($contenu[0], $row["code"]);
                array_push($contenu[1], __("Tableau des additions")." - ".$row["libelle"]);
            }
        }
        $this->form->setSelect("mouvementatraiter", $contenu);
        //
        $cinqjours = false;
        $additions = array();
        $mouvementatraiter_input = null;
        $mouvementatraiter = "";
        if (isset($_POST["mouvementatraiter"])) {
            $mouvementatraiter_input = $_POST["mouvementatraiter"];
        }
        if ($mouvementatraiter_input != null) {
            foreach ($mouvementatraiter_input as $elem) {
                if ($elem == "Immediat") {
                    $cinqjours = true;
                } else {
                    $additions[] = $elem;
                }
                $mouvementatraiter .= $elem.";";
            }
        }
        $this->form->setVal("mouvementatraiter", $mouvementatraiter);

        $params = array(
            "cinqjours" => $cinqjours,
            "additions" => $additions,
            "mouvementatraiter" => $mouvementatraiter,
        );
        $this->setParams($params);
        //
        $this->form->setLib("liste", __("Le traitement J-5 s'applique sur la liste :"));
        $this->form->setType("liste", "statiq");
        $this->form->setVal("liste", $_SESSION["libelle_liste"]);

    }

    function getValidButtonValue() {
        //
        return __("Appliquer le traitement J-5");
    }

    function displayAfterContentForm() {
        //
        $cinqjours = $this->params["cinqjours"];
        $additions = $this->params["additions"];
        $mouvementatraiter = $this->params["mouvementatraiter"];

        // Format Date de tableau
        $datetableau = $this->f->getParameter("datetableau");

        /**
         * Cette requête permet de compter tous les électeurs de la table 'electeur' en
         * fonction de la collectivité en cours et de la liste en cours de l'utilisateur
         * connecté.
         *
         * @param string $_SESSION["collectivite"]
         * @param string $_SESSION["liste"]
         */
        $query_count_electeur = sprintf(
            'SELECT count(*) FROM %1$selecteur WHERE electeur.liste=\'%3$s\' AND electeur.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"]
        );

        // Inclusion du fichier de requêtes
        include "../sql/".OM_DB_PHPTYPE."/trt_j5.inc.php";

        //
        echo "<h4>";
        echo __("Recapitulatif du traitement :");
        echo "</h4>";

        // Debut Tableau
        echo "\n<table class='tabCol'>\n";

        // NB electeur avant traitement
        $nbElecteur = $this->f->db->getone($query_count_electeur);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$query_count_electeur."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($nbElecteur);
        echo "\t<tr class='tabCol'>";
        echo "<td class=\"link\" rowspan=\"5\">";
        echo "<div class=\"choice ui-corner-all ui-widget-content\">";
        echo "<span>";
        echo "<a id=\"action-traitement_j5-pdf-recapitulatif\" class=\"om-prev-icon edition-16\" ";
        echo "title=\"Ce document contient le detail par bureau des mouvements, le listing des ";
        echo "inscriptions, des modifications et des radiations a appliquer lors du traitement J-5. ";
        echo "Une fois le traitement applique, il est possible de retrouver ce recapitulatif dans le menu 'Edition -> Revision Electorale'.\" ";
        echo "target=\"_blank\" href=\"../app/index.php?module=form&obj=revision&action=302&mode_edition=traitement&traitement=j5&mouvementatraiter=".$mouvementatraiter."\">";
        echo "Cliquer ici pour visualiser le recapitulatif complet du traitement";
        echo "</a>";
        echo "</span>";
        echo "</div>";
        echo "</td>";
        echo "<td class=\"libelle\">Nombre d'electeurs avant traitement J-5</td>";
        echo "<td class=\"total\">".$nbElecteur."</td>";
        echo "</tr>\n";

        //  NB inscription
        $nbInscription = $this->f->db->getone ($query_count_inscription);
        $this->f->isDatabaseError($nbInscription);
        echo "\t<tr class='tabData'>";
        echo "<td class=\"libelle nb-electeur\">Inscription(s)</td>";
        echo "<td class=\"total\">".$nbInscription."</td>";
        echo "</tr>\n";

        // NB modification
        $nbModification = $this->f->db->getone ($query_count_modification);
        $this->f->isDatabaseError($nbModification);
        echo "\t<tr class='tabData'>";
        echo "<td class=\"libelle\">Modification(s)</td>";
        echo "<td class=\"total\">".$nbModification."</td>";
        echo "</tr>\n";

        // NB radiation
        $nbRadiation = $this->f->db->getone ($query_count_radiation);
        $this->f->isDatabaseError($nbRadiation);
        echo "\t<tr class='tabData'>";
        echo "<td class=\"libelle\">Radiation(s)</td>";
        echo "<td class=\"total\">".$nbRadiation."</td>";
        echo "</tr>\n";

        // NB electeur apres traitement
        $nbElecteurApres = $nbElecteur + $nbInscription - $nbRadiation;
        echo "\t<tr class='tabCol'>";
        echo "<td class=\"libelle\">Nombre d'electeurs apres traitement J-5</td>";
        echo "<td class=\"total\">".$nbElecteurApres."</td>";
        echo "</tr>\n";
        echo "</table>";
    }

    function treatment () {
        //
        $this->LogToFile("start j5");
        //
        $datetableau = $this->f->getParameter("datetableau");
        //
        $cinqjours = false;
        $additions = array();
        $mouvementatraiter_input = null;
        $mouvementatraiter = "";
        if (isset($_POST["mouvementatraiter"])) {
            $mouvementatraiter_input = $_POST["mouvementatraiter"];
        } elseif (isset($this->params["mouvementatraiter"])
            && is_array($this->params["mouvementatraiter"])) {
            $mouvementatraiter_input = $this->params["mouvementatraiter"];
        }
        if ($mouvementatraiter_input != null) {
            foreach ($mouvementatraiter_input as $elem) {
                if ($elem == "Immediat") {
                    $cinqjours = true;
                } else {
                    $additions[] = $elem;
                }
                $mouvementatraiter .= $elem.";";
            }
        }
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_j5.inc.php";
        // Traitement RADIATIONS
        $res_select_radiation = $this->f->db->query($query_select_radiation);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query_select_radiation."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_select_radiation, true)) {
            $this->error = true;
            $message = $res_select_radiation->getMessage()." erreur sur ".$query_select_radiation."";
            $this->LogToFile($message);
        } else {
            $this->LogToFile("TRAITEMENT DES RADIATIONS");
            $this->LogToFile($query_select_radiation);
            while ($row =& $res_select_radiation->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom'];
                // suppression ELECTEUR
                $enr = $this->f->get_inst__om_dbform(array(
                    "obj" => "electeur",
                    "idx" => $row['electeur_id'],
                ));
                $ret = $enr->supprimerTraitement();
                if ($ret !== true) {
                    $this->error = true;
                    $message .= "-> Erreur lors de la suppresion de l'électeur";
                    $this->LogToFile($message);
                    break;
                }
                $message .= " -> ".$enr->msg;
                $this->LogToFile($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'j5',
                    'date_j5' => ''.$enr->dateSystemeDB().'',
                );
                $cle = "id=".$row['id'];
                //
                $res1 = $this->f->db->autoexecute(
                    sprintf('%1$s%2$s', DB_PREFIXE, "mouvement"),
                    $fields_values,
                    DB_AUTOQUERY_UPDATE,
                    $cle
                );
                $this->f->addToLog(
                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "mouvement")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                    VERBOSE_MODE
                );
                if ($this->f->isDatabaseError($res1)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage()." - ".$res1->getUserInfo();
                    $this->LogToFile($message);
                    //
                    break;
                } else {
                    //
                    $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                    $this->LogToFile($message);
                }
            }
            $res_select_radiation->free();
        }

        // Traitement INSCRIPTIONS
        $res_select_inscription = $this->f->db->query($query_select_inscription);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query_select_inscription."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_select_inscription, true)) {
            $this->error = true;
            $message = $res_select_inscription->getMessage()." erreur sur ".$query_select_inscription."";
            $this->LogToFile($message);
        } else {
            $this->LogToFile("TRAITEMENT DES INSCRIPTIONS");
            $this->LogToFile($query_select_inscription);
            while ($row =& $res_select_inscription->fetchRow(DB_FETCHMODE_ASSOC)) {
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom'];
                // ajout ELECTEUR
                $enr = $this->f->get_inst__om_dbform(array(
                    "obj" => "electeur",
                    "idx" => "]",
                ));
                $enr->valF['tableau'] = 'j5';
                $ret = $enr->ajouterTraitement($row, $this->f->getParameter("datetableau"));
                if ($ret !== true) {
                    $this->error = true;
                    $message .= " -> Erreur lors de l'ajout de l'électeur";
                    $this->LogToFile($message);
                    break;
                }
                $message .= " -> ".$enr->msg;
                $this->LogToFile($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'j5',
                    'electeur_id' => $enr->valF["id"],
                    'date_j5' => ''.$enr->dateSystemeDB().'',
                );
                $cle = "id=".$row['id'];
                //
                $res1 = $this->f->db->autoexecute(
                    sprintf('%1$s%2$s', DB_PREFIXE, "mouvement"),
                    $fields_values,
                    DB_AUTOQUERY_UPDATE,
                    $cle
                );
                $this->f->addToLog(
                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "mouvement")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                    VERBOSE_MODE
                );
                //
                if ($this->f->isDatabaseError($res1, true) !== false) {
                    $this->error = true;
                    $message = $res1->getMessage()." - ".$res1->getUserInfo();
                    $this->LogToFile($message);
                    break;
                }
                //
                $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                $this->LogToFile($message);
            }
            $res_select_inscription->free();
        }

        // Traitement MODIFICATIONS
        $res_select_modification = $this->f->db->query($query_select_modification);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query_select_modification."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_select_modification, true)) {
            $this->error = true;
            $message = $res_select_modification->getMessage()." erreur sur ".$query_select_modification."";
            $this->LogToFile($message);
        } else {
            $this->LogToFile("TRAITEMENT DES MODIFICATIONS");
            $this->LogToFile($query_select_modification);
            while ($row =& $res_select_modification->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $message = "-> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom'];
                // maj ELECTEUR
                $enr = $this->f->get_inst__om_dbform(array(
                    "obj" => "electeur",
                    "idx" => $row['electeur_id'],
                ));
                $enr->valF['tableau'] = 'j5';
                $ret = $enr->modifierTraitement($row, $this->f->getParameter("datetableau"));
                if ($ret !== true) {
                    $this->error = true;
                    $message .= " -> Erreur lors de la mise à jour de l'électeur";
                    $this->LogToFile($message);
                    break;
                }
                $message .= " -> ".$enr->msg;
                $this->LogToFile($message);
                // maj MOUVEMENT
                $fields_values = array(
                    'etat'    => 'trs',
                    'tableau' => 'j5',
                    'date_j5' => ''.$enr->dateSystemeDB().'',
                );
                $cle = "id=".$row['id'];
                //
                $res1 = $this->f->db->autoexecute(
                    sprintf('%1$s%2$s', DB_PREFIXE, "mouvement"),
                    $fields_values,
                    DB_AUTOQUERY_UPDATE,
                    $cle
                );
                $this->f->addToLog(
                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "mouvement")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                    VERBOSE_MODE
                );
                if ($this->f->isDatabaseError($res1, true) !== false) {
                    $this->error = true;
                    $message = $res1->getMessage()." - ".$res1->getUserInfo();
                    $this->LogToFile($message);
                    break;
                }
                //
                $message = "l'enregistrement ".$row['id']." de la table Mouvement est modifie";
                $this->LogToFile($message);
            }
            $res_select_modification->free();
        }
        //
        $this->LogToFile("end j5");
    }

    /**
     * Statistiques - 'traitement_j5'.
     *
     * > mode_edition
     * - "traitement"
     *   > datetableau
     *   > traitement
     *   > mouvementatraiter
     * - "recapitulatif"
     *   > datetableau
     *   > traitement
     *   > datej5
     *
     * @return array
     */
    function compute_stats__traitement_j5($params) {
        /**
         *
         */
        //
        $error = false;
        //
        $mode_edition = "";
        if (isset($params["mode_edition"])) {
            $mode_edition = $params["mode_edition"];
        }
        //
        switch ($mode_edition) {
            // Appel depuis les modules de traitement
            // Le traitement n'a pas encore ete applique
            case "traitement":
                // La date de tableau à utiliser est la date de tableau passée en
                // paramètre ou sinon la date de tableau en cours
                (isset($params["datetableau"]) ? $datetableau = $params["datetableau"] : $datetableau = $this->f->getParameter("datetableau"));
                // Si c'est un traitement J5 alors on recupere les parametres des mouvements a traiter
                if (isset($params["traitement"]) && $params["traitement"] == "j5") {
                    //
                    $cinqjours = false;
                    $additions = array();
                    $mouvementatraiter_input = null;
                    $mouvementatraiter = "";
                    if (isset($params["mouvementatraiter"])) {
                        $mouvementatraiter_input = explode(";", $params["mouvementatraiter"]);
                    }
                    if ($mouvementatraiter_input != null) {
                        foreach ($mouvementatraiter_input as $elem) {
                            if ($elem == "Immediat") {
                                $cinqjours = true;
                            } else {
                                $additions[] = $elem;
                            }
                            $mouvementatraiter .= $elem.";";
                        }
                    }
                    $params["additions"] = $additions;
                    $params["cinqjours"] = $cinqjours;
                }
                //
                break;
            // Appel depuis le module d'edition revision electorale
            // Le traitement a deja ete applique
            case "recapitulatif":
                // La date de tableau a utiliser est passee en parametre
                (isset($params["datetableau"]) ? $datetableau = $params["datetableau"] : $error = true);
                // Si c'est un traitement J5 alors on recupere la date de traitement
                if (isset($params["traitement"]) && $params["traitement"] == "j5") {
                    //
                    (isset($params["datej5"]) ? $datej5 = $params["datej5"] : $error = true);
                }
                //
                break;
            //
            default:
                //
                $error = true;
        }
        //
        if ($error == true) {
            return array(
                "data" => array(),
            );
        }

        //
        $totalavant=0;
        $totalinscription=0;
        $totalmodification=0;
        $totalradiation=0;
        $totalapres=0;
        $totalarrive=0;
        $totaldepart=0;

        /**
         *
         */
        require_once "../obj/statistiques_electeur.class.php";
        $statistiques_electeur = new statistiques_electeur();


        if ($mode_edition == "recapitulatif") {
            //
            $from_unix_time = mktime(0, 0, 0, substr($datej5, 5, 2), substr($datej5, 8, 2), substr($datej5, 0, 4));
            $day_before = strtotime("yesterday", $from_unix_time);
            $previous_datej5 = date('Y-m-d', $day_before);
        }
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_j5.inc.php";
        //
        $data = array();
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
            //
            if ($mode_edition == "traitement") {
                //
                $sqlB = sprintf(
                    'SELECT count(electeur.id) FROM %1$selecteur WHERE electeur.liste=\'%3$s\' AND electeur.bureau=%4$s AND electeur.om_collectivite=%2$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $_SESSION["liste"],
                    intval($bureau["id"])
                );
                $avant = $this->f->db->getone($sqlB);
                $this->f->addToLog(
                    __METHOD__."(): db->getone(\"".$sqlB."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($avant);
            } else {
                //
                $avant = $statistiques_electeur->calculNombreDelecteursDateParBureau($previous_datej5, $bureau["code"]);
            }
            //
            $sqlI = "select count(mouvement.id) ";
            $sqlI .= $query_inscription;
            $sqlI .= " and mouvement.bureau_de_vote_code = '".$bureau["code"]."' ";
            $inscription =$this->f->db->getOne($sqlI);
            $this->f->isDatabaseError($inscription);
            //
            $sqlM1 = sprintf(
                'SELECT
                    count(mouvement.id)
                %1$s
                    AND mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
                    AND mouvement.bureau_de_vote_code=\'%2$s\'',
                $query_modification,
                $bureau["code"]
            );
            $arrive =$this->f->db->getOne($sqlM1);
            $this->f->isDatabaseError($arrive);
            //
            $sqlM0 = sprintf(
                'SELECT
                    count(mouvement.id)
                %1$s
                    AND mouvement.bureau_de_vote_code=mouvement.ancien_bureau_de_vote_code
                    AND mouvement.bureau_de_vote_code=\'%2$s\'',
                $query_modification,
                $bureau["code"]
            );
            $modification =$this->f->db->getOne($sqlM0);
            $this->f->isDatabaseError($modification);
            //
            $sqlM2 = sprintf(
                'SELECT
                    count(mouvement.id)
                %1$s
                    AND mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
                    AND mouvement.ancien_bureau_de_vote_code=\'%2$s\'',
                $query_modification,
                $bureau["code"]
            );
            $depart =$this->f->db->getOne($sqlM2);
            $this->f->isDatabaseError($depart);
            //
            $sqlR = "select count(mouvement.id) ";
            $sqlR .= $query_radiation;
            $sqlR .= " and mouvement.bureau_de_vote_code='".$bureau["code"]."' ";
            $radiation =$this->f->db->getOne($sqlR);
            $this->f->isDatabaseError($radiation);
            //
            $apres = $avant + $inscription + $arrive - $depart - $radiation;
            //
            $datas = array(
                $bureau["code"]." - ".$bureau["libelle"],
                $avant,
                $inscription,
                $arrive,
                $modification,
                $depart,
                $radiation,
                $apres
            );
            $data[] = $datas;
            //
            $totalavant += $avant;
            $totalinscription += $inscription;
            $totalarrive += $arrive;
            $totalmodification += $modification;
            $totaldepart += $depart;
            $totalradiation += $radiation;
            $totalapres += $apres;
        }
        //
        $datas = array(
            __("TOTAL"),
            $totalavant,
            $totalinscription,
            $totalarrive,
            $totalmodification,
            $totaldepart,
            $totalradiation,
            $totalapres
        );
        $data[] = $datas;

        //
        if ($mode_edition == "recapitulatif") {
            //
            $title = sprintf(__("Detail des mouvements appliques lors du traitement J-5 du %s[Tableau du %s]"),$this->f->formatdate($datej5),$this->f->formatdate($datetableau));
        } else {
            //
            $title = sprintf(__("Detail des mouvements a appliquer au traitement J-5 du %s [Tableau du %s]"),date("d/m/Y"),$this->f->formatdate($this->f->getParameter("datetableau")));
        }

        // Array
        return array(
            "format" => "L",
            "title" => $title,
            "offset" => array(110,0,0,0,0,0,0,0),
            "align" => array("L", "R", "R", "R", "R", "R", "R", "R"),
            "column" => array(
                __("Bureau(x) de vote"),
                __("Avant TRT"),
                __("+ Inscription(s)"),
                __("+ Transfert(s)"),
                __("Modification(s)"),
                __("- Transfert(s)"),
                __("- Radiation(s)"),
                __("Apres TRT"),
            ),
            "data" => $data,
            "output" => "stats-traitemenj5"
        );
    }
}
