<?php
/**
 * Ce script définit la classe 'juryEpurationTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'reuSyncLETraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLETraitement extends traitement {

    /**
     *
     */
    var $listes = array(
        array(
            "reu" => "LP",
            "oe" => "01",
        ),
        array(
            "reu" => "LCE",
            "oe" => "02",
        ),
        array(
            "reu" => "LCM",
            "oe" => "03",
        ),
    );

    /**
     *
     */
    function reload_reu_sync_listes() {
        //
        $inst_reu = $this->f->get_inst__reu();
        //
        $uid_lp = $inst_reu->get_liste_in_csv("LP");
        $uid_lcm = $inst_reu->get_liste_in_csv("LCM");
        $uid_lce = $inst_reu->get_liste_in_csv("LCE");
        //
        if ($uid_lce === null
            || $uid_lcm === null
            || $uid_lp === null) {
            $message = __("Erreur lors de la récupération des listes. Essayez plus tard.");
            $this->LogToFile($message);
            $this->addToMessage($message);
            $this->LogToFile("end reu_synchro_liste_electorale_init");
            $this->error = true;
            return false;
        }
        $sql = sprintf(
            'DELETE FROM %1$sreu_sync_listes WHERE code_ugle=\'%5$s\';
            COPY %1$sreu_sync_listes from \'%2$s\' DELIMITERS \';\' CSV HEADER;
            COPY %1$sreu_sync_listes from \'%3$s\' DELIMITERS \';\' CSV HEADER; 
            COPY %1$sreu_sync_listes from \'%4$s\' DELIMITERS \';\' CSV HEADER; ',
            DB_PREFIXE,
            realpath($this->f->storage->getPath_temporary($uid_lp)),
            realpath($this->f->storage->getPath_temporary($uid_lce)),
            realpath($this->f->storage->getPath_temporary($uid_lcm)),
            $inst_reu->get_ugle()
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        // Suppression des fichiers temporaires
        $ret = $this->f->storage->storage->temporary_storage->delete($uid_lce);
        $this->LogToFile("Suppression du fichier temporaire ".$ret);
        $ret = $this->f->storage->storage->temporary_storage->delete($uid_lcm);
        $this->LogToFile("Suppression du fichier temporaire ".$ret);
        $ret = $this->f->storage->storage->temporary_storage->delete($uid_lp);
        $this->LogToFile("Suppression du fichier temporaire ".$ret);
        //
        return true;
    }

    /**
     *
     */
    function reload_reu_sync_procurations() {
        //
        $inst_reu = $this->f->get_inst__reu();
        //
        $dtime = new DateTime("now");
        $uid = $inst_reu->get_procurations_in_csv();
        //
        if ($uid === null) {
            $message = __("Erreur lors de la récupération des procurations. Essayez plus tard.");
            $this->LogToFile($message);
            $this->addToMessage($message);
            $this->LogToFile("end reload_reu_sync_procurations");
            $this->error = true;
            return false;
        }
        $sql = sprintf(
            'DELETE FROM %1$sreu_sync_procurations WHERE om_collectivite=%3$s;
            DROP TABLE IF EXISTS %1$sreu_sync_procurations_temp_%3$s;
            CREATE TABLE %1$sreu_sync_procurations_temp_%3$s (
                id character varying(250),
                date_creation character varying(250),
                date_debut character varying(250),
                date_fin character varying(250),
                date_modification character varying(250),
                date_modification_etat character varying(250),
                lieu_etablissement_ugle character varying(250),
                nom_prenom_autorite character varying(250),
                autorite_etablissement character varying(250),
                type_localisation_code character varying(250),
                date_etablissement character varying(250),
                motif_annulation_office_code character varying(250),
                etat_procuration_code character varying(250),
                electeur_mandant character varying(250),
                electeur_mandataire character varying(250),
                procuration_validite_scrutin_code character varying(250),
                nom_naissance_mandant character varying(250),
                nom_usage_mandant character varying(250),
                prenom_mandant character varying(250),
                sexe_mandant character varying(250),
                date_naissance_mandant character varying(250),
                nom_naissance_mandataire character varying(250),
                nom_usage_mandataire character varying(250),
                prenom_mandataire character varying(250),
                sexe_mandataire character varying(250),
                date_naissance_mandataire character varying(250),
                mandataire_dans_ugle character varying(250),
                provenance character varying(250)
            );
            COPY %1$sreu_sync_procurations_temp_%3$s from \'%2$s\' DELIMITERS \';\' CSV HEADER;
            INSERT INTO %1$sreu_sync_procurations (
                id,
                date_creation,
                date_debut,
                date_fin,
                date_modification,
                date_modification_etat,
                lieu_etablissement_ugle,
                nom_prenom_autorite,
                autorite_etablissement,
                type_localisation_code,
                date_etablissement,
                motif_annulation_office_code,
                etat_procuration_code,
                electeur_mandant,
                electeur_mandataire,
                procuration_validite_scrutin_code,
                nom_naissance_mandant,
                nom_usage_mandant,
                prenom_mandant,
                sexe_mandant,
                date_naissance_mandant,
                nom_naissance_mandataire,
                nom_usage_mandataire,
                prenom_mandataire,
                sexe_mandataire,
                date_naissance_mandataire,
                mandataire_dans_ugle,
                provenance,
                om_collectivite
            ) SELECT
                id,
                date_creation,
                date_debut,
                date_fin,
                date_modification,
                date_modification_etat,
                lieu_etablissement_ugle,
                nom_prenom_autorite,
                autorite_etablissement,
                type_localisation_code,
                date_etablissement,
                motif_annulation_office_code,
                etat_procuration_code,
                electeur_mandant,
                electeur_mandataire,
                procuration_validite_scrutin_code,
                nom_naissance_mandant,
                nom_usage_mandant,
                prenom_mandant,
                sexe_mandant,
                date_naissance_mandant,
                nom_naissance_mandataire,
                nom_usage_mandataire,
                prenom_mandataire,
                sexe_mandataire,
                date_naissance_mandataire,
                mandataire_dans_ugle,
                provenance,
                %3$s
            FROM %1$sreu_sync_procurations_temp_%3$s;
            DROP TABLE IF EXISTS %1$sreu_sync_procurations_temp_%3$s;
            INSERT INTO %1$som_parametre (om_parametre, libelle, valeur, om_collectivite)
            SELECT nextval(\'%1$som_parametre_seq\'), \'reu_sync_procurations\', \'%4$s\', %3$s
            WHERE NOT EXISTS (SELECT om_parametre.om_parametre FROM %1$som_parametre WHERE om_collectivite=%3$s and libelle=\'reu_sync_procurations\');
            UPDATE %1$som_parametre SET valeur=\'%4$s\' WHERE om_collectivite=%3$s and libelle=\'reu_sync_procurations\';',
            DB_PREFIXE,
            realpath($this->f->storage->getPath_temporary($uid)),
            intval($_SESSION["collectivite"]),
            $dtime->format('d/m/Y H:i:s')
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        $ret = $this->f->storage->storage->temporary_storage->delete($uid);
        $this->LogToFile("Suppression du fichier temporaire ".$ret);
        //
        return true;
    }

    /**
     *
     */
    function get_nb_procurations_oel_total() {
        $nb_procurations_oel_total = $this->f->db->getone(
            sprintf(
                'select count(*) from %1$sprocuration WHERE om_collectivite=%2$s AND id_externe IS NOT NULL',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        if ($this->f->isDatabaseError($nb_procurations_oel_total, true) !== false) {
            return 0;
        }
        return intval($nb_procurations_oel_total);
    }

    /**
     *
     */
    function get_nb_procurations_reu_total() {
        $nb_procurations_reu_total = $this->f->db->getone(
            sprintf(
                'select count(*) from %1$sreu_sync_procurations WHERE om_collectivite=%2$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        if ($this->f->isDatabaseError($nb_procurations_reu_total, true) !== false) {
            return 0;
        }
        return intval($nb_procurations_reu_total);
    }

    /**
     *
     */
    function get_nb_electeurs_oel_total() {
        $nb_electeurs_oel_total = $this->f->db->getone(
            sprintf(
                'select count(*) from %1$selecteur WHERE om_collectivite=%2$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        if ($this->f->isDatabaseError($nb_electeurs_oel_total, true) !== false) {
            return 0;
        }
        return intval($nb_electeurs_oel_total);
    }

    /**
     *
     */
    function get_nb_electeurs_oel_non_rattaches() {
        $nb_electeurs_oel_non_rattaches = $this->f->db->getone(
            sprintf(
                'select count(*) from %1$selecteur where om_collectivite=%2$s AND ine is null',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        if ($this->f->isDatabaseError($nb_electeurs_oel_non_rattaches, true) !== false) {
            return 0;
        }
        return intval($nb_electeurs_oel_non_rattaches);
    }

    /**
     *
     */
    function get_nb_electeurs_reu_total() {
        $inst_reu = $this->f->get_inst__reu();
        $nb_electeurs_reu_total = $this->f->db->getone(
            sprintf(
                'select count(*) from %1$sreu_sync_listes WHERE code_ugle=\'%2$s\'',
                DB_PREFIXE,
                $inst_reu->get_ugle()
            )
        );
        if ($this->f->isDatabaseError($nb_electeurs_reu_total, true) !== false) {
            return 0;
        }
        return intval($nb_electeurs_reu_total);
    }

    /**
     *
     */
    function get_nb_electeurs_reu_non_rattaches() {
        $inst_reu = $this->f->get_inst__reu();
        $nb_electeurs_reu_non_rattaches = $this->f->db->getone(
            sprintf(
                'SELECT count(*) 
                FROM %1$sreu_sync_listes
                    INNER JOIN %1$sliste
                        ON reu_sync_listes.code_type_liste=liste.liste_insee
                    LEFT JOIN %1$selecteur
                        ON reu_sync_listes.numero_d_electeur::integer=electeur.ine
                            AND electeur.liste=liste.liste
                WHERE 
                    code_ugle=\'%2$s\'
                    AND electeur.ine IS NULL',
                DB_PREFIXE,
                $inst_reu->get_ugle()
            )
        );
        if ($this->f->isDatabaseError($nb_electeurs_reu_non_rattaches, true) !== false) {
            return 0;
        }
        return intval($nb_electeurs_reu_non_rattaches);
    }

    /**
     *
     */
    function get_electeurs_oel_non_rattaches() {    
        $electeurs_oel_non_rattaches = array();
        $res = $this->f->db->query(
            sprintf(
                'select * from %1$selecteur where om_collectivite=%2$s AND ine is null',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $electeurs_oel_non_rattaches[] = $row;
        }
        return $electeurs_oel_non_rattaches;
    }

    /**
     *
     */
    function get_electeurs_reu_non_rattaches() {
        $electeurs_reu_non_rattaches = array();
        $inst_reu = $this->f->get_inst__reu();
        $res = $this->f->db->query(
            sprintf(
                'SELECT reu_sync_listes.*, liste.*
                FROM %1$sreu_sync_listes
                    INNER JOIN %1$sliste
                        ON reu_sync_listes.code_type_liste=liste.liste_insee
                    LEFT JOIN %1$selecteur
                        ON reu_sync_listes.numero_d_electeur::integer=electeur.ine
                            AND electeur.liste=liste.liste
                WHERE 
                    code_ugle=\'%2$s\'
                    AND electeur.ine IS NULL',
                DB_PREFIXE,
                $inst_reu->get_ugle()
            )
        );
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $electeurs_reu_non_rattaches[] = $row;
        }
        return $electeurs_reu_non_rattaches;
    }

    /**
     *
     */
    function get_electeurs_oel_absents_in_reu() {
        $electeurs_oel_absents_in_reu = array();
        $inst_reu = $this->f->get_inst__reu();
        $res = $this->f->db->query(
            sprintf(
                'SELECT * FROM %1$selecteur INNER JOIN %1$sliste ON electeur.liste=liste.liste WHERE electeur.om_collectivite=%2$s AND electeur.ine NOT IN (SELECT reu_sync_listes.numero_d_electeur::integer FROM %1$sreu_sync_listes WHERE code_type_liste=liste.liste_insee AND code_ugle=\'%3$s\')',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $inst_reu->get_ugle()
            )
        );
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $electeurs_oel_absents_in_reu[] = $row;
        }
        return $electeurs_oel_absents_in_reu;
    }

    /**
     *
     */
    function get_procurations_oel_absents_in_reu() {
        $procurations_oel_absents_in_reu = array();
        $res = $this->f->db->query(
            sprintf(
                'SELECT * FROM %1$sprocuration WHERE procuration.om_collectivite=%2$s AND procuration.id_externe IS NOT NULL AND procuration.id_externe::integer NOT IN (SELECT reu_sync_procurations.id::integer FROM %1$sreu_sync_procurations WHERE reu_sync_procurations.om_collectivite=%2$s)',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $procurations_oel_absents_in_reu[] = $row;
        }
        return $procurations_oel_absents_in_reu;
    }

    /**
     *
     */
    function get_procurations_reu_absents_in_oel() {
        $procurations_reu_absents_in_oel = array();
        $res = $this->f->db->query(
            sprintf(
                'SELECT * FROM %1$sreu_sync_procurations WHERE reu_sync_procurations.om_collectivite=%2$s AND reu_sync_procurations.id::integer NOT IN (SELECT procuration.id_externe::integer FROM %1$sprocuration WHERE procuration.om_collectivite=%2$s)',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $procurations_reu_absents_in_oel[] = $row;
        }
        return $procurations_reu_absents_in_oel;
    }

    /**
     *
     */
    function get_electeurs_oel_diffetatcivil() {
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done") {
            return array();
        }
        $this->init_reu_sync_listes();
        $electeurs_oel_diffetatcivil = array();
        $inst_reu = $this->f->get_inst__reu();
        $res = $this->f->db->query(
            sprintf(
                'SELECT
                    electeur.id,
                    liste.liste_insee,
                    electeur.ine,
                    electeur.nom as oel_nom,
                    electeur.prenom as oel_prenom,
                    electeur.date_naissance as oel_date_naissance,
                    electeur.sexe as oel_sexe,
                    electeur.code_lieu_de_naissance as oel_code_lieu_de_naissance,
                    electeur.libelle_lieu_de_naissance as oel_libelle_lieu_de_naissance,
                    electeur.code_departement_naissance as oel_code_departement_naissance,
                    electeur.libelle_departement_naissance as oel_libelle_departement_naissance,
                    reu_sync_listes.nom as reu_nom,
                    reu_sync_listes.prenoms as reu_prenom,
                    reu_sync_listes.date_de_naissance as reu_date_de_naissance,
                    reu_sync_listes.sexe as reu_sexe,
                    reu_sync_listes.code_commune_de_naissance as reu_clean_code_commune_de_naissance,
                    reu_sync_listes.libelle_commune_de_naissance as reu_clean_libelle_commune_de_naissance,
                    reu_sync_listes.code_pays_de_naissance as reu_clean_code_pays_de_naissance,
                    reu_sync_listes.libelle_pays_de_naissance as reu_clean_libelle_pays_de_naissance
                FROM 
                    %1$selecteur 
                    INNER JOIN %1$sliste ON electeur.liste=liste.liste
                    INNER JOIN %1$sreu_sync_listes ON (
                        electeur.ine=reu_sync_listes.numero_d_electeur::integer
                        AND reu_sync_listes.code_ugle=\'%3$s\'
                        AND reu_sync_listes.code_type_liste=liste.liste_insee
                    )
                WHERE
                    electeur.om_collectivite=%2$s
                    ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $inst_reu->get_ugle()
            )
        );
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $reu_birth_place = $this->f->handle_birth_place(array(
                "code_commune_de_naissance" => $row["reu_clean_code_commune_de_naissance"],
                "libelle_commune_de_naissance" => $row["reu_clean_libelle_commune_de_naissance"],
                "code_pays_de_naissance" => $row["reu_clean_code_pays_de_naissance"],
                "libelle_pays_de_naissance" => $row["reu_clean_libelle_pays_de_naissance"],
            ));
            if ($row["oel_nom"] != $row["reu_nom"]
                || ($row["oel_prenom"] != $row["reu_prenom"] && $row["oel_prenom"] != "-" && $row["reu_prenom"] != "")
                || $row["oel_sexe"] != $row["reu_sexe"]
                || $row["oel_date_naissance"] != $this->f->handle_birth_date($row["reu_date_de_naissance"])
                || $row["oel_code_departement_naissance"] != $reu_birth_place["code_departement_naissance"]
                || $row["oel_libelle_departement_naissance"] != $reu_birth_place["libelle_departement_naissance"]
                || $row["oel_code_lieu_de_naissance"] != $reu_birth_place["code_lieu_de_naissance"]
                || $row["oel_libelle_lieu_de_naissance"] != $reu_birth_place["libelle_lieu_de_naissance"]) {
                //
                $electeurs_oel_diffetatcivil[] = $row;
            }
        }
        return $electeurs_oel_diffetatcivil;
    }

    /**
     *
     */
    function get_electeurs_oel_diffbureau() {
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done") {
            return array();
        }
        $this->init_reu_sync_listes();
        $inst_reu = $this->f->get_inst__reu();
        $res = $this->f->get_all_results_from_db_query(
            sprintf(
                'SELECT
                    electeur.id,
                    liste.liste_insee,
                    electeur.ine,
                    electeur.nom as oel_nom,
                    electeur.prenom as oel_prenom,
                    electeur.date_naissance as oel_date_naissance,
                    electeur.sexe as oel_sexe,
                    bureau.code as oel_bureau_de_vote_code,
                    bureau.referentiel_id as oel_bureau_de_vote_referentiel_id,
                    electeur.numero_bureau as oel_numero_bureau,
                    reu_sync_listes.nom as reu_nom,
                    reu_sync_listes.prenoms as reu_prenom,
                    reu_sync_listes.date_de_naissance as reu_date_de_naissance,
                    reu_sync_listes.sexe as reu_sexe,
                    reu_sync_listes.code_bureau_de_vote as reu_bureau_de_vote_code,
                    reu_sync_listes.id_bureau_de_vote as reu_bureau_de_vote_id,
                    reu_sync_listes.numero_d_ordre_dans_le_bureau_de_vote as reu_numero_bureau
                FROM 
                    %1$selecteur 
                    INNER JOIN %1$sliste ON electeur.liste=liste.liste
                    INNER JOIN %1$sbureau ON electeur.bureau=bureau.id
                    INNER JOIN %1$sreu_sync_listes ON (
                        electeur.ine=reu_sync_listes.numero_d_electeur::integer
                        AND reu_sync_listes.code_ugle=\'%3$s\'
                        AND reu_sync_listes.code_type_liste=liste.liste_insee
                    )
                WHERE
                    electeur.om_collectivite=%2$s
                    AND bureau.referentiel_id <> reu_sync_listes.id_bureau_de_vote::integer
                    ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $inst_reu->get_ugle()
            )
        );
        return $res["result"];
    }

    /**
     *
     */
    function get_electeurs_oel_diffnumeroordre() {
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done") {
            return array();
        }
        $this->init_reu_sync_listes();
        $inst_reu = $this->f->get_inst__reu();
        $res = $this->f->get_all_results_from_db_query(
            sprintf(
                'SELECT
                    electeur.id,
                    liste.liste_insee,
                    electeur.ine,
                    electeur.nom as oel_nom,
                    electeur.prenom as oel_prenom,
                    electeur.date_naissance as oel_date_naissance,
                    electeur.sexe as oel_sexe,
                    bureau.code as oel_bureau_de_vote_code,
                    bureau.referentiel_id as oel_bureau_de_vote_referentiel_id,
                    electeur.numero_bureau as oel_numero_bureau,
                    reu_sync_listes.nom as reu_nom,
                    reu_sync_listes.prenoms as reu_prenom,
                    reu_sync_listes.date_de_naissance as reu_date_de_naissance,
                    reu_sync_listes.sexe as reu_sexe,
                    reu_sync_listes.code_bureau_de_vote as reu_bureau_de_vote_code,
                    reu_sync_listes.id_bureau_de_vote as reu_bureau_de_vote_id,
                    reu_sync_listes.numero_d_ordre_dans_le_bureau_de_vote as reu_numero_bureau
                FROM 
                    %1$selecteur 
                    INNER JOIN %1$sliste ON electeur.liste=liste.liste
                    INNER JOIN %1$sbureau ON electeur.bureau=bureau.id
                    INNER JOIN %1$sreu_sync_listes ON (
                        electeur.ine=reu_sync_listes.numero_d_electeur::integer
                        AND reu_sync_listes.code_ugle=\'%3$s\'
                        AND reu_sync_listes.code_type_liste=liste.liste_insee
                    )
                WHERE
                    electeur.om_collectivite=%2$s
                    AND electeur.numero_bureau<>reu_sync_listes.numero_d_ordre_dans_le_bureau_de_vote::integer
                    ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $inst_reu->get_ugle()
            )
        );
        return $res["result"];
    }

    /**
     *
     */
    function init_reu_sync_listes() {
        //
        $inst_reu = $this->f->get_inst__reu();
        //
        $query = sprintf(
            'SELECT date_extraction FROM %1$sreu_sync_listes WHERE code_ugle=\'%2$s\' LIMIT 1',
            DB_PREFIXE,
            $inst_reu->get_ugle()
        );
        $date_extraction = $this->f->db->getone($query);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($date_extraction);
        $dtime = DateTime::createFromFormat('d/m/Y H:i:s', $date_extraction);
        if ($dtime === false
            || strtotime("+15 minutes", strtotime($dtime->format('Y-m-d H:i:s'))) < time()
            || strtotime($dtime->format('Y-m-d H:i:s')) > time()) {
            // Plus de 60 minutes
            $ret = $this->reload_reu_sync_listes();
            if ($ret !== true) {
                return;
            }
            //
            $query = sprintf(
                'SELECT date_extraction FROM %1$sreu_sync_listes WHERE code_ugle=\'%2$s\' LIMIT 1',
                DB_PREFIXE,
                $inst_reu->get_ugle()
            );
            $date_extraction = $this->f->db->getone($query);
            $this->f->addToLog(
                __METHOD__."(): db->query(\"".$query."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($date_extraction);
        }
        return $date_extraction;
    }


    /**
     *
     */
    function init_reu_sync_procurations() {
        //
        $inst_reu = $this->f->get_inst__reu();
        //
        $query = sprintf(
            'SELECT valeur FROM %1$som_parametre WHERE om_collectivite=%2$s AND libelle=\'reu_sync_procurations\' LIMIT 1',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $date_extraction = $this->f->db->getone($query);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($date_extraction);
        $dtime = DateTime::createFromFormat('d/m/Y H:i:s', $date_extraction);
        if ($dtime === false
            || strtotime("+15 minutes", strtotime($dtime->format('Y-m-d H:i:s'))) < time()
            || strtotime($dtime->format('Y-m-d H:i:s')) > time()) {
            // Plus de 15 minutes
            $ret = $this->reload_reu_sync_procurations();
            if ($ret !== true) {
                return;
            }
            //
            $query = sprintf(
                'SELECT valeur FROM %1$som_parametre WHERE om_collectivite=%2$s AND libelle=\'reu_sync_procurations\' LIMIT 1',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            );
            $date_extraction = $this->f->db->getone($query);
            $this->f->addToLog(
                __METHOD__."(): db->query(\"".$query."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($date_extraction);
        }
        return $date_extraction;
    }

    /**
     *
     */
    function get_electeurs_reu_a_transmettre() {
        $get_electeurs_reu_a_transmettre = array();
        $res = $this->f->db->query(
            sprintf(
                'SELECT
                    electeur.id,
                    liste.liste_insee,
                    electeur.ine,
                    electeur.nom,
                    electeur.prenom,
                    electeur.date_naissance
                FROM 
                    %1$selecteur 
                    INNER JOIN %1$sliste ON electeur.liste=liste.liste
                WHERE 
                    electeur.om_collectivite=%2$s
                    AND electeur.a_transmettre IS TRUE',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $get_electeurs_reu_a_transmettre[] = $row;
        }
        return $get_electeurs_reu_a_transmettre;
    }

    /**
     *
     */
    function get_electeurs_reu_sans_bureau() {
        $inst_reu = $this->f->get_inst__reu();
        $ret = $inst_reu->get_liste_electeurs_sans_bureau();
        $electeurs_reu_sans_bureau = array();
        foreach ($ret as $values) {
            if (isset($values['result']) === true
                && is_array($values['result']) === true) {
                //
                $electeurs_reu_sans_bureau = array_merge(
                    $electeurs_reu_sans_bureau,
                    $values["result"]
                );
            }
        }
        $ine_sans_bureau = array();
        foreach ($electeurs_reu_sans_bureau as $key => $value) {
            if (isset($value["numeroElecteur"])) {
                $ine_sans_bureau[] = $value["numeroElecteur"];
            } else {
                return null;
            }
        }
        $electeurs_oel_reu_sans_bureau = array();
        $res = $this->f->db->query(
            sprintf(
                'SELECT
                    electeur.id,
                    liste.liste_insee,
                    electeur.ine,
                    electeur.nom,
                    electeur.prenom,
                    electeur.date_naissance
                FROM 
                    %1$selecteur 
                    INNER JOIN %1$sliste ON electeur.liste=liste.liste
                WHERE 
                    electeur.om_collectivite=%2$s
                    %3$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                (count($ine_sans_bureau) != 0 ? "AND electeur.ine IN (".implode(", ", $ine_sans_bureau).")" : "AND 1=2")
            )
        );
        $this->f->isDatabaseError($res);
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $electeurs_oel_reu_sans_bureau[] = $row;
        }
        return $electeurs_oel_reu_sans_bureau;
    }
}
