<?php
/**
 * Ce script définit la classe 'om_logo'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."obj/om_logo.class.php";

/**
 * Définition de la classe 'om_logo' (om_dbform).
 */
class om_logo extends om_logo_core {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[1]["condition"] = "is_not_from_configuration";
        $this->class_actions[2]["condition"] = "is_not_from_configuration";
        // ACTION - 006 - activer
        //
        $this->class_actions[6] = array(
            "identifier" => "activer",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("activer"),
                "order" => 40,
                "class" => "lu-16",
            ),
            "view" => "formulaire",
            "method" => "activer",
            "button" => "activer",
            "permission_suffix" => "activer",
            "condition" => "is_not_actif",
        );
        // ACTION - 007 - desactiver
        //
        $this->class_actions[7] = array(
            "identifier" => "desactiver",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("desactiver"),
                "order" => 40,
                "class" => "nonlu-16",
            ),
            "view" => "formulaire",
            "method" => "desactiver",
            "button" => "desactiver",
            "permission_suffix" => "desactiver",
            "condition" => "is_actif",
        );
    }


    function is_from_configuration() {
        if ($this->getVal($this->clePrimaire) < 100000) {
            return true;
        }
        return false;
    }

    function is_not_from_configuration() {
        return !$this->is_from_configuration();
    }

    function is_actif() {
        if ($this->getVal("actif") === 't') {
            return true;
        }
        return false;
    }

    function is_not_actif() {
        return !$this->is_actif();
    }

    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        $form->setSelect("fichier", array(
            "constraint" => array(
                "extension" => ".png;",
            ),
        ));
    }

    /**
     * TREATMENT - activer.
     *
     * @return boolean
     */
    function activer($val = array(), &$dnu1 = null, $dnu2 = null) {
        $this->begin_treatment(__METHOD__);
        //
        $table = $this->table;
        $primary_key = $this->clePrimaire;
        //
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$table,
            array(
                'actif' => false,
            ),
            DB_AUTOQUERY_UPDATE,
            $table.".id='".$this->getVal('id')."'
            AND ".$table.".actif IS TRUE
            AND ".$table.".om_collectivite='".$this->getVal('om_collectivite')."'"
        );
        if ($this->f->isDatabaseError($res, true)) {
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$table,
            array(
                'actif' => true,
            ),
            DB_AUTOQUERY_UPDATE,
            $table.".".$primary_key."=".$this->getVal($this->clePrimaire).""
        );
        if ($this->f->isDatabaseError($res, true)) {
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $this->addToMessage(_("L'élément a été correctement activé."));
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - desactiver.
     *
     * @return boolean
     */
    function desactiver($val = array(), &$dnu1 = null, $dnu2 = null) {
        $this->begin_treatment(__METHOD__);
        //
        $table = $this->table;
        $primary_key = $this->clePrimaire;
        //
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$table,
            array(
                'actif' => false,
            ),
            DB_AUTOQUERY_UPDATE,
            $table.".".$primary_key."=".$this->getVal($this->clePrimaire).""
        );
        if ($this->f->isDatabaseError($res, true)) {
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $this->addToMessage(_("L'élément a été correctement desactivé."));
        // Return
        return $this->end_treatment(__METHOD__, true);
    }
}

