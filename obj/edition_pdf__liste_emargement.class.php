<?php
/**
 * Ce script définit la classe 'edition_pdf__liste_emargement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__liste_emargement' (edition_pdf).
 */
class edition_pdf__liste_emargement extends edition_pdf {

    function LogToFile($msg) {
        //logger::instance()->log_to_file("debug.log", $msg);
    }

    /**
     * 
     */
    protected function handle_procurations_from_openelec($params = array()) {
        $vars = array(
            "dateelection1",
            "dateelection2",
            "bureau_code",
            "scrutin",
            "scrutin_demande_id",
            "liste",
        );
        //
        $procuration_validite = " AND ";
        $procuration_validite .= " ( ";
        if (isset($params["dateelection1"]) === true) {
            $procuration_validite .= " (procuration.debut_validite <='".$params["dateelection1"]."' ";
            $procuration_validite .= " AND ";
            $procuration_validite .= " procuration.fin_validite >='".$params["dateelection1"]."') ";
        }
        if (isset($params["dateelection1"]) === true
            && isset($params["dateelection2"]) === true) {
            //
            $procuration_validite .= " OR ";
        }
        if (isset($params["dateelection2"]) === true) {
            $procuration_validite .= " (procuration.debut_validite <='".$params["dateelection2"]."' ";
            $procuration_validite .= " AND ";
            $procuration_validite .= " procuration.fin_validite >='".$params["dateelection2"]."') ";
        }
        $procuration_validite .= " ) ";
        // Récupération des procurations
        $sqlP = sprintf(
            'SELECT 
                procuration.id,
                procuration.mandant,
                procuration.mandataire,
                mandant.nom AS mandant_nom,
                mandant.prenom AS mandant_prenom,
                bureau_mandant.code AS mandant_bureau,
                mandataire.nom AS mandataire_nom,
                mandataire.prenom AS mandataire_prenom, 
                bureau_mandataire.code AS mandataire_bureau,
                procuration.debut_validite,
                procuration.fin_validite
            FROM
                %1$sprocuration
                INNER JOIN %1$selecteur AS mandant ON procuration.mandant=mandant.id 
                LEFT JOIN %1$sbureau AS bureau_mandant ON mandant.bureau=bureau_mandant.id
                LEFT JOIN %1$selecteur AS mandataire ON procuration.mandataire=mandataire.id
                LEFT JOIN %1$sbureau AS bureau_mandataire ON mandataire.bureau=bureau_mandataire.id
            WHERE
                procuration.statut = \'procuration_confirmee\'
                AND procuration.om_collectivite=%2$s
                AND bureau_mandant.code=\'%4$s\'
                %3$s
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $procuration_validite,
            $this->f->db->escapesimple($params["bureau_code"])
        );
        $res = $this->f->db->query($sqlP);
        //
        if ($this->f->isDatabaseError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->addToLog($message, DEBUG_MODE);
            //
            //$this->addToMessage(__("Contactez votre administrateur."));
        } else {
            //
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                if ($this->error == false) {
                    //
                    $msg = "> ".__("Procuration")." ".$row['id']." ".$row['mandant_nom']." -> ".$row['mandataire_nom'];
                    $this->LogToFile($msg);
                    //
                    $validite = ($row["debut_validite"]!=$row["fin_validite"] ? "-".$this->f->formatdate($row["fin_validite"])."" : "");
                    //
                    $mandant = "- Mandataire ".addslashes($row['mandataire_nom'])." ".addslashes($row['mandataire_prenom'])." (".addslashes($row['mandataire_bureau']).") [".$this->f->formatdate($row["debut_validite"]).$validite."]";
                    $this->LogToFile($mandant);
                    $mandataire = "- Mandant ".addslashes($row['mandant_nom'])." ".addslashes($row['mandant_prenom'])." (".addslashes($row['mandant_bureau']).") [".$this->f->formatdate($row["debut_validite"]).$validite."]";
                    $this->LogToFile($mandataire);
                    // Mandant
                    $cle_mandant = sprintf(
                        ' electeur_id=%5$s
                        AND reu_livrable.demande_id=%4$s
                        AND reu_livrable.scrutin_id=%3$s
                        AND reu_livrable.liste=\'%2$s\'
                        AND reu_livrable.om_collectivite=%1$s ',
                        intval($_SESSION["collectivite"]),
                        $params["liste"],
                        $params["scrutin"],
                        $params["scrutin_demande_id"],
                        intval($row['mandant'])
                    );
                    $sql = sprintf(
                        'SELECT reu_livrable.procuration FROM %1$sreu_livrable WHERE %2$s',
                        DB_PREFIXE,
                        $cle_mandant
                    );
                    $procuration_mandant = $this->f->db->getone($sql);
                    $this->f->addToLog(
                        __METHOD__."(): db->getone(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    if ($this->f->isDatabaseError($procuration_mandant, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $procuration_mandant->getMessage()." - ".$procuration_mandant->getUserInfo();
                        $this->addToLog($message, DEBUG_MODE);
                        //
                        //$this->addToMessage(__("Contactez votre administrateur."));
                    }
                    //
                    $cle_mandataire = sprintf(
                        ' electeur_id=%5$s
                        AND reu_livrable.demande_id=%4$s
                        AND reu_livrable.scrutin_id=%3$s
                        AND reu_livrable.liste=\'%2$s\'
                        AND reu_livrable.om_collectivite=%1$s ',
                        intval($_SESSION["collectivite"]),
                        $params["liste"],
                        $params["scrutin"],
                        $params["scrutin_demande_id"],
                        intval($row['mandataire'])
                    );
                    $sql = sprintf(
                        'SELECT reu_livrable.procuration FROM %1$sreu_livrable WHERE %2$s',
                        DB_PREFIXE,
                        $cle_mandataire
                    );
                    $procuration_mandataire = $this->f->db->getone($sql);
                    $this->f->addToLog(
                        __METHOD__."(): db->getone(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    if ($this->f->isDatabaseError($procuration_mandataire, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $procuration_mandataire->getMessage()." - ".$procuration_mandataire->getUserInfo();
                        $this->addToLog($message, DEBUG_MODE);
                        //
                        //$this->addToMessage(__("Contactez votre administrateur."));
                    }
                    //
                    if ($this->error == false) {
                        //
                        $valF = array("procuration" => ($procuration_mandant != "" ? $procuration_mandant." " : "").$mandant);
                        //
                        $res1 = $this->f->db->autoexecute(
                            sprintf('%1$s%2$s', DB_PREFIXE, "reu_livrable"),
                            $valF,
                            DB_AUTOQUERY_UPDATE,
                            $cle_mandant
                        );
                        $this->f->addToLog(
                            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "reu_livrable")."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle_mandant."\");",
                            VERBOSE_MODE
                        );
                        //
                        if ($this->f->isDatabaseError($res1, true)) {
                            //
                            $this->error = true;
                            //
                            $message = $res1->getMessage()." - ".$res1->getUserInfo();
                            $this->addToLog($message, DEBUG_MODE);
                            //
                            //$this->addToMessage(__("Contactez votre administrateur."));
                        }
                        //
                        if ($this->error == false) {
                            // Mandataire
                            //
                            $valF = array("procuration" => ($procuration_mandataire != "" ? $procuration_mandataire." " : "").$mandataire);
                            //
                            $res1 = $this->f->db->autoexecute(
                                sprintf('%1$s%2$s', DB_PREFIXE, "reu_livrable"),
                                $valF,
                                DB_AUTOQUERY_UPDATE,
                                $cle_mandataire
                            );
                            $this->f->addToLog(
                                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "electeur")."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle_mandataire."\");",
                                VERBOSE_MODE
                            );
                            //
                            if ($this->f->isDatabaseError($res1, true)) {
                                //
                                $this->error = true;
                                //
                                $message = $res1->getMessage()." - ".$res1->getUserInfo();
                                $this->addToLog($message, DEBUG_MODE);
                                //
                                //$this->addToMessage(__("Contactez votre administrateur."));
                            }
                        }
                    } else {
                        $msg = "NON APPLIQUEE car mention CV ou ME";
                        $this->LogToFile($msg);
                    }
                }
            }
        }
    }

    /**
     * Édition PDF - Liste d'émargement.
     *
     * > mode_edition
     *  - "parbureau"
     *    > bureau_code
     *
     * @return array
     */
    public function compute_pdf__liste_emargement($params = array()) {
        //
        $libelle_commune = $this->f->collectivite["ville"];
        $liste = $_SESSION["liste"];
        $libelle_liste = $_SESSION["libelle_liste"];
        $titre_libelle_ligne1 = __("LISTE D'ÉMARGEMENT");
        $titre_libelle_ligne2 = "";
        $bureau_libelle = "";
        $canton_libelle = "";
        $circonscription_libelle = "";
        $message = "";
        $tour = "les_deux";
        $libelle_scrutin = "";
        $bloc_arrete_nb_emargements = null;
        //
        $mode_edition = "";
        if (isset($params["mode_edition"])) {
            if ($params["mode_edition"] == "parbureau") {
                $mode_edition = "parbureau";
                if (array_key_exists("liste", $params) === true) {
                    $liste = $params["liste"];
                    $ret = $this->f->get_one_result_from_db_query(sprintf(
                        'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                        DB_PREFIXE,
                        $this->f->db->escapesimple($liste)
                    ));
                    $libelle_liste = $ret["result"];
                }
                if (isset($params["bureau_code"])) {
                    $bureau_code = $params["bureau_code"];
                    $message = sprintf(
                        __("%s\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),
                        $titre_libelle_ligne1,
                        $libelle_liste,
                        $bureau_code
                    );
                    $sql = sprintf(
                        'SELECT bureau.id FROM %1$sbureau WHERE bureau.om_collectivite=%2$s AND bureau.code=\'%3$s\'',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        $this->f->db->escapesimple($bureau_code)
                    );
                    $bureau_id = $this->f->db->getone($sql);
                    $this->f->isDatabaseError($bureau_id);
                    //
                    $inst_bureau = $this->f->get_inst__om_dbform(array(
                        "obj" => "bureau",
                        "idx" => $bureau_id,
                    ));
                    $bureau_libelle = $inst_bureau->getVal("libelle");
                    //
                    $inst_canton = $this->f->get_inst__om_dbform(array(
                        "obj" => "canton",
                        "idx" => $inst_bureau->getVal("canton"),
                    ));
                    $canton_libelle = $inst_canton->getVal("libelle");
                    //
                    $inst_circonscription = $this->f->get_inst__om_dbform(array(
                        "obj" => "circonscription",
                        "idx" => $inst_bureau->getVal("circonscription"),
                    ));
                    $circonscription_libelle = $inst_circonscription->getVal("libelle");
                }
                if (array_key_exists("scrutin", $params) === true) {
                    $titre_libelle_ligne2 = sprintf(
                        '(%s)(%s)(%s)',
                        $params["scrutin"],
                        $params["scrutin_demande_id"],
                        $params["scrutin_livrable_date"]
                    );
                    //
                    if (isset($params["tour"])) {
                        $tour = $params["tour"];
                    }
                    //
                    if (isset($params["bloc_arrete_nb_emargements"])) {
                        $bloc_arrete_nb_emargements = $params["bloc_arrete_nb_emargements"];
                    }
                    if ($params["scrutin_tour1"] == $params["scrutin_tour2"]) {
                        $tour = "un";
                        $libelle_scrutin = sprintf(
                            '%1$s du %2$s',
                            $params["scrutin_libelle"],
                            $this->f->formatdate($params["scrutin_tour1"])
                        );
                    } else {
                        $libelle_scrutin = sprintf(
                            '%1$s des %2$s et %3$s%4$s',
                            $params["scrutin_libelle"],
                            $this->f->formatdate($params["scrutin_tour1"]),
                            $this->f->formatdate($params["scrutin_tour2"]),
                            ($tour !== "les_deux" ? sprintf(
                                ' (Tour %s)',
                                ($tour === "un" ? "1" : "2")
                            ) : "")
                        );
                    }
                    if ($tour == "les_deux") {
                        $dateelection1 = $params["scrutin_tour1"];
                        $dateelection2 = $params["scrutin_tour2"];
                    } elseif ($tour == "un") {
                        $dateelection1 = $params["scrutin_tour1"];
                    } elseif ($tour == "deux") {
                        $dateelection1 = $params["scrutin_tour2"];
                    }
                }
            }
        }
        //
        $is_option_tri_liste_emargement_alpha = $this->f->is_option_tri_liste_emargement_alpha();

        /**
         *
         */
        //
        $sql = " SELECT ";
        $sql .= " (nom || ' - ' || prenom) AS nomprenom,";
        $sql .= " nom_usage, ";
        $sql .= " to_char(date_naissance,'DD/MM/YYYY') AS naissance, ";
        $sql .= " (substring(libelle_lieu_de_naissance FROM 0 for 29) || ' (' || code_departement_naissance || ')') AS lieu, ";

        if ($liste == "01") {
            //
            $sql .= " CASE ";
                // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
                // alors on affiche la premiere ligne de la mention CENTRE VOTE
                $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
                $sql .= " substring(procuration FROM 55 for 68) ";
                // La mention CENTRE VOTE seule est a inscrire
                // alors on affiche la premiere ligne de la mention CENTRE VOTE
                $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
                $sql .= " substring(procuration FROM 1 for 68) ";
                //
                $sql .= " ELSE ";
                $sql .= " '' ";
            //
            $sql .= " END AS colonne1_ligne4,";
            //
            $sql .= " CASE ";
                // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
                // alors on affiche la deuxieme ligne de la mention CENTRE VOTE
                $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
                $sql .= " substring(procuration FROM 123 for 54) ";
                // La mention CENTRE VOTE seule est a inscrire
                // alors on affiche la deuxieme ligne de la mention CENTRE VOTE
                $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
                $sql .= " substring(procuration FROM 69 for 54) ";
                // La mention MAIRIE EUROPE seule est a inscrire
                // alors on affiche la ligne de la mention MAIRIE EUROPE
                $sql .= " WHEN position('***|' in procuration) <> 0 THEN ";
                $sql .= " substring(procuration FROM 1 for 52) ";
                //
                $sql .= " ELSE ";
                $sql .= " '' ";
            //
            $sql .= " END AS colonne1_ligne5,";
        } else {
            $sql .= " libelle_nationalite AS colonne1_ligne4,";
            $sql .= " '' AS colonne1_ligne5,";
        }
        //
        $sql .= " CASE resident ";
            $sql .= " WHEN 'Non' THEN (";
            $sql .= " CASE numero_habitation > 0 ";
                    $sql .= " WHEN True THEN (numero_habitation || ' ' || complement_numero || ' ' || libelle_voie) ";
                    $sql .= " WHEN False THEN libelle_voie ";
            $sql .= " END) ";
            $sql .= " WHEN 'Oui' THEN adresse_resident ";
        $sql .= " END AS adresse, ";
        $sql .= " CASE resident ";
            $sql .= " WHEN 'Non' THEN complement ";
            $sql .= " WHEN 'Oui' THEN (complement_resident || ' ' || cp_resident || ' - ' || ville_resident) ";
        $sql .= " END AS complement, ";
        //
        $sql .= " CASE ";
            // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
            // alors on affiche la ligne de la mention MAIRIE EUROPE
            $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
            $sql .= " substring(procuration FROM 1 for 52) ";
            // La mention CENTRE VOTE seule est a inscrire
            // alors on affiche la premiere ligne des PROCURATIONS
            $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
            $sql .= " substring(procuration FROM 125 for 65) ";
            // La mention MAIRIE EUROPE seule est a inscrire
            // alors on affiche la premiere ligne des PROCURATIONS
            $sql .= " WHEN position('***|' in procuration) <> 0 THEN ";
            $sql .= " substring(procuration FROM 55 for 65) ";
            // SINON on affiche la premiere ligne des PROCURATIONS
            $sql .= " ELSE ";
            $sql .= " substring(procuration FROM 1 for 65) ";
        //
        $sql .= " END AS colonne2_ligne3, ";
        //
        $sql .= " CASE ";
            // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
            // alors on affiche la premiere ligne des PROCURATIONS
            $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
            $sql .= " substring(procuration FROM 179 for 65) ";
            // La mention CENTRE VOTE seule est a inscrire
            // alors on affiche la premiere ligne des PROCURATIONS
            $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
            $sql .= " substring(procuration FROM 193 for 65) ";
            // La mention MAIRIE EUROPE seule est a inscrire
            // alors on affiche la premiere ligne des PROCURATIONS
            $sql .= " WHEN position('***|' in procuration) <> 0 THEN ";
            $sql .= " substring(procuration FROM 120 for 65) ";
            // SINON on affiche la deuxieme ligne des PROCURATIONS
            $sql .= " ELSE ";
            $sql .= " substring(procuration FROM 66 for 65) ";
        //
        $sql .= " END AS colonne2_ligne4, ";
        //
        $sql .= " CASE ";
            // Les deux mentions (CENTRE VOTE & MAIRIE EUROPE) sont a inscrire
            // alors on affiche la deuxieme ligne des PROCURATIONS
            $sql .= " WHEN position('***| ***' in procuration) <> 0 THEN ";
            $sql .= " substring(procuration FROM 244 for 65) ";
            // La mention CENTRE VOTE seule est a inscrire
            // alors on affiche la premiere ligne des PROCURATIONS
            $sql .= " WHEN position('***/' in procuration) <> 0 THEN ";
            $sql .= " substring(procuration FROM 193 for 65) ";
            // La mention MAIRIE EUROPE seule est a inscrire
            // alors on affiche la deuxiÃšme ligne des PROCURATIONS
            $sql .= " WHEN position('***|' in procuration) <> 0 THEN ";
            $sql .= " substring(procuration FROM 185 for 65) ";
            // SINON on affiche la troixieme ligne des PROCURATIONS
            $sql .= " ELSE ";
            $sql .= " substring(procuration FROM 131 for 65) ";
        //
        $sql .= " END AS colonne2_ligne5, ";
        // OPTION - Double Ã©margement
        // Quel est l'interet d'avoir l'id_electeur dans la liste d'Ã©margement lors du double Ã©margement
        // alors que cet identifiant n'apparait pas ?
        if ($tour == "les_deux") {
            $sql .= " ' ' as tour2, ";
            $sql .= " ' ' as tour1, ";
        }
        $sql .= " ' ' as emargement, ";
        $sql .= " ' ' as emargement_ligne1, ";
        $sql .= " ' ' as emargement_ligne2, ";
        $sql .= " ' ' as emargement_ligne3, ";
        $sql .= " ' ' as emargement_ligne4, ";
        $sql .= " ' ' as emargement_ligne5 ";
        //
        $sql .= sprintf(
            ' FROM %1$selecteur 
            LEFT JOIN %1$snationalite ON electeur.code_nationalite=nationalite.code 
            LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id ',
            DB_PREFIXE
        );
        //
        $sql .= " WHERE ";
        $sql .= " electeur.liste='".$liste."' ";
        $sql .= " AND electeur.om_collectivite=".intval($_SESSION["collectivite"])." ";
        //
        if ($mode_edition == "parbureau") {
            $sql .= " AND bureau.code='".$this->f->db->escapesimple($bureau_code)."' ";
        }
        // Tri
        if (isset($is_option_tri_liste_emargement_alpha) && $is_option_tri_liste_emargement_alpha === true) {
            $sql .= " ORDER BY withoutaccent(lower(nom)), withoutaccent(lower(prenom)), numero_bureau ";
        } else {
            $sql .= " ORDER BY numero_bureau";
        }

        /**
         *
         */
        if (array_key_exists("scrutin", $params) === true) {
            // On vide tous les champs procuration du livrable
            $valF = array(
                "procuration" => "",
            );
            $cle = sprintf(
                ' reu_livrable.demande_id=%4$s
                AND reu_livrable.scrutin_id=%3$s
                AND reu_livrable.liste=\'%2$s\'
                AND reu_livrable.om_collectivite=%1$s ',
                intval($_SESSION["collectivite"]),
                $liste,
                $params["scrutin"],
                $params["scrutin_demande_id"]
            );
            $res1 = $this->f->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, "reu_livrable"),
                $valF,
                DB_AUTOQUERY_UPDATE,
                $cle
            );
            $this->f->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "electeur")."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res1);
            // On positionne la mention
            $mentions = array(
                "1" => array(
                    "mention" => "*** ne vote pas au premier tour ***|1",
                    "tour" => "un",
                ),
                "2" => array(
                    "mention" => "*** ne vote pas dans la commune ***|-",
                    "tour" => "les_deux",
                ),
            );
            foreach ($mentions as $key => $value) {
                if ($value["tour"] === "un" && $tour == "deux") {
                    continue;
                }
                if ($value["tour"] === "deux" && $tour == "un") {
                    continue;
                }
                $table = "reu_livrable";
                $valF = array(
                    "procuration" => $value["mention"],
                );
                $cle = sprintf(
                    ' reu_livrable.demande_id=%4$s
                    AND reu_livrable.scrutin_id=%3$s
                    AND reu_livrable.liste=\'%2$s\'
                    AND reu_livrable.om_collectivite=%1$s 
                    AND reu_livrable.mention_code=\'%5$s\'
                    AND reu_livrable.code_du_bureau_de_vote=\'%6$s\' ',
                    intval($_SESSION["collectivite"]),
                    $liste,
                    $params["scrutin"],
                    $params["scrutin_demande_id"],
                    $key,
                    $this->f->db->escapesimple($bureau_code)
                );
                $res1 = $this->f->db->autoexecute(
                    sprintf('%1$s%2$s', DB_PREFIXE, $table),
                    $valF,
                    DB_AUTOQUERY_UPDATE,
                    $cle
                );
                $this->f->addToLog(
                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $table)."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res1);
            }
            $this->error = false;
            //
/*            $this->handle_procurations_from_openelec(array(
                "dateelection1" => (isset($dateelection1) === true ? $dateelection1 : null),
                "dateelection2" => (isset($dateelection2) === true ? $dateelection1 : null),
                "liste" => $liste,
                "bureau_code" => $bureau_code,
                "scrutin" => $params["scrutin"],
                "scrutin_demande_id" => $params["scrutin_demande_id"],
            ));*/

            // OPTION - Double Ã©margement
            // Quel est l'interet d'avoir l'id_electeur dans la liste d'Ã©margement lors du double Ã©margement
            // alors que cet identifiant n'apparait pas ?
            $double_emargement = "";
            if ($tour === "les_deux") {
                $double_emargement = sprintf('
                CASE 
                    WHEN position(\'***|2\' in reu_livrable.procuration) <> 0 
                    THEN
                        substring(reu_livrable.procuration FROM 1 for 35)
                    WHEN reu_livrable.nom_naissance_mandataire_tour2 <> \'\'
                    THEN
                        \'(procuration)\'
                    ELSE \'\'
                END as tour2, 
                CASE
                    WHEN position(\'***|1\' in reu_livrable.procuration) <> 0 
                    THEN
                        substring(reu_livrable.procuration FROM 1 for 35)
                    WHEN reu_livrable.nom_naissance_mandataire_tour1 <> \'\'
                    THEN
                        \'(procuration)\'
                    ELSE \'\'
                END as tour1, 
                \'\' as emargement,
                \'\' as emargement_ligne1,
                CASE
                    WHEN position(\'***|-\' in reu_livrable.procuration) <> 0 
                    THEN 
                        substring(reu_livrable.procuration FROM 1 for 35)
                    ELSE \'\'
                END AS emargement_ligne2,
                \'\' as emargement_ligne3,
                \'\' as emargement_ligne4,
                \'\' as emargement_ligne5');
            } else {
                if ($tour == "un") {
                    $double_emargement = sprintf(
                        '
                        \'\' as emargement,
                        \'\' as emargement_ligne1,
                        CASE
                            WHEN position(\'***|\' in reu_livrable.procuration) <> 0 
                            THEN 
                                substring(reu_livrable.procuration FROM 1 for 35)
                            WHEN reu_livrable.nom_naissance_mandataire_tour1 <> \'\'
                            THEN
                                \'(procuration)\'
                            ELSE \'\'
                        END AS emargement_ligne2,
                        \'\' as emargement_ligne3,
                        \'\' as emargement_ligne4,
                        \'\' as emargement_ligne5'
                    );
                } elseif ($tour == "deux") {
                    $double_emargement = sprintf(
                        '
                        \'\' as emargement,
                        \'\' as emargement_ligne1,
                        CASE
                            WHEN position(\'***|\' in reu_livrable.procuration) <> 0 
                            THEN 
                                substring(reu_livrable.procuration FROM 1 for 35)
                            WHEN reu_livrable.nom_naissance_mandataire_tour2 <> \'\'
                            THEN
                                \'(procuration)\'
                            ELSE \'\'
                        END AS emargement_ligne2,
                        \'\' as emargement_ligne3,
                        \'\' as emargement_ligne4,
                        \'\' as emargement_ligne5'
                    );
                }
            }

            /**
             * 
             */
            $query_procurations_col2_l3_4_5_from_openelec = sprintf(
                '
                CASE
                    WHEN position(\'***|\' in reu_livrable.procuration) <> 0 THEN
                        substring(reu_livrable.procuration FROM 38 for 65)
                    ELSE
                        substring(reu_livrable.procuration FROM 1 for 65)
                END AS colonne2_ligne3,
                CASE
                    WHEN position(\'***|\' in reu_livrable.procuration) <> 0 THEN
                        substring(reu_livrable.procuration FROM 103 for 65)
                    ELSE
                        substring(reu_livrable.procuration FROM 66 for 65)
                END AS colonne2_ligne4,
                CASE
                    WHEN position(\'***|\' in reu_livrable.procuration) <> 0 THEN
                        substring(reu_livrable.procuration FROM 168 for 65)
                    ELSE
                        substring(reu_livrable.procuration FROM 131 for 65)
                END AS colonne2_ligne5,'
            );

            if ($tour === "les_deux") {
                $query_procurations_mention_case = sprintf(
                    ' (
                    CASE
                        WHEN (
                            reu_livrable.nom_naissance_mandataire_tour1 <> \'\'
                            AND reu_livrable.nom_naissance_mandataire_tour2 <> \'\'
                            AND trim(reu_livrable.nom_naissance_mandataire_tour1) = trim(reu_livrable.nom_naissance_mandataire_tour2)
                            AND (trim(reu_livrable.nom_usage_mandataire_tour1) = trim(reu_livrable.nom_usage_mandataire_tour2) OR (reu_livrable.nom_usage_mandataire_tour1 IS NULL AND reu_livrable.nom_usage_mandataire_tour2 IS NULL))
                            AND trim(reu_livrable.prenoms_mandataire_tour1) = trim(reu_livrable.prenoms_mandataire_tour2)
                            AND trim(reu_livrable.date_naissance_mandataire_tour1) = trim(reu_livrable.date_naissance_mandataire_tour2)
                        ) THEN CONCAT(
                                \'Mandataire Tour 1-2 : \',
                                reu_livrable.nom_naissance_mandataire_tour1,
                                CASE
                                    WHEN reu_livrable.nom_usage_mandataire_tour1 <> \'\'
                                        THEN CONCAT(
                                            \' (\',
                                            reu_livrable.nom_usage_mandataire_tour1,
                                            \')\'
                                        )
                                    ELSE \'\'
                                END,
                                \' \',
                                reu_livrable.prenoms_mandataire_tour1,
                                \' \',
                                reu_livrable.date_naissance_mandataire_tour1
                            )
                        ELSE TRIM(CONCAT(
                            CASE
                                WHEN reu_livrable.nom_naissance_mandataire_tour1 <> \'\'
                                    THEN CONCAT(
                                        \'Mandataire Tour 1 : \',
                                        reu_livrable.nom_naissance_mandataire_tour1,
                                        CASE
                                            WHEN reu_livrable.nom_usage_mandataire_tour1 <> \'\' AND reu_livrable.nom_usage_mandataire_tour1 IS NOT NULL
                                                THEN CONCAT(
                                                    \' (\',
                                                    reu_livrable.nom_usage_mandataire_tour1,
                                                    \')\'
                                                )
                                            ELSE \'\'
                                        END,
                                        \' \',
                                        reu_livrable.prenoms_mandataire_tour1,
                                        \' \',
                                        reu_livrable.date_naissance_mandataire_tour1
                                    )
                                ELSE \'\'
                            END,
                            \' \',
                            CASE
                                WHEN reu_livrable.nom_naissance_mandataire_tour2 <> \'\'
                                    THEN CONCAT(
                                        \'Mandataire Tour 2 : \',
                                        reu_livrable.nom_naissance_mandataire_tour2,
                                        CASE
                                            WHEN reu_livrable.nom_usage_mandataire_tour2 <> \'\'  AND reu_livrable.nom_usage_mandataire_tour2 IS NOT NULL
                                                THEN CONCAT(
                                                    \' (\',
                                                    reu_livrable.nom_usage_mandataire_tour2,
                                                    \')\'
                                                )
                                            ELSE \'\'
                                        END,
                                        \' \',
                                        reu_livrable.prenoms_mandataire_tour2,
                                        \' \',
                                        reu_livrable.date_naissance_mandataire_tour2
                                    )
                                ELSE \'\'
                            END
                        ))
                    END
                    ) '
                );
            } elseif ($tour == "un") {
                $query_procurations_mention_case = sprintf(
                    ' (
                    CASE
                        WHEN reu_livrable.nom_naissance_mandataire_tour1 <> \'\'
                            THEN CONCAT(
                                \'Mandataire : \',
                                reu_livrable.nom_naissance_mandataire_tour1,
                                CASE
                                    WHEN reu_livrable.nom_usage_mandataire_tour1 <> \'\' AND reu_livrable.nom_usage_mandataire_tour1 IS NOT NULL
                                        THEN CONCAT(
                                            \' (\',
                                            reu_livrable.nom_usage_mandataire_tour1,
                                            \')\'
                                        )
                                    ELSE \'\'
                                END,
                                \' \',
                                reu_livrable.prenoms_mandataire_tour1,
                                \' \',
                                reu_livrable.date_naissance_mandataire_tour1
                            )
                        ELSE \'\'
                    END
                    ) '
                );
            } elseif ($tour == "deux") {
                $query_procurations_mention_case = sprintf(
                    ' (
                    CASE
                        WHEN reu_livrable.nom_naissance_mandataire_tour2 <> \'\'
                            THEN CONCAT(
                                \'Mandataire : \',
                                reu_livrable.nom_naissance_mandataire_tour2,
                                CASE
                                    WHEN reu_livrable.nom_usage_mandataire_tour2 <> \'\' AND reu_livrable.nom_usage_mandataire_tour2 IS NOT NULL
                                        THEN CONCAT(
                                            \' (\',
                                            reu_livrable.nom_usage_mandataire_tour2,
                                            \')\'
                                        )
                                    ELSE \'\'
                                END,
                                \' \',
                                reu_livrable.prenoms_mandataire_tour2,
                                \' \',
                                reu_livrable.date_naissance_mandataire_tour2
                            )
                        ELSE \'\'
                    END
                    ) '
                );
            }

            $query_procurations_col2_l3_4_5_from_reu = sprintf(
                '
                substring(%1$s FROM 1 for 65) AS colonne2_ligne3,
                substring(%1$s FROM 66 for 65) AS colonne2_ligne4,
                substring(%1$s FROM 131 for 65) AS colonne2_ligne5,
                ',
                $query_procurations_mention_case
            );
            //
            $query_procurations_col2_l3_4_5 = $query_procurations_col2_l3_4_5_from_reu;
            //
            $sql = sprintf(
                'SELECT 
                    CASE WHEN electeur.id IS NULL
                        THEN (\'*\' || reu_livrable.nom_de_naissance || \' - \' || reu_livrable.prenoms)
                        ELSE (electeur.nom || \' - \' || electeur.prenom)
                    END AS nomprenom,
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.nom_d_usage
                        ELSE electeur.nom_usage
                    END AS nom_usage,
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.date_de_naissance
                        ELSE to_char(electeur.date_naissance,\'DD/MM/YYYY\')
                    END AS naissance,
                    CASE WHEN electeur.id IS NULL
                        THEN concat(substring(reu_livrable.commune_de_naissance FROM 0 for 29), \' (\',
                        CASE
                            WHEN reu_livrable.pays_de_naissance = \'FRANCE\' THEN reu_livrable.code_de_departement_de_naissance
                            ELSE reu_livrable.pays_de_naissance
                        END, 
                        \')\')
                        ELSE (substring(libelle_lieu_de_naissance FROM 0 for 29) || \' (\' || code_departement_naissance || \')\')
                    END AS lieu,
                    CASE WHEN electeur.id IS NULL
                        THEN
                            CASE reu_livrable.liste
                                WHEN \'01\' then \'\'
                                ELSE reu_livrable.libelle_de_la_nationalite
                            END
                        ELSE
                            case electeur.liste 
                                when \'01\' then \'\' 
                                else nationalite.libelle_nationalite 
                            end
                    END AS colonne1_ligne4, 
                    \' \' colonne1_ligne5,
                    CASE WHEN electeur.id IS NULL
                        THEN CONCAT_WS(\' \', reu_livrable.numero_de_voie, reu_livrable.libelle_de_voie)
                        ELSE
                            case resident 
                                when \'Non\' then (
                                    case numero_habitation > 0 
                                        when True then (numero_habitation || \' \' || complement_numero || \' \' || electeur.libelle_voie) 
                                        when False then electeur.libelle_voie 
                                    end) 
                                when \'Oui\' then adresse_resident 
                            end
                    END AS adresse,
                    CASE WHEN electeur.id IS NULL
                        THEN CONCAT_WS(\' \', reu_livrable.complement_1, reu_livrable.complement_2, reu_livrable.lieu_dit, reu_livrable.code_postal, reu_livrable.commune, reu_livrable.pays)
                        ELSE
                            case resident 
                                when \'Non\' then electeur.complement 
                                when \'Oui\' then (complement_resident || \' \' || cp_resident || \' - \' || ville_resident) 
                            end
                    END as complement,
                    %8$s
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.numero_d_ordre_dans_le_bureau_de_vote
                        ELSE electeur.numero_bureau::text
                    END AS numero_bureau,
                    %7$s
                FROM
                    %1$sreu_livrable
                    LEFT JOIN (%1$selecteur
                        LEFT JOIN %1$snationalite ON electeur.code_nationalite=nationalite.code
                        LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
                    ) ON reu_livrable.electeur_id=electeur.id
                        AND reu_livrable.numero_d_ordre_dans_le_bureau_de_vote::integer=electeur.numero_bureau
                        AND reu_livrable.code_du_bureau_de_vote::integer=bureau.code::integer
                WHERE
                    reu_livrable.demande_id=%6$s
                    AND reu_livrable.scrutin_id=%5$s
                    %4$s
                    AND reu_livrable.liste=\'%3$s\'
                    AND reu_livrable.om_collectivite=%2$s
                ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $liste,
                ($mode_edition === "parbureau" && isset($bureau_code) ? "AND reu_livrable.code_du_bureau_de_vote='".$this->f->db->escapesimple($bureau_code)."'" : ""),
                $params["scrutin"],
                $params["scrutin_demande_id"],
                $double_emargement,
                $query_procurations_col2_l3_4_5
            );
            // Tri
            if (isset($is_option_tri_liste_emargement_alpha) && $is_option_tri_liste_emargement_alpha === true) {
                $sql .= " ORDER BY 
                    withoutaccent(lower(reu_livrable.nom_de_naissance)),
                    withoutaccent(lower(electeur.nom)),
                    withoutaccent(lower(reu_livrable.prenoms)),
                    withoutaccent(lower(electeur.prenom)),
                    electeur.numero_bureau
                ";
            } else {
                $sql .= " ORDER BY
                    electeur.numero_bureau
                ";
            }
            /**
             *
             */
            $nb_mentions_tour_un = $this->f->get_one_result_from_db_query(sprintf(
                'SELECT
                    count(*)
                FROM
                    %1$sreu_livrable
                WHERE
                    reu_livrable.demande_id=%6$s
                    AND reu_livrable.scrutin_id=%5$s
                    %4$s
                    AND reu_livrable.liste=\'%3$s\'
                    AND reu_livrable.om_collectivite=%2$s
                    AND position(\'***|1\' in reu_livrable.procuration) <> 0
                ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $liste,
                ($mode_edition === "parbureau" && isset($bureau_code) ? "AND reu_livrable.code_du_bureau_de_vote='".$this->f->db->escapesimple($bureau_code)."'" : ""),
                $params["scrutin"],
                $params["scrutin_demande_id"]
            ));
            $nb_mentions_tour_deux = $this->f->get_one_result_from_db_query(sprintf(
                'SELECT
                    count(*)
                FROM
                    %1$sreu_livrable
                WHERE
                    reu_livrable.demande_id=%6$s
                    AND reu_livrable.scrutin_id=%5$s
                    %4$s
                    AND reu_livrable.liste=\'%3$s\'
                    AND reu_livrable.om_collectivite=%2$s
                    AND position(\'***|2\' in reu_livrable.procuration) <> 0
                ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $liste,
                ($mode_edition === "parbureau" && isset($bureau_code) ? "AND reu_livrable.code_du_bureau_de_vote='".$this->f->db->escapesimple($bureau_code)."'" : ""),
                $params["scrutin"],
                $params["scrutin_demande_id"]
            ));
            $nb_mentions_tour_les_deux = $this->f->get_one_result_from_db_query(sprintf(
                'SELECT
                    count(*)
                FROM
                    %1$sreu_livrable
                WHERE
                    reu_livrable.demande_id=%6$s
                    AND reu_livrable.scrutin_id=%5$s
                    %4$s
                    AND reu_livrable.liste=\'%3$s\'
                    AND reu_livrable.om_collectivite=%2$s
                    AND position(\'***|-\' in reu_livrable.procuration) <> 0
                ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $liste,
                ($mode_edition === "parbureau" && isset($bureau_code) ? "AND reu_livrable.code_du_bureau_de_vote='".$this->f->db->escapesimple($bureau_code)."'" : ""),
                $params["scrutin"],
                $params["scrutin_demande_id"]
            ));
        }

        /**
         *
         */
        require_once "../obj/fpdf_table.class.php";
        $pdf = new PDF('L', 'mm', 'A4');
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(false);
        // Fixe la police utilisée pour imprimer les chaînes de caractères
        $pdf->SetFont(
            'Arial', // Famille de la police.
            '', // Style de la police.
            7 // Taille de la police en points.
        );
        // Fixe la couleur pour toutes les opérations de tracé
        $pdf->SetDrawColor(30, 7, 146);
        // Fixe les marges
        $pdf->SetMargins(
            10, // Marge gauche.
            10, // Marge haute.
            10 // Marge droite.
        );
        $pdf->SetDisplayMode('real', 'single');
        $pdf->AddPage();
        //
        $total=0;
        $decompte=0;
        $nbr_enregistrement=0;
        // COMMON
        $param = array(
            7, // Titre : Taille de la police en points.
            7, // Entete : Taille de la police en points.
            7, // Taille de la police en points.
            1, //
            1, //
            10, // Nombre maximum d'enregistrements par page pour la première page
            10, // Nombre maximum d'enregistrements par page pour les autres pages
            10, // Position de départ - X - La valeur de l'abscisse.
            10, // Position de départ - Y - La valeur de l'ordonnée.
            0, //
            0, //
            "display_lastpage_even_if_table_is_empty" => true,
            "display_table_even_if_table_is_empty" => true,
        );
        $page_content_width = 277;
        // Variable de developpement pour afficher les bordures de toutes les cellules
        $allborders = false;
        //-------------------------------------------------------------------//
        // TITRE (PAGE HEADER)
        $page_header = $this->get_page_header_config(array(
            "allborders" => $allborders,
            "page_content_width" => $page_content_width,
            "titre_libelle_ligne1" => $titre_libelle_ligne1,
            "titre_libelle_ligne2" => $titre_libelle_ligne2,
            "libelle_commune" => $libelle_commune,
            "libelle_liste" => $libelle_scrutin." - ".$libelle_liste,
            "bureau_code" => $bureau_code,
            "bureau_libelle" => $bureau_libelle,
            "canton_libelle" => $canton_libelle,
            "circonscription_libelle" => $circonscription_libelle,
            "page_header_line_height" => 4,
        ));
        //-------------------------------------------------------------------//
        // ENTETE (TABLE HEADER)
        $entete_height = 8;
        $entete = array(
            array(
                __("Nom Patronymique, Prénoms"),
                85, $entete_height-4, 2, ($allborders == true ? 1 : "LRT"), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0),
            array(
                sprintf(__("Nom d'Usage, Date et lieu de naissance%s"), ($liste != "01" ? __(", Nationalité"): "")),
                85, $entete_height-4, 0, ($allborders == true ? 1 : "LRB"), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0),
            array(
                __("Adresse"),
                95, $entete_height, 0, ($allborders == true ? 1 : 1), 'C', '0', '0', '0', '255', '255', '255', array(0, 4), 0, '', 'NB', 0, 1, 0),
            array(
                __("No Ordre"),
                12, $entete_height, 0, ($allborders == true ? 1 : 1), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0),
        );
        //
        if ($tour === "les_deux") {
            $entete[] = array(
                __("Emargement"),
                85, $entete_height-4, 2, ($allborders == true ? 1 : "LRT"), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0
            );
            $entete[] = array(
                'Tour 2                                              Tour 1',
                85, $entete_height-4, 0, ($allborders == true ? 1 : "LRB"), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0
            );
        } else {
            $entete[] = array(
                __("Emargement"),
                85, $entete_height, 0, ($allborders == true ? 1 : 1), 'C', '0', '0', '0', '255', '255', '255', array(0, 0), 0, '', 'NB', 0, 1, 0
            );
        }
        //-------------------------------------------------------------------//
        // FIRSTPAGE
        $firstpage = array_merge(
            $page_header,
            array(
                array(
                    " ",
                    $page_content_width, 20, 1, ($allborders == true ? 1 : "B"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(
                    " ",
                    $page_content_width, 1, 1, ($allborders == true ? 1 : "T"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(
                    $titre_libelle_ligne1,
                    $page_content_width, 10, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',18,1,0),
                array(
                    "",
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                array(
                    "",
                    $page_content_width, 5, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                array(
                    sprintf(__("Commune : %s"), $libelle_commune),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    sprintf(__("Liste : %s"), $libelle_liste),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    ($bureau_code != "ALL" ? sprintf(__("Bureau : %s - %s"), $bureau_code, $bureau_libelle) : ""),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    ($canton_libelle != "" ? sprintf(__("Canton : %s"), $canton_libelle) : ""),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    ($circonscription_libelle != "" ? sprintf(__("Circonscription : %s"), $circonscription_libelle) : ""),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    " ",
                    $page_content_width, 10, 1, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            )
        );
        //-------------------------------------------------------------------//
        // LASTPAGE
        $lastpage_height = 9;
        $lastpage = array_merge(
            $page_header,
            array(
                array(
                    __(" *  *  *  A R R Ê T É  *  *  *"),
                    $page_content_width, 10, 2, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',8,1,0),
                array(
                    __("LA LISTE D'ÉMARGEMENT"),
                    $page_content_width, 10, 2, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',10,1,0),
                array(
                    "  ",
                    50,10,0, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(
                    sprintf(__(' du bureau No %s  %s   -   au nombre de '), $bureau_code, $bureau_libelle),
                    130,10,0, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(
                    '<LIGNE>',
                    30,10,0, ($allborders == true ? 1 : 0), 'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(
                    " ".__("ÉLECTEURS."),
                    25, 10, 2, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            )
        );

        //
        $heightligne = 15;
        $line_height = 3;
        $line_nb = 5;
        //
        $col = array(
            // colonne 1 - ligne 1
            'nomprenom' => array(
                85, $line_height, 2, ($allborders == true ? 1 : "LRT"), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 
                'A', // On archive la valeur du champ pour la réutiliser
                'NC', '', 'B', 7, array('0')),
            // colonne 1 - ligne 2
            'nom_usage' => array(
                85, $line_height, 2, ($allborders == true ? 1 : "LR"), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), '       -    ', '', 'CELLSUP_NON', 0, 'NA', 'CONDITION', array('NN'), 'NB', 0, array('0')),
            // colonne 1 - ligne 3
            'naissance' => array(
                27, $line_height, 0, ($allborders == true ? 1 : "L"), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), __('     Ne(e) le '), '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0')),
            // colonne 1 - ligne 3
            'lieu' => array(
                58, $line_height, 1, ($allborders == true ? 1 : "R"), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), __(' a  '), '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0'))
        );
        //
        if ($liste == "01") {
            // colonne 1 - ligne 4
            $col["colonne1_ligne4"] = array(
                85, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'C', '213', '8', '26', '255', '255', '255', array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')
            );
        } else {
            // colonne 1 - ligne 4
            $col["colonne1_ligne4"] = array(
                85, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), '     ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0')
            );
        }
        //
        $col = array_merge($col, array(
            // colonne 1 - ligne 4
            'colonne1_ligne5' => array(
                85, $line_height, 0, ($allborders == true ? 1 : 'LRB'), 'C', '213', '8', '26', '255', '255', '255', array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
            // colonne 2 - ligne 1
            'adresse' => array(
                95, $line_height, 2, ($allborders == true ? 1 : 'LRT'), 'L', '0', '0', '0', '255', '255', '255', array(0, $line_height*($line_nb-1)), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0')),
            // colonne 2 - ligne 2
            'complement' => array(
                95, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'L', '0', '0', '0', '255', '255', '255', array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0')),
            // colonne 2 - ligne 3
            'colonne2_ligne3' => array(
                95, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'L', '213', '8', '26', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
            // colonne 2 - ligne 4
            'colonne2_ligne4' => array(
                95, $line_height, 2, ($allborders == true ? 1 : 'LR'), 'L', '213', '8', '26', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
            // colonne 2 - ligne 4
            'colonne2_ligne5' => array(
                95, $line_height, 0, ($allborders == true ? 1 : 'LRB'), 'L', '213', '8', '26', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
            // colonne 3 - ligne 1
            'numero_bureau' => array(
                12, $line_height*$line_nb, 0, ($allborders == true ? 1 : 1), 'R', '0', '0', '0', '255', '255', '255', array(0, $line_height*($line_nb-1)), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'NB', 0, array('0')),
        ));
        //
        if ($tour === "les_deux") {
            //
            $col = array_merge($col, array(
                // colonne 3 - ligne 1
                'tour2' => array(
                    42, $line_height*$line_nb, 0, ($allborders == true ? 1 : 'LTBR'), 'C', '213', '8', '26', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
                // colonne 3 - ligne 1
                'tour1' => array(
                    42, $line_height*$line_nb, 0, ($allborders == true ? 1 : 'LTB'), 'C', '213', '8', '26', '255', '255', '255', array(0, 0), ' ', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),

                // colonne 4 - ligne
                'emargement' => array(
                    1, $line_height*($line_nb-1), 2, ($allborders == true ? 1 : 0), 'L', '24', '4', '148', -1, -1, -1, array(0, 0), '', '',
                    '0', // Cellule supplémentaire avec le contenu du champ archive 0 (nomprenom)
                        //
                        array(1, $line_height*(1), 0, ($allborders == true ? 1 : 0), 'LDCELL',
                            '24', '4', '148',
                            '255', '255', '255',
                            array(0, 0), -1, 0, 'B', 7, 1, 0),
                    'NA', 'NC', '', 'B', 0, array('0')),
                // colonne 4 - ligne
                'emargement_ligne1' => array(
                    85, $line_height*(1), 2, ($allborders == true ? 1 : 'LTR'), 'C', '213', '8', '26', -1, -1, -1, array(85, $line_height*($line_nb-1)), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
                'emargement_ligne2' => array(
                    85, $line_height*(1), 2, ($allborders == true ? 1 : 'LR'), 'C', '213', '8', '26', -1, -1, -1, array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
                'emargement_ligne3' => array(
                    85, $line_height*(1), 2, ($allborders == true ? 1 : 'LR'), 'C', '213', '8', '26', -1, -1, -1, array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
                'emargement_ligne4' => array(
                    85, $line_height*(1), 2, ($allborders == true ? 1 : 'LR'), 'C', '213', '8', '26', -1, -1, -1, array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
                'emargement_ligne5' => array(
                    85, $line_height*(1), 0, ($allborders == true ? 1 : 'LRB'), 'C', '213', '8', '26', -1, -1, -1, array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
            ));
        } else {
            //
            $col = array_merge($col, array(
                // colonne 4 - ligne
                'emargement' => array(
                    85, $line_height*($line_nb-1), 2, ($allborders == true ? 1 : 0), 'C', '213', '8', '26', '255', '255', '255', array(0, 0), '', '', '0',
                        //
                        array(85, $line_height*(1), 0, ($allborders == true ? 1 : 0), 'LDCELL',
                            '24', '4', '148',
                            '255', '255', '255',
                            array(0, 0), -1, 0, 'B', 7, 1, 0),
                    'NA', 'NC', '', 'B', 0, array('0')),
                // colonne 4 - ligne
                'emargement_ligne1' => array(
                    85, $line_height*(1), 2, ($allborders == true ? 1 : 'LTR'), 'C', '213', '8', '26', -1, -1, -1, array(85, $line_height*($line_nb-1)), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
                'emargement_ligne2' => array(
                    85, $line_height*(1), 2, ($allborders == true ? 1 : 'LR'), 'C', '213', '8', '26', -1, -1, -1, array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
                'emargement_ligne3' => array(
                    85, $line_height*(1), 2, ($allborders == true ? 1 : 'LR'), 'C', '213', '8', '26', -1, -1, -1, array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
                'emargement_ligne4' => array(
                    85, $line_height*(1), 2, ($allborders == true ? 1 : 'LR'), 'C', '213', '8', '26', -1, -1, -1, array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
                'emargement_ligne5' => array(
                    85, $line_height*(1), 0, ($allborders == true ? 1 : 'LBR'), 'C', '213', '8', '26', -1, -1, -1, array(0, 0), '', '', 'CELLSUP_NON', 0, 'NA', 'NC', '', 'B', 0, array('0')),
            ));
        }

        //
        $enpied = array();
        $pdf->Table(
            $page_header,
            $entete,
            $col,
            $enpied,
            $sql,
            $this->f->db,
            $param,
            $lastpage,
            $firstpage
        );
        //
        $total=$pdf->nbrligne;

        // pas de prise en compte de mairie europe si pas d inscrit en mairie europe
        if ($total > 0) {
            //
            $heightligne = 6;
            //
            $nb_inscrits_avec_une_mention_sur_le_tour_1 = ($nb_mentions_tour_un["result"] + $nb_mentions_tour_les_deux["result"]);
            $nb_inscrits_avec_une_mention_sur_le_tour_2 = ($nb_mentions_tour_deux["result"] + $nb_mentions_tour_les_deux["result"]);
            $decompte_tour1 = $total - $nb_inscrits_avec_une_mention_sur_le_tour_1;
            $decompte_tour2 = $total - $nb_inscrits_avec_une_mention_sur_le_tour_2;
            //
            if ($tour == "un" || $tour == "les_deux") {
                $pdf->Ln(0);
                $pdf->Cell(50,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
                $pdf->SetFont('Arial','',8);
                $pdf->Cell(130,$heightligne,iconv(HTTPCHARSET,"CP1252",__('  Nombre inscrit(s) avec mention - Tour 1')), ($allborders == true ? 1 : 0),0,'L',1);
                $pdf->Cell(30,$heightligne,iconv(HTTPCHARSET,"CP1252",$nb_inscrits_avec_une_mention_sur_le_tour_1), ($allborders == true ? 1 : 0),0,'R',1);
                $pdf->Cell(25,$heightligne,iconv(HTTPCHARSET,"CP1252",__(' electeur(s)')), ($allborders == true ? 1 : 0),1,'L',1);
                $pdf->Cell(50,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(130,$heightligne,iconv(HTTPCHARSET,"CP1252",__(' TOTAL avec déduction inscrit(s) avec mention - Tour 1')), ($allborders == true ? 1 : 0),0,'L',1);
                $pdf->SetFont('Arial','B',12);
                $pdf->Cell(30,$heightligne,iconv(HTTPCHARSET,"CP1252",$decompte_tour1), ($allborders == true ? 1 : 0),0,'R',1);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(25,$heightligne,iconv(HTTPCHARSET,"CP1252",__(' electeur(s)')), ($allborders == true ? 1 : 0),1,'L',1);
            }
            if ($tour == "deux" || $tour == "les_deux") {
                $pdf->Ln(0);
                $pdf->Cell(50,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
                $pdf->SetFont('Arial','',8);
                $pdf->Cell(130,$heightligne,iconv(HTTPCHARSET,"CP1252",__('  Nombre inscrit(s) avec mention - Tour 2')), ($allborders == true ? 1 : 0),0,'L',1);
                $pdf->Cell(30,$heightligne,iconv(HTTPCHARSET,"CP1252",$nb_inscrits_avec_une_mention_sur_le_tour_2), ($allborders == true ? 1 : 0),0,'R',1);
                $pdf->Cell(25,$heightligne,iconv(HTTPCHARSET,"CP1252",__(' electeur(s)')), ($allborders == true ? 1 : 0),1,'L',1);
                $pdf->Cell(50,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(130,$heightligne,iconv(HTTPCHARSET,"CP1252",__(' TOTAL avec déduction inscrit(s) avec mention - Tour 2')), ($allborders == true ? 1 : 0),0,'L',1);
                $pdf->SetFont('Arial','B',12);
                $pdf->Cell(30,$heightligne,iconv(HTTPCHARSET,"CP1252",$decompte_tour2), ($allborders == true ? 1 : 0),0,'R',1);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(25,$heightligne,iconv(HTTPCHARSET,"CP1252",__(' electeur(s)')), ($allborders == true ? 1 : 0),1,'L',1);
            }
            //
            $pdf->SetFont('Arial','',8);
            $pdf->Cell(50,$heightligne,' ', ($allborders == true ? 1 : 0),0,'R',1);
            $pdf->Cell(130,$heightligne,iconv(HTTPCHARSET,"CP1252",sprintf(__(' %s, le %s'),$libelle_commune,date("d/m/Y"))), ($allborders == true ? 1 : 0), 1,'L',1);
            // Gestion d'une image signature + cachet
            $inst_om_edition = $this->f->get_inst__om_edition();
            $signature_logo = $inst_om_edition->get_logo_from_collectivite(
                "signature-liste-emargement.png",
                $_SESSION["collectivite"]
            );
            $pdf->Cell(35, 28,' ', ($allborders == true ? 1 : 0), 0, 'R', 1);
            if ($signature_logo != null) {
                $pdf->Image($signature_logo["file"], null, null, 105, 28,'png');
            } else {
                $pdf->Cell(105, 28, ' ', ($allborders == true ? 1 : 0), 1, 'L', 0);
            }
            //
            if ($bloc_arrete_nb_emargements === true) {
                $pdf->Ln(0);
                $pdf->Cell(276, 15, iconv(HTTPCHARSET, "CP1252", ""), ($allborders == true ? 1 : 0), 1, 'L', 1);            
                $heightligne = 7;
                $pdf->SetFont('Arial', '', 8);
                $text_line1 = __('Élection __________________________________________________ du _________________________');
                $text_line2 = __('Arrêtée par Nous, membres du bureau électoral, la présente liste, comprenant(1) :');
                $text_line3 = __('______________________________________________________________________________ électeurs');
                $text_line4 = __('à (1) ______________________________________________________________________ émargements');
                $text_line5_1 = __('pour le premier tour du scrutin.');
                $text_line5_2 = __('pour le second tour du scrutin.');
                $text_line6 = __('À __________________________________________________, le _________________________ ');
                $text_line7 = __('Les membres du bureau,                                                                     Le président,');
                $text_line8 = ' ';
                if ($tour == "un" || $tour == "les_deux") {
                    for ($i=1 ; $i<=8; $i++) {
                        if ($i == 5) {
                            $var_name_src = "text_line".$i."_1";
                        } else {
                            $var_name_src = "text_line".$i;
                        }
                        $var_name_dest = "text_line".$i."_t1";
                        ${$var_name_dest} = ${$var_name_src};
                    }
                }
                if ($tour == "deux" || $tour == "les_deux") {
                    for ($i=1 ; $i<=8; $i++) {
                        if ($i == 5) {
                            $var_name_src = "text_line".$i."_2";
                        } else {
                            $var_name_src = "text_line".$i;
                        }
                        $var_name_dest = "text_line".$i."_t2";
                        ${$var_name_dest} = ${$var_name_src};
                    }
                }
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line1_t1), ($allborders == true ? 1 : 'R'), 0, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line1_t2), ($allborders == true ? 1 : 0), 1, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line2_t1), ($allborders == true ? 1 : 'R'), 0, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line2_t2), ($allborders == true ? 1 : 0), 1, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line3_t1), ($allborders == true ? 1 : 'R'), 0, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line3_t2), ($allborders == true ? 1 : 0), 1, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line4_t1), ($allborders == true ? 1 : 'R'), 0, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line4_t2), ($allborders == true ? 1 : 0), 1, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line5_t1), ($allborders == true ? 1 : 'R'), 0, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line5_t2), ($allborders == true ? 1 : 0), 1, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line6_t1), ($allborders == true ? 1 : 'R'), 0, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line6_t2), ($allborders == true ? 1 : 0), 1, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line7_t1), ($allborders == true ? 1 : 'R'), 0, 'L', 1);
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text_line7_t2), ($allborders == true ? 1 : 0), 1, 'L', 1);
                $pdf->Cell(138, $heightligne*2, iconv(HTTPCHARSET, "CP1252", $text_line8_t1), ($allborders == true ? 1 : 'R'), 0, 'L', 1);
                $pdf->Cell(138, $heightligne*2, iconv(HTTPCHARSET, "CP1252", $text_line8_t2), ($allborders == true ? 1 : 0), 1, 'L', 1);
                $pdf->SetFont('Arial', 'I', 8);
                $text = __('(1) indiquer le nombre en toutes lettres.');
                $pdf->Cell(138, $heightligne, iconv(HTTPCHARSET, "CP1252", $text), ($allborders == true ? 1 : 0), 0, 'L', 1);
            }
        }

        /**
         * Affichage d'un bloc spécifique si aucun enregistrement sélectionné.
         */
        if ($pdf->msg==1 and $message!='') {
            $pdf->SetFont('Arial','',10);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetFillColor(213,8,26);
            $pdf->SetTextColor(255,255,255);
            $pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message), ($allborders == true ? 1 : 1),'C',1);
        }

        /**
         * OUTPUT
         */
        //
        $filename = date("Ymd-His");
        $filename .= "-emargement";
        $filename .= "-".$_SESSION["collectivite"];
        $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
        $filename .= "-liste".$liste;
        $filename .= "-bureau".$bureau_code;
        if (array_key_exists("scrutin", $params) === true) {
            $filename .= "-scrutin".$params["scrutin"];
        }
        $filename .= ".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
