<?php
/**
 * Ce script définit la classe 'module_jury'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_jury' (module).
 */
class module_jury extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_jury",
            "title" => __("Traitement")." -> ".__("Module Jury d'assises"),
            "description" => __("Les jures d'assises peuvent etre gere par le logiciel pour ".
                "faire le tirage aleatoire, et sortir les etiquettes ou le ".
                "listing. Vous pouvez aussi modifier manuellement les ".
                "informations de jure d'assises d'un electeur au moyen de ".
                "sa fiche electeur : \"Consultation -> Liste electorale\"."),
            "condition" => array(
                "is_liste_principale",
            ),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_jury-onglet_traitement_jury",
                    "href" => "../app/index.php?module=module_jury&view=jury",
                    "title" => __("Tirage aleatoire"),
                    "view" => "jury",
                    "id" => "jury-tirage-aleatoire"
                ),
                array(
                    "type" => "tab",
                    "right" => "module_jury-onglet_electeur_jury_tab",
                    "href" => OM_ROUTE_SOUSTAB."&obj=electeur_jury&amp;retourformulaire=module_jury&amp;idxformulaire=module_jury",
                    "title" => __("Liste preparatoire courante"),
                    "id" => "jury-liste-preparatoire-courante",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_jury-onglet_traitement_jury_epuration",
                    "href" => "../app/index.php?module=module_jury&view=epuration",
                    "title" => __("Epuration"),
                    "view" => "epuration",
                    "id" => "jury-epuration"
                ),
                array(
                    "type" => "tab",
                    "right" => "module_jury-onglet_traitement_jury_parametrage",
                    "href" => OM_ROUTE_SOUSTAB."&obj=parametrage_nb_jures&amp;retourformulaire=module_jury&amp;idxformulaire=module_jury",
                    "title" => __("Parametrage"),
                    "id" => "jury-parametrage",
                ),
                array(
                    "type" => "edition_pdf",
                    "right" => "module_jury-edition_pdf__recapitulatif_jury",
                    "view" => "edition_pdf__recapitulatif_jury",
                ),
            ),
        );
    }

    /**
     *
     */
    function view__jury() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_jury");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         * Le module jury n'est pas disponible pour les listes complementaires
         * Si l'utilisateur est connecte sur une liste complementaire, il n'a pas acces
         * a l'interface du traitement
         */
        if ($_SESSION["liste"] != "01") {
            // Affichage du message
            $message_class= "error";
            $message = __("Ce module n'est pas disponible pour les listes complementaires.");
            $this->f->displayMessage($message_class, $message);
            // Fin du script
            die();
        }

        /**
         *
         */
        //
        $description = __("Ce traitement permet de gerer les jures d'assises. Le tirage ".
                         "aleatoire permet de selectionner des electeurs selon ".
                         "la reglementation en vigueur.");
        $this->f->displayDescription($description);

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        if ($this->f->is_option_enabled('option_module_jures_suppleants')) {

        }
        $subtitle = "-> ".
            ($this->f->is_option_enabled('option_module_jures_suppleants') === true ?
                __("Tirage aléatoire des jurys titulaires") :
                __("Tirage aléatoire"));
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.jury.class.php";
        $trt = new juryTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        if ($this->f->is_option_enabled('option_module_jures_suppleants')) {
            // Ouverture de la balise - DIV paragraph
            echo "<div class=\"paragraph\">\n";
            // Affichage du titre du paragraphe
            $subtitle = "-> ". __("Tirage aléatoire des jurys suppléants");
            $this->f->displaySubTitle($subtitle);
            $trt = new juryTraitement();
            $trt->setJury(2);
            $trt->setFichier('jury_suppleant');
            $trt->setDescription(__("Attention, lancer le traitement efface la sélection des jurés suppléants stockés dans le système."));
            $trt->displayForm();
            // Fermeture de la balise - DIV paragraph
            echo "</div>\n";
        }

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Editions des statistiques, listings et etiquettes");
        $this->f->displaySubTitle($subtitle);
        //
        $links = array(
            "0" => array(
                "id" => "action-module-jury-pdf-edtiquettes",
                "href" => "../app/index.php?module=form&obj=electeur_jury&action=306",
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => __("Etiquettes des jures"),
            ),
            "1" => array(
                "id" => "action-module-jury-pdf-liste-preparatoire",
                "href" => "../app/index.php?module=module_jury&view=edition_pdf__recapitulatif_jury",
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => __("Liste preparatoire du jury d'assises"),
            ),
            "3" => array(
                "href" => OM_ROUTE_MODULE_REQMO."&obj=list__electeur_jury",
                "class" => "om-prev-icon reqmo-16",
                "title" => __("Export CSV des electeurs selectionnes"),
            ),
        );
        //
        if ($this->f->is_option_enabled('option_module_jures_suppleants')) {
            $this->f->displayLinksAsList($links);
            $links = array(
                "0" => array(
                    "id" => "action-module-jury-pdf-edtiquettes-suppleants",
                    "href" => "../app/index.php?module=form&obj=electeur_jury&action=307",
                    "target" => "_blank",
                    "class" => "om-prev-icon edition-16",
                    "title" => __("Étiquettes des jurés suppléants"),
                ),
                "1" => array(
                    "id" => "action-module-jury-pdf-liste-preparatoire-suppleants",
                    "href" => "../app/index.php?module=module_jury&view=edition_pdf__recapitulatif_jury&jury=2",
                    "target" => "_blank",
                    "class" => "om-prev-icon edition-16",
                    "title" => __("Liste préparatoire des suppléants du jury d'assises"),
                ),
                "2" => array(
                    "id" => "action-module-jury-exports-electeurs-suppleants",
                    "href" => OM_ROUTE_MODULE_REQMO."&obj=list__electeur_suppleant_jury",
                    "class" => "om-prev-icon reqmo-16",
                    "title" => __("Export CSV des électeurs suppléants sélectionnés"),
                ),
            );
        }
        //
        $this->f->displayLinksAsList($links);
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

    }

    function view__epuration() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_jury");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         * Le module jury n'est pas disponible pour les listes complementaires
         * Si l'utilisateur est connecte sur une liste complementaire, il n'a pas acces
         * a l'interface du traitement
         */
        if ($_SESSION["liste"] != "01") {
            // Affichage du message
            $message_class= "error";
            $message = __("Ce module n'est pas disponible pour les listes complementaires.");
            $this->f->displayMessage($message_class, $message);
            // Fin du script
            die();
        }

        /**
         *
         */
        //
        $description = __("L'epuration signifit la suppression des enregistrements ".
                         "dans la base de donnees. Ce traitement doit etre realise ".
                         "avec precaution.");
        $this->f->displayDescription($description);

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Epuration des jures d'assises");
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.jury_epuration.class.php";
        $trt = new juryEpurationTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

    }

    /**
     * VIEW - view__edition_pdf__recapitulatif_jury.
     *
     * @return void
     */
    function view__edition_pdf__recapitulatif_jury() {
        //
        $params = array();
        // Récupération du type de jury envoyé via l'url pour différencier l'affichage des titulaires
        // et des suppléants
        $jury = $this->f->get_submitted_get_value('jury');
        if (! empty($jury)) {
            $params['jury'] = $jury;
        }
        require_once "../obj/edition_pdf__recapitulatif_jury.class.php";
        $inst_edition_pdf = new edition_pdf__recapitulatif_jury();
        $pdf_edition = $inst_edition_pdf->compute_pdf__recapitulatif_jury($params);
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }
}
