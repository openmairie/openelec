<?php
/**
 * Ce script définit la classe 'decoupage'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/decoupage.class.php";

/**
 * Définition de la classe 'decoupage' (om_dbform).
 */
class decoupage extends decoupage_gen {

    /**
     *
     */
    function init_class_actions() {
        parent::init_class_actions();
        //
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-electeurpardecoupage",
            "view" => "view_edition_pdf__electeurpardecoupage",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Électeurs par découpage"),
               "class" => "pdf-16",
            ),
            "permission_suffix" => "edition_pdf__electeurpardecoupage",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "code_voie",
            "bureau",
            "premier_impair",
            "dernier_impair",
            "premier_pair",
            "dernier_pair",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return $this->get_common_var_sql_forminc__sql_bureau();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return $this->get_common_var_sql_forminc__sql_bureau_by_id();
    }

    /**
     * VIEW - view_edition_pdf__electeurpardecoupage.
     *
     * @return void
     */
    public function view_edition_pdf__electeurpardecoupage() {
        $this->checkAccessibility();
        //
        $_GET['obj'] = "electeurpardecoupage";
        $_GET['idx'] = $this->getVal($this->clePrimaire);
        //
        $om_edition = $this->f->get_inst__om_edition();
        $om_edition->view_pdf();
        die();
    }

    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        //
        if ($this->valF["premier_impair"]%2 == 0) {
            $this->correct = false;
            $this->msg .= __("Le champ")." ".__("premier_impair")." ".__("doit être impair")."<br />";
        }
        if ($this->valF["dernier_impair"]%2 == 0) {
            $this->correct = false;
            $this->msg .= __("Le champ")." ".__("dernier_impair")." ".__("doit être impair")."<br />";
        }
        if ($this->valF["premier_pair"]%2 == 1) {
            $this->correct = false;
            $this->msg .= __("Le champ")." ".__("premier_pair")." ".__("doit être pair")."<br />";
        }
        if ($this->valF["dernier_pair"]%2 == 1) {
            $this->correct = false;
            $this->msg .= __("Le champ")." ".__("dernier_pair")." ".__("doit être pair")."<br />";
        }
    }

    /**
     * Paramétrage des VIEW formulaire & sousformulaire - setLayout.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        //
        $form->setGroupe("premier_impair", "D");
        $form->setGroupe("dernier_impair", "F");
        $form->setGroupe("premier_pair", "D");
        $form->setGroupe("dernier_pair", "F");
        //
        $form->setRegroupe("id", "D", __("Identification"));
        $form->setRegroupe("code_voie", "G", "");
        $form->setRegroupe("bureau", "F", "");
        //
        $form->setRegroupe("premier_impair", "D", __("Positionnement"));
        $form->setRegroupe("dernier_impair", "G", "");
        $form->setRegroupe("premier_pair", "G", "");
        $form->setRegroupe("dernier_pair", "F", "");
    }

    /**
     * Paramétrage des VIEW formulaire & sousformulaire - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("bureau", __("Bureau"));
        $form->setLib("code_voie", __("Voie"));
        $form->setLib("premier_impair", __("Premier numero impair"));
        $form->setLib("dernier_impair", __("Dernier numero impair"));
        $form->setLib("premier_pair", __("Premier numero pair"));
        $form->setLib("dernier_pair", __("Dernier numero pair"));
    }

    /**
     * Paramétrage des VIEW formulaire & sousformulaire - setOnchange.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange("premier_impair", "VerifNumDecoupage(this)");
        $form->setOnchange("dernier_impair", "VerifNumDecoupage(this)");
        $form->setOnchange("premier_pair", "VerifNumDecoupage(this)");
        $form->setOnchange("dernier_pair", "VerifNumDecoupage(this)");
    }

    /**
     * Paramétrage des VIEW formulaire & sousformulaire - set_form_default_values.
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        //
        if ($validation == 0 && $maj == 0) {
            $form->setVal("premier_impair", 1);
            $form->setVal("dernier_impair", 90001);
            $form->setVal("premier_pair", 0);
            $form->setVal("dernier_pair", 90000);
        }
    }
}
