<?php
/**
 * Ce script définit la classe 'reuSyncAncienMouvementInscriptionTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_mouvement.class.php";

/**
 * Définition de la classe 'reuSyncAncienMouvementInscriptionTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncAncienMouvementInscriptionTraitement extends reuSyncMouvementTraitement {
    /**
     *
     */
    var $fichier = "reu_sync_ancien_mouvement_inscription";
    /**
     *
     */
    function displayBeforeContentForm() {
        echo $this->get_displayBeforeContentForm("inscription");
    }
    /**
     *
     */
    function treatment() {
        $this->LogToFile("start reu_sync_ancien_mouvement_inscription");
        //
        if ($this->f->getParameter("reu_sync_l_e_valid") != "done") {
            $this->error = true;
            $message = __("Cette fonctionnalité n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.");
            $this->addToMessage($message);
            $this->LogToFile($message);
            $this->LogToFile("end reu_sync_ancien_mouvement_inscription");
            return;
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->error = true;
            $this->addToMessage("Erreur de connexion REU.");
            $this->LogToFile("Erreur de connexion REU.");
            $this->LogToFile("end reu_sync_ancien_mouvement_inscription");
            return;
        }
        if ($inst_reu->is_connexion_standard_valid() !== true) {
            $this->error = true;
            $this->addToMessage(sprintf(
                '%s : %s',
                __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                sprintf(
                    '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                    __("cliquez ici pour vérifier")
                )
            ));
            $this->LogToFile("La connexion standard n'est pas valide.");
            $this->LogToFile("end reu_sync_ancien_mouvement_inscription");
            return;
        }
        //
        $errors = 0;
        $success = 0;
        $total = 0;
        //
        $inscriptions = $this->get_list_anciens_inscriptions_actif();
        $inscriptions_to_treat = $inscriptions['result'];
        if (count($inscriptions_to_treat) == 0) {
            $message = __("Aucune inscription à traiter.");
            $this->addToMessage($message);
            $this->LogToFile($message);
            $this->LogToFile("end reu_sync_ancien_mouvement_inscription");
            return;
        }
        $this->f->db->autoCommit(false);
        foreach ($inscriptions_to_treat as $key => $value) {
            $total += 1;
            $mouvement_id = intval($value["id"]);
            $inst_inscription = $this->f->get_inst__om_dbform(array(
                "obj" => "inscription",
                "idx" => $mouvement_id,
            ));
            if ($inst_inscription->exists() !== true) {
                $errors += 1;
                $inst_inscription->undoValidation();
                continue;
            }
            if ($inst_inscription->getVal('id_demande') == "") {
                //
                $inst_inscription->setValFFromVal();
                $val = $inst_inscription->valF;
                $val["types"] = "VOL1";
                $ret = $inst_inscription->handle_reu_demande_inscription("add", $mouvement_id, $val);
                if ($ret !== true) {
                    $errors += 1;
                    $this->LogToFile(sprintf(
                        "=> ECHEC Mouvement %s - Echec de la création du mouvement sur le REU",
                        $mouvement_id
                    ));
                    $this->LogToFile($mouvement_id." Erreur add");
                    $this->LogToFile($mouvement_id." ".$inst_inscription->msg);
                    $inst_inscription->undoValidation();
                    continue;
                }
                $this->f->db->commit();
                $inst_inscription->init_record_data($mouvement_id);
                $inst_inscription->msg = "";
            }
            if ($inst_inscription->getVal('id_demande') != ""
                && $inst_inscription->getVal('statut') == "ouvert") {
                $ret = $inst_inscription->completer(array(
                    "date_complet" => "01/01/2019",
                ));
                if ($ret !== true) {
                    $errors += 1;
                    $this->LogToFile(sprintf(
                        "=> ECHEC Mouvement %s - Demande REU %s - Echec de la mise à jour du mouvement > complet",
                        $mouvement_id,
                        $inst_inscription->getVal('id_demande')
                    ));
                    $this->LogToFile($mouvement_id." Erreur completer");
                    $this->LogToFile($mouvement_id." ".$inst_inscription->msg);
                    $inst_inscription->undoValidation();
                    continue;
                }
                $this->f->db->commit();
                $inst_inscription->init_record_data($mouvement_id);
                $inst_inscription->msg = "";
            }
            if ($inst_inscription->getVal('id_demande') != ""
                && $inst_inscription->getVal('statut') == "complet") {
                $ret = $inst_inscription->viser(array(
                    "date_visa" => "01/01/2019",
                    "visa" => "accepte",
                ));
                if ($ret !== true) {
                    $errors += 1;
                    $this->LogToFile(sprintf(
                        "=> ECHEC Mouvement %s - Demande REU %s - Echec lors du Visa de l'inscription",
                        $mouvement_id,
                        $inst_inscription->getVal('id_demande')
                    ));
                    $this->LogToFile($mouvement_id." Erreur viser");
                    $this->LogToFile($mouvement_id." ".$inst_inscription->msg);
                    $inst_inscription->undoValidation();
                    continue;
                }
                $this->f->db->commit();
            }
            $success += 1;
            $this->LogToFile($mouvement_id." OK");
        }
        $this->addToMessage(sprintf(
            __('Transmission au REU de %2$s inscription(s) avec succès sur un total de %1$s.'),
            $total,
            $success
        ));
        if ($errors != 0) {
            $this->error = true;
        }
        $this->LogToFile("end reu_sync_ancien_mouvement_inscription");
    }
}
