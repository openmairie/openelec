<?php
/**
 * Ce script contient la définition de la classe *periode*.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/periode.class.php";

/**
 * Définition de la classe *periode* (om_dbform).
 */
class periode extends periode_gen {

    /**
     * CHECK_TREATMENT - verifier.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        // calcul du temps passe et verification des dates saisies
        if ($this->valF['debut']!="" and $this->valF['fin']!="") {
            $heureD = explode(":", $this->valF['debut']);
            $heureF = explode(":", $this->valF['fin']);
            $heure = $heureF[0]-$heureD[0];
            $minute = $heureF[1]-$heureD[1];
            $total="00:00";
            if ($heure<0) {
                $this->correct=false;
                $this->msg .= __("Heure fde in INFERIEURE à l'heure de départ");
            }else{
                if($minute<0){
                    if($heure>0){
                        $heure=$heure-1;
                        $minute=$minute+60;
                        $total = $heure.":".$minute;
                    }else{
                        $this->correct=false;
                        $this->msg .= __("Heure fde in INFERIEURE à l'heure de départ");
                    }
                }else{
                    $total = $heure.":".$minute;
                }
                $this->msg .= sprintf(__("Heure(s) effectuées %s:%s"), $heure, $minute);
            }
        }
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    public function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        $form->setOnchange("debut", "ftime(this)");
        $form->setOnchange("fin", "ftime(this)");
    }

    /**
     * CHECK_TREATMENT - cleSecondaire.
     *
     * @return void
     */
    public function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        parent::cleSecondaire($id);
        // verification de la suppression
        if($id == 'matin' or $id == 'apres-midi' or $id== 'journee'){
            $this->msg .= sprintf(__("SUPPRESSION NON AUTORISÉE pour la période '%s' (utilisée dans les traitements)"), $id);
            $this->correct=false;
        }
    }
}
