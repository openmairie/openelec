<?php
/**
 * Ce script définit la classe 'module_reu'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_reu' (module).
 */
class module_reu extends module {

    /**
     * CONDITION - reu_sync_l_e_valid_is_done
     *
     * @return boolean
     */
    function reu_sync_l_e_valid_is_done() {
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done") {
            return true;
        }
        return false;
    }

    /**
     *
     * @return void
     */
    function init_module() {
        $this->module = array(
            "right" => "module_reu_connexion_standard",
            "title" => __("Traitement")." -> ".__("Module REU"),
            "condition" => array(
                "is_collectivity_mono",
            ),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_reu",
                    "id" => "module_reu-onglet-main",
                    "href" => "../app/index.php?module=module_reu&view=main",
                    "title" => __("statut"),
                    "view" => "main",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_reu",
                    "href" => "../app/index.php?module=module_reu&view=synchronisation_liste_electorale",
                    "title" => __("synchronisation liste electorale"),
                    "view" => "synchronisation_liste_electorale",
                    "id" => "module_reu-onglet-synchronisation_liste_electorale",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_reu",
                    "href" => "../app/index.php?module=module_reu&view=transmission_anciens_mouvements",
                    "title" => __("transmission mouvements 2018"),
                    "view" => "transmission_anciens_mouvements",
                    "id" => "module_reu-onglet-transmission_anciens_mouvements",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_reu",
                    "href" => "../app/index.php?module=module_reu&view=transmission_par_lots",
                    "title" => __("transmission par lots"),
                    "view" => "transmission_par_lots",
                    "id" => "module_reu-onglet-transmission_par_lots",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_reu",
                    "href" => OM_ROUTE_SOUSTAB."&obj=reu_notification&amp;idxformulaire=reu&amp;retourformulaire=reu",
                    "title" => __("notification(s)"),
                    "id" => "module_reu-onglet-notification",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_reu-onglet-synchronisation_referentiel",
                    "href" => "../app/index.php?module=module_reu&view=synchronisation_referentiel",
                    "title" => __("synchronisation référentiel"),
                    "view" => "synchronisation_referentiel",
                    "id" => "module_reu-onglet-synchronisation_referentiel",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_reu-onglet-check",
                    "href" => "../app/index.php?module=module_reu&view=check",
                    "title" => __("vérifications"),
                    "view" => "check",
                    "id" => "module_reu-onglet-check",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_reu-onglet-api",
                    "href" => "../app/index.php?module=module_reu&view=api",
                    "title" => __("api"),
                    "view" => "api",
                    "id" => "module_reu-onglet-api",
                ),
                array(
                    "type" => "overlay",
                    "right" => "module_reu_modifier_ugle",
                    "href" => "../app/index.php?module=module_reu&view=change_ugle",
                    "title" => __("définir/modifier ugle"),
                    "view" => "change_ugle",
                ),
                array(
                    "type" => "overlay",
                    "right" => "module_reu_modifier_compte_logiciel",
                    "href" => "../app/index.php?module=module_reu&view=change_compte_logiciel",
                    "title" => __("définir/modifier compte logiciel"),
                    "view" => "change_compte_logiciel",
                ),
                array(
                    "type" => "overlay",
                    "right" => "module_reu_connexion_standard",
                    "href" => "../app/index.php?module=module_reu&view=connexion_standard",
                    "title" => __("connexion standard"),
                    "view" => "connexion_standard",
                ),
                array(
                    "type" => "overlay",
                    "right" => "module_reu_connexion_standard",
                    "href" => "../app/index.php?module=module_reu&view=deconnexion_standard",
                    "title" => __("déconnexion standard"),
                    "view" => "deconnexion_standard",
                ),
            ),
        );
    }

    /**
     * VIEW - view__main.
     *
     * @return void
     */
    function view__main() {
        //
        printf(
            '
            <div class="container-fluid" id="reu-main-tab">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                Statut
                            </div>
                            <div class="card-body">
                                %1$s
                            </div>
                        </div>
                        %5$s
                    </div>
                    <div class="col-sm-4">
                        %3$s
                        %4$s
                    </div>
                    <div class="col-sm-4">
                        %2$s
                    </div>
                </div>
            </div>
            <div id="tabs-1"><div class="formControls"></div></div><!-- nécessaire pour le popupit -->
            <div class="visualClear"></div>
            ',
            $this->get_display__global_status(),
            $this->get_display__actualites(),
            $this->get_display__ugle(),
            $this->get_display__compte_logiciel(),
            $this->get_display__bureaux_status()
        );
    }

    /**
     * VIEW - view_transmission_par_lots.
     *
     * @return void
     */
    function view__transmission_par_lots() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }
        if ($this->reu_sync_l_e_valid_is_done() !== true) {
            $this->f->displayMessage(
                "error",
                __("Cet écran n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.")
            );
            return;
        }

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Validation des modifications par lots");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_mouvement_modification.class.php";
        $trt = new reuSyncMouvementModificationTraitement();
        $trt->displayForm();
        echo "</div>\n";
        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Mise à jour des électeurs REU sans bureau de vote");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_reu_sans_bureau.class.php";
        $trt = new reuSyncLEREUSansBureauTraitement();
        $trt->displayForm();
        echo "</div>\n";
        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Électeurs avec informations à transmettre au REU");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_reu_a_transmettre.class.php";
        $trt = new reuSyncLEREUATransmettreTraitement();
        $trt->displayForm();
        echo "</div>\n";
    }

    /**
     * VIEW - view_transmission_anciens_mouvements.
     *
     * @return void
     */
    function view__transmission_anciens_mouvements() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }
        if ($this->reu_sync_l_e_valid_is_done() !== true) {
            $this->f->displayMessage(
                "error",
                __("Cet écran n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.")
            );
            return;
        }

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Transmission des radiations");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_ancien_mouvement_radiation.class.php";
        $trt = new reuSyncAncienMouvementRadiationTraitement();
        $trt->displayForm();
        echo "</div>\n";

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Transmission des inscriptions");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_ancien_mouvement_inscription.class.php";
        $trt = new reuSyncAncienMouvementInscriptionTraitement();
        $trt->displayForm();
        echo "</div>\n";

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Transmission des modifications");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_ancien_mouvement_modification.class.php";
        $trt = new reuSyncAncienMouvementModificationTraitement();
        $trt->displayForm();
        echo "</div>\n";
    }

    /**
     * VIEW - view__api.
     *
     * @return void
     */
    function view__api() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Requête sur l'API");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_api.class.php";
        $trt = new reuApiTraitement();
        $trt->displayForm();
        echo "</div>\n";
    }

    /**
     * VIEW - view__check.
     *
     * @return void
     */
    function view__check() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }
        if ($this->reu_sync_l_e_valid_is_done() !== true) {
            $this->f->displayMessage(
                "error",
                __("Cet écran n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.")
            );
            return;
        }

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Synchronisation de la liste électorale pour régularisation (triplon)");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_reg.class.php";
        $trt = new reuSyncLERegTraitement();
        $trt->displayForm();
        echo "</div>\n";

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Vérification");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_check.class.php";
        $trt = new reuSyncLECheckTraitement();
        $trt->displayForm();
        echo "</div>\n";
    }


    /**
     * VIEW - view__synchronisation_referentiel.
     *
     * @return void
     */
    function view__synchronisation_referentiel() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Synchronisation des lieux de naissance");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_referentiel_lieu_naissance.class.php";
        $trt = new reuSyncReferentielLieuNaissanceTraitement();
        $trt->displayForm();
        echo "</div>\n";

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Synchronisation des bureaux de vote");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_bureau.class.php";
        $trt = new reuSyncBureauTraitement();
        $trt->displayForm();
        echo "</div>\n";

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Synchronisation des tables de référence");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_referentiel_misc.class.php";
        $trt = new reuSyncReferentielMiscTraitement();
        $trt->displayForm();
        echo "</div>\n";

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Mise à jour des électeurs avec un état civil différent dans le REU");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_reu_etat_civil.class.php";
        $trt = new reuSyncLEREUEtatCivilTraitement();
        $trt->displayForm();
        echo "</div>\n";

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Mise à jour du numéro d'ordre de l'électeur depuis le REU");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_reu_numero_ordre.class.php";
        $trt = new reuSyncLEREUNumeroOrdreTraitement();
        $trt->displayForm();
        echo "</div>\n";

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Mise à jour de la provenance des demandes sur les mouvements");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_mouvement_provenance_demande.class.php";
        $trt = new reuSyncLEMouvementProvenanceDemandeTraitement();
        $trt->displayForm();
        echo "</div>\n";
    }

    /**
     * VIEW - view__synchronisation_liste_electorale.
     *
     * @return void
     */
    function view__synchronisation_liste_electorale() {
        //
        if ($this->reu_sync_l_e_valid_is_done() === true) {
            $this->f->displayMessage(
                "valid",
                sprintf(
                    __("Initialisation terminée avec une extraction au %s."),
                    $this->f->getParameter("reu_sync_l_e_init")
                )
            );
            return;
        }
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->f->displayMessage(
                "error",
                __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
            );
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($this->f->getParameter("reu_sync_bureaux") !== "true") {
            if ($reu_is_down !== false) {
                $this->f->displayMessage(
                    "error",
                    __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
                );
                return;
            }
            $this->f->displayMessage(
                "error",
                __("Synchronisation impossible. Les bureaux de vote ne sont pas cohérents. Contactez votre administrateur.")
            );
            return;
        }

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Étape 1 - Initialisation de la synchronisation");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_init.class.php";
        $trt1 = new reuSyncLEInitTraitement();
        if ($trt1->is_done() === true) {
            $this->f->displayMessage("ok", __("L'étape 1 est terminée."));
            $trt1->displayBeforeContentForm();
        } else {
            if ($reu_is_down === true) {
                $this->f->displayMessage(
                    "error",
                    __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.")
                );
            } else {
                $trt1->displayForm();
            }
        }
        echo "</div>\n";

        if ($trt1->is_done() !== true) {
            return;
        }
        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Étape 2 - Rattachement des électeurs");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_attach.class.php";
        $trt2 = new reuSyncLEAttachTraitement();
        if ($trt2->is_done() === true) {
            $this->f->displayMessage("ok", __("Le rattachement automatique des électeurs est terminé."));
            $trt2->displayBeforeContentForm();
        } else {
            $trt2->displayForm();
        }
        echo "</div>\n";

        if ($trt2->is_done() !== true) {
            return;
        }

        /**
         *
         */
        echo "<div class=\"paragraph\">\n";
        $subtitle = "-> ".__("Étape 3 - Validation de la synchronisation");
        $this->f->displaySubTitle($subtitle);
        require_once "../obj/traitement.reu_sync_l_e_valid.class.php";
        $trt3 = new reuSyncLEValidTraitement();
        if ($trt3->is_done() === true) {
            $this->f->displayMessage("ok", __("L'étape 3 est terminée."));
            $trt3->displayBeforeContentForm();
        } else {
            $trt3->displayForm();
        }
        echo "</div>\n";
    }

    /**
     * Affichage du bloc d'infos sur le statut de la connexion avec le REU.
     *
     * @return string
     */
    function get_display__global_status() {
        //
        $global_status = array(
            "config_system" => array(
                "libelle" => __("Configuration système"),
                "description" => __("Pour se connecter au REU, un fichier de configuration avec les urls et les identifiants du prestataire doit être paramétré par l'administrateur technique."),
                "status" => __("ERROR"),
            ),
            "param_ugle" => array(
                "libelle" => __("Paramétrage UGLE"),
                "description" => __("Pour qu'openElec puisse dialoguer avec le REU, il est nécessaire de paramétrer le code de l'unité de gestion des listes électorales."),
                "status" => __("ERROR"),
            ),
            "param_compte_logiciel" => array(
                "libelle" => __("Paramétrage Compte logiciel"),
                "description" => __("Pour qu'openElec puisse dialoguer avec le REU, il est nécessaire de paramétrer un compte logiciel c'est à dire un identifiant et un mot de passe généré directement dans le protail ELIRE."),
                "status" => __("ERROR"),
            ),
            "api_reu_dispo" => array(
                "libelle" => __("API REU disponible"),
                "description" => __("Le service REU peut ne plus être accessible."),
                "status" => __("ERROR"),
            ),
            "connexion_logicielle" => array(
                "libelle" => __("Connexion logicielle"),
                "description" => __("Les identifiants sont corrects, openElec peut se connecter."),
                "status" => __("ERROR"),
            ),
        );
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu !== null) {
            //
            if ($inst_reu->is_config_valid() === true) {
                $global_status["config_system"]["status"] = __("OK");
            }
            // Est-ce qu'il y a un ugle correct ?
            if ($inst_reu->get_ugle() !== null) {
                $global_status["param_ugle"]["status"] = __("OK");
            }
            // Est-ce qu'il y a un compte logiciel paramétré ?
            if ($inst_reu->get_compte_logiciel() !== null) {
                $global_status["param_compte_logiciel"]["status"] = __("OK");
            }
            // // Est-ce que l'API REU est UP ?
            if ($inst_reu->is_api_up() === true) {
                $global_status["api_reu_dispo"]["status"] = __("OK");
            }
            // // Est-ce que la connexion logicielle est autorisée ?
            if ($inst_reu->is_connexion_logicielle_valid() === true) {
                $global_status["connexion_logicielle"]["status"] = __("OK");
            }
        }
        //
        $out = '<table class="table table-sm">';
        foreach ($global_status as $key => $value) {
            $out .= sprintf(
                '<tr><td>%s</td><td><span id="%s" class="badge badge-%s">%s</span></td>',
                $value["libelle"],
                $key,
                ($value["status"] == "OK" ? "success" : "danger"),
                $value["status"]
            );
        }
        $out .= '</table>';
        return $out;
    }
    /**
     * Affichage du bloc d'infos sur les bureaux de vote.
     *
     * @return string
     */
    function get_display__bureaux_status() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            return "";
        }
        //
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            return "";
        }
        //
        $content = "";
        if ($this->f->getParameter("reu_sync_bureaux") !== "true") {
            $content .= sprintf(
                '<span id="%s" class="badge badge-danger">ERROR</span>',
                "bureaux-status"
            );
        } else {
            $content .= sprintf(
                '<span id="%s" class="badge badge-success">OK</span><br/><br/><p>%s</p>',
                "bureaux-status",
                __("Les bureaux de vote et leurs codes sont identiques entre openElec et le REU.")
            );
        }
        //
        $bureaux_de_vote_reu = $inst_reu->get_bureaux_de_vote();
        $bureaux_de_vote_oel = $this->f->get_all__bureau__by_my_collectivite();
        //
        $global_error_status = false;
        //
        $bureaux_oel_manquants = array();
        $bureaux_reu_manquants = array();
        $error_circonscription_canton_reu = false;
        //
        if ($bureaux_de_vote_reu["code"] == 200) {
            foreach ($bureaux_de_vote_oel as $oel_bureau) {
                $exists = false;
                foreach ($bureaux_de_vote_reu["result"] as $reu_bureau) {
                    if ($oel_bureau["code"] == $reu_bureau["code"]
                        && $oel_bureau["referentiel_id"] == $reu_bureau["id"]) {
                        //
                        $exists = true;
                        if ($oel_bureau["circonscription_code"] !== gavoe($reu_bureau, array("circonscriptionLegislative", "code", ))
                            && $oel_bureau["canton_code"] !== gavoe($reu_bureau, array("canton", "code", ))) {
                            $error_circonscription_canton_reu = true;
                            $global_error_status = true;
                        }
                        continue;
                    }
                }
                if ($exists !== true) {
                    $global_error_status = true;
                    $bureaux_oel_manquants[] = $oel_bureau["code"];
                }
            }
            foreach ($bureaux_de_vote_reu["result"] as $reu_bureau) {
                $exists = false;
                foreach ($bureaux_de_vote_oel as $oel_bureau) {
                    if ($oel_bureau["code"] == $reu_bureau["code"]) {
                        $exists = true;
                        continue;
                    }
                }
                if ($exists !== true) {
                    $global_error_status = true;
                    $bureaux_reu_manquants[] = $reu_bureau["code"];
                }
            }
            if ($global_error_status !== false) {
                if ($error_circonscription_canton_reu === true) {
                    $content .= sprintf(
                        '<br/><br/><p>%s</p>',
                        sprintf(
                            __("Toutes les informations de cantons et de circonscriptions ne sont pas renseignées dans le REU.")
                        )
                    );
                }
                if (count($bureaux_de_vote_oel) != count($bureaux_de_vote_reu["result"])) {
                    $global_error_status = true;
                    $content .= sprintf(
                        '<br/><br/><p>%s</p>',
                        sprintf(
                            __("Le nombre de bureaux de vote est différent entre openElec (%s) et le REU (%s)."),
                            count($bureaux_de_vote_oel),
                            count($bureaux_de_vote_reu["result"])
                        )
                    );
                }
            }
            if (count($bureaux_oel_manquants) != 0) {
                $content .= sprintf(
                    '<br/><p>%s</p>',
                    __("Bureaux openElec qui n'existent pas dans le REU :")
                );
                $content .= "<ul>";
                foreach ($bureaux_oel_manquants as $value) {
                    $content .= sprintf(
                        "<li>%s</li>",
                        $value
                    );
                }
                $content .= "</ul>";
            }
            if (count($bureaux_reu_manquants) != 0) {
                $content .= sprintf(
                    '<br/><p>%s</p>',
                    __("Bureaux du REU qui n'existent pas dans openElec :")
                );
                $content .= "<ul>";
                foreach ($bureaux_reu_manquants as $value) {
                    $content .= sprintf(
                        "<li>%s</li>",
                        $value
                    );
                }
                $content .= "</ul>";
            }
        }

        //
        $out = sprintf(
            '
            <div class="card">
                <div class="card-header">
                    Bureaux de vote
                </div>
                <div class="card-body">
                    %1$s
                </div>
            </div>
            ',
            $content
        );
        return $out;
    }

    /**
     * Affichage du bloc d'infos sur les actualités du REU.
     *
     * @return string
     */
    function get_display__actualites() {
        $bloc_template = '
        <div class="card">
            <div class="card-header">
                %s
            </div>
            <div id="actu" class="card-body text-center">
                <img src="../app/img/logo-marianne.png" alt="-">
                <h4>Actualités</h4>
                %s
            </div>
        </div>
        ';
        $output = $this->get_display__actualites_list();
        if ($output === "") {
            $output = __("Aucune actualité.");
        }
        return sprintf(
            $bloc_template,
            __("Répertoire Électoral Unique"),
            $output
        );
    }

    /**
     * Retourne le bloc d'affichage d'une liste d'actualités du REU.
     *
     * Si une erreur survient ou si il n'y a aucune actualité alors on retourne
     * une chaîne vide.
     *
     * @return string
     */
    function get_display__actualites_list() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            return "";
        }
        $ret = $inst_reu->get_actualites();
        if (is_array($ret) !== true
            || array_key_exists("code", $ret) !== true
            || $ret["code"] != "200"
            || count($ret["result"]) == 0) {
            return "";
        }
        $line_template = '
        <span class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">%s</h5>
                <small>%s</small>
            </div>
            <p class="mb-1">%s</p>
        </span>
        ';
        $out = '<div class="list-group">';
        foreach ($ret["result"] as $key => $value) {
            $out .= sprintf(
                $line_template,
                $value["titre"],
                $value["dateDebut"],
                $value["texte"]
            );
        }
        $out .= '</div>';
        return $out;
    }

    /**
     * Affichage du bloc d'infos sur l'UGLE.
     *
     * @return string
     */
    function get_display__ugle() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            return "";
        }
        //
        $ugle = $inst_reu->get_ugle();
        //
        $edit = "";
        if ($this->f->isAccredited("module_reu_modifier_ugle") === true) {
            $edit = sprintf(
                '<a id="action-reu-overlay-change-ugle" href="../app/index.php?module=module_reu&view=change_ugle">Définir/Modifier</a>'
            );
        }
        //
        $out = sprintf(
            '
            <div class="card">
                <div class="card-header">
                    %s
                </div>
                <div class="card-body text-center">
                    <h4 class="card-title">%s</h4>
                    %s
                </div>
            </div>
            ',
            __("UGLE"),
            ($ugle === null ? __("Non défini.") : $ugle),
            $edit
        );
        return $out;
    }

    /**
     * Affichage du bloc d'infos sur le compte logiciel.
     *
     * @return string
     */
    function get_display__compte_logiciel() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            return "";
        }
        //
        $compte_logiciel = $inst_reu->get_compte_logiciel();
        //
        $edit = "";
        if ($this->f->isAccredited("module_reu_modifier_compte_logiciel") === true) {
            $edit = sprintf(
                '<a id="action-reu-overlay-change-compte-logiciel" href="../app/index.php?module=module_reu&view=change_compte_logiciel">Définir/Modifier</a>'
            );
        }
        //
        $out = sprintf(
            '
            <div class="card">
                <div class="card-header">
                    %s
                </div>
                <div class="card-body text-center">
                    <h4 class="card-title">%s</h4>
                    %s
                </div>
            </div>
            ',
            __("Compte Logiciel"),
            ($compte_logiciel === null ? __("Non défini.") : $compte_logiciel["username"]."/*****"),
            $edit
        );
        return $out;
    }

    /**
     * VIEW - view__connexion_standard.
     *
     * @return void
     */
    function view__connexion_standard() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            return;
        }
        if ($inst_reu->is_connexion_logicielle_valid() !== true) {
            $this->f->setFlag("htmlonly");
            $this->f->display();
            $this->f->displayMessage(
                "error",
                ERROR_MSG_REU_CONNECT
            );
            return;
        }
        $inst_reu->display_connexion_standard();
    }

    /**
     * VIEW - view__deconnexion_standard.
     *
     * @return void
     */
    function view__deconnexion_standard() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            return;
        }
        $inst_reu->display_deconnexion_standard();
    }

    /**
     * VIEW - view__change_ugle.
     *
     * @return void
     */
    function view__change_ugle() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            return;
        }
        // TREATMENT
        if (isset($_POST['changeugle_action_valid']) === true) {
            //
            $ugle = $this->f->get_submitted_post_value('ugle');
            //
            $ret = $inst_reu->set_ugle($ugle);
            if ($ret === true) {
                $this->f->displayMessage(
                    "ok",
                    __("Vos modifications ont bien été enregistrées.")
                );
            } else {
                $this->f->displayMessage(
                    "error",
                    __("Une erreur est survenue. Contactez votre administrateur.")
                );
            }
            return;
        }

        // FORM
        echo "\n<div id=\"changeugleform\" class=\"formulaire\">\n";
        //
        $this->f->layout->display__form_container__begin(array(
            "id" => "changeugleform_form",
            "action" => "",
            "name" => "f2",
            "onsubmit" => "affichersform('reu-change-form', '../app/index.php?module=module_reu&view=change_ugle', this);return false;",
        ));
        //
        echo "\t<div class=\"field\">\n";
        echo "\t\t<label for=\"ugle\">".__("UGLE")."</label>\n";
        echo "\t\t<input type=\"text\" name=\"ugle\" ";
        echo "maxlength=\"10\" size=\"10\" class=\"champFormulaire\" ";
        echo "tabindex=\"1\" size=\"15\" ";
        echo "value=\"".$this->f->getParameter("ugle")."\"/>\n";
        echo "\t</div>\n";
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "class" => "context boutonFormulaire",
            "name" => "changeugle.action.valid",
            "value" => __("Valider"),
        ));
        $this->f->layout->display_form_retour(array(
            "id" => "form-action-change-ugle-back-".uniqid(),
            "href" => "../app/index.php?module=module_reu",
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
        //
        echo "</div>\n";
    }

    /**
     * VIEW - view__change_compte_logiciel.
     *
     * @return void
     */
    function view__change_compte_logiciel() {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            return;
        }
        // TREATMENT
        if (isset($_POST['changecompte_logiciel_action_valid']) === true) {
            //
            $compte_logiciel_username = $this->f->get_submitted_post_value("compte_logiciel_username");
            $compte_logiciel_password = $this->f->get_submitted_post_value("compte_logiciel_password");
            $ret = $inst_reu->set_compte_logiciel($compte_logiciel_username, $compte_logiciel_password);
            if ($ret === true) {
                $this->f->displayMessage(
                    "ok",
                    __("Vos modifications ont bien été enregistrées.")
                );
            } else {
                $this->f->displayMessage(
                    "error",
                    __("Une erreur est survenue. Contactez votre administrateur.")
                );
            }
            return;
        }

        // FORM
        echo "\n<div id=\"changecompte_logicielform\" class=\"formulaire\">\n";
        //
        $this->f->layout->display__form_container__begin(array(
            "id" => "changecompte_logicielform_form",
            "action" => "",
            "name" => "f2",
            "onsubmit" => "affichersform('reu-change-form', '../app/index.php?module=module_reu&view=change_compte_logiciel', this);return false;",
        ));
        //
        echo "\t<div class=\"field\">\n";
        echo "\t\t<label for=\"compte_logiciel_username\">".__("username")."</label>\n";
        echo "\t\t<input type=\"text\" name=\"compte_logiciel_username\" ";
        echo "class=\"champFormulaire\" ";
        echo "tabindex=\"1\" size=\"15\" ";
        echo "value=\"\"/>\n";
        echo "\t</div>\n";
        //
        echo "\t<div class=\"field\">\n";
        echo "\t\t<label for=\"compte_logiciel_password\">".__("password")."</label>\n";
        echo "\t\t<input type=\"password\" name=\"compte_logiciel_password\" ";
        echo "class=\"champFormulaire\" ";
        echo "tabindex=\"1\" size=\"15\" ";
        echo "value=\"\"/>\n";
        echo "\t</div>\n";
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "class" => "context boutonFormulaire",
            "name" => "changecompte_logiciel.action.valid",
            "value" => __("Valider"),
        ));
        $this->f->layout->display_form_retour(array(
            "id" => "form-action-change-compte-logiciel-back-".uniqid(),
            "href" => "../app/index.php?module=module_reu",
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
        //
        echo "</div>\n";
    }
}
