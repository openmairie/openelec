<?php
/**
 * Ce script définit la classe 'module_refonte'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_refonte' (module).
 */
class module_refonte extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_refonte",
            "title" => __("Traitement")." -> ".__("Module Refonte"),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_refonte-onglet_traitement_refonte",
                    "href" => "../app/index.php?module=module_refonte&view=refonte",
                    "title" => __("Refonte"),
                    "view" => "refonte",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_refonte-onglet_numerobureau",
                    "href" => "../app/index.php?module=soustab&obj=numerobureau&retourformulaire=liste&idxformulaire=".$_SESSION["liste"],
                    "title" => __("Séquences - Numéro Bureau"),
                ),
            ),
        );
    }

    /**
     *
     */
    function view__refonte() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_refonte");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        //
        $description = __("La refonte a lieue tous les trois a cinq ans. Elle permet ".
                         "la renumérotation complete de la liste électorale. Chaque ".
                         "électeur se voit attribuer un nouveau numero dans son bureau. ".
                         "La numérotation est effectuée en fonction de l'ordre alphabétique ".
                         "des électeurs. La refonte est suivie de l'édition de la totalité ".
                         "des cartes d'électeur.");
        $this->f->displayDescription($description);

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Renumérotation de la liste électorale");
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.refonte.class.php";
        $trt = new refonteTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }
}
