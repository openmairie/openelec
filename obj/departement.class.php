<?php
/**
 * Ce script définit la classe 'departement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/departement.class.php";

/**
 * Définition de la classe 'departement' (om_dbform).
 */
class departement extends departement_gen {

    /**
     *
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //
        $this->rechercheTable($this->f->db, "commune", "code_departement", $id);
        $this->rechercheTable($this->f->db, "electeur", "code_departement_naissance", $id);
        $this->rechercheTable($this->f->db, "mouvement", "code_departement_naissance", $id);
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("code", __("Code"));
        $form->setLib("libelle_departement", __("Libellé"));
    }

    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange("libelle_departement", "this.value=this.value.toUpperCase()");
        $form->setOnchange("code", "this.value=this.value.toUpperCase()");
    }
}
