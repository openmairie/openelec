<?php
/**
 * Ce script définit la classe 'reuSyncReferentielLieuNaissanceTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'reuSyncReferentielLieuNaissanceTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncReferentielLieuNaissanceTraitement extends traitement {
    var $fichier = "reu_sync_referentiel_lieu_naissance";

    var $tables = array("commune", "pays", "ugle", );
    /**
     * 
     */
    protected function count_and_log_tables() {
        foreach ($this->tables as $table) {
            $query = sprintf(
                'SELECT count(*) FROM %1$sreu_referentiel_%2$s',
                DB_PREFIXE,
                $table
            );
            $res = $this->f->db->getone($query);
            if ($this->f->isDatabaseError($res, true)) {
                $this->error = true;
                $message = sprintf(
                    __("Erreur de base de données lors de la vérification des chiffres %s."),
                    $table
                );
                $this->addToMessage($message);
                $this->LogToFile("end ".$this->fichier);
                return;
            }
            $this->LogToFile($res." ".$table);
        }
    }

    /**
     *
     */
    function treatment () {
        $this->LogToFile("start ".$this->fichier);
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            $this->LogToFile($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            $this->LogToFile($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        //
        $this->count_and_log_tables();
        //
        $inst_reu = $this->f->get_inst__reu();
        //
        $uid_commune_csv = $inst_reu->get_referentiel_lieu_in_csv("commune");
        $this->LogToFile("-> téléchargement du référentiel commune");
        $this->LogToFile($uid_commune_csv);
        $uid_pays_csv = $inst_reu->get_referentiel_lieu_in_csv("pays");
        $this->LogToFile("-> téléchargement du référentiel pays");
        $this->LogToFile($uid_pays_csv);
        $uid_ugle_csv = $inst_reu->get_referentiel_lieu_in_csv("ugle");
        $this->LogToFile("-> téléchargement du référentiel ugle");
        $this->LogToFile($uid_ugle_csv);
        //
        if ($uid_commune_csv === null
            || $uid_pays_csv === null
            || $uid_ugle_csv === null) {
            $message = __("Erreur lors de la récupération des données du référentiel. Essayez plus tard.");
            $this->LogToFile($message);
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            $this->error = true;
            return;
        }
        $sql = sprintf(
            'DELETE FROM %1$sreu_referentiel_commune;
            COPY %1$sreu_referentiel_commune from \'%2$s\' DELIMITERS \';\' CSV HEADER;
            DELETE FROM %1$sreu_referentiel_pays;
            COPY %1$sreu_referentiel_pays from \'%3$s\' DELIMITERS \';\' CSV HEADER;
            DELETE FROM %1$sreu_referentiel_ugle;
            COPY %1$sreu_referentiel_ugle from \'%4$s\' DELIMITERS \';\' CSV HEADER;',
            DB_PREFIXE,
            realpath($this->f->storage->getPath_temporary($uid_commune_csv)),
            realpath($this->f->storage->getPath_temporary($uid_pays_csv)),
            realpath($this->f->storage->getPath_temporary($uid_ugle_csv))
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->error = true;
            $message = __("Erreur de base de données lors de l'import des fichiers CSV.");
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        // Suppression des fichiers temporaires
        $ret = $this->f->storage->storage->temporary_storage->delete($uid_commune_csv);
        $this->LogToFile("Suppression du fichier temporaire ".$ret);
        $ret = $this->f->storage->storage->temporary_storage->delete($uid_pays_csv);
        $this->LogToFile("Suppression du fichier temporaire ".$ret);
        $ret = $this->f->storage->storage->temporary_storage->delete($uid_ugle_csv);
        $this->LogToFile("Suppression du fichier temporaire ".$ret);
        //
        $this->count_and_log_tables();
        //
        $this->LogToFile("end ".$this->fichier);
    }
}
