<?php
/**
 * Ce script définit la classe 'workdecoupage'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_base.class.php";

/**
 * Définition de la classe 'workdecoupage' (om_base).
 */
class workdecoupage extends om_base {

    /**
     * Constructeur.
     *
     * @return void
     */
    function __construct() {
        //
        $this->init_om_application();
    }

    function display_result_from_db_load_all($result = array()) {
        //
        $table = '
        <table class="table table-bordered table-condensed table-striped">
            <thead>
                %s
            </thead>
            <tbody>
                %s
            </tbody>
        </table>
        ';
        $head = '
            %s
        ';
        $head_line = '
            <tr>
                %s
            </tr>
        ';
        $head_cell = '
            <th>
                %s
            </th>
        ';
        $body = '
            %s
        ';
        $body_line = '
            <tr>
                %s
            </tr>
        ';
        $body_cell = '
            <td>
                %s
            </td>
        ';
        //
        $head_content = '';
        $body_content = '';
        //
        foreach ($result as $key => $line) {
            //
            if ($key == 0) {
                $colspan = count($line);
                foreach ($line as $title => $value) {
                    $head_content .= sprintf($head_cell, $title);
                }
            }
            //
            $line_content = "";
            foreach ($line as $value) {
                $line_content .= sprintf($body_cell, $value);
            }
            $body_content .= sprintf($body_line, $line_content);
        }
        echo sprintf(
            $table,
            sprintf(
                $head,
                sprintf(
                    $head_line,
                    $head_content
                )
            ),
            sprintf(
                $body,
                $body_content
            )
        );
    }

    function db_load_all($sql = "") {
        //
        $res = $this->f->db->query($sql);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $result = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            array_push($result, $row);
        }
        $res->free();
        //
        return $result;
    }

    /**
     *
     */
    function simulation() {
        //
        $query_nb_electeurs_par_bureau_avant_simulation = sprintf(
            'SELECT
                count(*)
            FROM
                %1$selecteur
            WHERE
                electeur.bureau=bureau.id
                AND electeur.liste=\'%2$s\'
            ',
            DB_PREFIXE,
            $_SESSION["liste"]
        );
        //
        $query_nb_electeurs_par_bureau_bureauforce = sprintf(
            'SELECT
                count(*)
            FROM
                %1$selecteur
            WHERE
                electeur.bureauforce=\'Oui\'
                AND electeur.bureau=bureau.id
                AND electeur.liste=\'%2$s\'',
            DB_PREFIXE,
            $_SESSION["liste"]
        );
        //
        $query_nb_electeurs_affectes_au_bureau_par_les_decoupages = sprintf(
            'SELECT
                count(*)
            FROM
                %1$selecteur
                LEFT JOIN %1$svoie ON electeur.code_voie=voie.code
                JOIN %1$sdecoupage ON 
                    decoupage.code_voie=electeur.code_voie 
                    AND decoupage.bureau=bureau.id 
                    AND 
                    ( 
                        CASE numero_habitation %% 2 
                        WHEN 0 THEN decoupage.premier_pair<=numero_habitation AND numero_habitation<=decoupage.dernier_pair
                        WHEN 1 THEN decoupage.premier_impair<=numero_habitation AND numero_habitation <= decoupage.dernier_impair 
                        END
                    )
            WHERE
                liste=\'%2$s\'
                AND decoupage.bureau = bureau.id
                AND bureauforce = \'Non\'',
            DB_PREFIXE,
            $_SESSION["liste"]
        );
        //
        $sql = sprintf(
            'SELECT
                (bureau.code||\' - \'||bureau.libelle) AS "bureau",
                (%3$s) as "nb electeurs avant simu",
                (%4$s) as "nb electeurs bureau force",
                (%4$s) + (%5$s) as "nb electeurs apres simu"
            FROM
                %1$sbureau
                LEFT OUTER JOIN %1$som_collectivite ON bureau.om_collectivite=om_collectivite.om_collectivite
            WHERE
                bureau.om_collectivite=%2$s
            GROUP BY
                bureau.om_collectivite,
                om_collectivite.om_collectivite,
                om_collectivite.libelle,
                bureau.id,
                bureau.code,
                bureau.libelle
            ORDER BY
                om_collectivite.libelle,
                om_collectivite.om_collectivite,
                bureau.code,
                bureau.libelle
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $query_nb_electeurs_par_bureau_avant_simulation,
            $query_nb_electeurs_par_bureau_bureauforce,
            $query_nb_electeurs_affectes_au_bureau_par_les_decoupages
        );
        $plop = $this->db_load_all($sql);
        $this->display_result_from_db_load_all($plop);

        //
        $query_nb_electeurs_bureauforce = sprintf(
            'SELECT count(*) FROM %1$selecteur WHERE electeur.bureauforce=\'Oui\' AND electeur.liste=\'%3$s\' AND electeur.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"]
        );
        //
        $query_nb_electeurs_total = sprintf(
            'SELECT count(*) FROM %1$selecteur WHERE electeur.liste=\'%3$s\' AND electeur.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"]
        );
        //
        $sql = sprintf(
            'SELECT
                (%1$s) as "nb electeur bureauforce",
                (%2$s) as "nb electeur total",
                ((%1$s)*100/(%2$s)||\'%%\') as "pourcentage electeur en bureauforce"',
            $query_nb_electeurs_bureauforce,
            $query_nb_electeurs_total
        );
        $plop = $this->db_load_all($sql);
        $this->display_result_from_db_load_all($plop);
    }
}
