<?php
//$Id$ 
//gen openMairie le 21/01/2019 14:41

require_once "../gen/obj/piece.class.php";

class piece extends piece_gen {

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mouvement() {
        return sprintf(
            'SELECT mouvement.id, mouvement.id FROM %1$smouvement ORDER BY mouvement.id ASC',
            DB_PREFIXE
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mouvement_by_id() {
        return sprintf(
            'SELECT mouvement.id, mouvement.id FROM %1$smouvement WHERE id = <idx>',
            DB_PREFIXE
        );
    }

}
