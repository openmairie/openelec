<?php
/**
 * Ce script définit la classe 'edition_pdf__etiquette'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__etiquette' (edition_pdf).
 */
class edition_pdf__etiquette extends edition_pdf {

    /**
     * Édition PDF - Étiquette.
     *
     * @return array
     */
    public function compute_pdf__etiquette($params = array()) {
        /**
         *
         */
        //
        $parametrage_etiquettes = $this->f->get_parametrage_etiquettes();
        //
        $DEBUG=0;
        //******************************************************************************
        //                          OBJET PDF                                         //
        //******************************************************************************
        $orientation="P";// orientation P-> portrait L->paysage
        $format="A4"; // format A3 A4 A5
        //******************************************************************************
        //                          PARAMETRES GENERAUX                               //
        //******************************************************************************
        $size = $parametrage_etiquettes["etiquette_taille_du_texte"]; // taille police
        $police = "courier"; // police courier,times,arial,helvetica
        $gras = ""; //$gras="B" -> BOLD OU $gras=""
        $C1 = "0";// couleur texte  R
        $C2 = "0";// couleur texte V
        $C3 = "0";// couleur texte B
        //
        $cadre = $parametrage_etiquettes["etiquette_bordure_etiquette"]; // cadre etiquette 1 -> oui 0 -> non
        $cadrechamps = $parametrage_etiquettes["etiquette_bordure_texte"]; // cadre  zones affichees 1 -> oui 0 -> non
        //******************************************************************************
        //                      PARAMETRES ETIQUETTE                                  //
        //******************************************************************************
        $_x_number = $parametrage_etiquettes["planche_nb_colonnes"]; // Nombre d'etiquettes sur la largeur de la page
        $_y_number = $parametrage_etiquettes["planche_nb_lignes"]; // Nombre d'etiquettes sur la hauteur de la page
        $_margin_left = $parametrage_etiquettes["planche_marge_ext_gauche"]; // Marge de gauche de l'etiquette
        $_margin_top = $parametrage_etiquettes["planche_marge_ext_haut"]; // Marge en haut de la page avant la premiere etiquette
        $_x_space = $parametrage_etiquettes["etiquette_espace_entre_colonnes"]; //Espace horizontal entre 2 bandes d'Ã©tiquettes
        $_y_space = $parametrage_etiquettes["etiquette_espace_entre_lignes"]; //Espace vertical entre 2 bandes d'Ã©tiquettes
        $_width = $parametrage_etiquettes["etiquette_largeur"]; // Largeur de chaque etiquette
        $_height = $parametrage_etiquettes["etiquette_hauteur"]; //Hauteur de chaque etiquette
        $_char_size = $parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"]; // Hauteur des caracteres
        $_line_height = $parametrage_etiquettes["etiquette_hauteur_de_ligne_du_texte"]; // Hauteur par defaut interligne

        /**
         *
         */
        //
        (isset($params['obj']) ? $obj = $params['obj'] : $obj = "");
        // Verifications des parametres
        if (strpos($obj, "/") !== false
            || !file_exists("../sql/".OM_DB_PHPTYPE."/".$obj.".pdfetiquette.inc.php")) {
            $sql = "SELECT 'Aucun' WHERE 1=2;";
        } else {
            $f = $this->f;
            include "../sql/".OM_DB_PHPTYPE."/".$obj.".pdfetiquette.inc.php";
        }

        /**
         *
         */
        //
        require_once "../obj/fpdf_etiquette.class.php";
        $pdf = new PDF($orientation, 'mm', $format);
        //
        $pdf->SetFont($police, $gras, $size);
        $pdf->SetTextColor($C1, $C2, $C3);
        $pdf->SetMargins(0, 0);
        $pdf->SetAutoPageBreak(false);
        $pdf->AddPage();
        $pdf->SetDisplayMode('real', 'single');
        //
        $param = array(
            $_margin_left,
            $_margin_top,
            $_x_space,
            $_y_space,
            $_x_number,
            $_y_number,
            $_width,
            $_height,
            $_char_size,
            $_line_height,
            0,
            0,
            $size,
            $cadrechamps,
            $cadre
        );
        //
        $pdf->Table_position($sql, $this->f->db, $param, $champs, $texte, $champs_compteur, $img);

        /**
         * OUTPUT
         */
        //
        $filename = "";
        if (isset($params["filename_with_date"]) && $params["filename_with_date"] == false) {
            $filename .= "";
        } else {
            $filename .= date("Ymd-His")."-";
        }
        $filename .= "etiquettes";
        $filename .= "-".$obj;
        $filename .= "-".$_SESSION["collectivite"];
        $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
        if (isset($params["filename_more"])) {
            $filename .= $params["filename_more"];
        }
        $filename .= ".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
