<?php
/**
 * Ce script définit la classe 'procuration'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/procuration.class.php";

/**
 * Définition de la classe 'procuration' (om_dbform).
 */
class procuration extends procuration_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        //
        $this->class_actions[0]["condition"] = array(
            "is_collectivity_mono",
        );
        //
        $this->class_actions[1]["condition"] = array(
            "exists",
            "is_collectivity_mono",
            "is_not_locked",
            "is_not_statut__procuration_perimee",
        );
        //
        $this->class_actions[2]["condition"] = array(
            "exists",
            "is_collectivity_mono",
            "is_not_locked",
        );
        $this->class_actions[21] = array(
            "identifier" => "history",
            "portlet" => array(
                "type" => "action-self",
                "libelle" =>__("historique"),
                "class" => "history-16",
                "order" => 22,
                ),
            "permission_suffix" => "historique",
            "view" => "view_history",
            "condition" => array(
                "exists",
            ),
        );
        $this->class_actions[22] = array(
            "identifier" => "push",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" =>__("transmettre"),
                "class" => "push-16",
                "order" => 25,
                ),
            "permission_suffix" => "push",
            "method" => "push",
            "button" => "push",
            "crud" => "update",
            "condition" => array(
                "exists",
                "is_statut__demande_a_transmettre",
                "is_collectivity_mono",
                "has_all_required_fields",
            ),
        );
        $this->class_actions[23] = array(
            "identifier" => "cancel",
            "portlet" => array(
                "type" => "action-self",
                "libelle" =>__("annuler"),
                "class" => "cancel-16",
                "order" => 25,
                ),
            "permission_suffix" => "annuler",
            "method" => "cancel",
            "button" => "annuler",
            "crud" => "update",
            "condition" => array(
                "exists",
                "is_statut__procuration_confirmee",
                "is_collectivity_mono",
            ),
        );
        $this->class_actions[63] = array(
            "identifier" => "marquer_comme_vu",
            "crud" => "update",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("marquer comme vu"),
                "class" => "valider-16",
                "order" => 1,
            ),
            "permission_suffix" => "marquer_comme_vu",
            "method" => "mark_as_seen",
            "condition" => array(
                "is_collectivity_mono",
                "is_not_marked_as_seen",
            ),
        );
        //
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-refusprocuration",
            "view" => "view_edition_pdf__refusprocuration",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Refus de procuration"),
               "class" => "courrierrefus-16",
            ),
            "permission_suffix" => "edition_pdf__refusprocuration",
            "condition" => array(
                "is_refused",
            )
        );
        //
        $this->class_actions[401] = array(
            "identifier" => "statistiques-epuration_procuration",
            "view" => "view_edition_pdf__statistiques_epuration_procuration",
            "permission_suffix" => "edition_pdf__statistiques_epuration_procuration",
        );
        //
        $this->class_actions[301] = array(
            "identifier" => "edition-pdf-listeprocuration",
            "view" => "view_edition_pdf__listing_procurations_par_bureau",
            "permission_suffix" => "edition_pdf__listing_procurations_par_bureau",
        );
        //
        $this->class_actions[302] = array(
            "identifier" => "edition-pdf-listeregistreprocuration",
            "view" => "view_edition_pdf__registre_procurations_par_bureau",
            "permission_suffix" => "edition_pdf__registre_procurations_par_bureau",
        );
        //
        $this->class_actions[303] = array(
            "identifier" => "edition-pdf-listeregistretotproc",
            "view" => "view_edition_pdf__registre_procurations",
            "permission_suffix" => "edition_pdf__registre_procurations",
        );
        //
        $this->class_actions[304] = array(
            "identifier" => "edition-pdf-listetotalprocuration",
            "view" => "view_edition_pdf__statistiques_procurations",
            "permission_suffix" => "edition_pdf__statistiques_procurations",
        );
    }

    /**
     * CONDITION - is_marked_as_seen.
     *
     * @return boolean
     */
    function is_marked_as_seen() {
        if ($this->getVal("vu") == "t") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_marked_as_seen.
     *
     * @return boolean
     */
    function is_not_marked_as_seen() {
        if ($this->getVal("vu") == "f") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - has_all_required_fields.
     * 
     * @return boolean
     */
    public function has_all_required_fields() {
        if (trim($this->getVal("autorite_nom_prenom")) != ""
            && trim($this->getVal("autorite_type")) != "") {
            //
            if ($this->getVal("autorite_type") === "CSL"
                && trim($this->getVal("autorite_consulat")) != "") {
                //
                return true;
            }
            if (in_array($this->getVal("autorite_type"), array("TRIB", "GN", "PN")) === true
                && trim($this->getVal("autorite_commune")) != "") {
                //
                return true;
            }
        }
        return false;
    }

    /**
     * CONDITION - is_statut__demande_a_transmettre.
     * 
     * @return boolean
     */
    public function is_statut__demande_a_transmettre() {
        if ($this->getVal("statut") !== "demande_a_transmettre") {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_statut__procuration_confirmee.
     * 
     * @return boolean
     */
    public function is_statut__procuration_confirmee() {
        if ($this->getVal("statut") !== "procuration_confirmee") {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_statut__procuration_perimee.
     * 
     * @return boolean
     */
    public function is_statut__procuration_perimee() {
        if ($this->getVal("statut") !== "procuration_perimee") {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_not_statut__procuration_perimee.
     * 
     * @return boolean
     */
    public function is_not_statut__procuration_perimee() {
        return !$this->is_statut__procuration_perimee();
    }

    /**
     * CONDITION - is_locked.
     * 
     * Quand une procuration est gérée sur un référentiel externe, elle est considérée comme verrouillée.
     * 
     * @return boolean
     */
    public function is_locked() {
        if ($this->getVal("id_externe") !== "") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_locked.
     * 
     * @return boolean
     */
    public function is_not_locked() {
        return !$this->is_locked();
    }

    /**
     *
     */
    function get_procuration_id_from_reu_demande_id($reu_demande_id) {
        $query = sprintf(
            'SELECT procuration.id FROM %1$sprocuration WHERE id_externe=\'%2$s\'',
            DB_PREFIXE,
            $reu_demande_id
        );
        $res = $this->f->get_all_results_from_db_query($query, true);
        if ($res['code'] !== "OK") {
            return null;
        }
        if (count($res['result']) == 0) {
            return null;
        }
        if (count($res['result']) > 1) {
            return false;
        }
        return intval($res['result'][0]['id']);
    }

    /**
     * GETTER_FORMINC - champs.
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "procuration.id",
            "procuration.om_collectivite",
            "procuration.id_externe",
            "procuration.statut",
            "procuration.motif_refus",
            "'' as mandant_hors_commune",
            "mandant_ine",
            "mandant",
            "mandant_resume",
            "mandant_infos",
            "'' as mandataire_hors_commune",
            "mandataire_ine",
            "mandataire",
            "mandataire_resume",
            "mandataire_infos",
            "debut_validite",
            "fin_validite",
            "autorite_nom_prenom",
            "autorite_type",
            "autorite_commune",
            "autorite_consulat",
            "date_accord",
            "annulation_autorite_nom_prenom",
            "annulation_autorite_type",
            "annulation_autorite_commune",
            "annulation_autorite_consulat",
            "annulation_date",
            "procuration.vu",
            "procuration.historique",
            "procuration.notes",

        );
    }


    /**
     * Clause from pour la requête de sélection des données de l'enregistrement.
     *
     * @return string
     */
    function get_var_sql_forminc__tableSelect() {
        return sprintf(
            '%1$sprocuration LEFT JOIN %1$selecteur AS mandant ON procuration.mandant=mandant.id LEFT JOIN %1$selecteur AS mandataire ON procuration.mandataire=mandataire.id', // FROM
            DB_PREFIXE
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mandant() {
        return sprintf(
            'SELECT electeur.id, (electeur.nom||\' - \'||electeur.prenom||\' - \'||to_char(electeur.date_naissance,\'DD/MM/YYYY\')||\' - \'||bureau.code||\' - \'||liste.liste_insee) FROM %1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id LEFT JOIN %1$sliste ON electeur.liste=liste.liste ORDER BY electeur.nom ASC',
            DB_PREFIXE
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mandant_by_id() {
        return sprintf(
            'SELECT electeur.id, (electeur.nom||\' - \'||electeur.prenom||\' - \'||to_char(electeur.date_naissance,\'DD/MM/YYYY\')||\' - \'||bureau.code||\' - \'||liste.liste_insee) FROM %1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id LEFT JOIN %1$sliste ON electeur.liste=liste.liste WHERE electeur.id=<idx>',
            DB_PREFIXE
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mandataire() {
        return $this->get_var_sql_forminc__sql_mandant();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_mandataire_by_id() {
        return $this->get_var_sql_forminc__sql_mandant_by_id();
    }

    /**
     * Pour contourner un bug du framework de la méthode
     * dbform::get_values_merge_fields() qui se sert de cet attibut pour
     * vérifier les clés étrangères alors que ce n'est pas l'objet de cet
     * attribut.
     */
    var $foreign_keys_extended = array(
        "electeur" => array("electeur", ),
        "mandant" => null,
        "mandataire" => null,
    );


    /**
     * VIEW - view_edition_pdf__refusprocuration.
     *
     * @return void
     */
    public function view_edition_pdf__refusprocuration() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "refusprocurationmandant;refusprocurationmandataire"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'refusprocuration-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * TREATMENT - mark_as_seen.
     *
     * Cette methode permet de passer le marqueur *vu* à vrai.
     *
     * @return boolean true si maj effectué false sinon
     */
    function mark_as_seen() {
        // Cette méthode permet d'exécuter une routine en début des méthodes
        // dites de TREATMENT.
        $this->begin_treatment(__METHOD__);
        //
        $valF = array(
            "vu" => true,
        );
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => "mark_as_seen",
            'message' => sprintf(
                __('Action %1$s %2$s effectuée.'),
                63,
                $this->get_action_param(63, 'identifier')
            ),
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Vos modifications ont bien été enregistrées."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Statistiques - 'epuration_procuration'.
     *
     * Nombre de procurations à épurer par bureau.
     *
     * @return array
     */
    function compute_stats__epuration_procuration() {
        //
        $inc = "stats_procuration_aepurer";
        $dateElection = "";
        if (isset ($_GET ['dateElection']))
            $dateElection = $_GET ['dateElection'];
        if ($dateElection == "")
            $dateElection = date ('d/m/Y');
        //
        if ($dateElection != "") {
            if ($this->f->formatdate == "AAAA-MM-JJ") {
                $date = explode("/", $dateElection);
                // controle de date
                if (sizeof($date) == 3 and (checkdate($date[1],$date[0],$date[2]))) {
                    $dateElectionR = $date[2]."-".$date[1]."-".$date[0];
                } else {
                    die("La date ".$dateElection." n'est pas une date.");
                    $correct=false;
                }
            }
            if ($this->f->formatdate == "JJ/MM/AAAA") {
                $date = explode("/", $dateElection);
                // controle de date
                if (sizeof($date) == 3 and checkdate($date[1],$date[0],$date[2])) {
                    $dateElectionR = $date[0]."/".$date[1]."/".$date[2];
                } else {
                    die("La date ".$dateElection." n'est pas une date.");
                    $correct=false;
                }
            }
        }

        //
        $total_avant_epuration = 0;
        $total_a_epurer = 0;
        $total_apres_epuration = 0;
        // Recuperation des donnees
        $data = array();
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
            // Nombre de procurations dont le mandant (toutes listes confondues)
            // est dans le bureau passé en paramètre 
            $query = sprintf(
                'SELECT count(procuration.id) FROM %1$sprocuration LEFT JOIN %1$selecteur on procuration.mandant=electeur.id WHERE electeur.bureau=%2$s',
                DB_PREFIXE,
                intval($bureau["id"])
            );
            $avant_epuration = $this->f->db->getone($query);
            $this->addToLog(
                __METHOD__."(): db->getone(\"".$query."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($avant_epuration);
            // Nombre de procurations dont le mandant  (toutes listes confondues)
            // est dans le bureau passé en paramètre et dont la
            // date de fin de validité est inférieure ou égale à la date passée
            // en paramètre
            $query = sprintf(
                'SELECT count(procuration.id) FROM %1$sprocuration LEFT JOIN %1$selecteur on procuration.mandant=electeur.id WHERE procuration.fin_validite<=\'%3$s\' AND electeur.bureau=%2$s',
                DB_PREFIXE,
                intval($bureau["id"]),
                $dateElectionR
            );
            $a_epurer = $this->f->db->getone($query);
            $this->addToLog(
                __METHOD__."(): db->getone(\"".$query."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($a_epurer);
            // Calculs
            $apres_epuration = $avant_epuration - $a_epurer;
            $total_avant_epuration = $total_avant_epuration + $avant_epuration;
            $total_a_epurer = $total_a_epurer + $a_epurer;
            $total_apres_epuration = $total_apres_epuration + $apres_epuration;
            //
            $datas = array(
                $bureau["code"],
                $bureau["libelle"],
                $avant_epuration,
                $a_epurer,
                $apres_epuration
            );
            $data[] = $datas;
        }
        //
        $datas = array(
            "-",
            __("TOTAL"),
            $total_avant_epuration,
            $total_a_epurer,
            $total_apres_epuration
        );
        $data[] = $datas;
        // Array
        return array(
            "format" => "L",
            "title" => __("Statistiques - Procurations à épurer"),
            "subtitle" => sprintf(
                __("Épuration des procurations au %s (toutes listes confondues)"),
                $dateElection
            ),
            "offset" => array(10, 0, 50, 50, 40),
            "column" => array(
                "",
                "",
                __("Avant épuration"),
                __("À épurer"),
                __("Après épuration")
            ),
            "data" => $data,
            "output" => "stats-procurationaepurer"
        );
    }

    /**
     * VIEW - view_edition_pdf__statistiques_epuration_procuration.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_epuration_procuration() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__epuration_procuration(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * CONDITION - is_refused.
     *
     * @return bool
     */
    function is_refused() {
        if ($this->getVal("statut") === 'demande_refusee') {
            return true;
        }
        return false;
    }

    /**
     * SETTER_TREATMENT - setValF (setValF).
     *
     * @return void
     */
    function setvalF($val = array()) {
        parent::setvalf($val);
        $this->setter_treatment__mandant($val);
        $this->setter_treatment__mandataire($val);
        // Ces champs sont mis à jour uniquement par la gestion spécifique du log
        // et donc jamais par les actions de formulaire standards
        if (array_key_exists("historique", $this->valF) === true) {
            unset($this->valF["historique"]);
        }
        if (array_key_exists("vu", $this->valF) === true) {
            unset($this->valF["vu"]);
        }
    }

    /**
     * SETTER_FORM - set_form_default_values (setVal).
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        if ($validation == 0 && $maj == 0) {
            $form->setVal("statut", "demande_a_transmettre");
        }
        if ($validation == 0 && $maj == 1) {
            if ($this->getVal("mandant") == "") {
                $form->setVal("mandant_hors_commune", "t");
            }
            if ($this->getVal("mandataire") == "") {
                $form->setVal("mandataire_hors_commune", "t");
            }
        }
    }


    /**
     * SETTER_TREATMENT - setter_treatment__mandant (setValF).
     *
     * @return void
     */
    function setter_treatment__mandant($val = array()) {
        if (array_key_exists("mandant", $val) === true
            && $val["mandant"] !== "") {
            //
            $inst_mandant = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => intval($val["mandant"]),
            ));
            if ($inst_mandant->exists()) {
                $this->valF["mandant_ine"] = $inst_mandant->getVal("ine");
                $this->valF["mandant_resume"] = sprintf(
                    '%s - %s - %s - %s',
                    $inst_mandant->getVal("nom"),
                    $inst_mandant->getVal("prenom"),
                    $inst_mandant->getVal("date_naissance"),
                    $inst_mandant->get_liste_insee()
                );
            }
        } elseif (array_key_exists("mandant_ine", $val) === true
            && $val["mandant_ine"] !== "") {
            //
            $this->valF["mandant"] = $this->get_electeur_id_by_ine($val["mandant_ine"]);
        }
    }

    /**
     * 
     */
    function get_electeur_id_by_ine($ine) {
        $electeur_id = $this->f->db->getOne(
            sprintf(
                'SELECT id FROM %1$selecteur WHERE ine=\'%3$s\' AND om_collectivite=%2$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $this->f->db->escapesimple($ine)
            )
        );
        $this->f->isDatabaseError($electeur_id);
        return $electeur_id;
    }

    /**
     * SETTER_TREATMENT - setter_treatment__mandataire (setValF).
     *
     * @return void
     */
    function setter_treatment__mandataire($val = array()) {
        if (array_key_exists("mandataire", $val) === true
            && $val["mandataire"] !== "") {
            //
            $inst_mandataire = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => intval($val["mandataire"]),
            ));
            if ($inst_mandataire->exists()) {
                $this->valF["mandataire_ine"] = $inst_mandataire->getVal("ine");
                $this->valF["mandataire_resume"] = sprintf(
                    '%s - %s - %s - %s',
                    $inst_mandataire->getVal("nom"),
                    $inst_mandataire->getVal("prenom"),
                    $inst_mandataire->getVal("date_naissance"),
                    $inst_mandataire->get_liste_insee()
                );
            }
        } elseif (array_key_exists("mandataire_ine", $val) === true
            && $val["mandataire_ine"] !== "") {
            //
            $this->valF["mandataire"] = $this->get_electeur_id_by_ine($val["mandataire_ine"]);
        }
    }

    /**
     * CHECK_TREATMENT - verifier.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("autorite_type", "autorite_nom_prenom", );
        if (array_key_exists("autorite_type", $this->valF) === true) {
            if ($this->valF["autorite_type"] === "CSL") {
                $field_required[] = "autorite_consulat";
            } elseif (in_array($this->valF["autorite_type"], array("GN", "PN", "TRIB")) === true) {
                $field_required[] = "autorite_commune";
            }
        }
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->addToMessage(sprintf(
                    __("Le champ <b>%s</b> est obligatoire"),
                   $this->form->lib[$field] 
                ));
            }
        }
    }

    /**
     *
     */
    function get_widget_config__autorite_commune__autocomplete() {
        return array(
            "obj" => "autorite_commune",
            "table" => "reu_referentiel_ugle",
            "identifiant" => "reu_referentiel_ugle.code",
            "criteres" => array(
                "reu_referentiel_ugle.code_commune" => __("code_commune"),
                "reu_referentiel_ugle.libelle_commune" => __("Nom de la commune")
            ),
            "libelle" => array (
                "reu_referentiel_ugle.code_commune",
                "reu_referentiel_ugle.libelle_commune"
            ),
            "jointures" => array(),
            "group_by" => array(
                "reu_referentiel_ugle.code",
                "reu_referentiel_ugle.code_commune",
                "reu_referentiel_ugle.libelle_commune",
            ),
            "droit_ajout" => false,
        );
    }

    /**
     *
     */
    function get_widget_config__autorite_consulat__autocomplete() {
        return array(
            "obj" => "autorite_consulat",
            "table" => "reu_referentiel_ugle",
            "identifiant" => "reu_referentiel_ugle.code",
            "criteres" => array(
                "reu_referentiel_ugle.code_consulat" => __("code_consulat"),
                "reu_referentiel_ugle.libelle_consulat" => __("Nom du consulat")
            ),
            "libelle" => array (
                "reu_referentiel_ugle.code_consulat",
                "reu_referentiel_ugle.libelle_consulat"
            ),
            "jointures" => array(),
            "group_by" => array(
                "reu_referentiel_ugle.code",
                "reu_referentiel_ugle.code_consulat",
                "reu_referentiel_ugle.libelle_consulat",
            ),
            "droit_ajout" => false,
        );
    }

    /**
     *
     */
    function get_widget_config__annulation_autorite_commune__autocomplete() {
        return array(
            "obj" => "annulation_autorite_commune",
            "table" => "reu_referentiel_ugle",
            "identifiant" => "reu_referentiel_ugle.code",
            "criteres" => array(
                "reu_referentiel_ugle.code_commune" => __("code_commune"),
                "reu_referentiel_ugle.libelle_commune" => __("Nom de la commune")
            ),
            "libelle" => array (
                "reu_referentiel_ugle.code_commune",
                "reu_referentiel_ugle.libelle_commune"
            ),
            "jointures" => array(),
            "group_by" => array(
                "reu_referentiel_ugle.code",
                "reu_referentiel_ugle.code_commune",
                "reu_referentiel_ugle.libelle_commune",
            ),
            "droit_ajout" => false,
        );
    }

    /**
     *
     */
    function get_widget_config__annulation_autorite_consulat__autocomplete() {
        return array(
            "obj" => "annulation_autorite_consulat",
            "table" => "reu_referentiel_ugle",
            "identifiant" => "reu_referentiel_ugle.code",
            "criteres" => array(
                "reu_referentiel_ugle.code_consulat" => __("code_consulat"),
                "reu_referentiel_ugle.libelle_consulat" => __("Nom du consulat")
            ),
            "libelle" => array (
                "reu_referentiel_ugle.code_consulat",
                "reu_referentiel_ugle.libelle_consulat"
            ),
            "jointures" => array(),
            "group_by" => array(
                "reu_referentiel_ugle.code",
                "reu_referentiel_ugle.code_consulat",
                "reu_referentiel_ugle.libelle_consulat",
            ),
            "droit_ajout" => false,
        );
    }

    /**
     * SETTER_FORM - setLayout.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        //
        $form->setBloc(
            $this->clePrimaire,
            "D",
            "",
            sprintf(
                'form-%1$s form-%1$s-maj-%2$s',
                $this->get_absolute_class_name(),
                $this->getParameter("maj")
            )
        );
        //
        $form->setFieldset("id", "D", __("Identification"));
        $form->setBloc("id", "DF", "", "group first-fix-width");
        $form->setBloc("om_collectivite", "DF", "", "group first-fix-width");
        $form->setBloc("id_externe", "DF", "", "group first-fix-width");
        $form->setBloc("statut", "DF", "", "group first-fix-width");
        $form->setBloc("motif_refus", "DF", "", "group first-fix-width field--procuration--motif_refus");
        $form->setFieldset("motif_refus", "F", "");
        //
        $form->setFieldset("mandant_hors_commune", "D", __("Mandant"));
        $form->setBloc("mandant_hors_commune", "DF", "", "group first-fix-width");
        $form->setBloc("mandant_ine", "DF", "", "group first-fix-width field--procuration--mandant_ine");
        $form->setBloc("mandant", "DF", "", "group first-fix-width field--procuration--mandant");
        $form->setBloc("mandant_resume", "DF", "", "group first-fix-width");
        $form->setBloc("mandant_infos", "DF", "", "group first-fix-width");
        $form->setFieldset("mandant_infos", "F");
        //
        $form->setFieldset("mandataire_hors_commune", "D", __("Mandataire"));
        $form->setBloc("mandataire_hors_commune", "DF", "", "group first-fix-width");
        $form->setBloc("mandataire_ine", "DF", "", "group first-fix-width field--procuration--mandataire_ine");
        $form->setBloc("mandataire", "DF", "", "group first-fix-width field--procuration--mandataire");
        $form->setBloc("mandataire_resume", "DF", "", "group first-fix-width");
        $form->setBloc("mandataire_infos", "DF", "", "group first-fix-width");
        $form->setFieldset("mandataire_infos", "F", "");
        //
        $form->setFieldset("debut_validite", "D", __("Validite de la procuration"));
        $form->setBloc("debut_validite", "DF", "", "group first-fix-width");
        $form->setBloc("fin_validite", "DF", "", "group first-fix-width");
        $form->setFieldset("fin_validite", "F", "");
        //
        $form->setFieldset("autorite_nom_prenom", "D", __("Établissement de la procuration"));
        $form->setBloc("autorite_type", "DF", "", "group first-fix-width");
        $form->setBloc("autorite_nom_prenom", "DF", "", "group first-fix-width");
        $form->setBloc("autorite_commune", "DF", "", "group first-fix-width field--procuration--autorite_commune");
        $form->setBloc("autorite_consulat", "DF", "", "group first-fix-width field--procuration--autorite_consulat");
        $form->setGroupe("date_accord", "DF", "", "group first-fix-width");
        $form->setFieldset("date_accord", "F", "");
        //
        $form->setFieldset("annulation_autorite_nom_prenom", "D", __("Annulation de la procuration"));
        $form->setBloc("annulation_autorite_type", "DF", "", "group first-fix-width");
        $form->setBloc("annulation_autorite_nom_prenom", "DF", "", "group first-fix-width");
        $form->setBloc("annulation_autorite_commune", "DF", "", "group first-fix-width field--procuration--annulation_autorite_commune");
        $form->setBloc("annulation_autorite_consulat", "DF", "", "group first-fix-width field--procuration--annulation_autorite_consulat");
        $form->setGroupe("annulation_date", "DF", "", "group first-fix-width");
        $form->setFieldset("annulation_date", "F", "");
        //
        $form->setFieldset("notes", "D", __("Notes"));
        $form->setBloc("notes", "DF", "", "group first-fix-width");
        $form->setFieldset("notes", "F", "");
        //
        $form->setBloc(end($this->champs), "F", "");
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);
        // Le marqueur vu et l'historique ne sont jamais affichés sur le formulaire
        $form->setType('mandataire_hors_commune', 'hidden');
        $form->setType('mandant_hors_commune', 'hidden');
        $form->setType('vu', 'hidden');
        $form->setType('historique', 'hidden');
        $form->setType('mandant_infos', 'hidden');
        $form->setType('mandataire_infos', 'hidden');
        //
        if ($maj == 0 || $crud == 'create'
            || $maj == 1 || $crud == 'update') {
            //
            $form->setType('mandant_ine', 'ine');
            $form->setType('mandataire_ine', 'ine');
            $form->setType('mandant', 'autocomplete');
            $form->setType('mandataire', 'autocomplete');
            $form->setType('mandant_resume', 'hidden');
            $form->setType('mandataire_resume', 'hidden');
            //
            $form->setType('debut_validite', 'date');
            $form->setType('fin_validite', 'date');
            //
            $form->setType('date_accord', 'date');

            $form->setType('autorite_commune', 'autocomplete');
            $form->setType('autorite_consulat', 'autocomplete');

            $form->setType('autorite_type', 'select');
            $form->setType('mandataire_hors_commune', 'checkbox');
            $form->setType('mandant_hors_commune', 'checkbox');
        }
        //
        if ($maj == 1 || $crud == 'update') {
            $form->setType('id_externe', 'hiddenstatic');
            $form->setType('statut', 'select');
        }
        //
        if ($maj == 0 || $crud == 'create') {
            $form->setType('id_externe', 'hidden');
            $form->setType('statut', 'select');
        }
        //
        if ($maj == 3 || $crud == 'read') {
            if ($this->getVal("mandant") == "") {
                $form->setType('mandant', 'hidden');
            } else {
                $form->setType('mandant_resume', 'hidden');
            }
            if ($this->getVal("mandataire") == "") {
                $form->setType('mandataire', 'hidden');
            } else {
                $form->setType('mandataire_resume', 'hidden');
            }
            if ($this->getVal("statut") !== "demande_refusee") {
                $form->setType('motif_refus', 'hidden');
            }
            if ($this->getVal("autorite_type") !== "CSL") {
                $form->setType('autorite_consulat', 'hidden');
            }
        }
        if ($this->getVal("statut") != "procuration_annulee") {
            $form->setType("annulation_autorite_nom_prenom", 'hidden');
            $form->setType("annulation_autorite_type", 'hidden');
            $form->setType("annulation_autorite_consulat", 'hidden');
            $form->setType("annulation_autorite_commune", 'hidden');
            $form->setType("annulation_date", 'hidden');
        }
        //
        if ($maj == 21 || $maj == 23) {
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
        if ($maj == 23) {
            $form->setType("annulation_autorite_nom_prenom", 'text');
            $form->setType("annulation_autorite_type", 'select');
            $form->setType("annulation_autorite_consulat", 'autocomplete');
            $form->setType("annulation_autorite_commune", 'autocomplete');
            $form->setType("annulation_date", 'date');
        }
    }

    /**
     * Configuration du widget de formulaire autocomplete du champ 'mandant'.
     *
     * @return array
     */
    function get_widget_config__mandant__autocomplete() {
        return array(
            "obj" => "electeur-mandant",
            "table" => "electeur",
            "identifiant" => "electeur.id",
            "criteres" => array(
                "electeur.ine" => __("INE"),
                "electeur.nom" => __("Nom"),
                "electeur.nom_usage" => __("Nom d'usage"),
                "electeur.prenom" => __("Prenom"),
            ),
            "libelle" => array(
                "electeur.nom",
                "electeur.prenom",
                "to_char(electeur.date_naissance,'DD/MM/YYYY')",
                "bureau.code",
                "CONCAT('n°', electeur.numero_bureau)",
                "liste.liste_insee",
                "electeur.ine",
            ),
            "jointures" => array(
                "bureau ON electeur.bureau=bureau.id",
                "liste ON electeur.liste=liste.liste",
            ),
            "droit_ajout" => false,
            "where" => "electeur.om_collectivite=".intval($_SESSION["collectivite"]),
            "group_by" => array(
                "electeur.id",
                "bureau.code",
                "liste.liste_insee",
            ),
        );
    }

    /**
     * Configuration du widget de formulaire autocomplete du champ 'mandataire'.
     *
     * @return array
     */
    function get_widget_config__mandataire__autocomplete() {
        return array(
            "obj" => "electeur-mandataire",
            "table" => "electeur",
            "identifiant" => "electeur.id",
            "criteres" => array(
                "electeur.ine" => __("INE"),
                "electeur.nom" => __("Nom"),
                "electeur.nom_usage" => __("Nom d'usage"),
                "electeur.prenom" => __("Prenom"),
            ),
            "libelle" => array(
                "electeur.nom",
                "electeur.prenom",
                "to_char(electeur.date_naissance,'DD/MM/YYYY')",
                "bureau.code",
                "CONCAT('n°', electeur.numero_bureau)",
                "liste.liste_insee",
                "electeur.ine",
            ),
            "jointures" => array(
                "bureau ON electeur.bureau=bureau.id",
                "liste ON electeur.liste=liste.liste",
            ),
            "droit_ajout" => false,
            "where" => "electeur.om_collectivite=".intval($_SESSION["collectivite"]),
            "group_by" => array(
                "electeur.id",
                "bureau.code",
                "liste.liste_insee",
            ),
        );
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        // On n'appelle pas la méthode parent qui est trop gourmande
        // parent::setSelect($form, $maj);
        if ($maj == 999) {
            // om_collectivite
            $this->init_select(
                $form, 
                $this->f->db,
                $maj,
                null,
                "om_collectivite",
                $this->get_var_sql_forminc__sql("om_collectivite"),
                $this->get_var_sql_forminc__sql("om_collectivite_by_id"),
                false
            );
        }
        // SELECT
        if ($maj == 0 || $maj == 1) {
            $form->setSelect(
                "statut",
                array(
                    array(
                        "demande_a_transmettre",
                        "demande_refusee",
                    ),
                    array(
                        "demande_a_transmettre",
                        "demande_refusee",
                    ),
                )
            );
        } else {
            $form->setSelect(
                "statut",
                array(
                    array(
                        "",
                        "demande_a_transmettre",
                        "demande_refusee",
                        "procuration_confirmee",
                        "procuration_annulee",
                        "procuration_perimee",
                    ),
                    array(
                        __("Tous les statuts"),
                        "demande_a_transmettre",
                        "demande_refusee",
                        "procuration_confirmee",
                        "procuration_annulee",
                        "procuration_perimee",
                    ),
                )
            );
        }
        //
        $form->setSelect(
            "autorite_type",
            array(
                array("", "PN", "GN", "TRIB", "CSL", ),
                array("", "Police Nationale", "Gendarmerie", "Tribunal", "Consulat", ),
            )
        );
        $form->setSelect(
            "annulation_autorite_type",
            array(
                array("", "PN", "GN", "TRIB", "CSL", ),
                array("", "Police Nationale", "Gendarmerie", "Tribunal", "Consulat", ),
            )
        );
        if ($maj == 0 || $maj == 1) {
            //
            $form->setSelect(
                "mandant",
                $this->get_widget_config__mandant__autocomplete()
            );
            //
            $form->setSelect(
                "mandataire",
                $this->get_widget_config__mandataire__autocomplete()
            );
            //
            $form->setSelect(
                "autorite_commune",
                $this->get_widget_config__autorite_commune__autocomplete()
            );
            //
            $form->setSelect(
                "autorite_consulat",
                $this->get_widget_config__autorite_consulat__autocomplete()
            );
        }
        if ($maj == 23) {
            //
            $form->setSelect(
                "annulation_autorite_commune",
                $this->get_widget_config__annulation_autorite_commune__autocomplete()
            );
            //
            $form->setSelect(
                "annulation_autorite_consulat",
                $this->get_widget_config__autorite_consulat__autocomplete()
            );
        }
        if ($maj == 3) {
            parent::setSelect($form, $maj);
        }
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("id", __("Id"));
        $form->setLib("id_externe", __("Id externe"));
        $form->setLib("mandant_ine", __("Mandant INE"));
        $form->setLib("mandataire_ine", __("Mandataire INE"));
        $form->setLib("mandant", __("Mandant"));
        $form->setLib("mandataire", __("Mandataire"));
        $form->setLib("mandant_resume", __("Mandant Résumé"));
        $form->setLib("mandataire_resume", __("Mandataire Résumé"));
        $form->setLib("debut_validite", __("Du"));
        $form->setLib("fin_validite", __("Au"));
        $form->setLib("mandant_hors_commune", __("Mandant hors commune ?"));
        $form->setLib("mandataire_hors_commune", __("Mandataire hors commune ?"));
        $form->setLib("date_accord", __("Date d'établissement"));
        $form->setLib("autorite_nom_prenom", __("Nom et prénom de l'autorité d'établissement"));
        $form->setLib("autorite_type", __("Autorité d'établissement"));
        $form->setLib("autorite_commune", __("Lieu d'établissement (En France)"));
        $form->setLib("autorite_consulat", __("Lieu d'établissement (Hors France)"));
        $form->setLib("annulation_date", __("Date d'annulation"));
        $form->setLib("annulation_autorite_nom_prenom", __("Nom et prénom de l'autorité d'annulation"));
        $form->setLib("annulation_autorite_type", __("Autorité d'annulation"));
        $form->setLib("annulation_autorite_commune", __("Lieu d'annulation (En France)"));
        $form->setLib("annulation_autorite_consulat", __("Lieu d'annulation (Hors France)"));
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            $required_fields = array("autorite_type", "autorite_nom_prenom", "autorite_commune", "autorite_consulat", "mandant", "mandataire", );
            foreach ($required_fields as $field) {
                $form->setLib($field, $this->form->lib[$field] .' '.$form->required_tag);
            }
        }
    }

    /**
     * TREATMENT - update_status.
     * 
     * @return boolean
     */
    function update_status($val = array()) {
        $valF = array();
        if (array_key_exists("statut", $val) === true
            && $val["statut"] != "") {
            //
            $valF["statut"] = $val["statut"];
        }
        if (array_key_exists("vu", $val) === true
            && $val["vu"] != "") {
            //
            $valF["vu"] = $val["vu"];
        }
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => __METHOD__,
            'message' => sprintf(
                __('Mise à jour du statut de la procuration.')
            ),
            "datas" => $valF,
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Le statut a été mis à jour correctement."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * VIEW - view_history
     *
     * @return void
     */
    function view_history() {
        $this->checkAccessibility();
        // Affichage du bouton retour
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "top",
        ));
        $this->retour();
        $this->f->layout->display__form_controls_container__end();
        //
        printf(
            '<h2>%s</h2>',
            __("Historique")
        );
        $history = $this->getVal('historique');
        if ($history !== "") {
            echo $this->f->get_display_array_as_table(json_decode($this->getVal('historique'), true));
        } else {
            echo __("Aucun historique");
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            echo "Erreur REU";
        } else {
            if (intval($this->getVal("id_externe")) > 0) {
                printf(
                    '<h2>%s</h2>',
                    __("REU Procuration")
                );
                $method = "handle_".$this->get_absolute_class_name();
                $ret = $inst_reu->$method(array(
                    "mode" => "get",
                    "id" => intval($this->getVal("id_externe")),
                ));
                echo "<p>Code : ".$ret["code"]."<p>";
                echo $this->f->get_display_array_as_table(
                    $ret["result"]
                );
            }
        }
        // Affichage du bouton retour
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->retour();
        $this->f->layout->display__form_controls_container__end();
    }

   /**
     * VIEW - view_edition_pdf__listing_procurations_par_bureau.
     *
     * @return void
     */
    public function view_edition_pdf__listing_procurations_par_bureau() {
        $this->checkAccessibility();
        //
        $params = array(
            "mode_edition" => "parbureau",
            "liste" => $_SESSION["liste"],
        );
        if (isset($_POST["bureau"]) && isset($_POST["liste"])) {
            $params["bureau_code"] = $this->f->get_submitted_post_value("bureau");
            $params["liste"] = $this->f->get_submitted_post_value("liste");
            if ($this->f->get_submitted_post_value("validite") !== "") {
                $params["date_election"] = $this->f->get_submitted_post_value("validite");
            }
        }
        //
        require_once "../obj/edition_pdf__listing_procurations_par_bureau.class.php";
        $inst_edition_pdf = new edition_pdf__listing_procurations_par_bureau();
        $pdf_edition = $inst_edition_pdf->compute_pdf__listing_procurations_par_bureau($params);
        $inst_edition_pdf->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
        return;
    }

   /**
     * VIEW - view_edition_pdf__statistiques_procurations.
     *
     * @return void
     */
    public function view_edition_pdf__statistiques_procurations() {
        $this->checkAccessibility();
        //
        $params = array(
            "mode_edition" => "commune",
            "liste" => $_SESSION["liste"],
        );
        if (isset($_GET["liste"])) {
            $params["liste"] = $this->f->get_submitted_get_value("liste");
        }
        //
        require_once "../obj/edition_pdf__statistiques_procurations.class.php";
        $inst_edition_pdf = new edition_pdf__statistiques_procurations();
        $pdf_edition = $inst_edition_pdf->compute_pdf__statistiques_procurations($params);
        $inst_edition_pdf->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
        return;
    }

   /**
     * VIEW - view_edition_pdf__registre_procurations_par_bureau.
     *
     * @return void
     */
    public function view_edition_pdf__registre_procurations_par_bureau() {
        $this->checkAccessibility();
        //
        $params = array(
            "mode_edition" => "parbureau",
            "liste" => $_SESSION["liste"],
        );
        if (isset($_POST["bureau"]) && isset($_POST["liste"])) {
            $params["bureau_code"] = $this->f->get_submitted_post_value("bureau");
            $params["liste"] = $this->f->get_submitted_post_value("liste");
        }
        //
        require_once "../obj/edition_pdf__registre_procurations.class.php";
        $inst_edition_pdf = new edition_pdf__registre_procurations();
        $pdf_edition = $inst_edition_pdf->compute_pdf__registre_procurations($params);
        $inst_edition_pdf->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
        return;
    }

   /**
     * VIEW - view_edition_pdf__registre_procurations.
     *
     * @return void
     */
    public function view_edition_pdf__registre_procurations() {
        $this->checkAccessibility();
        //
        $params = array(
            "mode_edition" => "commune",
            "liste" => $_SESSION["liste"],
        );
        if (isset($_GET["liste"])) {
            $params["liste"] = $this->f->get_submitted_get_value("liste");
        }
        //
        require_once "../obj/edition_pdf__registre_procurations.class.php";
        $inst_edition_pdf = new edition_pdf__registre_procurations();
        $pdf_edition = $inst_edition_pdf->compute_pdf__registre_procurations($params);
        $inst_edition_pdf->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
        return;
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        $ret = parent::triggerajouterapres($id, $dnu1, $val);
/*        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }*/
        // Gestion de l'historique
        $val_histo = array(
            "id" => $id,
            'action' => "ajouter",
            'message' => sprintf(
                __('Action %1$s %2$s effectuée.'),
                1,
                $this->get_action_param(1, 'identifier')
            ),
            "datas" => $this->valF,
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        $ret = parent::triggermodifierapres($id, $dnu1, $val);
/*        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }*/
        // Gestion de l'historique
        $val_histo = array(
            'action' => "modifier",
            'message' => sprintf(
                __('Action %1$s %2$s effectuée.'),
                1,
                $this->get_action_param(1, 'identifier')
            ),
            "datas" => $this->valF,
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    function get_inst__mandataire() {
        return $this->f->get_inst__om_dbform(array(
            "obj" => "electeur",
            "idx" => $this->getVal("mandataire"),
        ));
    }

    function get_inst__mandant() {
        return $this->f->get_inst__om_dbform(array(
            "obj" => "electeur",
            "idx" => $this->getVal("mandant"),
        ));
    }

    /**
     * TREATMENT - push.
     * 
     * @return boolean
     */
    function push($val = array()) {
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done"
            || ($this->getVal("id_externe") !== "" && $this->getVal("id_externe") != null)) {
            //
            $this->addToMessage("no reu");
            return false;
        }
        // Transmission REU
        $inst_mandant = $this->get_inst__mandant();
        $inst_mandataire = $this->get_inst__mandataire();
        $autorite_ugle = ($this->getVal("autorite_type") == "CSL" ? $this->getVal("autorite_consulat") : $this->getVal("autorite_commune"));
        //
        $ret = $this->handle_reu_demande_procuration(array(
            "mode" => "add",
            "debut_validite" => $this->getVal("debut_validite"),
            "fin_validite" => $this->getVal("fin_validite"),
            "date_accord" => $this->getVal("date_accord"),
            "mandant_ine" => $this->getVal("mandant_ine"),
            "mandataire_ine" => $this->getVal("mandataire_ine"),
            "autorite_type" => $this->getVal("autorite_type"),
            "autorite_ugle" => $autorite_ugle,
            "autorite_nom_prenom" => $this->getVal("autorite_nom_prenom"),
        ));
        if ($ret !== true) {
            return $ret;
        }
        //
        return true;
    }

    /**
     * 
     */
    function check_mandatory_field($field, $val) {
        if (array_key_exists($field, $val) === true
            && $val[$field] !== null
            && $val[$field] !== "") {
            //
            $valF[$field] = $val[$field];
        } else {
            $this->addToMessage(sprintf(
                __("Le champ <b>%s</b> est obligatoire"),
                __($field)
            ));
            $this->correct = false;
        }
    }

    /**
     * TREATMENT - cancel.
     * 
     * @return boolean
     */
    function cancel($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        $fields = array(
            "annulation_date",
            "annulation_autorite_type",
            "annulation_autorite_nom_prenom",
        );
        $valF = array();
        foreach ($fields as $field) {
            if (array_key_exists($field, $val) === true
                && $val[$field] !== null
                && $val[$field] !== "") {
                //
                $valF[$field] = $val[$field];
            } else {
                $this->addToMessage(sprintf(
                    __("Le champ <b>%s</b> est obligatoire"),
                    __($field)
                ));
                $this->correct = false;
            }
        }
        if ($valF["annulation_autorite_type"] == "CSL") {
            $field = "annulation_autorite_consulat";
        } elseif (in_array($valF["annulation_autorite_type"], array("PN", "GN", "TRIB", )) === true) {
            $field = "annulation_autorite_commune";
        }
        if (array_key_exists($field, $val) === true
            && $val[$field] !== null
            && $val[$field] !== "") {
            //
            $valF[$field] = $val[$field];
        } else {
            $this->addToMessage(sprintf(
                __("Le champ <b>%s</b> est obligatoire"),
                __($field)
            ));
            $this->correct = false;
        }
        //
        if ($this->correct !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $ret = $this->update_autoexecute($valF, $this->getVal($this->clePrimaire), false);
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        // Transmission REU
        $autorite_ugle = ($val["annulation_autorite_type"] == "CSL" ? $val["annulation_autorite_consulat"] : $val["annulation_autorite_commune"]);
        $ret = $this->handle_reu_demande_procuration(array(
            "mode" => "cancel",
            "id_externe" => $this->getVal("id_externe"),
            "annulation_date" => $val["annulation_date"],
            "annulation_autorite_type" => $val["annulation_autorite_type"],
            "annulation_autorite_ugle" => $autorite_ugle,
            "annulation_autorite_nom_prenom" => $val["annulation_autorite_nom_prenom"],
        ));
        if ($ret !== true) {
            $this->addToMessage(print_r($ret, true));
            return $ret;
        }
        //
        return $this->end_treatment(__METHOD__, true);
    }

    function handle_reu_demande_procuration($val = array()) {
        $id = $this->getVal($this->clePrimaire);
        $id_externe = null;
        if (array_key_exists("mode", $val) === true
            && $val["mode"] == "add") {
            //
            $mode = "add";
            $datas = array(
                "date_debut_validite" => $this->f->formatDate($val["debut_validite"], true),
                "date_fin_validite" => $this->f->formatDate($val["fin_validite"], true),
                "date_etablissement" => $this->f->formatDate($val["date_accord"], true),
                "mandant_ine" => intval($val["mandant_ine"]),
                "mandataire_ine" => intval($val["mandataire_ine"]),
                "autorite_type" => $val["autorite_type"],
                "autorite_nom_prenom" => trim($val["autorite_nom_prenom"]),
                "autorite_ugle" => $val["autorite_ugle"],
            );
        } elseif (array_key_exists("mode", $val) === true
            && $val["mode"] == "cancel") {
            //
            $mode = "cancel";
            $datas = array(
                "date_etablissement" => $this->f->formatDate($val["annulation_date"], true),
                "autorite_type" => $val["annulation_autorite_type"],
                "autorite_nom_prenom" => trim($val["annulation_autorite_nom_prenom"]),
                "autorite_ugle" => $val["annulation_autorite_ugle"],
            );
            $id_externe = $val["id_externe"];
        } else {
            return $this->end_treatment(__METHOD__, false);
        }
        $params = array(
            "mode" => $mode,
            "datas" => $datas,
        );
        if ($id_externe !== null) {
            $params["id"] = $id_externe;
        }
        $inst_reu = $this->f->get_inst__reu();
        $ret = $inst_reu->handle_procuration($params);
/*        // En cas d'erreur 400, on souhaite enregistrer le retour
        // dans certains cas de figure
        if (array_key_exists("code", $ret) === true
            && $ret["code"] === 400
            && array_key_exists("result", $ret) === true
            && is_array($ret["result"]) === true
            && array_key_exists("cle", $ret["result"]) === true
            && in_array($ret["result"]["cle"],
                array(
                    "procurationView.mandant.procurationExistante",
                    "procurationView.dateFin.superieurMaxAutorise",
                    "procurationView.dateFin.incoherent",
                )) === true
            ) {
            $valF = array();
            if ($mode == "add") {
                $valF = array(
                    "statut" => "demande_refusee",
                    "vu" => "f",
                    "motif_refus" => $ret["result"]["message"],
                );
                $ret = $this->update_autoexecute($valF, $id, false);
                if ($ret !== true) {
                    return $this->end_treatment(__METHOD__, false);
                }
            }
            // Gestion de l'historique du mouvement
            $val_histo = array(
                'id' => $id,
                'action' => __METHOD__,
                'message' => __('Demande refusée par le REU.'),
                'datas' => $valF,
            );
            $histo = $this->handle_historique($val_histo);
            if ($histo !== true) {
                $this->addToMessage("Erreur lors de la mise à jour de l'historique de la procuration.");
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $this->addToMessage("Demande refusée par le REU.");
            return $this->end_treatment(__METHOD__, true);
        }*/
        if (isset($ret["code"]) !== true
            || ($mode == "cancel" && $ret["code"] != 201)
            || ($mode == "add" && $ret["code"] != 201)) {
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            return $this->end_treatment(__METHOD__, false);
        }
        $valF = array();
        if ($mode == "add") {
            $procuration = $inst_reu->handle_procuration(array(
                "mode" => "get",
                "id" => trim($ret["headers"]["X-App-params"]),
            ));
            $valF = array(
                "mandant_infos" => json_encode(gavoe($procuration, array("result", "electeurMandant", ))),
                "mandataire_infos" => json_encode(gavoe($procuration, array("result", "electeurMandataire", ))),
                "id_externe" => trim($ret["headers"]["X-App-params"]),
                "statut" => "procuration_confirmee",
            );
            $ret = $this->update_autoexecute($valF, $id, false);
            if ($ret !== true) {
                return $this->end_treatment(__METHOD__, false);
            }
        } elseif ($mode == "cancel") {
            $valF = array(
                "statut" => "procuration_annulee",
            );
            $ret = $this->update_autoexecute($valF, $id, false);
            if ($ret !== true) {
                return $this->end_treatment(__METHOD__, false);
            }
        }
        // Gestion de l'historique du mouvement
        $val_histo = array(
            'id' => $id,
            'action' => __METHOD__,
            'message' => __('Transmission au REU.'),
            'datas' => $valF,
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->addToMessage("Erreur lors de la mise à jour de l'historique de la procuration.");
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage("Transmission au REU OK.");
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function handle_historique($val = array()) {
        //
        if (isset($val['id']) !== true) {
            $val['id'] = $this->getVal($this->clePrimaire);
        }
        //
        $histo = array();
        //
        $add_histo = array(
            'date' => (isset($val['date']) === true ? $val['date'] : date('Y-m-d H:i:s')),
            'user' => (isset($val['login']) === true ? $val['login'] : $_SESSION['login']),
            'action' => (isset($val['action']) === true ? $val['action'] : sprintf(
                '%1$s %2$s',
                $this->getParameter('maj'),
                $this->get_action_param($this->getParameter('maj'), 'identifier')
            )),
            'message' => (isset($val['message']) === true ? $val['message'] : sprintf(
                __('Action %1$s %2$s effectuée.'),
                $this->getParameter('maj'),
                $this->get_action_param($this->getParameter('maj'), 'identifier')
            )),
        );
        //
        if (isset($val['datas']) === true 
            && $val['datas'] !== ''
            && $val['datas'] !== null) {
            //
            $add_histo['datas'] = $val['datas'];
        }
        //
        $histo[] = $add_histo;
        $histo_exists = $this->f->get_one_result_from_db_query(sprintf(
            'SELECT historique FROM %1$sprocuration WHERE id=\'%2$s\'',
            DB_PREFIXE,
            $val['id']
        ), true);
        if ($histo_exists['code'] !== 'OK') {
            return false;
        }
        if ($histo_exists['result'] !== ''
            && $histo_exists['result'] !== null
            && $histo_exists['result'] !== 'null'
            && json_decode($histo_exists['result'], true) !== null) {
            //
            $histo = array_merge($histo, json_decode($histo_exists['result'], true));
        }
        //
        $valF = array(
            "historique" => json_encode($histo),
        );
        $ret = $this->update_autoexecute($valF, $val['id'], false);
        if ($ret !== true) {
            return false;
        }
        return true;
    }

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     *
     * @return array
     */
    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        //
        $mandant = $this->get_inst__mandant();
        $mandant_labels = $mandant->get_merge_fields("labels");
        $labels["mandant"] = array();
        foreach ($mandant_labels["electeur"] as $key => $value) {
            if (preg_match("/electeur./i", $key)) {
                $labels["mandant"][str_replace("electeur.", "mandant.", $key)] = $value;
            } else {
                $labels["mandant"][str_replace("bureau.", "mandant_bureau.", $key)] = $value;
            }
        }
        //
        $mandataire = $this->get_inst__mandataire();
        $mandataire_labels = $mandataire->get_merge_fields("labels");
        $labels["mandataire"] = array();
        foreach ($mandataire_labels["electeur"] as $key => $value) {
            if (preg_match("/electeur./i", $key)) {
                $labels["mandataire"][str_replace("electeur.", "mandataire.", $key)] = $value;
            } else {
                $labels["mandataire"][str_replace("bureau.", "mandataire_bureau.", $key)] = $value;
            }
        }
        // Retour de tous les libellés
        return $labels;
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     *
     * @return array
     */
    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        //
        $mandant = $this->get_inst__mandant();
        $mandant_values = $mandant->get_merge_fields("values");
        $mandant_new_values = array();
        foreach ($mandant_values as $key => $value) {
            if (preg_match("/electeur./i", $key)) {
                $mandant_new_values[str_replace("electeur.", "mandant.", $key)] = $value;
            } else {
                $mandant_new_values[str_replace("bureau.", "mandant_bureau.", $key)] = $value;
            }
        }
        //
        $mandataire = $this->get_inst__mandataire();
        $mandataire_values = $mandataire->get_merge_fields("values");
        $mandataire_new_values = array();
        foreach ($mandataire_values as $key => $value) {
            if (preg_match("/electeur./i", $key)) {
                $mandataire_new_values[str_replace("electeur.", "mandataire.", $key)] = $value;
            } else {
                $mandataire_new_values[str_replace("bureau.", "mandataire_bureau.", $key)] = $value;
            }
        }
        //
        $values = array_merge(
            $values,
            $mandant_new_values,
            $mandataire_new_values
        );
        //
        return $values;
    }
}
