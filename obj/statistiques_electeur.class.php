<?php
/**
 * Ce script définit la classe 'statistiques_electeur'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_base.class.php";

/**
 * Définition de la classe 'statistiques_electeur' (om_base).
 */
class statistiques_electeur extends om_base {

    /**
     *
     */
    var $electeurs_actuels_groupby_bureau = array();
    var $inscriptions_radiations_stats_groupby_bureau = array();
    var $transferts_plus_stats_groupby_bureau = array();
    var $transferts_moins_stats_groupby_bureau = array();

    /**
     *
     */
    function __construct() {
        //
        $this->init_om_application();
        //
        require_once "../sql/".OM_DB_PHPTYPE."/statistiques_electeur.inc.php";

        // Nombre actuel d'electeurs dans la base de donnees par bureau
        // array("<BUREAU>" => array("M" => <>, "F" => <>, "total" => <>),);
        $res_electeurs_actuels_groupby_bureau = $this->f->db->query($query_electeurs_actuels_groupby_bureau);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_electeurs_actuels_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_electeurs_actuels_groupby_bureau);
        while ($row_electeurs_actuels_groupby_bureau =& $res_electeurs_actuels_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($this->electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]])) {
                $this->electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]] = array();
            }
            //
            $this->electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]][$row_electeurs_actuels_groupby_bureau["sexe"]] = $row_electeurs_actuels_groupby_bureau["total"];
        }
        foreach ($this->electeurs_actuels_groupby_bureau as $key => $value) {
            $this->electeurs_actuels_groupby_bureau[$key]["total"] = (isset($this->electeurs_actuels_groupby_bureau[$key]["M"]) ? $this->electeurs_actuels_groupby_bureau[$key]["M"] : 0) + (isset($this->electeurs_actuels_groupby_bureau[$key]["F"]) ? $this->electeurs_actuels_groupby_bureau[$key]["F"] : 0);
        }

        //
        $res_inscriptions_radiations_stats_groupby_bureau = $this->f->db->query($query_inscriptions_radiations_stats_groupby_bureau);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_inscriptions_radiations_stats_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_inscriptions_radiations_stats_groupby_bureau);
        while ($row_inscriptions_radiations_stats_groupby_bureau =& $res_inscriptions_radiations_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($this->inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]])) {
                $this->inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]] = array();
            }
            //
            array_push($this->inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]], $row_inscriptions_radiations_stats_groupby_bureau);
        }
        $this->f->addToLog(__METHOD__."(): ".print_r($this->inscriptions_radiations_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);

        //
        $res_transferts_plus_stats_groupby_bureau = $this->f->db->query($query_transferts_plus_stats_groupby_bureau);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_transferts_plus_stats_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_transferts_plus_stats_groupby_bureau);
        while ($row_transferts_plus_stats_groupby_bureau =& $res_transferts_plus_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($this->transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]])) {
                $this->transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]] = array();
            }
            //
            array_push($this->transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]], $row_transferts_plus_stats_groupby_bureau);
        }
        $this->f->addToLog(__METHOD__."(): ".print_r($this->transferts_plus_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);

        //
        $res_transferts_moins_stats_groupby_bureau = $this->f->db->query($query_transferts_moins_stats_groupby_bureau);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_transferts_moins_stats_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_transferts_moins_stats_groupby_bureau);
        while ($row_transferts_moins_stats_groupby_bureau =& $res_transferts_moins_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($this->transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]])) {
                $this->transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]] = array();
            }
            //
            array_push($this->transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]], $row_transferts_moins_stats_groupby_bureau);
        }
        $this->f->addToLog(__METHOD__."(): ".print_r($this->transferts_moins_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);
    }


    //
    function calculNombreDelecteursDateTableauParBureau($date_tableau_reference, $bureau, $sexe = "ALL") {
        //
        if ($sexe == "ALL") {
            $cpt_electeur = $this->electeurs_actuels_groupby_bureau[$bureau]["total"];
        } else {
            $cpt_electeur = $this->electeurs_actuels_groupby_bureau[$bureau][$sexe];
        }
        //
        if (isset($this->inscriptions_radiations_stats_groupby_bureau[$bureau])) {
        foreach ($this->inscriptions_radiations_stats_groupby_bureau[$bureau] as $element) {
            if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                //
                if ($element["date_tableau"] > $date_tableau_reference) {
                    if ($element["etat"] == "trs") {
                        if ($element["typecat"] == "Inscription") {
                            $cpt_electeur -= $element["total"];
                        } elseif ($element["typecat"] == "Radiation") {
                            $cpt_electeur += $element["total"];
                        }
                    }
                } else {
                    if ($element["etat"] == "actif") {
                        if ($element["typecat"] == "Inscription") {
                            $cpt_electeur += $element["total"];
                        } elseif ($element["typecat"] == "Radiation") {
                            $cpt_electeur -= $element["total"];
                        }
                    }
                }
            }
        }
        }
        //
        if (isset($this->transferts_plus_stats_groupby_bureau[$bureau])) {
        foreach ($this->transferts_plus_stats_groupby_bureau[$bureau] as $element) {
            if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                //
                if ($element["date_tableau"] > $date_tableau_reference) {
                    if ($element["etat"] == "trs") {
                        $cpt_electeur -= $element["total"];
                    }
                } else {
                    if ($element["etat"] == "actif") {
                        $cpt_electeur += $element["total"];
                    }
                }
            }
        }
        }
        //
        if (isset($this->transferts_moins_stats_groupby_bureau[$bureau])) {
        foreach ($this->transferts_moins_stats_groupby_bureau[$bureau] as $element) {
            if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                //
                if ($element["date_tableau"] > $date_tableau_reference) {
                    if ($element["etat"] == "trs") {
                            $cpt_electeur += $element["total"];
                    }
                } else {
                    if ($element["etat"] == "actif") {
                        $cpt_electeur -= $element["total"];
                    }
                }
            }
        }
        }
        return $cpt_electeur;
    }

    //
    function calculNombreDelecteursDateParBureau($date_reference, $bureau, $sexe = "ALL") {
        //
        if ($sexe == "ALL") {
            $cpt_electeur = $this->electeurs_actuels_groupby_bureau[$bureau]["total"];
        } else {
            $cpt_electeur = $this->electeurs_actuels_groupby_bureau[$bureau][$sexe];
        }
        //
        if (isset($this->inscriptions_radiations_stats_groupby_bureau[$bureau])) {
        foreach ($this->inscriptions_radiations_stats_groupby_bureau[$bureau] as $element) {
            if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                //
                if (($element["date_tableau"] > $date_reference && $element["tableau"] == "annuel") || ($element["date_j5"] > $date_reference && $element["tableau"] == "j5")) {
                    if ($element["etat"] == "trs") {
                        if ($element["typecat"] == "Inscription") {
                            $cpt_electeur -= $element["total"];
                        } elseif ($element["typecat"] == "Radiation") {
                            $cpt_electeur += $element["total"];
                        }
                    }
                } else {
                    if ($element["etat"] == "actif") {
                        if ($element["typecat"] == "Inscription") {
                            $cpt_electeur += $element["total"];
                        } elseif ($element["typecat"] == "Radiation") {
                            $cpt_electeur -= $element["total"];
                        }
                    }
                }
            }
        }
        }
        //
        if (isset($this->transferts_plus_stats_groupby_bureau[$bureau])) {
        foreach ($this->transferts_plus_stats_groupby_bureau[$bureau] as $element) {
            if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                //
                if (($element["date_tableau"] > $date_reference && $element["tableau"] == "annuel") || ($element["date_j5"] > $date_reference && $element["tableau"] == "j5")) {
                    if ($element["etat"] == "trs") {
                        $cpt_electeur -= $element["total"];
                    }
                } else {
                    if ($element["etat"] == "actif") {
                        $cpt_electeur += $element["total"];
                    }
                }
            }
        }
        }
        //
        if (isset($this->transferts_moins_stats_groupby_bureau[$bureau])) {
        foreach ($this->transferts_moins_stats_groupby_bureau[$bureau] as $element) {
            if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                //
                if (($element["date_tableau"] > $date_reference && $element["tableau"] == "annuel") || ($element["date_j5"] > $date_reference && $element["tableau"] == "j5")) {
                    if ($element["etat"] == "trs") {
                            $cpt_electeur += $element["total"];
                    }
                } else {
                    if ($element["etat"] == "actif") {
                        $cpt_electeur -= $element["total"];
                    }
                }
            }
        }
        }
        return $cpt_electeur;
    }


}


