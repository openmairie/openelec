<?php
/**
 * Ce script définit la classe 'module_traitement_annuel'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_traitement_annuel' (module).
 */
class module_traitement_annuel extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_traitement_annuel",
            "title" => __("Traitement")." -> ".__("Module Traitement annuel"),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_traitement_annuel-onglet_traitement_annuel",
                    "href" => "../app/index.php?module=module_traitement_annuel&view=traitement_annuel",
                    "title" => __("Traitement annuel"),
                    "view" => "traitement_annuel",
                ),
                array(
                    "type" => "edition_pdf",
                    "right" => "module_traitement_annuel-edition_pdf__verification_doublon_traitement",
                    "view" => "edition_pdf__verification_doublon_traitement",
                ),
            ),
        );
    }

    /**
     *
     */
    function view__traitement_annuel() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_traitement_annuel");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        echo "<div class=\"alert ui-accordion-header ui-helper-reset ui-state-default ui-corner-all\">";
        echo "<img title=\"".__("Date de tableau")."\" alt=\"".__("Date de tableau")."\" src=\"../app/img/calendar.png\">";
        echo __("Tableau du")." ".$this->f->formatDate($this->f->getParameter("datetableau"));
        echo "</div>";

        /**
         *
         */
        //
        $description = __(
            "Le traitement annuel va appliquer les mouvements actifs a effet 'Annuel' ".
            "(1er mars) ainsi que ceux a effet 'Immediat' sur la liste electorale. Les ".
            "mouvements appliques sont ceux concernant la date de tableau en cours et ".
            "la liste par defaut selectionnee. Pour appliquer le traitement, il suffit ".
            "de suivre les etapes suivantes. Attention, les mouvements a effet 'Election' ".
            "ne sont pas appliques, ils doivent l'etre par le traitement J-5."
        );
        $this->f->displayDescription($description);


        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Quand appliquer le traitement annuel ?");
        $this->f->displaySubTitle($subtitle);
        // Affichage du texte du paragraphe
        $description = __(
            "Le traitement annuel doit etre applique le 10 janvier et le dernier ".
            "jour de fevrier une fois que la commission a dresse respectivement le premier et ".
            "le second tableau rectificatif."
        );
        $this->f->displayDescription($description);
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Etape 1 - Verification des doublons");
        $this->f->displaySubTitle($subtitle);
        //
        echo "<p>";
        echo __("Cette recherche verifie que les inscriptions dans l'etat actif n'ont ".
               "pas de doublons dans la liste electorale. C'est-a-dire si une nouvelle ".
               "inscription d'electeur n'est pas deja inscrit dans la liste (meme nom ".
               "et date de naissance).");
        echo "</p>";
        //
        $links = array(
            "0" => array(
                "href" => "../app/index.php?module=module_traitement_annuel&view=edition_pdf__verification_doublon_traitement",
                "target" => "_blank",
                "class" => "om-prev-icon doublon-16",
                "title" => __("Verification des doublons"),
                "id" => "action-traitement_annuel-verification-doublon",
            ),
        );
        //
        $this->f->displayLinksAsList($links);
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Traitement annuel");
        $subtitle = "-> ".__("Etape 2 - Verification et application du Traitement annuel du")." ".$this->f->formatDate ($this->f->getParameter("datetableau"))." [Tableau du ".$this->f->formatDate ($this->f->getParameter("datetableau"))."]";
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.annuel.class.php";
        $trt = new annuelTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Etape 3 - Edition des nouvelles cartes d'electeur");
        $this->f->displaySubTitle($subtitle);
        // Affichage du texte du paragraphe
        $description = __("Une fois le traitement applique, il est possible d'editer les nouvelles cartes electorales a tout moment depuis l'ecran");
        $description .= " <a href=\"../app/view_revision_electorale.php\">'Edition -> Revision Electorale'</a>.";
        $this->f->displayDescription($description);
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }

    /**
     * VIEW - view__edition_pdf__verification_doublon_traitement.
     *
     * @return void
     */
    function view__edition_pdf__verification_doublon_traitement() {
        //
        require_once "../obj/traitement.annuel.class.php";
        $trt = new annuelTraitement();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $trt->compute_stats__verification_doublon_traitement(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }
}
