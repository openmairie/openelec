<?php
/**
 * Ce script définit la classe 'redecoupageTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'redecoupageTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class redecoupageTraitement extends traitement {

    var $fichier = "redecoupage";
    var $champs = array("dry_run", );

    function getDescription() {
        //
        return __("Ce traitement applique le decoupage en creant les mouvements ".
                 "necessaires pour que les electeurs soient dans le bureau qui ".
                 "correspond a leur adresse. Ce traitement modifie egalement ".
                 "les mouvements en cours.");
    }

    function setContentForm() {
        //
        $this->form->setLib("dry_run", __("Mode \"Essai\" - si cette case est cochee ".
                                         "alors le traitement n'est pas applique ".
                                         "seuls les resultats sont affiches."));
        $this->form->setType("dry_run", "checkbox");
        $this->form->setTaille("dry_run", 1);
        $this->form->setMax("dry_run", 1);
    }

    function getValidButtonValue() {
        //
        return __("Redecoupage electoral");
    }

    function treatment () {
        //
        $this->LogToFile("start redecoupage");
        //
        (isset($_POST['dry_run']) ? $dry_run = true : $dry_run = false);
        if ($dry_run === true) {
            echo __("Le mode \"Essai\" est active. Aucune modification ne sera appliquee.");
        }
        //
        $this->LogToFile("dry_run : ".($dry_run == true ? "true" : "false")."");
        //
        $DEBUG = 0;
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        //                                                                           //
        //            REDECOUPAGE DES BUREAUX                                        //
        //                                                                           //
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        if ($DEBUG == 1) {
            echo "<pre>";
        }


        // Initialisation des compteurs
        $cpt = array(
            'mouv_new' => 0,
            'mouv_mod' => 0,
            'mouv_old' => 0,
            'mouv_rad' => 0,
            'mouv_ins' => 0,
            'mouv_ins_mod' => 0
        );

        ///////////////////////////////////////////////////////////////////////////////
        // Requête: SELECT on récupère la table bureau
        $bureaux_to_treat = $this->f->get_all__bureau__by_my_collectivite();
        if ($DEBUG == 1) {
            echo "<b>bureaux_</b>";
            print_r($bureaux_to_treat);
        }
        ///////////////////////////////////////////////////////////////////////////////

        // Pour chaque bureau
        foreach ($bureaux_to_treat as $bureau_to_treat) {
            ///////////////////////////////////////////////////////////////
            // Requête: SELECT on récupère la table électeur pour un bureau : séparation des traitements
            $sql = sprintf(
                'SELECT electeur.id, liste, bureau, nom, nom_usage, prenom, date_naissance, code_voie, libelle_voie, numero_habitation FROM %1$selecteur WHERE electeur.om_collectivite=%2$s AND electeur.bureau=%3$s and electeur.bureauforce<>\'Oui\' and electeur.code_voie<>\'\'',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $bureau_to_treat['id']
            );
            $res = $this->f->db->query($sql);
            $this->f->addToLog(
                __METHOD__."(): db->query(\"".$sql."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            //
            $electeurs_to_treat = array();
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                array_push($electeurs_to_treat, $row);
            }
            if ($DEBUG == 1) {
                echo "<b>bureau_".$bureau_to_treat['id']."_</b>";
                print_r($electeurs_to_treat);
            }
            ///////////////////////////////////////////////////////////////

            // Pour chaque électeur
            foreach ($electeurs_to_treat as $electeur_to_treat) {
                ///////////////////////////////////////////////////////////////
                // Requête: SELECT on vérifie si un mouvement concerne l'électeur
                $sql = sprintf(
                    'SELECT id, types, typecat FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.electeur_id=%3$s AND mouvement.om_collectivite=%2$s AND etat=\'actif\'',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $electeur_to_treat['id']
                );
                $res = $this->f->db->query($sql);
                $this->f->addToLog(
                    __METHOD__."(): db->query(\"".$sql."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                //
                $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);
                $mouv = $row;
                if ($DEBUG == 1) {
                    echo "<b>mouvement_electeur_".$electeur_to_treat['id']."_</b>";
                    print_r($mouv);
                    echo "<br />";
                }
                ///////////////////////////////////////////////////////////////

                // Si aucun mouvement ne concerne l'électeur
                if (! isset($mouv['id']) || $mouv['id'] == "") {
                    ///////////////////////////////////////////////////////////////
                    // Requête: SELECT on récupère le code bureau correspondant à la voie de l'électeur dans la table découpage
                    if ($electeur_to_treat["numero_habitation"] % 2 == 0) {
                        $sql = sprintf(
                            'SELECT bureau FROM %1$sdecoupage WHERE code_voie=\'%2$s\' AND premier_pair<=%3$s AND dernier_pair>=%3$s',
                            DB_PREFIXE,
                            $electeur_to_treat["code_voie"],
                            $electeur_to_treat['numero_habitation']
                        );
                    } else {
                        $sql = sprintf(
                            'SELECT bureau FROM %1$sdecoupage WHERE code_voie=\'%2$s\' AND premier_impair<=%3$s AND dernier_impair>=%3$s',
                            DB_PREFIXE,
                            $electeur_to_treat["code_voie"],
                            $electeur_to_treat['numero_habitation']
                        );
                    }
                    $bureau = $this->f->db->getone($sql);
                    $this->f->addToLog(
                        __METHOD__."(): db->getone(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($bureau);
                    //
                    if ($DEBUG == 1)
                    {
                        echo "<b>bureau_decoupage_</b>";
                        echo $bureau;
                        echo "<br />";
                    }

                    // Si le code du bureau est NULL, c'est qu'il n'y a pas d'entrée
                    // dans le découpage pour l'électeur en cours de traitement
                    // Le traitement ne peut pas continuer
                    if ($bureau == NULL) {
                        //
                        $this->LogToFile("impossible de trouver le bureau d'affectation pour l'electeur : ".print_r($electeur_to_treat, true));
                        //
                        $this->error = true;
                        $this->addToMessage(__("Probleme dans le decoupage."));
                        //
                        return;
                    }

                    ///////////////////////////////////////////////////////////////

                    // Si le code bureau de l'électeur ne correspond pas au code bureau du découpage
                    if ($bureau != $electeur_to_treat['bureau'])
                    {
                        ///////////////////////////////////////////////////////////////
                        // Requête: SELECT on récupère toutes les infos électeur
                        $sql = sprintf(
                            'SELECT * FROM %1$selecteur WHERE electeur.id=%2$s',
                            DB_PREFIXE,
                            $electeur_to_treat['id']
                        );
                        $res = $this->f->db->query($sql);
                        $this->f->addToLog(
                            __METHOD__."(): db->query(\"".$sql."\");",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($res);
                        //
                        $row=& $res->fetchRow(DB_FETCHMODE_ASSOC);
                        $new_mouv = $row;
                        if ($DEBUG == 1) {
                            echo "<b>electeur_".$electeur_to_treat['id']."_</b>";
                            print_r($new_mouv);
                        }
                        ///////////////////////////////////////////////////////////////

                        ///////////////////////////////////////////////////////////////
                        // Requête: SELECT on récupère le dernier numero provisoire du bureau
                        $sql = sprintf(
                            'SELECT dernier_numero_provisoire FROM %1$snumerobureau WHERE numerobureau.om_collectivite=%2$s AND numerobureau.bureau=%3$s and numerobureau.liste=\'%4$s\'',
                            DB_PREFIXE,
                            intval($_SESSION["collectivite"]),
                            intval($bureau),
                            $new_mouv["liste"]
                        );
                        $new_numero_bureau = $this->f->db->getone($sql);
                        $this->f->addToLog(
                            __METHOD__."(): db->getone(\"".$sql."\");",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($new_numero_bureau);
                        //
                        if ($DEBUG == 1) {
                            echo "<b>dernier_numero_bureau_</b>";
                            echo $new_numero_bureau;
                            echo " ";
                        }
                        ///////////////////////////////////////////////////////////////

                        ///////////////////////////////////////////////////////////////
                        // Requête: UPDATE on met à jour le dernier numero provisoire du bureau
                        $sql = sprintf(
                            'UPDATE %1$snumerobureau SET dernier_numero_provisoire = (dernier_numero_provisoire+1) WHERE bureau=%2$s and liste=\'%3$s\'',
                            DB_PREFIXE,
                            intval($bureau),
                            $new_mouv["liste"]
                        );
                        $res = $this->f->db->query($sql);
                        $this->f->addToLog(
                            __METHOD__."(): db->query(\"".$sql."\");",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($res);
                        //
                        if ($DEBUG == 1) {
                            echo "OK<br />";
                        }
                        ///////////////////////////////////////////////////////////////

                        ///////////////////////////////////////////////////////////////
                        // Requête: SELECT on récupère le n° de séquence de la table mouvement (incrémentation)
                        $sql = sprintf(
                            'SELECT nextval(\'%1$smouvement_seq\')',
                            DB_PREFIXE
                        );
                        $id = $this->f->db->getone($sql);
                        $this->f->addToLog(
                            __METHOD__."(): db->getone(\"".$sql."\");",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($id);
                        //
                        if ($DEBUG == 1) {
                            echo "<b>nouveau_mouvement_</b>".$id;
                            echo " ";
                        }
                        ///////////////////////////////////////////////////////////////
                        // Requête: INSERT on insère le nouveau mouvement
                        $valF = array(
                            "id" => $id,
                            "etat" => 'actif',
                            "liste" => $new_mouv['liste'],
                            "types" => 'BR',
                            "electeur_id" => $new_mouv['id'],
                            "bureau" => $bureau,
                            "bureauforce" => $new_mouv['bureauforce'],
                            "numero_bureau" => $new_numero_bureau,
                            "date_modif" => date("Y-m-d"),
                            "utilisateur" => $_SESSION ['login'],
                            "civilite" => $new_mouv['civilite'],
                            "sexe" => $new_mouv['sexe'],
                            "nom" => $new_mouv['nom'],
                            "nom_usage" => $new_mouv['nom_usage'],
                            "prenom" => $new_mouv['prenom'],
                            "date_naissance" => $new_mouv['date_naissance'],
                            "code_departement_naissance" => $new_mouv['code_departement_naissance'],
                            "libelle_departement_naissance" => $new_mouv['libelle_departement_naissance'],
                            "code_lieu_de_naissance" => $new_mouv['code_lieu_de_naissance'],
                            "libelle_lieu_de_naissance" => $new_mouv['libelle_lieu_de_naissance'],
                            "code_nationalite" => $new_mouv['code_nationalite'],
                            "code_voie" => $new_mouv['code_voie'],
                            "libelle_voie" => $new_mouv['libelle_voie'],
                            "numero_habitation" => $new_mouv['numero_habitation'],
                            "complement_numero" => $new_mouv['complement_numero'],
                            "complement" => $new_mouv['complement'],
                            "provenance" => $new_mouv['provenance'],
                            "libelle_provenance" => $new_mouv['libelle_provenance'],
                            "ancien_bureau" => $new_mouv['bureau'],
                            "observation" => __("Modification redecoupage"),
                            "resident" => $new_mouv['resident'],
                            "adresse_resident" => $new_mouv['adresse_resident'],
                            "complement_resident" => $new_mouv['complement_resident'],
                            "cp_resident" => $new_mouv['cp_resident'],
                            "ville_resident" => $new_mouv['ville_resident'],
                            "tableau" => 'annuel',
                            "date_j5" => NULL,
                            "date_tableau" => $this->f->getParameter("datetableau"),
                            "envoi_cnen" => '',
                            "statut" => "ouvert",
                            "date_cnen" => NULL,
                            "ine" => $new_mouv["ine"],
                            "om_collectivite" => $_SESSION["collectivite"],
                        );
                        //
                        $res = $this->f->db->autoexecute(
                            sprintf('%1$smouvement', DB_PREFIXE),
                            $valF,
                            DB_AUTOQUERY_INSERT
                        );
                        $this->f->addToLog(
                            __METHOD__."(): db->autoexecute(\"".sprintf('%1$smouvement', DB_PREFIXE)."\", ".print_r($valF, true).", DB_AUTOQUERY_INSERT);",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($res);
                        //
                        if ($DEBUG == 1)
                        {
                            echo "OK<br />";
                        }
                        ///////////////////////////////////////////////////////////////

                        ///////////////////////////////////////////////////////////////
                        // Requête: UPDATE on met à jour l'enregistrement de l'électeur pour indiquer qu'il est en modification
                        $sql = sprintf(
                            'UPDATE %1$selecteur SET mouvement=%2$s, typecat=\'modification\' WHERE id=%3$s',
                            DB_PREFIXE,
                            $id,
                            $new_mouv["id"]
                        );
                        $res = $this->f->db->query($sql);
                        $this->f->addToLog(
                            __METHOD__."(): db->query(\"".$sql."\");",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($res);
                        //
                        if ($DEBUG == 1)
                        {
                            echo "<b>flag_electeur</b> OK<br />";
                        }
                        ///////////////////////////////////////////////////////////////

                        // on commite les changements dans la base
                        if ($dry_run === false) {
                            $this->f->db->commit() ;
                        } else {
                            $this->f->db->rollback() ;
                        }
                        $cpt ['mouv_new']++;
                    }
                }
                else
                {
                    if (isset($mouv['typecat']) && $mouv['typecat'] == 'Modification')
                    {
                        $cpt ['mouv_old']++;

                        ///////////////////////////////////////////////////////////////
                        // Requête: SELECT on récupère le numéro de voie et le nouveau bureau du mouvement
                        $sql = sprintf(
                            'SELECT code_voie, bureau, bureauforce, numero_habitation FROM %1$smouvement WHERE id=%2$s',
                            DB_PREFIXE,
                            $mouv['id']
                        );
                        $res = $this->f->db->query($sql);
                        $this->f->addToLog(
                            __METHOD__."(): db->query(\"".$sql."\");",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($res);
                        //
                        $row=& $res->fetchRow();
                        $code_voie = $row[0];
                        $bureau_actuel = $row[1];
                        $bureauforce =$row[2];
                        $numero_habitation = $row[3];
                        ///////////////////////////////////////////////////////////////

                        if ($bureauforce == 'Non')
                        {
                            ///////////////////////////////////////////////////////////////
                            // Requête: SELECT on récupère le numéro du bureau correspondant au numéro de voie
                            if ($numero_habitation % 2 == 0) {
                                $sql = sprintf(
                                    'SELECT bureau FROM %1$sdecoupage WHERE code_voie=\'%2$s\' AND premier_pair<=%3$s AND dernier_pair>=%3$s',
                                    DB_PREFIXE,
                                    $code_voie,
                                    $numero_habitation
                                );
                            } else {
                                $sql = sprintf(
                                    'SELECT bureau FROM %1$sdecoupage WHERE code_voie=\'%2$s\' AND premier_impair<=%3$s AND dernier_impair>=%3$s',
                                    DB_PREFIXE,
                                    $code_voie,
                                    $numero_habitation
                                );
                            }
                            $bureau = $this->f->db->getone($sql);
                            $this->f->addToLog(
                                __METHOD__."(): db->getone(\"".$sql."\");",
                                VERBOSE_MODE
                            );
                            $this->f->isDatabaseError($bureau);
                            ///////////////////////////////////////////////////////////////

                            //
                            if ($bureau != $bureau_actuel)
                            {
                                ///////////////////////////////////////////////////////////////
                                // Requête: SELECT on récupère le dernier numero provisoire du bureau
                                $sql = sprintf(
                                    'SELECT dernier_numero_provisoire FROM %1$snumerobureau WHERE numerobureau.om_collectivite=%2$s AND numerobureau.bureau=%3$s AND numerobureau.liste=\'01\'',
                                    DB_PREFIXE,
                                    intval($_SESSION["collectivite"]),
                                    intval($bureau)
                                );
                                $new_numero_bureau = $this->f->db->getone($sql);
                                $this->f->addToLog(
                                    __METHOD__."(): db->getone(\"".$sql."\");",
                                    VERBOSE_MODE
                                );
                                $this->f->isDatabaseError($new_numero_bureau);
                                ///////////////////////////////////////////////////////////////

                                ///////////////////////////////////////////////////////////////
                                // Requête: UPDATE on met à jour le dernier numero provisoire du bureau
                                $sql = sprintf(
                                    'UPDATE %1$snumerobureau SET dernier_numero_provisoire=(dernier_numero_provisoire+1) WHERE bureau=%2$s and liste=\'01\'',
                                    DB_PREFIXE,
                                    intval($bureau)
                                );
                                $res = $this->f->db->query($sql);
                                $this->f->addToLog(
                                    __METHOD__."(): db->query(\"".$sql."\");",
                                    VERBOSE_MODE
                                );
                                $this->f->isDatabaseError($res);
                                ///////////////////////////////////////////////////////////////

                                ///////////////////////////////////////////////////////////////
                                // Requête: UPDATE on met à jour la table mouvement
                                $sql = sprintf(
                                    'UPDATE %1$smouvement SET bureau=%2$s, numero_bureau=\'%3$s\' where id=%4$s',
                                    DB_PREFIXE,
                                    intval($bureau),
                                    $new_numero_bureau,
                                    $mouv['id']
                                );
                                $res = $this->f->db->query($sql);
                                $this->f->addToLog(
                                    __METHOD__."(): db->query(\"".$sql."\");",
                                    VERBOSE_MODE
                                );
                                $this->f->isDatabaseError($res);
                                ///////////////////////////////////////////////////////////////

                                // on commite les changements dans la base
                                if ($dry_run === false) {
                                    $this->f->db->commit() ;
                                } else {
                                    $this->f->db->rollback() ;
                                }
                                $cpt ['mouv_mod']++;
                            }
                        }
                    }
                    elseif (isset($mouv['typecat']) && $mouv['typecat']=='Radiation')
                    {
                        $cpt ['mouv_rad']++;
                    }
                }

            }
        }

        // Traitement pour les inscriptions
        ///////////////////////////////////////////////////////////////
        // Requête: SELECT on récupère les nouveau inscrits
        $sql = sprintf(
            'SELECT mouvement.id, mouvement.code_voie, mouvement.bureau, mouvement.bureauforce, mouvement.numero_habitation FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.om_collectivite=%2$s AND mouvement.etat=\'actif\' AND lower(param_mouvement.typecat)=\'inscription\'',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $inscrit = array ();
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC))
            array_push ($inscrit, $row);
        ///////////////////////////////////////////////////////////////

        foreach ($inscrit as $ins)
        {
            $cpt ['mouv_ins']++;
            if ($ins['bureauforce'] == 'Non')
            {
                ///////////////////////////////////////////////////////////////
                // Requête: SELECT on récupère le numéro du bureau correspondant au numéro de voie
                if ($ins['numero_habitation'] % 2 == 0) {
                    $sql = sprintf(
                        'SELECT bureau FROM %1$sdecoupage WHERE code_voie=\'%2$s\' AND premier_pair<=%3$s AND dernier_pair>=%3$s',
                        DB_PREFIXE,
                        $ins ['code_voie'],
                        $ins['numero_habitation']
                    );
                } else {
                    $sql = sprintf(
                        'SELECT bureau FROM %1$sdecoupage WHERE code_voie=\'%2$s\' AND premier_impair<=%3$s AND dernier_impair>=%3$s',
                        DB_PREFIXE,
                        $ins ['code_voie'],
                        $ins['numero_habitation']
                    );
                }
                $bureau = $this->f->db->getone($sql);
                $this->f->addToLog(
                    __METHOD__."(): db->getone(\"".$sql."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($bureau);
                ///////////////////////////////////////////////////////////////

                if ($bureau != $ins ['bureau'])
                {
                    ///////////////////////////////////////////////////////////////
                    // Requête: SELECT on récupère le dernier numero provisoire du bureau
                    $sql = sprintf(
                        'SELECT dernier_numero_provisoire FROM %1$snumerobureau WHERE bureau=%2$s AND liste=\'01\'',
                        DB_PREFIXE,
                        intval($bureau)
                    );
                    $new_numero_bureau = $this->f->db->getone($sql);
                    $this->f->addToLog(
                        __METHOD__."(): db->getone(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($new_numero_bureau);
                    ///////////////////////////////////////////////////////////////

                    ///////////////////////////////////////////////////////////////
                    // Requête: UPDATE on met à jour le dernier numero provisoire du bureau
                    $sql = sprintf(
                        'UPDATE %1$snumerobureau SET dernier_numero_provisoire=(dernier_numero_provisoire+1) WHERE bureau=%2$s and liste=\'01\'',
                        DB_PREFIXE,
                        intval($bureau)
                    );
                    $res = $this->f->db->query($sql);
                    $this->f->addToLog(
                        __METHOD__."(): db->query(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($res);
                    ///////////////////////////////////////////////////////////////

                    ///////////////////////////////////////////////////////////////
                    // Requête: UPDATE on met à jour la table mouvement
                    $sql = sprintf(
                        'UPDATE %1$smouvement SET bureau=%2$s, numero_bureau=\'%3$s\' WHERE id=%4$s',
                        DB_PREFIXE,
                        intval($bureau),
                        $new_numero_bureau,
                        $ins['id']
                    );
                    $res = $this->f->db->query($sql);
                    $this->f->addToLog(
                        __METHOD__."(): db->query(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($res);
                    ///////////////////////////////////////////////////////////////

                    // on commite les changements dans la base
                    if ($dry_run === false) {
                        $this->f->db->commit() ;
                    } else {
                        $this->f->db->rollback() ;
                    }
                    $cpt ['mouv_ins_mod']++;
                }
            }
        }

        // Affichage du résultat
        $message =  "Le nouveau découpage a été appliqué.<br />
        - ".$cpt ['mouv_new']." nouveaux mouvements correctements enregistrés,<br />
        - ".$cpt ['mouv_mod']." mouvements modifiés sur ".$cpt['mouv_old']." mouvements concernés,<br />
        - ".$cpt ['mouv_ins_mod']." inscriptions modifiées sur ".$cpt['mouv_ins']." inscriptions concernées,<br />
        - ".$cpt ['mouv_rad']." radiations concernées non modifiées.";
        $this->LogToFile($message);
        $this->addToMessage($message);
        ///////////////////////////////////////////////////////////////////////////////
        //
        ////
        //$res = $this->f->db->query($query_update_jury);
        ////
        //if ($this->f->isDatabaseError($res, true)) {
        //    //
        //    $this->error = true;
        //    //
        //    $message = $res->getMessage()." - ".$res->getUserInfo();
        //    $this->LogToFile($message);
        //    //
        //    $this->addToMessage(__("Contactez votre administrateur."));
        //} else {
        //    //
        //    $message = $this->f->db->affectedRows()." ".__("jure(s) de-selectionne(s)");
        //    $this->LogToFile($message);
        //    $this->addToMessage($message);
        //}
        ////
        $this->LogToFile("end redecoupage");
    }
}


