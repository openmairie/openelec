<?php
/**
 * Ce script définit la classe 'module_commission'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_commission' (module).
 */
class module_commission extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_commission",
            "title" => __("Traitement")." -> ".__("Module Commission"),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_commission-traitement_commission",
                    "href" => "../app/index.php?module=module_commission&view=commission",
                    "title" => __("Commissions"),
                    "view" => "commission",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_commission-convocation_commission",
                    "href" => "../app/index.php?module=module_commission&view=convocation",
                    "title" => __("Convocation"),
                    "view" => "convocation",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_commission-membre_commission",
                    "href" => OM_ROUTE_SOUSTAB."&obj=membre_commission&amp;idxformulaire=commission&amp;retourformulaire=commission",
                    "title" => __("Membres"),
                ),
            ),
        );
    }

    /**
     *
     */
    function view__commission() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_commission");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        //
        $description = __("Le traitement des commissions permet la preparation des ".
                         "commissions par l'édition de listing de mouvements.");
        $this->f->displayDescription($description);

        /**
         * Export des éditions de mouvement sur interval de dates
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div id=\"commission--edition-des-mouvements-entre-deux-dates\" class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Edition des mouvements à présenter (rupture par liste)");
        $this->f->displaySubTitle($subtitle);
        //
        echo "<form name=\"f1\" action=\".\">";
        //
        $champs = array("datedebut", "datefin", 'provenance_demande', 'par_bureau');
        //
        $form = $this->f->get_inst__om_formulaire(array(
            "validation" => 0,
            "maj" => 0,
            "champs" => $champs,
        ));
        //
        $form->setLib("datedebut", __("Date de debut"));
        $form->setType("datedebut", "date");
        $form->setTaille("datedebut", 10);
        $form->setMax("datedebut", 10);
        $form->setOnchange("datedebut", "fdate(this)");
        //
        $form->setLib("datefin", __("Date de fin"));
        $form->setType("datefin", "date");
        $form->setTaille("datefin", 10);
        $form->setMax("datefin", 10);
        $form->setOnchange("datefin", "fdate(this)");
        //
        $form->setLib("provenance_demande", __("Provenance des demandes"));
        $form->setType("provenance_demande", "select_multiple");
        $form->setTaille("provenance_demande", 10);
        $form->setMax("provenance_demande", 10);
        $form->setSelect("provenance_demande", array(
            0 => array(
                'MAIRIE',
                'INSEE',
                'ILE',
                'ELECTIS',
                'null',
            ),
            1 => array(
                __('Mairie'),
                __('Insee'),
                __('Inscription en ligne'),
                __('Listes consulaires'),
                __('Sans provenance identifiée'),
            ),
        ));
        $form->setLib("par_bureau", __("Activer la rupture par bureau"));
        $form->setType("par_bureau", "checkbox");
        $form->setTaille("par_bureau", 1);
        $form->setMax("par_bureau", 1);
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        echo "<div class=\"formControls\">\n";
        echo "<p class=\"likeabutton\">\n";
        echo "<a href=\"javascript:entre2dates()\">";
        echo __("Telecharger l'edition des mouvements entre deux dates");
        echo "</a>\n";
        echo "</p>\n";
        echo "</div>\n";
        //
        echo "</form>\n";
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }

    /**
     * Permet de rédiger le message de convocation
     * des membres aux commissions.
     */
    function view__convocation() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_commission");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        //
        $description = __("Le message redige sera le contenu du courrier de convocation a".
                        " la commission envoye aux membres.");
        $this->f->displayDescription($description);

        /**
         * Rédaction du message de convocation à la commission
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Edition du message de convocation a la commission");
        $this->f->displaySubTitle($subtitle);
        // Vérification du nombre de membre participant àla commission
        $sql_nb_membre = sprintf(
            'SELECT count(*) FROM %1$smembre_commission WHERE membre_commission.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $nb_membre = $this->f->db->getone($sql_nb_membre);
        $this->f->addToLog(
            __METHOD__."(): db->getone(\"".$sql_nb_membre."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($nb_membre);
        if($nb_membre == 0) {
            $this->f->displayMessage("error", __("Veuillez ajouter les membres participant a la commission (onglet \"Membres\")."));
        } else {
            //
            echo "<form name=\"courriercommission\"
                        id=\"courriercommission\"
                        action=\"../app/index.php?module=form&obj=membre_commission&idx=0&action=201\"
                        method=\"post\"
                        onsubmit=\"return checkformconvocationcommission()\"
                        target=\"_blank\">";
            //
            $champs = array("message");
            //
            $form = $this->f->get_inst__om_formulaire(array(
                "validation" => 0,
                "maj" => 0,
                "champs" => $champs,
            ));
            //
            $form->setLib("message", __("Message"));
            $form->setType("message", "textarea");
            $form->setVal("message", __("le ... a ..."));
            $form->setTaille("message", 80);
            $form->setMax("message", 5);
            //
            $form->entete();
            $form->afficher($champs, 0, false, false);
            $form->enpied();
            //
            echo "<div class=\"formControls\">\n";

            echo "<input type=\"submit\" value=\"".__("Telecharger l'edition des courriers de convocation")."\" ";

            echo "</div>\n";
            //
            echo "</form>\n";
        }
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }
}
