<?php
/**
 * XXX
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/reu_sync_l_e_oel.class.php";

/**
 * XXX
 */
class reu_sync_l_e_reu_non_rattaches extends reu_sync_l_e_oel {

    protected $_absolute_class_name = "reu_sync_l_e_reu_non_rattaches";

    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 503 - Bureau
        //
        $this->class_actions[503] = array(
            "identifier" => "affect-bureau",
            "permission_suffix" => "affecter_bureau",
            "view" => "view_affecter_bureau",
            "condition" => array(
                "is_unbind_to_reu",
                "has_treatment_valid_not_done",
            ),
        );
    }


    function getDataSubmit() {
        //
        $datasubmit = parent::getDataSubmit();
        if ($this->getParameter('maj') == 503) {
            $datasubmit .= "&numero_d_electeur=".$this->f->get_submitted_get_value('numero_d_electeur');
        }
        //
        return $datasubmit;
    }

    function view_affecter_bureau() {
        $this->checkAccessibility();
        //
        $numero_d_electeur = preg_replace('~\D~', '', $this->f->get_submitted_get_value('numero_d_electeur'));
        $liste = preg_replace('~\d~', '', $this->f->get_submitted_get_value('numero_d_electeur'));
        //
        $error = false;
        //
        $submit = $this->f->get_submitted_post_value('submit');
        //
        if ($this->f->get_submitted_post_value('submit') !== null
            && ($this->f->get_submitted_post_value('bureau') !== null
                && $this->f->get_submitted_post_value('bureau') !== '')) {
            //
            $cle = sprintf(
                ' code_type_liste=\'%s\' AND numero_d_electeur=\'%s\' ',
                $liste,
                $numero_d_electeur
            );
            //
            $valF = array();
            $valF['code_bureau_de_vote'] = $this->f->get_submitted_post_value('bureau');
            $res = $this->f->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, "reu_sync_listes"),
                $valF,
                DB_AUTOQUERY_UPDATE,
                $cle
            );
            $this->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "reu_sync_listes")."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res, true) !== false) {
                $error = true;
                $this->f->displayMessage(
                    "error",
                    __("Le bureau n'a pas pu être mis à jour.")
                );
            };
            if ($error === false) {
                //
                $this->msg = __("Le bureau a été correctement mis à jour.");
                $this->redirect_to_back_link('formulaire');
                return;
            }
        }

        $this->f->layout->display__form_container__begin(array(
            "action" => $this->getDataSubmit(),
            "name" => "f1",
            "id" => $this->get_absolute_class_name()."_affecter_bureau",
        ));

        //Compatibilite anterieure - On decremente la variable validation
        $this->setParameter("validation", $this->getParameter("validation") - 1);
        $maj = $this->getParameter("maj");
        $champs = array('bureau');

        // Instanciation de l'objet formulaire
        $form = $this->f->get_inst__om_formulaire(array(
            "validation" => $this->getParameter("validation"),
            "maj" => $maj,
            "champs" => $champs,
        ));

        $form->setParameter("obj", $this->get_absolute_class_name());
        $form->setParameter("idx", $this->getParameter("idx"));
        $form->setParameter("maj", $this->getParameter("maj"));
        $form->setParameter("form_type", "form");
        
        $form->setType('bureau', "select");
        $form->setLib('bureau', __("Bureau"));
        $this->init_select(
            $form,
            $this->f->db,
            1,
            null,
            "bureau",
            sprintf(
                'SELECT bureau.code, concat(bureau.code, \' \', bureau.libelle) FROM %1$sbureau WHERE om_collectivite=%2$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            ),
            $this->get_var_sql_forminc__sql("bureau_by_id"),
            false
        );
        // Affichage du bouton retour et du bouton
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "top",
        ));
        $this->bouton($maj);
        $this->retour(
            $this->getParameter("premier"),
            $this->getParameter("recherche"),
            $this->getParameter("tricol")
        );
        $this->f->layout->display__form_controls_container__end();
        // Ouverture du conteneur de formulaire
        $form->entete();
        $this->display_fiche_electeur_to_compare();
        $get_infos_electeur_reu = $this->get_infos_electeur_reu($numero_d_electeur, $liste);
        printf(
            '<p>Bureau : %s</p>
            <p>Adresse : %s - %s - %s - %s - %s - %s - %s - %s</p>',
            $get_infos_electeur_reu["code_bureau_de_vote"],
            $get_infos_electeur_reu["numero_de_voie"],
            $get_infos_electeur_reu["type_et_libelle_de_voie"],
            $get_infos_electeur_reu["premier_complement_adresse"],
            $get_infos_electeur_reu["second_complement_adresse"],
            $get_infos_electeur_reu["lieu_dit"],
            $get_infos_electeur_reu["code_postal"],
            $get_infos_electeur_reu["libelle_de_commune"],
            $get_infos_electeur_reu["libelle_du_pays"]
        );

        $form->afficher($champs, $this->getParameter("validation"), false, false);
        $form->enpied();
        // Affichage du bouton et du bouton retour
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->bouton($maj);
        $this->retour(
            $this->getParameter("premier"),
            $this->getParameter("recherche"),
            $this->getParameter("tricol")
        );
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }
}
