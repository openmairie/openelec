<?php
//$Id$ 
//gen openMairie le 05/09/2019 15:50

require_once PATH_OPENMAIRIE."obj/om_widget.class.php";

class om_widget extends om_widget_core {

    /**
     *
     */
    var $template_panel = '
<div class="panel panel-box %s">
    <div class="list-justified-container">
        <ul class="list-justified text-center">
            %s
        </ul>
    </div>
</div>';

    /**
     *
     */
    var $template_panel_elem_icon = '
            <li class="%s">
                <span>%s</span>
            </li>';

    /**
     *
     */
    var $template_panel_elem_box = '
            <li>
                <span class="size-h3 box-icon %s">%s</span>
                <p class="text-muted">%s</p> 
            </li>';

    /**
     *
     */
    var $template_panel_elem_list = '
            <li class="%s">
                %s
            </li>';

    /**
     *
     */
    var $template_tasks_category_title = '
        <h4 class="tasks-category-title tasks-title-%s"><span>%s</span></h4>';

    /**
     * Nombre de taches en cours en fonction de la collectivité
     *
     * @return string
     */
    function get_config_taches_en_cours($type = "mouvements") {
        if ($type === "procurations") {
            return array(
                "taches" => array(
                    "mes_procurations_actives" => array(
                        "id" => "mes_procurations_actives",
                        "widget" => false,
                        "title" => "actives",
                        "category" => "procuration",
                        "bloc" => "Procurations",
                        "help" => __("..."),
                        "table" => "procuration",
                        "advs_obj" => "procuration",
                        "criteria" => array(
                            array(
                                "advs_field" => "statut",
                                "sqlquery" => "procuration.statut",
                                "value" => "procuration_confirmee",
                            ),
                            array(
                                "advs_field" => "fin_validite_min",
                                "value" => date("d/m/Y"),
                            ),
                        ),
                    ),
                    "mes_procurations_a_transmettre" => array(
                        "id" => "mes_procurations_a_transmettre",
                        "widget" => true,
                        "title" => "à transmettre",
                        "badge" => "warning",
                        "category" => "procuration",
                        "bloc" => "Procurations",
                        "help" => __("..."),
                        "table" => "procuration",
                        "advs_obj" => "procuration",
                        "criteria" => array(
                            array(
                                "advs_field" => "statut",
                                "sqlquery" => "procuration.statut",
                                "value" => "demande_a_transmettre",
                            ),
                        ),
                    ),
                    "mes_procurations_validees" => array(
                        "id" => "mes_procurations_validees",
                        "widget" => true,
                        "title" => "validées",
                        "category" => "procuration",
                        "bloc" => "Procurations",
                        "help" => __("..."),
                        "table" => "procuration",
                        "advs_obj" => "procuration",
                        "criteria" => array(
                            array(
                                "advs_field" => "statut",
                                "sqlquery" => "procuration.statut",
                                "value" => "procuration_confirmee",
                            ),
                            array(
                                "advs_field" => "vu",
                                "sqlquery" => "procuration.vu",
                                "value" => "f",
                                "type" => "boolean",
                            ),
                        ),
                    ),
                    "mes_procurations_annulees" => array(
                        "id" => "mes_procurations_annulees",
                        "widget" => true,
                        "title" => "annulées",
                        "category" => "procuration",
                        "bloc" => "Procurations",
                        "help" => __("..."),
                        "table" => "procuration",
                        "advs_obj" => "procuration",
                        "criteria" => array(
                            array(
                                "advs_field" => "statut",
                                "sqlquery" => "procuration.statut",
                                "value" => "procuration_annulee",
                            ),
                            array(
                                "advs_field" => "vu",
                                "sqlquery" => "procuration.vu",
                                "value" => "f",
                                "type" => "boolean",
                            ),
                        ),
                    ),
                ),
                "prefix_query_taches_en_cours" => sprintf(
                    'SELECT count(*) FROM %1$sprocuration',
                    DB_PREFIXE
                ),
            );
        }
        return array(
            "taches" => array(
                "mes_inscriptions_office_a_valider" => array(
                    "id" => "mes_inscriptions_office_a_valider",
                    "widget" => true,
                    "title" => "d'office à valider",
                    "badge" => "warning",
                    "category" => "inscription",
                    "bloc" => "Inscriptions",
                    "help" => __("Inscriptions d'office déjà validées par l'INSEE et déjà appliquées sur les listes électorales REU mais qui sont en attente d'un bureau de vote et de prise en compte par la commune pour être appliquées sur les listes électorales openElec"),
                    "table" => "mouvement",
                    "advs_obj" => "inscription",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "inscription",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "vise_insee",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "actif",
                        ),
                    ),
                ),
                "mes_inscriptions_a_instruire" => array(
                    "id" => "mes_inscriptions_a_instruire",
                    "widget" => true,
                    "title" => "à instruire",
                    "category" => "inscription",
                    "badge" => "warning",
                    "bloc" => "Inscriptions",
                    "help" => __("Inscriptions qui sont dans l'état ouvert et qui sont en attente d'analyse des pièces transmises pour juger la complétude"),
                    "table" => "mouvement",
                    "advs_obj" => "inscription",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "inscription",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "ouvert",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "actif",
                        ),
                    ),
                ),
                "mes_inscriptions_en_attente" => array(
                    "id" => "mes_inscriptions_en_attente",
                    "widget" => true,
                    "title" => "en attente",
                    "category" => "inscription",
                    "bloc" => "Inscriptions",
                    "help" => __("Inscriptions qui sont dans l'état en attente"),
                    "table" => "mouvement",
                    "advs_obj" => "inscription",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "inscription",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "en_attente",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "actif",
                        ),
                    ),
                ),
                "mes_inscriptions_a_viser" => array(
                    "id" => "mes_inscriptions_a_viser",
                    "widget" => true,
                    "title" => "à viser",
                    "badge" => "warning",
                    "category" => "inscription",
                    "bloc" => "Inscriptions",
                    "help" => __("Inscriptions qui sont dans l'état complet et qui sont en attente d'un visa du maire"),
                    "table" => "mouvement",
                    "advs_obj" => "inscription",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "inscription",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "complet",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "actif",
                        ),
                    ),
                ),
                "mes_inscriptions_en_attente_insee" => array(
                    "id" => "mes_inscriptions_en_attente_insee",
                    "widget" => true,
                    "title" => "en attente de validation INSEE",
                    "category" => "inscription",
                    "bloc" => "Inscriptions",
                    "help" => __("Inscriptions qui sont dans l'état visée maire et qui sont en attente de validation INSEE."),
                    "table" => "mouvement",
                    "advs_obj" => "inscription",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "inscription",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "vise_maire",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "actif",
                        ),
                    ),
                ),
                "mes_inscriptions_acceptees_insee" => array(
                    "id" => "mes_inscriptions_acceptees_insee",
                    "widget" => true,
                    "title" => "acceptées par l'INSEE",
                    "category" => "inscription",
                    "bloc" => "Inscriptions",
                    "help" => __("Inscriptions acceptées par l'INSEE et qui ne sont pas encore marquées comme lues."),
                    "table" => "mouvement",
                    "advs_obj" => "inscription",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "inscription",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "accepte",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "trs",
                        ),
                        array(
                            "advs_field" => "vu",
                            "sqlquery" => "mouvement.vu",
                            "value" => "f",
                            "type" => "boolean",
                        ),
                    ),
                ),
                "mes_inscriptions_refusees_insee" => array(
                    "id" => "mes_inscriptions_refusees_insee",
                    "widget" => true,
                    "title" => "refusées par l'INSEE",
                    "badge" => "danger",
                    "category" => "inscription",
                    "bloc" => "Inscriptions",
                    "help" => __("Inscriptions refusées par l'INSEE et qui ne sont pas encore marquées comme lues."),
                    "table" => "mouvement",
                    "advs_obj" => "inscription",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "inscription",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "refuse",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "na",
                        ),
                        array(
                            "advs_field" => "vu",
                            "sqlquery" => "mouvement.vu",
                            "value" => "f",
                            "type" => "boolean",
                        ),
                    ),
                ),
                "mes_radiations_office_a_valider" => array(
                    "id" => "mes_radiations_office_a_valider",
                    "widget" => true,
                    "title" => "d'office à valider",
                    "category" => "radiation",
                    "bloc" => "Radiations",
                    "help" => __("Radiations d'office déjà validées par l'INSEE et déjà appliquées sur les listes électorales REU et openElec mais qui sont en attente de prise en compte par la commune"),
                    "table" => "mouvement",
                    "advs_obj" => "radiation",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "radiation",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "vise_insee",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "trs",
                        ),
                    ),
                ),
                "mes_radiations_a_viser" => array(
                    "id" => "mes_radiations_a_viser",
                    "widget" => true,
                    "title" => "à viser",
                    "badge" => "warning",
                    "category" => "radiation",
                    "bloc" => "Radiations",
                    "help" => __("Radiations qui sont dans l'état ouvert et qui sont en attente d'un visa du maire"),
                    "table" => "mouvement",
                    "advs_obj" => "radiation",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "radiation",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "ouvert",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "actif",
                        ),
                    ),
                ),
                "mes_radiation_en_attente_insee" => array(
                    "id" => "mes_radiation_en_attente_insee",
                    "widget" => true,
                    "title" => "en attente de validation INSEE",
                    "category" => "radiation",
                    "bloc" => "Radiations",
                    "help" => __("Radiations qui sont dans l'état visée maire et qui sont en attente de validation INSEE."),
                    "table" => "mouvement",
                    "advs_obj" => "radiation",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "radiation",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "vise_maire",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "actif",
                        ),
                    ),
                ),
                "mes_radiations_acceptees_insee" => array(
                    "id" => "mes_radiations_acceptees_insee",
                    "widget" => true,
                    "title" => "acceptées par l'INSEE",
                    "category" => "radiation",
                    "bloc" => "Radiations",
                    "help" => __("Radiations acceptées par l'INSEE et qui ne sont pas encore marquées comme lues."),
                    "table" => "mouvement",
                    "advs_obj" => "radiation",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "radiation",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "accepte",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "trs",
                        ),
                        array(
                            "advs_field" => "vu",
                            "sqlquery" => "mouvement.vu",
                            "value" => "f",
                            "type" => "boolean",
                        ),
                    ),
                ),
                "mes_radiations_refusees_insee" => array(
                    "id" => "mes_radiations_refusees_insee",
                    "widget" => true,
                    "title" => "refusées par l'INSEE",
                    "badge" => "danger",
                    "category" => "radiation",
                    "bloc" => "Radiations",
                    "help" => __("Radiations refusées par l'INSEE et qui ne sont pas encore marquées comme lues."),
                    "table" => "mouvement",
                    "advs_obj" => "radiation",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "radiation",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "refuse",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "na",
                        ),
                        array(
                            "advs_field" => "vu",
                            "sqlquery" => "mouvement.vu",
                            "value" => "f",
                            "type" => "boolean",
                        ),
                    ),
                ),
                "mes_modifications_a_valider" => array(
                    "id" => "mes_modifications_a_valider",
                    "widget" => true,
                    "title" => "à valider",
                    "badge" => "warning",
                    "category" => "modification",
                    "bloc" => "Modifications",
                    "help" => __("Modifications qui sont dans l'état ouvert et qui sont en attente d'une validation mairie pour transmission au REU"),
                    "table" => "mouvement",
                    "advs_obj" => "modification",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "modification",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "ouvert",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "actif",
                        ),
                    ),
                ),
                "mes_modifications_d_etat_civil" => array(
                    "id" => "mes_modifications_d_etat_civil",
                    "widget" => true,
                    "title" => "d'état civil",
                    "category" => "modification",
                    "bloc" => "Modifications",
                    "help" => __("Modifications d'office déjà validées par l'INSEE et déjà appliquées sur les listes électorales REU et openElec mais qui sont en attente de prise en compte par la commune"),
                    "table" => "mouvement",
                    "advs_obj" => "modification",
                    "criteria" => array(
                        array(
                            "advs_field" => false,
                            "sqlquery" => "lower(param_mouvement.typecat)",
                            "value" => "modification",
                        ),
                        array(
                            "advs_field" => "statut",
                            "sqlquery" => "mouvement.statut",
                            "value" => "vise_insee",
                        ),
                        array(
                            "advs_field" => "etat",
                            "sqlquery" => "mouvement.etat",
                            "value" => "trs",
                        ),
                    ),
                ),
            ),
            "prefix_query_taches_en_cours" => sprintf(
                'SELECT count(*) FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code',
                DB_PREFIXE
            ),
        );
    }

    /**
     * VIEW - widget_election.
     *
     * Élections.
     *
     * L'objet de ce widget est de présenter pour les collectivités MULTI des
     * métriques sur les élections : arrêt des listes, j-5 et listes
     * d'émargement, ...
     * 
     * @return boolean
     */
    public function view_widget_election($content = null) {
        if ($this->f->getParameter("is_collectivity_multi") !== true) {
            return true;
        }

        $ret_elections = $this->f->get_all_results_from_db_query(sprintf(
            'SELECT 
                referentiel_id,
                libelle,
                date_debut,
                date_tour1,
                date_tour2,
                count(*)
            FROM
                %1$sreu_scrutin
            WHERE
                date_debut < CURRENT_DATE
                AND date_fin > CURRENT_DATE
            GROUP BY
                referentiel_id,
                libelle,
                date_debut,
                date_tour1,
                date_tour2
            ',
            DB_PREFIXE
        ));
        if ($ret_elections["code"] !== "OK") {
            return;
        }

        if (count($ret_elections["result"]) == 0) {
            echo "Aucune élection en cours.";
            return;
        }

        printf ('<div id="elections-en-cours">');
        foreach ($ret_elections["result"] as $key => $value) {
            printf(
                '<table class="table table-sm table-condensed table-bordered table-striped table-hover tasks-bloc-elections">
                    <tr>
                        <th colspan="5"><h4>%s <span class="badge badge-info">%s</span></h4></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>[OK]</th>
                        <th>[PENDING]</th>
                        <th>[WARNING]</th>
                        <th>[CRITICAL]</th>
                    </tr>
                ',
                $this->f->formatDate($value["date_tour1"])." & ".$this->f->formatDate($value["date_tour2"])." - ".$value["libelle"],
                $value["count"]
            );
            $ret = $this->f->get_all_results_from_db_query(sprintf(
                'SELECT
                    om_collectivite.om_collectivite as om_collectivite_id,
                    om_collectivite.libelle as om_collectivite_libelle,
                    CASE
                        WHEN arrets_liste.livrable IS NOT NULL
                        THEN
                            CONCAT(\'[OK] \', arrets_liste.livrable_date)
                        WHEN arrets_liste.livrable_demande_id IS NULL
                        THEN 
                            CASE
                                WHEN CURRENT_DATE <= (date_tour1 - integer \'24\')
                                THEN
                                    \'\'
                                WHEN CURRENT_DATE > (date_tour1 - integer \'7\')
                                THEN
                                    \'[CRITICAL] Arrêt des listes non demandé\'
                                ELSE
                                    \'[WARNING] Arrêt des listes non demandé\'
                            END
                        WHEN arrets_liste.livrable_demande_id IS NOT NULL
                        THEN
                            \'[PENDING] EN ATTENTE DE L INSEE Arrêt des listes demandé\'
                        ELSE
                            \'[CRITICAL]\'
                    END
                    AS j20,
                    CASE
                        WHEN j5_livrable IS NOT NULL
                        THEN
                            CONCAT(\'[OK] \', j5_livrable_date)
                        WHEN j5_livrable_demande_id IS NULL
                        THEN
                            CASE
                                WHEN CURRENT_DATE <= (date_tour1 - integer \'7\')
                                THEN
                                    \'\'
                                WHEN CURRENT_DATE > (date_tour1 - integer \'1\')
                                THEN
                                    \'[CRITICAL] J-5 non demandé \'
                                ELSE
                                    \'[WARNING] J-5 non demandé\'
                            END
                        WHEN arrets_liste.livrable_demande_id IS NOT NULL
                        THEN
                            \'[PENDING] EN ATTENTE DE L INSEE J-5 demandé\'
                        ELSE
                            \'[CRITICAL]\'
                    END
                    AS j5,
                    CASE
                        WHEN emarge_livrable IS NOT NULL
                        THEN
                            CONCAT(\'[OK] \', emarge_livrable_date)
                        WHEN emarge_livrable_demande_id IS NULL
                        THEN
                            CASE
                                WHEN CURRENT_DATE <= (date_tour1 - integer \'6\')
                                THEN
                                    \'\'
                                WHEN CURRENT_DATE > (date_tour1 - integer \'1\')
                                THEN
                                    \'[CRITICAL] EMARGEMENT non demandé \'
                                ELSE
                                    \'[WARNING] EMARGEMENT non demandé\'
                            END
                        WHEN emarge_livrable_demande_id IS NOT NULL
                        THEN
                            \'[PENDING] EN ATTENTE DE L INSEE Emargement demandé emargementnonlivré par l insee\'
                    END
                    AS emarge
                FROM
                    %1$sreu_scrutin
                    INNER JOIN %1$som_collectivite
                        ON reu_scrutin.om_collectivite=om_collectivite.om_collectivite
                    INNER JOIN %1$sarrets_liste
                        ON reu_scrutin.arrets_liste=arrets_liste.arrets_liste
                WHERE
                    referentiel_id=%2$s
                ORDER BY
                    om_collectivite.om_collectivite',
                DB_PREFIXE,
                intval($value["referentiel_id"])
            ));
            if ($ret_elections["code"] !== "OK") {
                return;
            }
            $counters = array(
                "j20" => array("ok" => 0, "warning" => 0, "pending" => 0, "critical" => 0, ),
                "j5" => array("ok" => 0, "warning" => 0, "pending" => 0, "critical" => 0, ),
                "emarge" => array("ok" => 0, "warning" => 0, "pending" => 0, "critical" => 0, ),
            );
            foreach($ret["result"] as $election_status) {
                if (strpos($election_status["j20"], "[OK]") !== false) {
                    $counters["j20"]["ok"]++;
                } elseif (strpos($election_status["j20"], "[PENDING]") !== false) {
                    $counters["j20"]["pending"]++;
                } elseif (strpos($election_status["j20"], "[WARNING]") !== false) {
                    $counters["j20"]["warning"]++;
                } elseif (strpos($election_status["j20"], "[CRITICAL]") !== false) {
                    $counters["j20"]["critical"]++;
                }
                if (strpos($election_status["j5"], "[OK]") !== false) {
                    $counters["j5"]["ok"]++;
                } elseif (strpos($election_status["j5"], "[PENDING]") !== false) {
                    $counters["j5"]["pending"]++;
                } elseif (strpos($election_status["j5"], "[WARNING]") !== false) {
                    $counters["j5"]["warning"]++;
                } elseif (strpos($election_status["j5"], "[CRITICAL]") !== false) {
                    $counters["j5"]["critical"]++;
                }
                if (strpos($election_status["emarge"], "[OK]") !== false) {
                    $counters["emarge"]["ok"]++;
                } elseif (strpos($election_status["emarge"], "[PENDING]") !== false) {
                    $counters["emarge"]["pending"]++;
                } elseif (strpos($election_status["emarge"], "[WARNING]") !== false) {
                    $counters["emarge"]["warning"]++;
                } elseif (strpos($election_status["emarge"], "[CRITICAL]") !== false) {
                    $counters["emarge"]["critical"]++;
                }
            }
            $result_content = "";
            $template_line = '
                <tr>
                    <td>%1$s</td>
                    <td><span class="badge %2$s">%3$s</span></td>
                    <td><span class="badge %4$s">%5$s</span></td>
                    <td><span class="badge %6$s">%7$s</span></td>
                    <td><span class="badge %8$s">%9$s</span></td>
                </tr>
            ';
            $result_content .= sprintf(
                $template_line,
                __("Arrêt des listes"),
                ($counters["j20"]["ok"] == 0 ? "" : "badge-success"),
                $counters["j20"]["ok"],
                ($counters["j20"]["pending"] == 0 ? "" : "badge-secondary"),
                $counters["j20"]["pending"],
                ($counters["j20"]["warning"] == 0 ? "" : "badge-warning"),
                $counters["j20"]["warning"],
                ($counters["j20"]["critical"] == 0 ? "" : "badge-danger"),
                $counters["j20"]["critical"]
            );
            $result_content .= sprintf(
                $template_line,
                __("J-5"),
                ($counters["j5"]["ok"] == 0 ? "" : "badge-success"),
                $counters["j5"]["ok"],
                ($counters["j5"]["pending"] == 0 ? "" : "badge-secondary"),
                $counters["j5"]["pending"],
                ($counters["j5"]["warning"] == 0 ? "" : "badge-warning"),
                $counters["j5"]["warning"],
                ($counters["j5"]["critical"] == 0 ? "" : "badge-danger"),
                $counters["j5"]["critical"]
            );
            $result_content .= sprintf(
                $template_line,
                __("Liste d'émargement"),
                ($counters["emarge"]["ok"] == 0 ? "" : "badge-success"),
                $counters["emarge"]["ok"],
                ($counters["emarge"]["pending"] == 0 ? "" : "badge-secondary"),
                $counters["emarge"]["pending"],
                ($counters["emarge"]["warning"] == 0 ? "" : "badge-warning"),
                $counters["emarge"]["warning"],
                ($counters["emarge"]["critical"] == 0 ? "" : "badge-danger"),
                $counters["emarge"]["critical"]
            );
            printf(
                '%s
                </table>',
                $result_content
            );
            if ($this->f->get_submitted_get_value("widget_more") === "true") {
                printf('<table class="table table-sm table-condensed table-bordered table-striped table-hover">');
                printf(
                    '<tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th></tr>',
                    __("om_collectivite"),
                    __("j20"),
                    __("j5"),
                    __("emarge")
                );
                foreach($ret["result"] as $elem) {
                    printf(
                        '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
                        sprintf(
                            '%s - %s',
                            $elem["om_collectivite_id"],
                            $elem["om_collectivite_libelle"]
                        ),
                        $elem["j20"],
                        $elem["j5"],
                        $elem["emarge"]
                    );
                }
                printf('</table>');
            }
        }

        // Lien
        if ($this->f->get_submitted_get_value("widget_more") !== "true") {
            printf(
                '<a href="%s">Voir +</a>',
                "../app/widget_election.php?widget_more=true"
            );
        }
        echo "</div>";
        return false;
    }

    /**
     * VIEW - widget_procurations.
     *
     * Procurations.
     *
     * L'objet de ce widget est de présenter des métriques et des liens vers
     * les procurations : nombre de procurations actives, à transmettre, ...
     * 
     * @return boolean
     */
    public function view_widget_procurations($content = null) {
        $this->redirect_to_advs(array(
            "type" => "procurations",
        ));
        $nb = $this->f->get_one_result_from_db_query(sprintf(
            'SELECT count(*) FROM %1$sprocuration WHERE statut=\'procuration_confirmee\' AND fin_validite >= CURRENT_DATE %2$s',
            DB_PREFIXE,
            ($this->f->getParameter("is_collectivity_multi") == true ? "" : sprintf('AND %1$s.om_collectivite=%2$s', "procuration", intval($_SESSION["collectivite"])))

        ));
        $actions_add_link = "";
        if ($this->f->getParameter("is_collectivity_mono") === true) {
            $actions_add_link = sprintf(
                '<a href="../app/index.php?module=form&obj=procuration&action=0&retour=form">> Ajouter une procuration</a>'
            );
        }
        printf(
            $this->template_panel,
            "panel-procurations",
            sprintf(
                '%s%s',
                sprintf(
                    $this->template_panel_elem_box,
                    "bg-default",
                    sprintf(
                        '<a href="../app/widget_procurations.php?pool=mes_procurations_actives">%s</a>',
                        intval($nb["result"])
                    ),
                    __("procurations actives")
                ),
                sprintf(
                    $this->template_panel_elem_list,
                    "list-procurations",
                    sprintf(
                        '%s<br/><br/><a href="../app/index.php?module=tab&obj=procuration">> Voir toutes les procurations</a>',
                        $actions_add_link
                    )
                )
            )
        );
        printf('<div id="taches-en-cours">');
        echo $this->get_display_pending_task_lists(array(
            "href" => "../app/widget_procurations.php?pool=%s",
            "type" => "procurations",
            "categories" => array("Procurations", ),
        ));
        printf("</div>");
        return false;
    }

    /**
     * VIEW - widget_taches_en_cours.
     *
     * Tâches en cours.
     *
     * L'objet de ce widget est de présenter des métriques et des liens vers
     * les tâches à réaliser : nombre d'inscriptions à instruire, à viser, ...
     * 
     * @return boolean
     */
    public function view_widget_taches_en_cours($content = null) {
        $this->redirect_to_advs(array(
            "type" => "mouvements",
        ));
        printf('<div id="taches-en-cours">');
        echo $this->get_display_pending_task_lists(array(
            "href" => "../app/widget_taches_en_cours.php?pool=%s",
            "type" => "mouvements",
            "categories" => array("Inscriptions", "Radiations", "Modifications", ),
        ));
        //
        $reu_infos = "";
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu !== null) {
            if ($inst_reu->is_connexion_logicielle_valid() === true) {
                $indicateurs = $inst_reu->get_indicateurs();
                if ($indicateurs == null) {
                    $indicateurs = array();
                }
                $reu_infos = sprintf(
                    'A titre d\'information, sont présentées ici les tâches provenant du REU.

            Nombres d\'inscriptions/radiations à valider : %s
            Nombre de demandes à instruire : %s
             - dont nombre de nouvelles inscriptions en ligne à traiter : %s
             - dont nombre de nouvelles propositions d\'inscription à traiter : %s
             - dont nombre de demandes d\'inscription complètes ou à instruire : %s
            Electeurs sans bureau de vote (toutes listes) : %s',
                    gavoe($indicateurs, array("result", "nbDemandesAValider", ), 0),
                    gavoe($indicateurs, array("result", "nbDemandesAInstruire", ), 0),
                    gavoe($indicateurs, array("result", "nbNouvellesInscriptionsEnLigneATraiter", ), 0),
                    gavoe($indicateurs, array("result", "nbNouvellesPropositionsInscriptionATraiter", ), 0),
                    gavoe($indicateurs, array("result", "nbDemandesInscriptionCompletesAInstruire", ), 0),
                    gavoe($indicateurs, array("result", "nbElecteursSansBureauVote", ), 0)
                );
            } else {
                $inst_reu = null;
            }
        }
        printf(
            '
            %s
            </div>
            ',
            ($inst_reu === null ? "" : sprintf(
                '<span class="info-16"title="%s"></span>',
                $reu_infos
            ))
        );
    }

    /**
     *
     */
    protected function redirect_to_advs($params = array()) {
        //
        $cfg_taches_en_cours = $this->get_config_taches_en_cours($params["type"]);
        //
        $pool = $this->f->get_submitted_get_value("pool");
        if ($pool != ""
            && in_array($pool, array_keys($cfg_taches_en_cours["taches"])) !== true) {
            $this->f->setFlag(null);
            $this->f->display();
            return;
        }
        if ($pool != "") {
            // Création d'une variable de session de recherche avancée telle que créée normalement
            $obj = $cfg_taches_en_cours["taches"][$pool]["advs_obj"];
            $search = array(
                "advanced-search-submit" => "",
            );
            foreach($cfg_taches_en_cours["taches"][$pool]["criteria"] as $criteria) {
                if (array_key_exists("advs_field", $criteria) === true
                    && $criteria["advs_field"] !== false) {
                    //
                    $search[$criteria["advs_field"]] = $criteria["value"];
                }
            }
            $advs_id = str_replace(array('.',','), '', microtime(true));
            $_SESSION["advs_ids"][$advs_id] = serialize($search);
            header('Location: '.OM_ROUTE_TAB.'&obj='.$obj.'&advs_id='.$advs_id);
            die();
        }
    }

    /**
     * 
     */
    protected function get_display_pending_task_lists($params = array()) {
        $cfg_taches_en_cours = $this->get_config_taches_en_cours($params["type"]);
        //
        $blocs = array();
        foreach ($cfg_taches_en_cours["taches"] as $line) {
            if (array_key_exists("bloc", $line) === true
                && in_array($line["bloc"], $params["categories"]) === true) {
                //
                $blocs[$line["bloc"]][] = $line;
            }
        }
        foreach ($blocs as $key => $value) {
            printf(
            '
            <table class="table table-sm table-condensed table-bordered table-striped table-hover tasks-bloc-%s">
            <tr>
            <th colspan="2">%s</th></tr>
            ',
                strtolower($key),
                sprintf(
                    $this->template_tasks_category_title, 
                    strtolower($key),
                    $key
                )
            );

            foreach ($value as $line) {
                if ($line["widget"] !== true) {
                    continue;
                }
                $sqlquery_condition = "";
                foreach($line["criteria"] as $criteria) {
                    if (array_key_exists("type", $criteria) === true
                        && $criteria["type"] === "boolean") {
                        //
                        $sqlquery_condition .= sprintf(
                            '%s IS %s AND ',
                            $criteria["sqlquery"],
                            ($criteria["value"] == 't' ? "TRUE" : "FALSE")
                        );
                        continue;
                    }
                    $sqlquery_condition .= sprintf(
                        '%s=\'%s\' AND ',
                        $criteria["sqlquery"],
                        $criteria["value"]
                    );
                }
                $sqlquery_condition = substr($sqlquery_condition, 0, strlen($sqlquery_condition) - 4);
                $nb = $this->f->get_one_result_from_db_query(sprintf(
                    '%1$s WHERE %3$s %4$s',
                    $cfg_taches_en_cours["prefix_query_taches_en_cours"],
                    DB_PREFIXE,
                    ($this->f->getParameter("is_collectivity_multi") == true ? "" : sprintf('%1$s.om_collectivite=%2$s AND ', $line["table"], intval($_SESSION["collectivite"]))),
                    $sqlquery_condition
                ));
                $badge = "";
                if (array_key_exists("badge", $line) === true
                    && in_array($line["badge"], array("warning", "danger", )) === true
                    && $nb["result"] > 0) {
                    //
                    $badge = $line["badge"];
                }
                printf(
                    '<tr>
                        <td>%1$s</td>
                        <td class="nb_task">
                            <a href="%3$s" title="%4$s">
                                <span class="badge badge-%6$s" id="task-%2$s-nb">%5$s</span>
                            </a>
                        </td>
                    </tr>',
                    $line["title"],
                    $line["id"],
                    sprintf($params["href"], $line["id"]),
                    $line["help"],
                    $nb["result"],
                    $badge
                );
            }
            printf('</table>');

        }
    }
}
