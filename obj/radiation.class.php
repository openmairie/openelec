<?php
/**
 * Ce script définit la classe 'radiation'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/mouvement_electeur.class.php";

/**
 * Définition de la classe 'radiation' (om_dbform).
 *
 * Surcharge de la classe 'mouvement_electeur'.
 */
class radiation extends mouvement_electeur {

    /**
     *
     */
    protected $_absolute_class_name = "radiation";

    /**
     * Type de mouvement correspondant à la valeur de typecat dans la table
     * param_mouvement
     * @var string Type de mouvement
     */
    var $typeCat = "radiation";

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        mouvement_gen::init_class_actions();
        //
        $this->class_actions[0]["condition"] = array(
            "is_collectivity_mono",
            "is_idxelecteur_exists",
        );
        //
        $this->class_actions[1]["condition"] = array(
            "is_collectivity_mono",
            "is_not_treated",
            "is_not_in_status__abandonne",
            "has_not_a_visa",
            "is_not_reu_connected",
        );
        //
        $this->class_actions[2]["condition"] = array(
            "is_collectivity_mono",
            "is_not_treated",
            "is_not_reu_connected",
        );
        //
        $this->class_actions[4] = array(
            "identifier" => "consulter-from-electeur",
            "view" => "view_consulter_from_idxelecteur",
            "permission_suffix" => "consulter",
        );
        $this->class_actions[61] = array(
            "identifier" => "valider",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("valider"),
                "class" => "valider-16",
            ),
            "permission_suffix" => "valider",
            "method" => "valider",
            "condition" => array(
                "is_collectivity_mono",
                "is_reu_connected",
                "is_in_status__vise_insee",
                "has_a_visa__accepte",
            ),
        );
        $this->class_actions[63] = array(
            "identifier" => "marquer_comme_vu",
            "crud" => "update",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("marquer comme vu"),
                "class" => "valider-16",
                "order" => 1,
            ),
            "permission_suffix" => "marquer_comme_vu",
            "method" => "mark_as_seen",
            "condition" => array(
                "is_collectivity_mono",
                "is_not_marked_as_seen",
            ),
        );
        //
        $this->class_actions[501] = array(
            "identifier" => "reu-abandonner",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("Abandonner"),
                "class" => "abandonner-16",
            ),
            "permission_suffix" => "abandonner",
            "method" => "abandonner",
            "condition" => array(
                "is_collectivity_mono",
                "is_reu_connected",
                "has_not_a_visa",
                "is_not_in_status__abandonne",
                "is_not_treated",
            ),
        );
        //
        $this->class_actions[502] = array(
            "identifier" => "reu-viser",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("Viser"),
                "class" => "viser-16",
            ),
            "permission_suffix" => "viser",
            "method" => "viser",
            "button" => __("viser"),
            "condition" => array(
                "is_collectivity_mono",
                "is_reu_connected",
                "has_not_a_visa",
                "is_not_in_status__abandonne",
                "is_not_treated",
            ),
        );
        //
        $this->class_actions[101] = array(
            "identifier" => "add-search-form",
            "view" => "view_add_search_form",
            "permission_suffix" => "ajouter",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[102] = array(
            "identifier" => "add-search-form-results",
            "view" => "view_add_search_form_results",
            "permission_suffix" => "ajouter",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-attestationradiation",
            "view" => "view_edition_pdf__attestationradiation",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Attestation de radiation"),
               "class" => "pdf-16",
               "order" => 52,
            ),
            "permission_suffix" => "edition_pdf__attestationradiation",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[202] = array(
            "identifier" => "edition-pdf-refusradiation",
            "view" => "view_edition_pdf__refusradiation",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Refus de radiation"),
               "class" => "courrierrefus-16",
               "order" => 53,
            ),
            "permission_suffix" => "edition_pdf__refusradiation",
            "condition" => array(
                "is_collectivity_mono",
                "is_option_refus_mouvement_enabled",
            ),
        );
        //
        $this->class_actions[204] = array(
            "identifier" => "edition-pdf-recepisseradiation",
            "view" => "view_edition_pdf__recepisseradiation",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Récépissé"),
               "class" => "pdf-16",
               "order" => 51,
            ),
            "permission_suffix" => "edition_pdf__recepisseradiation",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[251] = array(
            "identifier" => "decoupage",
            "view" => "view_decoupage_json",
            "permission_suffix" => "consulter",
        );
        //
        $this->class_actions[401] = array(
            "identifier" => "consulter-historique",
            "view" => "view_consulter_historique",
            "permission_suffix" => "consulter_historique",
            "condition" => array(
                "exists",
            ),
        );
        $this->class_actions[91] = array(
            "identifier" => "traitements_par_lot",
            "view" => "view_traitements_par_lot",
            "permission_suffix" => "traiter_par_lot",
        );
    }

    /**
     * VIEW - view_edition_pdf__recepisseradiation.
     *
     * @return void
     */
    public function view_edition_pdf__recepisseradiation() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "recepisseradiation"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'recepisseradiation-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__attestationinscription.
     *
     * @return void
     */
    public function view_edition_pdf__attestationradiation() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "attestationradiation"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'attestationinscription-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__refusinscription.
     *
     * @return void
     */
    public function view_edition_pdf__refusradiation() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "refusradiation"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'refusmodification-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     *
     */
    function setValFAjout($val = array()) {
        //
        $this->valF['electeur_id'] = $val['electeur_id'];
        $this->valF['numero_bureau'] = ($val['numero_bureau'] !== '' ? $val['numero_bureau'] : null);
        if (array_key_exists("provenance_demande", $val) === true
            && $val["provenance_demande"] !== "") {
            //
            $this->valF["provenance_demande"] = $val["provenance_demande"];
        } else {
            $this->valF["provenance_demande"] = "MAIRIE";
        }
    }

    /**
     *
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        // Initialisation de l'attribut correct a true
        // $this->correct = true;
        //// Mouvement deja traite
        //// Inutile puisque la selection empeche le chargement des donnees dans
        //// le formulaire lorsque le mouvement est 'trs'
        //if ($this->val[array_search('etat', $this->champs)] == "trs") {
        //    $this->correct = false;
        //    $this->addToMessage(__("Operation impossible. Vous ne pouvez pas ".
        //                          "modifier un mouvement deja traite.")."<br/>");
        //}
        // ???
        $this->valF['date_naissance'] = $this->dateDB($this->valF['date_naissance']);
        $this->valF['date_tableau'] = $this->dateDB($this->valF['date_tableau']);
    }

    /**
     * TREATMENT - apply.
     *
     * Ce traitement permet d'appliquer le mouvement sur la liste électorale.
     *
     * @return boolean
     */
    function apply($val = array()) {
        $this->begin_treatment(__METHOD__);
        // Il y a deux modes :
        // - soit nous sommes sur une instance d'une radiation existante
        //   et on applique donc le traitement sur celle là
        // - soit on reçoit une liste d'identifiants de radiations à traiter
        //   et on applique donc le traitement sur celles là
        $radiations_ids = array();
        if ($this->exists() === true) {
            $radiations_ids = array($this->getVal($this->clePrimaire), );
        } elseif (array_key_exists("radiations_ids", $val) === true
            && is_array($val["radiations_ids"]) === true) {
            $radiations_ids = $val["radiations_ids"];
        }
        $nb_radiations_to_apply = count($radiations_ids);
        if ($nb_radiations_to_apply == 0) {
            $this->correct = false;
            $this->addToLog("Mauvais paramètres. Aucune radiation à appliquer.", DEBUG_MODE);
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // On récupère toutes les informations dont on a besoin sur le ou les
        // mouvements pour pouvoir appliquer le traitement
        $query_select_radiation = sprintf(
            'SELECT
                mouvement.id as mouvement_id,
                mouvement.bureau_de_vote_code as bureau_code,
                mouvement.*
            FROM
                %1$smouvement
                INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
            WHERE
                lower(param_mouvement.typecat)=\'radiation\'
                AND mouvement.etat=\'actif\'
                AND mouvement.id IN (%2$s)',
            DB_PREFIXE,
            implode(',', $radiations_ids)
        );
        $res_select_radiation = $this->f->db->query($query_select_radiation);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query_select_radiation."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_select_radiation, true)) {
            $this->correct = false;
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        if ($nb_radiations_to_apply != $res_select_radiation->numrows()) {
            $this->correct = false;
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        //
        while ($row =& $res_select_radiation->fetchRow(DB_FETCHMODE_ASSOC)) {
            // Gestion des LC2
            if ($row["liste"] == "04") {
                $message = "-> Cas particulier LC2";
                $this->addToMessage($message);
                $listes = array("02", "03", );
            } else {
                $listes = array($row["liste"], );
            }
            foreach ($listes as $liste) {
                // suppression ELECTEUR
                $electeur_id = $this->get_electeur_id_by_ine_and_list(
                    $row['ine'],
                    $liste
                );
                $enr = $this->f->get_inst__om_dbform(array(
                    "obj" => "electeur",
                    "idx" => $electeur_id,
                ));

                $ret = $enr->supprimerTraitement();
                if ($ret !== true) {
                    $this->correct = false;
                    $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
                    return $this->end_treatment(__METHOD__, false);
                }
            }
            // maj MOUVEMENT
            $fields_values = array(
                'etat'    => 'trs',
                'tableau' => 'annuel',
                'date_j5' => ''.$enr->dateSystemeDB().'',
            );
            $cle = "id=".$row['mouvement_id'];
            //
            $res1 = $this->f->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, "mouvement"),
                $fields_values,
                DB_AUTOQUERY_UPDATE,
                $cle
            );
            $this->f->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "mouvement")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res1, true) !== false) {
                $this->correct = false;
                $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
                return $this->end_treatment(__METHOD__, false);
            }
        }
        $res_select_radiation->free();
        if ($this->exists() === true) {
            // Gestion de l'historique
            $val_histo = array(
                'action' => 'apply',
                'message' => __("Mouvement appliqué sur la liste électorale.")
            );
            $histo = $this->handle_historique($val_histo);
            if ($histo !== true) {
                $this->correct = false;
                $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $this->addToMessage(_("La radiation a été appliquée correctement sur l'électeur."));
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - On active le verrou sur l'électeur
     * - On transmet le mouvement au REU
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($this->manageVerrouElecteur($id, $val, true) !== true) {
            return false;
        }
        // Transmission REU
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done"
            && ($val['id_demande'] === '' || $val['id_demande'] === null)
            && (array_key_exists('origin', $val) === false || $val['origin'] !== 'reu_regularisation')) {
            //
            $inst_electeur = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => $val['electeur_id'],
            ));
            if ($inst_electeur->getVal("ine") !== "") {
                $inst_liste = $this->f->get_inst__om_dbform(array(
                    "obj" => "liste",
                    "idx" => $val['liste'],
                ));
                $inst_reu = $this->f->get_inst__reu();
                $ret = $inst_reu->handle_radiation(array(
                    "mode" => "add",
                    "datas" => array(
                        "mouvement_id" => $id,
                        "date_demande" => $val["date_demande"],
                        "motif" => "MAI",
                        "ine" => $inst_electeur->getVal("ine"),
                        "liste" => $inst_liste->getVal("liste_insee"),
                    ),
                ));
                if (isset($ret["code"]) !== true || $ret["code"] != 201) {
                    $this->correct = false;
                    $this->addToMessage("Erreur de transmission au REU.");
                    if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                        $this->addToMessage($ret["result"]["message"]);
                    }
                    if ($ret["code"] == 401) {
                        if ($inst_reu->is_connexion_standard_valid() === false) {
                            $this->addToMessage(sprintf(
                                '%s : %s',
                                __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                                sprintf(
                                    '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                    __("cliquez ici pour vérifier")
                                )
                            ));
                        }
                    }
                    return $this->end_treatment(__METHOD__, false);
                } else {
                    $valF = array(
                        "id_demande" => trim($ret["headers"]["X-App-params"]),
                        "ine" => $inst_electeur->getVal("ine"),
                        "statut" => "ouvert",
                    );
                    $ret = $this->update_autoexecute($valF, $id, false);
                    if ($ret !== true) {
                        return $this->end_treatment(__METHOD__, false);
                    }
                }
            }
        }
        //
        return true;
    }

    /**
     * TRIGGER - triggersupprimerapres.
     *
     * - On désactive le verrou sur l'électeur
     *
     * @return boolean
     */
    function triggersupprimerapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($this->manageVerrouElecteur($id, $val, false) !== true) {
            return false;
        }
        return true;
    }

    /**
     * TREATMENT - viser.
     *
     * @return boolean
     */
    function viser($val = array()) {
        $ret = parent::viser($val);
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        if (array_key_exists("etat", $this->valF) === true && $this->valF["etat"] == "na") {
            //
            $val = array(
                "electeur_id" => $this->getVal("electeur_id"),
            );
            if ($this->manageVerrouElecteur($this->getVal($this->clePrimaire), $val, false) !== true) {
                $this->correct = false;
                $this->addToMessage("Erreur lors du déverrouillage de l'électeur.");
                return $this->end_treatment(__METHOD__, false);
            }
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - refuser.
     *
     * @return boolean
     */
    function refuser($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        $valF = array(
            "statut" => "refuse",
            "vu" => false,
            "etat" => "na",
        );
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => 'refuser',
            'message' => __("Méthode 'refuser' appliquée."),
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $val = array(
            "electeur_id" => $this->getVal("electeur_id"),
        );
        if ($this->manageVerrouElecteur($this->getVal($this->clePrimaire), $val, false) !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors du déverrouillage de l'électeur.");
            return $this->end_treatment(__METHOD__, false);
        }
        //
        return $this->end_treatment(__METHOD__, true);
    }
}
