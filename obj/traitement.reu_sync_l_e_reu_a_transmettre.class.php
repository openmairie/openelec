<?php
/**
 * Ce script définit la classe 'reuSyncLEREUATransmettreTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_l_e.class.php";

/**
 * Définition de la classe 'reuSyncLEREUATransmettreTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLEREUATransmettreTraitement extends reuSyncLETraitement {
    /**
     * @var string
     */
    var $fichier = "reu_sync_l_e_reu_a_transmettre";

    /**
     *
     */
    function displayBeforeContentForm() {
        //
        $electeurs_reu_a_transmettre = $this->get_electeurs_reu_a_transmettre();
        if (!is_array($electeurs_reu_a_transmettre)) {
            $this->f->displayMessage(
                "error",
                __("Une erreur est survenue lors de la récupération des informations. Réessayez plus tard.")
            );
            return;
        }
        //
        printf(
            '
            <div class="container-fluid" id="reu-sync-tab">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="card text-center">
                            <div class="card-header">
                                modification(s) à transmettre
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%s</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="visualClear"></div>
            ',
            count($electeurs_reu_a_transmettre)
        );
    }

    /**
     *
     */
    function treatment() {
        $this->LogToFile("begin ".$this->fichier);
        //
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done") {
            $this->error = true;
            $message = __("Cet écran n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée.");
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }
        if ($inst_reu->is_connexion_standard_valid() !== true) {
            $this->error = true;
            $this->addToMessage(sprintf(
                '%s : %s',
                __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                sprintf(
                    '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                    __("cliquez ici pour vérifier")
                )
            ));
            $this->LogToFile("La connexion standard n'est pas valide.");
            $this->LogToFile("end ".$this->fichier);
            return;
        }

        /**
         *
         */
        $elems_to_treat = $this->get_electeurs_reu_a_transmettre();
        $nb_elems_to_treat = count($elems_to_treat);
        $this->LogToFile(sprintf(
            __("Nombre d'électeurs avec informations à transmettre : %s"),
            $nb_elems_to_treat
        ));
        $nb_elems_valid = 0;
        foreach ($elems_to_treat as $key => $value) {
            $this->startTransaction();
            $inst_electeur = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => intval($value["id"]),
            ));
            if ($inst_electeur->exists() !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC L'électeur %s n'existe pas",
                    intval($value["id"])
                ));
                $this->rollbackTransaction();
                continue;
            }
            $ret = $inst_electeur->push_infos_to_reu();
            if ($ret !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC Transmission des informations de l'électeur %s échouée : %s",
                    intval($value["id"]),
                    $inst_electeur->msg
                ));
                $this->rollbackTransaction();
                continue;
            }
            $valF = array(
                "a_transmettre" => false,
            );
            $ret = $inst_electeur->update_autoexecute($valF, null, false);
            if ($ret !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC Mise à jour du marqueur 'a_transmettre' sur l'électeur %s échouée : %s",
                    intval($value["id"]),
                    $inst_electeur->msg
                ));
                $this->rollbackTransaction();
                continue;
            }
            $nb_elems_valid++;
            $this->commitTransaction();
            $this->LogToFile(sprintf(
                "Electeur %s - INE %s - Informations transmises au REU avec succès",
                intval($value["id"]),
                $inst_electeur->getVal("ine")
            ));
        }
        //
        if ($nb_elems_to_treat != 0) {
            $message = sprintf(
                __('%1$s modification(s) validée(s) sur un total de %2$s.'),
                $nb_elems_valid,
                $nb_elems_to_treat
            );
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        if ($nb_elems_to_treat == 0) {
            $message = __("Aucun électeur avec informations à transmettre au REU.");
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        //
        $this->LogToFile("end ".$this->fichier);
    }
}
