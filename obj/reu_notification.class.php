<?php
/**
 * Ce script définit la classe 'reu_notification'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/reu_notification.class.php";

/**
 * Définition de la classe 'reu_notification' (om_dbform).
 */
class reu_notification extends reu_notification_gen {

    /**
     *
     */
    function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[0] = null;
        $this->class_actions[1] = null;
        $this->class_actions[2] = null;
        // ACTION - 101 - sync_last_notifications
        $this->class_actions[101] = array(
            "identifier" => "sync_last_notifications",
            "method" => "sync_last_notifications",
            "permission_suffix" => "synchroniser",
            "button" => __("synchroniser"),
            "condition" => array(
                "reu_sync_l_e_valid_is_done",
            ),
        );
        // ACTION - 102 - treat_all_notifications
        $this->class_actions[102] = array(
            "identifier" => "treat_all_notifications",
            "method" => "treat_all_notifications",
            "permission_suffix" => "traiter_tout",
            "button" => __("traiter toutes les notifications"),
            "condition" => array(
                "reu_sync_l_e_valid_is_done",
            ),
        );
        // ACTION - 201 - traiter
        $this->class_actions[201] = array(
            "identifier" => "traiter",
            "permission_suffix" => "traiter",
            "view" => "formulaire",
            "method" => "traiter",
            "portlet" => array(
               "type" => "action-direct-with-confirmation",
               "libelle" => __("Traiter"),
               "class" => "traiter-16",
               "order" => 10,
            ),
            "condition" => array(
                "is_not_treated",
                "reu_sync_l_e_valid_is_done",
            ),
        );
    }

    /**
     * CONDITION - reu_sync_l_e_valid_is_done
     *
     * @return boolean
     */
    function reu_sync_l_e_valid_is_done() {
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done") {
            return true;
        }
        return false;
    }

    /**
     * TREATMENT - traiter.
     *
     * @return boolean
     */
    function traiter($val = array()) {
        if ($this->reu_sync_l_e_valid_is_done() !== true) {
            $this->addToMessage(__("Cette fonctionnalité n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée."));
            return false;
        }
        $this->begin_treatment(__METHOD__);
        $this->init_rapport_traitement();
        $ret = false;
        if ($this->is_demande_inscription() === true) {
            $ret = $this->handle_inscription($val);
        } elseif ($this->is_demande_radiation() === true) {
            $ret = $this->handle_radiation($val);
        } elseif ($this->is_demande_modification() === true) {
            $ret = $this->handle_modification($val);
        } elseif ($this->is_demande_livrable() === true) {
            $ret = $this->handle_livrable($val);
        } elseif ($this->is_demande_procuration() === true) {
            $ret = $this->handle_procuration($val);
        } else {
            $this->correct = true;
            $this->addToMessage("Aucun traitement à effectuer.");
            return $this->end_treatment(__METHOD__, true);
        }
        if ($ret !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors de l'application du traitement. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        $valF = array(
            "traitee" => 't',
            "rapport_traitement" => json_encode($this->get_rapport_traitement()),
        );
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors de l'application du traitement. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * CONDITION - is_demande_modification
     *
     * @return boolean
     */
    function is_demande_modification() {
        $type_de_notification = $this->getVal('type_de_notification');
        if ($type_de_notification !== 'MODEC') {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_demande_radiation
     *
     * @return boolean
     */
    function is_demande_radiation() {
        $type_de_notification = $this->getVal('type_de_notification');
        if ($type_de_notification !== 'RAD') {
            return false;
        }
        return true;
    }

    /**
     * Est-ce que l'ine passé en paramètre est présent dans la liste des
     * électeurs openElec ?
     *
     * @param string $ine Identifiant National d'électeur
     *
     * @return boolean
     */
    function ine_electeur_exists($ine = null, $liste = null) {
        if ($ine === null) {
            return false;
        }
        $where_liste = '';
        if ($liste !== null) {
            $template = ' AND liste = \'%1$s\' ';
            $where_liste = sprintf(
                $template,
                $liste
            );
        }
        $sql = sprintf('
            SELECT count(id)
            FROM %1$selecteur
            WHERE ine=\'%3$s\'
                AND om_collectivite=%2$s
                %4$s
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $ine,
            $where_liste
        );
        $count = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($count);
        if ($count != 0) {
            return true;
        }
        return false;
    }

    /**
     *
     */
    function ine_mouvement_inscription_actif_exists($ine = null) {
        if ($ine === null) {
            return false;
        }
        $res = $this->f->get_one_result_from_db_query(
            sprintf(
                'SELECT count(id) FROM %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code WHERE om_collectivite=%2$s AND lower(typecat)=\'inscription\' AND etat=\'actif\' AND ine=\'%3$s\'',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $ine
            ),
            true
        );
        if ($res['result'] != 0) {
            return true;
        }
        return false;
    }

    /**
     *
     */
    function get_id_radiation_actif_oel($listes = array()) {
        if ($this->ine_electeur_exists($this->getVal('id_electeur')) === false) {
            return null;
        }
        $get_electeur_ids_by_ine = $this->get_electeur_ids_by_ine($this->getVal('id_electeur'), $listes);
        $electeur_ids = array();
        foreach ($get_electeur_ids_by_ine as $key => $electeur_id) {
            $electeur_ids[] = $electeur_id['id'];
        }
        $query = sprintf(
            'SELECT m.id FROM %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code WHERE electeur_id IN (%3$s) AND om_collectivite=%2$s AND etat = \'actif\' AND (id_demande IS NULL OR id_demande = \'\') AND date_tableau = \'2019-01-10\' AND lower(typecat)=\'radiation\'',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            implode(', ', $electeur_ids)
        );
        $res = $this->f->get_all_results_from_db_query($query, true);
        if ($res['code'] !== "OK") {
            return null;
        }
        if (count($res['result']) == 0) {
            return null;
        }
        if (count($res['result']) > 1) {
            return false;
        }
        return $res['result'][0]['id'];
    }

    /**
     *
     */
    function get_date_extraction_reu_listes() {
        $dtime = DateTime::createFromFormat('d/m/Y H:i:s', $this->f->getParameter("reu_sync_l_e_init"));
        if ($dtime === false) {
            return false;
        }
        return $dtime->format('Y-m-d H:i:s');
    }

    /**
     *
     */
    function init_rapport_traitement() {
        $this->rapport_traitement = array(
            "date" => date('Y-m-d H:i:s'),
            "user" => $_SESSION['login'],
            "rapport" => array(),
        );
    }
    /**
     *
     */
    function update_rapport_traitement($message = '') {
        $this->rapport_traitement["rapport"][] = $message;
    }
    /**
     *
     */
    function get_rapport_traitement() {
        return $this->rapport_traitement;
    }

    /**
     * Vérifie l'existance de l'électeur dans le REU.
     *
     * @return mixed (array ou false)
     */
    function is_electeur_exists_in_reu($wo_demande = false) {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->addToMessage(__("Problème de configuration de la connexion au REU."));
            return false;
        }
        if ($inst_reu->is_connexion_logicielle_valid() !== true) {
            $this->addToMessage(__("La connexion logicielle au REU n'est pas valide."));
            return false;
        }
        //
        $res = array(
            'exists' => false,
        );
        $electeur = $inst_reu->handle_electeur(array(
            "mode" => "get",
            "ine" => $this->getVal('id_electeur'),
        ));
        // Vérifie s'il s'agit d'un électeur n'ayant pas encore la majorité
        // Les électeurs n'ayant pas encore la majorité possède seulement un
        // rattachement inactif mais sans date de radiation et dont la date
        // d'inscription est dans le futur (+1 jour au jour de naissance)
        $num_list = array();
        if (gavoe($electeur['result'], array('rattachementsActifs', ), null) === null
            && gavoe($electeur['result'], array('rattachementsInactifs', ), null) !== null) {
            //
            foreach (gavoe($electeur['result'], array('rattachementsInactifs', ), null)  as $key => $value) {
                //
                if (gavoe($value, array('dateRadiation', ), null) === null) {
                    $num_list[] = $key;
                }
            }
            if (isset($num_list[2]) === true) {
                // L'électeur ne peut pas avoir plus de deux listes affectées
                return false;
            }
        }
        // S'il n'y a pas de demande rattachée à la notfification
        if ($wo_demande === true) {
            //
            if ($electeur["code"] == "200") {
                if (gavoe($electeur["result"], array("rattachementsActifs", "0", ), null) !== null) {
                    //
                    $res['exists'] = true;
                }
                if (count($num_list) > 0) {
                    $res['exists'] = true;
                }
            }
            return $res;
        }
        $demande = $inst_reu->handle_radiation(array(
            "mode" => 'get',
            "id" => $this->getVal('id_demande'),
        ));
        //
        if ($electeur["code"] == "200" && $demande["code"] == "200") {
            $first_list = strtolower(gavoe($electeur["result"], array("rattachementsActifs", "0", "typeListe", "code", )));
            $second_list = strtolower(gavoe($electeur["result"], array("rattachementsActifs", "1", "typeListe", "code", )));
            $demande_list = strtolower(gavoe($demande["result"], array("typeListe", "code", )));
            if (count($num_list) > 0) {
                $first_list = strtolower(gavoe($electeur["result"], array("rattachementsInactifs", strval($num_list[0]), "typeListe", "code", )));
                $second_list = strtolower(gavoe($electeur["result"], array("rattachementsInactifs", strval((isset($num_list[1]) === true ? $num_list[1] : '999')), "typeListe", "code", )));
            }
            //
            if (($first_list !== '' && $first_list === $demande_list)
                || ($second_list !== '' && $second_list === $demande_list)) {
                //
                $res['exists'] = true;
            } elseif ($demande_list == 'lc2') {
                if (($first_list === 'lcm' && $second_list === 'lce')
                    || ($first_list === 'lce' && $second_list === 'lcm')) {
                    //
                    $res['exists'] = true;
                }
            }
            return $res;
        }
        if (($electeur["code"] == "404" || $electeur["code"] == "400") && $demande["code"] == "200") {
            return $res;
        }
        //
        return false;
    }

    /**
     *
     */
    function add_procuration() {
        // Récupération de la demande
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->addToMessage(__("Problème de configuration de la connexion au REU."));
            return false;
        }
        if ($inst_reu->is_connexion_logicielle_valid() !== true) {
            $this->addToMessage(__("La connexion logicielle au REU n'est pas valide."));
            return false;
        }
        $demande_procuration = $inst_reu->handle_procuration(array(
            "mode" => "get",
            "id" => $this->getVal("id_demande"),
        ));
        if ($demande_procuration["code"] != "200") {
            $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
            return false;
        }
        //
        $inst_procuration = $this->f->get_inst__om_dbform(array(
            "obj" => "procuration",
            "idx" => "]",
        ));
        $val = array(
            'om_collectivite' => intval($_SESSION["collectivite"]),
            "id_externe" => $this->getVal("id_demande"),
            "mandant_ine" => gavoe($demande_procuration, array("result", "electeurMandant", "numeroElecteur", )),
            "mandant_infos" => json_encode(gavoe($demande_procuration, array("result", "electeurMandant", ))),
            "mandataire_infos" => json_encode(gavoe($demande_procuration, array("result", "electeurMandataire", ))),
            "mandant_resume" => sprintf(
                '%s - %s - %s - %s',
                gavoe($demande_procuration, array("result", "electeurMandant", "etatCivil", "nomNaissance", )),
                gavoe($demande_procuration, array("result", "electeurMandant", "etatCivil", "prenoms", )),
                gavoe($demande_procuration, array("result", "electeurMandant", "etatCivil", "dateNaissance", )),
                gavoe($demande_procuration, array("result", "electeurMandant", "rattachementTypeListe", "code", ))
            ),
            "mandataire_ine" => gavoe($demande_procuration, array("result", "electeurMandataire", "numeroElecteur", )),
            "mandataire_resume" => sprintf(
                '%s - %s - %s - %s',
                gavoe($demande_procuration, array("result", "electeurMandataire", "etatCivil", "nomNaissance", )),
                gavoe($demande_procuration, array("result", "electeurMandataire", "etatCivil", "prenoms", )),
                gavoe($demande_procuration, array("result", "electeurMandataire", "etatCivil", "dateNaissance", )),
                gavoe($demande_procuration, array("result", "electeurMandataire", "rattachementTypeListe", "code", ))
            ),
            "debut_validite" => gavoe($demande_procuration, array("result", "dateDebut", )),
            "fin_validite" => gavoe($demande_procuration, array("result", "dateFin", )),
            "date_accord" => gavoe($demande_procuration, array("result", "dateEtablissement", )),
            "autorite_type" => gavoe($demande_procuration, array("result", "autoriteEtablissement", "code", )),
            "autorite_nom_prenom" => gavoe($demande_procuration, array("result", "nomPrenomAutorite", )),
            "statut" => "procuration_confirmee",
        );
        if ($val["autorite_type"] === "CSL") {
            $val["types"] = "HF";
            $val["autorite_consulat"] = gavoe($demande_procuration, array("result", "lieuEtablissementUgle", "code"), "NC");
        } else {
            $val["types"] = "EF";
            $val["autorite_commune"] = gavoe($demande_procuration, array("result", "lieuEtablissementUgle", "code"), "NC");
        }

        // Initialisation des valeurs de la procuration et merge avec le tableau des valeurs
        // déjà récupérées
        $initialized_val = array();
        foreach ($inst_procuration->champs as $champ) {
            $initialized_val[$champ] = '';
        }
        $val = array_merge($initialized_val, $val);
        $add_proc = $inst_procuration->ajouter($val);
        if ($add_proc === false) {
            $this->addToMessage($inst_procuration->msg);
            $this->addToMessage(__("Erreur lors de la création de la procuration."));
            return false;
        }
        return true;
    }


    /**
     *
     */
    function handle_procuration($val = array()) {
        $procuration_already_exists = null;
        $inst_procuration = $this->f->get_inst__om_dbform(array(
            "obj" => "procuration",
        ));
        $id_procuration = $inst_procuration->get_procuration_id_from_reu_demande_id($this->getVal('id_demande'));
        // Si la demande de procuration est rattaché à plusieurs procurations
        if ($id_procuration === false) {
            $this->correct = false;
            $this->addToMessage("Il existe déjà plusieurs procurations avec le même identifiant de demande.");
            return false;
        }
        //
        if ($id_procuration !== null) {
            $procuration_already_exists = true;
            $this->update_rapport_traitement(
                sprintf(
                    __("Procuration %s déjà existante avec la demande déjà rattachée."),
                    $id_procuration
                )
            );
            $this->addToMessage(sprintf(
                __("Procuration %s déjà existante avec la demande déjà rattachée."),
                $id_procuration
            ));
            if ($this->getVal('type_de_notification') == "CREAPROC") {
                //
                $inst_procuration = $this->f->get_inst__om_dbform(array(
                    "obj" => "procuration",
                    "idx" => $id_procuration,
                ));
                $ret = $inst_procuration->update_status(array(
                    "statut" => "procuration_confirmee",
                ));
                if ($ret !== true) {
                    $this->correct = false;
                    $this->addToMessage(__("La procuration n'a pas pu être mise à jour."));
                    return false;
                }
                $this->update_rapport_traitement(
                    sprintf(
                        __("Mise à jour de l'état de la procuration %s."),
                        $id_procuration
                    )
                );
                $this->addToMessage(__("La procuration a été mise à jour."));
                return true;
            } elseif ($this->getVal('type_de_notification') == "ANNUPROC"
                      || $this->getVal('type_de_notification') == "ANNUPROCLIGNE") {
                //
                $inst_procuration = $this->f->get_inst__om_dbform(array(
                    "obj" => "procuration",
                    "idx" => $id_procuration,
                ));
                $valF = array(
                    "statut" => "procuration_annulee",
                );
                if ($inst_procuration->getVal("annulation_date") == "") {
                    // Récupération de la demande
                    $inst_reu = $this->f->get_inst__reu();
                    if ($inst_reu == null) {
                        $this->addToMessage(__("Problème de configuration de la connexion au REU."));
                        return false;
                    }
                    if ($inst_reu->is_connexion_logicielle_valid() !== true) {
                        $this->addToMessage(__("La connexion logicielle au REU n'est pas valide."));
                        return false;
                    }
                    $demande_procuration = $inst_reu->handle_procuration(array(
                        "mode" => "get",
                        "id" => $this->getVal("id_demande"),
                    ));
                    if ($demande_procuration["code"] != "200") {
                        $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                        return false;
                    }
                    if (gavoe($demande_procuration, array("result", "dateEtablissementAnnulationVol", ), "") !== "") {
                        $valF_annul["annulation_date"] = gavoe($demande_procuration, array("result", "dateEtablissementAnnulationVol", ));
                        $valF_annul["annulation_autorite_nom_prenom"] = gavoe($demande_procuration, array("result", "nomPrenomAutoriteAnnulationVol", ));
                        $valF_annul["annulation_autorite_type"] = gavoe($demande_procuration, array("result", "autoriteEtablissementAnnulationVol", "code", ));
                        if ($valF_annul["annulation_autorite_type"] === "CSL") {
                            $valF_annul["annulation_autorite_consulat"] = gavoe($demande_procuration, array("result", "lieuEtablissementUgleAnnulationVol", "code"), "NC");
                        } else {
                            $valF_annul["annulation_autorite_commune"] = gavoe($demande_procuration, array("result", "lieuEtablissementUgleAnnulationVol", "code"), "NC");
                        }
                        $ret = $inst_procuration->update_autoexecute($valF_annul, null, false);
                        if ($ret !== true) {
                            $this->addToMessage(__("La procuration n'a pas pu être mise à jour."));
                            return false;
                        }
                    }
                    $valF["vu"] = "f";
                }
                $ret = $inst_procuration->update_status($valF);
                if ($ret !== true) {
                    $this->correct = false;
                    $this->addToMessage(__("La procuration n'a pas pu être mise à jour."));
                    return false;
                }
                $this->update_rapport_traitement(
                    sprintf(
                        __("Mise à jour de l'état de la procuration %s."),
                        $id_procuration
                    )
                );
                $this->addToMessage(__("La procuration a été mise à jour."));
                return true;
            }
        } else {
            if ($this->getVal('type_de_notification') == "CREAPROC"
                || $this->getVal('type_de_notification') == "CREAPROCLIGNE") {
                // Ajout de la procuration
                $add_procuration = $this->add_procuration();
                if ($add_procuration !== true) {
                    $this->correct = false;
                    $this->addToMessage(__("La procuration n'a pas pu être ajoutée."));
                    return false;
                }
                $id_procuration = $inst_procuration->get_procuration_id_from_reu_demande_id($this->getVal('id_demande'));
                if ($id_procuration !== null) {
                    $this->update_rapport_traitement(
                        sprintf(
                            __("Création de la procuration %s."),
                            $id_procuration
                        )
                    );
                }
                $this->addToMessage("Procuration ajoutée.");
                //
                $inst_procuration = $this->f->get_inst__om_dbform(array(
                    "obj" => "procuration",
                    "idx" => $id_procuration,
                ));
                $ret = $inst_procuration->update_status(array(
                    "statut" => "procuration_confirmee",
                    "vu" => "f",
                ));
                if ($ret !== true) {
                    $this->correct = false;
                    $this->addToMessage(__("La procuration n'a pas pu être mise à jour."));
                    return false;
                }
                $this->update_rapport_traitement(
                    sprintf(
                        __("Mise à jour de l'état de la procuration %s."),
                        $id_procuration
                    )
                );
                $this->addToMessage(__("La procuration a été mise à jour."));
                return true;
            }
        }
        //
        $this->addToMessage("Traitement de la notification non pris en charge.");
        return false;
    }


    /**
     *
     */
    function handle_livrable($val = array()) {
        // On vérifie si la notification porte sur une demande de livrable
        // connue dans un des scrutins
        $res = $this->f->get_all_results_from_db_query(sprintf(
            'SELECT
                id,
                emarge_livrable_demande_id AS livrable_demande_id,
                \'emarge_livrable\' AS type_livrable
            FROM
                %1$sreu_scrutin
            WHERE
                emarge_livrable_demande_id=%3$s
                AND om_collectivite=\'%2$s\'
            UNION
            SELECT
                id,
                j5_livrable_demande_id AS livrable_demande_id,
                \'j5_livrable\' AS type_livrable
            FROM
                %1$sreu_scrutin
            WHERE
                j5_livrable_demande_id=%3$s
                AND om_collectivite=\'%2$s\'
            UNION
            SELECT
                arrets_liste AS id,
                livrable_demande_id,
                \'arret_liste_livrable\' AS type_livrable
            FROM
                %1$sarrets_liste
            WHERE
                livrable_demande_id=%3$s
                AND om_collectivite=\'%2$s\'',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            intval($this->getVal("id_demande"))
        ), true);
        if ($res["code"] === "KO") {
            $this->addToMessage("Erreur de base de données.");
            return false;
        }
        $demande_in_scrutin = true;
        // Si la requête sql ne donne rien
        if (count($res["result"]) !== 1) {
            $demande_in_scrutin = false;
            $reu_scrutin = array();
            $this->addToMessage("La demande n'a pas été faite sur openElec.");
            $this->update_rapport_traitement("La demande n'a pas été faite sur openElec.");
        } else {    
            $reu_scrutin = $res["result"][0];
        }
        
        $inst_reu = $this->f->get_inst__reu();
        // On initialise la variable
        $type_livrable = "";
        if ($demande_in_scrutin === false) {
            // On récupère la demande de livrable
            $demande_livrable = $inst_reu->handle_livrable(array(
                "mode" => 'get',
                "id" => $this->getVal('id_demande'),
            ));

            if($demande_livrable["code"] != 200) {
                $this->addToMessage("Erreur lors de la récupération de la demande de livrable.");
                return false;
            }
            $reu_scrutin["referentiel_id"] = "";
            if (isset($demande_livrable) == true
                && is_array($demande_livrable) === true
                && array_key_exists("result", $demande_livrable) === true
                && is_array($demande_livrable["result"]) === true
                && array_key_exists("idScrutin", $demande_livrable["result"]) === true) {
                //
                $reu_scrutin["referentiel_id"] = $demande_livrable["result"]["idScrutin"];
            }
            $type_livrable = $demande_livrable["result"]["typeLivrable"]["code"];
            if ($type_livrable === "PROC"
                || $type_livrable === "MOUVDEM") {
                //
                    $this->addToMessage("Ce type de livrable n'est pas encore géré.");
                    return false;
            }
            // Si il n'y a pas de scrutin lié à la demande on ne fait rien
            if ($reu_scrutin["referentiel_id"] == "" || $reu_scrutin == null) {
                $this->addToMessage("Pas de scrutin lié à la demande.");
                if ($type_livrable !== "COM3112") {
                    return false;
                }
                // Si la demande concerne un arret des listes et n'a pas été faite sur openElec
                // alors on crée une nouvelle instance d'arrêt des listes.
                $arret_liste = $this->f->get_inst__om_dbform(array(
                    "obj" => "arrets_liste",
                    "idx" => "]",
                ));
                $valF = array_combine($arret_liste->champs, $arret_liste->val);
                $valF["non_openelec"] = true;
                $valF["om_collectivite"] = intval($_SESSION["collectivite"]);
                $ret = $arret_liste->ajouter($valF);
                if ($ret === false) {
                    $this->addToMessage("Echec de l'ajout de l'arrêt des listes.");
                    return false;
                }
                $reu_scrutin["id"] = $arret_liste->valF['arrets_liste'];
            } else {
                $ret = $this->f->get_one_result_from_db_query(sprintf(
                    'SELECT id FROM %1$sreu_scrutin WHERE referentiel_id=%3$s AND om_collectivite=%2$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    intval($reu_scrutin["referentiel_id"])
                ));
                $reu_scrutin["id"] = $ret["result"];
                if ($reu_scrutin["id"] == "" || $reu_scrutin == null) {
                    $this->addToMessage("Pas de scrutin correspondant dans openElec.");
                    return false;
                }
            }
        }

        $field = null;
        // Si il s'agit d'un arrêt des listes on instancie l'arrêt des listes en question,
        // sinon c'est le scrutin associé au livrable qui est récupéré.
        $type_obj = "reu_scrutin";
        if ((array_key_exists("type_livrable", $reu_scrutin)
                && $reu_scrutin["type_livrable"] === "arret_liste_livrable")
            || (! empty($type_livrable)
                && ($type_livrable === "COMJ-20"
                    || $type_livrable === "COM3112"))) {
            $type_obj = "arrets_liste";
        }
        $obj = $this->f->get_inst__om_dbform(array(
            "obj" => $type_obj,
            "idx" => $reu_scrutin["id"],
        ));
        // Récupération de l'identifiant du scrutin pour l'intégrer aux données issues du livrable.
        $id_scrutin = $reu_scrutin["id"];
        // En fonction du resultat de la requête sql
        // Pour chaque typê de livrable indique le champs à remplir
        if ((array_key_exists("type_livrable", $reu_scrutin)
            && array_key_exists("livrable_demande_id", $reu_scrutin)
            && $reu_scrutin["type_livrable"] === "arret_liste_livrable"
            && $reu_scrutin["livrable_demande_id"] == intval($this->getVal("id_demande")))
            || $type_livrable == "COMJ-20"
            || $type_livrable == "COM3112") {
            //
            $field = "livrable";
            // Pour les arrêts des listes c'est la table arrets_liste qui est utilisé pour récupérer
            // les informations et mettre à jour la table.
            // L'id du scrutin étant utilisé pour remplir la table contenant les livrables, si l'arret
            // des listes est associé à un scrutin on récupère son id et on le stocke sinon une valeur
            // null est renvoyée
            $scrutin = $obj->get_related_scrutin();
            $id_scrutin = ! empty($scrutin) ?
                $scrutin->getVal($scrutin->clePrimaire) :
                null;
        } elseif ((array_key_exists("type_livrable", $reu_scrutin)
            && array_key_exists("livrable_demande_id", $reu_scrutin)
            && $reu_scrutin["type_livrable"] === "j5_livrable"
            && $reu_scrutin["livrable_demande_id"] == intval($this->getVal("id_demande")))
            || $type_livrable == "MOUVJ-5") {
            //
            $field = "j5_livrable";
        } elseif ((array_key_exists("type_livrable", $reu_scrutin)
            && array_key_exists("livrable_demande_id", $reu_scrutin)
            && $reu_scrutin["type_livrable"] === "emarge_livrable"
            && $reu_scrutin["livrable_demande_id"] == intval($this->getVal("id_demande")))
            || $type_livrable == "EMARGE") {
            //
            $field = "emarge_livrable";
        }

        // On vérifie si un livrable existe dans le scrutin et si il est plus récent
        if ($obj->getVal($field) != ""
            && (date("Y-m-d H:i:s", strtotime($obj->getVal($field."_date"))) > date("Y-m-d H:i:s", strtotime($this->getVal("date_de_creation")))) ) {
            //
            $this->addToMessage("Un livrable plus récent existe dans le scrutin.");
            $this->addToMessage("Traitement de la notification de livrable abandonné.");
            $this->update_rapport_traitement(_("Un livrable plus récent existe dans le scrutin."));
            $this->update_rapport_traitement(_("Traitement de la notification de livrable abandonné."));
            return true;
        }

        $lien_uri = explode("/livrable/", $this->getVal("lien_uri"));
        $id_livrable = $lien_uri[1];

        // On récupère le livrable
        $ret = $inst_reu->handle_livrable(array(
            "mode" => "fichier",
            "id_livrable" => intval($id_livrable),
        ));
        if ($ret["code"] != 200) {
            $this->addToMessage("Erreur lors du téléchargement du fichier livrable.");
            return false;
        }
        $metadata = array(
            "filename" => str_replace("attachment; filename=", "", $ret["headers"]["Content-Disposition"]),
            "size" => strlen($ret["result"]),
            "mimetype" => "application/zip",
        );
        $uid = $this->f->storage->create($ret["result"], $metadata);
        if ($uid == "OP_FAILURE") {
            $this->addToMessage("Erreur lors du stockage du fichier.");
            return false;
        }

        $valF = array(
            $field => $uid,
            $field."_date" => $this->getVal("date_de_creation"),
        );
        // Si la demande ne provient pas du scrutin on met à jour l'id de demande de livrable sur le scrutin
        if($demande_in_scrutin === false) {
            $valF[$field."_demande_id"] = $this->getVal("id_demande");
        }
        $ret = $obj->update_autoexecute($valF, $obj->getVal($obj->clePrimaire), false);
        if ($ret !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors de la mise à jour du scrutin.");
            return false;
        }
        //
        if ($field === "j5_livrable") {
            $this->addToMessage("Traitement de la notification de livrable terminé.");
            return true;
        }
        $obj->init_record_data($obj->getVal($obj->clePrimaire));
        $insert_reu_livrable = $this->f->insert_reu_livrable($field, $this->getVal('id_demande'), $obj, $id_scrutin);
        if ($insert_reu_livrable !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors de l'insertion des données du livrable.");
            # On supprime le livrable
            $this->f->storage->delete($uid);
            return false;
        }
        $this->addToMessage("Traitement de la notification de livrable terminé.");
        return true;
    }

    /**
     * TREATMENT - handle_modification.
     *
     * @return boolean
     */
    function handle_modification($val = array()) {
        //
        if ($this->getVal('type_de_notification') !== "MODEC") {
            $this->correct = false;
            $this->addToMessage("Traitement de la notification non pris en charge.");
            return false;
        }
        //
        $date_extraction_reu_listes = $this->get_date_extraction_reu_listes();
        if ($date_extraction_reu_listes === false) {
            $this->correct = false;
            $this->addToMessage(__("Il n'y a pas de date de dernière synchronisation de la liste électorale."));
            return false;
        }
        //
        if ($this->getVal('date_de_creation') <= $date_extraction_reu_listes) {
            $this->update_rapport_traitement(__("Notification déjà traitée lors de la synchronisation des listes électorales."));
            $this->addToMessage("Traitement de la notification de modification terminé.");
            return true;
        }
        // Vérifie l'existence de l'INE sur la notification
        if ($this->getVal('id_electeur') === ''
            || $this->getVal('id_electeur') === null) {
            //
            $this->correct = false;
            $this->addToMessage(__("Il n'existe pas d'INE sur la notification."));
            return false;
        }
        // Vérifie l'existance de l'électeur dans le REU
        $is_electeur_exists_in_reu = $this->is_electeur_exists_in_reu(true);
        if (is_array($is_electeur_exists_in_reu) === false
            && $is_electeur_exists_in_reu === false) {
            //
            $this->correct = false;
            $this->addToMessage("Erreur lors de la récupération de l'état de l'électeur dans le REU.");
            return false;
        }
        //
        $electeur_ids = array();
        if ($is_electeur_exists_in_reu['exists'] === true) {
            $electeur_ids = $this->get_electeur_ids_by_ine($this->getVal("id_electeur"));
            if (is_array($electeur_ids) !== true
                || count($electeur_ids) == 0) {
                //
                $this->correct = false;
                $this->addToMessage(__("Cet INE n'est pas présent dans la liste des électeurs."));
                return false;
            }
        } else {
            $this->update_rapport_traitement(
                sprintf(
                    __("Le mouvement de modification n'a pas été appliqué sur l'électeur %s car celui-ci n'existe plus dans le REU."),
                    intval($electeur_id["id"])
                )
            );
        }
        //
        foreach ($electeur_ids as $key => $electeur_id) {
            $inst_electeur = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => intval($electeur_id["id"]),
            ));
            if ($inst_electeur->exists() !== true) {
                $this->correct = false;
                $this->addToMessage(__("L'électeur n'existe pas."));
                return false;
            }
            $ret = $inst_electeur->handle_modec(array(
                "statut" => "vise_insee",
                "observation" => $this->getVal("libelle"),
                "provenance_demande" => 'INSEE',
            ));
            if ($ret !== true) {
                $this->correct = false;
                $this->addToMessage(__("Erreur lors de l'application du MODEC sur l'électeur."));
                $this->addToMessage($inst_electeur->msg);
                return false;
            }
            $this->update_rapport_traitement(
                sprintf(
                    __("Application du modec sur l'électeur %s."),
                    intval($electeur_id["id"])
                )
            );
        }
        //
        $this->update_rapport_traitement(__("Traitement de la notification terminé."));
        //
        $this->addToMessage("Traitement de la notification de modification terminé.");
        return true;
    }

    /**
     * TREATMENT - handle_radiation.
     *
     * @return boolean
     */
    function handle_radiation($val = array()) {
        $radiation_already_exists = false;
        // Traite seulement les notifications post synchronisation des listes
        if ($this->getVal('resultat_de_notification') == "ACCEPTEE"
            || $this->getVal('resultat_de_notification') == "REFUSEE") {
            //
            $date_extraction_reu_listes = $this->get_date_extraction_reu_listes();
            if ($date_extraction_reu_listes === false) {
                $this->correct = false;
                $this->addToMessage(__("Il n'y a pas de date de dernière synchronisation de la liste électorale."));
                return false;
            }
            if ($this->getVal('date_de_creation') <= $date_extraction_reu_listes) {
                $this->update_rapport_traitement(__("Notification déjà traitée lors de la synchronisation des listes électorales."));
                //
                $this->addToMessage("Traitement de la notification de radiation terminé.");
                return true;
            }
        }
        //
        $inst_radiation = $this->f->get_inst__om_dbform(array(
            "obj" => "radiation",
        ));

        // Si la notification n'est pas rattachée à une demande, il n'y a pas de
        // traitement à effectuer
        if ($this->getVal('id_demande') === ''
            || $this->getVal('id_demande') === null) {
            //
            $this->addToMessage(__("Il n'existe pas de demande rattachée à la notification."));
            return true;
        }

        // Vérifie l'existance de l'INE sur la notification
        if ($this->getVal('id_electeur') === ''
            || $this->getVal('id_electeur') === null) {
            //
            $this->correct = false;
            $this->addToMessage(__("Il n'existe pas d'INE sur la notification."));
            return false;
        }

        $id_radiation = $inst_radiation->get_mouvement_id_from_reu_demande_id($this->getVal('id_demande'));
        // Si la demande de radiation est rattaché à plusieurs mouvements
        if ($id_radiation === false) {
            $this->correct = false;
            $this->addToMessage("Il existe déjà plusieurs mouvements avec le même identifiant de demande.");
            return false;
        }
        if ($id_radiation !== null) {
            $radiation_already_exists = true;
            $this->update_rapport_traitement(
                sprintf(
                    __("Mouvement de radiation %s déjà existant avec la demande déjà rattachée."),
                    $id_radiation
                )
            );
        }

        // Si la demande de radiation n'est pas rattachée à un mouvement
        if ($id_radiation === null) {
            // Vérifie s'il existe un mouvement de radiation de l'électeur
            $inst_reu = $this->f->get_inst__reu();
            if ($inst_reu == null) {
                $this->addToMessage(__("Problème de configuration de la connexion au REU."));
                return false;
            }
            if ($inst_reu->is_connexion_logicielle_valid() !== true) {
                $this->addToMessage(__("La connexion logicielle au REU n'est pas valide."));
                return false;
            }
            $demande_rad = $inst_reu->handle_radiation(array(
                "mode" => 'get',
                "id" => $this->getVal('id_demande'),
            ));
            $liste = $this->f->db->getOne(
                sprintf(
                    'SELECT liste FROM %1$sliste WHERE liste_insee=\'%2$s\'',
                    DB_PREFIXE,
                    gavoe($demande_rad["result"], array("typeListe", "code", ))
                )
            );
            $this->f->isDatabaseError($liste);
            $listes = array($liste, );
            if ($liste == "04") {
                $listes = array("02", "03", );
            }
            $id_radiation = $this->get_id_radiation_actif_oel($listes);
            if ($id_radiation === false) {
                $this->correct = false;
                $this->addToMessage("Il existe déjà plusieurs mouvements de radiation sur cet électeur.");
                return false;
            }
            if ($id_radiation !== null) {
                $radiation_already_exists = true;
                $this->update_rapport_traitement(
                    sprintf(
                        __("Mouvement de radiation %s déjà existant avec la demande pas encore rattachée."),
                        $id_radiation
                    )
                );
            }
        }

        // Si la demande de radiation n'est pas rattachée à un mouvement
        if ($id_radiation === null) {
            // Ajout de la radiation
            $add_mouvement_radiation = $this->add_mouvement_radiation();
            if ($add_mouvement_radiation === false) {
                $this->correct = false;
                $this->addToMessage(__("Le mouvement n'a pas pu être ajouté."));
                return false;
            }
            $id_radiation = $inst_radiation->get_mouvement_id_from_reu_demande_id($this->getVal('id_demande'));
            if ($id_radiation !== null) {
                $this->update_rapport_traitement(
                    sprintf(
                        __("Création du mouvement de radiation %s."),
                        $id_radiation
                    )
                );
            }
        }
        //
        $inst_radiation = $this->f->get_inst__om_dbform(array(
            "obj" => "radiation",
            "idx" => $id_radiation,
        ));
        if ($inst_radiation->exists() !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors de l'application du traitement. Contactez votre administrateur.");
            return false;
        }
        // Met à jour le numéro de la demande dans le mouvement de
        // radiation
        $valF = array(
            "id_demande" => $this->getVal('id_demande'),
        );
        $ret = $inst_radiation->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors de la mise à jour de l'identifiant de la demande du mouvement.");
            return false;
        }
        // Gestion de l'historique du mouvement
        $val_histo = array(
            'action' => 'update_autoexecute',
            'message' => sprintf(
                __("Mise à jour du numéro de la demande depuis la notification %s."),
                $this->getVal($this->clePrimaire)
            ),
            'datas' => $valF,
        );
        $histo = $inst_radiation->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
            return false;
        }
        // Demande acceptée
        if ($this->getVal('resultat_de_notification') == "ACCEPTEE") {
            if ($this->get_provenance_demande_reu() === 'INSEE'
                || $this->get_provenance_demande_reu() === 'MAIRIE'
                || $radiation_already_exists === true) {
                // Récupère le résultat de la vérification de l'existance de l'électeur
                // dans la liste électorale du REU
                $is_electeur_exists_in_reu = $this->is_electeur_exists_in_reu();
                if (is_array($is_electeur_exists_in_reu) === false
                    && $is_electeur_exists_in_reu === false) {
                    //
                    $this->correct = false;
                    $this->addToMessage("Erreur lors de la récupération de l'état de l'électeur dans le REU.");
                    return false;
                }
                // Vérifie qu'il n'existe pas un mouvement d'inscription actif sur
                // l'ine et que l'électeur n'existe plus dans la liste électorale du REU
                // Tant que le mouvement d'inscription n'est pas appliqué, la
                // radiation ne doit pas s'appliquer
                if ($this->ine_mouvement_inscription_actif_exists($this->getVal('id_electeur')) === true
                    && $is_electeur_exists_in_reu['exists'] === false) {
                    //
                    $this->correct = false;
                    $this->addToMessage("Il existe un mouvement d'inscription actif sur l'INE, la radiation ne peut donc pas encore s'appliquer.");
                    return false;
                }
                // Vérifie que l'ine existe dans la table electeur sur la/les liste(s) et que l'électeur
                // n'existe plus dans la liste électorale du REU avant d'appliquer le mouvement
                $mv_listes = array($inst_radiation->getVal('liste'), );
                if ($inst_radiation->getVal('liste') == "04") {
                    $mv_listes = array("02", "03", );
                }
                $ine_electeur_exists = true;
                foreach ($mv_listes as $mv_liste) {
                    if ($this->ine_electeur_exists($this->getVal('id_electeur'), $mv_liste) === false) {
                        $ine_electeur_exists = false;
                    }
                }
                if ($ine_electeur_exists === true
                    && $is_electeur_exists_in_reu['exists'] === false) {
                    //
                    $ret = $inst_radiation->apply();
                    $this->addToMessage($inst_radiation->msg);
                    if ($ret !== true) {
                        $this->correct = false;
                        $this->addToMessage("Erreur lors de l'application du traitement du mouvement de radiation. Contactez votre administrateur.");
                        return false;
                    }
                    //
                    $this->update_rapport_traitement(
                        sprintf(
                            __("L'électeur existe toujours dans la liste électorale, le mouvement de radiation %s est donc appliqué."),
                            $id_radiation
                        )
                    );
                } else {
                    //
                    $valF = array(
                        "etat" => 'trs',
                    );
                    $ret = $inst_radiation->update_autoexecute($valF, null, false);
                    if ($ret !== true) {
                        $this->correct = false;
                        $this->addToMessage("Erreur lors du changement de statut du mouvement. Contactez votre administrateur.");
                        return false;
                    }
                    // Gestion de l'historique du mouvement
                    $val_histo = array(
                        'action' => 'update_autoexecute',
                        'message' => sprintf(
                            __("Mise à jour de l'état depuis la notification %s."),
                            $this->getVal($this->clePrimaire)
                        ),
                        'datas' => $valF,
                    );
                    $histo = $inst_radiation->handle_historique($val_histo);
                    if ($histo !== true) {
                        $this->correct = false;
                        $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
                        return false;
                    }
                    if ($ine_electeur_exists === true) {
                        //
                        $ret = $inst_radiation->manageVerrouElecteur(
                            $inst_radiation->getVal($this->clePrimaire),
                            array(
                                "electeur_id" => $inst_radiation->getVal("electeur_id"),
                            ),
                            false
                        );
                        if ($ret !== true) {
                            $this->addToMessage("Erreur lors du dévérouillage de l'électeur.");
                            return false;
                        }
                        //
                        $this->update_rapport_traitement(
                            sprintf(
                                __("L'électeur existe dans le REU, et est toujours dans la liste des électeurs OEL, le mouvement de radiation %s n'est donc pas appliqué."),
                                $id_radiation
                            )
                        );
                    } else {
                        //
                        $this->update_rapport_traitement(
                            sprintf(
                                __("L'électeur n'existe déjà plus dans la liste électorale, le mouvement de radiation %s n'est donc pas appliqué."),
                                $id_radiation
                            )
                        );
                    }
                }
                // Met à jour le statut et le visa
                if ($this->get_provenance_demande_reu() === 'INSEE') {
                    $valF = array(
                        "statut" => 'vise_insee',
                        "visa" => 'accepte',
                    );
                } elseif ($this->get_provenance_demande_reu() === 'MAIRIE'
                          || $radiation_already_exists === true) {
                    //
                    $valF = array(
                        "statut" => 'accepte',
                        "vu" => false,
                    );
                }
                $ret = $inst_radiation->update_autoexecute($valF, null, false);
                if ($ret !== true) {
                    $this->correct = false;
                    $this->addToMessage("Erreur lors du changement de statut du mouvement. Contactez votre administrateur.");
                    return false;
                }
                // Gestion de l'historique du mouvement
                $val_histo = array(
                    'action' => 'update_autoexecute',
                    'message' => sprintf(
                        __("Mise à jour du statut et du visa depuis la notification %s."),
                        $this->getVal($this->clePrimaire)
                    ),
                    'datas' => $valF,
                );
                $histo = $inst_radiation->handle_historique($val_histo);
                if ($histo !== true) {
                    $this->correct = false;
                    $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
                    return false;
                }
                //
                $this->update_rapport_traitement(__("Traitement de la notification terminé."));
                //
                $this->addToMessage("Traitement de la notification de radiation terminé.");
                return true;
            }
        }
        // Demande refusée
        if ($this->getVal('resultat_de_notification') == "REFUSEE") {
            //
            $ret = $inst_radiation->refuser();
            if ($ret !== true) {
                $this->correct = false;
                $this->addToMessage("Erreur lors du changement de statut et de l'état du mouvement pour un refus. Contactez votre administrateur.");
                return false;
            }
            //
            $this->update_rapport_traitement(__("Traitement de la notification terminé."));
            $this->addToMessage("Traitement de la notification de radiation terminé.");
            return true;
        }
        //
        $this->addToMessage("Traitement de la notification non pris en charge.");
        return false;
    }

    /**
     * TREATMENT - handle_inscription.
     *
     * @return boolean
     */
    function handle_inscription($val = array()) {
        $inscription_already_exists = false;
        // Traite seulement les notifications post synchronisation des listes
        if ($this->getVal('resultat_de_notification') == "ACCEPTEE"
            || $this->getVal('resultat_de_notification') == "REFUSEE") {
            //
            $date_extraction_reu_listes = $this->get_date_extraction_reu_listes();
            if ($date_extraction_reu_listes === false) {
                $this->correct = false;
                $this->addToMessage(__("Il n'y a pas de date de dernière synchronisation de la liste électorale."));
                return false;
            }
            if ($this->getVal('date_de_creation') <= $date_extraction_reu_listes) {
                $this->update_rapport_traitement(__("Notification déjà traitée lors de la synchronisation des listes électorales."));
                //
                $this->addToMessage("Traitement de la notification d'inscription terminé.");
                return true;
            }
        }
        //
        $inst_inscription = $this->f->get_inst__om_dbform(array(
            "obj" => "inscription",
        ));

        // Si la notification n'est pas rattachée à une demande, il n'y a pas de
        // traitement à effectuer
        if ($this->getVal('id_demande') === ''
            || $this->getVal('id_demande') === null) {
            //
            $this->addToMessage(__("Il n'existe pas de demande rattachée à la notification."));
            return true;
        }
        $id_inscription = $inst_inscription->get_mouvement_id_from_reu_demande_id($this->getVal('id_demande'));

        // Si la demande d'inscription est rattaché à plusieurs mouvements
        if ($id_inscription === false) {
            $this->correct = false;
            $this->addToMessage("Il existe déjà plusieurs mouvements avec le même identifiant de demande.");
            return false;
        }
        if ($id_inscription !== null) {
            $inscription_already_exists = true;
            $this->update_rapport_traitement(
                sprintf(
                    __("Mouvement d'inscription %s déjà existant avec la demande déjà rattachée."),
                    $id_inscription
                )
            );
        }

        // Si la demande d'inscription n'est pas rattachée à un mouvement
        if ($id_inscription === null) {
            // Ajout de l'inscription
            $add_mouvement_inscription = $this->add_mouvement_inscription();
            if ($add_mouvement_inscription === false) {
                $this->correct = false;
                $this->addToMessage(__("Le mouvement n'a pas pu être ajouté."));
                return false;
            }
            $id_inscription = $inst_inscription->get_mouvement_id_from_reu_demande_id($this->getVal('id_demande'));
            if ($id_inscription !== null) {
                $this->update_rapport_traitement(
                    sprintf(
                        __("Création du mouvement d'inscription %s."),
                        $id_inscription
                    )
                );
            }
        }
        //
        $inst_inscription = $this->f->get_inst__om_dbform(array(
            "obj" => "inscription",
            "idx" => $id_inscription,
        ));
        if ($inst_inscription->exists() !== true
            && $this->getVal('type_de_notification') !== 'DDE') {
            $this->correct = false;
            $this->addToMessage("Le mouvement d'inscription n'existe pas.");
            return false;
        }
        //
        if ($this->getVal('resultat_de_notification') == "ACCEPTEE"
            && ($this->getVal('type_de_notification') === 'INS'
                ||$this->getVal('type_de_notification') === 'INS_OFF')) {
            // Met à jour le numéro ine dans le mouvement
            $valF = array(
                "ine" => $this->getVal('id_electeur'),
            );
            $ret = $inst_inscription->update_autoexecute($valF, null, false);
            if ($ret !== true) {
                $this->correct = false;
                $this->addToMessage("Erreur lors de la mise à jour du numéro ine du mouvement. Contactez votre administrateur.");
                return false;
            }
            // Gestion de l'historique du mouvement
            $val_histo = array(
                'action' => 'update_autoexecute',
                'message' => sprintf(
                    __("Mise à jour du INE depuis la notification %s."),
                    $this->getVal($this->clePrimaire)
                ),
                'datas' => $valF,
            );
            $histo = $inst_inscription->handle_historique($val_histo);
            if ($histo !== true) {
                $this->correct = false;
                $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
                return false;
            }
            //
            if (($inst_inscription->getVal('statut') === 'vise_maire'
                && $inst_inscription->getVal('visa') === 'accepte')
                || ($inst_inscription->getVal('statut') === 'ouvert'
                    && ($this->get_provenance_demande_reu() === 'MAIRIE'
                        || $this->get_provenance_demande_reu() === 'ILE'
                        || $inscription_already_exists === true))
                && $inst_inscription->getVal('bureau') !== null
                && $inst_inscription->getVal('bureau') !== '') {
                // Vérifie que l'ine existe dans la table electeur sur la/les liste(s)
                // avant d'appliquer le mouvement
                $mv_listes = array($inst_inscription->getVal('liste'), );
                if ($inst_inscription->getVal('liste') == "04") {
                    $mv_listes = array("02", "03", );
                }
                $ine_electeur_exists = true;
                foreach ($mv_listes as $mv_liste) {
                    if ($this->ine_electeur_exists($this->getVal('id_electeur'), $mv_liste) === false) {
                        $ine_electeur_exists = false;
                    }
                }
                //
                $ret = $inst_inscription->apply();
                $this->addToMessage($inst_inscription->msg);
                if ($ret !== true) {
                    $this->correct = false;
                    $this->addToMessage("Erreur lors de l'application du traitement du mouvement d'inscription. Contactez votre administrateur.");
                    return false;
                }
                if ($ine_electeur_exists === false) {
                    //
                    $this->update_rapport_traitement(
                        sprintf(
                            __("L'électeur n'existe pas dans la liste électorale, le mouvement d'inscription %s est donc appliqué."),
                            $id_inscription
                        )
                    );
                } else {
                    //
                    $this->update_rapport_traitement(
                        sprintf(
                            __("L'électeur existe déjà, mais le mouvement d'inscription %s est tout de même appliqué pour mettre à jour les informations de celui-ci."),
                            $id_inscription
                        )
                    );
                }
                //
                $valF = array(
                    "statut" => 'accepte',
                    "vu" => false,
                );
                $ret = $inst_inscription->update_autoexecute($valF, null, false);
                if ($ret !== true) {
                    $this->correct = false;
                    $this->addToMessage("Erreur lors du changement de statut du mouvement. Contactez votre administrateur.");
                    return false;
                }
                // Gestion de l'historique du mouvement
                $val_histo = array(
                    'action' => 'update_autoexecute',
                    'message' => sprintf(
                        __("Mise à jour du statut depuis la notification %s."),
                        $this->getVal($this->clePrimaire)
                    ),
                    'datas' => $valF,
                );
                $histo = $inst_inscription->handle_historique($val_histo);
                if ($histo !== true) {
                    $this->correct = false;
                    $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
                    return false;
                }
                //
                $this->update_rapport_traitement(__("Traitement de la notification terminé."));
                $this->addToMessage("Traitement de la notification d'inscription terminé.");
                return true;
            }
            //
            if ($inst_inscription->getVal('statut') === 'ouvert'
                && (($this->get_provenance_demande_reu() === 'INSEE'
                        || (($this->get_provenance_demande_reu() === 'MAIRIE'
                                || $this->get_provenance_demande_reu() === 'ILE'
                                || $inscription_already_exists === true)
                            && ($inst_inscription->getVal('bureau') === null
                                || $inst_inscription->getVal('bureau') === '')))
                    || $inscription_already_exists === true)) {
                // Met à jour le statut et le visa
                $valF = array(
                    "statut" => 'vise_insee',
                    "visa" => 'accepte',
                );
                $ret = $inst_inscription->update_autoexecute($valF, null, false);
                if ($ret !== true) {
                    $this->correct = false;
                    $this->addToMessage("Erreur lors de la mise à jour du statut et du visa du mouvement. Contactez votre administrateur.");
                    return false;
                }
                // Gestion de l'historique du mouvement
                $val_histo = array(
                    'action' => 'update_autoexecute',
                    'message' => sprintf(
                        __("Mise à jour du statut et du visa depuis la notification %s."),
                        $this->getVal($this->clePrimaire)
                    ),
                    'datas' => $valF,
                );
                $histo = $inst_inscription->handle_historique($val_histo);
                if ($histo !== true) {
                    $this->correct = false;
                    $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
                    return false;
                }
                //
                $this->update_rapport_traitement(__("Traitement de la notification terminé."));
                $this->addToMessage("Traitement de la notification d'inscription terminé.");
                return true;
            }
        }
        //
        if ($this->getVal('type_de_notification') === 'DDE') {
            //
            $this->update_rapport_traitement(__("Traitement de la notification terminé."));
            $this->addToMessage("Traitement de la notification d'inscription terminé.");
            return true;
        }
        // Demande refusée
        if ($this->getVal('resultat_de_notification') == "REFUSEE") {
            //
            $ret = $inst_inscription->refuser();
            if ($ret !== true) {
                $this->correct = false;
                $this->addToMessage("Erreur lors du changement de statut et de l'état du mouvement pour un refus. Contactez votre administrateur.");
                return false;
            }
            //
            $this->update_rapport_traitement(__("Traitement de la notification terminé."));
            $this->addToMessage("Traitement de la notification d'inscription terminé.");
            return true;
        }
        //
        $this->addToMessage("Traitement de la notification non pris en charge.");
        return false;
    }

    /**
     *
     */
    function set_provenance_demande_reu($val) {
        $this->__provenance_demande_reu = $val;
    }

    /**
     *
     */
    function get_provenance_demande_reu() {
        if (isset($this->__provenance_demande_reu) === true) {
            return $this->__provenance_demande_reu;
        }
        return null;
    }


    /**
     *
     */
    function add_mouvement_inscription() {
        // Récupération de la demande
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->addToMessage(__("Problème de configuration de la connexion au REU."));
            return false;
        }
        if ($inst_reu->is_connexion_logicielle_valid() !== true) {
            $this->addToMessage(__("La connexion logicielle au REU n'est pas valide."));
            return false;
        }
        $params = array(
            "mode" => 'get',
            "id" => $this->getVal('id_demande'),
        );
        $demande_ins = $inst_reu->handle_inscription($params);
        //
        if ($demande_ins["code"] != "200") {
            $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
            return false;
        }

        // Les notifications DDE close ne doivent pas créer un mouvement d'inscription
        if ($this->getVal('type_de_notification') === 'DDE'
            && strtolower(gavoe($demande_ins["result"], array("etatCourant", "etat", "code", ))) === 'clos') {
            //
            return true;
        }

        // Récupération de la provenance
        $this->set_provenance_demande_reu(trim(gavoe($demande_ins["result"], array("provenance", "code", ))));

        // Bureau de vote
        $res_bureau = $this->f->db->query(
            sprintf(
                'SELECT id, code FROM %1$sbureau WHERE referentiel_id=%3$s AND om_collectivite=%2$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                intval(gavoe($demande_ins["result"], array("bureauVote", "id", )))
            )
        );
        $this->f->isDatabaseError($res_bureau);
        $row_bureau = $res_bureau->fetchRow(DB_FETCHMODE_ASSOC);
        $bureau = is_array($row_bureau) === true && array_key_exists("id", $row_bureau) === true ?
            $row_bureau["id"] :
            null;

        // Liste
        $liste = $this->f->db->getOne(
            sprintf(
                'SELECT liste FROM %1$sliste WHERE liste_insee=\'%2$s\'',
                DB_PREFIXE,
                gavoe($demande_ins["result"], array("typeListe", "code", ))
            )
        );
        $this->f->isDatabaseError($liste);

        // Date de naissance
        $date_naissance = $this->f->handle_birth_date(gavoe($demande_ins["result"], array("etatCivilUgle", "dateNaissance", )));

        // Lieu de naissance
        $oel_birth_place = $this->f->handle_birth_place(array(
            "code_commune_de_naissance" => gavoe($demande_ins["result"], array("etatCivilUgle", "communeNaissance", "code", )),
            "libelle_commune_de_naissance" => gavoe($demande_ins["result"], array("etatCivilUgle", "communeNaissance", "libelle", )),
            "code_pays_de_naissance" => gavoe($demande_ins["result"], array("etatCivilUgle", "paysNaissance", "code", )),
            "libelle_pays_de_naissance" => gavoe($demande_ins["result"], array("etatCivilUgle", "paysNaissance", "libelle", )),
        ));
        $code_departement_naissance = $oel_birth_place["code_departement_naissance"];
        $libelle_departement_naissance = $oel_birth_place["libelle_departement_naissance"];
        $code_lieu_de_naissance = $oel_birth_place["code_lieu_de_naissance"];
        $libelle_lieu_de_naissance = $oel_birth_place["libelle_lieu_de_naissance"];

        //
        $inst_inscription = $this->f->get_inst__om_dbform(array(
            "obj" => "inscription",
            "idx" => "]",
        ));
        $naissance_type_saisie = $inst_inscription->get_naissance_type_saisie(
            $code_departement_naissance,
            $libelle_departement_naissance,
            $code_lieu_de_naissance,
            $libelle_lieu_de_naissance
        );
        $live_pays_de_naissance = '';
        $live_ancien_departement_francais_algerie = '';
        $live_commune_de_naissance = '';
        if ($naissance_type_saisie === "Etranger") {
            $live_pays_de_naissance = $code_departement_naissance;
        } elseif ($naissance_type_saisie === "Ancien_dep_franc") {
            $live_ancien_departement_francais_algerie = $code_departement_naissance;
        } elseif ($naissance_type_saisie === "France") {
            $live_commune_de_naissance = $code_lieu_de_naissance;
        }
        // Adresse de rattachement
        $adresse_rattachement = sprintf(
            '%1$s %2$s %3$s %4$s %5$s %6$s %7$s %8$s',
            trim(gavoe($demande_ins["result"], array("adresseRattachement", "numVoie", ))),
            trim(gavoe($demande_ins["result"], array("adresseRattachement", "libVoie", ))),
            trim(gavoe($demande_ins["result"], array("adresseRattachement", "complement1", ))),
            trim(gavoe($demande_ins["result"], array("adresseRattachement", "complement2", ))),
            trim(gavoe($demande_ins["result"], array("adresseRattachement", "lieuDit", ))),
            trim(gavoe($demande_ins["result"], array("adresseRattachement", "codePostal", ))),
            trim(gavoe($demande_ins["result"], array("adresseRattachement", "commune", ))),
            trim(gavoe($demande_ins["result"], array("adresseRattachement", "pays", )))
        );
        // Adresse résident
        $adresse_resident = trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "numVoie", )))." ".trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "libVoie", )));
        $complement_resident = trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "complement1", )))." ".trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "complement2", )));
        $cp_resident = (trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "codePostal", ))) !== '' ? trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "codePostal", ))) : '-');
        $ville_resident = (trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "commune", ))) !== '' ? trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "commune", ))) : '-');
        if ($adresse_resident === "" || $adresse_resident === null) {
            $adresse_resident = $complement_resident;
            $complement_resident = trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "lieuDit", )));
        }
        if ($adresse_resident === "" || $adresse_resident === null) {
            $adresse_resident = trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "lieuDit", )));
            $complement_resident = '-';
        }
        if ($ville_resident !== '-'
            && trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "pays", ))) !== ''
            && strtolower(trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "pays", )))) !== 'france') {
            //
            $ville_resident .= " ".trim(gavoe($demande_ins["result"], array("coordonneesContact", "adresse", "pays", )));
        }

        // VISA
        $visa = null;
        if (gavoe($demande_ins["result"], array("visa", "code", )) == "D_OK") {
            $visa = "accepte";
        }
        if (gavoe($demande_ins["result"], array("visa", "code", )) == "D_KO") {
            $visa = "refuse";
        }

        // Date de la demande
        $date_demande = gavoe($demande_ins["result"], array("dateDemandeUsager", ));
        if ($date_demande === "") {
            $date_demande = gavoe($demande_ins["result"], array("dateDemandeComplete", ));
        }

        //
        $val = array(
            //
            'origin' => 'from_reu_notification',
            //
            'electeur_id' => 0,
            'numero_bureau' => '',
            'bureau' => $bureau,
            'ancien_bureau' => '',
            'bureauforce' => ($bureau !== null ? 'Oui' : 'Non'),
            // liste et traitement
            'liste' => $liste,
            'date_tableau' => '2024-12-31',
            'tableau' => 'annuel',
            'etat' => 'actif',
            // etat civil
            'civilite' => (gavoe($demande_ins["result"], array("etatCivilUgle", "sexe", "code", )) == "M" ? "M." : "Mme"),
            'sexe' => gavoe($demande_ins["result"], array("etatCivilUgle", "sexe", "code", )),
            'nom' => gavoe($demande_ins["result"], array("etatCivilUgle", "nomNaissance", )),
            'nom_usage' => gavoe($demande_ins["result"], array("etatCivilUgle", "nomUsage", )),
            'prenom' => gavoe($demande_ins["result"], array("etatCivilUgle", "prenoms", ), "."),
            //naissance
            'date_naissance' => $date_naissance,
            'code_departement_naissance' => $code_departement_naissance,
            'libelle_departement_naissance' => $libelle_departement_naissance,
            'code_lieu_de_naissance' => $code_lieu_de_naissance,
            'libelle_lieu_de_naissance' => $libelle_lieu_de_naissance,
            'naissance_type_saisie' => $naissance_type_saisie,
            'live_pays_de_naissance' => $live_pays_de_naissance,
            'live_ancien_departement_francais_algerie' => $live_ancien_departement_francais_algerie,
            'live_commune_de_naissance' => $live_commune_de_naissance,
            'code_nationalite' => gavoe($demande_ins["result"], array("etatCivilUgle", "nationalite", "code")),
            'numero_habitation' => 0,
            'libelle_voie' => 'NON COMMUNIQUEE',
            'code_voie' => "NC".intval($_SESSION["collectivite"]),
            'complement_numero' => '',
            'complement' => '',
            // provenance
            'provenance' => '',
            'libelle_provenance' => '',
            // recuperation adresse resident
            'resident' => (gavoe($demande_ins["result"], array("coordonneesContact", "adresse", )) === "" ? "Non" : "Oui"),
            'adresse_resident' => $adresse_resident,
            'complement_resident' => $complement_resident,
            'cp_resident' => $cp_resident,
            'ville_resident' => $ville_resident,
            //
            'telephone' => gavoe($demande_ins["result"], array("coordonneesContact", "telephone1", )),
            'courriel' => gavoe($demande_ins["result"], array("coordonneesContact", "mail", )),
            //initialiser a vide
            'id' => '',
            'utilisateur' => '',
            'date_modif' => '',
            'envoi_cnen' => '',
            'date_cnen' => '',
            //
            'om_collectivite' => intval($_SESSION["collectivite"]),
            'date_j5' => '',
            'types' => gavoe($demande_ins["result"], array("motif", "code", )),
            'observation' => $this->getVal('libelle'),
            'ine' => $this->getVal('id_electeur'),
            'id_demande' => $this->getVal('id_demande'),
            'visa' => $visa,
            'date_visa' => gavoe($demande_ins["result"], array("dateDemandeVisee", ), null),
            'date_complet' => gavoe($demande_ins["result"], array("dateDemandeComplete", )),
            'date_demande' => $date_demande,
            'statut' => 'ouvert',
            'adresse_rattachement_reu' => $adresse_rattachement,
            'historique' => null,
            'archive_electeur' => null,
            'provenance_demande' => trim(gavoe($demande_ins["result"], array("provenance", "code", )), null),
        );
        //
        $add_ins = $inst_inscription->ajouter($val);
        if ($add_ins === false) {
            $this->addToMessage($inst_inscription->msg);
            $this->addToMessage(__("Erreur lors de la création du mouvement d'inscription."));
            return false;
        }

        // Gestion des pièces justificatives
        $pieces = gavoe($demande_ins["result"], array("justificatifs", ), null);
        if ($pieces !== null) {
            //
            foreach ($pieces as $piece) {
                $params = array(
                    "mode" => 'piece',
                    "id" => $this->getVal('id_demande'),
                    "id_piece" => $piece["id"],
                );
                $content_file = $inst_reu->handle_inscription($params);
                $filename = str_replace('filename=', "", substr(gavoe($content_file["headers"], array("Content-Disposition", )), strrpos(gavoe($content_file["headers"], array("Content-Disposition", )), "filename=")));
                $mimetype = $content_file['content_type'];
                // Vérification des extensions
                if ($mimetype == 'application/octet-stream') {
                    $file_info = new SplFileInfo($filename);
                    $ext = $file_info->getExtension();
                    if (strtolower($ext) === 'pdf') {
                        $mimetype = 'application/pdf';
                    }
                    if (strtolower($ext) === 'jpg' || strtolower($ext) === 'jpeg') {
                        $mimetype = 'image/jpeg';
                    }
                    if (strtolower($ext) === 'png') {
                        $mimetype = 'image/png';
                    }
                }
                // Métadonnées du document
                $metadata = array(
                    'filename' => $filename,
                    'mimetype' => $mimetype,
                    'size' => strlen($content_file["result"]),
                );
                $uid = $this->f->storage->create($content_file["result"], $metadata);
                if ($uid == 'OP_FAILURE') {
                    $this->addToMessage(__("Erreur lors de la récupération des fichiers des pièces justificatives de l'inscription."));
                    return false;
                }

                // Gestion des types des pièces
                $piece_type = $this->f->get_one_result_from_db_query(
                    sprintf(
                        'SELECT id FROM %1$spiece_type WHERE code=\'%2$s\'',
                        DB_PREFIXE,
                        $piece['typeJustificatif']['code']
                    ),
                    true
                );
                if ($piece_type['code'] === 'KO') {
                    $this->addToMessage(__("Erreur lors de la récupération des types des pièces justificatives de l'inscription."));
                    return false;
                }

                $inst_piece = $this->f->get_inst__om_dbform(array(
                    "obj" => "piece",
                    "idx" => "]",
                ));
                $val = array(
                    'id' => '',
                    'mouvement' => $inst_inscription->get_mouvement_id_from_reu_demande_id($this->getVal('id_demande')),
                    'libelle' => (isset($piece["libelleJustificatif"]) === true ? $piece["libelleJustificatif"] : '-'),
                    'piece_type' => $piece_type['result'],
                    'fichier' => $uid,
                    'om_collectivite' => intval($_SESSION["collectivite"]),
                );
                $add_piece = $inst_piece->ajouter($val);
                if ($add_piece === false) {
                    $this->addToMessage($inst_piece->msg);
                    $this->addToMessage(__("Erreur lors de la création des pièces justificatives de l'inscription."));
                    return false;
                }
            }
        }

        // Gestion de l'historique du mouvement
        $val_histo = array(
            'id' => $inst_inscription->valF[$inst_inscription->clePrimaire],
            'action' => 'ajouter',
            'message' => $this->getVal('libelle'),
        );
        $histo = $inst_inscription->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
            return false;
        }

        //
        return true;
    }

    /**
     *
     */
    function add_mouvement_radiation() {
        // Récupération de la demande
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->addToMessage(__("Problème de configuration de la connexion au REU."));
            return false;
        }
        if ($inst_reu->is_connexion_logicielle_valid() !== true) {
            $this->addToMessage(__("La connexion logicielle au REU n'est pas valide."));
            return false;
        }
        $params = array(
            "mode" => 'get',
            "id" => $this->getVal('id_demande'),
        );
        $demande_rad = $inst_reu->handle_radiation($params);
        //
        if ($demande_rad["code"] != "200") {
            $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
            return false;
        }

        // Récupération de la provenance
        $this->set_provenance_demande_reu(trim(gavoe($demande_rad["result"], array("provenance", "code", ))));
        // Liste
        $liste = $this->f->db->getOne(
            sprintf(
                'SELECT liste FROM %1$sliste WHERE liste_insee=\'%2$s\'',
                DB_PREFIXE,
                gavoe($demande_rad["result"], array("typeListe", "code", ))
            )
        );
        $this->f->isDatabaseError($liste);
        //
        if (strtolower(gavoe($demande_rad["result"], array("typeListe", "code", ))) === 'lc2') {
            $elec_id = $this->get_electeur_id_by_ine($this->getVal('id_electeur'));
        } else {
            $get_electeur_ids_by_ine = $this->get_electeur_ids_by_ine($this->getVal('id_electeur'), array($liste, ));
            if (is_array($get_electeur_ids_by_ine) === true) {
                $elec_id = $get_electeur_ids_by_ine[0]['id'];
            } else {
                $elec_id = 0;
            }
        }
        $inst_electeur = $this->f->get_inst__om_dbform(array(
            "obj" => "electeur",
            "idx" => $elec_id,
        ));
        $val = array();
        if (strtolower(gavoe($demande_rad["result"], array("typeListe", "code", ))) === 'lc2'
            || $inst_electeur->exists() !== true) {
            //
            $inst_radiation = $this->f->get_inst__om_dbform(array(
                "obj" => "mouvement",
                "idx" => "]",
            ));
            // Bureau de vote
            $res_bureau = $this->f->db->query(
                sprintf(
                    'SELECT id, code FROM %1$sbureau WHERE referentiel_id=%3$s AND om_collectivite=%2$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    intval(gavoe($demande_rad["result"], array("bureauVote", "id", )))
                )
            );
            $this->f->isDatabaseError($res_bureau);
            $row_bureau = $res_bureau->fetchRow(DB_FETCHMODE_ASSOC);
            $bureau = is_array($row_bureau) === true && array_key_exists("id", $row_bureau) === true ?
                $row_bureau["id"] :
                null;
            // Date de naissance
            $date_naissance = $this->f->handle_birth_date(gavoe($demande_rad["result"], array("etatCivilElecteur", "dateNaissance", )));
            // Lieu de naissance
            $oel_birth_place = $this->f->handle_birth_place(array(
                "code_commune_de_naissance" => gavoe($demande_rad["result"], array("etatCivilElecteur", "communeNaissance", "code", )),
                "libelle_commune_de_naissance" => gavoe($demande_rad["result"], array("etatCivilElecteur", "communeNaissance", "libelle", )),
                "code_pays_de_naissance" => gavoe($demande_rad["result"], array("etatCivilElecteur", "paysNaissance", "code", )),
                "libelle_pays_de_naissance" => gavoe($demande_rad["result"], array("etatCivilElecteur", "paysNaissance", "libelle", )),
            ));
            $code_departement_naissance = $oel_birth_place["code_departement_naissance"];
            $libelle_departement_naissance = $oel_birth_place["libelle_departement_naissance"];
            $code_lieu_de_naissance = $oel_birth_place["code_lieu_de_naissance"];
            $libelle_lieu_de_naissance = $oel_birth_place["libelle_lieu_de_naissance"];
            $naissance_type_saisie = $inst_radiation->get_naissance_type_saisie(
                $code_departement_naissance,
                $libelle_departement_naissance,
                $code_lieu_de_naissance,
                $libelle_lieu_de_naissance
            );
            $live_pays_de_naissance = '';
            $live_ancien_departement_francais_algerie = '';
            $live_commune_de_naissance = '';
            if ($naissance_type_saisie === "Etranger") {
                $live_pays_de_naissance = $code_departement_naissance;
            } elseif ($naissance_type_saisie === "Ancien_dep_franc") {
                $live_ancien_departement_francais_algerie = $code_departement_naissance;
            } elseif ($naissance_type_saisie === "France") {
                $live_commune_de_naissance = $code_lieu_de_naissance;
            }
            // Adresse de rattachement
            $adresse_rattachement = sprintf(
                '%1$s %2$s %3$s %4$s %5$s %6$s %7$s %8$s',
                trim(gavoe($demande_rad["result"], array("adresseRattachement", "numVoie", ))),
                trim(gavoe($demande_rad["result"], array("adresseRattachement", "libVoie", ))),
                trim(gavoe($demande_rad["result"], array("adresseRattachement", "complement1", ))),
                trim(gavoe($demande_rad["result"], array("adresseRattachement", "complement2", ))),
                trim(gavoe($demande_rad["result"], array("adresseRattachement", "lieuDit", ))),
                trim(gavoe($demande_rad["result"], array("adresseRattachement", "codePostal", ))),
                trim(gavoe($demande_rad["result"], array("adresseRattachement", "commune", ))),
                trim(gavoe($demande_rad["result"], array("adresseRattachement", "pays", )))
            );
            // Adresse résident
            $adresse_resident = trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "numVoie", )))." ".trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "libVoie", )));
            $complement_resident = trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "complement1", )))." ".trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "complement2", )));
            $cp_resident = (trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "codePostal", ))) !== '' ? trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "codePostal", ))) : '-');
            $ville_resident = (trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "commune", ))) !== '' ? trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "commune", ))) : '-');
            if ($adresse_resident === "" || $adresse_resident === null) {
                $adresse_resident = $complement_resident;
                $complement_resident = trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "lieuDit", )));
            }
            if ($adresse_resident === "" || $adresse_resident === null) {
                $adresse_resident = trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "lieuDit", )));
                $complement_resident = '-';
            }
            if ($ville_resident !== '-'
                && trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "pays", ))) !== ''
                && strtolower(trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "pays", )))) !== 'france') {
                //
                $ville_resident .= " ".trim(gavoe($demande_rad["result"], array("coordonneesContact", "adresse", "pays", )));
            }
            // VISA
            $visa = null;
            if (gavoe($demande_rad["result"], array("visa", "code", )) == "D_OK") {
                $visa = "accepte";
            }
            if (gavoe($demande_rad["result"], array("visa", "code", )) == "D_KO") {
                $visa = "refuse";
            }
            //
            $date_ouverture = gavoe($demande_rad["result"], array("etats", "0", "date"));
            if ($date_ouverture != "") {
                $date_ouverture = substr($date_ouverture, 0, 10);
            }
            //
            $val = array(
                //
                'origin' => 'from_reu_notification',
                //
                'electeur_id' => 0,
                'numero_bureau' => '',
                'bureau' => $bureau,
                'ancien_bureau' => $bureau,
                'bureauforce' => 'Oui',
                // liste et traitement
                'liste' => $liste,
                'date_tableau' => '2024-12-31',
                'tableau' => 'annuel',
                'etat' => 'actif',
                // etat civil
                'civilite' => (gavoe($demande_rad["result"], array("etatCivilElecteur", "sexe", "code", )) == "M" ? "M." : "Mme"),
                'sexe' => gavoe($demande_rad["result"], array("etatCivilElecteur", "sexe", "code", )),
                'nom' => gavoe($demande_rad["result"], array("etatCivilElecteur", "nomNaissance", )),
                'nom_usage' => gavoe($demande_rad["result"], array("etatCivilElecteur", "nomUsage", )),
                'prenom' => gavoe($demande_rad["result"], array("etatCivilElecteur", "prenoms", )),
                //naissance
                'date_naissance' => $date_naissance,
                'code_departement_naissance' => $code_departement_naissance,
                'libelle_departement_naissance' => $libelle_departement_naissance,
                'code_lieu_de_naissance' => $code_lieu_de_naissance,
                'libelle_lieu_de_naissance' => $libelle_lieu_de_naissance,
                'naissance_type_saisie' => $naissance_type_saisie,
                'live_pays_de_naissance' => $live_pays_de_naissance,
                'live_ancien_departement_francais_algerie' => $live_ancien_departement_francais_algerie,
                'live_commune_de_naissance' => $live_commune_de_naissance,
                'code_nationalite' => gavoe($demande_rad["result"], array("etatCivilElecteur", "nationalite", "code")),
                'numero_habitation' => 0,
                'libelle_voie' => 'NON COMMUNIQUEE',
                'code_voie' => "NC".intval($_SESSION["collectivite"]),
                'complement_numero' => '',
                'complement' => '',
                // provenance
                'provenance' => '',
                'libelle_provenance' => '',
                // recuperation adresse resident
                'resident' => (gavoe($demande_rad["result"], array("coordonneesContact", "adresse", )) === "" ? "Non" : "Oui"),
                'adresse_resident' => $adresse_resident,
                'complement_resident' => $complement_resident,
                'cp_resident' => $cp_resident,
                'ville_resident' => $ville_resident,
                //
                'telephone' => gavoe($demande_rad["result"], array("coordonneesContact", "telephone1", )),
                'courriel' => gavoe($demande_rad["result"], array("coordonneesContact", "mail", )),
                //initialiser a vide
                'id' => '',
                'utilisateur' => '',
                'date_modif' => '',
                'envoi_cnen' => '',
                'date_cnen' => '',
                //
                'om_collectivite' => intval($_SESSION["collectivite"]),
                'date_j5' => '',
                'types' => gavoe($demande_rad["result"], array("motif", "code", )),
                'observation' => $this->getVal('libelle'),
                'ine' => $this->getVal('id_electeur'),
                'id_demande' => $this->getVal('id_demande'),
                'visa' => $visa,
                'date_visa' => gavoe($demande_rad["result"], array("dateDemandeVisee", ), null),
                'date_complet' => gavoe($demande_rad["result"], array("dateDemandeComplete", )),
                'date_demande' => $this->f->formatDate($date_ouverture, false),
                'statut' => 'ouvert',
                'adresse_rattachement_reu' => $adresse_rattachement,
                'historique' => null,
                'archive_electeur' => null,
                'provenance_demande' => trim(gavoe($demande_rad["result"], array("provenance", "code", )), null),
            );
        } else {
            //
            $inst_radiation = $this->f->get_inst__om_dbform(array(
                "obj" => "radiation",
                "idx" => "]",
            ));
            // Naissance
            $naissance_type_saisie = $inst_radiation->get_naissance_type_saisie(
                $inst_electeur->getVal('code_departement_naissance'),
                $inst_electeur->getVal('libelle_departement_naissance'),
                $inst_electeur->getVal('code_lieu_de_naissance'),
                $inst_electeur->getVal('libelle_lieu_de_naissance')
            );
            // VISA
            $visa = null;
            if (gavoe($demande_rad["result"], array("visa", "code", )) == "D_OK") {
                $visa = "accepte";
            }
            if (gavoe($demande_rad["result"], array("visa", "code", )) == "D_KO") {
                $visa = "refuse";
            }
            //
            $date_ouverture = gavoe($demande_rad["result"], array("etats", "0", "date"));
            if ($date_ouverture != "") {
                $date_ouverture = substr($date_ouverture, 0, 10);
            }
            //
            $val = array(
                'electeur_id' => $inst_electeur->getVal('id'),
                'numero_bureau' => $inst_electeur->getVal('numero_bureau'),
                'bureau' => $inst_electeur->getVal('bureau'),
                'ancien_bureau' => $inst_electeur->getVal('bureau'),
                'bureauforce' => $inst_electeur->getVal('bureauforce'),
                // liste et traitement
                'liste' => $inst_electeur->getVal('liste'),
                'date_tableau' => '2024-12-31',
                'tableau' => "annuel",
                'etat' => "actif",
                // etat civil
                'civilite' => $inst_electeur->getVal('civilite'),
                'sexe' => $inst_electeur->getVal('sexe'),
                'nom' => $inst_electeur->getVal('nom'),
                'nom_usage' => $inst_electeur->getVal('nom_usage'),
                'prenom' => $inst_electeur->getVal('prenom'),
                //naissance
                'date_naissance' => $inst_electeur->getVal('date_naissance'),
                'code_departement_naissance' => $inst_electeur->getVal('code_departement_naissance'),
                'libelle_departement_naissance' => $inst_electeur->getVal('libelle_departement_naissance'),
                'code_lieu_de_naissance' => $inst_electeur->getVal('code_lieu_de_naissance'),
                'libelle_lieu_de_naissance' => $inst_electeur->getVal('libelle_lieu_de_naissance'),
                'naissance_type_saisie' => $naissance_type_saisie,
                'code_nationalite' => $inst_electeur->getVal('code_nationalite'),
                // adresse dans la commune
                'numero_habitation' => $inst_electeur->getVal('numero_habitation'),
                'libelle_voie' => $inst_electeur->getVal('libelle_voie'),
                'code_voie' => $inst_electeur->getVal('code_voie'),
                'complement_numero' => $inst_electeur->getVal('complement_numero') ,
                'complement' => $inst_electeur->getVal('complement'),
                // provenance
                'provenance' => $inst_electeur->getVal('provenance'),
                'libelle_provenance' => $inst_electeur->getVal('libelle_provenance'),
                // recuperation adresse resident
                'resident' => $inst_electeur->getVal('resident'),
                'adresse_resident' => $inst_electeur->getVal('adresse_resident'),
                'complement_resident' => $inst_electeur->getVal('complement_resident'),
                'cp_resident' => $inst_electeur->getVal('cp_resident'),
                'ville_resident' => $inst_electeur->getVal('ville_resident'),
                //
                'telephone' => $inst_electeur->getVal('telephone'),
                'courriel' => $inst_electeur->getVal('courriel'),
                //initialiser a vide
                'id' => '',
                'utilisateur' => '',
                'date_modif' => '',
                'envoi_cnen' => '',
                'date_cnen' => '',
                //
                'om_collectivite' => $inst_electeur->getVal('om_collectivite'),
                'date_j5' => '',
                'types' => gavoe($demande_rad["result"], array("motif", "code", )),
                'observation' => $this->getVal('libelle'),
                'ine' => $this->getVal('id_electeur'),
                'id_demande' => $this->getVal('id_demande'),
                'visa' => $visa,
                'date_visa' => gavoe($demande_rad["result"], array("dateDemandeVisee", ), null),
                'date_complet' => null,
                'date_demande' => $date_ouverture,
                "statut" => 'ouvert',
                'adresse_rattachement_reu' => null,
                'historique' => null,
                'archive_electeur' => null,
                'provenance_demande' => trim(gavoe($demande_rad["result"], array("provenance", "code", )), null),
            );
        }
        $add_rad = $inst_radiation->ajouter($val);
        if ($add_rad === false) {
            $this->addToMessage($inst_radiation->msg);
            $this->addToMessage(__("Erreur lors de la création du mouvement de radiation."));
            return false;
        }

        // Gestion des pièces justificatives
        $pieces = gavoe($demande_rad["result"], array("justificatifs", ), null);
        if ($pieces !== null) {
            //
            foreach ($pieces as $piece) {
                $params = array(
                    "mode" => 'piece',
                    "id" => $this->getVal('id_demande'),
                    "id_piece" => $piece["id"],
                );
                $content_file = $inst_reu->handle_radiation($params);
                $filename = str_replace('filename=', "", substr(gavoe($content_file["headers"], array("Content-Disposition", )), strrpos(gavoe($content_file["headers"], array("Content-Disposition", )), "filename=")));
                $mimetype = $content_file['content_type'];
                // Vérification des extensions
                if ($mimetype == 'application/octet-stream') {
                    $file_info = new SplFileInfo($filename);
                    $ext = $file_info->getExtension();
                    if (strtolower($ext) === 'pdf') {
                        $mimetype = 'application/pdf';
                    }
                    if (strtolower($ext) === 'jpg' || strtolower($ext) === 'jpeg') {
                        $mimetype = 'image/jpeg';
                    }
                    if (strtolower($ext) === 'png') {
                        $mimetype = 'image/png';
                    }
                }
                // Métadonnées du document
                $metadata = array(
                    'filename' => $filename,
                    'mimetype' => $mimetype,
                    'size' => strlen($content_file["result"]),
                );
                $uid = $this->f->storage->create($content_file["result"], $metadata);
                if ($uid == 'OP_FAILURE') {
                    $this->addToMessage(__("Erreur lors de la récupération des fichiers des pièces justificatives de radiation."));
                    return false;
                }

                // Gestion des types des pièces
                $piece_type = $this->f->get_one_result_from_db_query(
                    sprintf(
                        'SELECT id FROM %1$spiece_type WHERE code=\'%2$s\'',
                        DB_PREFIXE,
                        $piece['typeJustificatif']['code']
                    ),
                    true
                );
                if ($piece_type['code'] === 'KO') {
                    $this->addToMessage(__("Erreur lors de la récupération des types des pièces justificatives de la radiation."));
                    return false;
                }

                $inst_piece = $this->f->get_inst__om_dbform(array(
                    "obj" => "piece",
                    "idx" => "]",
                ));
                $val = array(
                    'id' => '',
                    'mouvement' => $inst_radiation->get_mouvement_id_from_reu_demande_id($this->getVal('id_demande')),
                    'libelle' => (isset($piece["libelleJustificatif"]) === true ? $piece["libelleJustificatif"] : '-'),
                    'piece_type' => $piece_type['result'],
                    'fichier' => $uid,
                    'om_collectivite' => intval($_SESSION["collectivite"]),
                );
                $add_piece = $inst_piece->ajouter($val);
                if ($add_piece === false) {
                    $this->addToMessage($inst_piece->msg);
                    $this->addToMessage(__("Erreur lors de la création des pièces justificatives de la radiation."));
                    return false;
                }
            }
        }

        // Gestion de l'historique du mouvement
        $val_histo = array(
            'id' => $inst_radiation->valF[$inst_radiation->clePrimaire],
            'action' => 'ajouter',
            'message' => $this->getVal('libelle'),
        );
        $histo = $inst_radiation->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
            return false;
        }

        //
        return true;
    }

    /**
     *
     */
    function get_electeur_ids_by_ine($ine, $listes = '') {
        if ($this->ine_electeur_exists($ine) === false) {
            return false;
        }
        $where_liste = '';
        if ($listes !== '') {
            $template = ' AND liste IN (\'%s\') ';
            $where_liste = sprintf(
                $template,
                implode('\', \'', $listes)
            );
        }
        $electeur_ids = $this->f->get_all_results_from_db_query(
            sprintf(
                'SELECT id FROM %1$selecteur WHERE ine=\'%3$s\' AND om_collectivite=%2$s %4$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $ine,
                $where_liste
            )
        );
        if ($electeur_ids["code"] != "OK") {
            return false;
        }
        return $electeur_ids["result"];
    }

    function get_electeur_id_by_ine($ine) {
        if ($this->ine_electeur_exists($ine) === false) {
            return false;
        }
        $electeur_id = $this->f->db->getOne(
            sprintf(
                'SELECT id FROM %1$selecteur WHERE ine=\'%3$s\' AND om_collectivite=%2$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $ine
            )
        );
        $this->f->isDatabaseError($electeur_id);
        return $electeur_id;
    }

    /**
     * CONDITION - is_not_treated
     *
     * @return boolean
     */
    function is_not_treated() {
        $treated = $this->getVal('traitee');
        if ($treated === 't') {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_demande_procuration
     *
     * @return boolean
     */
    function is_demande_procuration() {
        $type_de_notification = $this->getVal('type_de_notification');
        if ($type_de_notification !== 'CREAPROC'
            && $type_de_notification !== 'CREAPROCLIGNE'
            && $type_de_notification !== 'MODIFPROC'
            && $type_de_notification !== 'ANNUPROC'
            && $type_de_notification !== 'ANNUPROCLIGNE') {
            //
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_demande_inscription
     *
     * @return boolean
     */
    function is_demande_inscription() {
        $type_de_notification = $this->getVal('type_de_notification');
        if ($type_de_notification !== 'INS'
            && $type_de_notification !== 'INS_OFF'
            && $type_de_notification !== 'DDE') {
            //
            return false;
        }
        return true;
    }

    function is_demande_livrable() {
        $type_de_notification = $this->getVal('type_de_notification');
        if ($type_de_notification !== 'LIV') {
            //
            return false;
        }
        return true;
    }

    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire);
    }

    /**
     * [setType description]
     * @param [type] &$form [description]
     * @param [type] $maj   [description]
     */
    function setType(&$form, $maj) {
        // Cache tous les champs du formulaire
        if ($maj == 101 || $maj == 201 || $maj == 102) {
            foreach ($this->champs as $champ) {
                $form->setType($champ, 'hidden');
            }
        }
    }

    /**
     *
     */
    function get_back_target() {
        if ($this->getParameter("maj") == 101 || $this->getParameter("maj") == 102) {
            return "tab";
        }
        return parent::get_back_target();
    }
    /**
     *
     */
    function get_last_notification() {
        $query = sprintf(
            'SELECT max(id) FROM %1$sreu_notification WHERE om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->getone($query);
        $this->f->isDatabaseError($res);
        return $res;
    }

    /**
     * TREATMENT - treat_all_notifications.
     *
     * @return boolean
     */
    function treat_all_notifications($val = array()) {
        if ($this->reu_sync_l_e_valid_is_done() !== true) {
            $this->addToMessage(__("Cette fonctionnalité n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée."));
            return false;
        }
        $this->begin_treatment(__METHOD__);
        $query = sprintf(
            'SELECT id FROM %1$sreu_notification WHERE traitee IS FALSE AND om_collectivite=%2$s ORDER BY id',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->query($query);
        $this->f->isDatabaseError($res);
        $total = 0;
        $success = 0;
        $fail = 0;
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $total += 1;
            $reu_notification = $this->f->get_inst__om_dbform(array(
                "obj" => "reu_notification",
                "idx" => $row['id']
            ));
            $this->addToMessage(sprintf('=> notification n°%s', $row["id"]));
            $this->f->db->autoCommit(false);
            $ret = $reu_notification->traiter();
            if ($ret !== true) {
                $fail += 1;
                $this->correct = false;
                $this->addToMessage($reu_notification->msg);
                $this->undoValidation();
            } else {
                $success += 1;
                $this->addToMessage($reu_notification->msg);
                $this->f->db->commit();
            }
        }
        //
        $reu_sync_check_status = array(
            "last_notification_treatment_date" => date("d/m/Y H:i:s"),
        );
        $current_reu_sync_check_status = json_decode($this->f->getParameter("reu_sync_check_status"), true);
        if (is_array($current_reu_sync_check_status) !== true) {
            $current_reu_sync_check_status = array();
        }
        $new_reu_sync_check_status = array_merge($current_reu_sync_check_status, $reu_sync_check_status);
        $this->f->insert_or_update_parameter("reu_sync_check_status", json_encode($new_reu_sync_check_status));
        $this->f->db->commit();
        //
        $this->addToMessage(sprintf('%s notification(s) traitée(s) sur un total de %s.', $success, $total));
        return true;
    }

    /**
     *
     */
    function sync_last_notifications() {
        if ($this->reu_sync_l_e_valid_is_done() !== true) {
            $this->addToMessage(__("Cette fonctionnalité n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée."));
            return false;
        }
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->addToMessage(__("Problème de configuration de la connexion au REU."));
            return false;
        }
        if ($inst_reu->is_connexion_logicielle_valid() !== true) {
            $this->addToMessage(__("La connexion logicielle au REU n'est pas valide."));
            return false;
        }
        $params = array();
        $debutId = intval($this->get_last_notification());
        if ($debutId !== 0) {
            $params = array(
                "debutId" => $debutId,
            );
        }
        $notifications = $inst_reu->get_notifications($params);
        if ($notifications["code"] != "200" && $notifications["code"] != "206") {
            if ($notifications["code"] == "204") {
                $this->addToMessage(__("Aucune nouvelle notification."));
                //
                $reu_sync_check_status = array(
                    "last_notification_sync_date" => date("d/m/Y H:i:s"),
                );
                $current_reu_sync_check_status = json_decode($this->f->getParameter("reu_sync_check_status"), true);
                if (is_array($current_reu_sync_check_status) !== true) {
                    $current_reu_sync_check_status = array();
                }
                $new_reu_sync_check_status = array_merge($current_reu_sync_check_status, $reu_sync_check_status);
                $this->f->insert_or_update_parameter("reu_sync_check_status", json_encode($new_reu_sync_check_status));
                return true;
            }
            $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
            return false;
        }
        $inserted = 0;
        foreach ($notifications["result"] as $key => $value) {
            //
            $dateCollecte = null;
            if (array_key_exists("dateCollecte", $value) === true) {
                $dtime = DateTime::createFromFormat('d/m/Y', $value["dateCollecte"]);
                if ($dtime === false) {
                    $this->correct = false;
                    $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                    return false;
                }
                $dateCollecte = $dtime->format('Y-m-d');
            }
            $valF = array(
                "id" => $value["id"],
                "date_de_creation" => $value["dateCreation"],
                "libelle" => $value["libelle"],
                "id_demande" => (array_key_exists("idDemande", $value) ? $value["idDemande"] : ""),
                "id_electeur" => (array_key_exists("idElecteur", $value) ? $value["idElecteur"] : null),
                "lu" => $value["lu"],
                "lien_uri" => $value["lienUri"],
                "resultat_de_notification" => (array_key_exists("resultatNotification", $value) && array_key_exists("code", $value["resultatNotification"]) ? $value["resultatNotification"]["code"] : ""),
                "type_de_notification" => (array_key_exists("typeNotification", $value)  && array_key_exists("code", $value["typeNotification"])? $value["typeNotification"]["code"] : ""),
                "date_de_collecte" => $dateCollecte,
                "om_collectivite" => intval($_SESSION["collectivite"]),
            );
            $cle = sprintf(
                ' NOT EXISTS (SELECT id FROM %1$sreu_notification WHERE id=%3$s AND om_collectivite=%2$s) ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $valF["id"]
            );
            $res = $this->f->db->autoExecute(DB_PREFIXE."reu_notification", $valF, DB_AUTOQUERY_INSERT, $cle);
            // Logger
            $this->f->addToLog(__METHOD__."(): db->autoExecute(\"".DB_PREFIXE."reu_notification\", ".print_r($valF, true).", DB_AUTOQUERY_INSERT);", VERBOSE_MODE);
            if ($this->f->isDatabaseError($res, true) !== false) {
                $this->correct = false;
                $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                return false;
            }
            if ($this->f->db->affectedRows() == 1) {
                $inserted += 1;
            }
        }
        //
        $reu_sync_check_status = array(
            "last_notification_sync_date" => date("d/m/Y H:i:s"),
        );
        $current_reu_sync_check_status = json_decode($this->f->getParameter("reu_sync_check_status"), true);
        if (is_array($current_reu_sync_check_status) !== true) {
            $current_reu_sync_check_status = array();
        }
        $new_reu_sync_check_status = array_merge($current_reu_sync_check_status, $reu_sync_check_status);
        $this->f->insert_or_update_parameter("reu_sync_check_status", json_encode($new_reu_sync_check_status));
        //
        $this->addToMessage(
            sprintf(
                '%s (%s) notification(s) importées.',
                count($notifications["result"]),
                $inserted
            )
        );
        return true;
    }

    /**
     *
     * @param integer $id Identifiant de la notification
     *
     * @return boolean
     */
    function is_notification_exist($id = null) {
        if ($id === null) {
            return false;
        }
        //
        $query = sprintf(
            'SELECT id FROM %1$sreu_notification WHERE id=%3$s AND om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            intval($id)
        );
        $res = $this->f->db->getone($query);
        if ($this->f->isDatabaseError($res, true) !== false) {
            return false;
        }
        if ($res === null || $res === "") {
            return false;
        }
        return true;
    }

    /**
     * SETTER_FORM - set_form_default_values (setVal).
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        // Il est nécessaire de définir des valeurs par défaut pour chaque
        // champ en maj = 101 et maj = 102 car ces actions utilisent la view
        // formulaire sans être positionnée sur un objet or om_formulaire
        // n'intiialise pas ces valeurs par défaut et om_dbform non plus
        // lorsqu'il n'y a pas de données dans la table ce qui cause des
        // notices.
        if ($validation == 0 && ($maj == 101 || $maj == 102)) {
            foreach ($this->champs as $key => $value) {
                $form->setVal($value, "");
            }
        }
    }
}
