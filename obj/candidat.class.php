<?php
/**
 * Ce script contient la définition de la classe *candidat*.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/candidat.class.php";

/**
 * Définition de la classe *candidat* (om_dbform).
 */
class candidat extends candidat_gen {

    public function __construct($id, $db, $debug) {
        $this->constructeur($id, $db, $debug);
    }
}
