<?php
/**
 * Ce script définit la classe 'module_decoupage'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_decoupage' (module).
 */
class module_decoupage extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_redecoupage",
            "title" => __("Traitement")." -> ".__("Module Redecoupage"),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_redecoupage-onglet_simulation",
                    "href" => "../app/index.php?module=module_decoupage&view=decoupage_simulation",
                    "id" => "decoupage_simulation",
                    "title" => __("Simulation"),
                    "view" => "decoupage_simulation",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_redecoupage-onglet_traitement_redecoupage",
                    "href" => "../app/index.php?module=module_decoupage&view=redecoupage",
                    "id" => "traitement_redecoupage",
                    "title" => __("Redécoupage"),
                    "view" => "redecoupage",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_redecoupage-onglet_initialisation",
                    "href" => "../app/index.php?module=module_decoupage&view=decoupage_initialisation",
                    "id" => "decoupage_initialisation",
                    "title" => __("Initialisation"),
                    "view" => "decoupage_initialisation",
                ),
            ),
        );
    }

    /**
     * Permet d'interfacer l'onglet 'Initialisation' du module Découpage.
     */
    function view__decoupage_initialisation() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_decoupage");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Decoupage electoral");
        $this->f->displaySubTitle($subtitle);
        $description = __("L'edition suivante permet de visualiser le decoupage ".
                         "electoral parametre dans le logiciel. Il est compose de la ".
                         "liste des voies qui sont associees a un bureau de vote en ".
                         "fonction des numeros d'habitation.");
        $this->f->displayDescription($description);
        //
        $links = array(
            "0" => array(
                "href" => OM_ROUTE_MODULE_EDITION."&obj=decoupage",
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => __("Parametrage du decoupage electoral"),
            ),
        );
        //
        $this->f->displayLinksAsList($links);
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Aide a la saisie");
        $this->f->displaySubTitle($subtitle);
        $description = __("L'edition suivante permet de visualiser la liste de toutes ".
                         "les voies qui n'ont pas de parametrage dans le decoupage. ".
                         "Elle presente la liste des voies separees par bureau de vote ".
                         "avec le nombre d'electeurs associe à ce bureau et cette voie.");
        $this->f->displayDescription($description);
        //
        $links = array(
            "0" => array(
                "href" => OM_ROUTE_MODULE_EDITION."&obj=decoupage_manquant",
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => __("Liste des voies qui ne sont pas parametrees dans le decoupage"),
            ),
        );
        //
        $this->f->displayLinksAsList($links);
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Normalisation des voies");
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.decoupage_normalisation_voies.class.php";
        $trt = new decoupageNormalisationVoiesTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Initialisation des electeurs en bureau force");
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.decoupage_init_bureauforce.class.php";
        $trt = new decoupageInitBureauForceTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Initialisation de la table decoupage");
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.decoupage_init_decoupage.class.php";
        $trt = new decoupageInitDecoupageTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }

    /**
     * Permet d'interfacer l'onglet 'Redécoupage' du module Découpage.
     */
    function view__redecoupage() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_decoupage");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Redecoupage electoral");
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.redecoupage.class.php";
        $trt = new redecoupageTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }

    /**
     * Permet d'interfacer l'onglet 'Simulation' du module Découpage.
     */
    function view__decoupage_simulation() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_decoupage");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Decoupage electoral");
        $this->f->displaySubTitle($subtitle);
        $description = __("L'edition suivante permet de visualiser le decoupage ".
                         "electoral parametre dans le logiciel. Il est compose de la ".
                         "liste des voies qui sont associees a un bureau de vote en ".
                         "fonction des numeros d'habitation.");
        $this->f->displayDescription($description);
        //
        $links = array(
            "0" => array(
                "href" => OM_ROUTE_MODULE_EDITION."&obj=decoupage",
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => __("Parametrage du decoupage electoral"),
            ),
        );
        //
        $this->f->displayLinksAsList($links);
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Simulation de redecoupage");
        $this->f->displaySubTitle($subtitle);
        $description = __("Cette simulation se base uniquement sur les electeurs ".
                         "effectivement inscrits sur la liste electorale. Elle ne ".
                         "prend pas en compte les eventuelles inscriptions, ".
                         "modifications ou radiations en cours. Le tableau presente ".
                         "le nombre d'electeurs actuellement inscrits dans chacun ".
                         "des bureaux de vote avec parmi eux le nombre d'electeurs ".
                         "qui sont inscrits en bureau force. La derniere colonne ".
                         "indique le nombre d'electeurs qui seraient inscrits dans ".
                         "chacun des bureaux si on appliquait le découpage électoral ".
                         "actuellement paramétré sur les electeurs qui ne sont pas en ".
                         "bureau force.");
        $this->f->displayDescription($description);
        //
        require_once "../obj/workdecoupage.class.php";
        $wd = new workdecoupage();
        $result = $wd->simulation();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }
}
