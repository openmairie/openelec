<?php
/**
 * Ce script définit la classe 'edition_pdf__statistiques_inscriptions'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__statistiques_inscriptions' (edition_pdf).
 */
class edition_pdf__statistiques_inscriptions extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * @return array
     */
    public function compute_pdf__statistiques_inscriptions($params = array()) {
        //
        require_once "fpdf.php";
        $aujourdhui = date("d/m/Y");
        $nolibliste = $_SESSION["libelle_liste"];

        //===========================================================================
        ////// Compteur inscription homme "Exclu Office"
        $sql_cpt_ins_homme_exc_office = sprintf(
            'SELECT count(mouvement.*) FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.om_collectivite=om_collectivite.om_collectivite AND param_mouvement.typecat=\'Inscription\' and param_mouvement.codeinscription<>\'8\' AND mouvement.sexe=\'M\' AND mouvement.date_tableau=\'%2$s\'',
            DB_PREFIXE,
            $this->f->getParameter("datetableau")
        );

        ////// Compteur inscription femme "Exclu Office"
        $sql_cpt_ins_femme_exc_office = sprintf(
            'SELECT count(mouvement.*) FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.om_collectivite=om_collectivite.om_collectivite AND param_mouvement.typecat=\'Inscription\' and param_mouvement.codeinscription<>\'8\' AND mouvement.sexe=\'F\' AND mouvement.date_tableau=\'%2$s\'',
            DB_PREFIXE,
            $this->f->getParameter("datetableau")
        );

        ////// Compteur inscription homme "Inclu Office"
        $sql_cpt_ins_homme_inc_office = sprintf(
            'SELECT count(mouvement.*) FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.om_collectivite=om_collectivite.om_collectivite AND param_mouvement.typecat=\'Inscription\' and param_mouvement.codeinscription=\'8\' AND mouvement.sexe=\'M\' AND mouvement.date_tableau=\'%2$s\'',
            DB_PREFIXE,
            $this->f->getParameter("datetableau")
        );

        ////// Compteur inscription femme "Inclu Office"
        $sql_cpt_ins_femme_inc_office = sprintf(
            'SELECT count(mouvement.*) FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.om_collectivite=om_collectivite.om_collectivite AND param_mouvement.typecat=\'Inscription\' and param_mouvement.codeinscription=\'8\' AND mouvement.sexe=\'F\' AND mouvement.date_tableau=\'%2$s\'',
            DB_PREFIXE,
            $this->f->getParameter("datetableau")
        );


        // communes 
        $sqlcom1 = sprintf(
            'SELECT om_collectivite.om_collectivite as id, om_collectivite.libelle as ville, (%2$s) as totalm, (%3$s) as totalf,  (%4$s) as totalm8, (%5$s) as totalf8 FROM %1$som_collectivite',
            DB_PREFIXE,
            $sql_cpt_ins_homme_exc_office,
            $sql_cpt_ins_femme_exc_office,
            $sql_cpt_ins_homme_inc_office,
            $sql_cpt_ins_femme_inc_office
        );
        if ($this->f->isMulti() != true) {
            $sqlcom1 .= " WHERE om_collectivite.libelle='".$this->f->collectivite['ville']."' ";
        } else {
            $sqlcom1 .= " WHERE om_collectivite.niveau='1' ";
        }
        $sqlcom1.=" ORDER BY om_collectivite.libelle ";

        //===========================================================================



        // Communes
        $commune = array();

        $rescom =& $this->f->db->query($sqlcom1);
        $this->f->isDatabaseError($rescom);

        while ($rowcom =& $rescom->fetchrow(DB_FETCHMODE_ASSOC))
            array_push ($commune, $rowcom);


        $pdf=new FPDF('L','mm','A4');
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(true);
        $pdf->SetFont('courier','',11);
        $pdf->SetDrawColor(30,7,146);
        $pdf->SetMargins(5,5,5);
        $pdf->SetDisplayMode('real','single');


        foreach ($commune as $c)
        {

                $pdf->addpage();
                // entete
            $pdf->SetFont('courier','',11);
            $pdf->Cell(190,5, iconv(HTTPCHARSET,"CP1252",__("COMMUNE      : ").$c['ville']),0,0,'L',0);
            $pdf->Cell(95,5, iconv(HTTPCHARSET,"CP1252",__("EDITEE LE : ").$aujourdhui),0,1,'R',0);
            $pdf->Cell(190,5, iconv(HTTPCHARSET,"CP1252",__("DATE TABLEAU : ").$this->f->formatDate($this->f->getParameter("datetableau"))),0,1,'L',0);
            $pdf->Cell(0,10,"",0,1,'L',0);
                $pdf->SetFont('courier','B',11);
                $pdf->Cell(190,5,iconv(HTTPCHARSET,"CP1252",__("STATISTIQUES INSCRIPTIONS D'OFFICE ET INSCRIPTIONS PAR SEXE ")),0,1,'C',0);
                $pdf->SetFont('courier','',11);
                $pdf->ln();
                $pdf->ln();
                $pdf->ln();
            // Tableau
            $pdf->Cell(80,7,"",0,0,'L',0);
            $pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",__("Inscription d'office (Code 8)")),1,0,'C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",__("Autres inscriptions")),1,0,'C',0);
            $pdf->Cell(40,7,iconv(HTTPCHARSET,"CP1252",__("Total")),1,1,'C',0);
            // Calcul totaux
            $totaux_homme = $c['totalm'] + $c['totalm8'];
            $totaux_femme = $c['totalf'] + $c['totalf8'];
            $totaux_autres = $c['totalm'] + $c['totalf'];
            $totaux_8 = $c['totalm8'] + $c['totalf8'];
            $totaux = $totaux_8 + $totaux_autres;
            // Affichage
            $pdf->Cell(30,7,"",0,0,'L',0);
            $pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",__("Hommes")),1,0,'C',0);
            $pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",$c['totalm8']),1,0,'C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$c['totalm']),1,0,'C',0);
            $pdf->Cell(40,7,iconv(HTTPCHARSET,"CP1252",$totaux_homme),1,1,'C',0);
            $pdf->Cell(30,7,"",0,0,'L',0);
            $pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",__("Femmes")),1,0,'C',0);
            $pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",$c['totalf8']),1,0,'C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$c['totalf']),1,0,'C',0);
            $pdf->Cell(40,7,iconv(HTTPCHARSET,"CP1252",$totaux_femme),1,1,'C',0);
            $pdf->Cell(30,7,"",0,0,'L',0);
            $pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",__("Total")),1,0,'C',0);
            $pdf->Cell(80,7,iconv(HTTPCHARSET,"CP1252",$totaux_8),1,0,'C',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",$totaux_autres),1,0,'C',0);
            $pdf->Cell(40,7,iconv(HTTPCHARSET,"CP1252",$totaux),1,1,'C',0);
        }

        /**
         * OUTPUT
         */
        //
        $filename = "statistiques_inscriptions-".date('Ymd-His').".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
