<?php
/**
 * Ce script contient la définition de la classe *agent*.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/agent.class.php";

/**
 * Définition de la classe *agent* (om_dbform).
 */
class agent extends agent_gen {

    public function __construct($id, $db, $debug) {
        $this->constructeur($id, $db, $debug);
    }

    public function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //libelle des champs
        $form->setLib('agent',__('matricule agent'));
    }

    public function setOnchange(&$form, $maj){
        parent::setOnchange($form, $maj);
        $form->setOnchange("nom","this.value=this.value.toUpperCase()");
        $form->setOnchange("ville","this.value=this.value.toUpperCase()");
    }
}
