<?php
/**
 * Ce script contient la définition de la classe *poste*.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/poste.class.php";

/**
 * Définition de la classe *poste* (om_dbform).
 */
class poste extends poste_gen {

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $crud = $this->get_action_crud($maj);
        if ($maj == 0 || $maj == 1) {
            $form->setType("nature", "select");
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    public function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // nature
        $contenu=array();
        $contenu[0]=array('candidature', 'affectation', );
        $contenu[1]=array(__("candidature d'agent"), __("affectation d'élu"), );
        $form->setSelect("nature",$contenu);
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    public function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        $form->setOnchange("poste", "this.value=this.value.toUpperCase()");
    }

    /**
     * CHECK_TREATMENT - cleSecondaire.
     *
     * @return void
     */
    public function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        parent::cleSecondaire($id);
        // verification de la suppression
        if(in_array($id, array(
            'PRESIDENT', 'PRESIDENT SUPPLEANT',
            'SECRETAIRE',
            'AGENT CENTRALISATION',
            'ASSESSEUR SUPPLEANT', 'ASSESSEUR TITULAIRE',
            'DELEGUE SUPPLEANT', 'DELEGUE TITULAIRE'))) {
            $this->msg .= sprintf(__("Vous ne pouvez pas supprimer le poste '%s' (utilisé dans les traitements)"), $id);
            $this->correct=false;
        }
    }
}
