<?php
/**
 * Ce script définit la classe 'reuSyncLEREUNumeroOrdreTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_l_e.class.php";

/**
 * Définition de la classe 'reuSyncLEREUNumeroOrdreTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLEREUNumeroOrdreTraitement extends reuSyncLETraitement {

    var $fichier = "reu_sync_l_e_reu_numero_ordre";
    /**
     *
     */
    function treatment() {
        set_time_limit(180);
        $this->LogToFile("begin ".$this->fichier);
        //
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done") {
            $this->error = true;
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }
        
        $ret = $this->reload_reu_sync_listes();
        if ($ret !== true) {
            return false;
        }
        $sql = sprintf(
            'UPDATE %1$selecteur SET numero_bureau = null WHERE electeur.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true) !== false) {
            $this->error = true;
            $this->LogToFile("=> ECHEC - Erreur de base de données");
            $this->LogToFile("end ".$this->fichier);
            return false;
        }
        $sql = sprintf(
            'UPDATE %1$selecteur 
            SET numero_bureau = (
                SELECT 
                    numero_d_ordre_dans_le_bureau_de_vote::bigint
                FROM 
                    %1$sreu_sync_listes 
                    INNER JOIN %1$sliste ON reu_sync_listes.code_type_liste=liste.liste_insee
                    INNER JOIN %1$sbureau ON reu_sync_listes.id_bureau_de_vote::integer=bureau.referentiel_id
                WHERE 
                    electeur.om_collectivite=%2$s
                    AND electeur.liste=liste.liste
                    AND electeur.bureau=bureau.id
                    AND electeur.ine=reu_sync_listes.numero_d_electeur::integer 
                    AND reu_sync_listes.code_ugle=\'%3$s\'
            )
            WHERE electeur.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $inst_reu->get_ugle()
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true) !== false) {
            $this->error = true;
            $this->LogToFile("=> ECHEC - Erreur de base de données");
            $this->LogToFile("end ".$this->fichier);
            return false;
        }

        $this->LogToFile("Le traitement a été effectué avec succès.");
        $this->LogToFile("end ".$this->fichier);
        return true;
    }
}
