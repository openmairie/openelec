<?php
/**
 * Ce script définit la classe 'edition_pdf__liste_electorale'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__liste_electorale' (edition_pdf).
 */
class edition_pdf__liste_electorale extends edition_pdf {

    /**
     * Édition PDF - Liste électorale.
     *
     * > mode_edition
     *  - "parbureau"
     *    > bureau_code
     *  - "commune"
     *
     * @return array
     */
    public function compute_pdf__liste_electorale($params = array()) {
        //
        $libelle_commune = $this->f->collectivite["ville"];
        $liste = $_SESSION["liste"];
        $libelle_liste = $_SESSION["libelle_liste"];
        $titre_libelle_ligne1 = __("LISTE ÉLECTORALE");
        $titre_libelle_ligne2 = "";
        $bureau_libelle = "";
        $canton_libelle = "";
        $circonscription_libelle = "";
        $message = "";
        $tour = "les_deux";
        $libelle_scrutin = "";
        //
        $mode_edition = "";
        if (isset($params["mode_edition"])) {
            if ($params["mode_edition"] == "parbureau") {
                $mode_edition = "parbureau";
                if (array_key_exists("liste", $params) === true) {
                    $liste = $params["liste"];
                    $ret = $this->f->get_one_result_from_db_query(sprintf(
                        'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                        DB_PREFIXE,
                        $this->f->db->escapesimple($liste)
                    ));
                    $libelle_liste = $ret["result"];
                }
                if (isset($params["bureau_code"])) {
                    $bureau_code = $params["bureau_code"];
                    $message = sprintf(
                        __("%s\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),
                        $titre_libelle_ligne1,
                        $libelle_liste,
                        $bureau_code
                    );
                    $sql = sprintf(
                        'SELECT bureau.id FROM %1$sbureau WHERE bureau.om_collectivite=%2$s AND bureau.code=\'%3$s\'',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        $this->f->db->escapesimple($bureau_code)
                    );
                    $bureau_id = $this->f->db->getone($sql);
                    $this->f->isDatabaseError($bureau_id);
                    //
                    $inst_bureau = $this->f->get_inst__om_dbform(array(
                        "obj" => "bureau",
                        "idx" => $bureau_id,
                    ));
                    $bureau_libelle = $inst_bureau->getVal("libelle");
                    //
                    $inst_canton = $this->f->get_inst__om_dbform(array(
                        "obj" => "canton",
                        "idx" => $inst_bureau->getVal("canton"),
                    ));
                    $canton_libelle = $inst_canton->getVal("libelle");
                    //
                    $inst_circonscription = $this->f->get_inst__om_dbform(array(
                        "obj" => "circonscription",
                        "idx" => $inst_bureau->getVal("circonscription"),
                    ));
                    $circonscription_libelle = $inst_circonscription->getVal("libelle");
                }
                if (array_key_exists("scrutin", $params) === true) {
                    $titre_libelle_ligne2 = sprintf(
                        '(%s)(%s)(%s)',
                        $params["scrutin"],
                        $params["scrutin_demande_id"],
                        $params["scrutin_livrable_date"]
                    );
                    //
                    if (isset($params["tour"])) {
                        $tour = $params["tour"];
                    }
                    if ($params["scrutin_tour1"] == $params["scrutin_tour2"]) {
                        $tour = "un";
                        $libelle_scrutin = sprintf(
                            '%1$s du %2$s',
                            $params["scrutin_libelle"],
                            $this->f->formatdate($params["scrutin_tour1"])
                        );
                    } else {
                        $libelle_scrutin = sprintf(
                            '%1$s des %2$s et %3$s%4$s',
                            $params["scrutin_libelle"],
                            $this->f->formatdate($params["scrutin_tour1"]),
                            $this->f->formatdate($params["scrutin_tour2"]),
                            ($tour !== "les_deux" ? sprintf(
                                ' (Tour %s)',
                                ($tour === "un" ? "1" : "2")
                            ) : "")
                        );
                    }
                }
            } elseif ($params["mode_edition"] == "commune") {
                $mode_edition = "commune";
                $bureau_code = "ALL";
                if (array_key_exists("liste", $params) === true) {
                    $liste = $params["liste"];
                    $ret = $this->f->get_one_result_from_db_query(sprintf(
                        'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                        DB_PREFIXE,
                        $this->f->db->escapesimple($liste)
                    ));
                    $libelle_liste = $ret["result"];
                }
                if (array_key_exists("scrutin", $params) === true) {
                    $titre_libelle_ligne2 = sprintf(
                        '(%s)(%s)(%s)',
                        $params["scrutin"],
                        $params["scrutin_demande_id"],
                        $params["scrutin_livrable_date"]
                    );
                    //
                    if (isset($params["tour"])) {
                        $tour = $params["tour"];
                    }
                    if ($params["scrutin_tour1"] == $params["scrutin_tour2"]) {
                        $tour = "un";
                        $libelle_scrutin = sprintf(
                            '%1$s du %2$s',
                            $params["scrutin_libelle"],
                            $this->f->formatdate($params["scrutin_tour1"])
                        );
                    } else {
                        $libelle_scrutin = sprintf(
                            '%1$s des %2$s et %3$s%4$s',
                            $params["scrutin_libelle"],
                            $this->f->formatdate($params["scrutin_tour1"]),
                            $this->f->formatdate($params["scrutin_tour2"]),
                            ($tour !== "les_deux" ? sprintf(
                                ' (Tour %s)',
                                ($tour === "un" ? "1" : "2")
                            ) : "")
                        );
                    }
                }
                if (array_key_exists("demande", $params) === true) {
                    $titre_libelle_ligne2 = sprintf(
                        '(Liste année sans scrutin)(%s)(%s)',
                        $params["demande"],
                        $params["demande_livrable_date"]
                    );
                    $libelle_scrutin = "Année sans scrutin";
                }
            }
        }

        /**
         *
         */
        $sql = sprintf(
            'SELECT 
                (electeur.nom || \' - \' || electeur.prenom) as nomprenom, 
                nom_usage, 
                to_char(date_naissance,\'DD/MM/YYYY\') as naissance, 
                (substring(libelle_lieu_de_naissance FROM 0 for 44) || \' (\' || code_departement_naissance || \')\') as lieu, 
                case liste 
                    when \'01\' then \'\' 
                    else libelle_nationalite 
                end as nationalite, 
                case resident 
                    when \'Non\' then (
                        case numero_habitation > 0 
                            when True then (numero_habitation || \' \' || complement_numero || \' \' || electeur.libelle_voie) 
                            when False then electeur.libelle_voie 
                        end) 
                    when \'Oui\' then adresse_resident 
                end as adresse, 
                case resident 
                    when \'Non\' then electeur.complement 
                    when \'Oui\' then (complement_resident || \' \' || cp_resident || \' - \' || ville_resident) 
                end as complement, 
                \'\' as blank2, 
                \'\' as blank3, 
                bureau.code as bureau_code, 
                numero_bureau 
            FROM
                %1$selecteur
                LEFT JOIN %1$snationalite ON electeur.code_nationalite=nationalite.code
                LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
            WHERE
                electeur.liste=\'%3$s\'
                AND electeur.om_collectivite=%2$s
                %4$s
            ORDER BY
                withoutaccent(lower(electeur.nom)),
                withoutaccent(lower(electeur.prenom)) 
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $liste,
            ($mode_edition === "parbureau" && isset($bureau_code) ? "AND bureau.code='".$this->f->db->escapesimple($bureau_code)."'" : "")
        );
        /**
         *
         */
        if (array_key_exists("scrutin", $params) === true) {
            $sql = sprintf(
                'SELECT 
                    CASE WHEN electeur.id IS NULL
                        THEN (\'*\' || reu_livrable.nom_de_naissance || \' - \' || reu_livrable.prenoms)
                        ELSE (electeur.nom || \' - \' || electeur.prenom)
                    END AS nomprenom,
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.nom_d_usage
                        ELSE electeur.nom_usage
                    END AS nom_usage,
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.date_de_naissance
                        ELSE to_char(electeur.date_naissance,\'DD/MM/YYYY\')
                    END AS naissance,
                    CASE WHEN electeur.id IS NULL
                        THEN concat(substring(reu_livrable.commune_de_naissance FROM 0 for 44), \' (\', 
                        CASE
                            WHEN reu_livrable.pays_de_naissance = \'FRANCE\' THEN reu_livrable.code_de_departement_de_naissance
                            ELSE reu_livrable.pays_de_naissance
                        END
                       , \')\')
                        ELSE (substring(libelle_lieu_de_naissance FROM 0 for 44) || \' (\' || code_departement_naissance || \')\')
                    END AS lieu, 
                    CASE WHEN electeur.id IS NULL
                        THEN
                            CASE reu_livrable.liste
                                WHEN \'01\' then \'\'
                                ELSE reu_livrable.libelle_de_la_nationalite
                            END
                        ELSE
                            case electeur.liste 
                                when \'01\' then \'\' 
                                else nationalite.libelle_nationalite 
                            end
                    END AS nationalite, 
                    CASE WHEN electeur.id IS NULL
                        THEN CONCAT_WS(\' \', reu_livrable.numero_de_voie, reu_livrable.libelle_de_voie)
                        ELSE
                            case resident 
                                when \'Non\' then (
                                    case numero_habitation > 0 
                                        when True then (numero_habitation || \' \' || complement_numero || \' \' || electeur.libelle_voie) 
                                        when False then electeur.libelle_voie 
                                    end) 
                                when \'Oui\' then adresse_resident 
                            end
                    END AS adresse,
                    CASE WHEN electeur.id IS NULL
                        THEN CONCAT_WS(\' \', reu_livrable.complement_1, reu_livrable.complement_2, reu_livrable.lieu_dit, reu_livrable.code_postal, reu_livrable.commune, reu_livrable.pays)
                        ELSE
                            case resident 
                                when \'Non\' then electeur.complement 
                                when \'Oui\' then (complement_resident || \' \' || cp_resident || \' - \' || ville_resident) 
                            end
                    END as complement, 
                    \'\' as blank2,
                    \'\' as blank3,
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.code_du_bureau_de_vote
                        ELSE bureau.code
                    END AS bureau_code, 
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.numero_d_ordre_dans_le_bureau_de_vote
                        ELSE electeur.numero_bureau::text
                    END AS numero_bureau 
                FROM
                    %1$sreu_livrable
                    LEFT JOIN (%1$selecteur
                        LEFT JOIN %1$snationalite ON electeur.code_nationalite=nationalite.code
                        LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
                    ) ON reu_livrable.electeur_id=electeur.id
                        AND reu_livrable.numero_d_ordre_dans_le_bureau_de_vote::integer=electeur.numero_bureau
                        AND reu_livrable.code_du_bureau_de_vote::integer=bureau.code::integer
                WHERE
                    reu_livrable.om_collectivite=%2$s
                    AND reu_livrable.scrutin_id=%5$s
                    AND reu_livrable.liste=\'%3$s\'
                    %4$s
                    %6$s
                ORDER BY
                    withoutaccent(lower(reu_livrable.nom_de_naissance)),
                    withoutaccent(lower(electeur.nom)),
                    withoutaccent(lower(reu_livrable.prenoms)),
                    withoutaccent(lower(electeur.prenom))
                ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $liste,
                ($mode_edition === "parbureau" && isset($bureau_code) ? "AND reu_livrable.code_du_bureau_de_vote='".$this->f->db->escapesimple($bureau_code)."'" : ""),
                $params["scrutin"],
                (isset($params["scrutin_demande_id"]) === true
                    && $params["scrutin_demande_id"] !== null
                    && $params["scrutin_demande_id"] !== ''
                    ? sprintf('AND reu_livrable.demande_id=%s', $params["scrutin_demande_id"])
                    : ''
                )
            );
        } elseif (array_key_exists("demande", $params) === true) {
            $sql = sprintf(
                'SELECT 
                    CASE WHEN electeur.id IS NULL
                        THEN (\'*\' || reu_livrable.nom_de_naissance || \' - \' || reu_livrable.prenoms)
                        ELSE (electeur.nom || \' - \' || electeur.prenom)
                    END AS nomprenom,
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.nom_d_usage
                        ELSE electeur.nom_usage
                    END AS nom_usage,
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.date_de_naissance
                        ELSE to_char(electeur.date_naissance,\'DD/MM/YYYY\')
                    END AS naissance,
                    CASE WHEN electeur.id IS NULL
                        THEN concat(substring(reu_livrable.commune_de_naissance FROM 0 for 44), \' (\', 
                        CASE
                            WHEN reu_livrable.pays_de_naissance = \'FRANCE\' THEN reu_livrable.code_de_departement_de_naissance
                            ELSE reu_livrable.pays_de_naissance
                        END
                       , \')\')
                        ELSE (substring(libelle_lieu_de_naissance FROM 0 for 44) || \' (\' || code_departement_naissance || \')\')
                    END AS lieu, 
                    CASE WHEN electeur.id IS NULL
                        THEN
                            CASE reu_livrable.liste
                                WHEN \'01\' then \'\'
                                ELSE reu_livrable.libelle_de_la_nationalite
                            END
                        ELSE
                            case electeur.liste 
                                when \'01\' then \'\' 
                                else nationalite.libelle_nationalite 
                            end
                    END AS nationalite, 
                    CASE WHEN electeur.id IS NULL
                        THEN CONCAT_WS(\' \', reu_livrable.numero_de_voie, reu_livrable.libelle_de_voie)
                        ELSE
                            case resident 
                                when \'Non\' then (
                                    case numero_habitation > 0 
                                        when True then (numero_habitation || \' \' || complement_numero || \' \' || electeur.libelle_voie) 
                                        when False then electeur.libelle_voie 
                                    end) 
                                when \'Oui\' then adresse_resident 
                            end
                    END AS adresse,
                    CASE WHEN electeur.id IS NULL
                        THEN CONCAT_WS(\' \', reu_livrable.complement_1, reu_livrable.complement_2, reu_livrable.lieu_dit, reu_livrable.code_postal, reu_livrable.commune, reu_livrable.pays)
                        ELSE
                            case resident 
                                when \'Non\' then electeur.complement 
                                when \'Oui\' then (complement_resident || \' \' || cp_resident || \' - \' || ville_resident) 
                            end
                    END as complement, 
                    \'\' as blank2,
                    \'\' as blank3,
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.code_du_bureau_de_vote
                        ELSE bureau.code
                    END AS bureau_code, 
                    CASE WHEN electeur.id IS NULL
                        THEN reu_livrable.numero_d_ordre_dans_le_bureau_de_vote
                        ELSE electeur.numero_bureau::text
                    END AS numero_bureau 
                FROM
                    %1$sreu_livrable
                    LEFT JOIN (%1$selecteur
                        LEFT JOIN %1$snationalite ON electeur.code_nationalite=nationalite.code
                        LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
                    ) ON reu_livrable.electeur_id=electeur.id
                        AND reu_livrable.numero_d_ordre_dans_le_bureau_de_vote::integer=electeur.numero_bureau
                        AND reu_livrable.code_du_bureau_de_vote::integer=bureau.code::integer
                WHERE
                    reu_livrable.om_collectivite=%2$s
                    AND reu_livrable.liste=\'%3$s\'
                    AND reu_livrable.demande_id=%4$s
                ORDER BY
                    withoutaccent(lower(reu_livrable.nom_de_naissance)),
                    withoutaccent(lower(electeur.nom)),
                    withoutaccent(lower(reu_livrable.prenoms)),
                    withoutaccent(lower(electeur.prenom))
                ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $liste,
                $params["demande"]
            );
        }

        /**
         *
         */
        require_once "../obj/fpdf_table.class.php";
        $pdf = new PDF('L', 'mm', 'A4');
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(false);
        // Fixe la police utilisée pour imprimer les chaînes de caractères
        $pdf->SetFont(
            'Arial', // Famille de la police.
            '', // Style de la police.
            7 // Taille de la police en points.
        );
        // Fixe la couleur pour toutes les opérations de tracé
        $pdf->SetDrawColor(30, 7, 146);
        // Fixe les marges
        $pdf->SetMargins(
            10, // Marge gauche.
            10, // Marge haute.
            10 // Marge droite.
        );
        $pdf->SetDisplayMode('real', 'single');
        $pdf->AddPage();
        // COMMON
        $param = array(
            7, // Titre : Taille de la police en points.
            7, // Entete : Taille de la police en points.
            7, // Taille de la police en points.
            1, //
            1, //
            10, // Nombre maximum d'enregistrements par page pour la première page
            10, // Nombre maximum d'enregistrements par page pour les autres pages
            10, // Position de départ - X - La valeur de l'abscisse.
            10, // Position de départ - Y - La valeur de l'ordonnée.
            0, //
            0, //
            "display_lastpage_even_if_table_is_empty" => true,
            "display_table_even_if_table_is_empty" => true,
        );
        $page_content_width = 277;
        // Variable de developpement pour afficher les bordures de toutes les cellules
        $allborders = false;
        //-------------------------------------------------------------------//
        // TITRE (PAGE HEADER)
        $page_header = $this->get_page_header_config(array(
            "allborders" => $allborders,
            "page_content_width" => $page_content_width,
            "titre_libelle_ligne1" => $titre_libelle_ligne1,
            "titre_libelle_ligne2" => $titre_libelle_ligne2,
            "libelle_commune" => $libelle_commune,
            "libelle_liste" => $libelle_scrutin." - ".$libelle_liste,
            "bureau_code" => $bureau_code,
            "bureau_libelle" => $bureau_libelle,
            "canton_libelle" => $canton_libelle,
            "circonscription_libelle" => $circonscription_libelle,
            "page_header_line_height" => 4,
        ));
        //-------------------------------------------------------------------//
        // ENTETE (TABLE HEADER)
        $entete_height = 8;
        $entete = array(
            array(
                __("Nom Patronymique, Prénoms"),
                125, $entete_height-4, 2, ($allborders == true ? 1 : "LRT"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                sprintf(__("Nom d'Usage, Date et lieu de naissance%s"), ($liste != "01" ? __(", Nationalité"): "")),
                125, $entete_height-4, 0, ($allborders == true ? 1 : "LRB"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __("Adresse"),
                90, $entete_height, 0, ($allborders == true ? 1 : 1), 'C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
            array(
                __("Bureau"),
                15, $entete_height-4, 2, ($allborders == true ? 1 : "LRT"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __("No Ordre"),
                15, $entete_height-4, 0, ($allborders == true ? 1 : "LRB"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __("Observation(s)"),
                45, $entete_height, 0, ($allborders == true ? 1 : 1), 'C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
        );
        //-------------------------------------------------------------------//
        // FIRSTPAGE
        $firstpage = array_merge(
            $page_header,
            array(
                array(
                    " ",
                    $page_content_width, 20, 1, ($allborders == true ? 1 : "B"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(
                    " ",
                    $page_content_width, 1, 1, ($allborders == true ? 1 : "T"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(
                    $titre_libelle_ligne1,
                    $page_content_width, 10, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',18,1,0),
                array(
                    "",
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                array(
                    "",
                    $page_content_width, 5, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                array(
                    sprintf(__("Commune : %s"), $libelle_commune),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    sprintf(__("Liste : %s"), $libelle_liste),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    ($bureau_code != "ALL" ? sprintf(__("Bureau : %s - %s"), $bureau_code, $bureau_libelle) : ""),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    ($canton_libelle != "" ? sprintf(__("Canton : %s"), $canton_libelle) : ""),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    ($circonscription_libelle != "" ? sprintf(__("Circonscription : %s"), $circonscription_libelle) : ""),
                    $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                array(
                    " ",
                    $page_content_width, 10, 1, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
            )
        );
        //-------------------------------------------------------------------//
        // LASTPAGE
        $lastpage_height = 9;
        $lastpage = array_merge(
            $page_header,
            array(
                array(
                    " ",
                    $page_content_width, 10, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
                array(
                    __(" *  *  *  A R R Ê T É  *  *  *"),
                    $page_content_width, 10, 2, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',8,1,0),
                array(
                    __("LA LISTE ÉLECTORALE AU NOMBRE DE")." ",
                    160, 10, 0, ($allborders == true ? 1 : 0), 'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(
                    '<LIGNE>',
                    13, 10, 0, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',9,1,0),
                array(
                    " ".__("ÉLECTEURS."),
                    25, 10, 2, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(
                    sprintf(__('%s , le %s'), $libelle_commune, date("d/m/Y")),
                    75, 10, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(50,0),0,'','NB',0,1,0),
                array(
                    ' ',
                    $page_content_width, 50, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
                array(
                    "",
                    $page_content_width/3, 10, 0, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',8,1,0),
                array(
                    "",
                    $page_content_width/3, 10, 0, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
                array(
                    "",
                    $page_content_width/3, 10, 0, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',8,1,0),
            )
        );

        //
        $line_height = 4;
        //
        $col = array(
            //
            'nomprenom' => array(
                125,
                $line_height,
                2,
                'LRT', // Border
                'L', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 0),
                ' ',
                '',
                'CELLSUP_NON',
                '0',
                'NA',
                'NC',
                '',
                'B',
                9,
                array('0'),
            ),
            //
            'nom_usage' => array(
                125,
                $line_height,
                2,
                'LR', // Border
                'L', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 0),
                '       -    ',
                '',
                'CELLSUP_NON',
                0,
                'NA',
                'CONDITION',
                array('NN'),
                'NB',
                0,
                array('0'),
            ),
            //
            'naissance' => array(
                31,
                $line_height,
                0,
                'L', // Border
                'L', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 0),
                __('     Ne(e) le '),
                '',
                'CELLSUP_NON',
                '0',
                'NA',
                'NC',
                '',
                'NB',
                0,
                array('0'),
            ),
            //
            'lieu' => array(
                94,
                $line_height,
                1,
                'R', // Border
                'L', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 0),
                __(' a  '),
                '',
                'CELLSUP_NON',
                '0',
                'NA',
                'NC',
                '',
                'NB',
                0,
                array('0'),
            ),
            //
            'nationalite' => array(
                125,
                $line_height,
                0,
                'LRB', // Border
                'L', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 0),
                '     ',
                '',
                'CELLSUP_NON',
                0,
                'NA',
                'CONDITION',
                array('NN'),
                'NB',
                0,
                array('0'),
            ),
            //
            'adresse' => array(
                90,
                $line_height,
                2,
                'LRT', // Border
                'L', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 12),
                '',
                '',
                'CELLSUP_NON',
                0,
                'NA',
                'NC',
                '',
                'NB',
                0,
                array('0'),
            ),
            //
            'complement' => array(
                90,
                $line_height,
                2,
                'LR', // Border
                'L', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 0),
                '',
                '',
                'CELLSUP_NON',
                0,
                0,
                'NA',
                'NC',
                '',
                'NB',
                0,
                array('0'),
            ),
            //
            'blank2' => array(
                90,
                $line_height,
                2,
                'LR', // Border
                'L', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 0),
                '',
                '',
                'CELLSUP_NON',
                0,
                0,
                'NA',
                'NC',
                '',
                'NB',
                0,
                array('0'),
            ),
            //
            'blank3' => array(
                90,
                $line_height,
                0,
                'LRB', // Border
                'L', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 0),
                '',
                '',
                'CELLSUP_NON',
                0,
                0,
                'NA',
                'NC',
                '',
                'NB',
                0,
                array('0'),
            ),
            //
            'bureau_code' => array(
                15,
                $line_height*2,
                2,
                'LRT', // Border
                'R', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 12),
                '',
                '',
                'CELLSUP_NON',
                '0',
                'NA',
                'NC',
                '',
                'NB',
                0,
                array('0'),
            ),
            //
            'numero_bureau' => array(
                15,
                $line_height*2,
                0,
                'LRB', // Border
                'R', // Align
                '0', '0', '0', // Text Color
                '255', '255', '255', // Fill Color
                array(0, 0),
                '',
                '',
                'CELLSUP_VIDE',
                array(
                    45,
                    $line_height*4,
                    0,
                    '1', // Border
                    'C', // Align
                    '0', '0', '0', // Text Color
                    '255', '255', '255', // Fill Color
                    array(0, 8),
                ),
                'NA',
                'NC',
                '',
                'NB',
                0,
                array('0'),
            )
        );
        //-------------------------------------------------------------------//
        $enpied = array();
        //-------------------------------------------------------------------//
        $pdf->Table(
            $page_header,
            $entete,
            $col,
            $enpied,
            $sql,
            $this->f->db,
            $param,
            $lastpage,
            $firstpage
        );

        /**
         * Affichage d'un bloc spécifique si aucun enregistrement sélectionné.
         */
        if ($pdf->msg==1 and $message!='') {
            $pdf->SetFont('Arial','',10);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetFillColor(213,8,26);
            $pdf->SetTextColor(255,255,255);
            $pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message), ($allborders == true ? 1 : 1),'C',1);
        }

        /**
         * OUTPUT
         */
        //
        $filename = date("Ymd-His");
        $filename .= "-listeelectorale";
        $filename .= "-".$_SESSION["collectivite"];
        $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
        $filename .= "-liste".$liste;
        if ($mode_edition === "parbureau") {
            $filename .= "-bureau".$bureau_code;
        }
        if (array_key_exists("scrutin", $params) === true) {
            $filename .= "-scrutin".$params["scrutin"];
        }
        $filename .= ".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
