<?php
/**
 * Ce script définit la classe 'module_carteretour'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_carteretour' (module).
 */
class module_carteretour extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_carteretour",
            "title" => __("Traitement")." -> ".__("Module Carte en retour"),
            "description" => __("Les cartes en retour peuvent être gérées par le logiciel ".
                "pour répertorier la liste des cartes d'électeurs retournées ".
                "par la poste. Vous pouvez aussi modifier manuellement les ".
                "informations de cartes en retour d'un électeur depuis sa fiche électeur ".
                "menu \"Consultation -> Liste Électorale\"."),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_carteretour-onglet_saisie_par_lot",
                    "href" => "../app/index.php?module=module_carteretour&view=carteretour",
                    "title" => __("Saisie par lots"),
                    "view" => "carteretour",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_carteretour-onglet_listing",
                    "href" => OM_ROUTE_SOUSTAB."&obj=electeur_carteretour&amp;retourformulaire=module_carteretour&amp;idxformulaire=module_carteretour",
                    "title" => __("Liste des électeurs"),
                ),
                array(
                    "type" => "tab",
                    "right" => "bureau_edition",
                    "href" => "../app/index.php?module=module_carteretour&view=editions",
                    "title" => __("Éditions"),
                    "view" => "editions",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_carteretour-onglet_epuration",
                    "href" => "../app/index.php?module=module_carteretour&view=epuration",
                    "title" => __("Épuration"),
                    "view" => "epuration",
                ),
            ),
        );
    }

    /**
     *
     */
    function view__carteretour() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_carteretour");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ". __("Saisie de la carte en retour");
        $this->f->displaySubTitle($subtitle);
        //
        printf('
        <script type="text/javascript">
        function trt_form_trigger_succes() {
            giveFocus();
        }
        </script>
        ');
        //
        require_once "../obj/traitement.carteretour.class.php";
        $trt = new carteretourTraitement();
        $trt->displayForm();
        //
        printf(
            '<script type="text/javascript">
            function giveFocus(){
                $("#id_elec").val("");
                $("#id_elec").focus();
            }
            giveFocus();
            </script>'
        );
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }

    function view__epuration() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_carteretour");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        //
        $description = __("L'épuration signifit la suppression des enregistrements ".
                         "dans la base de données. Ce traitement doit être réalisé ".
                         "avec précaution.");
        $this->f->displayDescription($description);

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Épuration des cartes en retour");
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.carteretour_epuration.class.php";
        $trt = new carteretourEpurationTraitement();
        $trt->displayForm();
        //
        echo "</form>\n";
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }

    /**
     * Vue de l'onglet edition du module carte en retour
     *
     * @return void
     */
    function view__editions() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_carteretour");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        $description = __("Cet onglet vous permet d'accéder aux différentes éditions du module carte en retour.");
        $this->f->displayDescription($description);

        ob_start();
        $this->view_form_edition_carte_retour();
        $edition = ob_get_clean();
        $description = __('Registre de toutes les cartes en retour, dont les électeurs sont rattachés à la liste et au bureau de vote sélectionnés, triés par ordre alphabétique de nom d\'électeur.');

        printf(
            '<div class="container-fluid" id="module-carte-retour-main-tab">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                Édition par bureau de vote
                            </div>
                            <div class="card-body">
                                <p>%1$s</p>
                                %2$s
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="visualClear"></div>
        </form>
    </div>',
            $description,
            $edition
        );
    }

    /**
     * Vue du bloc permettant de télécharger l'édition des cartes en retour
     * par liste et par bureau.
     *
     * @return void
     */
    protected function view_form_edition_carte_retour() {
        // Mise en place de l'action permettant de récupérer l'édition
        $this->f->layout->display__form_container__begin(array(
            "action" => "../app/index.php?module=form&obj=electeur&action=301&mode_edition=parbureau",
            "id" => "carte_retour_par_bureau",
            "target" => "_blank",
        ));
        // Paramétrage des champs de sélection du bureau et de la liste
        $champs = array("liste", "bureau");
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
        ));
        // Paramétrage du select de la Liste
        $form->setLib("liste", __("Liste"));
        $form->setType("liste", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__listes() as $key => $liste) {
            array_push($contenu[0], $liste["liste"]);
            array_push($contenu[1], $liste["libelle_liste"]);
        }
        $form->setSelect("liste", $contenu);
        // Paramétrage du select du Bureau de vote
        $form->setLib("bureau", __("Bureau de vote"));
        $form->setType("bureau", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $key => $bureau) {
            array_push($contenu[0], $bureau["code"]);
            array_push($contenu[1], $bureau["code"]." ".$bureau["libelle"]);
        }
        $form->setSelect("bureau", $contenu);
        // Fermeture et affichage du formulaire
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        // Affichage du bouton de téléchargement de l'édition
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "carte_retour_par_bureau.submit",
            "value" => __("Télécharger"),
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }

}
