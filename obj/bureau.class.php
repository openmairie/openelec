<?php
/**
 * Ce script définit la classe 'bureau'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/bureau.class.php";

/**
 * Définition de la classe 'bureau' (om_dbform).
 */
class bureau extends bureau_gen {

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "referentiel_id",
            "code",
            "libelle",
            "canton",
            "circonscription",
            "adresse_numero_voie",
            "adresse_libelle_voie",
            "adresse_complement1",
            "adresse_complement2",
            "adresse_lieu_dit",
            "adresse_code_postal",
            "adresse_ville",
            "adresse_pays",
            "surcharge_adresse_carte_electorale",
            "adresse1",
            "adresse2",
            "adresse3",
            "om_collectivite",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton() {
        return sprintf(
            'SELECT id, (code||\' \'|| libelle) AS lib FROM %1$scanton ORDER BY code',
            DB_PREFIXE
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton_by_id() {
        return sprintf(
            'SELECT id, (code||\' \'|| libelle) AS lib FROM %1$scanton WHERE id=\'<idx>\'',
            DB_PREFIXE
        );
    }

    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Verification de la cle secondaire : decoupage
        $this->rechercheTable($this->f->db, "decoupage", "bureau", $id);
        // Verification de la cle secondaire : electeur
        $this->rechercheTable($this->f->db, "electeur", "bureau", $id);
        // Verification de la cle secondaire : mouvement
        $this->rechercheTable($this->f->db, "mouvement", "bureau", $id);
        // On supprime manuellement les éléments liés dans numerobureau
        // avec le trigger
        // // Verification de la cle secondaire : numerobureau
        // $this->rechercheTable($this->f->db, "numerobureau", "bureau", $id);

        // verification de la suppression
        if(isset($val['code']) && in_array($val['code'], array('T', 'C'))) {
            $this->msg .= sprintf(__("SUPPRESSION NON AUTORISEE pour le bureau '%s' (utilisé dans les traitements)"), $id);
            $this->correct=false;
        }
    }

    /**
     *
     */
    function get_bureau_id_from_code($bureau_code) {
        $ret = $this->f->get_one_result_from_db_query(sprintf(
            'SELECT 
                bureau.id
            FROM 
                %1$sbureau
            WHERE 
                bureau.om_collectivite=%2$s
                AND bureau.code::integer=%3$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            intval($bureau_code)
        ));
        return $ret["result"];
    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        $form->setLib("adresse_numero_voie", __("numéro voie"));
        $form->setLib("adresse_libelle_voie", __("libellé voie"));
        $form->setLib("adresse_complement1", __("complément 1"));
        $form->setLib("adresse_complement2", __("complément 2"));
        $form->setLib("adresse_lieu_dit", __("lieu dit"));
        $form->setLib("adresse_code_postal", __("code postal"));
        $form->setLib("adresse_ville", __("ville"));
        $form->setLib("adresse_pays", __("pays"));
        $form->setLib("surcharge_adresse_carte_electorale", __("surcharge activée ?"));
        $form->setLib("adresse1", __("surcharge (ligne 1)"));
        $form->setLib("adresse2", __("surcharge (ligne 2)"));
        $form->setLib("adresse3", __("surcharge (ligne 3)"));
    }

    /**
     *
     */
    function setLayout(&$form, $maj) {
        parent::setLayout($form, $maj);
        $form->setBloc("adresse_numero_voie", "D", __("adresse"));
        $form->setBloc("adresse_pays", "F");
        $form->setBloc("surcharge_adresse_carte_electorale", "D", __("surcharge adresse carte électorale"));
        $form->setBloc("adresse3", "F");
    }
    /**
     *
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);
        if ($maj == 0 || $maj == 1) {
            $form->setType("referentiel_id", "hiddenstatic");
        }
    }

    function setMax(&$form, $maj) {
        parent::setMax ($form, $maj);
        $form->setMax('libelle', 40);
    }

    function setTaille(&$form, $maj) {
        parent::setTaille ($form, $maj);
        $form->setTaille('libelle', 40);
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($this->creerNumeroBureauAutomatique($id, $this->valF["om_collectivite"]) !== true) {
            return false;
        }
        if ($this->updateParametrageNbJures($this->valF["om_collectivite"]) !== true) {
            return false;
        }
        //
        if ($this->f->getParameter("reu_sync_l_e_init") !== ""
            && (array_key_exists("origin", $val) === false || $val["origin"] != "from_reu")) {
            // Transmission au REU du nouveau bureau
            $inst_canton = $this->f->get_inst__om_dbform(array(
                "obj" => "canton",
                "idx" => $this->valF["canton"],
            ));
            $inst_circonscription = $this->f->get_inst__om_dbform(array(
                "obj" => "circonscription",
                "idx" => $this->valF["circonscription"],
            ));
            $inst_reu = $this->f->get_inst__reu();
            $datas = array(
                "code" => $this->valF["code"],
                "libelle" => $this->valF["libelle"],
                "adresse" => array(
                    "numero_voie" => $this->valF["adresse_numero_voie"],
                    "libelle_voie" => $this->valF["adresse_libelle_voie"],
                    "complement1" => $this->valF["adresse_complement1"],
                    "complement2" => $this->valF["adresse_complement2"],
                    "lieu_dit" => $this->valF["adresse_lieu_dit"],
                    "code_postal" => $this->valF["adresse_code_postal"],
                    "ville" => $this->valF["adresse_ville"],
                    "pays" => $this->valF["adresse_pays"],
                ),
                "canton" => $inst_canton->getVal("code"),
                "circonscription" => $inst_circonscription->getVal("code"),
            );
            $ret = $inst_reu->handle_bureau_de_vote(array(
                "mode" => "add",
                "id" => $this->getVal("referentiel_id"),
                "datas" => $datas,
            ));
            if (isset($ret["code"]) !== true
                || $ret["code"] != 201) {
                $this->addToMessage("Erreur de transmission au REU.");
                if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                    $this->addToMessage($ret["result"]["message"]);
                }
                if ($ret["code"] == 401) {
                    if ($inst_reu->is_connexion_standard_valid() === false) {
                        $this->addToMessage(sprintf(
                            '%s : %s',
                            __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                            sprintf(
                                '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                __("cliquez ici pour vérifier")
                            )
                        ));
                    }
                }
                return $this->end_treatment(__METHOD__, false);
            }
            $valF = array(
                "referentiel_id" => trim($ret["headers"]["X-App-params"]),
            );
            $ret = $this->update_autoexecute($valF, $id);
            if ($ret !== true) {
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $this->addToMessage("Transmission des informations au REU OK.");
        }
        return true;
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($this->updateParametrageNbJures($this->valF["om_collectivite"]) !== true) {
            return false;
        }
        //
        if ($this->getVal("referentiel_id") !== ""
            && (array_key_exists("origin", $val) === false || $val["origin"] != "from_reu")) {
            // Modification du bureau dans le REU
            $inst_canton = $this->f->get_inst__om_dbform(array(
                "obj" => "canton",
                "idx" => $this->valF["canton"],
            ));
            $inst_circonscription = $this->f->get_inst__om_dbform(array(
                "obj" => "circonscription",
                "idx" => $this->valF["circonscription"],
            ));
            $inst_reu = $this->f->get_inst__reu();
            $datas = array(
                "code" => $this->valF["code"],
                "libelle" => $this->valF["libelle"],
                "adresse" => array(
                    "numero_voie" => $this->valF["adresse_numero_voie"],
                    "libelle_voie" => $this->valF["adresse_libelle_voie"],
                    "complement1" => $this->valF["adresse_complement1"],
                    "complement2" => $this->valF["adresse_complement2"],
                    "lieu_dit" => $this->valF["adresse_lieu_dit"],
                    "code_postal" => $this->valF["adresse_code_postal"],
                    "ville" => $this->valF["adresse_ville"],
                    "pays" => $this->valF["adresse_pays"],
                ),
                "canton" => $inst_canton->getVal("code"),
                "circonscription" => $inst_circonscription->getVal("code"),
            );
            $ret = $inst_reu->handle_bureau_de_vote(array(
                "mode" => "edit",
                "id" => $this->getVal("referentiel_id"),
                "datas" => $datas,
            ));
            if (isset($ret["code"]) !== true
                || $ret["code"] != 201) {
                $this->addToMessage("Erreur de transmission au REU.");
                if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                    $this->addToMessage($ret["result"]["message"]);
                }
                if ($ret["code"] == 401) {
                    if ($inst_reu->is_connexion_standard_valid() === false) {
                        $this->addToMessage(sprintf(
                            '%s : %s',
                            __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                            sprintf(
                                '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                __("cliquez ici pour vérifier")
                            )
                        ));
                    }
                }
                return false;
            }
            //
            $this->addToMessage("Transmission des informations au REU OK.");
        }
        return true;
    }

    /**
     * TRIGGER - triggersupprimer.
     *
     * @return boolean
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($this->supprimerNumeroBureauAutomatique($id) !== true) {
            return false;
        }
        return true;
    }

    /**
     * TRIGGER - triggersupprimerapres.
     *
     * @return boolean
     */
    function triggersupprimerapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($this->updateParametrageNbJures($this->getVal("om_collectivite")) !== true) {
            return false;
        }
        //
        if ($this->getVal("referentiel_id") !== "") {
            // Suppression du bureau dans le REU
            $inst_reu = $this->f->get_inst__reu();
            $ret = $inst_reu->handle_bureau_de_vote(array(
                "mode" => "delete",
                "id" => $this->getVal("referentiel_id"),
            ));
            if (isset($ret["code"]) !== true
                || $ret["code"] != 200) {
                $this->addToMessage("Erreur de transmission au REU.");
                if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                    $this->addToMessage($ret["result"]["message"]);
                }
                if ($ret["code"] == 401) {
                    if ($inst_reu->is_connexion_standard_valid() === false) {
                        $this->addToMessage(sprintf(
                            '%s : %s',
                            __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                            sprintf(
                                '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                __("cliquez ici pour vérifier")
                            )
                        ));
                    }
                }
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $this->addToMessage("Suppression du bureau de vote dans le REU OK.");
        }
        return true;
    }

    /**
     * ...
     *
     * @return boolean
     */
    function supprimerParametrageNbJures($canton_id, $collectivite_id) {
        $query = sprintf(
            'DELETE FROM %1$sparametrage_nb_jures WHERE parametrage_nb_jures.om_collectivite=%2$s AND parametrage_nb_jures.canton=\'%3$s\'',
            DB_PREFIXE,
            intval($collectivite_id),
            $canton_id
        );
        $res = $this->f->db->query($query);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true) === true) {
            return false;
        }
        $this->addToLog(
            __METHOD__."(): ".$this->f->db->affectedRows()." enregistrement(s) supprime(s)",
            EXTRA_VERBOSE_MODE
        );
        return true;
    }

    /**
     * Permet de mettre à jour le paramétrage (canton) du tirage au sort des jury.
     *
     * @return boolean
     */
    function updateParametrageNbJures($collectivite_id) {
        // Requête de vérification de des canton de bureau
        $query = sprintf(
            'SELECT DISTINCT canton.id FROM %1$sbureau INNER JOIN %1$scanton ON bureau.canton=canton.id WHERE bureau.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($collectivite_id)
        );
        $res_list_canton = $this->f->db->query($query);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_list_canton, true) === true) {
            return false;
        }
        //
        $list_canton_bureau=array();
        while ($row = $res_list_canton->fetchRow()) {
            $list_canton_bureau[] = $row[0];
        }
        // Requête de vérification de des canton de paramétrage de jury
        $query = sprintf(
            'SELECT DISTINCT parametrage_nb_jures.canton FROM %1$sparametrage_nb_jures WHERE parametrage_nb_jures.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($collectivite_id)
        );
        $res_list_canton = $this->f->db->query($query);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_list_canton, true) === true) {
            return false;
        }
        //
        $list_canton_jury=array();
        while ($row = $res_list_canton->fetchRow()) {
            $list_canton_jury[] = $row[0];
        }
        // Comparaison entre les cantons utilisés dans le paramétrage des bureaux
        // et le paramétrage du jury
        foreach (array_diff($list_canton_bureau, $list_canton_jury) as $canton_id) {
            if ($this->creerParametrageNbJures($canton_id, $collectivite_id) !== true) {
                return false;
            }
        }
        foreach (array_diff($list_canton_jury, $list_canton_bureau) as $canton_id) {
            if ($this->supprimerParametrageNbJures($canton_id, $collectivite_id) !== true) {
                return false;
            }
        }
        return true;
    }

    /**
     * ...
     *
     * @return boolean
     */
    function creerParametrageNbJures($canton_id, $collectivite_id) {
        //
        $valF = array();
        $valF["canton"] = intval($canton_id);
        $valF["om_collectivite"] = intval($collectivite_id);
        //
        $res = $this->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, "parametrage_nb_jures"),
            $valF,
            DB_AUTOQUERY_INSERT
        );
        $this->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "parametrage_nb_jures")."\", ".print_r($valF, true).", DB_AUTOQUERY_INSERT);",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true) === true) {
            return false;
        }
        //
        $this->addToLog(
            "creerParametrageNbJures(): ".$this->f->db->affectedRows().
                " enregistrement(s) ajoute(s)",
            EXTRA_VERBOSE_MODE
        );
        return true;
    }

    /**
     * ...
     *
     * @return boolean
     */
    function supprimerNumeroBureauAutomatique($id) {
        //
        $sql = sprintf(
            'DELETE FROM %1$snumerobureau WHERE numerobureau.bureau=%2$s',
            DB_PREFIXE,
            intval($id)
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true) === true) {
            return false;
        }
        //
        $this->addToMessage("".$this->f->db->affectedRows()." enregistrement(s) de la table numerobureau supprime(s) en lien avec le bureau ".$id."");
        return true;
    }

    /**
     * ...
     *
     * @return boolean
     */
    function creerNumeroBureauAutomatique($id, $collectivite_id) {
        // On recupere toutes les listes
        $sql = sprintf(
            'SELECT * FROM %1$sliste WHERE liste.officielle IS TRUE',
            DB_PREFIXE
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true) === true) {
            return false;
        }
        // Chaque element du tableau $liste correspond a une liste
        $liste = array();
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            array_push($liste, $row);
        }
        // On boucle sur chaque liste pour ajouter une ligne par liste pour
        // ce bureau dans la table numerobureau
        $inst__numerobureau = $this->f->get_inst__om_dbform(array(
            "obj" => "numerobureau",
            "idx" => "]",
        ));
        $inst__numerobureau->setParameter("maj", 0);
        foreach ($liste as $l) {
            //
            $ret = $inst__numerobureau->ajouter(array(
                'id' => null,
                'bureau' => intval($id),
                'om_collectivite' => intval($collectivite_id),
                'liste' => $l['liste'],
                'dernier_numero' => 0,
                'dernier_numero_provisoire' => 9000,
            ));
            if ($ret !== true) {
                return false;
            }
            $this->addToMessage("Enregistrement ".$inst__numerobureau->valF['id']." de la table 'numerobureau'.");
        }
        return true;
    }
}
