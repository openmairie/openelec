<?php
/**
 * Ce script définit la classe 'archivageTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'archivageTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class archivageTraitement extends traitement {

    var $fichier = "archivage";

    function getValidButtonValue() {
        //
        return __("Archivage des mouvements");
    }

    function displayBeforeContentForm() {
        //
        $datetableau = $this->f->getParameter("datetableau");
        $datetableau_show = $this->f->formatDate($this->f->getParameter("datetableau"));
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_archivage.inc.php";
        // NB mouvements a archiver
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);
        $nbArchive = $res->numRows();
        $res->free();
        //
        echo "NOMBRE DE MOUVEMENTS A ARCHIVER a la date du ".$datetableau_show;
        echo " ".$nbArchive."";
    }

    function treatment () {
        //
        $this->LogToFile("start archivage");
        //
        $datetableau = $this->f->getParameter("datetableau");
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_archivage.inc.php";
        // TRAITEMENT ARCHIVAGE
        $this->LogToFile("TRAITEMENT ARCHIVAGE");
        $query_piece = $this->f->db->query($query_select_piece);
        if ($this->f->isDatabaseError($query_piece, true)) {
            //
            $this->error = true;
            //
            $message = $query_piece->getMessage()." - ".$query_piece->getUserInfo ();
            $this->LogToFile($message);
        } else {
            //
            $this->LogToFile($query_piece->numRows()." piece a supprimer");
            while ($row =& $query_piece -> fetchRow (DB_FETCHMODE_ASSOC)){
                $inst_piece = $this->f->get_inst__om_dbform(array(
                    "obj" => "piece",
                    "idx" =>$row['id'],
                ));
                $inst_piece->setParameter('maj', 2);
                $inst_piece->supprimer($row);
            }
        }
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo ();
            $this->LogToFile($message);
        } else {
            //
            $this->LogToFile($res->numRows()." mouvements a archiver");
            while ($row =& $res->fetchRow (DB_FETCHMODE_ASSOC)) {
                // ajout ARCHIVE
                $enr = $this->f->get_inst__om_dbform(array(
                    "obj" => "archive",
                    "idx" => "]",
                ));
                $enr->ajouterTraitement($row, $datetableau);
                //
                $message = "=> Mouvement: ".$row['id']." ".$row['nom']." ".$row['prenom']." ".$enr->msg;
                $this->LogToFile($message);
                // suppression MOUVEMENT
                include ("../sql/".OM_DB_PHPTYPE."/trt_archivage.inc.php");
                $res1 = $this->f->db->query($sql1);
                if ($this->f->isDatabaseError($res1, true)) {
                    //
                    $this->error = true;
                    //
                    $message = $res1->getMessage()." - ".$res1->getUserInfo();
                    $this->LogToFile($message);
                } else {
                    $message = "l'enregistrement ".$row['id']." de la table mouvement est detruit";
                    $this->LogToFile($message);
                }
            }
            $res->free();
        }
        //
        $this->LogToFile("end archivage");
    }

    /**
     * Statistiques - 'mouvements_pour_archivage'.
     *
     * 
     *
     * @return array
     */
    function compute_stats__mouvements_pour_archivage() {
        //
        $totalavant = 0;
        $totalactif = 0;
        $totaltrs = 0;
        $totalcnen = 0;
        $totalapres = 0;
        //
        $data = array();
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
            /**
             * Cette requete permet de compter tous les mouvements de la date de tableau
             * en cours en fonction de la collectivite en cours, de la liste en cours,
             * du bureau selectionne
             *
             * @param string $bureau['id'] Identifiant du bureau
             * @param string $_SESSION["collectivite"]
             * @param string $_SESSION["liste"]
             * @param string $this->f->getParameter("datetableau")
             */
            $query_count_mouvement = sprintf(
                'SELECT count(*) FROM %1$smouvement WHERE mouvement.liste=\'%3$s\' AND mouvement.om_collectivite=%2$s AND mouvement.bureau=%5$s AND mouvement.date_tableau=\'%4$s\'',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $_SESSION["liste"],
                $this->f->getParameter("datetableau"),
                intval($bureau["id"])
            );
            $res_count_mouvement = $this->f->db->getOne($query_count_mouvement);
            $this->f->isDatabaseError($res_count_mouvement);
            //
            $totalavant += $res_count_mouvement;
            /**
             * Cette requete permet de compter tous les mouvements de la date de tableau
             * en cours en fonction de la collectivite en cours, de la liste en cours,
             * du bureau selectionne et dans l'etat "actif"
             *
             * @param string $bureau['id'] Identifiant du bureau
             * @param string $_SESSION["collectivite"]
             * @param string $_SESSION["liste"]
             * @param string $this->f->getParameter("datetableau")
             */
            $query_count_mouvement_actif = $query_count_mouvement;
            $query_count_mouvement_actif .= " and mouvement.etat='actif' ";
            $res_count_mouvement_actif = $this->f->db->getOne($query_count_mouvement_actif);
            $this->f->isDatabaseError($res_count_mouvement_actif);
            //
            $totalactif += $res_count_mouvement_actif;
            /**
             * Cette requete permet de compter tous les mouvements de la date de tableau
             * en cours en fonction de la collectivite en cours, de la liste en cours,
             * du bureau selectionne et dans l'etat "trs"
             *
             * @param string $bureau['id'] Identifiant du bureau
             * @param string $_SESSION["collectivite"]
             * @param string $_SESSION["liste"]
             * @param string $this->f->getParameter("datetableau")
             */
            $query_count_mouvement_trs = $query_count_mouvement;
            $query_count_mouvement_trs .= " and mouvement.etat='trs' ";
            $res_count_mouvement_trs = $this->f->db->getOne($query_count_mouvement_trs);
            $this->f->isDatabaseError($res_count_mouvement_trs);
            //
            $totaltrs += $res_count_mouvement_trs;
            /**
             * Cette requete permet de compter tous les mouvements de la date de tableau
             * en cours en fonction de la collectivite en cours, de la liste en cours,
             * du bureau selectionne et dans l'etat "trs" ayant ete transmis a l'insee
             *
             * @param string $bureau['id'] Identifiant du bureau
             * @param string $_SESSION["collectivite"]
             * @param string $_SESSION["liste"]
             * @param string $this->f->getParameter("datetableau")
             */
            $query_count_mouvement_trs_cnen = $query_count_mouvement_trs;
            $query_count_mouvement_trs_cnen .= " and mouvement.envoi_cnen<>'' ";
            $res_count_mouvement_trs_cnen = $this->f->db->getOne($query_count_mouvement_trs_cnen);
            $this->f->isDatabaseError($res_count_mouvement_trs_cnen);
            //
            $totalcnen += $res_count_mouvement_trs_cnen;
            //
            $apres = $res_count_mouvement - $res_count_mouvement_trs;
            //
            $totalapres += $apres;
            //
            $datas = array(
                $bureau['code'],
                $bureau['libelle'],
                $res_count_mouvement,
                $res_count_mouvement_actif,
                $res_count_mouvement_trs,
                $res_count_mouvement_trs_cnen,
                $apres
            );
            $data[] = $datas;
        }
        //
        $data[] = array(
            "-",
            __("TOTAL"),
            $totalavant,
            $totalactif,
            $totaltrs,
            $totalcnen,
            $totalapres
        );
        // Array
        return array(
            "format" => "L",
            "title" => __("Statistiques"),
            "subtitle" => __("Statistiques des mouvements"),
            "offset" => array(10,0,30,30,30,0,30),
            "column" => array(
                "",
                __("LISTE")." : ".$_SESSION["libelle_liste"]."",
                __("Avant archivage"),
                __("Mouvements actifs"),
                __("Mouvements traites"),
                __("Mouvements traites transmis a l'INSEE"),
                __("Apres archivage")
            ),
            "data" => $data,
            "output" => "stats-mouvements_pour_archivage",
        );
    }
}
