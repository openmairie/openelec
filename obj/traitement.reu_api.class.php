<?php
/**
 * Ce script définit la classe 'reuApiTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'reuApiTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuApiTraitement extends traitement {
    var $generate_log_file = false;
    var $fichier = "reu_api";
    var $champs = array("elem", "mode", "id_elem");
    function setContentForm() {
        //
        $this->form->setLib("elem", __("Element"));
        $this->form->setType("elem", "select");
        $this->form->setSelect("elem", array(
            array("", "electeur", "inscription", "radiation", "bureaux", "procuration", ),
            array("", "electeur", "inscription", "radiation", "bureaux", "procuration", ),
        ));
        //
        $this->form->setLib("mode", __("Mode"));
        $this->form->setType("mode", "select");
        $this->form->setSelect("mode", array(
            array("", "get", "list", ),
            array("", "get", "list", ),
        ));
        //
        $this->form->setLib("id_elem", __("Id"));
        $this->form->setType("id_elem", "text");
        $this->form->setTaille("id_elem", 15);
        $this->form->setMax("id_elem", 15);
    }
    function treatment () {
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }
        if (in_array($this->f->get_submitted_post_value("elem"), array("electeur", "inscription", "radiation", "bureaux", "procuration", )) !== true) {
            $this->error = true;
            $this->addToMessage("Element obligatoire");
            return false;
        }
        if (in_array($this->f->get_submitted_post_value("mode"), array("get", "list", )) !== true) {
            $this->error = true;
            $this->addToMessage("Mode obligatoire");
            return false;
        }
        $mode = $this->f->get_submitted_post_value("mode");
        $method = "handle_".$this->f->get_submitted_post_value("elem");
        if ($this->f->get_submitted_post_value("elem") === "bureaux" && $mode === "list") {
            echo "<h2>bureaux - listes</h2>";
            $uid1 = $inst_reu->get_bureaux_de_vote_in_csv();
            $uid2 = $inst_reu->get_bureaux_de_vote_indicateurs_in_csv();
            if ($uid1 != null && $uid2 != null) {
                printf(
                    '<ul><li><a href="%s">%s</a></li><li><a href="%s">%s</a></li></ul>',
                    sprintf('../app/index.php?module=form&snippet=file&uid=%s&mode=temporary', $uid1),
                    $uid1,
                    sprintf('../app/index.php?module=form&snippet=file&uid=%s&mode=temporary', $uid2),
                    $uid2
                );
            } else {
                echo "<p>Problème de quota ?</p>";
            }
            return;
        }
        if ($mode == "get") {
            $key = "id";
            if ($method == "handle_electeur") {
                $key = "ine";
            }
            $ret = $inst_reu->$method(array(
                "mode" => $mode,
                $key => intval($this->f->get_submitted_post_value("id_elem")),
            ));
        } elseif ($mode == "list") {
            $ret = $inst_reu->$method(array(
                "mode" => $mode,
            ));
        }
        echo $method." ".$mode." ".intval($this->f->get_submitted_post_value("id_elem"));
        if ($mode === 'list' && $this->f->get_submitted_post_value("elem") === 'inscription' && $this->f->get_submitted_post_value("id_elem") === 'orphelin') {
            $ref_ext = array();
            $ref_oel = array();
            foreach ($ret as $values) {
                if (isset($values['result']) === true && is_array($values['result']) === true) {
                    foreach ($values['result'] as $value) {
                        if (isset($value['referenceExterne']) === true
                            && intval($value['referenceExterne']) !== 0
                            && ($value['etatCourant']['etat']['code'] === 'COMP'
                                || $value['etatCourant']['etat']['code'] === 'OUV')) {
                            if (isset($ref_ext[intval($value['referenceExterne'])]) === false) {
                                //
                                $ref_ext[intval($value['referenceExterne'])] = array();
                            }
                            array_push($ref_ext[intval($value['referenceExterne'])], intval($value['id']));
                            $ref_oel[] = intval($value['referenceExterne']);
                        }
                    }
                }
            }
            echo "<h2>Liste des identifiants des mouvements liés à une ou plusieurs demandes ouverte ou complétée</h2>";
            echo "<pre>";
            print_r($ref_ext);
            print_r(count($ref_ext));
            echo "</pre>";
            if (count($ref_oel) > 0) {
                $res = $this->f->get_all_results_from_db_query(
                    sprintf(
                        'SELECT id FROM %1$smouvement WHERE id IN (%2$s) AND date_tableau <>\'2019-01-10\'',
                        DB_PREFIXE,
                        implode($ref_oel, ',')
                    ),
                    true
                );
                foreach ($res['result'] as $value) {
                    unset($ref_ext[$value['id']]);
                }
                echo "<h2>Liste des identifiants des mouvements du tableau du 10/01/2019 liés à une ou plusieurs demandes ouverte ou complétée</h2>";
                echo "<pre>";
                print_r($ref_ext);
                print_r(count($ref_ext));
                echo "</pre>";
            }
        } else {
            echo "<pre>";
            print_r($ret);
            echo "</pre>";
        }
    }
}
