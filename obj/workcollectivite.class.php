<?php
/**
 * Ce script définit la classe 'workCollectivite'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_base.class.php";

/**
 * Définition de la classe 'workCollectivite' (om_base).
 */
class workCollectivite extends om_base {

    /**
     *
     */
    var $action = ".";

    /**
     *
     */
    var $collectivites = array ();

    /**
     * Constructeur.
     *
     * @return void
     */
    function __construct($action = ".") {
        //
        $this->init_om_application();
        //
        $this->action = $action;
        //
        $this->calculateAllCollectivites ();
    }

    function validateForm () {
        //
        if (isset ($_POST['changecollectivite_action_cancel'])) {
            $this->f->goToDashboard ();
        }
        //
        if (isset ($_POST['collectivite']) or isset ($_POST['changecollectivite_action_valid'])) {
            //
            if (!isset ($this->collectivites[$_POST['collectivite']])) {
                // message
                $message_class = "error";
                $message = __("Cette collectivite n'existe pas.");
                $this->f->addToMessage($message_class, $message);
            } else {
                //
                $_SESSION["collectivite"] = $_POST['collectivite'];
                $_SESSION["libelle_collectivite"] = $this->collectivites[$_POST['collectivite']]['libelle'];
                $this->f->getCollectivite();
                $_SESSION['multi_collectivite'] = $this->f->isMulti();
                //
                //Si la connexion au reu est faite on met à jour la connexion logicielle
                $inst_reu = $this->f->get_inst__reu();
                if ($inst_reu != null) {
                    $inst_reu->logout_connexion_logicielle();
                    $inst_reu->__destruct();
                    $inst_reu = $this->f->get_inst__reu(true);
                }
                //
                $sql = sprintf(
                    'SELECT niveau FROM %1$som_collectivite WHERE om_collectivite=%2$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"])
                );
                //
                $niveau = $this->f->db->getone($sql);
                $this->f->addToLog(
                    __METHOD__."(): db->getone(\"".$sql."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($niveau);
                $_SESSION["niveau"] = $niveau;
                // message
                $message_class = "ok";
                $message = __("Vous travaillez maintenant sur la collectivite : ").$_SESSION["collectivite"]." - ".$_SESSION["libelle_collectivite"].".";
                $this->f->addToMessage($message_class, $message);
            }
        }
    }

    function getCollectivites () {
        return $this->collectivites;
    }

    function countCollectivites () {
        return count($this->getCollectivites());
    }

    function calculateAllCollectivites () {
        //
        $sql = sprintf(
            'SELECT * FROM %1$som_collectivite ORDER BY libelle',
            DB_PREFIXE
        );
        //
        $res = $this->f->db->query($sql);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        while ($row =& $res->fetchRow (DB_FETCHMODE_ASSOC)) {
            $this->collectivites[$row['om_collectivite']] = array(
                "code" => $row['om_collectivite'],
                "libelle" => $row['libelle'],
            );
        }
    }


    function showForm ($full = true) {
        //
        echo "\n<div id=\"changecollectiviteform\" class=\"formulaire\">\n";

        //
        echo "<form method=\"post\" id=\"changecollectivite_form\" action=\"".$this->action."\">\n";

        //
        $tabindex = 1;

        //
        echo "\t<div class=\"field\">\n";
        //
        echo "\t\t<label for=\"collectivite\">";
        echo __("Selectionner ici la collectivite sur laquelle vous souhaitez travailler.");
        echo "</label>\n";
        //
        echo "\t\t<select name=\"collectivite\" tabindex=\"".$tabindex++."\" ";
        echo " class=\"champFormulaire\" >\n\t\t\t";
        foreach ($this->collectivites as $value) {
            echo "<option value=\"".$value['code']."\"";
            if ($_SESSION["collectivite"] == $value['code']) {
                echo " selected=\"selected\"";
            }
            echo ">".$value['libelle']."</option>";
        }
        echo "\n\t\t</select>\n";
        //
        echo "\t</div>\n";

        //
        echo "\t<div class=\"formControls\">\n";
        echo "\t\t<input name=\"changecollectivite.action.valid\" tabindex=\"".$tabindex++."\" value=\"".__("Valider")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
        if ($full == true) {
            echo "<a class=\"retour\" title=\"".__("Retour")."\" ";
            echo "href=\"".OM_ROUTE_DASHBOARD."\">";
            echo __("Retour");
            echo "</a>";
        }
        echo "\t</div>\n";

        //
        echo "</form>\n";

        //
        echo "</div>\n";
    }
}


