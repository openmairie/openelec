<?php
/**
 * Ce script contient la définition de la classe *candidature*.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/candidature.class.php";

/**
 * Définition de la classe *candidature* (om_dbform).
 */
class candidature extends candidature_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    public function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-candidature",
            "view" => "view_edition_pdf__candidature",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Affectation / Convocation"),
               "class" => "pdf-16",
               "order" => 40,
            ),
            "permission_suffix" => "edition_pdf__candidature",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);
        if ($maj == 0 || $maj == 1) {
            $form->setType("agent", "autocomplete");
        }
        //
        if ($this->is_in_context_of_foreign_key("composition_scrutin", $this->getParameter("retourformulaire"))) {
            $form->setType("composition_scrutin", "hidden");
        }
    }

    /**
     * SETTER_FORM - set_form_default_values (setVal).
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        if ($validation == 0 && $maj == 0) {
            $form->setVal("periode", "journee");
        }
    }

    /**
     * VIEW - view_edition_pdf__candidature.
     *
     * @return void
     */
    public function view_edition_pdf__candidature() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output("etat", 'candidature');
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'candidature-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * GETTER_FORMINC - sql_poste.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_poste() {
        return sprintf(
            'SELECT poste.poste, poste.libelle FROM %1$sposte WHERE poste.nature=\'candidature\' ORDER BY poste.libelle ASC',
            DB_PREFIXE
        );
    }

    /**
     * GETTER_FORMINC - sql_bureau.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return $this->get_common_var_sql_forminc__sql_bureau();
    }

    /**
     * GETTER_FORMINC - sql_bureau_by_id.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return $this->get_common_var_sql_forminc__sql_bureau_by_id();
    }

    /**
     * Configuration du widget de formulaire autocomplete du champ 'agent'.
     *
     * @return array
     */
    function get_widget_config__agent__autocomplete() {
        return array(
            "obj" => "agent",
            "table" => "agent",
            "identifiant" => "agent.id",
            "criteres" => array(
                "agent.nom" => __("Nom"),
                "agent.prenom" => __("Prénom"),
            ),
            "libelle" => array(
                "agent.nom",
                "agent.prenom",
            ),
            "jointures" => array(
            ),
            "droit_ajout" => false,
            "where" => "",
            "group_by" => array(
            ),
        );
    }

    /**
     * SETTER_TREATMENT - setValF (setValF).
     *
     * @return void
     */
    public function setvalF($val = array()) {
        parent::setvalF($val);
        if ($val['debut']=='') 
            unset($this->valF['debut']);
        if ($val['fin']=='') 
            unset($this->valF['fin']);
    }

    protected function getNumberOfCandidaturesForPoste($composition_scrutin, $poste, $bureau,
                                                       $id = null, $periode = null) {
        $sql = sprintf("
            SELECT
                COUNT(id)
            FROM
                ".DB_PREFIXE."candidature
            WHERE
                candidature.composition_scrutin = %s
                AND poste ='%s'
                AND bureau = %s
                AND decision IS True
            ",
            $composition_scrutin,
            $poste,
            $bureau
        );
        if(!empty($id)) { // maj == 1
            $sql .= "   AND id != ".$id."\n";
        }
        if (in_array($periode, array('matin', 'apres-midi'))) {
            $sql .= "   AND (periode = 'journee' OR periode = '".$periode."')";
        }
        return $this->f->db->getOne($sql);
    }

    protected function getNumberOfCandidaturesForAgent($agent, $date_scrutin,
                                                     $id = null, $periode = null) {
        $sql = sprintf("
            SELECT
                COUNT(candidature.id)
            FROM
                ".DB_PREFIXE."candidature
                    INNER JOIN ".DB_PREFIXE."composition_scrutin
                        ON candidature.composition_scrutin=composition_scrutin.id
            WHERE
                date_tour = '%s'
                AND agent = %s
                AND decision IS True
            ",
            $date_scrutin,
            $agent
        );
        if(!empty($id)) {
            $sql .= "    AND candidature.id != ".$id."\n";
        }
        if (in_array($periode, array('matin', 'apres-midi'))) {
            $sql .= "   AND (periode = 'journee' OR periode = '".$periode."')";
        }
        return $this->f->db->getOne($sql);
    }

    protected function getCantonFor($entity_table, $entity_id_value) {
        $sql = sprintf("
            SELECT
                C.id,
                C.code,
                C.libelle
            FROM
                ".DB_PREFIXE."%s AS E
                LEFT JOIN ".DB_PREFIXE."canton AS C ON C.id=E.canton
            WHERE
                E.id = %s
            ",
            $entity_table,
            $entity_id_value
        );
        $res = $this->f->db->query($sql);
        if (DB::isError($res)) {
            die($res->getMessage(). " => Echec  ".$sql);
        }
        else {
            $count = $res->numrows();
            if ($count > 1) {
                die($res->getMessage(). sprintf(
                    " => Il ne devrait y avoir qu'un seul canton pour $entity_table '%s' (non %d). ".
                    "Requête: ".$sql,
                    $entity_id_value,
                    $count
                ));
            }
        }
        return $res->fetchRow(DB_FETCHMODE_ASSOC);
    }

    /**
     * CHECK_TREATMENT - verifier.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);

        // calcul du temps passe et verification des dates saisies
        if ($val['debut']!="" and $val['fin']!="") {
            $heureD = explode(":", $val['debut']);
            $heureF = explode(":", $val['fin']);
            $heure = $heureF[0]-$heureD[0];
            $minute = $heureF[1]-$heureD[1];
            $total="00:00";
            if ($heure<0) {
                $this->correct=false;
                $this->msg .= __("Heure de fin INFERIEURE à l'heure de départ");
            }
            else {
                if ($minute<0) {
                    if ($heure>0) {
                        $heure=$heure-1;
                        $minute=$minute+60;
                        $total = $heure.":".$minute;
                    }
                    else {
                        $this->correct=false;
                        $this->msg .= __("Heure de fin INFERIEURE à l'heure de départ");
                    }
                }
                else {
                    $total = $heure.":".$minute;
                }
                $this->msg .= sprintf(__("Heure(s) effectuées %s:%s"), $heure, $minute);
            }
        }

        if ($this->correct) {

            // on ne peut pas affecter des heures si il n y a pas de decision d affectation
            if (($val['debut']!="" or $val['fin']!="") and $this->valF['decision']!='True' ) {
                $this->correct=false;
                $this->msg .= __("Décision affectation NON PRISE");
            }

            // récupération du code du bureau
            $bureau_code = null;
            if(!empty($this->valF['bureau'])) {
                $sql = sprintf("SELECT code FROM ".DB_PREFIXE."bureau WHERE id = %s",
                                $this->valF['bureau']);
                $bureau_code = $this->f->db->getOne($sql);
            }

            //   exception agent centralisation
            // - tous bureau
            // - possibilté plusieurs agents ayant le meme poste
            if($this->valF['poste'] != 'AGENT CENTRALISATION') { // sauf les agents centralisateurs

                // affectation obligatoire d un bureau
                if ($this->valF['decision'] == 'True' and $bureau_code == 'T') {
                    $this->correct = false;
                    $this->msg = $this->msg.sprintf(__(
                        "Choix du poste : un %s ne peut être affecté à tous les bureaux ".
                        "(seulement le poste %s le peut)"),
                        $this->valF['poste'],
                        'AGENT CENTRALISATION');
                }

                // verification si le poste est occupe par un autre agent
                if ($this->valF['decision'] == 'True' and $bureau_code != 'T') {
                    $number_of_candidatures = $this->getNumberOfCandidaturesForPoste(
                        $this->valF['composition_scrutin'],
                        $this->valF['poste'],
                        $this->valF['bureau'],
                        $this->valF['id'],
                        $this->valF['periode']
                    );
                    if ($number_of_candidatures) {
                        $this->correct = false;
                        $sql = sprintf("SELECT libelle FROM ".DB_PREFIXE."bureau WHERE id = %s",
                                    $this->valF['bureau']);
                        $bureau_libelle = $this->f->db->getOne($sql);
                        $sql = sprintf("SELECT libelle FROM ".DB_PREFIXE."composition_scrutin WHERE id = %s",
                                    $this->valF['composition_scrutin']);
                        $scrutin_libelle = $this->f->db->getOne($sql);
                        $this->msg .= sprintf(__(
                            "Il y a déjà %d %s pour le bureau %s à la période '%s' ".
                            "pour le scrutin '%s'"),
                            $number_of_candidatures,
                            $this->valF['poste'],
                            $bureau_libelle,
                            $this->valF['periode'],
                            $scrutin_libelle);
                    }
                }
            }

            // verification si l agent n occupe pas un poste dans la meme periode
            // il peut y avoir plusieurs elections en meme temps
            // non bloquant
            if ($this->valF['decision'] == 'True' and $bureau_code != 'T') {

                $sql = "SELECT date_tour FROM ".DB_PREFIXE."composition_scrutin WHERE id = '".$this->valF['composition_scrutin']."'";
                $date_scrutin = $this->f->db->getOne($sql);

                $number_of_candidatures = $this->getNumberOfCandidaturesForAgent(
                    $this->valF['agent'],
                    $date_scrutin,
                    $this->valF['id'],
                    $this->valF['periode']
                );
                if ($number_of_candidatures) {
                    $this->correct = false;
                    $sql = sprintf("SELECT nom||' '||prenom FROM ".DB_PREFIXE."agent WHERE id = %s",
                                   $this->valF['agent']);
                    $agent_nom = $this->f->db->getOne($sql);
                    $this->msg .= sprintf(__(
                        "Il y a déjà %d poste(s) occupé(s) pour l'agent %s ".
                        "durant la période '%s' le %s"),
                        $number_of_candidatures,
                        $agent_nom,
                        $this->valF['periode'],
                        $date_scrutin);
                }
            }

            // verification des bureaux Cas des cantonales
/*            if ($bureau_code != 'T') {
                $scrutin_canton = null; //$this->getCantonFor('reu_scrutin', $this->valF['scrutin']);
                if (!empty($scrutin_canton) and $scrutin_canton['code'] != 'T') {
                    $bureau_canton = $this->getCantonFor('bureau', $this->valF['bureau']);
                    if($scrutin_canton['id'] != $bureau_canton['id']) {
                        $this->correct = false;
                        $sql = sprintf("SELECT libelle FROM ".DB_PREFIXE."bureau WHERE id = %s",
                                       $this->valF['bureau']);
                        $bureau_libelle = $this->f->db->getOne($sql);
                        $sql = sprintf("SELECT libelle FROM ".DB_PREFIXE."composition_scrutin WHERE id = %s",
                                       $this->valF['composition_scrutin']);
                        $scrutin_libelle = $this->f->db->getOne($sql);
                        if (!empty($this->msg)) {
                            $this->msg .= "<br/>";
                        }
                        $this->msg .= sprintf(__(
                            "Le canton '%s' du bureau '%s' ne correspond pas ".
                            "au canton '%s' du scrutin '%s'"),
                            $bureau_canton['libelle'],
                            $bureau_libelle,
                            $scrutin_canton['libelle'],
                            $scrutin_libelle);
                    }
                }
            }*/
        }
    }

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    public function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        $form->setOnchange("debut", "ftime(this)");
        $form->setOnchange("fin", "ftime(this)");
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    public function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if ($maj == 0 || $maj == 1) {
            $form->setSelect(
                "agent",
                $this->get_widget_config__agent__autocomplete()
            );
        }
    }

    /**
     * SETTER_FORM - setLib.
     *
     * @return void
     */
    public function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        $form->setLib("debut", "heure debut (format :hh:mm)");
        $form->setLib("fin", "heure fin (format : hh:mm)");
    }

    /**
     * TRIGGER - triggerajouter.
     * 
     * @return boolean
     */
    public function triggerajouter($id, &$dnu1 = NULL, $val = array(), $dnu2 = NULL) {
        parent::triggerajouter($id, $dnu1, $val, $dnu2);
        $this->valF['note']="1ère demande : ".$val['poste'].' au bureau '.$val['bureau'].
            ' pendant  '.$val['periode'];
    }
}
