<?php
/**
 * Ce script contient la définition de la classe *composition_bureau*.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/composition_bureau.class.php";

/**
 * Définition de la classe *composition_bureau* (om_dbform).
 */
class composition_bureau extends composition_bureau_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * Surcharge de la fonction pour supprimer les actions:
     * - ajout
     * - modification
     * - suppression
     *
     * @return void
     */
    public function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[0] = null;
        $this->class_actions[1] = null;
        $this->class_actions[2] = null;
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-compo-bureau",
            "view" => "view_edition_pdf__compo_bureau",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Composition du bureau"),
               "class" => "pdf-16",
               "order" => 10,
            ),
            "permission_suffix" => "edition_pdf__compo_bureau",
        );
    }

    /**
     * VIEW - view_edition_pdf__compo_bureau.
     *
     * @return void
     */
    public function view_edition_pdf__compo_bureau() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output("etat", 'composition_bureau');
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'scrutin-compo-bureau-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }
}
