<?php
//$Id$ 
//gen openMairie le 29/04/2019 13:01

require_once "../gen/obj/reu_scrutin.class.php";

class reu_scrutin extends reu_scrutin_gen {
    /**
     *
     */
    function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[0] = null;
        $this->class_actions[1] = null;
        $this->class_actions[2] = null;
        // ACTION - 101 - sync_scrutin
        $this->class_actions[101] = array(
            "identifier" => "sync_scrutin",
            "method" => "sync_scrutin",
            "permission_suffix" => "synchroniser",
            "button" => __("synchroniser"),
            "condition" => array(
                "reu_sync_l_e_valid_is_done",
            ),
        );
        //
        $this->class_actions[201] = array(
            "identifier" => "demander_j20_livrable",
            "portlet" => array(
               "type" => "action-direct-with-confirmation",
               "libelle" => __("Demander l'arrêt des listes"),
               "class" => "demander_j20_livrable-16",
               "order" => 51,
            ),
            "method" => "demander_j20_livrable",
            "permission_suffix" => "demander_j20_livrable",
            "condition" => array(
                "is_j20_period_valid",
                "is_j20_not_already_done",

            ),
            "button" => __("demander_j20_livrable"),
        );
        //
        $this->class_actions[202] = array(
            "identifier" => "demander_j5_livrable",
            "portlet" => array(
               "type" => "action-direct-with-confirmation",
               "libelle" => __("Demander le livrable 'mouvements J-5'"),
               "class" => "demander_j5_livrable-16",
               "order" => 52,
            ),
            "method" => "demander_j5_livrable",
            "permission_suffix" => "demander_j5_livrable",
            "condition" => array(
                "is_j5_period_valid",
                "is_j5_not_already_asked",
            ),
            "button" => __("demander_j5_livrable"),
        );
        //
        $this->class_actions[203] = array(
            "identifier" => "demander_emarge_livrable",
            "portlet" => array(
               "type" => "action-direct-with-confirmation",
               "libelle" => __("Demander le livrable 'liste d'émargement'"),
               "class" => "demander_emarge_livrable-16",
               "order" => 53,
            ),
            "method" => "demander_emarge_livrable",
            "permission_suffix" => "demander_emarge_livrable",
            "condition" => array(
                "is_emarge_not_already_asked",
            ),
            "button" => __("demander_emarge_livrable"),
        );
        //
        $this->class_actions[302] = array(
            "identifier" => "edition-pdf-recapitulatif-mention",
            "view" => "view__edition_pdf__recapitulatif_mention",
            "permission_suffix" => "edition_pdf__recapitulatif_mention",
        );
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        $hidden_fields= array(
            "j5_livrable_demande_id",
            "j5_livrable_demande_date",
            "j5_livrable",
            "j5_livrable_date",
            "emarge_livrable_demande_id",
            "emarge_livrable_demande_date",
            "emarge_livrable",
            "emarge_livrable_date",
            "arrets_liste"
        );
        foreach ($hidden_fields as $key => $value) {
            $form->setType($value, "hidden");
        }
        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation" 
            || $maj == 101) {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            // $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    function demander_j20_livrable() {
        $inst_reu = $this->f->get_inst__reu();
        $ret = $inst_reu->handle_livrable(array(
            "mode" => "add",
            "type_livrable" => "COMJ-20",
            "datas" => array(
                "code" => "PDF",
                "scrutinId" => $this->getVal("referentiel_id"),
            ),
        ));
        if (isset($ret["code"]) !== true || $ret["code"] != 201) {
            $this->correct = false;
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $valF = array(
            "livrable_demande_id" => trim($ret["headers"]["X-App-params"]),
            "livrable_demande_date" => date("Y-m-d H:i:s"),
        );
        $arrets_liste = $this->get_arrets_liste();
        $ret = $arrets_liste->update_autoexecute($valF, $arrets_liste->getVal($arrets_liste->clePrimaire));
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Vos modifications ont bien ete enregistrees."));
        $this->addToMessage("La demande de livrable COMJ-20 a été transmise au REU.");
        return $this->end_treatment(__METHOD__, true);
    }

    function demander_j5_livrable() {
        $inst_reu = $this->f->get_inst__reu();
        $ret = $inst_reu->handle_livrable(array(
            "mode" => "add",
            "type_livrable" => "MOUVJ-5",
            "datas" => array(
                "code" => "PDF",
                "scrutinId" => $this->getVal("referentiel_id"),
            ),
        ));
        if (isset($ret["code"]) !== true || $ret["code"] != 201) {
            $this->correct = false;
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        if ($this->getVal("j5_livrable") != "") {
            $this->f->storage->delete($this->getVal("j5_livrable"));
        }
        // Vide la table des livrables si déjà populée
        if ($this->getVal('j5_livrable_demande_id') !== null
            && $this->getVal('j5_livrable_demande_id') !== '') {
            //
            $query = sprintf(
                'DELETE FROM %1$sreu_livrable WHERE demande_id=%2$s ',
                DB_PREFIXE,
                $this->getVal('j5_livrable_demande_id')
            );
            $res = $this->f->db->query($query);
            $this->addToLog(
                __METHOD__."(): db->query(\"".$query."\");",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res, true) === true) {
                $this->correct = false;
                return $this->end_treatment(__METHOD__, false);
            }
            $this->addToLog(
                __METHOD__."(): ".$this->f->db->affectedRows()." enregistrement(s) supprime(s)",
                EXTRA_VERBOSE_MODE
            );
        }
        $valF = array(
            "j5_livrable_demande_id" => trim($ret["headers"]["X-App-params"]),
            "j5_livrable_demande_date" => date("Y-m-d H:i:s"),
            "j5_livrable" => "",
            "j5_livrable_date" => null,
        );
        $ret = $this->update_autoexecute($valF, $this->getVal($this->clePrimaire));
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage("La demande de livrable MOUVJ-5 a été transmise au REU.");
        return $this->end_treatment(__METHOD__, true);
    }

    function demander_emarge_livrable() {
        $inst_reu = $this->f->get_inst__reu();
        $ret = $inst_reu->handle_livrable(array(
            "mode" => "add",
            "type_livrable" => "EMARGE",
            "datas" => array(
                "code" => "PDF",
                "scrutinId" => $this->getVal("referentiel_id"),
            ),
        ));
        if (isset($ret["code"]) !== true || $ret["code"] != 201) {
            $this->correct = false;
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        if ($this->getVal("emarge_livrable") != "") {
            $this->f->storage->delete($this->getVal("emarge_livrable"));
        }
        // Vide la table des livrables si déjà populée
        if ($this->getVal('emarge_livrable_demande_id') !== null
            && $this->getVal('emarge_livrable_demande_id') !== '') {
            //
            $query = sprintf(
                'DELETE FROM %1$sreu_livrable WHERE demande_id=%2$s ',
                DB_PREFIXE,
                $this->getVal('emarge_livrable_demande_id')
            );
            $res = $this->f->db->query($query);
            $this->addToLog(
                __METHOD__."(): db->query(\"".$query."\");",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res, true) === true) {
                $this->correct = false;
                return $this->end_treatment(__METHOD__, false);
            }
            $this->addToLog(
                __METHOD__."(): ".$this->f->db->affectedRows()." enregistrement(s) supprime(s)",
                EXTRA_VERBOSE_MODE
            );
        }
        $valF = array(
            "emarge_livrable_demande_id" => trim($ret["headers"]["X-App-params"]),
            "emarge_livrable_demande_date" => date("Y-m-d H:i:s"),
            "emarge_livrable" => "",
            "emarge_livrable_date" => null,
        );
        $ret = $this->update_autoexecute($valF, $this->getVal($this->clePrimaire), true);
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage("La demande de livrable EMARGE a été transmise au REU.");
        return $this->end_treatment(__METHOD__, true);
    }

    function add_new_arrets_liste($arrets_liste, $om_collectivite, $livrable_demande_id = null, $livrable_demande_date = null, $livrable = null, $livrable_date =  null) {
        $arrets_liste_val = array(
            'om_collectivite' => $om_collectivite,
            'arrets_liste' => $arrets_liste,
            'livrable_demande_id' => $livrable_demande_id,
            'livrable_demande_date' => $livrable_demande_date,
            'livrable' => $livrable,
            'livrable_date' => $livrable_date,
        );
        $res = $this->f->db->autoExecute(DB_PREFIXE."arrets_liste", $arrets_liste_val, DB_AUTOQUERY_INSERT);
        return $res;
    }

    /**
     * Récupère une instance d'arrêt des listes à partir de son identifiant
     * issus du scrutin.
     *
     * @return arrets_liste
     */
    function get_arrets_liste() {
        if (empty($this->get_arrets_liste)) {
            $this->arrets_liste = $this->f->get_inst__om_dbform(array(
                'obj' => 'arrets_liste',
                'idx' => $this->getVal('arrets_liste')
            ));
        }
        return $this->arrets_liste;
    }

    /**
     * CONDITION - reu_sync_l_e_valid_is_done
     *
     * @return boolean
     */
    function reu_sync_l_e_valid_is_done() {
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_j5_not_already_asked
     *
     * @return boolean
     */
    function is_j5_not_already_asked() {
        if ($this->getVal("j5_livrable_demande_id") != ""
            && $this->getVal("j5_livrable") == "") {
            //
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_emarge_not_already_asked
     *
     * @return boolean
     */
    function is_emarge_not_already_asked() {
        if ($this->getVal("emarge_livrable_demande_id") != ""
            && $this->getVal("emarge_livrable") == "") {
            //
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_j20_period_valid
     *
     * @return boolean
     */
    function is_j20_period_valid() {
        $date_tour1 = $this->getVal("date_tour1");
        $dtime = DateTime::createFromFormat('Y-m-d', $date_tour1);
        if (strtotime("-23 days", strtotime($dtime->format('Y-m-d'))) < time()
            && strtotime("-7 days", strtotime($dtime->format('Y-m-d'))) > time()) {
            //
            return true;
        } else {
            return false;
        }
    }

    /**
     * CONDITION - is_j20_not_already_done
     *
     * @return boolean
     */
    function is_j20_not_already_done() {
        $arrets_liste = $this->get_arrets_liste();
        return $arrets_liste->has_no_demande();
    }

    /**
     * CONDITION - is_j5_period_valid
     *
     * @return boolean
     */
    function is_j5_period_valid() {
        $date_tour1 = $this->getVal("date_tour1");
        $date_tour2 = $this->getVal("date_tour2");
        $dtime_tour1 = DateTime::createFromFormat('Y-m-d', $date_tour1);
        $dtime_tour2 = DateTime::createFromFormat('Y-m-d', $date_tour2);
        if (strtotime("-6 days", strtotime($dtime_tour1->format('Y-m-d'))) < time()
            && strtotime("-1 days", strtotime($dtime_tour2->format('Y-m-d'))) > time()) {
            //
            return true;
        } else {
            return false;
        }
    }


    /**
     *
     */
    function sync_scrutin() {
        if ($this->reu_sync_l_e_valid_is_done() !== true) {
            $this->addToMessage(__("Cette fonctionnalité n'est pas disponible si la synchronisation initiale de la liste électorale n'est pas effectuée."));
            return false;
        }
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->addToMessage(__("Problème de configuration de la connexion au REU."));
            return false;
        }
        if ($inst_reu->is_connexion_logicielle_valid() !== true) {
            $this->addToMessage(__("La connexion logicielle au REU n'est pas valide."));
            return false;
        }
        $params = array(
            'mode' => 'list'
        );
        $scrutins = $inst_reu->handle_scrutin($params);
        //On récupère les id des scrutins déjà présents
        $query_select_id = sprintf(
            'SELECT id, referentiel_id FROM %1$sreu_scrutin WHERE om_collectivite=\'%2$s\'',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->query($query_select_id);
        if ($this->f->isDatabaseError($res, true) !== false) {
            $this->correct = false;
            $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
            return false;
        }
        $Ids = array();
        //On met les id de scrutin dans un tableau
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $Ids[$row["id"]] = $row["referentiel_id"];
        }
        //
        foreach ($scrutins as $scrutin) {
            if ($scrutin["code"] != "200" && $scrutin["code"] != "206") {
                if ($scrutin["code"] == "204") {
                    $this->addToMessage(__("Aucun nouveau scrutin."));
                    return true;
                }
                $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                return false;
            }
            $inserted = 0;
            $updated = 0;
            foreach ($scrutin["result"] as $key => $value) {
                $this->addToMessage(var_export($value, true));
                //
                $canton = null;
                if (array_key_exists("canton", $value) === true) {
                    $code_canton = gavoe($value, array("canton", "code", ), "-1");
                    $id_canton = $this->f->get_one_result_from_db_query(sprintf(
                        'SELECT id FROM %1$scanton WHERE code=\'%2$s\'',
                        DB_PREFIXE,
                        $code_canton
                    ));
                    if ($id_canton["code"] === "OK" && $id_canton["result"] !== "" && $code_canton !== "NC") {
                        $canton = intval($id_canton["result"]);
                    }
                }
                //
                $circonscription_legislative = null;
                if (array_key_exists("circonscriptionElectionLegislative", $value) === true) {
                    $code_circonscription = gavoe($value, array("circonscriptionElectionLegislative", "code", ), "");
                    $id_circonscription = $this->f->get_one_result_from_db_query(sprintf(
                        'SELECT id FROM %1$scirconscription WHERE code=\'%2$s\'',
                        DB_PREFIXE,
                        $code_circonscription
                    ));
                    $this->addToMessage(var_export($code_circonscription, true));
                    if ($id_circonscription["code"] === "OK" && $id_circonscription["result"] !== ""  || $code_circonscription !== "NC") {
                        $circonscription_legislative = intval($id_circonscription["result"]);
                    }
                }
                //
                $circonscription_metropolitaine = null;
                //
                $dateTour1 = null;
                if (array_key_exists("tour1", $value) === true) {
                    $dtime = DateTime::createFromFormat('d/m/Y', $value["tour1"]);
                    if ($dtime === false) {
                        $this->correct = false;
                        $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                        return false;
                    }
                    $dateTour1 = $dtime->format('Y-m-d');
                }
                //
                $dateTour2 = null;
                if (array_key_exists("tour2", $value) === true) {
                    $dtime = DateTime::createFromFormat('d/m/Y', $value["tour2"]);
                    if ($dtime === false) {
                        $this->correct = false;
                        $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                        return false;
                    }
                    $dateTour2 = $dtime->format('Y-m-d');
                }
                //
                $dateDebut = null;
                if (array_key_exists("dateDebut", $value) === true) {
                    $dtime = DateTime::createFromFormat('d/m/Y', $value["dateDebut"]);
                    if ($dtime === false) {
                        $this->correct = false;
                        $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                        return false;
                    }
                    $dateDebut = $dtime->format('Y-m-d');
                }
                //
                $dateL30 = null;
                if (array_key_exists("dateL30", $value) === true) {
                    $dtime = DateTime::createFromFormat('d/m/Y', $value["dateL30"]);
                    if ($dtime === false) {
                        $this->correct = false;
                        $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                        return false;
                    }
                    $dateL30 = $dtime->format('Y-m-d');
                }
                //
                $dateFin = null;
                if (array_key_exists("dateFin", $value) === true) {
                    $dtime = DateTime::createFromFormat('d/m/Y', $value["dateFin"]);
                    if ($dtime === false) {
                        $this->correct = false;
                        $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                        return false;
                    }
                    $dateFin = $dtime->format('Y-m-d');
                }
                //
                $zone = "";
                if (array_key_exists("zoneGeoScrutins", $value)  === true
                    && is_array($value["zoneGeoScrutins"]) === true
                    && count($value["zoneGeoScrutins"]) > 0) {
                    //
                    foreach ($value["zoneGeoScrutins"] as $zonegeoscrutin) {
                        if (is_array($zonegeoscrutin) === true
                            && array_key_exists("libelle", $zonegeoscrutin) === true
                            && is_string($zonegeoscrutin["libelle"]) === true) {
                            //
                            $zone .= $zonegeoscrutin["libelle"].", ";
                        }
                    }
                    $zone = trim($zone);
                }
                // Si le scrutin n'existe pas, on l'insère
                if (in_array($value["id"], array_values($Ids)) !== true) {
                    // Création de l'arrêt des listes associés au scrutin
                    $next_arrets_liste = $this->f->db->nextId(DB_PREFIXE.'arrets_liste');
                    if ($this->add_new_arrets_liste($next_arrets_liste, intval($_SESSION["collectivite"])) === false) {
                        $this->f->addToLog('echec ajout', DEBUG_MODE);
                        $this->correct = false;
                        $this->addToMessage("Echec de la préparation de l'arrêt des listes associé au scrutin. Veuillez contacter votre administrateur.");
                        return $this->correct;
                    }
                    // Ajout du scrutin
                    $valF = array(
                        "id" => $this->f->db->nextId(DB_PREFIXE.$this->table),
                        "referentiel_id" => $value["id"],
                        "type" => (array_key_exists("typeScrutin", $value) && array_key_exists("code", $value["typeScrutin"]) ? $value["typeScrutin"]["code"] : ""),
                        "partiel" => $value["partiel"],
                        "canton" => $canton,
                        "circonscription_legislative" => $circonscription_legislative,
                        "circonscription_metropolitaine" => $circonscription_metropolitaine,
                        "libelle" => $value["libelle"],
                        "zone" => $zone,
                        "date_tour1" => $dateTour1,
                        "date_tour2" => $dateTour2,
                        "date_debut" => $dateDebut,
                        "date_l30" => $dateL30,
                        "date_fin" => $dateFin,
                        "om_collectivite" => intval($_SESSION["collectivite"]),
                        "arrets_liste" => $next_arrets_liste
                    );
                    $res = $this->f->db->autoExecute(DB_PREFIXE."reu_scrutin", $valF, DB_AUTOQUERY_INSERT);
                    // Logger
                    $this->f->addToLog(__METHOD__."(): db->autoExecute(\"".DB_PREFIXE."reu_scrutin\", ".print_r($valF, true).", DB_AUTOQUERY_INSERT);", VERBOSE_MODE);
                    if ($this->f->isDatabaseError($res, true) !== false) {
                        $this->correct = false;
                        $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                        return false;
                    }
                    if ($this->f->db->affectedRows() == 1) {
                        $inserted += 1;
                    }
                } else {
                    $id = array_search($value["id"], $Ids);
                    $valF = array(
                        "id" => $id,
                        "referentiel_id" => $value["id"],
                        "type" => (array_key_exists("typeScrutin", $value) && array_key_exists("code", $value["typeScrutin"]) ? $value["typeScrutin"]["code"] : ""),
                        "partiel" => $value["partiel"],
                        "canton" => $canton,
                        "circonscription_legislative" => $circonscription_legislative,
                        "circonscription_metropolitaine" => $circonscription_metropolitaine,
                        "libelle" => $value["libelle"],
                        "zone" => $zone,
                        "date_tour1" => $dateTour1,
                        "date_tour2" => $dateTour2,
                        "date_debut" => $dateDebut,
                        "date_l30" => $dateL30,
                        "date_fin" => $dateFin,
                    );
                    $res = $this->f->db->autoExecute(
                        DB_PREFIXE."reu_scrutin",
                        $valF,
                        DB_AUTOQUERY_UPDATE,
                        $this->getCle($id)
                    );
                    // Si le scrutin n'a pas d'arrêt des listes associés on l'ajoute et on l'intègre au valeur à mettre à jour
                    $reu_scrutin = $this->f->get_inst__om_dbform(array(
                        'obj' => 'reu_scrutin',
                        'idx' => $id
                    ));
                    if (empty($reu_scrutin->getVal('arrets_liste'))) {
                        $next_arrets_liste = $this->f->db->nextId(DB_PREFIXE.'arrets_liste');
                        if ($this->add_new_arrets_liste($next_arrets_liste, intval($_SESSION["collectivite"])) === false) {
                            $this->f->addToLog('echec ajout', DEBUG_MODE);
                            $this->correct = false;
                            $this->addToMessage("Echec de la préparation de l'arrêt des listes associé au scrutin. Veuillez contacter votre administrateur.");
                            return $this->correct;
                        }
                        $valF['arrets_liste'] = $next_arrets_liste;
                    }
                    // Logger
                    $this->f->addToLog(__METHOD__."(): db->autoExecute(\"".DB_PREFIXE."reu_scrutin\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($id)."\")", VERBOSE_MODE);
                    if ($this->f->isDatabaseError($res, true) !== false) {
                        $this->correct = false;
                        $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
                        return false;
                    }
                    if ($this->f->db->affectedRows() == 1) {
                        $updated += 1;
                    }
                }
            }
        }
        $this->addToMessage(
            sprintf(
                '%s scrutin(s) importé(s). %s scrutin(s) mis à jour.',
                $inserted,
                $updated
            )
        );
        return true;
    }

    function bloc_livrable_j20_content() {
        $j20_content = "La période n'est pas propice (J-23 > J-7) à la demande d'arrêt des listes.";
        $arrets_liste = $this->get_arrets_liste();
        if ($arrets_liste->getVal("livrable") != "") {
            $j20_content = sprintf(
                '<img src="../app/img/arrive.png" /> L\'arrêt des listes a été réalisé le %s. Fichiers produits : <a href="%s">Télécharger</a>',
                $arrets_liste->getVal("livrable_date"),
                sprintf(
                    '%s&snippet=file&uid=%s',
                    OM_ROUTE_FORM,
                    $arrets_liste->getVal("livrable")
                )
            );
        } elseif ($arrets_liste->getVal("livrable_demande_id") != "") {
            $j20_content = sprintf(
                '<img src="../app/img/nonarrive.png" /> L\'arrêt des listes a été demandé (%s) le %s. En attente du retour de l\'INSEE.',
                $arrets_liste->getVal("livrable_demande_id"),
                $arrets_liste->getVal("livrable_demande_date")
            );
        } elseif ($this->is_j20_period_valid() === true) {
            $j20_content = "La période est propice (J-23 > J-7) pour demander l'arrêt des listes.";
        }
        return $j20_content;
    }

    function bloc_livrable_j5_content() {
        $j5_content = "La période n'est pas propice (J-6 > J-1) à la demande du tableau des mouvements j-5.";
        if ($this->getVal("j5_livrable") != "") {
            $j5_content = sprintf(
                '<img src="../app/img/arrive.png" /> Le tableau des mouvements j-5 du %s est disponible ici : <a href="%s">Télécharger</a>',
                $this->getVal("j5_livrable_date"),
                sprintf(
                    '%s&snippet=file&uid=%s',
                    OM_ROUTE_FORM,
                    $this->getVal("j5_livrable")
                )
            );
        } elseif ($this->getVal("j5_livrable_demande_id") != "") {
            $j5_content = sprintf(
                '<img src="../app/img/nonarrive.png" /> Le tableau des mouvements j-5 a été demandé (%s) le %s. En attente du retour de l\'INSEE.',
                $this->getVal("j5_livrable_demande_id"),
                $this->getVal("j5_livrable_demande_date")
            );
        } elseif ($this->is_j5_period_valid() === true) {
            $j5_content = "La période est propice (J-6 > J-1) pour demander le tableau des mouvements j-5.";
        }
        return $j5_content;
    }

    function bloc_livrable_emarge_content() {
        $emarge_content = "";
        if ($this->getVal("emarge_livrable") != "") {
            $emarge_content = sprintf(
                '<img src="../app/img/arrive.png" /> Le livrable liste d\'émargement a été produit le %s : <a href="%s">Télécharger</a>',
                $this->getVal("emarge_livrable_date"),
                sprintf(
                    '%s&snippet=file&uid=%s',
                    OM_ROUTE_FORM,
                    $this->getVal("emarge_livrable")
                )
            );
        } elseif ($this->getVal("emarge_livrable_demande_id") != "") {
            $emarge_content = sprintf(
                '<img src="../app/img/nonarrive.png" /> Le livrable liste d\'émargement a été demandé (%s) le %s. En attente du retour de l\'INSEE.',
                $this->getVal("emarge_livrable_demande_id"),
                $this->getVal("emarge_livrable_demande_date")
            );
        }
        return $emarge_content;
    }

    function bloc_generation_listes_electorales() {
        $arrets_liste = $this->get_arrets_liste();
        return $arrets_liste->bloc_generation_listes_electorales();
    }

    function bloc_edition_recapitulatif_mention() {
        $bloc__edition_recapitulatif_mention = "";
        if ($this->getVal("emarge_livrable") != "") {
            ob_start();
            printf(
                '<div id="edition_recap_mention" class="card">
                <div class="card-header">
                    Récapitulatif des mentions
                </div>
                <div class="card-body">
                '
            );
            $this->view__edition_recapitulatif_mention();
            printf(
                '</div>
                </div>
                '
            );
            $bloc__edition_recapitulatif_mention = ob_get_clean();
        }
        return $bloc__edition_recapitulatif_mention;
    }

    function bloc_edition_listes_electorales() {
        $bloc__edition_listes_electorales = "";
        if ($this->getVal("emarge_livrable") != "") {
            ob_start();
            printf(
                '<div id="edition_liste_elec_par_bureau" class="card">
                <div class="card-header">
                    Liste électorale par bureau
                </div>
                <div class="card-body">
                '
            );
            $this->view__edition_listes_electorales();
            printf(
                '</div>
                </div>
                '
            );
            $bloc__edition_listes_electorales = ob_get_clean();
        }
        return $bloc__edition_listes_electorales;
    }

    function bloc__edition_listes_emargements() {
        $bloc__edition_listes_emargements = "";
        if ($this->getVal("emarge_livrable") != "") {
            ob_start();
            printf(
                '<div id="edition_liste_emarge_par_bureau" class="card">
                <div class="card-header">
                    Liste d\'émargement par bureau
                </div>
                <div class="card-body">
                '
            );
            $this->view__edition_listes_emargements();
            printf(
                '</div>
                </div>
                '
            );
            $bloc__edition_listes_emargements = ob_get_clean();
        }
        return $bloc__edition_listes_emargements;
    }

    function display_infos_livrable_scrutin() {
        printf(
            '<div class="container-fluid" id="module-election-scrutin">
                <h2>Livrables \'Répertoire Électorale Unique\'</h2>
                <div class="row">
                    <div id="arret_listes" class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                Arrêt des listes
                            </div>
                            <div class="card-body">
                                %1$s
                            </div>
                        </div>
                    </div>
                    <div id="mouv_j5" class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                Mouvements J-5
                            </div>
                            <div class="card-body">
                                %2$s
                            </div>
                        </div>
                    </div>
                    <div id="liste_emarge" class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                Liste d\'émargement
                            </div>
                            <div class="card-body">
                                %3$s
                            </div>
                        </div>
                    </div>
                </div>
                <div class="visualClear"></div>
                <h2>Éditions</h2>
                <div class="row">
                    <div class="col-sm-6">
                        %4$s
                    </div>
                    <div class="col-sm-6">
                        %5$s
                        %6$s
                        %7$s
                    </div>
                </div>
                <div class="visualClear"></div>
            </div>
            <div class="visualClear"></div>',
            //
            $this->bloc_livrable_j20_content(),
            $this->bloc_livrable_j5_content(),
            $this->bloc_livrable_emarge_content(),
            $this->bloc_generation_listes_electorales(),
            $this->bloc_edition_recapitulatif_mention(),
            $this->bloc_edition_listes_electorales(),
            $this->bloc__edition_listes_emargements()
        );
    }

    /**
     *
     */
    function afterFormSpecificContent() {
        if ($this->getParameter("maj") == 3) {
            $this->display_infos_livrable_scrutin();
        }
    }

    /**
     *
     */
    public function view__edition_recapitulatif_mention() {
        //
        $this->f->layout->display__form_container__begin(array(
            "action" => sprintf(
                "../app/index.php?module=form&obj=reu_scrutin&action=302&idx=%s",
                $this->getVal($this->clePrimaire)
            ),
            "id" => "recapitulatif_emarge_par_bureau",
            "target" => "_blank",
        ));
        $champs = array("tour", "liste", );
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
        ));
        //
        $form->setLib("tour", __("Tour"));
        $form->setType("tour", "select");
        $contenu = array(
            0 => array("les_deux", "un", "deux", ),
            1 => array("les deux (double émargement)", "tour 1 (simple émargement)", "tour 2 (simple émargement)", ),
        );
        $form->setSelect("tour", $contenu);
        //
        $form->setLib("liste", __("Liste"));
        $form->setType("liste", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__listes(true) as $key => $liste) {
            array_push($contenu[0], $liste["liste"]);
            array_push($contenu[1], $liste["libelle_liste"]);
        }
        $form->setSelect("liste", $contenu);
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "recapitulatif_emarge_par_bureau.submit",
            "value" => __("Télécharger"),
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }

    /**
     *
     */
    function view__edition_listes_electorales() {
        //
        $this->f->layout->display__form_container__begin(array(
            "action" => "../app/index.php?module=form&obj=electeur&action=302&mode_edition=parbureau&scrutin=".$this->getVal($this->clePrimaire),
            "id" => "liste_electeorale_par_bureau",
            "target" => "_blank",
        ));
        $champs = array("liste", "bureau", );
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
        ));
        //
        $form->setLib("liste", __("Liste"));
        $form->setType("liste", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__listes(true) as $key => $liste) {
            array_push($contenu[0], $liste["liste"]);
            array_push($contenu[1], $liste["libelle_liste"]);
        }
        $form->setSelect("liste", $contenu);
        //
        $form->setLib("bureau", __("Bureau de vote"));
        $form->setType("bureau", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $key => $bureau) {
            array_push($contenu[0], $bureau["code"]);
            array_push($contenu[1], $bureau["code"]." ".$bureau["libelle"]);
        }
        $form->setSelect("bureau", $contenu);
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "liste_electorale_par_bureau.submit",
            "value" => __("Télécharger"),
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }
    /**
     *
     */
    function view__edition_listes_emargements() {
        //
        $this->f->layout->display__form_container__begin(array(
            "action" => "../app/index.php?module=form&obj=electeur&action=304&mode_edition=parbureau&scrutin=".$this->getVal($this->clePrimaire),
            "id" => "liste_emargement_par_bureau",
            "target" => "_blank",
        ));
        $champs = array("tour", "bloc_arrete_nb_emargements", "liste", "bureau", );
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
        ));
        //
        $form->setLib("tour", __("Tour"));
        $form->setType("tour", "select");
        $contenu = array(
            0 => array("les_deux", "un", "deux", ),
            1 => array("les deux (double émargement)", "tour 1 (simple émargement)", "tour 2 (simple émargement)", ),
        );
        $form->setSelect("tour", $contenu);
        //
        $form->setLib("bloc_arrete_nb_emargements", __("Afficher le bloc 'arrêté nombre d'émargements' ?"));
        $form->setType("bloc_arrete_nb_emargements", "checkbox");
        $form->setVal("bloc_arrete_nb_emargements", "t");
        $form->setTaille("bloc_arrete_nb_emargements", 1);
        $form->setMax("bloc_arrete_nb_emargements", 1);
        //
        $form->setLib("liste", __("Liste"));
        $form->setType("liste", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__listes(true) as $key => $liste) {
            array_push($contenu[0], $liste["liste"]);
            array_push($contenu[1], $liste["libelle_liste"]);
        }
        $form->setSelect("liste", $contenu);
        //
        $form->setLib("bureau", __("Bureau de vote"));
        $form->setType("bureau", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $key => $bureau) {
            array_push($contenu[0], $bureau["code"]);
            array_push($contenu[1], $bureau["code"]." ".$bureau["libelle"]);
        }
        $form->setSelect("bureau", $contenu);
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "liste_emargement_par_bureau.submit",
            "value" => __("Télécharger"),
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }

    /**
     * VIEW - view__edition_pdf__recapitulatif_mention.
     *
     * @return void
     */
    function view__edition_pdf__recapitulatif_mention() {
        //
        $params = array(
            "scrutin" => $this->getVal($this->clePrimaire),
            "scrutin_libelle" => $this->getVal("libelle"),
            "scrutin_tour1" => $this->getVal("date_tour1"),
            "scrutin_tour2" => $this->getVal("date_tour2"),
            "livrable_demande_id" => $this->getVal("emarge_livrable_demande_id"),
        );
        if ($this->f->get_submitted_post_value('tour') !== null) {
            $tour = $this->f->get_submitted_post_value('tour');
            $params["tour"] = $tour;
            if ($tour == "les_deux") {
                $params["dateelection1"] = $this->f->formatdate($this->getVal("date_tour1"), true);
                $params["dateelection2"] = $this->f->formatdate($this->getVal("date_tour2"), true);
            } elseif ($tour == "un") {
                $params["dateelection1"] = $this->f->formatdate($this->getVal("date_tour1"), true);
            } elseif ($tour == "deux") {
                $params["dateelection1"] = $this->f->formatdate($this->getVal("date_tour2"), true);
            }
        }
        if ($this->f->get_submitted_post_value('liste') !== null) {
            $params["liste"] = $this->f->get_submitted_post_value('liste');
        }
        //
        require_once "../obj/edition_pdf__recapitulatif_mention.class.php";
        $inst_edition_pdf = new edition_pdf__recapitulatif_mention();
        $pdf_edition = $inst_edition_pdf->compute_pdf__recapitulatif_mention($params);
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * Statistiques - 'mentions_valides_par_bureau'.
     *
     * Nombre de mentions valides à une date par bureau.
     *
     * @param array $params Tableau de paramètres.
     *  - "dateelection"
     *  - "dateelection1"
     *  - "dateelection2"
     *
     * @return array
     */
    function compute_stats__mentions_valides_par_bureau($params) {
        // Récupération de la liste
        $liste = $_SESSION["liste"];
        $libelle_liste = $_SESSION["libelle_liste"];
        if (array_key_exists("liste", $params) === true) {
            $liste = $params["liste"];
            $ret = $this->f->get_one_result_from_db_query(sprintf(
                'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                DB_PREFIXE,
                $this->f->db->escapesimple($liste)
            ));
            $libelle_liste = $ret["result"];
        }
        //
        $totalavant=0;
        $totalapres1=0;
        $totalapres2=0;
        $total_mention_1 = 0;
        $total_mention_2 = 0;
        //
        $data = array();
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
            //
            $query_nb_el = sprintf(
                'SELECT count(*)
                FROM %1$sreu_livrable
                WHERE om_collectivite=\'%2$s\'
                    AND liste=\'%3$s\'
                    AND demande_id=%4$s
                    AND code_du_bureau_de_vote=\'%5$s\'
                    AND scrutin_id=%6$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $liste,
                $this->getVal('emarge_livrable_demande_id'),
                $bureau['code'],
                $this->getVal($this->clePrimaire)
            );
            $nb_el = $this->f->get_one_result_from_db_query(
                $query_nb_el
            );
            $avant = $nb_el['result'];
            //
            $totalavant=$totalavant+$avant;
            //
            $query_mention_1 = sprintf(
                'SELECT count(*)
                FROM %1$sreu_livrable
                WHERE om_collectivite=\'%2$s\'
                    AND liste=\'%3$s\'
                    AND demande_id=%4$s
                    AND code_du_bureau_de_vote=\'%5$s\'
                    AND scrutin_id=%6$s
                    AND mention_code=\'%7$s\'',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $liste,
                $this->getVal('emarge_livrable_demande_id'),
                $bureau['code'],
                $this->getVal($this->clePrimaire),
                '1'
            );
            $nb_mention_1 = $this->f->get_one_result_from_db_query(
                $query_mention_1
            );
            $mention_1 = $nb_mention_1['result'];
            $total_mention_1 += $mention_1;
            //
            $query_mention_2 = sprintf(
                'SELECT count(*)
                FROM %1$sreu_livrable
                WHERE om_collectivite=\'%2$s\'
                    AND liste=\'%3$s\'
                    AND demande_id=%4$s
                    AND code_du_bureau_de_vote=\'%5$s\'
                    AND scrutin_id=%6$s
                    AND mention_code=\'%7$s\'',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $liste,
                $this->getVal('emarge_livrable_demande_id'),
                $bureau['code'],
                $this->getVal($this->clePrimaire),
                '2'
            );
            $nb_mention_2 = $this->f->get_one_result_from_db_query(
                $query_mention_2
            );
            $mention_2 = $nb_mention_2['result'];
            $total_mention_2 += $mention_2;
            //
            $apres1 = $avant - $mention_1 - $mention_2;
            $apres2 = $avant - $mention_2;
            //
            $totalapres1=$totalapres1+$apres1;
            $totalapres2=$totalapres2+$apres2;
            //
            $datas = array();
            array_push($datas, $bureau['code']." - ".$bureau['libelle']);
            array_push($datas, $avant);
            array_push($datas, $mention_1);
            array_push($datas, $mention_2);
            array_push($datas, $apres1);
            array_push($datas, $apres2);
            $data[] = $datas;
        }
        //
        $datas = array();
        array_push($datas, __("TOTAL"));
        array_push($datas, $totalavant);
        array_push($datas, $total_mention_1);
        array_push($datas, $total_mention_2);
        array_push($datas, $totalapres1);
        array_push($datas, $totalapres2);
        $data[] = $datas;
        // column/offset Array
        $column = array();
        $offset = array();
        $align = array();
        array_push($column, '');
        array_push($offset, 0);
        array_push($align, "L");
        //
        array_push($column, __("inscrit(s)"));
        array_push($offset, 32);
        array_push($align, "R");
        //
        array_push($column, __("mention 1"));
        array_push($offset, 32);
        array_push($align, "R");
        //
        array_push($column, __("mention 2"));
        array_push($offset, 32);
        array_push($align, "R");
        //
        array_push($column, __("inscrit(s) après mentions tour1"));
        array_push($offset, 32);
        array_push($align, "R");
        //
        array_push($column, __("inscrit(s) après mentions tour2"));
        array_push($offset, 32);
        array_push($align, "R");
        //
        $subtitle = __("Détail des mentions / mention 1 (*** ne vote pas au premier tour ***) / mention 2 (*** ne vote pas dans la commune ***)");
        // Array
        return array(
            "format" => "L",
            "title" => $subtitle,
            "offset" => $offset,
            "align" => $align,
            "column" => $column,
            "data" => $data,
            "output" => "stats-mentionbureau",
        );
    }

    /**
     * SETTER_FORM - set_form_default_values (setVal).
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        // Il est nécessaire de définir des valeurs par défaut pour chaque
        // champ en maj = 101 car cette action utilisen la view formulaire
        // sans être positionnée sur un objet or om_formulaire
        // n'intiialise pas ces valeurs par défaut et om_dbform non plus
        // lorsqu'il n'y a pas de données dans la table ce qui cause des
        // notices.
        if ($validation == 0 && $maj == 101) {
            foreach ($this->champs as $key => $value) {
                $form->setVal($value, "");
            }
        }
    }
}
