<?php
/**
 * Ce script définit la classe 'electionEpurationProcurationTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'electionEpurationProcurationTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class electionEpurationProcurationTraitement extends traitement {

    var $fichier = "election_epuration_procuration";

    var $champs = array("dateelection");

    function getValidButtonValue() {
        return __("Epuration des procurations");
    }

    function getDescription() {
        //
        return __("Saisir une date jusqu'a laquelle toutes les procurations ".
                 "vont etre supprimees. Cette suppression se fait sur toutes ".
                 "les listes.");
    }

    function setContentForm() {
        //
        $this->form->setLib("dateelection", __("Suppression des procurations actives dont la date de validite est anterieure ou egale au [date avant election en cours]"));
        $this->form->setType("dateelection", "date");
        $this->form->setTaille("dateelection", 10);
        $this->form->setMax("dateelection", 10);
        $this->form->setOnchange("dateelection", "fdate(this)");
        $this->form->setVal("dateelection", date("d/m/Y"));
    }

    function displayAfterContentForm() {
        //
        $links = array(
            "0" => array(
                "href" => "javascript:epuration_procuration_stats();",
                "class" => "om-prev-icon statistiques-16",
                "title" => __("Statistiques details par bureau"),
                "id" => "action-traitement-election_epuration_procuration-statistiques",
            ),
        );
        //
        $this->f->displayLinksAsList($links);
    }

    function treatment () {
        //
        $this->LogToFile("start election_epuration_procuration");
        //
        $dateElection = date('d/m/Y');
        if (isset ($_POST['dateelection']))
            $dateElection = $_POST['dateelection'];
        if ($this->f->formatdate=="AAAA-MM-JJ") {
            $date = explode("/", $dateElection);
            // controle de date
            if (sizeof($date) == 3 and (checkdate($date[1],$date[0],$date[2]))) {
                $dateElectionR = $date[2]."-".$date[1]."-".$date[0];
            } else {
                //
                $this->error = true;
                //
                $msg = "La date ".$dateElection." n'est pas une date<br />";
                $this->LogToFile($msg);
                $this->addToMessage($msg);
            }
        }
        if ($this->f->formatdate=="JJ/MM/AAAA") {
            $date = explode("/", $dateElection);
            // controle de date
            if (sizeof($date) == 3 and checkdate($date[1],$date[0],$date[2])){
                $dateElectionR = $date[0]."/".$date[1]."/".$date[2];
            } else {
                //
                $this->error = true;
                //
                $msg = "La date ".$dateElection." n'est pas une date<br />";
                $this->LogToFile($msg);
                $this->addToMessage($msg);
            }
        }
        //
        if ($this->error == false) {
            //
            if (isset($dateElectionR)) {
                $sqlE = sprintf(
                    'SELECT procuration.id as procuration_id, procuration.mandant, procuration.mandataire, procuration.fin_validite, electeur.id as electeur_id, electeur.liste FROM %1$sprocuration LEFT JOIN %1$selecteur ON procuration.mandant=electeur.id WHERE procuration.id_externe IS NULL AND procuration.fin_validite<=\'%3$s\' and procuration.om_collectivite=%2$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $dateElectionR
                );
            }
            //
            $msg = "Epuration prise en compte pour le ".$dateElection." : ";
            $this->LogToFile($msg);
            //
            $res = $this->f->db->query($sqlE);
            //
            if ($this->f->isDatabaseError($res, true)) {
                //
                $this->error = true;
                //
                $message = $res->getMessage()." - ".$res->getUserInfo();
                $this->LogToFile($message);
                //
                $this->addToMessage(__("Contactez votre administrateur."));
            } else {
                $i=0;
                while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                    //
                    $sql = sprintf(
                        'DELETE FROM %1$sprocuration WHERE id=%2$s',
                        DB_PREFIXE,
                        $row['procuration_id']
                    );
                    $res1 = $this->f->db->query($sql);
                    $this->f->addToLog(
                        __METHOD__."(): db->query(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    if ($this->f->isDatabaseError($res1, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $res1->getMessage()." - ".$res1->getUserInfo();
                        $this->LogToFile($message);
                        //
                        $this->addToMessage(__("Contactez votre administrateur."));
                        //
                        break;
                    } else {
                        $i++ ;
                        $this->LogToFile("- procuration id : ".$row["procuration_id"]." / ".$row['mandant']." => ".$row['mandataire']);
                    }
                }
                //
                $msg = $i." ".__("procuration(s) supprimee(s) dont la date de fin de validite etait le ou avant le")." ".$dateElection;
                $this->LogToFile($msg);
                $this->addToMessage($msg);
            }
        }

        //
        $this->LogToFile("end election_epuration_procuration");
    }
}


