<?php
/**
 * Ce script définit la classe 'om_parametre'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."obj/om_parametre.class.php";

/**
 * Définition de la classe 'om_parametre' (om_dbform).
 */
class om_parametre extends om_parametre_core {
    /**
     *
     */
    public $_hidden_and_locked_elements = array(
        "ugle",
        "datetableau",
        "reu_compte_logiciel",
        "reu_sync_l_e_init",
        "reu_sync_l_e_attach",
        "reu_sync_l_e_valid",
        "reu_sync_bureaux",
        "reu_sync_check_status",
        "reu_sync_refonte_date_renumerotation",
        "reu_sync_refonte_date_synchronisation",
        "notification_courriel_profils",
    );

    /**
     *
     */
    function init_class_actions() {
        parent::init_class_actions();
        //
        $this->class_actions[1]["condition"][] = "is_not_an_hidden_and_locked_element";
        $this->class_actions[2]["condition"][] = "is_not_an_hidden_and_locked_element";
        $this->class_actions[3]["condition"][] = "is_not_an_hidden_and_locked_element";
    }

    /**
     *
     */
    function is_not_an_hidden_and_locked_element() {
        if (in_array($this->getVal("libelle"), $this->_hidden_and_locked_elements) === true) {
            return false;
        }
        return true;
    }

    /**
     *
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val);
        //
        if (array_key_exists("libelle", $val) === true
            and in_array($val["libelle"], $this->_hidden_and_locked_elements) === true) {
            //
            $this->correct = false;
            $this->addToMessage(__("Ce libellé ne peut pas être utilisé."));
        }
    }
}
