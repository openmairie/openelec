<?php
/**
 * Ce script définit la classe 'param_mouvement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/param_mouvement.class.php";

/**
 * Définition de la classe 'param_mouvement' (om_dbform).
 */
class param_mouvement extends param_mouvement_gen {

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "code",
            "libelle",
            "typecat",
            "effet",
            "edition_carte_electeur",
            "cnen",
            "codeinscription",
            "coderadiation",
            "insee_import_radiation",
            "om_validite_debut",
            "om_validite_fin",
        );
    }

    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("libelle", "typecat", "cnen", "effet");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= __("est obligatoire")."<br />";
            }
        }
        //
        if ($this->correct == true) {
            $msg = "";
            if ($this->valF['cnen'] == "Oui") {
                if ($this->valF['typecat']=="Inscription") {
                    if ($this->valF['codeinscription']=='' || $this->valF['coderadiation']!='') {
                        $msg .= "* ".__("Vous avez sélectionné un code RADIATION pour le transfert INSEE alors que la catégorie du mouvement est INSCRIPTION.")."<br/>";
                        $this->correct=false;
                    }

                }
                if ($this->valF['typecat']=="Radiation") {
                    if ($this->valF['coderadiation']=='' || $this->valF['codeinscription']!='') {
                        $msg .= "* ".__("Vous avez sélectionné un code INSCRIPTION pour le transfert INSEE alors que la catégorie du mouvement est RADIATION.")."<br/>";
                        $this->correct=false;
                    }
                }
                if ($this->valF['typecat']=="Modification") {
                    $msg .= "* ".__("Vous ne pouvez pas transférer a l'INSEE des mouvements ayant la catégorie MODIFICATION.")."<br/>";
                    $this->correct=false;
                }
            } else {
                if ($this->valF['codeinscription']!='' || $this->valF['coderadiation']!='') {
                    $this->correct=false;
                    if ($this->valF['codeinscription']!='') {
                        $msg .= "* ".__("Vous ne pouvez pas sélectionner un code INSCRIPTION si vous n'activez pas le transfert INSEE.")."<br/>";
                    }
                    if ($this->valF['coderadiation']!='') {
                        $msg .= "* ".__("Vous ne pouvez pas sélectionner un code RADIATION si vous n'activez pas le transfert INSEE.")."<br/>";
                    }
                }
            }
            //
            if ($this->valF['insee_import_radiation'] != "") {
                if ($this->valF['typecat']=="Inscription" || $this->valF['typecat']=="Modification") {
                    $this->correct=false;
                    $msg .= "* ".__("Vous ne pouvez pas sélectionner de code RADIATION en correspondance pour l'import INSEE si la catégorie de mouvement n'est pas RADIATION.")."<br/>";
                }
            }
            if ($this->correct==false) {
                $this->msg .= __("Problème(s) dans les paramètres de lien(s) avec l'INSEE")." :<br/>".$msg;
            }
        }
        //
        if ($this->correct == true) {
            //
            $sql = sprintf(
                'SELECT code, insee_import_radiation FROM %1$sparam_mouvement WHERE typecat=\'Radiation\'',
                DB_PREFIXE
            );
            $res = $this->f->db->query($sql);
            $this->addToLog(
                __METHOD__."(): db->query(\"".$sql."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            //
            $param_mouvement = array();
            $insee_import_radiation_new = explode(";", $this->valF['insee_import_radiation']);
            while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                if ($val["code"] != $row["code"] && $row["insee_import_radiation"] != "") {
                    $param_mouvement[$row["code"]] = explode(";",$row["insee_import_radiation"]);
                }
            }
            foreach ($insee_import_radiation_new as $code_insee) {
                //
                foreach ($param_mouvement as $key => $insee_import_radiation_old) {
                    if (in_array($code_insee, $insee_import_radiation_old)) {
                        $this->correct=false;
                        $this->msg.= __("La correspondance du motif de radiation")." ".$code_insee." ".__("est déja utilisée par un autre type de mouvement")." : ".$key.".<br />";
                    }
                }
            }
        }
        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {

            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(__('La date de fin de validité doit être future à la date de début de validité.'));
            }
        }
    }

    /**
     *
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //
        $this->rechercheTable($this->f->db, "mouvement", "types", $id);
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        //
        if ($validation == 0 and $maj == 0) {
            $form->setVal("typecat", "Modification");
            $form->setVal("effet", "1erMars");
            $form->setVal("cnen", "Non");
            $form->setVal("codeinscription", "");
            $form->setVal("coderadiation", "");
            $form->setVal("edition_carte_electeur", 0);
        }
    }

    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setGroupe(&$form, $maj) {
        //
        $form->setGroupe("code", "D");
        $form->setGroupe("libelle", "F");
        //$form->setGroupe("codeinscription", "D");
        //$form->setGroupe("coderadiation", "F");
        $form->setGroupe("typecat", "D");
        $form->setGroupe("effet", "F");
    }

    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setRegroupe(&$form,$maj) {
        //
        $form->setRegroupe("code", "D", __("Mouvement"));
        $form->setRegroupe("libelle", "F", "");
        //
        $form->setRegroupe("cnen", "D", __("Lien(s) avec l'INSEE"));
        $form->setRegroupe("codeinscription", "G", "");
        $form->setRegroupe("coderadiation", "G", "");
        $form->setRegroupe("insee_import_radiation", "F", "");
        //
        $form->setRegroupe("typecat", "D", __("Parametres"));
        $form->setRegroupe("effet", "G", "");
        $form->setRegroupe("edition_carte_electeur", "F", "");
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj < 2) { // ajouter et modifier
            $form->setType("typecat", "select");
            $form->setType("effet", "select");
            $form->setType("edition_carte_electeur", "select");
            $form->setType("cnen", "select");
            $form->setType("codeinscription", "select");
            $form->setType("coderadiation", "select");
            $form->setType("insee_import_radiation", "checkbox_multiple");
            if ($maj == 1) { // modifier
                $form->setType("code", "hiddenstatic");
                $form->setType("typecat", "hiddenstatic");
            } else { // ajouter
            }
        } else { // supprimer
            $form->setType("code", "hiddenstatic");
        }
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if ($maj < 2) {
            //
            $contenu[0] = array("Inscription", "Radiation", "Modification");
            $contenu[1] = array(__("Inscription"), __("Radiation"), __("Modification"));
            $form->setSelect("typecat",$contenu);
            //
            $contenu[0] = array("1erMars", "Immediat", "Election");
            $contenu[1] = array(__("1erMars"), __("Immediat"), __("Election"));
            $form->setSelect("effet",$contenu);
            //
            $contenu[0] = array("Oui", "Non");
            $contenu[1] = array(__("Oui"), __("Non"));
            $form->setSelect("cnen",$contenu);
            //
            $contenu[0] = array("", "1", "2", "8");
            $contenu[1] = array(__("Sans"), __("1 Normal"), __("2 Inscription juridique"), __("8 Inscription Office"));
            $form->setSelect("codeinscription",$contenu);
            //
            $contenu[0] = array("", "P", "D", "J", "E");
            $contenu[1] = array(__("Sans"), __("(P)erte de qualite requise par la loi"), __("(D)eces"), __("(J)Decision tribunal"), __("(E)rreur materiel"));
            $form->setSelect("coderadiation",$contenu);
            //
            $contenu[0] = array("2", "1", "0");
            $contenu[1] = array(__("Lors de changement de bureau"), __("Toujours"), __("Jamais"));
            $form->setSelect("edition_carte_electeur",$contenu);
            //
            $contenu[0] = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9",);
            $contenu[1] = array(__("0 - inscription d'office dans une autre commune"),
                                __("1 - deces"),
                                __("2 - changement de commune d'inscription (nouvelle inscription sur decision de la commission administrative)"),
                                __("3 - changement de commune d'inscription (nouvelle inscription sur decision judiciaire)"),
                                __("4 - n'a pas atteint l'age electoral"),
                                __("5 - etat civil incontrolable"),
                                __("6 - inscription volontaire annulant l'inscription d'office dans votre commune"),
                                __("7 - decision de tutelle privative de la capacite electorale"),
                                __("8 - condamnation privative de la capacite electorale"),
                                __("9 - perte de nationalite francaise"),
                                );
            $form->setSelect("insee_import_radiation",$contenu);
        }
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("code", __("Code"));
        $form->setLib("effet", __("Date d'effet"));
        $form->setLib("typecat", __("Categorie du mouvement"));
        $form->setLib("edition_carte_electeur", __("Edition de la carte d'electeur"));
        $form->setLib("cnen", __("Transferer ce type de mouvement a l'INSEE ?"));
        $form->setLib("codeinscription", __("Quelle est la correspondance avec le code motif de l'inscription de l'INSEE ? (A selectionner uniquement si la categorie du mouvement est INSCRIPTION et que le transfert est a OUI)"));
        $form->setLib("coderadiation", __("Quelle est la correspondance avec le code motif de la radiation de l'INSEE ? (A selectionner uniquement si la categorie du mouvement est INSCRIPTION et que le transfert est a OUI)"));
        $form->setLib("insee_import_radiation", __("Quelle sont les correspondances avec les valeurs du code motif de la radiation dans les fichiers d'import de radiations INSEE ?  (A selectionner uniquement si la categorie du mouvement est RADIATION)"));
        $form->setLib('om_validite_debut',__('Debut de validite (avant cette date le mouvement ne sera pas selectionnable)'));
        $form->setLib('om_validite_fin',__('Fin de validite (apres cette date le mouvement ne sera pas selectionnable)'));
    }

    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange("code", "this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle", "this.value=this.value.toUpperCase()");
    }
}
