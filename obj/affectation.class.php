<?php
/**
 * Ce script contient la définition de la classe *affectation*.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/affectation.class.php";

/**
 * Définition de la classe *affectation* (om_dbform).
 */
class affectation extends affectation_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    public function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-affectation",
            "view" => "view_edition_pdf__affectation",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Affectation / Convocation"),
               "class" => "pdf-16",
               "order" => 40,
            ),
            "permission_suffix" => "edition_pdf__affectation",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        $this->class_actions[202] = array(
            "identifier" => "edition-pdf-recepisse",
            "view" => "view_edition_pdf__recepisse",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Récépissé"),
               "class" => "pdf-16",
               "order" => 40,
            ),
            "permission_suffix" => "edition_pdf__recepisse",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
    }

    /**
     * GETTER_FORMINC - sql_bureau.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return $this->get_common_var_sql_forminc__sql_bureau();
    }

    /**
     * GETTER_FORMINC - sql_bureau_by_id.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return $this->get_common_var_sql_forminc__sql_bureau_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_candidat() {
        if ($this->is_in_context_of_foreign_key("composition_scrutin", $this->getParameter("retourformulaire"))) {
            return sprintf(
                'SELECT candidat.id, candidat.nom FROM %1$scandidat WHERE candidat.composition_scrutin=%2$s ORDER BY candidat.nom ASC',
                DB_PREFIXE,
                intval($this->getParameter("idxformulaire"))
            );
        }
        return "SELECT candidat.id, candidat.nom FROM ".DB_PREFIXE."candidat ORDER BY candidat.nom ASC";
    }

    /**
     *
     */
    function get_widget_config__elu__autocomplete() {
        return array(
            "obj" => "elu",
            "table" => "elu",
            "identifiant" => "elu.id",
            "criteres" => array(
                "elu.nom" => __("Nom"),
                "elu.prenom" => __("Prénom"),
            ),
            "libelle" => array(
                "elu.nom",
                "elu.prenom",
            ),
            "jointures" => array(
            ),
            "droit_ajout" => false,
            "where" => "",
            "group_by" => array(
            ),
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_poste() {
        return "SELECT poste.poste, poste.libelle FROM ".DB_PREFIXE."poste WHERE poste.nature='affectation' ORDER BY poste.libelle ASC";
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);
        if ($maj == 0 || $maj == 1) {
            $form->setType("elu", "autocomplete");
        }
        //
        if ($this->is_in_context_of_foreign_key("composition_scrutin", $this->getParameter("retourformulaire"))) {
            $form->setType("composition_scrutin", "hidden");
        }
    }

    /**
     * SETTER_FORM - setSelect.
     *
     * @return void
     */
    public function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if ($maj == 0 || $maj == 1) {
            $form->setSelect(
                "elu",
                $this->get_widget_config__elu__autocomplete()
            );
        }
    }

    /**
     * SETTER_FORM - set_form_default_values (setVal).
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        parent::set_form_default_values($form, $maj, $validation);
        if ($validation == 0 && $maj == 0) {
            $form->setVal("periode", "journee");
        }
    }

    /**
     * VIEW - view_edition_pdf__affectation.
     *
     * @return void
     */
    public function view_edition_pdf__affectation() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output("etat", 'affectation');
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'affectation-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__recepisse.
     *
     * @return void
     */
    public function view_edition_pdf__recepisse() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output("etat", 'recepisse');
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'recepisse-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    protected function getNumberOfAffectationsForPoste($composition_scrutin, $poste, $bureau,
                                                       $id = null, $periode = null) {
        $sql = sprintf("
            SELECT
                COUNT(id)
            FROM
                ".DB_PREFIXE."affectation
            WHERE
                composition_scrutin = %s
                AND poste ='%s'
                AND bureau = %s
                AND decision IS True
            ",
            $composition_scrutin,
            $poste,
            $bureau
        );
        if(!empty($id)) { // maj == 1
            $sql .= "   AND id != ".$id."\n";
        }
        if (in_array($periode, array('matin', 'apres-midi'))) {
            $sql .= "   AND (periode = 'journee' OR periode = '".$periode."')";
        }
        return $this->f->db->getOne($sql);
    }

    protected function getNumberOfAffectationsForElu($elu, $date_scrutin,
                                                     $id = null, $periode = null) {
        $sql = sprintf("
            SELECT
                COUNT(affectation.id)
            FROM
                ".DB_PREFIXE."affectation
                    INNER JOIN ".DB_PREFIXE."composition_scrutin
                        ON affectation.composition_scrutin=composition_scrutin.id
            WHERE
                date_tour = '%s'
                AND elu = %s
                AND decision IS True
            ",
            $date_scrutin,
            $elu
        );
        if(!empty($id)) {
            $sql .= "    AND affectation.id != ".$id."\n";
        }
        if (in_array($periode, array('matin', 'apres-midi'))) {
            $sql .= "   AND (periode = 'journee' OR periode = '".$periode."')";
        }
        return $this->f->db->getOne($sql);
    }

    protected function getCantonFor($entity_table, $entity_id_value) {
        $sql = sprintf("
            SELECT
                C.id,
                C.code,
                C.libelle
            FROM
                ".DB_PREFIXE."%s AS E
                LEFT JOIN ".DB_PREFIXE."canton AS C ON C.id=E.canton
            WHERE
                E.id = %s
            ",
            $entity_table,
            $entity_id_value
        );
        $res = $this->f->db->query($sql);
        if (DB::isError($res)) {
            die($res->getMessage(). " => Echec  ".$sql);
        }
        else {
            $count = $res->numrows();
            if ($count > 1) {
                die($res->getMessage(). sprintf(
                    " => Il ne devrait y avoir qu'un seul canton pour $entity_table '%s' (non %d). ".
                    "Requête: ".$sql,
                    $entity_id_value,
                    $count
                ));
            }
        }
        return $res->fetchRow(DB_FETCHMODE_ASSOC);
    }

    /**
     * CHECK_TREATMENT - verifier.
     */
    public function verifier($val = array(), &$dnu1 = NULL, $dnu2 = NULL) {
        parent::verifier($val);
        if ($this->correct) {

            // récupération du code du bureau
            $bureau_code = null;
            if(!empty($this->valF['bureau'])) {
                $sql = sprintf("SELECT code FROM ".DB_PREFIXE."bureau WHERE id = %s",
                               $this->valF['bureau']);
                $bureau_code = $this->f->db->getOne($sql);
            }

            // affectation obligatoire d un bureau
            if ($this->valF['decision'] == 'True' and $bureau_code == 'T'
            and ! in_array($this->valF['poste'], array('DELEGUE TITULAIRE', 'DELEGUE SUPPLEANT'))) {
                $this->correct = false;
                $this->msg = $this->msg.sprintf(__(
                    "Choix du poste : un %s ne peut être affecté à tous les bureaux ".
                    "(seulement les postes %s et %s le peuvent)"),
                    $this->valF['poste'],
                    'DELEGUE TITULAIRE', 'DELEGUE SUPPLEANT');
            }

            // verification si le poste est occupe par un autre elu / non bloquant
            if ($this->valF['decision'] == 'True' and $bureau_code != 'T') {
                $number_of_affectations = $this->getNumberOfAffectationsForPoste(
                    $this->valF['composition_scrutin'],
                    $this->valF['poste'],
                    $this->valF['bureau'],
                    $this->valF['id'],
                    $this->valF['periode']
                );
                if ($number_of_affectations) {
                    $sql = sprintf("SELECT libelle FROM ".DB_PREFIXE."bureau WHERE id = %s",
                                   $this->valF['bureau']);
                    $bureau_libelle = $this->f->db->getOne($sql);
                    $sql = sprintf("SELECT libelle FROM ".DB_PREFIXE."composition_scrutin WHERE id = %s",
                                   $this->valF['composition_scrutin']);
                    $scrutin_libelle = $this->f->db->getOne($sql);
                    //$this->correct = false;
                    $this->msg .= sprintf(__(
                        "Il y a déjà %d %s pour le bureau %s à la période '%s' ".
                        "pour le scrutin '%s'"),
                        $number_of_affectations,
                        $this->valF['poste'],
                        $bureau_libelle,
                        $this->valF['periode'],
                        $scrutin_libelle);
                }
            }
            // verification si l agent n occupe pas un poste dans la meme periode
            // il peut y avoir plusieurs elections en meme temps
            // non bloquant
            if ($this->valF['decision'] == 'True' and $bureau_code != 'T') {

                $sql = "SELECT date_tour FROM ".DB_PREFIXE."composition_scrutin WHERE id = '".$this->valF['composition_scrutin']."'";
                $date_scrutin = $this->f->db->getOne($sql);

                $number_of_affectations = $this->getNumberOfAffectationsForElu(
                    $this->valF['elu'],
                    $date_scrutin,
                    $this->valF['id'],
                    $this->valF['periode']
                );
                if ($number_of_affectations) {
                    $sql = sprintf("SELECT nom||' '||prenom FROM ".DB_PREFIXE."elu WHERE id = %s",
                                   $this->valF['elu']);
                    $elu_nom = $this->f->db->getOne($sql);
                    //$this->correct = false;
                    $this->msg .= sprintf(__(
                        "Il y a déjà %d poste(s) occupé(s) pour l'élu %s ".
                        "durant la période '%s' le %s"),
                        $number_of_affectations,
                        $elu_nom,
                        $this->valF['periode'],
                        $date_scrutin);
                }
            }
/*            // verification des bureaux Cas des cantonales
            if ($bureau_code != 'T') {
                $scrutin_canton = $this->getCantonFor('reu_scrutin', $this->valF['composition_scrutin']);
                if (!empty($scrutin_canton) and $scrutin_canton['code'] != 'T') {
                    $bureau_canton = $this->getCantonFor('bureau', $this->valF['bureau']);
                    if($scrutin_canton['id'] != $bureau_canton['id']) {
                        $this->correct = false;
                        $sql = sprintf("SELECT libelle FROM ".DB_PREFIXE."bureau WHERE id = %s",
                                       $this->valF['bureau']);
                        $bureau_libelle = $this->f->db->getOne($sql);
                        $sql = sprintf("SELECT libelle FROM ".DB_PREFIXE."composition_scrutin WHERE id = %s",
                                       $this->valF['composition_scrutin']);
                        $scrutin_libelle = $this->f->db->getOne($sql);
                        if (!empty($this->msg)) {
                            $this->msg .= "<br/>";
                        }
                        $this->msg .= sprintf(__("Le canton '%s' du bureau '%s' ne correspond pas au canton '%s' du scrutin '%s'"),
                            $bureau_canton['libelle'],
                            $bureau_libelle,
                            $scrutin_canton['libelle'],
                            $scrutin_libelle);
                    }
                }
            }*/
        }
    }

    /*public function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = NULL, $dnu2 = NULL) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        if ($validation==0) {
            if ($maj == 0){
                $scrutin = null;
                if(isset($_GET['idxformulaire'])) {
                    $scrutin = $_GET['idxformulaire'];
                }
                $form->setVal($retourformulaire, $idxformulaire);
                $form->setVal("scrutin", $scrutin);
                $form->setVal("periode", "journee");
            }
        }
    }*/

}
