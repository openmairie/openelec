<?php
/**
 * Ce script définit la classe 'reuSyncMouvementTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'reuSyncMouvementTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncMouvementTraitement extends traitement {
    /**
     *
     */
    var $date_tableau = "2019-01-10";
    /**
     *
     */
    function get_nb_anciens_mouvements($mode = "", $typecat = "") {
        if ($typecat == "radiation") {
            return $this->get_anciens_radiations(array(
                "type" => "nb",
                "mode" => $mode,
            ));
        } elseif ($typecat == "inscription") {
            return $this->get_anciens_inscriptions(array(
                "type" => "nb",
                "mode" => $mode,
            ));
        } elseif ($typecat == "modification") {
            return $this->get_anciens_modifications(array(
                "type" => "nb",
                "mode" => $mode,
            ));
        }
    }
    /**
     *
     */
    function get_list_anciens_inscriptions_actif() {
        return $this->get_anciens_inscriptions(array(
            "type" => "list",
        ));
    }
    /**
     *
     */
    function get_list_anciens_radiations_actif($step) {
        return $this->get_anciens_radiations(array(
            "type" => "list",
            "mode" => $step,
        ));
    }
    /**
     *
     */
    function get_list_anciens_modifications_actif($step) {
        return $this->get_anciens_modifications(array(
            "type" => "list",
            "mode" => $step,
        ));
    }

    /**
     *
     */
    function get_modifications($params) {
        //
        if (array_key_exists("type", $params) === true
            && $params["type"] === "nb"
            && array_key_exists("mode", $params) === true
            && $params["mode"] === "a_transmettre") {
            //
            return $this->f->get_one_result_from_db_query(
                sprintf(
                    'SELECT
                        count(*)
                    FROM
                        %1$smouvement
                        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
                        INNER JOIN %1$selecteur ON mouvement.electeur_id=electeur.id
                        INNER JOIN %1$sliste ON mouvement.liste=liste.liste
                    WHERE 
                        mouvement.om_collectivite=%2$s
                        AND mouvement.date_tableau > \'2019-01-10\'
                        AND lower(param_mouvement.typecat)=\'modification\'
                        AND mouvement.etat = \'actif\' 
                        AND mouvement.statut=\'ouvert\'
                    ',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"])
                )
            );
        } elseif (array_key_exists("type", $params) === true
            && $params["type"] === "list"
            && array_key_exists("mode", $params) === true
            && $params["mode"] === "a_transmettre") {
            //
            return $this->f->get_all_results_from_db_query(
                sprintf(
                    'SELECT
                        mouvement.id
                    FROM
                        %1$smouvement
                        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
                        INNER JOIN %1$selecteur ON mouvement.electeur_id=electeur.id
                        INNER JOIN %1$sliste ON mouvement.liste=liste.liste
                    WHERE 
                        mouvement.om_collectivite=%2$s
                        AND mouvement.date_tableau > \'2019-01-10\'
                        AND lower(param_mouvement.typecat)=\'modification\'
                        AND mouvement.etat = \'actif\' 
                        AND mouvement.statut=\'ouvert\'
                    ORDER BY
                        mouvement.id',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"])
                )
            );
        }
    }
    /**
     *
     */
    function get_anciens_radiations($params) {
        //
        $typecat = "radiation";
        //
        $condition_a_transmettre_etape1 = ' 
            m.etat = \'actif\' 
            AND (m.id_demande=\'\' OR m.id_demande IS NULL)
            AND (m.statut<>\'vise_maire\' OR m.statut IS NULL) 
        ';
        $condition_a_transmettre_etape2 = ' 
            m.etat = \'actif\' 
            AND m.id_demande<>\'\'
            AND m.id_demande IS NOT NULL
            AND m.statut=\'ouvert\'
        ';
        //
        if (array_key_exists("type", $params) === true
            && $params["type"] === "nb"
            && array_key_exists("mode", $params) === true) {
            //
            $mode = $params["mode"];
            $condition_a_transmettre = ' ('.$condition_a_transmettre_etape1.') OR ('.$condition_a_transmettre_etape2.') ';
            $condition_transmis_en_cours = ' m.etat = \'actif\' AND m.id_demande<>\'\' AND m.id_demande IS NOT NULL AND m.statut=\'vise_maire\' ';
            $condition_trs = ' etat = \'trs\' ';
            //
            if ($mode == "total") {
                $condition = ' AND ( ('.$condition_a_transmettre.') OR ('.$condition_transmis_en_cours.') OR ('.$condition_trs.') ) ';
            } elseif ($mode == "a_transmettre") {
                $condition = ' AND ( '.$condition_a_transmettre.' ) ';
            } elseif ($mode == "transmis_en_cours") {
                $condition = ' AND ( '.$condition_transmis_en_cours.' ) ';
            } elseif ($mode == "trs") {
                $condition = ' AND ( '.$condition_trs.' ) ';
            }
            return $this->f->get_one_result_from_db_query(
                sprintf(
                    'SELECT count(*) from %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code LEFT JOIN %1$selecteur as e ON m.electeur_id=e.id INNER JOIN %1$sliste as l ON m.liste=l.liste
                    WHERE 
                        m.om_collectivite=%2$s
                        AND m.date_tableau=\'%5$s\'
                        %3$s
                        %4$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    ($typecat != "" ? ' AND lower(pm.typecat)=\''.$typecat.'\'' : ''),
                    ($condition != "" ? $condition : ''),
                    $this->date_tableau
                )
            );
        } elseif (array_key_exists("type", $params) === true
            && $params["type"] === "list"
            && array_key_exists("mode", $params) === true) {
            //
            $mode = $params["mode"];
            //
            if ($mode == "etape_1") {
                return $this->f->get_all_results_from_db_query(
                    sprintf(
                        'SELECT m.id, e.ine, l.liste_insee from %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code INNER JOIN %1$selecteur as e ON m.electeur_id=e.id INNER JOIN %1$sliste as l ON e.liste=l.liste
                        WHERE 
                            m.om_collectivite=%2$s 
                            AND m.date_tableau=\'%5$s\' 
                            %3$s
                            %4$s
                        ORDER BY l.liste_insee LIMIT 50',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        ($typecat != "" ? ' AND lower(pm.typecat)=\''.$typecat.'\'' : ''),
                        ' AND ('.$condition_a_transmettre_etape1.') ',
                        $this->date_tableau
                    )
                );
            } elseif ($mode == "etape_2") {
                return $this->f->get_all_results_from_db_query(
                    sprintf(
                        'SELECT m.id from %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code
                        WHERE 
                            m.om_collectivite=%2$s
                            AND m.date_tableau=\'%5$s\'
                            %3$s
                            %4$s
                        ORDER BY m.id',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        ($typecat != "" ? ' AND lower(pm.typecat)=\''.$typecat.'\'' : ''),
                        ' AND ('.$condition_a_transmettre_etape2.') ',
                        $this->date_tableau
                    )
                );
            }
        }
    }
    /**
     *
     */
    function get_anciens_inscriptions($params) {
        //
        $typecat = "inscription";
        $condition_a_transmettre = ' m.etat = \'actif\' AND (m.statut<>\'vise_maire\' OR m.statut IS NULL) ';
        //
        if (array_key_exists("type", $params) === true
            && $params["type"] === "nb"
            && array_key_exists("mode", $params) === true) {
            //
            $mode = $params["mode"];
            //
            $condition_transmis_en_cours = ' etat = \'actif\' AND id_demande<>\'\' AND id_demande IS NOT NULL AND statut=\'vise_maire\' ';
            $condition_trs = ' etat = \'trs\' ';
            //
            if ($mode == "total") {
                $condition = ' AND ( ('.$condition_a_transmettre.') OR ('.$condition_transmis_en_cours.') OR ('.$condition_trs.') ) ';
            } elseif ($mode == "a_transmettre") {
                $condition = ' AND ( '.$condition_a_transmettre.' ) ';
            } elseif ($mode == "transmis_en_cours") {
                $condition = ' AND ( '.$condition_transmis_en_cours.' ) ';
            } elseif ($mode == "trs") {
                $condition = ' AND ( '.$condition_trs.' ) ';
            }
            return $this->f->get_one_result_from_db_query(
                sprintf(
                    'SELECT count(*) from %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code
                    WHERE
                        om_collectivite=%2$s
                        AND date_tableau=\'%5$s\'
                        %3$s
                        %4$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    ($typecat != "" ? ' AND lower(pm.typecat)=\''.$typecat.'\'' : ''),
                    ($condition != "" ? $condition : ''),
                    $this->date_tableau
                )
            );
        } elseif (array_key_exists("type", $params) === true
            && $params["type"] === "list") {
            //
            return $this->f->get_all_results_from_db_query(
                sprintf(
                    'SELECT m.id, m.*, l.liste_insee from %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code INNER JOIN %1$sliste as l ON m.liste=l.liste
                    WHERE
                        m.om_collectivite=%2$s
                        AND m.date_tableau=\'%5$s\'
                        %3$s
                        %4$s
                    ORDER BY l.liste_insee',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    ($typecat != "" ? ' AND lower(pm.typecat)=\''.$typecat.'\'' : ''),
                    ' AND ('.$condition_a_transmettre.') ',
                    $this->date_tableau
                )
            );
        }
    }
    /**
     *
     */
    function get_anciens_modifications($params) {
        //
        $typecat = "modification";
        //
        $condition_a_transmettre_etape1 = ' 
            m.etat = \'actif\' 
        ';
        $condition_a_transmettre_etape2 = ' 
            m.etat = \'actif\' 
            AND m.statut=\'ouvert\'
        ';
        //
        if (array_key_exists("type", $params) === true
            && $params["type"] === "nb"
            && array_key_exists("mode", $params) === true) {
            //
            $mode = $params["mode"];
            //
            if ($mode == "transmis_en_cours") {
                return array('result' => 0, );
            }
            $condition_a_transmettre = ' ('.$condition_a_transmettre_etape1.') OR ('.$condition_a_transmettre_etape2.') ';
            $condition_trs = ' etat = \'trs\' ';
            //
            if ($mode == "total") {
                $condition = ' AND ( ('.$condition_a_transmettre.') OR ('.$condition_trs.') ) ';
            } elseif ($mode == "a_transmettre") {
                $condition = ' AND ( '.$condition_a_transmettre.' ) ';
            } elseif ($mode == "trs") {
                $condition = ' AND ( '.$condition_trs.' ) ';
            }
            return $this->f->get_one_result_from_db_query(
                sprintf(
                    'SELECT count(*) from %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code LEFT JOIN %1$selecteur as e ON m.electeur_id=e.id INNER JOIN %1$sliste as l ON m.liste=l.liste
                    WHERE 
                        m.om_collectivite=%2$s
                        AND m.date_tableau=\'%5$s\'
                        %3$s
                        %4$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    ($typecat != "" ? ' AND lower(pm.typecat)=\''.$typecat.'\'' : ''),
                    ($condition != "" ? $condition : ''),
                    $this->date_tableau
                )
            );
        } elseif (array_key_exists("type", $params) === true
            && $params["type"] === "list"
            && array_key_exists("mode", $params) === true) {
            //
            $mode = $params["mode"];
            //
            if ($mode == "etape_1") {
                return $this->f->get_all_results_from_db_query(
                    sprintf(
                        'SELECT m.id, e.ine, l.liste_insee from %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code INNER JOIN %1$selecteur as e ON m.electeur_id=e.id INNER JOIN %1$sliste as l ON e.liste=l.liste
                        WHERE 
                            m.om_collectivite=%2$s 
                            AND m.date_tableau=\'%5$s\' 
                            %3$s
                            %4$s
                        ORDER BY l.liste_insee',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        ($typecat != "" ? ' AND lower(pm.typecat)=\''.$typecat.'\'' : ''),
                        ' AND ('.$condition_a_transmettre_etape1.') ',
                        $this->date_tableau
                    )
                );
            } elseif ($mode == "etape_2") {
                return $this->f->get_all_results_from_db_query(
                    sprintf(
                        'SELECT m.id from %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code
                        WHERE 
                            m.om_collectivite=%2$s
                            AND m.date_tableau=\'%5$s\'
                            %3$s
                            %4$s
                        ORDER BY m.id',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        ($typecat != "" ? ' AND lower(pm.typecat)=\''.$typecat.'\'' : ''),
                        ' AND ('.$condition_a_transmettre_etape2.') ',
                        $this->date_tableau
                    )
                );
            }
        }
    }
    /**
     *
     */
    function get_displayBeforeContentForm($typecat = "") {
        //
        $anciens_mouvements_total = $this->get_nb_anciens_mouvements(
            "total",
            $typecat
        );
        //
        $anciens_mouvements_total_a_transmettre = $this->get_nb_anciens_mouvements(
            "a_transmettre",
            $typecat
        );
        //
        $anciens_mouvements_total_transmis_en_cours = $this->get_nb_anciens_mouvements(
            "transmis_en_cours",
            $typecat
        );
        //
        $anciens_mouvements_total_trs = $this->get_nb_anciens_mouvements(
            "trs",
            $typecat
        );
        //
        printf(
            '
            <div class="container-fluid" id="reu-sync-tab">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="card text-center">
                            <div class="card-header">
                                nb %1$ss total au 10/01/2019
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%2$s</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-center">
                            <div class="card-header">
                                %1$ss traités
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%3$s</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-center">
                            <div class="card-header">
                                %1$s transmis au REU (en cours de traitement par l\'INSEE)
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%4$s</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-center">
                            <div class="card-header">
                                %1$ss restant à transmettre
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="card text-center">
                                        <div class="card-body">
                                            <div class="card-title">%5$s</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="visualClear"></div>
            ',
            $typecat,
            $anciens_mouvements_total["result"],
            $anciens_mouvements_total_trs["result"],
            $anciens_mouvements_total_transmis_en_cours["result"],
            $anciens_mouvements_total_a_transmettre["result"]
        );
    }
}
