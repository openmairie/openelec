<?php
/**
 * Ce script définit la classe 'revision'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/revision.class.php";

/**
 * Définition de la classe 'revision' (om_dbform).
 */
class revision extends revision_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        //
        $this->class_actions[301] = array(
            "identifier" => "edition-pdf-etiquette_additions_des_jeunes",
            "view" => "view_edition_pdf__etiquette_additions_des_jeunes",
            "permission_suffix" => "electorale_edition_pdf__etiquette_additions_des_jeunes",
        );
        //
        $this->class_actions[302] = array(
            "identifier" => "edition-pdf-recapitulatif_traitement",
            "view" => "view_edition_pdf__recapitulatif_traitement",
            "permission_suffix" => "electorale_edition_pdf__recapitulatif_traitement",
        );
        //
        $this->class_actions[303] = array(
            "identifier" => "edition-pdf-listing_mouvements_tableau_commission",
            "view" => "view_edition_pdf__listing_mouvements_tableau_commission",
            "permission_suffix" => "electorale_edition_pdf__listing_mouvements_tableau_commission",
        );
        //
        $this->class_actions[401] = array(
            "identifier" => "statistiques-recapitulatif_tableau",
            "view" => "view_edition_pdf__statistiques_recapitulatif_tableau",
            "permission_suffix" => "electorale_edition_pdf__statistiques_recapitulatif_tableau",
        );
    }

    /**
     *
     */
    var $revisions = null;

    /**
     *
     */
    var $current_revision = null;

    /**
     *
     */
    function get_current_revision($mode = null, $param = null) {

        //
        if ($this->current_revision != null) {
            return $this->current_revision;
        }

        //
        $revisions = $this->get_all_revisions();
        //
        if (count($revisions) == 0) {
            //
            return null;
        }

        //
        if ($mode == "fromid") {
            //
            foreach ($revisions as $key => $value) {
                if ($key == $param) {
                    $this->current_revision = $value;
                    return $this->current_revision;
                }
            }
            //
            return null;
        }

        if ($mode == "fromdatetableau") {
            //
            foreach ($revisions as $key => $value) {
                if ($value["date_tr1"] == $param
                    || $value["date_tr2"] == $param) {
                    $this->current_revision = $value;
                    return $this->current_revision;
                }
            }
            //
            return null;
        }

        //
        foreach ($revisions as $key => $value) {
            if ($value["date_tr1"] == $this->f->getParameter("datetableau")
                || $value["date_tr2"] == $this->f->getParameter("datetableau")) {
                $this->current_revision = $value;
                return $this->current_revision;
            }
        }

        //
        $revisions_ids = array_keys($revisions);
        $this->current_revision = $revisions[$revisions_ids[0]];
        return $this->current_revision;
    }

    /**
     *
     */
    function get_all_revisions() {

        //
        if ($this->revisions != null) {
            return $this->revisions;
        }

        // Cette requete permet de lister toutes les révisions présentes dans
        // le paramétrage.
        $sql = sprintf(
            'SELECT * FROM %1$srevision ORDER BY revision.date_effet DESC',
            DB_PREFIXE
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $this->revisions = array();
        //
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            $this->revisions[$row["id"]] = $row;
        }
        //
        $this->addToLog(
            __METHOD__."(): ".print_r($this->revisions, true),
            EXTRA_VERBOSE_MODE
        );
        //
        return $this->revisions;
    }

    /**
     *
     */
    function get_date($date_field, $format = null) {
        //
        $revision = $this->get_current_revision();
        //
        $date = new DateTime($revision["date_".$date_field]);
        //
        if ($format == "day") {
            return $date->format('d');
        } elseif($format == "month") {
            return $date->format('m');
        } elseif($format == "year") {
            return $date->format('Y');
        } elseif($format != null) {
            return $date->format($format);
        } else {
            return $date->format("Y-m-d");
        }
    }

    /**
     *
     */
    function get_previous_date_tableau($datetableau) {
        //
        $revisions = $this->get_all_revisions();
        //
        foreach ($revisions as $key => $value) {
            if ($value["date_tr1"] == $datetableau
                || $value["date_tr2"] == $datetableau) {
                $revision = $value;
                break;
            }
        }
        //
        if ($revision["date_tr2"] == $datetableau) {
            return $revision["date_tr1"];
        } else {
            $debut = $revision["date_debut"];
            $date = new DateTime($debut);
            $date->modify('-1 day');
            return $date->format('Y-m-d');
        }
    }

    /**
     *
     */
    function is_date_tableau_valid() {
        //
        $revisions = $this->get_all_revisions();
        //
        foreach ($revisions as $key => $value) {
            //
            if ($value["date_tr1"] == $this->f->getParameter("datetableau")
                || $value["date_tr2"] == $this->f->getParameter("datetableau")) {
                //
                return true;
            }
        }
        //
        return false;
    }

    /**
     *
     */
    function compute_stats__recapitulatif_tableau() {
        //
        $datetableau = "";
        if (isset($_GET["tableau"])) {
            $datetableau = $_GET["tableau"];
        };
        //
        $datetableau_precedent = $this->get_previous_date_tableau($datetableau);
        //
        function calculNombreDelecteursDateTableau($electeurs_actuels_groupby_bureau, $inscriptions_radiations_stats_groupby_bureau, $transferts_plus_stats_groupby_bureau, $transferts_moins_stats_groupby_bureau, $date_tableau_reference, $bureau, $sexe = "ALL") {
            //
            if ($sexe == "ALL") {
                $cpt_electeur = $electeurs_actuels_groupby_bureau[$bureau]["total"];
            } else {
                $cpt_electeur = $electeurs_actuels_groupby_bureau[$bureau][$sexe];
            }
            //
            if (isset($inscriptions_radiations_stats_groupby_bureau[$bureau])) {
                foreach ($inscriptions_radiations_stats_groupby_bureau[$bureau] as $element) {
                    if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                        //
                        if ($element["date_tableau"] > $date_tableau_reference) {
                            if ($element["etat"] == "trs") {
                                if ($element["typecat"] == "Inscription") {
                                    $cpt_electeur -= $element["total"];
                                } elseif ($element["typecat"] == "Radiation") {
                                    $cpt_electeur += $element["total"];
                                }
                            }
                        } else {
                            if ($element["etat"] == "actif") {
                                if ($element["typecat"] == "Inscription") {
                                    $cpt_electeur += $element["total"];
                                } elseif ($element["typecat"] == "Radiation") {
                                    $cpt_electeur -= $element["total"];
                                }
                            }
                        }
                    }
                }
            }
            //
            if (isset($transferts_plus_stats_groupby_bureau[$bureau])) {
                foreach ($transferts_plus_stats_groupby_bureau[$bureau] as $element) {
                    if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                        //
                        if ($element["date_tableau"] > $date_tableau_reference) {
                            if ($element["etat"] == "trs") {
                                $cpt_electeur -= $element["total"];
                            }
                        } else {
                            if ($element["etat"] == "actif") {
                                $cpt_electeur += $element["total"];
                            }
                        }
                    }
                }
            }
            //
            if (isset($transferts_moins_stats_groupby_bureau[$bureau])) {
                foreach ($transferts_moins_stats_groupby_bureau[$bureau] as $element) {
                    if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                        //
                        if ($element["date_tableau"] > $date_tableau_reference) {
                            if ($element["etat"] == "trs") {
                                    $cpt_electeur += $element["total"];
                            }
                        } else {
                            if ($element["etat"] == "actif") {
                                $cpt_electeur -= $element["total"];
                            }
                        }
                    }
                }
            }
            return $cpt_electeur;
        }

        //
        include "../sql/".OM_DB_PHPTYPE."/stats_recapitulatif_tableau.inc.php";

        // Nombre actuel d'electeurs dans la base de donnees par bureau
        // array("<BUREAU>" => array("M" => <>, "F" => <>, "total" => <>),);
        $electeurs_actuels_groupby_bureau = array();
        $res_electeurs_actuels_groupby_bureau = $this->f->db->query($query_electeurs_actuels_groupby_bureau);
        $this->addToLog(__METHOD__."(): db->query(\"".$query_electeurs_actuels_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_electeurs_actuels_groupby_bureau);
        while ($row_electeurs_actuels_groupby_bureau =& $res_electeurs_actuels_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]])) {
                $electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]] = array();
            }
            //
            $electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]][$row_electeurs_actuels_groupby_bureau["sexe"]] = $row_electeurs_actuels_groupby_bureau["total"];
        }
        foreach ($electeurs_actuels_groupby_bureau as $key => $value) {
            $electeurs_actuels_groupby_bureau[$key]["total"] = (isset($electeurs_actuels_groupby_bureau[$key]["M"]) ? $electeurs_actuels_groupby_bureau[$key]["M"] : 0) + (isset($electeurs_actuels_groupby_bureau[$key]["F"]) ? $electeurs_actuels_groupby_bureau[$key]["F"] : 0);
        }

        //
        $inscriptions_radiations_stats_groupby_bureau = array();
        $res_inscriptions_radiations_stats_groupby_bureau = $this->f->db->query($query_inscriptions_radiations_stats_groupby_bureau);
        $this->addToLog(__METHOD__."(): db->query(\"".$query_inscriptions_radiations_stats_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_inscriptions_radiations_stats_groupby_bureau);
        while ($row_inscriptions_radiations_stats_groupby_bureau =& $res_inscriptions_radiations_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]])) {
                $inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]] = array();
            }
            //
            array_push($inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]], $row_inscriptions_radiations_stats_groupby_bureau);
        }
        $this->addToLog(__METHOD__."(): ".print_r($inscriptions_radiations_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);

        //
        $transferts_plus_stats_groupby_bureau = array();
        $res_transferts_plus_stats_groupby_bureau = $this->f->db->query($query_transferts_plus_stats_groupby_bureau);
        $this->addToLog(__METHOD__."(): db->query(\"".$query_transferts_plus_stats_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_transferts_plus_stats_groupby_bureau);
        while ($row_transferts_plus_stats_groupby_bureau =& $res_transferts_plus_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]])) {
                $transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]] = array();
            }
            //
            array_push($transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]], $row_transferts_plus_stats_groupby_bureau);
        }
        $this->addToLog(__METHOD__."(): ".print_r($transferts_plus_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);

        //
        $transferts_moins_stats_groupby_bureau = array();
        $res_transferts_moins_stats_groupby_bureau = $this->f->db->query($query_transferts_moins_stats_groupby_bureau);
        $this->addToLog(__METHOD__."(): db->query(\"".$query_transferts_moins_stats_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_transferts_moins_stats_groupby_bureau);
        while ($row_transferts_moins_stats_groupby_bureau =& $res_transferts_moins_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]])) {
                $transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]] = array();
            }
            //
            array_push($transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]], $row_transferts_moins_stats_groupby_bureau);
        }
        $this->addToLog(__METHOD__."(): ".print_r($transferts_moins_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);

        //
        $inscriptions_radiations_groupby_bureau = array();
        $res_inscriptions_radiations_groupby_bureau = $this->f->db->query($query_inscriptions_radiations_groupby_bureau);
        $this->addToLog(__METHOD__."(): db->query(\"".$query_inscriptions_radiations_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_inscriptions_radiations_groupby_bureau);
        while ($row_inscriptions_radiations_groupby_bureau =& $res_inscriptions_radiations_groupby_bureau->fetchrow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($inscriptions_radiations_groupby_bureau[$row_inscriptions_radiations_groupby_bureau["bureau"]])) {
                $inscriptions_radiations_groupby_bureau[$row_inscriptions_radiations_groupby_bureau["bureau"]] = array();
            }
            //
            $inscriptions_radiations_groupby_bureau[$row_inscriptions_radiations_groupby_bureau["bureau"]][$row_inscriptions_radiations_groupby_bureau["typecat"]][$row_inscriptions_radiations_groupby_bureau["sexe"]] = $row_inscriptions_radiations_groupby_bureau["total"];
        }
        foreach ($inscriptions_radiations_groupby_bureau as $key1 => $value1) {
            foreach ($value1 as $key2 => $value2) {
                $inscriptions_radiations_groupby_bureau[$key1][$key2]["total"] = (isset($value2["M"]) ? $value2["M"] : 0) + (isset($value2["F"]) ? $value2["F"] : 0);
            }
        }

        //
        $transferts_plus_groupby_bureau = array();
        $res_transferts_plus_groupby_bureau = $this->f->db->query($query_transferts_plus_groupby_bureau);
        $this->addToLog(__METHOD__."(): db->query(\"".$query_transferts_plus_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_transferts_plus_groupby_bureau);
        while ($row_transferts_plus_groupby_bureau =& $res_transferts_plus_groupby_bureau->fetchrow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($transferts_plus_groupby_bureau[$row_transferts_plus_groupby_bureau["bureau"]])) {
                $transferts_plus_groupby_bureau[$row_transferts_plus_groupby_bureau["bureau"]] = array();
            }
            //
            $transferts_plus_groupby_bureau[$row_transferts_plus_groupby_bureau["bureau"]][$row_transferts_plus_groupby_bureau["sexe"]] = $row_transferts_plus_groupby_bureau["total"];
        }
        foreach ($transferts_plus_groupby_bureau as $key => $value) {
            $transferts_plus_groupby_bureau[$key]["total"] = (isset($transferts_plus_groupby_bureau[$key]["M"]) ? $transferts_plus_groupby_bureau[$key]["M"] : 0) + (isset($transferts_plus_groupby_bureau[$key]["F"]) ? $transferts_plus_groupby_bureau[$key]["F"] : 0);
        }


        //
        $transferts_moins_groupby_bureau = array();
        $res_transferts_moins_groupby_bureau = $this->f->db->query($query_transferts_moins_groupby_bureau);
        $this->addToLog(__METHOD__."(): db->query(\"".$query_transferts_moins_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_transferts_moins_groupby_bureau);
        while ($row_transferts_moins_groupby_bureau =& $res_transferts_moins_groupby_bureau->fetchrow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($transferts_moins_groupby_bureau[$row_transferts_moins_groupby_bureau["bureau"]])) {
                $transferts_moins_groupby_bureau[$row_transferts_moins_groupby_bureau["bureau"]] = array();
            }
            //
            $transferts_moins_groupby_bureau[$row_transferts_moins_groupby_bureau["bureau"]][$row_transferts_moins_groupby_bureau["sexe"]] = $row_transferts_moins_groupby_bureau["total"];
        }
        foreach ($transferts_moins_groupby_bureau as $key => $value) {
            $transferts_moins_groupby_bureau[$key]["total"] = (isset($transferts_moins_groupby_bureau[$key]["M"]) ? $transferts_moins_groupby_bureau[$key]["M"] : 0) + (isset($transferts_moins_groupby_bureau[$key]["F"]) ? $transferts_moins_groupby_bureau[$key]["F"] : 0);
        }

        //
        $totalj5=0;
        $totalinscription=0;
        $totalradiation=0;
        $totalapres=0;
        $totalarrive=0;
        $totaldepart=0;
        $totalhommeapres=0;
        $totalfemmeapres=0;

        //
        $data = array();
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
            //
            $electeurs_datetableau_precedent = calculNombreDelecteursDateTableau(
                $electeurs_actuels_groupby_bureau,
                $inscriptions_radiations_stats_groupby_bureau,
                $transferts_plus_stats_groupby_bureau,
                $transferts_moins_stats_groupby_bureau,
                $datetableau_precedent,
                $bureau["code"]
            );
            //
            $inscriptions = (isset($inscriptions_radiations_groupby_bureau[$bureau["code"]]["Inscription"]["total"]) ? $inscriptions_radiations_groupby_bureau[$bureau["code"]]["Inscription"]["total"] : 0);
            //
            $transferts_plus = (isset($transferts_plus_groupby_bureau[$bureau["code"]]["total"]) ? $transferts_plus_groupby_bureau[$bureau["code"]]["total"] : 0);
            //
            $transferts_moins = (isset($transferts_moins_groupby_bureau[$bureau["code"]]["total"]) ? $transferts_moins_groupby_bureau[$bureau["code"]]["total"] : 0);
            //
            $radiations = (isset($inscriptions_radiations_groupby_bureau[$bureau["code"]]["Radiation"]["total"]) ? $inscriptions_radiations_groupby_bureau[$bureau["code"]]["Radiation"]["total"] : 0);
            //
            $electeurs_datetableau = $electeurs_datetableau_precedent + $inscriptions + $transferts_plus - $transferts_moins - $radiations;
            //
            $electeurs_homme_datetableau = calculNombreDelecteursDateTableau(
                $electeurs_actuels_groupby_bureau,
                $inscriptions_radiations_stats_groupby_bureau,
                $transferts_plus_stats_groupby_bureau,
                $transferts_moins_stats_groupby_bureau,
                $datetableau,
                $bureau["code"],
                "M"
            );
            //
            $electeurs_femme_datetableau = calculNombreDelecteursDateTableau(
                $electeurs_actuels_groupby_bureau,
                $inscriptions_radiations_stats_groupby_bureau,
                $transferts_plus_stats_groupby_bureau,
                $transferts_moins_stats_groupby_bureau,
                $datetableau,
                $bureau["code"],
                "F"
            );
            // Vérification d'éventuelles erreurs
            $electeurs_datetableau_verif = calculNombreDelecteursDateTableau(
                $electeurs_actuels_groupby_bureau,
                $inscriptions_radiations_stats_groupby_bureau,
                $transferts_plus_stats_groupby_bureau,
                $transferts_moins_stats_groupby_bureau,
                $datetableau,
                $bureau["code"]
            );
            if ($electeurs_datetableau_verif != $electeurs_datetableau) {
                $erreur = true;
            } else {
                $erreur = false;
            }
            //
            $datas = array(
                $bureau["code"]." - ".$bureau["libelle"],
                $electeurs_datetableau_precedent,
                $inscriptions,
                $transferts_plus,
                $transferts_moins,
                $radiations,
                $electeurs_datetableau.($erreur == true ? "*" : ""),
                $electeurs_homme_datetableau,
                $electeurs_femme_datetableau,
            );
            $data[] = $datas;
            //
            $totalj5 += $electeurs_datetableau_precedent;
            $totalinscription += $inscriptions;
            $totalarrive += $transferts_plus;
            $totaldepart += $transferts_moins;
            $totalradiation += $radiations;
            $totalapres += $electeurs_datetableau;
            $totalhommeapres += $electeurs_homme_datetableau;
            $totalfemmeapres += $electeurs_femme_datetableau;
        }
        //
        $datas = array(
            __("TOTAL"),
            $totalj5,
            $totalinscription,
            $totalarrive,
            $totaldepart,
            $totalradiation,
            $totalapres,
            $totalhommeapres,
            $totalfemmeapres,
        );
        $data[] = $datas;

        // Array
        return array(
            "collectivite" => $this->f->collectivite["ville"],
            "format" => "L",
            "title" => __("Recapitulatif du tableau rectificatif du")." ".$this->f->formatDate($datetableau),
            "subtitle" => $_SESSION["libelle_liste"],
            "offset" => array(80,0,0,0,0,0,0,0,0),
            "align" => array("L","R","R","R","R","R","R","R","R"),
            "column" => array(
                __("Bureaux de vote"),
                __("Electeur(s) au")." ".$this->f->formatDate($datetableau_precedent),
                __("+ Inscription(s)"),
                __("+ Transfert(s)"),
                __("- Transfert(s)"),
                __("- Radiation(s)"),
                __("Electeur(s) au")." ".$this->f->formatDate($datetableau),
                __("dont Hommes"),
                __("dont Femmes"),
            ),
            "data" => $data,
            "output" => "recapitulatif-tableau-rectificatif-".$datetableau,
        );
    }

    /**
     * VIEW - view_edition_pdf__statistiques_recapitulatif_tableau.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_recapitulatif_tableau() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__recapitulatif_tableau(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__etiquette_additions_des_jeunes.
     *
     * @return void
     */
    public function view_edition_pdf__etiquette_additions_des_jeunes() {
        $this->checkAccessibility();
        $params = array(
            "obj" => "additions_des_jeunes",
        );
        if (isset($_GET["type"])) {
            $params["type"] = $_GET["type"];
        }
        if (isset($_GET["datetableau"])) {
            $params["datetableau"] = $_GET["datetableau"];
        }
        if (isset($_GET["datej5"])) {
            $params["datej5"] = $_GET["datej5"];
        }
        //
        require_once "../obj/edition_pdf__etiquette.class.php";
        $inst_edition_pdf = new edition_pdf__etiquette();
        $pdf_edition = $inst_edition_pdf->compute_pdf__etiquette($params);
        //
        $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__recapitulatif_traitement.
     *
     * @return void
     */
    public function view_edition_pdf__recapitulatif_traitement() {
        $this->checkAccessibility();
        $usecase = "fallback";
        $params = array();
        if (isset($_GET["mode_edition"])) {
            $params["mode_edition"] = $_GET["mode_edition"];
        }
        if (isset($_GET["traitement"])) {
            if ($_GET["traitement"] == "annuel" || $_GET["traitement"] == "j5") {
                $params["traitement"] = $_GET["traitement"];
                if (isset($_GET["datetableau"])) {
                    $params["datetableau"] = $_GET["datetableau"];
                }
                if (isset($_GET["datej5"])) {
                    $params["datej5"] = $_GET["datej5"];
                }
                if (isset($_GET["mouvementatraiter"])) {
                    $params["mouvementatraiter"] = $_GET["mouvementatraiter"];
                }
                //
                $cinqjours = false;
                $additions = array();
                $mouvementatraiter_input = null;
                $mouvementatraiter = "";
                if (isset($params["mouvementatraiter"])) {
                    $mouvementatraiter_input = explode(";", $params["mouvementatraiter"]);
                }
                if ($mouvementatraiter_input != null) {
                    foreach ($mouvementatraiter_input as $elem) {
                        if ($elem == "Immediat") {
                            $cinqjours = true;
                        } else {
                            $additions[] = $elem;
                        }
                        $mouvementatraiter .= $elem.";";
                    }
                }
                $params["additions"] = $additions;
                $params["cinqjours"] = $cinqjours;
            }
        }
        //
        require_once "../obj/edition_pdf__recapitulatif_traitement.class.php";
        $inst_edition_pdf = new edition_pdf__recapitulatif_traitement();
        $pdf_edition = $inst_edition_pdf->compute_pdf__recapitulatif_traitement($params);
        //
        $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__listing_mouvements_tableau_commission.
     *
     * @return void
     */
    public function view_edition_pdf__listing_mouvements_tableau_commission() {
        $this->checkAccessibility();
        $params = array(
            'provenance_demande' => $this->f->get_submitted_get_value('provenance_demande'),
        );
        //
        require_once "../obj/edition_pdf__listing_mouvements_tableau_commission.class.php";
        $inst_edition_pdf = new edition_pdf__listing_mouvements_tableau_commission();
        $pdf_edition = $inst_edition_pdf->compute_pdf__listing_mouvements_tableau_commission($params);
        //
        $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }
}
