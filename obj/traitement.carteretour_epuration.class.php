<?php
/**
 * Ce script définit la classe 'carteretourEpurationTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'carteretourEpurationTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class carteretourEpurationTraitement extends traitement {

    var $fichier = "carteretour_epuration";

    var $champs = array(
        "date_debut",
        "date_fin"
    );

    function getDescription() {
        //
        return __("L'épuration des cartes en retour se fait en fonction de leur date de saisie.
            Le traitement se fait sur toutes les listes.
            <ul class=\"padding-left-20px\">
                <li>Si aucune date n'est sélectionnée, toutes les cartes en retour seront épurées.</li>
                <li>Si la date de début et la date de fin sont remplies, les cartes en retour dont la date de saisie est comprise dans cet intervalle seront supprimées.</li>
                <li>Si uniquement la date de début est sélectionnée, toutes les cartes en retour dont la date de saisie est postérieure à cette date seront supprimées.</li>
                <li>Si uniquement une date de fin est remplie, toutes les cartes en retour n'ayant pas de date de saisie ou ayant une date de saisie antérieure seront supprimées.</li>
            </ul>
        ");
    }

    function setContentForm() {
        // Champs permettant de saisir l'intervalle de date pour lequel on souhaite
        // supprimer les cartes en retour
        // date de saisie de la date de début
        $this->form->setLib("date_debut", __("Date de début"));
        $this->form->setType("date_debut", "date");
        $this->form->setTaille("date_debut", 10);
        $this->form->setMax("date_debut", 10);
        // date de saisie de la date de fin
        $this->form->setLib("date_fin", __("Date de fin"));
        $this->form->setType("date_fin", "date");
        $this->form->setTaille("date_fin", 10);
        $this->form->setMax("date_fin", 10);
    }

    function getValidButtonValue() {
        //
        return __("Epuration des cartes en retour");
    }

    function displayBeforeContentForm() {
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_carteretour_epuration.inc.php";
        //
        $res_carteretour = $this->f->db->query($sql_nbcarteretour);
        $this->f->isDatabaseError($res_carteretour);
        $nb_carteretour = $res_carteretour->numRows ();
        $res_carteretour->free();
        //
        echo "\n<div class=\"field\">\n\t<label>";
        echo __("Le nombre d'electeurs etant marques avec une carte en retour a la date du")." ";
        echo date('d/m/Y')." ".__("est de")." ".$nb_carteretour.".";
        echo "</label>\n</div>\n";
    }

    function treatment () {
        //
        $this->LogToFile("start carteretour_epuration");
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_carteretour_epuration.inc.php";
        // Traitement lié au retour du formulaire
        // 3 cas :
        //   1. Aucune date n'a été sélectionnée -> toutes les cartes en retour sont épurées
        //   2. Une date de fin a été saisi mais pas de date de début -> toutes les cartes en
        //      retour ayant une date de retour inférieure à la date de fin ou pas de date de
        //      retour sont épurées
        //   3. Une date de début et de fin ont été saisi -> toutes les cartes en retour ayant
        //      une date de retour comprise entre les deux dates saisies sont épurées.
        $dateDebut = isset($_POST["date_debut"]) ? $_POST["date_debut"] : '';
        $dateFin = isset($_POST["date_fin"]) ? $_POST["date_fin"] : '';
        if (! empty($dateDebut) && ! empty($dateFin)) {
            $sql_carteretour = str_replace(
                'WHERE',
                "WHERE
                    date_saisie_carte_retour >= '".
                    $dateDebut.
                    "' AND date_saisie_carte_retour <= '".
                    $dateFin.
                    "' AND ",
                $sql_carteretour
            );
        } elseif (! empty($dateFin)) {
            $sql_carteretour = str_replace(
                'WHERE',
                "WHERE
                    (date_saisie_carte_retour IS NULL OR
                    date_saisie_carte_retour <= '".
                    $dateFin.
                    "') AND ",
                $sql_carteretour
            );
        } elseif (! empty($dateDebut)) {
            $sql_carteretour = str_replace(
                'WHERE',
                "WHERE
                    date_saisie_carte_retour >= '".
                    $dateDebut.
                    "' AND ",
                $sql_carteretour
            );
        }
        //
        $res = $this->f->db->query($sql_carteretour);
        //
        if ($this->f->isDatabaseError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(__("Contactez votre administrateur."));
        } else {
            //
            $message = $this->f->db->affectedRows()." ".__("carte(s) en retour supprimee(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end carteretour_epuration");
    }
}


