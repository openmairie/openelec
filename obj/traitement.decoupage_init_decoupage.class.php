<?php
/**
 * Ce script définit la classe 'decoupageInitDecoupageTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'decoupageInitDecoupageTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class decoupageInitDecoupageTraitement extends traitement {

    var $fichier = "decoupage_init_decoupage";

    function getDescription() {
        //
        return __("Attention, il ne faut appliquer ce traitement qu'en ".
                 "connaissance de cause. Il permet pour une base qui n'a pas ".
                 "ete intialisee en decoupage de parcourir la table electeur ".
                 "pour creer le decoupage de maniere automatique pour toutes ".
                 "les voies qui ne sont affectees qu'a un seul bureau.");
    }

    function getValidButtonValue() {
        //
        return __("Initialiser la table de decoupage");
    }

    function treatment () {
        //
        $this->LogToFile("start decoupage_init_decoupage");
        // liste des voies dont tous les electeurs sont dans le meme bureau
        $sql_decoupage_init_decoupage = sprintf(
            'INSERT INTO %1$sdecoupage (id, bureau, code_voie, premier_impair, dernier_impair, premier_pair, dernier_pair)
            SELECT
                nextval(\'%1$sdecoupage_seq\'),
                (SELECT DISTINCT bureau FROM %1$selecteur where electeur.code_voie = voie.code),
                voie.code,
                1,99999,0,99998
            FROM %1$svoie
            WHERE voie.code in (
                SELECT code
                FROM (
                    SELECT DISTINCT
                    voie.code,
                    electeur.bureau
                    FROM %1$selecteur
                    LEFT OUTER JOIN %1$svoie ON electeur.code_voie = voie.code
                    LEFT OUTER JOIN %1$sdecoupage ON voie.code = decoupage.code_voie
                    WHERE voie.om_collectivite=%2$s AND decoupage.id IS NULL
                    GROUP BY voie.code, electeur.bureau
                ) AS voie_bureau
                GROUP BY code
                HAVING count(bureau)=1
            )',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        //
        $res = $this->f->db->query($sql_decoupage_init_decoupage);
        //
        if ($this->f->isDatabaseError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(__("Contactez votre administrateur."));
        } else {
            //
            $message = $this->f->db->affectedRows()." ".__("decoupage(s) insere(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end decoupage_init_decoupage");
    }
}


