<?php
/**
 * Ce script définit la classe 'edition_pdf__listing_mouvements_tableau_commission'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__listing_mouvements_tableau_commission' (edition_pdf).
 */
class edition_pdf__listing_mouvements_tableau_commission extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * L'édition générée ici est soit : un tableau rectificatif,
     * un tableau des additions, un tableau pour la commission. Les
     * différentes possibilités d'impression sont : pour un bureau,
     * sur toute la commune sans rupture par bureau, entre deux dates, ...
     * Le principe est que l'édition est un listing de mouvements
     * regroupé par catégories.
     *
     * @return array
     */
    public function compute_pdf__listing_mouvements_tableau_commission($params = array()) {

        // Condition sur la provenance de la demande du mouvement
        $where_provenance_demande = '';
        $provenance_demande_libelle = '';
        if (isset($params['provenance_demande']) === true
            && ($params['provenance_demande'] !== null
                || $params['provenance_demande'] !== '')) {
            //
            $provenance_demande_tab = explode(',', $params['provenance_demande']);
            //
            if (in_array('null', $provenance_demande_tab) === true) {
                unset($provenance_demande_tab[array_search('null', $provenance_demande_tab)]);
                $provenance_demande_tab[] = __('sans provenance identifiée');
            }
            $provenance_demande_libelle = implode(', ', $provenance_demande_tab);
            //
            $provenance_demande_tab = explode(',', $params['provenance_demande']);
            //
            $liste_provenance_demande = array();
            foreach ($provenance_demande_tab as $provenance_demande) {
                $liste_provenance_demande[] = sprintf('\'%s\'', $provenance_demande);
            }
            $where_provenance_demande = sprintf(
                'AND (mouvement.provenance_demande IN (%s) %s)',
                implode(', ', $liste_provenance_demande),
                (in_array('null', $provenance_demande_tab) === true ? ' OR mouvement.provenance_demande IS NULL ' : '')
            );
        }

        /**
         *
         */
        //
        $query_electeurs_actuels_groupby_bureau = sprintf(
            'SELECT
                bureau.code AS bureau,
                electeur.sexe AS sexe,
                count(electeur.id) AS total
            FROM
                %1$selecteur
                LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
            WHERE
                electeur.om_collectivite=%2$s
                AND electeur.liste=\'%3$s\'
            GROUP BY
                bureau.code,
                electeur.sexe
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"]
        );
        //
        $query_inscriptions_radiations_stats_groupby_bureau = sprintf(
            'SELECT
                mouvement.bureau_de_vote_code AS bureau,
                mouvement.date_tableau,
                param_mouvement.typecat,
                mouvement.etat,
                mouvement.sexe AS sexe,
                count(*) AS total
            FROM
                %1$smouvement
                INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
            WHERE
                mouvement.om_collectivite=%2$s
                AND mouvement.liste=\'%3$s\'
                AND 
                (
                    lower(param_mouvement.typecat)=\'inscription\'
                    OR lower(param_mouvement.typecat)=\'radiation\'
                )
                %4$s
            GROUP BY
                mouvement.bureau_de_vote_code,
                mouvement.date_tableau,
                param_mouvement.typecat,
                mouvement.etat,
                mouvement.sexe
            ORDER BY
                mouvement.bureau_de_vote_code,
                mouvement.date_tableau,
                param_mouvement.typecat,
                mouvement.etat,
                mouvement.sexe
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"],
            $where_provenance_demande
        );
        //
        $query_transferts_plus_stats_groupby_bureau = sprintf(
            'SELECT
                mouvement.bureau_de_vote_code AS bureau,
                mouvement.date_tableau,
                param_mouvement.typecat,
                mouvement.etat,
                mouvement.sexe AS sexe,
                count(*) AS total
            FROM
                %1$smouvement
                INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
            WHERE
                mouvement.om_collectivite=%2$s
                AND mouvement.liste=\'%3$s\'
                AND TRIM(LEADING \'0\' FROM mouvement.bureau_de_vote_code)<>TRIM(LEADING \'0\' FROM mouvement.ancien_bureau_de_vote_code)
                AND lower(param_mouvement.typecat)=\'modification\'
                %4$s
            GROUP BY
                mouvement.bureau_de_vote_code,
                mouvement.date_tableau,
                param_mouvement.typecat,
                mouvement.etat,
                mouvement.sexe
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"],
            $where_provenance_demande
        );
        //
        $query_transferts_moins_stats_groupby_bureau = sprintf(
            'SELECT
                mouvement.ancien_bureau_de_vote_code AS bureau,
                mouvement.date_tableau,
                param_mouvement.typecat,
                mouvement.etat, mouvement.sexe AS sexe,
                count(*) AS total
            FROM
                %1$smouvement
                INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
            WHERE
                mouvement.om_collectivite=%2$s
                AND mouvement.liste=\'%3$s\'
                AND TRIM(LEADING \'0\' FROM mouvement.bureau_de_vote_code)<>TRIM(LEADING \'0\' FROM mouvement.ancien_bureau_de_vote_code)
                AND lower(param_mouvement.typecat)=\'modification\'
                AND mouvement.ancien_bureau_de_vote_code<>\'\'
                %4$s
            GROUP BY
                mouvement.ancien_bureau_de_vote_code,
                mouvement.date_tableau,
                param_mouvement.typecat,
                mouvement.etat,
                mouvement.sexe
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"],
            $where_provenance_demande
        );
        // Nombre actuel d'electeurs dans la base de donnees par bureau
        // array("<BUREAU>" => array("M" => <>, "F" => <>, "total" => <>),);
        $electeurs_actuels_groupby_bureau = array();
        $res_electeurs_actuels_groupby_bureau = $this->f->db->query($query_electeurs_actuels_groupby_bureau);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_electeurs_actuels_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_electeurs_actuels_groupby_bureau);
        while ($row_electeurs_actuels_groupby_bureau =& $res_electeurs_actuels_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]])) {
                $electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]] = array();
            }
            //
            $electeurs_actuels_groupby_bureau[$row_electeurs_actuels_groupby_bureau["bureau"]][$row_electeurs_actuels_groupby_bureau["sexe"]] = $row_electeurs_actuels_groupby_bureau["total"];
        }
        foreach ($electeurs_actuels_groupby_bureau as $key => $value) {
            $electeurs_actuels_groupby_bureau[$key]["total"] = (isset($electeurs_actuels_groupby_bureau[$key]["M"]) ? $electeurs_actuels_groupby_bureau[$key]["M"] : 0) + (isset($electeurs_actuels_groupby_bureau[$key]["F"]) ? $electeurs_actuels_groupby_bureau[$key]["F"] : 0);
        }
        //
        $inscriptions_radiations_stats_groupby_bureau = array();
        $res_inscriptions_radiations_stats_groupby_bureau = $this->f->db->query($query_inscriptions_radiations_stats_groupby_bureau);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_inscriptions_radiations_stats_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_inscriptions_radiations_stats_groupby_bureau);
        while ($row_inscriptions_radiations_stats_groupby_bureau =& $res_inscriptions_radiations_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]])) {
                $inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]] = array();
            }
            //
            array_push($inscriptions_radiations_stats_groupby_bureau[$row_inscriptions_radiations_stats_groupby_bureau["bureau"]], $row_inscriptions_radiations_stats_groupby_bureau);
        }
        $this->f->addToLog(__METHOD__."(): ".print_r($inscriptions_radiations_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);
        //
        $transferts_plus_stats_groupby_bureau = array();
        $res_transferts_plus_stats_groupby_bureau = $this->f->db->query($query_transferts_plus_stats_groupby_bureau);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_transferts_plus_stats_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_transferts_plus_stats_groupby_bureau);
        while ($row_transferts_plus_stats_groupby_bureau =& $res_transferts_plus_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]])) {
                $transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]] = array();
            }
            //
            array_push($transferts_plus_stats_groupby_bureau[$row_transferts_plus_stats_groupby_bureau["bureau"]], $row_transferts_plus_stats_groupby_bureau);
        }
        $this->f->addToLog(__METHOD__."(): ".print_r($transferts_plus_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);
        //
        $transferts_moins_stats_groupby_bureau = array();
        $res_transferts_moins_stats_groupby_bureau = $this->f->db->query($query_transferts_moins_stats_groupby_bureau);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_transferts_moins_stats_groupby_bureau."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res_transferts_moins_stats_groupby_bureau);
        while ($row_transferts_moins_stats_groupby_bureau =& $res_transferts_moins_stats_groupby_bureau->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if (!isset($transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]])) {
                $transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]] = array();
            }
            //
            array_push($transferts_moins_stats_groupby_bureau[$row_transferts_moins_stats_groupby_bureau["bureau"]], $row_transferts_moins_stats_groupby_bureau);
        }
        $this->f->addToLog(__METHOD__."(): ".print_r($transferts_moins_stats_groupby_bureau, true), EXTRA_VERBOSE_MODE);
        //
        if (!function_exists('calculNombreDelecteursDateTableauBureau')) {
            /**
             *
             */
            function calculNombreDelecteursDateTableauBureau($electeurs_actuels_groupby_bureau, $inscriptions_radiations_stats_groupby_bureau, $transferts_plus_stats_groupby_bureau, $transferts_moins_stats_groupby_bureau, $date_tableau_reference, $bureau, $sexe = "ALL") {
                //
                if (!isset($electeurs_actuels_groupby_bureau[$bureau])) {
                    $cpt_electeur = 0;
                } else {
                    if ($sexe == "ALL") {
                        $cpt_electeur = $electeurs_actuels_groupby_bureau[$bureau]["total"];
                    } else {
                        $cpt_electeur = $electeurs_actuels_groupby_bureau[$bureau][$sexe];
                    }
                }
                //
                if (isset($inscriptions_radiations_stats_groupby_bureau[$bureau])) {
                    foreach ($inscriptions_radiations_stats_groupby_bureau[$bureau] as $element) {
                        if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                            //
                            if ($element["date_tableau"] > $date_tableau_reference) {
                                if ($element["etat"] == "trs") {
                                    if ($element["typecat"] == "Inscription") {
                                        $cpt_electeur -= $element["total"];
                                    } elseif ($element["typecat"] == "Radiation") {
                                        $cpt_electeur += $element["total"];
                                    }
                                }
                            } else {
                                if ($element["etat"] == "actif") {
                                    if ($element["typecat"] == "Inscription") {
                                        $cpt_electeur += $element["total"];
                                    } elseif ($element["typecat"] == "Radiation") {
                                        $cpt_electeur -= $element["total"];
                                    }
                                }
                            }
                        }
                    }
                }
                //
                if (isset($transferts_plus_stats_groupby_bureau[$bureau])) {
                    foreach ($transferts_plus_stats_groupby_bureau[$bureau] as $element) {
                        if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                            //
                            if ($element["date_tableau"] > $date_tableau_reference) {
                                if ($element["etat"] == "trs") {
                                    $cpt_electeur -= $element["total"];
                                }
                            } else {
                                if ($element["etat"] == "actif") {
                                    $cpt_electeur += $element["total"];
                                }
                            }
                        }
                    }
                }
                //
                if (isset($transferts_moins_stats_groupby_bureau[$bureau])) {
                    foreach ($transferts_moins_stats_groupby_bureau[$bureau] as $element) {
                        if ($sexe == "ALL" || $element["sexe"] == $sexe) {
                            //
                            if ($element["date_tableau"] > $date_tableau_reference) {
                                if ($element["etat"] == "trs") {
                                        $cpt_electeur += $element["total"];
                                }
                            } else {
                                if ($element["etat"] == "actif") {
                                    $cpt_electeur -= $element["total"];
                                }
                            }
                        }
                    }
                }
                return $cpt_electeur;
            }
        }

        /**
         * Gestion du nombre d'electeurs a des dates anterieures
         */
        //
        $query_count_all_mouvement_by_typecat = sprintf(
            'SELECT
                mouvement.date_tableau,
                mouvement.date_j5,
                param_mouvement.typecat,
                param_mouvement.effet,
                mouvement.types,
                param_mouvement.libelle,
                mouvement.etat, count(*)
            FROM %1$smouvement
            INNER JOIN %1$sparam_mouvement
                ON mouvement.types=param_mouvement.code
            WHERE mouvement.liste=\'%3$s\'
                AND mouvement.om_collectivite=%2$s
                %4$s
            GROUP BY
                mouvement.date_tableau,
                mouvement.date_j5,
                param_mouvement.typecat,
                param_mouvement.effet,
                mouvement.types,
                param_mouvement.libelle,
                mouvement.etat
            ORDER BY
                mouvement.date_tableau,
                mouvement.date_j5,
                param_mouvement.typecat,
                param_mouvement.effet,
                mouvement.types,
                param_mouvement.libelle,
                mouvement.etat',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"],
            $where_provenance_demande
        );
        //
        $res_count_all_mouvement_by_typecat = $this->f->db->query($query_count_all_mouvement_by_typecat);
        // Logger
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_count_all_mouvement_by_typecat."\")", VERBOSE_MODE);
        // Gestion des erreurs
        $this->f->isDatabaseError($res_count_all_mouvement_by_typecat);
        //
        $all_mouvement_by_typecat = array();
        while ($row_count_all_mouvement_by_typecat =& $res_count_all_mouvement_by_typecat->fetchRow(DB_FETCHMODE_ASSOC)) {
            array_push($all_mouvement_by_typecat, $row_count_all_mouvement_by_typecat);
        }
        $this->f->addToLog(__METHOD__."(): ".print_r($all_mouvement_by_typecat, true), EXTRA_VERBOSE_MODE);
        //
        if (!function_exists('calculNombreDelecteursDate')) {
            /**
             *
             */
            function calculNombreDelecteursDate($all_mouvement_by_typecat, $date_tableau_reference, $electeur_aujourdhui) {
                //
                $cpt_electeur = $electeur_aujourdhui;
                //
                foreach ($all_mouvement_by_typecat as $element) {
                    //
                    if ($element["date_tableau"] > $date_tableau_reference) {
                        if ($element["etat"] == "trs") {
                            if ($element["typecat"] == "Inscription") {
                                $cpt_electeur -= $element["count"];
                            } elseif ($element["typecat"] == "Radiation") {
                                $cpt_electeur += $element["count"];
                            }
                        }
                    } else {
                        if ($element["etat"] == "actif") {
                            if ($element["typecat"] == "Inscription") {
                                $cpt_electeur += $element["count"];
                            } elseif ($element["typecat"] == "Radiation") {
                                $cpt_electeur -= $element["count"];
                            }
                        }
                    }
                }
                return $cpt_electeur;
            }
        }

        /**
         *
         */
        //
        $edition_avec_recapitulatif = true;
        if (isset($_GET["edition_avec_recapitulatif"])) {
            $edition_avec_recapitulatif = ($_GET["edition_avec_recapitulatif"] == "false" ? false : true);
        }
        $mode_edition = "";
        $tableau = "";
        if (isset($_GET["datetableau"])) {
            $datetableau = $_GET["datetableau"];
        } else {
            $datetableau = $this->f->getParameter("datetableau");
        }
        if (isset($_GET["datej5"])) {
            $datej5 = $_GET["datej5"];
        } else {
            $datej5 = false;
        }
        if (isset($_GET["globale"])) {
            $globale = true;
        } else {
            $globale = false;
        }
        //
        $date_debut = "";
        $date_fin = "";
        // Gestion des dates Pour INTERNET EXPLORER 7
        if (isset($_GET['first']) && isset($_GET['last'])) {
            //
            if (preg_match("#([0-9]{1,2})([0-9]{1,2})([0-9]{4})#", $_GET ['first'], $regs)
                || preg_match("#([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})#", $_GET ['first'], $regs)
            ) {
                $annee = $regs[3];
                $mois = $regs [2];
                $jour = $regs [1];
                if (checkdate($mois, $jour, $annee)) {
                    $date_debut = $annee."-".$mois."-".$jour;
                }
            }
            //
            if (preg_match("#([0-9]{1,2})([0-9]{1,2})([0-9]{4})#", $_GET ['last'], $regs)
                || preg_match("#([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})#", $_GET ['last'], $regs)
            ) {
                $annee = $regs[3];
                $mois = $regs [2];
                $jour = $regs [1];
                if (checkdate($mois, $jour, $annee)) {
                    $date_fin = $annee."-".$mois."-".$jour;
                }
            }
            //
            if ($date_fin < $date_debut) {
                $date_debut = "";
                $date_fin = "";
            }
            //
            if ($date_debut == "" || $date_fin == "") {
                die("Les dates sont incorrectes");
            }
        }
        $libelle_commune = $this->f->collectivite["ville"];
        $libelle_liste = $_SESSION["libelle_liste"];
        $pagedebut_commission = array();

        /**
         * Recuperation des informations sur les bureaux de vote
         */

        // Initialisation
        $liste_bureaux = array();

        // Si ($globale == false), alors l'édition doit se faire avec une rupture par bureau
        // Donc on remplit la variable $liste_bureaux avec tous les bureaux de vote de la commune
        if ($globale == false) {
            //
            if ($edition_avec_recapitulatif == true) {
                $liste_bureaux = array(
                    0 => array(
                        "bureau" => "ALL",
                        "bureau_libelle" => "",
                        "canton_libelle" => "",
                        "circonscription_libelle" => "",
                    )
                );
            }
            // Inclut les requêtes
            // passage == 0 signifit que c'est la première inclusion du fichier
            // cette première inclusion vise à récupérer la requête :
            //  - $query_liste_bureaux
            $passage = 0;
            //
            $query_liste_bureaux = sprintf(
                'SELECT
                    bureau.code AS bureau,
                    canton.code as canton_code,
                    bureau.libelle AS bureau_libelle,
                    canton.libelle as canton_libelle,
                    circonscription.libelle as circonscription_libelle
                FROM
                    %1$sbureau
                    LEFT JOIN %1$scanton ON bureau.canton=canton.id
                    LEFT JOIN %1$scirconscription ON bureau.circonscription=circonscription.id
                WHERE
                    bureau.om_collectivite=%2$s
                ORDER BY
                    bureau.code
                ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            );
            //
            $res = $this->f->db->query($query_liste_bureaux);
            //
            $this->f->addToLog(__METHOD__."(): db->query(\"".$query_liste_bureaux."\")", VERBOSE_MODE);
            //
            $this->f->isDatabaseError($res);
            //
            while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                array_push($liste_bureaux, $row);
            }
            //
            $res->free();
            //
            $this->f->addToLog(__METHOD__."(): \$liste_bureaux = ".print_r($liste_bureaux, true), EXTRA_VERBOSE_MODE);

            /**
             *
             */
            //
            if (isset($_GET['idx'])) {
                //
                $liste_bureaux_temp = array();
                foreach ($liste_bureaux as $bureau) {
                    if ($bureau["bureau"] == $_GET["idx"]) {
                        array_push($liste_bureaux_temp, $bureau);
                    }
                }
                if (count($liste_bureaux_temp) == 1) {
                    $liste_bureaux = $liste_bureaux_temp;
                } else {
                    $this->f->notExistsError(__("Les parametres ne sont pas corrects."));
                }
            }
        } else {
            // Si ($globale == true), alors l'édition se fait sans rupture par bureau
            // Donc on remplit la variable $liste_bureaux avec une unique ligne : "ALL"
            $liste_bureaux = array(
                0 => array(
                    "bureau" => "ALL",
                    "bureau_libelle" => "",
                    "canton_libelle" => "",
                    "circonscription_libelle" => "",
                ),
            );
        }

        /**
         * Initialisation des listes en cas de rupture par liste
         */
        $liste_listes = array(
            0 => array(
                "liste" => "ALL",
            ),
        );

        //
        (!isset($_GET["mode_edition"]) ? $_GET["mode_edition"] = "commission" : "");
        //
        switch ($_GET["mode_edition"]) {
            //
            case "tableau":
                //
                $mode_edition = "tableau";
                //
                (!isset($_GET["tableau"]) ? $tableau = "tableau_rectificatif" : "");
                //
                switch ($_GET["tableau"]) {
                    //
                    case "tableau_rectificatif":
                        //
                        $tableau = "tableau_rectificatif";
                        //
                        $filename_objet = "tableau-rectificatif";
                        $titre_libelle_ligne1 = __("TABLEAU RECTIFICATIF DU")." ".$this->f->formatDate($datetableau);
                        $titre_libelle_ligne2 = "";
                        $pagefin_libelle = sprintf(
                            __("Arrete le tableau du %s a :"),
                            $this->f->formatDate($datetableau)
                        );
                        //
                        $mouvement_categories = array("Inscription", "Radiation",);
                        //
                        break;
                    //
                    case "tableau_des_cinq_jours":
                        //
                        $tableau = "tableau_des_cinq_jours";
                        //
                        $filename_objet = "tableau-des-cinq-jours";
                        $titre_libelle_ligne1 = __("TABLEAU DES CINQ JOURS DU")." ".($datej5 == false ? date("d/m/Y") : $this->f->formatDate($datej5));
                        $titre_libelle_ligne2 = sprintf(
                            __("[Pour le tableau rectificatif du %s]"),
                            $this->f->formatDate($datetableau)
                        );
                        $pagefin_libelle = __("Nombre :");
                        //
                        $mouvement_categories = array("Inscription", "Radiation",);
                        //
                        break;
                    //
                    case "tableau_des_additions_des_jeunes":
                        //
                        (!isset($_GET["type"]) ? $this->f->notExistsError(__("Les parametres ne sont pas corrects.")) : "");
                        //
                        $query_libelle_mouvement = sprintf(
                            'SELECT libelle FROM %1$sparam_mouvement WHERE code=\'%2$s\'',
                            DB_PREFIXE,
                            addslashes($_GET["type"])
                        );
                        $libelle_mouvement = $this->f->db->getone($query_libelle_mouvement);
                        $this->f->addToLog(
                            __METHOD__."(): db->getone(\"".$query_libelle_mouvement."\")",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($libelle_mouvement);
                        //
                        $tableau = "tableau_des_additions_des_jeunes";
                        //
                        $filename_objet = "tableau-des-additions-des-jeunes";
                        $titre_libelle_ligne1 = __("TABLEAU DES ADDITIONS : JEUNES DE 18 ANS (L11-2)");
                        $titre_libelle_ligne2 = sprintf(
                            __("%s [Pour le tableau rectificatif du %s]"),
                            $libelle_mouvement,
                            $this->f->formatDate($datetableau)
                        );
                        $pagefin_libelle = __("Nombre :");
                        //
                        $mouvement_categories = array("Inscription",);
                        //
                        $type_mouvement_io = $_GET["type"];
                        //
                        break;
                };
                break;
            //
            case "commission":
                //
                $mode_edition = "commission";
                //
                (!isset($_GET["commission"]) ? $_GET["commission"] = "globale" : "");
                //
                switch ($_GET["commission"]) {
                    //
                    case "globale":
                        //
                        $commission = "globale";
                        //
                        $filename_objet = "commission";
                        $titre_libelle_ligne1 = sprintf(
                            __("ETAT POUR LA COMMISSION, TABLEAU DU %s"),
                            $this->f->formatDate($datetableau)
                        );
                        $titre_libelle_ligne2 = "";
                        $pagefin_libelle = sprintf(
                            __("Arrete le tableau du %s a"),
                            $this->f->formatDate($datetableau)
                        );
                        //
                        $mouvement_categories = array("Inscription", "Radiation",);
                        break;
                    case "entre_deux_dates":
                        //
                        $commission = "entre_deux_dates";
                        //
                        $filename_objet = "commission-entre-deux-dates";
                        $titre_libelle_ligne1 = __("ETAT POUR LA COMMISSION");
                        $titre_libelle_ligne2 = sprintf(
                            __("Mouvements du %s au %s"),
                            $this->f->formatDate($date_debut),
                            $this->f->formatDate($date_fin)
                        );
                        $pagefin_libelle = '';
                        //
                        $mouvement_categories = array("Inscription", "Radiation",);
                        // Rupture par liste
                        $liste_listes = array(
                            0 => array(
                                'liste' => '\'01\'',
                                'libelle_liste' => 'LISTE PRINCIPALE',
                                'liste_insee' => 'LP',
                            ),
                            1 => array(
                                'liste' => '\'02\', \'04\'',
                                'libelle_liste' => 'LISTE COMPLÉMENTAIRE EUROPÉENNE',
                                'liste_insee' => 'LCE',
                            ),
                            2 => array(
                                'liste' => '\'03\', \'04\'',
                                'libelle_liste' => 'LISTE COMPLÉMENTAIRE MUNICIPALE',
                                'liste_insee' => 'LCM',
                            ),
                        );
                        break;
                };
                break;
        };


        //


        //
        $sans_modification = $this->f->is_option_commission_sans_modification_enabled();

        /**
         * Ouverture du fichier pdf
         */
        //
        require_once "../obj/fpdf_table.class.php";
        //
        $pdf = new PDF('L', 'mm', 'A4');
        //
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(false);
        $pdf->SetFont(
            'Arial',
            '',
            7
        );
        $pdf->SetDrawColor(30, 7, 146);
        $pdf->SetMargins(10, 10, 10);
        $pdf->SetDisplayMode('real', 'single');

        /**
         * BOUCLE SUR CHAQUE BUREAU
         */
        //
        foreach ($liste_bureaux as $bureau) {
            // var_dump($liste_listes);
            foreach ($liste_listes as $liste) {
                /**
                 *
                 */
                // Initialise les variables du bureau et du canton
                $bureau_code = $bureau["bureau"];
                $bureau_libelle = $bureau["bureau_libelle"];
                $canton_libelle = $bureau["canton_libelle"];
                $circonscription_libelle = $bureau["circonscription_libelle"];

                // Initialise les variables de la liste
                $liste_id = $liste['liste'];
                if ($liste_id != "ALL") {
                    $liste_libelle = $liste['libelle_liste'];
                    $liste_insee = $liste['liste_insee'];
                }

                // TABLES
                $critere_table_commun = sprintf(
                    ' FROM %1$smouvement
                    LEFT JOIN %1$sparam_mouvement ON mouvement.types = param_mouvement.code ',
                    DB_PREFIXE
                );

                // SELECTION COMMUNE
                $critere_where_commun = sprintf(
                    ' WHERE %s AND %s %s %s %s ',
                    sprintf('mouvement.om_collectivite=%s', intval($_SESSION["collectivite"])),
                    sprintf('mouvement.date_tableau=\'%s\'', $datetableau),
                    $where_provenance_demande,
                    (($mode_edition == "commission" && $commission == "entre_deux_dates" ) ? 'AND mouvement.statut=\'accepte\'' : ''),
                    ($liste_id !== "ALL" ? sprintf(' AND mouvement.liste IN (%s) ', $liste_id) : '')
                );

                // SELECTION ADDITION
                $critere_where_inscription = " AND ((lower(param_mouvement.typecat)='inscription' ";
                if ($bureau_code != "ALL") {
                    $critere_where_inscription .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                }
                $critere_where_inscription .= " ) ";
                //
                if ($globale !== true) {
                    if ($sans_modification === true) {
                        //
                        $critere_where_inscription .= " OR (lower(param_mouvement.typecat)='modification' ";
                        if ($bureau_code != "ALL") {
                            $critere_where_inscription .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                        $critere_where_inscription .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code)!=TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code))) ";
                    } else {
                        //
                        $critere_where_inscription .= " OR (lower(param_mouvement.typecat)='modification' ";
                        if ($bureau_code != "ALL") {
                            $critere_where_inscription .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                        $critere_where_inscription .= " )) ";
                    }
                } else {
                    $critere_where_inscription .= " ) ";
                }

                // SELECTION RADIATION
                $critere_where_radiation = " AND ((lower(param_mouvement.typecat)='radiation' ";
                if ($bureau_code != "ALL") {
                    $critere_where_radiation .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                }
                $critere_where_radiation .= " ) ";
                //
                if ($globale !== true) {
                    if ($sans_modification === true) {
                        //
                        $critere_where_radiation .= " OR (lower(param_mouvement.typecat)='modification' ";
                        if ($bureau_code != "ALL") {
                            $critere_where_radiation .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                        $critere_where_radiation .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) != TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code))) ";
                    } else {
                        //
                        $critere_where_radiation .= " OR (lower(param_mouvement.typecat)='modification' ";
                        if ($bureau_code != "ALL") {
                            $critere_where_radiation .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                        $critere_where_radiation .= " )) ";
                    }
                } else {
                    $critere_where_radiation .= " ) ";
                }

                // SELECTION DATES
                $critere_filtre_dates_inscription = "
                    AND (mouvement.date_j5 >= '".$date_debut."'
                    AND mouvement.date_j5 <= '".$date_fin."')
                ";
                $critere_filtre_dates_radiation = "
                    AND (mouvement.date_j5 >= '".$date_debut."'
                    AND mouvement.date_j5 <= '".$date_fin."')
                ";

                /**
                 * COMPOSITION DES PREMIERES PAGES
                 */
                // Inclut les requêtes
                // passage == 1 signifit que c'est la seconde inclusion du fichier
                // cette seconde inclusion vise à récupérer les requêtes :
                //  - $nbins
                //  - $nbins_actif
                //  - $nbrad
                //  - $nbrad_actif
                //  - $nbelec
                //  - $nb_addition_dtsup
                //  - $nb_radiation_dtsup
                //  - $query_nb_additions
                //  - $query_nb_radiations
                //
                if ($mode_edition == "tableau"
                    && $tableau == "tableau_des_additions_des_jeunes"
                ) {
                    if ($datej5 == false) {
                        //
                        $query_nb_additions = "SELECT count(*) ";
                        $query_nb_additions .= $critere_table_commun;
                        $query_nb_additions .= $critere_where_commun;
                        $query_nb_additions .= " AND lower(param_mouvement.typecat)='inscription' ";
                        $query_nb_additions .= " AND param_mouvement.effet='Election' ";
                        $query_nb_additions .= " AND mouvement.etat='actif' ";
                        $query_nb_additions .= " AND mouvement.types='".$type_mouvement_io."' ";
                        if ($bureau_code != "ALL") {
                            $query_nb_additions .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                    } else {
                        //
                        $query_nb_additions = "SELECT count(*) ";
                        $query_nb_additions .= $critere_table_commun;
                        $query_nb_additions .= $critere_where_commun;
                        $query_nb_additions .= " AND lower(param_mouvement.typecat)='inscription' ";
                        $query_nb_additions .= " AND mouvement.etat='trs' ";
                        $query_nb_additions .= " AND mouvement.types='".$type_mouvement_io."' ";
                        if ($bureau_code != "ALL") {
                            $query_nb_additions .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                        $query_nb_additions .= " AND mouvement.date_j5='".$datej5."' ";
                    }

                } elseif ($mode_edition == "tableau"
                    && $tableau == "tableau_des_cinq_jours"
                ) {
                    if ($datej5 == false) {
                        //
                        $query_nb_additions = "SELECT count(*) ";
                        $query_nb_additions .= $critere_table_commun;
                        $query_nb_additions .= $critere_where_commun;
                        $query_nb_additions .= " AND lower(param_mouvement.typecat)='inscription' ";
                        $query_nb_additions .= " AND param_mouvement.effet='Immediat' ";
                        $query_nb_additions .= " AND mouvement.etat='actif' ";
                        if ($bureau_code != "ALL") {
                            $query_nb_additions .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                        //
                        $query_nb_radiations = "SELECT count(*) ";
                        $query_nb_radiations .= $critere_table_commun;
                        $query_nb_radiations .= $critere_where_commun;
                        $query_nb_radiations .= " AND lower(param_mouvement.typecat)='radiation' ";
                        $query_nb_radiations .= " AND param_mouvement.effet='Immediat' ";
                        $query_nb_radiations .= " AND mouvement.etat='actif' ";
                        if ($bureau_code != "ALL") {
                            $query_nb_radiations .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                    } else {
                        //
                        $query_nb_additions = "SELECT count(*) ";
                        $query_nb_additions .= $critere_table_commun;
                        $query_nb_additions .= $critere_where_commun;
                        $query_nb_additions .= " AND lower(param_mouvement.typecat)='inscription' ";
                        $query_nb_additions .= " AND param_mouvement.effet<>'Election' ";
                        $query_nb_additions .= " AND mouvement.date_j5='".$datej5."' ";
                        $query_nb_additions .= " AND mouvement.etat='trs' ";
                        if ($bureau_code != "ALL") {
                            $query_nb_additions .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                        //
                        $query_nb_radiations = "SELECT count(*) ";
                        $query_nb_radiations .= $critere_table_commun;
                        $query_nb_radiations .= $critere_where_commun;
                        $query_nb_radiations .= " AND lower(param_mouvement.typecat)='radiation' ";
                        $query_nb_radiations .= " AND param_mouvement.effet<>'Election' ";
                        $query_nb_radiations .= " AND mouvement.date_j5='".$datej5."' ";
                        $query_nb_radiations .= " AND mouvement.etat='trs' ";
                        if ($bureau_code != "ALL") {
                            $query_nb_radiations .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                    }

                } elseif ($mode_edition == "commission"
                    && $commission == "entre_deux_dates"
                ) {
                    //// COMMISSION ENTRE DEUX DATES

                    //
                    $query_nb_additions = "SELECT count(*) ";
                    $query_nb_additions .= $critere_table_commun;
                    $query_nb_additions .= $critere_where_commun;
                    $query_nb_additions .= $critere_where_inscription;
                    $query_nb_additions .= $critere_filtre_dates_inscription;

                    //
                    $query_nb_radiations = "SELECT count(*) ";
                    $query_nb_radiations .= $critere_table_commun;
                    $query_nb_radiations .= $critere_where_commun;
                    $query_nb_radiations .= $critere_where_radiation;
                    $query_nb_radiations .= $critere_filtre_dates_radiation;

                } else {
                    // Requete de recuperation du nombre d'additions a la date de tableau en cours
                    $nbins = " SELECT count(*) ";
                    $nbins .= $critere_table_commun;
                    $nbins .= $critere_where_commun;
                    if ($bureau_code != "ALL") {
                        $nbins .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                    }
                    // Avec une option pour la prise en compte ou non des modifications
                    if ($sans_modification === true) {
                        $nbins .= " AND (lower(param_mouvement.typecat)='inscription' or ";
                        $nbins .= " (lower(param_mouvement.typecat)='modification' ";
                        if ($bureau_code != "ALL") {
                            $nbins .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                        $nbins .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) != TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code))) ";
                    } else {
                        $nbins .= " and (lower(param_mouvement.typecat)='inscription' or lower(param_mouvement.typecat)='modification') ";
                    }

                    // Requete de recuperation du nombre d'additions actives a la date de tableau en cours
                    // Avec une option pour la prise en compte ou non des modifications
                    $nbins_actif = $nbins." and mouvement.etat='actif'";

                    // Requete de recuperation du nombre de radiations a la date de tableau en cours
                    $nbrad = " SELECT count(*) ";
                    //
                    $nbrad .= $critere_table_commun;
                    $nbrad .= $critere_where_commun;
                    //
                    $nbrad .= "and ((lower(param_mouvement.typecat)='radiation'";
                    if ($bureau_code != "ALL") {
                        $nbrad .= "AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."')";
                    }
                    $nbrad .= ") ";
                    // Avec une option pour la prise en compte ou non des modifications
                    if ($sans_modification === true) {
                        $nbrad .= " or (lower(param_mouvement.typecat)='modification' ";
                        if ($bureau_code != "ALL") {
                            $nbrad .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        }
                        $nbrad .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) != TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code))) ";
                    } else {
                        $nbrad .= " or (lower(param_mouvement.typecat)='modification' ";
                        if ($bureau_code != "ALL") {
                            $nbrad .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."')";
                        }
                        $nbrad .= " )) ";
                    }

                    // Requete de recuperation du nombre de radiations actives a la date de tableau en cours
                    // Avec une option pour la prise en compte ou non des modifications
                    $nbrad_actif = $nbrad." and mouvement.etat='actif'";

                    // Requete de recuperation du nombre de l'electeur a l'instant t dans la table electeur
                    $nbelec = sprintf(
                        'SELECT
                            count(*)
                        FROM
                            %1$selecteur
                            LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id
                        WHERE
                            electeur.om_collectivite=%2$s
                            AND electeur.liste=\'%3$s\'
                            %4$s',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        $_SESSION["liste"],
                        ($bureau_code != "ALL" ? " AND bureau.code='".$this->f->db->escapesimple($bureau_code)."' " : "")
                    );

                    // Requete de recuperation du nombre de radiations de tous les mouvements
                    // traites a une date superieure ou egale a la date de tableau en cours
                    $nb_radiation_dtsup = "select count(*) as nbr ";
                    $nb_radiation_dtsup .= sprintf(
                        ' FROM %1$smouvement 
                        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code ',
                        DB_PREFIXE
                    );
                    $nb_radiation_dtsup .= "where mouvement.date_tableau>='".$datetableau."' ";
                    $nb_radiation_dtsup .= " and mouvement.liste='".$_SESSION["liste"]."'";
                    $nb_radiation_dtsup .= " and mouvement.om_collectivite=".intval($_SESSION["collectivite"])." ";
                    $nb_radiation_dtsup .= "AND ((lower(param_mouvement.typecat)='radiation' and TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."')) ";
                    // Avec une option pour la prise en compte ou non des modifications
                    if ($sans_modification === true) {
                        $nb_radiation_dtsup .= " or (lower(param_mouvement.typecat)='modification' and ";
                        $nb_radiation_dtsup .= " TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        $nb_radiation_dtsup .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) != TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code))) ";
                    } else {
                        $nb_radiation_dtsup .= " or (lower(param_mouvement.typecat)='modification' ";
                        $nb_radiation_dtsup .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."'))) ";
                    }
                    $nb_radiation_dtsup .= " and mouvement.etat='trs'";

                    // Requete de recuperation du nombre d'additions de tous les mouvements
                    // traites a une date superieure ou egale a la date de tableau en cours
                    $nb_addition_dtsup = "select count(*) as nbr ";
                    $nb_addition_dtsup .= sprintf(
                        ' FROM %1$smouvement 
                        INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code ',
                        DB_PREFIXE
                    );
                    $nb_addition_dtsup .= "where mouvement.date_tableau>='".$datetableau."' ";
                    $nb_addition_dtsup .= "AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') and ";
                    $nb_addition_dtsup .= "mouvement.om_collectivite=".intval($_SESSION["collectivite"])." ";
                    $nb_addition_dtsup .= " and mouvement.liste='".$_SESSION["liste"]."'";
                    // Avec une option pour la prise en compte ou non des modifications
                    if ($sans_modification === true) {
                        $nb_addition_dtsup .= " and (lower(param_mouvement.typecat)='inscription' or ";
                        $nb_addition_dtsup .= " (lower(param_mouvement.typecat)='modification' ";
                        $nb_addition_dtsup .= " AND TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code) = TRIM(LEADING '0' FROM '".$this->f->db->escapesimple($bureau_code)."') ";
                        $nb_addition_dtsup .= " AND TRIM(LEADING '0' FROM mouvement.ancien_bureau_de_vote_code) != TRIM(LEADING '0' FROM mouvement.bureau_de_vote_code))) ";
                    } else {
                        $nb_addition_dtsup .= " and (lower(param_mouvement.typecat)='inscription' or lower(param_mouvement.typecat)='modification') ";
                    }
                    $nb_addition_dtsup .= " and mouvement.etat='trs'";
                }
                //
                if ($mode_edition == "tableau" && $tableau == "tableau_rectificatif"
                    || $mode_edition == "commission" && $commission == "globale"
                ) {
                    //// L'édition pour la commission globale ou pour le tableau
                    //// rectificatif sont identiques sauf le titre de la page.
                    //// XXX Deux titres pour une même édition, est-ce vraiment utile ?
                    /**
                     *
                     */
                    $inst_revision = $this->f->get_inst__om_dbform(array(
                        "obj" => "revision",
                    ));
                    $datetableau_precedente = $inst_revision->get_previous_date_tableau($datetableau);
                    // Inscriptions
                    $nb_add = $this->f->db->getone($nbins);
                    $this->f->isDatabaseError($nb_add);
                    // Inscriptions dans l'etat actif
                    $nb_add_actif = $this->f->db->getone($nbins_actif);
                    $this->f->isDatabaseError($nbins_actif);
                    // Radiations
                    $nb_rad = $this->f->db->getone($nbrad);
                    $this->f->isDatabaseError($nb_rad);
                    // Radiations dans l'etat actif
                    $nb_rad_actif = $this->f->db->getone($nbrad_actif);
                    $this->f->isDatabaseError($nb_rad_actif);
                    // Electeurs
                    $nb_elec = $this->f->db->getone($nbelec);
                    $this->f->isDatabaseError($nb_elec);
                    // Recuperation du nombre d'additions traitees aux dates de tableau superieures
                    // ou egales a la date de tableau actuelle
                    $res_nb_addition_dtsup = $this->f->db->getOne($nb_addition_dtsup);
                    $this->f->isDatabaseError($res_nb_addition_dtsup);
                    // Recuperation du nombre de radiations traitees aux dates de tableau
                    // superieures ou egales a la date de tableau actuelle
                    $res_nb_radiation_dtsup = $this->f->db->getOne($nb_radiation_dtsup);
                    $this->f->isDatabaseError($res_nb_radiation_dtsup);
                    // Calcul du nombre d'électeurs à la date de tableau précédente
                    // Au tableau precedent le nombre d'electeurs est egal au nombre d'electeurs
                    // dans la table electeur a l'heure actuelle auquel on enleve toutes les
                    // additions traitees aux dates de tableau superieures ou egales a la date
                    // de tableau actuelle et auquel on ajoute toutes les radiations traitees
                    // aux dates de tableau superieures ou egales a la date de tableau actuelle
                    if ($bureau_code == "ALL") {
                        $nb = calculNombreDelecteursDate($all_mouvement_by_typecat, $datetableau_precedente, $nb_elec);
                    } else {
                        $nb = calculNombreDelecteursDateTableauBureau($electeurs_actuels_groupby_bureau, $inscriptions_radiations_stats_groupby_bureau, $transferts_plus_stats_groupby_bureau, $transferts_moins_stats_groupby_bureau, $datetableau_precedente, $bureau_code);
                    }
                    // Calcul du nombre d'électeurs à la date de tableau suivante
                    // Au tableau suivant le nombre d'electeurs est egal au nombre d'electeurs
                    // au tableau precedent calcule juste au dessus auquel on ajoute toutes
                    // les additions de la date de tableau en cours et auquel on enleve toutes
                    // les radiations de la date de tableau en cours
                    $nb_newelec = $nb + $nb_add - $nb_rad;
                    //
                    $pagedebut = array(
                        //
                        array(sprintf(__("Nombre d'inscrits au %s : "), $this->f->formatdate($datetableau_precedente)),
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(strval($nb),
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(__("Additions : "),
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array($nb_add,
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(__("Radiations : "),
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array($nb_rad,
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(sprintf(__("Nombre d'inscrits au %s : "), $this->f->formatDate($datetableau)),
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array($nb_newelec,
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(sprintf(__("Arrete le tableau du %s %s au nombre de %s electeurs."), $this->f->formatDate($datetableau), ($bureau_code != "ALL" ? __(" du bureau no").$bureau_code : ""), $nb_newelec),
                              260, 20, 0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array("",
                              260, 25, 2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                    );
                } elseif ($mode_edition == "tableau" && $tableau == "tableau_des_cinq_jours") {
                    //
                    $nb_additions = $this->f->db->getone($query_nb_additions);
                    $this->f->addToLog(__METHOD__."(): db->getone(\"".$query_nb_additions."\")", VERBOSE_MODE);
                    $this->f->isDatabaseError($nb_additions);
                    //
                    $nb_radiations = $this->f->db->getone($query_nb_radiations);
                    $this->f->addToLog(__METHOD__."(): db->getone(\"".$query_nb_radiations."\")", VERBOSE_MODE);
                    $this->f->isDatabaseError($nb_radiations);
                    //
                    $pagedebut = array(
                        //
                        array("",
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array("",
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(__("Additions : "),
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array($nb_additions,
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(__("Radiations : "),
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array($nb_radiations,
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array("",
                              260, 35, 2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                    );

                } elseif ($mode_edition == "commission" && $commission == "entre_deux_dates") {
                    //
                    $nb_additions = $this->f->db->getone($query_nb_additions);
                    $this->f->addToLog(__METHOD__."(): db->getone(\"".$query_nb_additions."\")", VERBOSE_MODE);
                    $this->f->isDatabaseError($nb_additions);
                    //
                    $nb_radiations = $this->f->db->getone($query_nb_radiations);
                    $this->f->addToLog(__METHOD__."(): db->getone(\"".$query_nb_radiations."\")", VERBOSE_MODE);
                    $this->f->isDatabaseError($nb_radiations);
                    //
                    $pagedebut = array(
                        //
                        array("",
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(
                            ($liste_id !== "ALL" ? sprintf(__('Liste : %s - %s'), $liste_insee, $liste_libelle) : ''),
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(__("Additions : "),
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array($nb_additions,
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(__("Radiations : "),
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array($nb_radiations,
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        array("",
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        array(($provenance_demande_libelle !== '' ? sprintf(__('Provenances : %s'), $provenance_demande_libelle) : ''),
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array("",
                              260, 35, 2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                    );

                } elseif ($mode_edition == "tableau" && $tableau == "tableau_des_additions_des_jeunes") {
                    //
                    $nb_additions = $this->f->db->getone($query_nb_additions);
                    $this->f->addToLog(__METHOD__."(): db->getone(\"".$query_nb_additions."\")", VERBOSE_MODE);
                    $this->f->isDatabaseError($nb_additions);
                    //
                    $pagedebut = array(
                        //
                        array("",
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array("",
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array(__("Additions : "),
                              160, 10, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array($nb_additions,
                              30, 10, 1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        //
                        array("",
                              260, 45, 2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                    );
                }

                /**
                 * COMPOSITION DU FICHIER PDF
                 *
                 * $mouvement_categories
                 * $titre_libelle_ligne1
                 * $titre_libelle_ligne2
                 * $libelle_commune
                 * $canton_libelle
                 * $bureau_libelle
                 * $libelle_liste
                 * $bureau_code
                 * $pagefin_libelle
                 * $pagedebut
                 *
                 */
                // PARAM
                $param = array(
                    7,
                    9,
                    10,
                    1,
                    1,
                    7,
                    7,
                    10,
                    10,
                    1,
                    1,
                );
                $page_content_width = 277;
                // Variable de developpement pour afficher les bordures de toutes les cellules
                $allborders = false;
                // ENTETE DES COLONNES
                $entete_height = 8;
                // PAGEFIN
                $pagefin_height = 9;
                $pagefin_border = "T";
                // DATAS
                $datas_height = 20;
                //-------------------------------------------------------------------//
                // TITRE (PAGE HEADER)
                $page_header_line_height = 4;
                $page_header = $this->get_page_header_config(array(
                    "allborders" => $allborders,
                    "page_content_width" => $page_content_width,
                    "titre_libelle_ligne1" => $titre_libelle_ligne1,
                    "titre_libelle_ligne2" => $titre_libelle_ligne2,
                    "libelle_commune" => $libelle_commune,
                    "libelle_liste" => ($liste_id !== "ALL" ? sprintf('Liste : %s - %s', $liste_insee, $liste_libelle) : $libelle_liste),
                    "bureau_code" => $bureau_code,
                    "bureau_libelle" => $bureau_libelle,
                    "canton_libelle" => $canton_libelle,
                    "circonscription_libelle" => $circonscription_libelle,
                    "page_header_line_height" => $page_header_line_height,
                ));
                // FIRSTPAGE
                $firstpage = array_merge(
                    array(
                        array(
                            " ",
                            $page_content_width, 1, 1, ($allborders == true ? 1 : "B"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                        array(
                            " ",
                            $page_content_width, 1, 1, ($allborders == true ? 1 : "T"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                        array(
                            $titre_libelle_ligne1,
                            $page_content_width, 10, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',18,1,0),
                        array(
                            $titre_libelle_ligne2,
                            $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        array(
                            "",
                            $page_content_width, 5, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                        array(
                            sprintf(__("Commune : %s"), $libelle_commune),
                            $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                        array(
                            ($bureau_code != "ALL" ? sprintf(__("Bureau : %s - %s"), $bureau_code, $bureau_libelle) : ""),
                            $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                        array(
                            "",
                            $page_content_width, 16, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                    ),
                    //
                    $pagedebut,
                    //
                    array(
                        //
                        array("",
                              $page_content_width, $pagefin_height, 1, "", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                        //
                        array(" ",
                              $page_content_width, 1, 1, "B", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                        //
                        array(" ",
                              $page_content_width, 1, 1, "T", 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                        //
                        array(
                            (($mode_edition == "commission" && $commission == "entre_deux_dates" ) ? '' : $libelle_commune.' le '.date("d/m/Y")),
                              230, $pagefin_height+5, 1, "0", 'R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                        //
                        array(
                            (($mode_edition == "commission" && $commission == "entre_deux_dates" ) ? '' : __("Le Maire")),
                            $page_content_width/3, $pagefin_height, 0, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
                        //
                        array(
                            (($mode_edition == "commission" && $commission == "entre_deux_dates" ) ? '' : __("Le delegue du prefet")),
                            $page_content_width/3, $pagefin_height, 0, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
                        //
                        array((($mode_edition == "commission" && $commission == "entre_deux_dates" ) ? '' : __("Le delegue du TGI")),
                            $page_content_width/3, $pagefin_height, 0, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
                    )
                );
                $entete = array();
                //aucune donnee
                $datas = array("test" => array(
                                    90, $datas_height-16, 2, '0', 'L', '0', '0', '0', '255', '255', '255', array(0,0),     ' ',
                                    '', 'CELLSUP_NON', 0, 'NA','NC','','B',0,array('0')),  );
                $enpied = array();
                // AFFICHAGE DE LA PREMIERE PAGE
                $pdf->Table(
                    $page_header, $entete, $datas, $enpied,
                    sprintf('SELECT \' \' AS test'),
                    $this->f->db, $param, $firstpage
                );
                // XXX mettre une variable pour configurer ça
                if ($globale == false && $bureau_code == "ALL"
                    && $mode_edition != "commission"
                ) {
                    continue;
                }
                // BOUCLE SUR CHAQUE CATEGORIE DE MOUVEMENTS
                foreach ($mouvement_categories as $mouvement_categorie) {
                    // TITRE
                    $specific_page_header = $page_header;
                    $specific_page_header[] = array(
                        __("Famille de Changements : ").($mouvement_categorie == "Inscription" ? __("Addition") : $mouvement_categorie),
                        $page_content_width, $page_header_line_height, 0, ($allborders == true ? 1 : 0),'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0
                    );
                    // ENTETE
                    $entete = array(
                        //
                        array(__("Nom patronymique, Prenoms, Nom d'usage,"),
                              90, $entete_height/2, 2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                        //
                        array(__("Date et lieu de naissance"),
                              90, $entete_height/2, 0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                        //
                        array(__('Adresse'),
                              90, $entete_height, 0, '1','C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
                        //
                        array('<VCELL>',
                              15, $entete_height, 0, '1','C','0','0','0','255','255','255',array(0,0),3,
                              (($mode_edition == "commission" && $commission == "entre_deux_dates" ) ? array('Bureau') : array('Bureau,','No ordre')),
                              'NB',0,1,0),
                        //
                        array(__('Observations'),
                              82, $entete_height, 0, '1','C','0','0','0','255','255','255',array(0,8),0,'','NB',0,1,0)
                    );
                    // PAGEFIN
                    $pagefin = array(
                        //
                        array($pagefin_libelle,
                              200, $pagefin_height, 0, $pagefin_border, 'R','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
                        //
                        array('<LIGNE>',
                              20, $pagefin_height, 0, $pagefin_border, 'R','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
                        //
                        array(strtoupper(($mouvement_categorie == "Inscription" ? __("Addition") : $mouvement_categorie)).'(s)',
                              57, $pagefin_height, 1, $pagefin_border, 'L','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
                    );

                    // MOUVEMENT
                    $sql = "SELECT ";
                    $sql .= " mouvement.nom, ";
                    $sql .= " mouvement.prenom, ";
                    $sql .= " mouvement.nom_usage, ";
                    $sql .= " to_char(mouvement.date_naissance,'DD/MM/YYYY') as naissance, ";
                    $sql .= " (mouvement.libelle_lieu_de_naissance||' ('||mouvement.code_departement_naissance||')' ) as lieu, ";
                    // Adresse - ligne 1 (numero + complement du numero + adresse)
                    $sql .= " ( ";
                    $sql .= " CASE mouvement.numero_habitation ";
                        $sql .= " WHEN 0 then '' ";
                        $sql .= " ELSE (mouvement.numero_habitation||' ') ";
                    $sql .= " END";
                    $sql .= " || ";
                    $sql .= " CASE mouvement.complement_numero ";
                        $sql .= " WHEN '' then '' ";
                        $sql .= " ELSE (upper(mouvement.complement_numero)||' ') ";
                    $sql .= " END";
                    $sql .= " || ";
                    $sql .= " mouvement.libelle_voie ";
                    $sql .= " ) AS adresse, ";
                    // Adresse - ligne 2 (complement)
                    $sql .= " mouvement.complement, ";
                    //
                    if ($mouvement_categorie == "Inscription") {
                        $sql .= " mouvement.bureau_de_vote_code as bureau, ";
                    } elseif ($mouvement_categorie == "Radiation") {
                        $sql .= " mouvement.ancien_bureau_de_vote_code as bureau, ";
                    }
                    if ($mode_edition == "commission" && $commission == "entre_deux_dates" ) {
                        $sql .= "";
                    } else {
                        $sql .= " mouvement.numero_bureau, ";
                    }
                    $sql .= " (param_mouvement.libelle) as libmvt, ";
                    //
                    $sql .= " ( ";
                    $sql .= " ('('||param_mouvement.typecat||') ') ";
                    $sql .= " || ";
                    if ($mouvement_categorie == "Inscription") {
                        $sql .= " CASE mouvement.ancien_bureau_de_vote_code ";
                            $sql .= " WHEN '' then '' ";
                            $sql .= " ELSE ";
                            $sql .= " CASE param_mouvement.typecat ";
                                $sql .= " WHEN 'Modification' then ('(Ancien bureau : '||mouvement.ancien_bureau_de_vote_code||') ') ";
                                $sql .= " ELSE '' ";
                            $sql .= " END";
                        $sql .= " END";
                    } elseif ($mouvement_categorie == "Radiation") {
                        $sql .= " CASE mouvement.ancien_bureau_de_vote_code ";
                            $sql .= " WHEN '' then '' ";
                            $sql .= " ELSE ";
                            $sql .= " CASE param_mouvement.typecat ";
                                $sql .= " WHEN 'Modification' then ('(Nouveau bureau : '||mouvement.bureau_de_vote_code||') ') ";
                                $sql .= " ELSE '' ";
                            $sql .= " END";
                        $sql .= " END";
                    }
                    $sql .= " || ";
                    $sql .= " CASE mouvement.etat ";
                        $sql .= " WHEN 'actif' then '' ";
                        $sql .= " ELSE ('('||mouvement.tableau||') ') ";
                    $sql .= " END";
                    $sql .= " ) AS observations ";
                    // TABLES
                    $sql .= $critere_table_commun;
                    // SELECTION COMMUNE
                    $sql .= $critere_where_commun;
                    //
                    if ($mouvement_categorie == "Inscription") {
                        //
                        $sql .= $critere_where_inscription;

                    } elseif ($mouvement_categorie == "Radiation") {
                        //
                        $sql .= $critere_where_radiation;
                    }
                    //
                    if ($mode_edition == "tableau" && $tableau == "tableau_des_additions_des_jeunes") {
                        if ($datej5 == false) {
                            $sql .= " AND mouvement.etat='actif' ";
                            $sql .= " AND mouvement.types='".$type_mouvement_io."' ";
                        } else {
                            $sql .= " AND mouvement.date_j5='".$datej5."' ";
                            $sql .= " AND mouvement.etat='trs' ";
                            $sql .= " AND mouvement.types='".$type_mouvement_io."' ";
                        }
                    } elseif ($mode_edition == "tableau" && $tableau == "tableau_des_cinq_jours") {
                        if ($datej5 == false) {
                            $sql .= " AND mouvement.etat='actif' ";
                            $sql .= " AND param_mouvement.effet='Immediat' ";
                        } else {
                            $sql .= " AND mouvement.date_j5='".$datej5."' ";
                            $sql .= " AND mouvement.etat='trs' ";
                            $sql .= " AND param_mouvement.effet<>'Election' ";
                        }
                    } elseif ($mode_edition == "commission" && $commission == "entre_deux_dates") {
                        if ($mouvement_categorie == "Inscription") {
                            $sql .= $critere_filtre_dates_inscription;
                        } elseif ($mouvement_categorie == "Radiation") {
                            $sql .= $critere_filtre_dates_radiation;
                        }
                    }
                    //
                    $sql .= " order by withoutaccent(lower(mouvement.nom)), withoutaccent(lower(mouvement.prenom)) ";
                    // var_dump($sql);
                    // DATAS
                    $datas = array(
                        // COLONNE 1
                        'nom' => array(
                                    90, $datas_height-16, 2, 'LRT', 'L', '0', '0', '0', '255', '255', '255', array(0,0),     ' ',
                                    '', 'CELLSUP_NON', 0, 'NA','NC','','B',0,array('0')),
                        'prenom' => array(
                                    90, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), '     ',
                                    '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
                        'nom_usage' => array(
                                    90, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), '       -    ',
                                    '', 'CELLSUP_NON', 0, 'NA','CONDITION',array('NN'),'NB',0,array('0')),
                        'naissance' => array(
                                    90, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), __('     Ne(e) le '),
                                    '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
                        'lieu' => array(
                                    90, $datas_height-16, 0, 'LRB', 'L', '0', '0', '0', '255', '255', '255', array(0,0), __('     a  '),
                                    '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
                        // COLONNE 2
                        'adresse' => array(
                                    90, $datas_height-16, 2, 'LRT', 'L', '0', '0', '0', '255', '255', '255', array(0,16), '',
                                    '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
                        'complement' => array(
                                    90, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), '',
                                    '', 'CELLSUP_VIDE', array(
                                    90, $datas_height- 8, 0, 'LRB', 'C', '0', '0', '0', '255', '255', '255', array(0,0)),
                                                          'NA','NC','','NB',0,array('0')),
                        // COLONNE 3
                        'bureau' => array(
                                    15, $datas_height-16, 2, 'LRT', 'C', '0', '0', '0', '255', '255', '255', array(0,8), '',
                                    '', 'CELLSUP_NON', 0, 'NA','NC','','B',0,array('0')),
                        'numero_bureau' => array(
                                    15, $datas_height-16, 2,  'LR', 'C', '0', '0', '0', '255', '255', '255', array(0,0), '',
                                    '', 'CELLSUP_VIDE', array(
                                    15, $datas_height- 8, 0, 'LRB', 'C', '0', '0', '0', '255', '255', '255', array(0,0)),
                                                          'NA','NC','','NB',0,array('0')),
                        // COLONNE 4
                        'libmvt' => array(
                                    82, $datas_height-16, 2, 'LTR', 'L', '0', '0', '0', '255', '255', '255', array(0,8), '',
                                    '', 'CELLSUP_NON', 0, 'NA','NC','','NB',0,array('0')),
                        'observations' => array(
                            82, $datas_height-16, 2,  'LR', 'L', '0', '0', '0', '255', '255', '255', array(0,0), '', '', 'CELLSUP_VIDE',
                            array(82, $datas_height- 8, 0, 'LRB', 'C', '0', '0', '0', '255', '255', '255', array(0,0)),
                            'NA','CONDITION',array('NN'),'NB',0,array('0'))
                    );
                    if ($mode_edition == "commission" && $commission == "entre_deux_dates" ) {
                        $datas['bureau'] = array(15, $datas_height-16, 2,  'LR', 'C', '0', '0', '0', '255', '255', '255', array(0,4), '', '', 'CELLSUP_VIDE', array(15, $datas_height- 8, 0, 'LRB', 'C', '0', '0', '0', '255', '255', '255', array(0,0)), 'NA','NC','','NB',0, array('0')
                        );
                        unset($datas['numero_bureau']);
                    }
                    // ENPIED
                    $enpied = array();
                    // AFFICHAGE DU TABLEAU DE MOUVEMENTS
                    $pdf->Table(
                        $specific_page_header,
                        $entete,
                        $datas,
                        $enpied,
                        $sql,
                        $this->f->db,
                        $param,
                        $pagefin
                    );
                }

            }
        }
        // die();


        /**
         * OUTPUT
         */
        //
        $filename = "";
        if (isset($_GET["filename_with_date"]) && $_GET["filename_with_date"] == "false") {
            $filename .= "";
        } else {
            $filename .= date("Ymd-His")."-";
        }
        $filename .= $filename_objet;
        $filename .= "-".$_SESSION["collectivite"];
        $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
        if (isset($filename_more)) {
            $filename .= $filename_more;
        }
        $filename .= ".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
