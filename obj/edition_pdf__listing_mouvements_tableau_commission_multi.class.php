<?php
/**
 * Ce script définit la classe 'edition_pdf__listing_mouvements_tableau_commission_multi'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__listing_mouvements_tableau_commission_multi' (edition_pdf).
 */
class edition_pdf__listing_mouvements_tableau_commission_multi extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * Ce fichier permet de realiser une edition recapitulant tous les mouvements
     * d'inscriptions et de radiation et de changement de bureau pour une commune
     * ou pour le mode multi.
     *
     * @return array
     */
    public function compute_pdf__listing_mouvements_tableau_commission_multi($params = array()) {
        /**
         *
         */
        //
        $nolibliste = $_SESSION["libelle_liste"];
        // Gestion des dates
        $aujourdhui = date ("d/m/Y");

        /**
         * Ouverture du fichier pdf
         */
        //
        require_once "../obj/fpdf_table.class.php";
        //
        $pdf = new PDF('L', 'mm', 'A4');
        //
        $pdf->SetFont('Arial', '');
        //
        $pdf->SetDrawColor(30, 7, 146);
        //
        $pdf->SetMargins(5, 5, 5);
        //
        $pdf->SetDisplayMode('real', 'single');
        //
        $pdf->multi = true;


        // Liste les bureaux en associant leur canton
        $sql_bureau_a = sprintf(
            'SELECT bureau.code AS code, bureau.libelle AS bureau_libelle, canton.code as canton_code, canton.libelle as canton_libelle FROM %1$sbureau INNER JOIN %1$scanton ON bureau.canton=canton.id WHERE bureau.om_collectivite=%2$s ORDER BY bureau.code',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->query($sql_bureau_a);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$sql_bureau_a."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);

        $k = 0;
        $bureau = array();
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $bureau[$k] = $row;
            $k++;
        }

        // Page de garde commune
        $pdf->AddPage();
        $pdf->SetFont('Arial','',28);
        $pdf->Cell(280,160,iconv(HTTPCHARSET,"CP1252",$this->f->collectivite['ville']),0,0,"C",0);
        $pdf->SetFont('Arial','',12);
        $pdf->Text(250,180,iconv(HTTPCHARSET,"CP1252",__("Page ").$pdf->PageNo()));

        //
        // Requête générale compteur des mouvements
        $nbsql = sprintf(
            'SELECT
                count(*) AS nbr
            FROM
                %1$smouvement
                LEFT JOIN %1$sparam_mouvement on mouvement.types=param_mouvement.code
            WHERE
                mouvement.liste=\'%3$s\'
                AND mouvement.om_collectivite=%2$s
                AND mouvement.date_tableau=\'%4$s\'
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"],
            $this->f->getParameter("datetableau")
        );

        // Requête générale des mouvements
        $sql = sprintf(
            'SELECT
                mouvement.nom,
                mouvement.prenom,
                mouvement.nom_usage,
                to_char(mouvement.date_naissance, \'DD/MM/YYYY\') AS naissance,
                (mouvement.libelle_lieu_de_naissance || \' (\' || mouvement.code_departement_naissance || \')\') AS lieu,
                (mouvement.numero_habitation || \' \' || mouvement.complement_numero || \' \' || mouvement.libelle_voie) AS adresse,
                mouvement.complement,
                mouvement.numero_bureau,
                param_mouvement.libelle as libmvt,
                \' \' as ancien_bureau
            FROM
                %1$smouvement
                LEFT JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
            WHERE
                mouvement.liste=\'%3$s\'
                AND mouvement.om_collectivite=%2$s
                AND mouvement.date_tableau=\'%4$s\'
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"],
            $this->f->getParameter("datetableau")
        );

        //
        foreach ($bureau as $b) {

            //
            $pdf->AddPage();
            $pdf->AliasNbPages();
            $pdf->SetAutoPageBreak(false);

            // Requête : nombre d'inscriptions
            // Clause where particulière (Inscription ou Radiation)
            $nbins = $nbsql." and ((typecat='Modification' and mouvement.ancien_bureau_de_vote_code<>'".$b ['code']."') or (typecat='Inscription')) and mouvement.bureau_de_vote_code='".$b ['code']."'";
            $res = $this->f->db->query($nbins);
            $this->f->isDatabaseError($res);
            $row =& $res->fetchrow(DB_FETCHMODE_ASSOC);
            $nb_add = $row ['nbr'];

            // Requête : nombre de radiations
            // Clause where particulière (Inscription ou Radiation)
            $nbrad = $nbsql." and ((typecat='Modification' and mouvement.ancien_bureau_de_vote_code='".$b ['code']."' and mouvement.bureau_de_vote_code<>'".$b ['code']."') or (typecat='Radiation' and mouvement.bureau_de_vote_code='".$b ['code']."'))";
            $res = $this->f->db->query($nbrad);
            $this->f->isDatabaseError($res);
            $row =& $res->fetchrow(DB_FETCHMODE_ASSOC);
            $nb_rad = $row ['nbr'];

            // Type de catégorie de mouvements
            $typecat = array (0 => "Inscription", 1 => "Radiation");

            // Inscriptions
            $nbins_actif = $nbins." and etat='actif'";
            $res = $this->f->db->query($nbins_actif);
            $this->f->isDatabaseError($res);
            $row =& $res->fetchrow();
            $nb_add_actif = $row [0];

            // Radiations
            $nbrad_actif = $nbrad." and etat='actif'";
            $res = $this->f->db->query($nbrad_actif);
            $this->f->isDatabaseError($res);
            $row =& $res->fetchrow();
            $nb_rad_actif = $row [0];

            // Electeurs
            $nbelec = sprintf(
                'SELECT count(*) FROM %1$selecteur LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id WHERE bureau.code=\'%4$s\' AND electeur.liste=\'%3$s\' AND electeur.om_collectivite=%2$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $_SESSION["liste"],
                $b['code']
            );
            $res = $this->f->db->query($nbelec);
            $this->f->isDatabaseError($res);
            $row =& $res->fetchrow();
            $nb_elec = $row [0];

            // Recuperation du nombre d'additions traitees aux dates de tableau superieures
            // ou egales a la date de tableau actuelle
            // Requete de recuperation du nombre d'additions de tous les mouvements
            // traites a une date superieure ou egale a la date de tableau en cours
            $nb_addition_dtsup = "select count(*) as nbr ";
            $nb_addition_dtsup .= sprintf(
                ' FROM %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code ',
                DB_PREFIXE
            );
            $nb_addition_dtsup .= "where date_tableau>='".$this->f->getParameter("datetableau")."' ";
            $nb_addition_dtsup .= "and mouvement.bureau_de_vote_code='".$b ['code']."' and ";
            $nb_addition_dtsup .= "m.om_collectivite=".intval($_SESSION["collectivite"])." ";
            $nb_addition_dtsup .= " and liste='".$_SESSION["liste"]."'";
            // Avec une option pour la prise en compte ou non des modifications
            $nb_addition_dtsup .= " and (typecat='Inscription' or ";
            $nb_addition_dtsup .= " (typecat='Modification' ";
            $nb_addition_dtsup .= " AND mouvement.bureau_de_vote_code='".$b ['code']."' ";
            $nb_addition_dtsup .= " and mouvement.ancien_bureau_de_vote_code != mouvement.bureau_de_vote_code)) ";
            // Mouvements traités
            $nb_addition_dtsup .= " and m.etat='trs'";
            $res_nb_addition_dtsup = $this->f->db->getOne($nb_addition_dtsup);
            $this->f->isDatabaseError($res_nb_addition_dtsup);
            // Recuperation du nombre de radiations traitees aux dates de tableau
            // superieures ou egales a la date de tableau actuelle
            // Requete de recuperation du nombre de radiations de tous les mouvements
            // traites a une date superieure ou egale a la date de tableau en cours
            $nb_radiation_dtsup = "select count(*) as nbr ";
            $nb_radiation_dtsup .= sprintf(
                ' FROM %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm ON m.types=pm.code ',
                DB_PREFIXE
            );
            $nb_radiation_dtsup .= "where date_tableau>='".$this->f->getParameter("datetableau")."' ";
            $nb_radiation_dtsup .= " and liste='".$_SESSION["liste"]."'";
            $nb_radiation_dtsup .= " and m.om_collectivite=".intval($_SESSION["collectivite"])." ";
            $nb_radiation_dtsup .= "and ((pm.typecat='Radiation' and mouvement.bureau_de_vote_code='".$b ['code']."') ";
            // Avec une option pour la prise en compte ou non des modifications
            $nb_radiation_dtsup .= " or (typecat='Modification' and ";
            $nb_radiation_dtsup .= " mouvement.ancien_bureau_de_vote_code='".$b ['code']."' ";
            $nb_radiation_dtsup .= " and mouvement.ancien_bureau_de_vote_code != mouvement.bureau_de_vote_code)) ";
            // Mouvements traités
            $nb_radiation_dtsup .= " and m.etat='trs'";
            $res_nb_radiation_dtsup = $this->f->db->getOne($nb_radiation_dtsup);
            $this->f->isDatabaseError($res_nb_radiation_dtsup);

            /**
             * Calcul du nombre d'electeur a la date de tableau precedente et a la date de
             * tableau suivante
             */
            // Au tableau precedent le nombre d'electeurs est egal au nombre d'electeurs
            // dans la table electeur a l'heure actuelle auquel on enleve toutes les
            // additions traitees aux dates de tableau superieures ou egales a la date
            // de tableau actuelle et auquel on ajoute toutes les radiations traitees
            // aux dates de tableau superieures ou egales a la date de tableau actuelle
            $nb = $nb_elec - $res_nb_addition_dtsup + $res_nb_radiation_dtsup;
            // Au tableau suivant le nombre d'electeurs est egal au nombre d'electeurs
            // au tableau precedent calcule juste au dessus auquel on ajoute toutes
            // les additions de la date de tableau en cours et auquel on enleve toutes
            // les radiations de la date de tableau en cours
            $nb_newelec = $nb + $nb_add - $nb_rad;

            // Clause where particulière (Inscription ou Radiation)
            $sql_Inscription = $sql." and ((typecat='Modification' and mouvement.ancien_bureau_de_vote_code<>'".$b ['code']."') or (typecat='Inscription')) and mouvement.bureau_de_vote_code='".$b ['code']."' ";
            $sql_Inscription .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
            $sql_Radiation = $sql." and ((typecat='Modification' and mouvement.ancien_bureau_de_vote_code='".$b ['code']."' and mouvement.bureau_de_vote_code<>'".$b ['code']."') or (typecat='Radiation' and mouvement.bureau_de_vote_code='".$b ['code']."')) ";
            $sql_Radiation .= " order by withoutaccent(lower(nom)), withoutaccent(lower(prenom)) ";
            foreach ($typecat as $key => $t) {

                $var = "sql_".$t;
                //
                $param = array ();
                $param = array (9, 9, 10, 1, 1, 7, 7, 5, 10, 1, 1);
                //------------------------page de garde par bureau ----------------------------------------------------------------------------------
                $pagedebut = array ();
                $pagedebut = array (
                array (__('BUREAU No ').$b ['code'], 260, 10, 2, '0', 'R', '0', '0', '0', '255', '255', '255', array (0, 0), 0, '', 'NB', 18, 1, 0),
                array (__('LISTE ').$_SESSION["libelle_liste"]." - ".$this->f->collectivite['ville'],260,20,2,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                array (__('Nombre d\'electeurs inscrits au dernier tableau : ').$nb,260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                array (__('Inscriptions : ').$nb_add,260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                array (__('Radiations : ').$nb_rad,260,20,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                array (sprintf(__('Arrete le tableau du bureau no %s au nombre de %s electeurs.'),$b['code'],$nb_newelec),260,15,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                array ('',260,25,2,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                array ($this->f->collectivite['ville'].__(' le ').substr($this->f->getParameter("datetableau"),8,2).'/'.substr($this->f->getParameter("datetableau"),5,2).'/'.substr($this->f->getParameter("datetableau"),0,4).'.',260,15,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                array (__('Le Maire                         Le Delegue du Prefet                    Le Delegue du TGI'),260,15,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',18,1,0),
                array (__('Page ').$pdf->PageNo(), 260, 15, 1, '0', 'R', '0', '0', '0', '255', '255', '255', array (0, 0), 0, '', 'NB', 12, 1, 0)
                );
                //------------------------entete page ----------------------------------------------------------------------------------
                $heighttitre=4;
                $titre=array();
                $titre=array(
                array(__('BUREAU No ').$b ['code'].' '.$b ['bureau_libelle'],285,$heighttitre+1,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(__('ETAT POUR LA COMMISSION, MOUVEMENTS TABLEAU DU ').substr($this->f->getParameter("datetableau"),8,2).'/'.substr($this->f->getParameter("datetableau"),5,2).'/'.substr($this->f->getParameter("datetableau"),0,4),200,$heighttitre+5,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(' ',10,$heighttitre+5,1,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(__('Canton  ').$b ['canton_libelle'],280,$heighttitre,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array('',100,4,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(__('Famille de Changements : '). $t,270,$heighttitre,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                );
                //------------------------entete colonne   ------------------------------------------------------------------------//
                $heightentete=8;
                $entete=array();
                $entete=array(
                array(__('Nom Patronymique,Prenoms'),90,$heightentete-4,2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(__('Nom d\'Usage,Date et lieu de naissance'),90,$heightentete-4,0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(__('Adresse'),90,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
                array('<VCELL>',15,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),3,array('No','Ordre'),'NB',0,1,0),
                array(__('Motif du mouvement'),90,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,8),0,'','NB',0,1,0)
                );
                //------------------------page fin ----------------------------------------------------------------------------------
                $nbr_enregistrement=0;
                $pagefin=array();
                $pagefin=array(
                array(__('Arrete le tableau du '),45,$heighttitre+5,0,'T','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(substr($this->f->getParameter("datetableau"),8,2).'/'.substr($this->f->getParameter("datetableau"),5,2).'/'.substr($this->f->getParameter("datetableau"),0,4),25,$heighttitre+5,0,'T','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array(__(' a '),10,$heighttitre+5,0,'T','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                array('<LIGNE>',18,$heighttitre+5,0,'T','C','0','0','0','255','255','255',array(0,0),0,'','B',0,1,0),
                array(strtoupper($t).'(S)',45,$heighttitre+5,0,'T','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                //array('<PAGE>',140,$heighttitre+5,0,'T','R','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                );
                //------------------------enregistrements ----------------------------------------------------------------------------------
                $heightligne=20;
                $col=array();
                $col=array(
                'nom'=>array(90,$heightligne-16,2,'LRT','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','B',0,array('0')),
                'prenom'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),'     ','','CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
                'nom_usage'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','','CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
                'naissance'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),__('     Ne(e) le '),'','CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
                'lieu'=>array(90,$heightligne-16,0,'LRB','L','0','0','0','255','255','255',array(0,0),__('     a  '),'','CELLSUP_NON',0,'NA','NC','','NB',0,array('0')),
                'adresse'=>array(90,$heightligne-16,2,'LRT','L','0','0','0','255','255','255',array(0,16),'','','CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
                'complement'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),'','','CELLSUP_VIDE',array(90,$heightligne-8,0,'LRB','C','0','0','0','255','255','255',array(0,0)),'NA','NC','','NB',0,array('0')),
                'numero_bureau'=>array(15,$heightligne,0,'1','C','0','0','0','255','255','255',array(0,8),'','','CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
                'libmvt'=>array(90,$heightligne-16,2,'LTR','L','0','0','0','255','255','255',array(0,0),'','','CELLSUP_NON',0,0,'NA','NC','','NB',0,array('0')),
                'ancien_bureau'=>array(90,$heightligne-16,2,'LR','L','0','0','0','255','255','255',array(0,0),'','','CELLSUP_VIDE',array(90,$heightligne-8,0,'LRB','C','0','0','0','255','255','255',array(0,0)),'NA','CONDITION',array('NN'),'NB',0,array('0'))
                );
                //
                $enpied = array();

                if ($key==0)
                    $pdf->Table($titre,$entete,$col,$enpied,${$var},$this->f->db,$param,$pagefin,$pagedebut);
                else
                    $pdf->Table($titre,$entete,$col,$enpied,${$var},$this->f->db,$param,$pagefin);
            }
        }

        /**
         * OUTPUT
         */
        //
        $filename = date("Ymd-His");
        $filename .= "-commission";
        $filename .= "-".$_SESSION["collectivite"];
        $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
        $filename .= "-liste".$_SESSION["liste"];
        $filename .= ".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
