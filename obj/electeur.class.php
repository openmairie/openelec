<?php
/**
 * Ce script définit la classe 'electeur'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/electeur.class.php";

/**
 * Définition de la classe 'electeur' (om_dbform).
 */
class electeur extends electeur_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        $traduction = __("electeur");
        // Pas d'action 'ajouter'
        unset($this->class_actions[0]);
        // Pas d'action 'modifier'
        unset($this->class_actions[1]);
        // Pas d'action 'supprimer'
        unset($this->class_actions[2]);
        //
        $this->class_actions[3]["view"] = "view_fiche_electeur";
        //
        $this->class_actions[51] = array(
            "identifier" => "add-carteretour",
            "view" => "formulaire",
            "method" => "mark_carteretour",
            "portlet" => array(
               "type" => "action-direct-with-confirmation",
               "libelle" => __("Enregistrer une carte en retour pour cet électeur"),
               "class" => "carteretour-add-16",
               "order" => 50,
            ),
            "permission_suffix" => "carteretour_gerer",
            "condition" => array(
                "has_not_carteretour",
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[52] = array(
            "identifier" => "remove-carteretour",
            "view" => "formulaire",
            "method" => "unmark_carteretour",
            "portlet" => array(
               "type" => "action-direct-with-confirmation",
               "libelle" => __("Supprimer la carte en retour enregistrée"),
               "class" => "carteretour-remove-16",
               "order" => 50,
            ),
            "permission_suffix" => "carteretour_gerer",
            "condition" => array(
                "has_carteretour",
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[56] = array(
            "identifier" => "carte_electorale-add-to-cart",
            "view" => "formulaire",
            "method" => "add_to_cart__carte_electorale",
            "portlet" => array(
               "type" => "action-direct",
               "libelle" => __("Ajouter au panier 'Carte électorale'"),
               "class" => "carte_electorale-add-to-cart-16",
               "order" => 12,
            ),
            "permission_suffix" => "edition_pdf__carte_electorale",
            "condition" => array(
                "is_cart_carte_electorale_enabled",
                "is_not_in_cart_carte_electorale",
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[57] = array(
            "identifier" => "carte_electorale-remove-from-cart",
            "view" => "formulaire",
            "method" => "remove_from_cart__carte_electorale",
            "portlet" => array(
               "type" => "action-direct",
               "libelle" => __("Retirer du panier 'Carte électorale'"),
               "class" => "carte_electorale-remove-from-cart-16",
               "order" => 12,
            ),
            "permission_suffix" => "edition_pdf__carte_electorale",
            "condition" => array(
                "is_cart_carte_electorale_enabled",
                "is_in_cart_carte_electorale",
                "is_collectivity_mono",
            ),
        );
        // Actions disponible sur la fiche électeur
        // pour éditer les infos sur le jury
        $this->class_actions[54] = array(
            "identifier" => "edit-jury",
            "view" => "view_edit_infos_jury",
            "portlet" => array(
               "type" => "action-self",
               "libelle" => __("Juré d'assises"),
               "class" => "jury-16",
               "order" => 60,
            ),
            "permission_suffix" => "jury_modifier",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[71] = array(
            "identifier" => "modifier-electeur",
            "view" => "view_modifier_electeur",
            "portlet" => array(
               "type" => "action-self",
               "libelle" => __("Modifier l'électeur"),
               "class" => "modification-16",
               "order" => 71,
            ),
            "permission_suffix" => "modification",
            "condition" => array(
                "is_collectivity_mono",
                "has_not_mouvement_in_progress",
            ),
        );
        //
        $this->class_actions[72] = array(
            "identifier" => "radier-electeur",
            "view" => "view_radier_electeur",
            "portlet" => array(
               "type" => "action-self",
               "libelle" => __("Radier l'électeur"),
               "class" => "radiation-16",
               "order" => 72,
            ),
            "permission_suffix" => "radiation",
            "condition" => array(
                "is_collectivity_mono",
                "has_not_mouvement_in_progress",
            ),
        );
        //
        $this->class_actions[73] = array(
            "identifier" => "voir-mouvement-en-cours-electeur",
            "view" => "view_mouvement_electeur",
            "portlet" => array(
               "type" => "action-self",
               "libelle" => __("Voir le mouvement en cours"),
               "class" => "mouvement-16",
               "order" => 72,
            ),
            "permission_suffix" => "consulter",
            "condition" => array(
                "has_mouvement_in_progress",
                "is_accredited_to_view_mouvement_in_progress",
            ),
        );
        //
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-attestationelecteur",
            "view" => "view_edition_pdf__attestationelecteur",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Attestation d'électeur"),
               "class" => "pdf-16",
               "order" => 10,
            ),
            "permission_suffix" => "edition_pdf__attestationelecteur",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[202] = array(
            "identifier" => "edition-pdf-carteelecteur",
            "view" => "view_edition_pdf__carteelecteur",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Carte électorale"),
               "class" => "carte_electorale-16",
               "order" => 11,
               ""
            ),
            "permission_suffix" => "edition_pdf__carteelecteur",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        $this->class_actions[301] = array(
            "identifier" => "edition-pdf-listecarteretour",
            "view" => "view_edition_pdf__listing_electeurs_carteretour",
            "permission_suffix" => "edition_pdf__listing_electeurs_carteretour",
        );
        $this->class_actions[302] = array(
            "identifier" => "edition-pdf-liste_electorale",
            "view" => "view_edition_pdf__liste_electorale",
            "permission_suffix" => "edition_pdf__liste_electorale",
        );
        $this->class_actions[303] = array(
            "identifier" => "edition-pdf-carte_electorale",
            "view" => "view_edition_pdf__carte_electorale",
            "permission_suffix" => "edition_pdf__carte_electorale",
        );
        $this->class_actions[304] = array(
            "identifier" => "edition-pdf-liste_emargement",
            "view" => "view_edition_pdf__liste_emargement",
            "permission_suffix" => "edition_pdf__liste_emargement",
        );
        $this->class_actions[305] = array(
            "identifier" => "edition-pdf-etiquette_electorale",
            "view" => "view_edition_pdf__etiquette_electorale",
            "permission_suffix" => "edition_pdf__etiquette_electorale",
        );
        $this->class_actions[367] = array(
            "identifier" => "etiquettes-from-listing",
            "permission_suffix" => "etiquettes_from_listing",
            "view" => "view__edition_pdf__etiquettes_from_listing",
        );

        //
        $this->class_actions[401] = array(
            "identifier" => "statistiques-electeur_age_bureau",
            "view" => "view_edition_pdf__statistiques_electeur_age_bureau",
            "permission_suffix" => "edition_pdf__statistiques_electeur_age_bureau",
        );
        //
        $this->class_actions[402] = array(
            "identifier" => "statistiques-electeur_sexe_bureau",
            "view" => "view_edition_pdf__statistiques_electeur_sexe_bureau",
            "permission_suffix" => "edition_pdf__statistiques_electeur_sexe_bureau",
        );
        //
        $this->class_actions[403] = array(
            "identifier" => "statistiques-electeur_sexe_departementnaissance",
            "view" => "view_edition_pdf__statistiques_electeur_sexe_departementnaissance",
            "permission_suffix" => "edition_pdf__statistiques_electeur_sexe_departementnaissance",
        );
        //
        $this->class_actions[404] = array(
            "identifier" => "statistiques-electeur_sexe_nationalite",
            "view" => "view_edition_pdf__statistiques_electeur_sexe_nationalite",
            "permission_suffix" => "edition_pdf__statistiques_electeur_sexe_nationalite",
        );
        //
        $this->class_actions[405] = array(
            "identifier" => "statistiques-electeur_sexe_voie",
            "view" => "view_edition_pdf__statistiques_electeur_sexe_voie",
            "permission_suffix" => "edition_pdf__statistiques_electeur_sexe_voie",
        );
        $this->class_actions[601] = array(
            "identifier" => "listing_from_advs",
            "permission_suffix" => "tab",
            "view" => "view__listing_advs",
        );
    }

    /**
     * CONDITION - is_cart_carte_electorale_enabled.
     *
     * @return boolean
     */
    function is_cart_carte_electorale_enabled() {
        if (isset($_SESSION["panier_carte_electorale"]) !== true) {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_in_cart_carte_electorale.
     *
     * @return boolean
     */
    function is_in_cart_carte_electorale() {
        if (in_array($this->getVal($this->clePrimaire), $_SESSION["panier_carte_electorale"]) !== true) {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_not_in_cart_carte_electorale.
     *
     * @return boolean
     */
    function is_not_in_cart_carte_electorale() {
        return !$this->is_in_cart_carte_electorale();
    }

    /**
     *
     */
    function add_to_cart__carte_electorale() {
        if ($this->is_cart_carte_electorale_enabled() !== true) {
            $this->correct = false;
            return false;
        }
        $_SESSION["panier_carte_electorale"][] = $this->getVal($this->clePrimaire);
        $this->correct = true;
        $this->addToMessage("La carte électorale a été ajoutée au panier.");
        return true;
    }

    /**
     *
     */
    function remove_from_cart__carte_electorale() {
        if ($this->is_cart_carte_electorale_enabled() !== true) {
            $this->correct = false;
            return false;
        }
        unset($_SESSION["panier_carte_electorale"][array_search($this->getVal($this->clePrimaire), $_SESSION["panier_carte_electorale"])]);
        $this->correct = true;
        $this->addToMessage("La carte électorale a été retirée du panier.");
        return true;
    }


    /**
     * Redirige l'utilisateur vers le listing de la liste électorale avec la sélection
     * de la liste en session.
     *
     * @return void
     */
    function view__listing_advs() {
        $obj = "electeur";
        $search = array(
            "liste" => $_SESSION["liste"],
            "advanced-search-submit" => "",
        );
        $advs_id = str_replace(array('.',','), '', microtime(true));
        $_SESSION["advs_ids"][$advs_id] = serialize($search);
        header('Location: '.OM_ROUTE_TAB.'&obj='.$obj.'&advs_id='.$advs_id);
        return;
    }

    /**
     * Retourne le libellé par défaut d'un enregistrement.
     *
     * Principalement utilisé pour le titre de la page. C'est le dernier élément du
     * titre : Rubrique > Catégorie > Libellé par défaut.
     *
     * @return string
     */
    function get_default_libelle() {
        if (!$this->exists()) {
            return parent::get_default_libelle();
        }
        return __("Electeur - Id")." : ".$this->getVal($this->clePrimaire);
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            //
            "id",                  //0
            "liste",                        //30
            "bureauforce",                  //3
            "numero_bureau",                //1
            "bureau",
            "date_modif",
            "utilisateur",
            //
            "civilite",                     //4
            "sexe",                         //5
            "nom",                          //6
            "nom_usage",                    //7
            "prenom",                       //8
            //
            "date_naissance",               //10
            "code_departement_naissance",   //11
            "libelle_departement_naissance",//12
            "code_lieu_de_naissance",       //13
            "libelle_lieu_de_naissance",    //14
            "code_nationalite",             //15
            //
            "numero_habitation",            //16
            "code_voie",                    //18
            "libelle_voie",                 //17
            "complement_numero",            //19
            "complement",                   //20
            //
            "resident",                     //25
            "adresse_resident",             //26
            "complement_resident",          //27
            "cp_resident",                  //28
            "ville_resident",               //29
            //
            "provenance",                   //21
            "libelle_provenance",           //22
            //
            "carte",                        //32 non transfere
            "date_saisie_carte_retour",
            "jury",                         //33 non transfere
            "jury_effectif",
            "date_jeffectif",
            "profession",
            "motif_dispense_jury",
            //
            "mouvement",                    //23
            "procuration",                  //24
            "typecat",
            //
            "telephone",
            "courriel",
            "ine",
            "reu_sync_info",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return $this->get_common_var_sql_forminc__sql_bureau();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return $this->get_common_var_sql_forminc__sql_bureau_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste() {
        return $this->get_common_var_sql_forminc__sql_liste_officielle();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste_by_id() {
        return $this->get_common_var_sql_forminc__sql_liste_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_nationalite() {
        return sprintf(
            'SELECT nationalite.code, (nationalite.libelle_nationalite || \'(\' || nationalite.code || \')\') AS lib FROM %1$snationalite ORDER BY lib',
            DB_PREFIXE
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_nationalite_by_id() {
        return sprintf(
            'SELECT nationalite.code, (nationalite.libelle_nationalite || \'(\' || nationalite.code || \')\') AS lib FROM %1$snationalite WHERE nationalite.code = \'<idx>\'',
            DB_PREFIXE
        );
    }

    /**
     *
     */
    function is_back_link_redirect_activated() {
        if ($this->getParameter("maj") == 51
            || $this->getParameter("maj") == 52
            || $this->getParameter("maj") == 53
            || $this->getParameter("maj") == 55
            || $this->getParameter("maj") == 56
            || $this->getParameter("maj") == 57
            || $this->getParameter("maj") == 501) {
            return false;
        }
        return parent::is_back_link_redirect_activated();
    }

    /**
     * CONDITION - has_carteretour.
     *
     * @return boolean
     */
    function has_carteretour() {
        if ($this->getVal("carte") == "1") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - has_mouvement_in_progress.
     *
     * @return boolean
     */
    function has_mouvement_in_progress() {
        if ($this->getVal("typecat") !== "") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - has_not_mouvement_in_progress.
     *
     * @return boolean
     */
    function has_not_mouvement_in_progress() {
        return !$this->has_mouvement_in_progress();
    }

    /**
     * CONDITION - has_not_carteretour.
     *
     * @return boolean
     */
    function has_not_carteretour() {
        return !$this->has_carteretour();
    }

    /**
     * TREATMENT - mark_carteretour.
     *
     * @return boolean
     */
    function mark_carteretour() {
        //
        $valF = array(
            "carte" => '1',
            "date_saisie_carte_retour" => date("Y-m-d")
        );
        //
        $res_update = $this->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, $this->table),
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($this->getVal($this->clePrimaire))
        );
        $this->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $this->table)."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_update, true) !== false) {
            $this->correct = false;
            $this->addToMessage("Raté");
            return false;
        };
        $this->correct = true;
        $this->addToMessage(__("La carte en retour est enregitrée."));
        return true;
    }

    /**
     * TREATMENT - unmark_carteretour.
     *
     * @return boolean
     */
    function unmark_carteretour() {
        //
        $valF = array(
            "carte" => '0',
            "date_saisie_carte_retour" => null
        );
        //
        $res_update = $this->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, $this->table),
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($this->getVal($this->clePrimaire))
        );
        $this->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $this->table)."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_update, true) !== false) {
            $this->correct = false;
            $this->addToMessage("Raté");
            return false;
        };
        $this->correct = true;
        $this->addToMessage(__("La carte en retour est supprimée."));
        return true;
    }

    /**
     * VIEW - view__edition_pdf__etiquettes_from_listing.
     *
     * @return void
     */
    public function view__edition_pdf__etiquettes_from_listing() {
        $this->checkAccessibility();
        $f = $this->f;
        // Initialisation des paramètres
        $params = array(
            // Nom de l'objet metier
            "obj" => array(
                "default_value" => "",
            ),
            // Premier enregistrement a afficher
            "premier" => array(
                "default_value" => 0,
            ),
            // Colonne choisie pour le tri
            "tricol" => array(
                "default_value" => "",
            ),
            // Id unique de la recherche avancee
            "advs_id" => array(
                "default_value" => "",
            ),
            // Valilite des objets a afficher
            "valide" => array(
                "default_value" => "",
            ),
            "contentonly" => array(
                "default_value" => null,
            ),
            "mode" => array(
                "default_value" => null,

            ),
        );
        foreach ($this->f->get_initialized_parameters($params) as $key => $value) {
            ${$key} = $value;
        }
        //
        $standard_script_path = "../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
        $core_script_path = PATH_OPENMAIRIE."sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
        $gen_script_path = "../gen/sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
        //
        if (!isset($options)) {
            $options = array();
        }
        // Ce tableau permet a chaque application de definir des variables
        // supplementaires qui seront passees a l'objet metier dans le constructeur
        // a travers ce tableau
        // Voir le fichier dyn/form.get.specific.inc.php pour plus d'informations
        $extra_parameters = array();
        // surcharge globale
        if (file_exists('../dyn/tab.inc.php')) {
            require '../dyn/tab.inc.php';
        }
        // Inclusion du script [sql/<OM_DB_PHPTYPE>/<OBJ>.inc.php]
        $custom_script_path = $this->f->get_custom("tab", $obj);
        if ($custom_script_path !== null) {
            require $custom_script_path;
        } elseif (file_exists($standard_script_path) === false
            && file_exists($core_script_path) === true) {
            require $core_script_path;
        } elseif (file_exists($standard_script_path) === false
            && file_exists($gen_script_path) === true) {
            require $gen_script_path;
        } elseif (file_exists($standard_script_path) === true) {
            require $standard_script_path;
        }

        /**
         *
         */
        //
        if (!isset($om_validite) or $om_validite != true) {
            $om_validite = false;
        }
        if (!isset($options)) {
            $options = array();
        }
        $tb = $this->f->get_inst__om_table(array(
            "aff" => OM_ROUTE_TAB,
            "table" => $table,
            "serie" => $serie,
            "champAffiche" => $champAffiche,
            "champRecherche" => $champRecherche,
            "tri" => $tri,
            "selection" => $selection,
            "edition" => "",
            "options" => $options,
            "advs_id" => $advs_id,
            "om_validite" => $om_validite,
        ));
        $params = array(
            "obj" => $obj,
            "premier" => $premier,
            "tricol" => $tricol,
            "valide" => $valide,
            "advs_id" => $advs_id,
        );
        $params = array_merge($params, $extra_parameters);
        $tb->setParams($params);
        $tb->composeSearchTab();
        $tb->composeQuery();
        //
        require_once "../obj/edition_pdf__etiquette.class.php";
        $inst_edition_pdf = new edition_pdf__etiquette();
        $pdf_edition = $inst_edition_pdf->compute_pdf__etiquette(array(
            "obj" => "electeur_from_listing",
            "query" => $tb->sql,
        ));
        $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edit_infos_jury.
     *
     * @return void
     */
    function view_edit_infos_jury() {
        //
        $this->checkAccessibility();
        header("Location:".OM_ROUTE_SOUSFORM."&obj=electeur_jury&action=53&retourformulaire=electeur_view&idxformulaire=".$this->getVal($this->clePrimaire)."&idx=".$this->getVal($this->clePrimaire)."");
        die();
    }

    /**
     * TREATMENT - edit_infos_jury.
     *
     * @return boolean
     */
    function edit_infos_jury($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        $valF = array();
        $valF['jury'] = $val['jury'];
        $valF['jury_effectif'] = $val['jury_effectif'];
        // suppression des info "juré" si l'électeur n'est pas juré efectif
        if ($valF['jury_effectif']=="oui") {
            if ($val['date_jeffectif'] == "") {
                $this->correct = false;
                $this->msg .= "Date de prefecture obligatoire<br />";
                return $this->end_treatment(__METHOD__, false);
            }
            $valF['date_jeffectif'] = $this->dateDB($val['date_jeffectif']);
        } else {
            $valF['date_jeffectif'] = null;
        }
        // suppression des info "juré" si l'électeur n'est pas juré
        if ($valF['jury'] != "0") {
            $valF['profession'] = $val['profession'];
            $valF['motif_dispense_jury'] = $val['motif_dispense_jury'];
        } else {
            $valF['profession'] = "";
            $valF['motif_dispense_jury'] = "";
        }
        //
        $res_update = $this->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, $this->table),
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($this->getVal($this->clePrimaire))
        );
        $this->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $this->table)."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_update, true) !== false) {
            $this->correct = false;
            $this->addToMessage("Raté");
            return false;
        };
        $this->correct = true;
        $this->addToMessage(__("Vos modifications ont bien été enregistrées."));
        return true;
    }

    /**
     * VIEW - view_modifier_electeur.
     *
     * @return void
     */
    function view_modifier_electeur() {
        //
        $this->checkAccessibility();
        header("Location:".OM_ROUTE_FORM."&obj=modification&idxelecteur=".$this->getVal($this->clePrimaire)."&maj=0&origin=ficheelecteur&retour=form");
        die();
    }

    /**
     * VIEW - view_radier_electeur.
     *
     * @return void
     */
    function view_radier_electeur() {
        //
        $this->checkAccessibility();
        header("Location:".OM_ROUTE_FORM."&obj=radiation&idxelecteur=".$this->getVal($this->clePrimaire)."&maj=0&origin=ficheelecteur&retour=form");
        die();
    }

    /**
     * VIEW - view_mouvement_electeur.
     *
     * @return void
     */
    function view_mouvement_electeur() {
        //
        $this->checkAccessibility();
        header(sprintf(
            'Location:%s&obj=%s&idx=%s&action=3&origin=ficheelecteur',
            OM_ROUTE_FORM,
            $this->getVal("typecat"),
            $this->getVal("mouvement")
        ));
        die();
    }


    /**
     * CONDITION - is_accredited_to_view_mouvement_en_cours.
     *
     * @return boolean
     */
    function is_accredited_to_view_mouvement_in_progress() {
        if ($this->has_mouvement_in_progress() === false) {
            return false;
        }
        return $this->f->isAccredited(
            array($this->getVal("typecat"), $this->getVal("typecat")."_consulter"),
            "OR"
        );
    }

    /**
     * VIEW - view_fiche_electeur.
     *
     * @return void
     */
    function view_fiche_electeur() {
        $this->checkAccessibility();
        // Rétrocompatibilité
        $id_elec = $this->getVal($this->clePrimaire);

        // Premier enregistrement a afficher sur le tableau de la page precedente (tab.php?premier=)
        (isset($_GET['premier']) ? $premier = $_GET['premier'] : $premier = 0);
        // Colonne choisie pour le tri sur le tableau de la page precedente (tab.php?tricol=)
        (isset($_GET['tricol']) ? $tricol = $_GET['tricol'] : $tricol = "");
        // Colonne choisie pour la selection sur le tableau de la page precedente (tab.php?selectioncol=)
        (isset($_GET['selectioncol']) ? $selectioncol = $_GET['selectioncol'] : $selectioncol = "");
        // Chaine recherchee
        if (isset($_GET['recherche'])) {
            $recherche = $_GET['recherche'];
            $recherche1 = $recherche;
        } else {
            $recherche = "";
            $recherche1 = "";
        }

        // Lien avec un électeur du REU
        $link_reu = false;


        /**
         *
         */
        //
        echo "<div id=\"ficheelecteur\">\n";

        if ($this->has_mouvement_in_progress()) {
            $this->f->displayMessage("error", __("Cet électeur a un mouvement en cours"));
        }

        // Affichage du portlet d'actions s'il existe des actions
        $this->compose_portlet_actions();
        if (!empty($this->user_actions)) {
            $inst__om_formulaire = $this->f->get_inst__om_formulaire();
            $inst__om_formulaire->afficher_portlet(
                $this->getParameter("idx"),
                $this->user_actions
            );
        }

        //
        echo "<div>\n";

        //
        $infos = array(
            "liste" => $this->getVal("liste"),
            "civilite" => $this->val[array_search("civilite", $this->champs)],
            "nom" => $this->val[array_search("nom", $this->champs)],
            "prenom" => $this->val[array_search("prenom", $this->champs)],
            "nom_usage" => $this->val[array_search("nom_usage", $this->champs)],
            "date_naissance" => $this->val[array_search("date_naissance", $this->champs)],
            "libelle_lieu_de_naissance" => $this->val[array_search("libelle_lieu_de_naissance", $this->champs)],
            "libelle_departement_naissance" => $this->val[array_search("libelle_departement_naissance", $this->champs)],
            "code_departement_naissance" => $this->val[array_search("code_departement_naissance", $this->champs)],
            "sexe" => $this->val[array_search("sexe", $this->champs)],
            "code_nationalite" => $this->val[array_search("code_nationalite", $this->champs)],
            "numero_d_electeur" => $this->getVal("ine"),
        );

        $template_fiche_electeur = "
            %s
            %s
        ";
        if ($link_reu === true) {
            $template_fiche_electeur = "
                <div class=\"container_ficheelecteur_oel\">\n
                    %s
                </div>\n
                <div class=\"container_ficheelecteur_reu\">\n
                    %s
                </div>\n
                <div id=\"reu_sync_info\">\n
                </div>
            ";
        }

        $get_display_fiche_electeur = $this->get_display_fiche_electeur($infos, $link_reu);

        //
        $get_display_fiche_electeur_reu = '';
        if ($link_reu === true) {
            $get_display_fiche_electeur_reu = $this->get_display_fiche_electeur_reu();
        }

        printf(
            $template_fiche_electeur,
            $get_display_fiche_electeur,
            $get_display_fiche_electeur_reu
        );

        //
        if ($this->val[array_search("carte", $this->champs)] == "1") {
            $this->display_carte_retour($this->getVal('date_saisie_carte_retour'));
        }
        //
        echo "<div class=\"adresse\">";
        echo "<p class=\"titre\">";
        echo __("Adresse dans la commune :");
        echo "</p>";
        echo "<p class=\"contenu\">";
        if ($this->val[array_search("numero_habitation", $this->champs)] != 0) {
            echo $this->val[array_search("numero_habitation", $this->champs)];
            echo " ";
        }
        echo $this->val[array_search("complement_numero", $this->champs)];
        echo " ";
        echo $this->val[array_search("libelle_voie", $this->champs)];
        if ($this->val[array_search("complement", $this->champs)] != "") {
        echo "<br/>";
        echo $this->val[array_search("complement", $this->champs)];
        }
        echo "<br/>";
        //
        $query_voie = sprintf(
            'SELECT * FROM %1$svoie WHERE voie.code=\'%2$s\'',
            DB_PREFIXE,
            $this->val[array_search("code_voie", $this->champs)]
        );
        $res_voie = $this->f->db->query($query_voie);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query_voie."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res_voie);
        //
        $row_voie =& $res_voie->fetchrow(DB_FETCHMODE_ASSOC);
        echo $row_voie['cp']." ".$row_voie['ville'];
        echo "</p>";
        echo "</div>";

        //
        if ($this->val[array_search("resident", $this->champs)] == 'Oui') {
        echo "<div class=\"residence\">";
        echo "<p class=\"titre\">";
        echo __("Adresse de contact :");
        echo "</p>";
        echo "<p class=\"contenu\">";
        echo $this->val[array_search("adresse_resident", $this->champs)];
        if ($this->val[array_search("complement_resident", $this->champs)] != "") {
        echo "<br/>";
        echo $this->val[array_search("complement_resident", $this->champs)];
        }
        echo "<br/>";
        echo $this->val[array_search("cp_resident", $this->champs)]." ".$this->val[array_search("ville_resident", $this->champs)];
        echo "</p>";
        echo "</div>";
        }

        //
        if ($this->getVal("telephone") != ""
            || $this->getVal("courriel") != "") {
            echo "<div class=\"coordonnees\">";
            echo "<p class=\"titre\">";
            echo __("Coordonnées :");
            echo "</p>";
            echo "<p class=\"contenu\">";
            if ($this->getVal("telephone") != "") {
                echo __("Téléphone")." : ";
                echo $this->getVal("telephone");
                echo "<br/>";
            }
            if ($this->getVal("courriel") != "") {
                echo __("Courriel")." : ";
                echo $this->getVal("courriel");
                echo "<br/>";
            }
            echo "</p>";
            echo "</div>";
        }

        //
        echo "<div class=\"bureau\">";
        echo "<h3>";
        echo __("Bureau de vote :");
        echo " ";
        //
        $bureau = $this->get_inst__bureau();
        echo $bureau->getVal('code') ." ".$bureau->getVal('libelle');
        echo "</h3>";
        echo "<p class=\"contenu\">";
        echo __("Numero de l'electeur dans le bureau :");
        echo " ".$this->val[array_search("numero_bureau", $this->champs)];
        echo "</p>";
        echo "</div>";

        echo "<div id=\"jury_assise\">";
        // Info concernant l'aspect juré de l'électeur
        if($this->val[array_search("jury", $this->champs)] == 1 OR
            $this->val[array_search("jury", $this->champs)] == 2 OR
            $this->val[array_search("jury_effectif", $this->champs)] == "oui") {
            echo "<h3>";
            echo __("Jury d'assises :");
            echo "</h3>";
            echo "<p class=\"contenu\">";
            if($this->val[array_search("jury", $this->champs)] == 1) {
                echo __("L'electeur fait parti de la liste provisoire courante.");
                echo "<br/>";
            }
            if($this->val[array_search("jury", $this->champs)] == 2) {
                echo __("L'electeur fait parti de la liste provisoire courante en tant que suppléant.");
                echo "<br/>";
            }
            if($this->val[array_search("profession", $this->champs)] != "") {
                echo __("Profession de l'electeur :");
                echo " ".$this->val[array_search("profession", $this->champs)];
                echo "<br/>";
            }
            if($this->val[array_search("jury_effectif", $this->champs)] == "oui") {
                if($this->val[array_search("jury", $this->champs)] == 1) {
                    echo __("L'electeur est jure effectif le :");
                } else {
                    echo __("L'electeur est jure suppleant effectif le :");
                }
                $dateFormat = new DateTime($this->val[array_search("date_jeffectif", $this->champs)]);
                echo " ".$dateFormat->format('d/m/Y');
                echo "<br/>";
            }
            if($this->val[array_search("motif_dispense_jury", $this->champs)] != "") {
                echo __("Motif de dispense :");
                echo " ".$this->val[array_search("motif_dispense_jury", $this->champs)];
                echo "<br/>";
            }

            echo "</p>";
        }
        echo "</div>";
        //
        echo "<div class=\"historique\">";
        echo "<h3>";
        echo __("Historique");
        echo "</h3>";
        //
        $historique = array(
            "trs" => array(),
            "archive" => array(),
        );
        //
        $query_mouvement = sprintf(
            'SELECT * FROM %1$smouvement AS m INNER JOIN %1$sparam_mouvement AS pm ON m.types=pm.code WHERE m.electeur_id=%2$s and etat=\'trs\' ORDER BY date_tableau DESC',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        );
        $res_mouvement = $this->f->db->query($query_mouvement);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query_mouvement."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res_mouvement);
        //
        while ($row_mouvement =& $res_mouvement->fetchrow(DB_FETCHMODE_ASSOC)) {
            $mouvement = array(
                "row" => $row_mouvement,
                "etat" => $row_mouvement['etat'],
                "categorie" => $row_mouvement['typecat'],
                "mouvement" => $row_mouvement['libelle'],
                "datetableau" => $row_mouvement['date_tableau'],
                "tableau" => $row_mouvement['tableau'],
                "datemouvement" => $row_mouvement['date_modif'],
            );
            array_push($historique[$row_mouvement['etat']], $mouvement);
        }
        //
        $query_archive = sprintf(
            'SELECT * FROM %1$sarchive AS a INNER JOIN %1$sparam_mouvement AS pm ON a.types=pm.code WHERE a.electeur_id=%2$s ORDER BY date_tableau DESC, date_mouvement DESC',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        );
        $res_archive = $this->f->db->query($query_archive);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query_archive."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res_archive);
        //
        while ($row_archive =& $res_archive->fetchrow(DB_FETCHMODE_ASSOC)) {
            $archive = array(
                "etat" => "archive",
                "categorie" => $row_archive['typecat'],
                "mouvement" => $row_archive['libelle'],
                "datetableau" => $row_archive['date_tableau'],
                "tableau" => $row_archive['tableau'],
                "datemouvement" => $row_archive['date_mouvement'],
            );
            array_push ($historique['archive'], $archive);
        }
        //
        echo "<table class=\"tabCol\">";

        foreach ($historique as $key => $categorie) {
            echo "<tr class=\"fenData\">";
            echo "<td>";
            echo "<b>";
            switch ($key) {
                case "trs": echo __("Mouvement(s) traite(s)"); break;
                case "archive": echo __("Mouvement(s) archive(s)"); break;
                default:;
            }
            echo "</b>";
            echo "</td>";
            echo "</tr>";
            foreach ($categorie as $mouvement) {
                echo "<tr>";
                echo "<td>";
                echo $mouvement['categorie'];
                echo " [";
                echo $mouvement['mouvement'];
                echo "] ";
                if ($mouvement['etat'] == 'archive') {
                    echo " ".__("valide lors du traitement")." ";
                    echo $mouvement['tableau'];
                    echo " ".__("a la date de tableau du")." ";
                    echo $this->f->formatDate ($mouvement['datetableau'], true);
                    echo " ";
                    echo __("Mouvement archive le")." ";
                    echo $this->f->formatDate ($mouvement['datemouvement'], true);
                } elseif ($mouvement['etat'] == 'trs') {
                    echo " ".__("valide lors du traitement")." ";
                    echo $mouvement['tableau'];
                    echo " ".__("a la date de tableau du")." ";
                    echo $this->f->formatDate ($mouvement['datetableau'], true);
                    //
                    echo " (<a class=\"tooltip-opener\" rel=\"mouvement-".$mouvement["row"]["id"]."\" href=\"#\">".__("Voir le detail")."</a>)";

                    /**
                     * OVERLAY
                     */
                    //
                    echo "<div class=\"tooltip\" id=\"mouvement-".$mouvement["row"]["id"]."\">";
                    echo "<div class=\"message ui-state-highlight ui-corner-all\">";
                    echo $mouvement['categorie'];
                    echo " [";
                    echo $mouvement['mouvement'];
                    echo "] ";
                        echo " ".__("valide lors du traitement")." ";
                    echo $mouvement['tableau'];
                    echo " ".__("a la date de tableau du")." ";
                    echo $this->f->formatDate ($mouvement['datetableau'], true);
                    echo "</div>";
                    echo "<div id=\"ficheelecteur\">";
                    echo $this->get_display_fiche_electeur($mouvement["row"]);
                    //
                    echo "<div class=\"adresse\">";
                    echo "<p class=\"titre\">";
                    echo __("Adresse dans la commune :");
                    echo "</p>";
                    echo "<p class=\"contenu\">";
                    if ($mouvement["row"]["numero_habitation"] != 0) {
                        echo $mouvement["row"]["numero_habitation"];
                        echo " ";
                    }
                    echo $mouvement["row"]["complement_numero"];
                    echo " ";
                    echo $mouvement["row"]["libelle_voie"];
                    if ($mouvement["row"]["complement"] != "") {
                    echo "<br/>";
                    echo $mouvement["row"]["complement"];
                    }
                    echo "<br/>";
                    //
                    $query_voie = sprintf(
                        'SELECT * FROM %1$svoie WHERE voie.code=\'%2$s\'',
                        DB_PREFIXE,
                        $mouvement["row"]["code_voie"]
                    );
                    $res_voie = $this->f->db->query($query_voie);
                    $this->addToLog(
                        __METHOD__."(): db->query(\"".$query_voie."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($res_voie);
                    //
                    $row_voie =& $res_voie->fetchrow(DB_FETCHMODE_ASSOC);
                    echo $row_voie['cp']." ".$row_voie['ville'];
                    echo "</p>";
                    echo "</div>";
                    //
                    echo "<div class=\"bureau\">";
                    echo "<h3>";
                    echo __("Bureau de vote :");
                    echo " ";
                    //
                    echo $mouvement["row"]["bureau_de_vote_code"]." ".$mouvement["row"]["bureau_de_vote_libelle"];
                    echo "</h3>";
                    echo "</div>";
                    echo "</div>";
                    //

                    //
                    echo "</div>";

                }
                echo "</td>";
                echo "</tr>";
            }
        }
        echo "</table>";
        //
        $sql = sprintf(
            'SELECT * FROM %1$selecteur LEFT JOIN %1$sparam_mouvement ON electeur.code_inscription=param_mouvement.code WHERE electeur.id=%2$s',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        if ($res->numrows() == 1) {
            $row =& $res->fetchrow(DB_FETCHMODE_ASSOC);
            $this->addToLog(__METHOD__."(): ".print_r($row, true), EXTRA_VERBOSE_MODE);
            if ($row["date_inscription"] != "") {
                echo "<h4>";
                echo __("Inscrit le :");
                echo " ".$this->f->formatDate($row["date_inscription"], true);
                if ($row["code_inscription"] != "") {
                    echo " - ";
                    echo __("Motif :");
                    echo " ".$row["libelle"];
                }
                echo "</h4>";
            }
        } else {
            $this->addToLog(__METHOD__."(): resultat de la requete : ".$res->numrows(), EXTRA_VERBOSE_MODE);
        }
        echo "</div>";
        //
        echo "</div>";

        //
        echo "<div class=\"formControls col_12\">";
        $this->retour();
        echo "</div>";

        echo "<div class=\"visualClear\"><!-- --></div>";

        //
        echo "</div>";
    }

    function get_liste_insee() {
        $inst_liste = $this->f->get_inst__om_dbform(array(
            "obj" => "liste",
            "idx" => $this->getVal("liste"),
        ));
        if ($inst_liste->exists() === true) {
            return $inst_liste->getVal("liste_insee");
        }
        return "NC";
    }

    /**
     *
     */
    public function get_display_fiche_electeur($infos = array(), $display_type_electeur = false, $class = 'ficheelecteur') {
        //
        if (isset($infos['code_nationalite']) === true) {
            //
            $query_nationalite = sprintf(
                'SELECT * FROM %1$snationalite WHERE code=\'%2$s\'',
                DB_PREFIXE,
                $infos["code_nationalite"]
            );
            $res_nationalite = $this->f->db->query($query_nationalite);
            $this->addToLog(
                __METHOD__."(): db->query(\"".$query_nationalite."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res_nationalite);
            //
            $row_nationalite =& $res_nationalite->fetchrow(DB_FETCHMODE_ASSOC);
        }
        //
        $libelle_liste = "";
        if (isset($infos["liste"]) === true) {
            $inst_liste = $this->f->get_inst__om_dbform(array(
                "obj" => "liste",
                "idx" => $infos["liste"],
            ));
            if ($inst_liste->exists() === true) {
                $libelle_liste = $inst_liste->getVal("liste_insee")." - ".$inst_liste->getVal("libelle_liste");
            }
        }
        //
        if (isset($infos['libelle_nationalite']) === true) {
            $row_nationalite['libelle_nationalite'] = $infos['libelle_nationalite'];
        }
        //
        $output = "<div class=\"".$class."\">";
        if ($display_type_electeur === true) {
            $title_fiche = __("Électeur openElec");
            if ($class === 'ficheelecteur_reu') {
                $title_fiche = __("Électeur REU");
            }
            $output .= sprintf(
                "<h2>%s</h2>",
                $title_fiche
            );
        }
        //
        $output .= "<div class=\"etatcivil\">";
        $output .= "<h2>";
        $output .= $infos["civilite"];
        $output .= " ";
        $output .= "<span class=\"nom\" title=\"".__("nom")."\">";
        $output .= $infos["nom"];
        $output .= "</span>";
        $output .= " ";
        if ($infos["nom_usage"] != "") {
            $output .= "- ";
            $output .= "<span class=\"nom_usage\" title=\"".__("nom d'usage")."\">";
            $output .= $infos["nom_usage"];
            $output .= "</span>";
            $output .= " ";
        }
        $output .= "<span class=\"prenom\" title=\"".__("prenom(s)")."\">";
        $output .= $infos["prenom"];
        $output .= "</span>";
        $output .= "</h2>";
        $output .= "</div>";

        //
        $output .= "<div class=\"naissance\">";
        $output .= "<p>";
        $output .= __("Ne(e) le")." ";
        $output .= $this->f->formatDate($infos["date_naissance"], true);
        $output .= "<br/>";
        $output .= " ".__("a")." ";
        $output .= $infos["libelle_lieu_de_naissance"];
        $output .= " [";
        $output .= $infos["libelle_departement_naissance"];
        $output .= " (";
        $output .= $infos["code_departement_naissance"];
        $output .= ")]";
        $output .= "</p>";
        $output .= "<p>";
        $output .= __("Sexe")." : ";
        $sexe = $infos["sexe"];
        if ($sexe == "M") {
            $output .= __("Masculin");
        } elseif ($sexe == "F") {
            $output .= __("Feminin");
        } else {
            $output .= "-";
        }
        $output .= "</p>";
        $output .= "<p>";
        $output .= __("Nationalite")." : ";
        $output .= $row_nationalite['libelle_nationalite'];
        $output .= "</p>";
        if ($libelle_liste != "") {
            $output .= "<p>";
            $output .= __("Liste")." : ";
            $output .= $libelle_liste;
            $output .= "</p>";
        }
        $output .= "</div>";

        //
        $output .= "<div class=\"autres_infos\">";
        if (isset($infos['numero_d_electeur']) === true) {
            $output .= "<p>";
            $output .= __("Numéro d'électeur")." : ";
            $output .= $infos['numero_d_electeur'];
            $output .= "</p>";
        }
        $output .= "</div>";
        $output .= "</div>";
        //
        return $output;
    }

    public function get_display_fiche_electeur_reu($context_reu = false) {
        //
        $numero_d_electeur = null;
        $liste = null;
        if ($context_reu === true) {
            $numero_d_electeur = preg_replace('~\D~', '', $this->f->get_submitted_get_value('numero_d_electeur'));
            $liste = preg_replace('~\d~', '', $this->f->get_submitted_get_value('numero_d_electeur'));
        }
        $get_infos_electeur_reu = $this->get_infos_electeur_reu($numero_d_electeur, $liste);
        if ($get_infos_electeur_reu === false || $get_infos_electeur_reu === null) {
            return sprintf(
                "<p>%s</p>",
                __("Impossible de récupérer les informations de l'électeur du REU.")
            );
        }
        $civilite_reu = 'M.';
        if ($get_infos_electeur_reu['sexe'] === 'F') {
            $civilite_reu = 'Mme';
        }
        //
        $code_departement_naissance = "";
        $libelle_departement_naissance = "";
        $code_lieu_de_naissance = "";
        $libelle_lieu_de_naissance = "";
        // Né à l'étranger
        if ($get_infos_electeur_reu["code_commune_de_naissance"] == "") {
            $code_lieu_de_naissance = $get_infos_electeur_reu["code_pays_de_naissance"];
            if ($get_infos_electeur_reu["libelle_commune_de_naissance"] == "") {
                $libelle_lieu_de_naissance = $get_infos_electeur_reu["libelle_pays_de_naissance"];
            } else {
                $libelle_lieu_de_naissance = $get_infos_electeur_reu["libelle_commune_de_naissance"];
            }
            $code_departement_naissance = $get_infos_electeur_reu["code_pays_de_naissance"];
            $libelle_departement_naissance = $get_infos_electeur_reu["libelle_pays_de_naissance"];
        } else {// Né en France
            $libelle_lieu_de_naissance = $get_infos_electeur_reu["libelle_commune_de_naissance"];
            $code_lieu_de_naissance = $get_infos_electeur_reu["code_commune_de_naissance"];
            if ($this->f->starts_with($get_infos_electeur_reu["code_commune_de_naissance"], "98") === true
                || $this->f->starts_with($get_infos_electeur_reu["code_commune_de_naissance"], "97") === true) {
                $code_departement_naissance = substr($get_infos_electeur_reu["code_commune_de_naissance"], 0, 3);
                $code_lieu_de_naissance = $code_departement_naissance." ".substr($get_infos_electeur_reu["code_commune_de_naissance"], 3, 2);
            } else {
                $code_departement_naissance = substr($get_infos_electeur_reu["code_commune_de_naissance"], 0, 2);
                $code_lieu_de_naissance = $code_departement_naissance." ".substr($get_infos_electeur_reu["code_commune_de_naissance"], 2, 3);
            }
            $libelle_departement_naissance = $this->f->db->getone(
                sprintf(
                    'SELECT libelle_departement from %1$sdepartement where code=\'%2$s\'',
                    DB_PREFIXE,
                    $code_departement_naissance
                )
            );
        }
        $infos_reu = array(
            "civilite" => $civilite_reu,
            "nom" => $get_infos_electeur_reu['nom'],
            "prenom" => $get_infos_electeur_reu['prenoms'],
            "nom_usage" => $get_infos_electeur_reu['nom_d_usage'],
            "date_naissance" => $get_infos_electeur_reu['date_de_naissance'],
            "libelle_lieu_de_naissance" => $libelle_lieu_de_naissance,
            "libelle_departement_naissance" => $libelle_departement_naissance,
            "code_departement_naissance" => $code_departement_naissance,
            "sexe" => $get_infos_electeur_reu['sexe'],
            "libelle_nationalite" => $get_infos_electeur_reu['libelle_nationalite'],
            "numero_d_electeur" => $get_infos_electeur_reu['numero_d_electeur'],
        );
        return $this->get_display_fiche_electeur($infos_reu, true, 'ficheelecteur_reu');
    }

    public function get_infos_electeur_reu($numero_d_electeur = null, $code_type_liste = null) {
        if ($numero_d_electeur === null) {
            $numero_d_electeur = $this->getVal('ine');
        }
        if ($code_type_liste === null) {
            $inst_liste = $this->f->get_inst__om_dbform(array(
                "obj" => "liste",
                "idx" => $this->getVal('liste'),
            ));
            $code_type_liste = $inst_liste->getVal('liste_insee');
        }
        $sql = sprintf("
            SELECT *
            FROM %sreu_sync_listes
            WHERE numero_d_electeur = '%s'
                AND code_type_liste = '%s'",
            DB_PREFIXE,
            $numero_d_electeur,
            $code_type_liste
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true) !== false) {
            $this->f->displayMessage(
                "error",
                __("Impossible de récupérer l'électeur depuis le REU.")
            );
            return false;
        }
        $row = $res->fetchRow(DB_FETCHMODE_ASSOC);

        // Si l'électeur du REU n'est pas récupéré depuis la liste synchronisée
        if ($row === null) {
            // Récupère l'électeur par webservice
            $inst_reu = $this->f->get_inst__reu();
            if ($inst_reu == null) {
                $this->addToMessage(__("Problème de configuration de la connexion au REU."));
                return false;
            }
            if ($inst_reu->is_connexion_logicielle_valid() !== true) {
                $this->addToMessage(__("La connexion logicielle au REU n'est pas valide."));
                return false;
            }
            $ret = $inst_reu->handle_electeur(array(
                "mode" => "get",
                "ine" => $numero_d_electeur,
            ));

            if ($ret["code"] != "200") {
                $this->f->displayMessage(
                    "error",
                    __("Impossible de récupérer l'électeur depuis le REU.")
                );
                return false;
            }

            //
            $rattach = 'rattachementsActifs';
            if (gavoe($ret['result'], array('rattachementsInactifs', ), null) !== null) {
                $rattach = 'rattachementsInactifs';
            }
            $row = array(
                'code_ugle' => gavoe($ret['result'], array($rattach, 'ugle', 'code' )),
                'code_commune_de_l_ugle' => gavoe($ret['result'], array($rattach, 'ugle', 'commune', 'code', )),
                'libelle_ugle' => gavoe($ret['result'], array($rattach, 'ugle', 'libelle' )),
                'date_extraction' => date("dd/mm/YYYY"),
                'code_type_liste' => gavoe($ret['result'], array($rattach, 'typeListe', 'code' )),
                'libelle_type_liste' => gavoe($ret['result'], array($rattach, 'typeListe', 'libelle' )),
                'numero_d_electeur' => gavoe($ret['result'], array('numeroElecteur', )),
                'code_categorie_d_electeur' => '',
                'libelle_categorie_d_electeur' => '',
                'date_d_inscription' => gavoe($ret['result'], array($rattach, 'dateInscription' )),
                'nom' => gavoe($ret['result'], array('etatCivil', 'nomNaissance', )),
                'nom_d_usage' => gavoe($ret['result'], array('etatCivil', 'nomUsage', )),
                'prenoms' => gavoe($ret['result'], array('etatCivil', 'prenoms', )),
                'sexe' => gavoe($ret['result'], array('etatCivil', 'sexe', 'code', )),
                'date_de_naissance' => gavoe($ret['result'], array('etatCivil', 'dateNaissance', )),
                'code_commune_de_naissance' => gavoe($ret['result'], array('etatCivil', 'communeNaissance', 'code', )),
                'libelle_commune_de_naissance' => gavoe($ret['result'], array('etatCivil', 'communeNaissance', 'libelle', )),
                'code_pays_de_naissance' => gavoe($ret['result'], array('etatCivil', 'paysNaissance', 'code', )),
                'libelle_pays_de_naissance' => gavoe($ret['result'], array('etatCivil', 'paysNaissance', 'libelle', )),
                'code_nationalite' => gavoe($ret['result'], array('etatCivil', 'nationalite', 'code', )),
                'libelle_nationalite' => gavoe($ret['result'], array('etatCivil', 'nationalite', 'libelle', )),
                'numero_de_voie' => '',
                'type_et_libelle_de_voie' => '',
                'premier_complement_adresse' => '',
                'second_complement_adresse' => '',
                'lieu_dit' => '',
                'code_postal' => '',
                'libelle_de_commune' => '',
                'libelle_du_pays' => '',
                'id_bureau_de_vote' => '',
                'code_bureau_de_vote' => '',
                'libelle_bureau_de_vote' => '',
                'numero_d_ordre_dans_le_bureau_de_vote' => '',
                'code_circonscription_legislative' => '',
                'libelle_circonscription_legislative' => '',
                'code_canton' => '',
                'libelle_canton' => '',
                'code_motif_inscription' => '',
                'libelle_motif_inscription' => '',
            );
        }

        //
        return $row;
    }

    /**
     * VIEW - view_edition_pdf__attestationelecteur.
     *
     * @return void
     */
    public function view_edition_pdf__attestationelecteur() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "attestationelecteur"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'attestationelecteur-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__carteelecteur.
     *
     * @return void
     */
    public function view_edition_pdf__carteelecteur() {
        $this->checkAccessibility();
        $params = array(
            "mode_edition" => "electeur",
            "electeur_id" => $this->getVal($this->clePrimaire),
        );
        //
        require_once "../obj/edition_pdf__carte_electorale.class.php";
        $inst_edition_pdf = new edition_pdf__carte_electorale();
        $pdf_edition = $inst_edition_pdf->compute_pdf__carte_electorale($params);
        $this->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
    }

    /**
     *
     */
    function getDataSubmit() {
        //
        $datasubmit = parent::getDataSubmit();
        //
        $datasubmit .= "&amp;nom=".$this->getParameter("nom");
        $datasubmit .= "&amp;exact=".$this->getParameter("exact");
        $datasubmit .= "&amp;prenom=".$this->getParameter("prenom");
        $datasubmit .= "&amp;datenaissance=".$this->getParameter("datenaissance");
        $datasubmit .= "&amp;marital=".$this->getParameter("marital");
        $datasubmit .= "&amp;origin=".$this->getParameter("origin");
        $datasubmit .= "&amp;retour=".$this->getParameter("retour");
        //
        return $datasubmit;
    }

    /**
     * Cette methode est appelee lors d'un traitement
     * Elle appplique un mouvement d'inscription sur un electeur
     * INSERT electeur
     *
     * @param array $val
     * @param string $datebleau
     *
     * @return boolean
     */
    function ajouterTraitement($val = array(), $datetableau = "") {
        // Initialisation message
        $this->msg = "";
        //
        $this->setId();
        $this->setValFTraitement($val, $datetableau);
        $this->setValFNumeroBureau();
        $this->setValFInformationInscription($val);
        // Insertion ELECTEUR
        $res = $this->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, $this->table),
            $this->valF,
            DB_AUTOQUERY_INSERT
        );
        $this->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $this->table)."\", ".print_r($this->valF, true).", DB_AUTOQUERY_INSERT);",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true) !== false) {
            return false;
        }
        if ($this->f->db->affectedRows() != 1) {
            return false;
        }
        //
        $this->msg .= __("Enregistrement ").$this->valF[$this->clePrimaire].__(" de la table ").$this->table." [".$this->f->db->affectedRows().__(" enregistrement(s) ajoute(s)]")."<br />";
        return true;
    }

    /**
     * Cette methode est appelee lors d'un traitement
     * Elle appplique un mouvement de modification sur un electeur
     * UPDATE electeur
     *
     * @param array $val
     * @param string $datetableau
     *
     * @return boolean
     */
    function modifierTraitement($val = array(), $datetableau = "") {
        // Initialisation du message
        $this->msg = "";
        // Recuperation de l'identifiant de l'electeur
        $id = $this->getVal($this->clePrimaire);
        $cle = $this->getCle($id);
        //
        $this->setValFTraitement($val, $datetableau);
        if ($val["bureau"] != $val["ancien_bureau"]) {
            $this->setValFNumeroBureau();
        }
        // Verification numero_bureau
        if ($this->valF['numero_bureau'] >= 9000) {
            $this->setValFNumeroBureau();
        }
        //
        $res = $this->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, $this->table),
            $this->valF,
            DB_AUTOQUERY_UPDATE,
            $cle
        );
        $this->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $this->table)."\", ".print_r($this->valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        if ($this->f->db->affectedRows() != 1) {
            return false;
        }
        //
        $this->msg .= __("Enregistrement ").$id.__(" de la table ").$this->table." [".$this->f->db->affectedRows().__(" enregistrement mis a jour]")."<br />";
        return true;
    }

    /**
     * Cette methode est appelee lors d'un traitement
     * Elle appplique un mouvement de radiation sur un electeur
     * Et supprime tout procuration concernant l'electeur
     * DELETE electeur
     *
     * @param array $val
     *
     * @return boolean
     */
    function supprimerTraitement($val = array()) {
        // Initialisation message
        $this->msg = "";
        // Recuperation de l'identifiant de l'electeur
        $id = $this->getVal($this->clePrimaire);

        // Destruction PROCURATION
        $sql = sprintf(
            'UPDATE %1$sprocuration SET mandant=null WHERE mandant=%2$s',
            DB_PREFIXE,
            $id
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        $this->msg .= __("Enregistrement mandant = ").$id.__(" de la table procuration [").$this->f->db->affectedRows ().__(" enregistrement(s) modifié(s)]")."<br />";

        // Destruction PROCURATION
        $sql = sprintf(
            'UPDATE %1$sprocuration SET mandataire=null WHERE mandataire=%2$s',
            DB_PREFIXE,
            $id
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        $this->msg .= __("Enregistrement mandataire = ").$id.__(" de la table procuration [").$this->f->db->affectedRows ().__(" enregistrement(s) modifié(s)]")."<br />";

        // Suppression ELECTEUR
        $sql = sprintf(
            'DELETE FROM %1$s%2$s WHERE %3$s=%4$s',
            DB_PREFIXE,
            $this->table,
            $this->clePrimaire,
            $id
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        if ($this->f->db->affectedRows() != 1) {
            return false;
        }
        $this->msg .= __("Enregistrement ").$id.__(" de la table ").$this->table." [".$this->f->db->affectedRows().__(" enregistrement(s) supprime(s)]")."<br />";
        return true;
    }

    /**
     *
     */
    function setvalF($val = array()) {
        // Civilite
        $this->valF['civilite'] = $val['civilite'];
        $this->valF['nom'] = $val['nom'];
        $this->valF['sexe'] = $val['sexe'];
        $this->valF['nom_usage'] = $val['nom_usage'];
        $this->valF['prenom'] = $val['prenom'];
        // Naissance & Nationalite
        $this->valF['date_naissance'] = $val['date_naissance'];
        $this->valF['code_departement_naissance'] = $val['code_departement_naissance'];
        $this->valF['libelle_departement_naissance'] = $val['libelle_departement_naissance'];
        $this->valF['code_lieu_de_naissance'] = $val['code_lieu_de_naissance'];
        $this->valF['libelle_lieu_de_naissance'] = $val['libelle_lieu_de_naissance'];
        $this->valF['code_nationalite'] = $val[ 'code_nationalite'];
        // Adresse
        $this->valF['numero_habitation'] = $val['numero_habitation'];
        $this->valF['complement_numero'] = $val['complement_numero'];
        //
        $this->valF['code_voie'] = $val['code_voie'];
        //
        $this->valF['libelle_voie'] = $val['libelle_voie'];
        $this->valF['complement'] = $val['complement'] ;
        //????
        $this->valF['provenance'] = $val['provenance'];
        $this->valF['libelle_provenance'] = $val['libelle_provenance'];
        $this->valF['procuration'] = $val['procuration'];
        // Resident
        $this->valF['resident'] = $val['resident'];
        $this->valF['adresse_resident'] = $val['adresse_resident'];
        $this->valF['complement_resident'] = $val['complement_resident'];
        $this->valF['cp_resident'] = $val['cp_resident'];
        $this->valF['ville_resident'] = $val['ville_resident'];
        // Carte en Retour & Jury & Coordonnées
        unset($this->valF['carte']);
        unset($this->valF['jury']);
        unset($this->valF['jury_effectif']);
        unset($this->valF['date_jeffectif']);
        unset($this->valF['profession']);
        unset($this->valF['motif_dispense_jury']);
    }

    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Verification sur le nom
        if ($this->valF['nom']=="") {
            $this->correct=false;
            $this->msg .= "Le nom de l'electeur est obligatoire<br />";
        }
        // Verification sur le prenom
        if ($this->valF['prenom'] == "") {
            $this->correct=false;
            $this->msg .= "Le prenom de l'electeur est obligatoire<br />";
        }
        // Verifications Date de naissance
        if ($this->valF['date_naissance']=="") {
            $this->correct=false;
            $this->msg .= "La date de naissance est obligatoire<br />";
        } else {
            $aujourdhui = date ("d/m/Y");
            $temp['y'] = date ('Y');
            $temp['m'] = date ('m');
            $temp['d'] = date ('d');
            $temp0 = $this->valF['date_naissance'];
            $temp1['y'] = substr ($temp0, 6, 4);
            $temp1['m'] = substr ($temp0, 3, 2);
            $temp1['d'] = substr ($temp0, 0, 2);
            if ($temp['m']>=$temp1['m']){
                if ($temp['m']==$temp1['m']){
                    if ($temp['d']>=$temp1['d'])
                        $age = $temp['y'] - $temp1['y'];
                    else
                        $age = $temp['y'] - $temp1['y'] - 1;
                } else
                    $age = $temp['y'] - $temp1['y'];
            } else {
                $age = $temp['y'] - $temp1['y'] - 1;
            }
            $this->valF['date_naissance'] = $this->dateDB ($this->valF['date_naissance']);
            // controle electeur plus de 18 ans
            if ($age < 18) {
               $this->correct=false;
               $this->msg= $this->msg."L'electeur n'a pas 18 ans le ".$aujourdhui."<br />";
            }
        }
        // Gestion Resident
        if ($this->valF['resident'] == "Oui") {
            if ($this->valF['adresse_resident'] == "") {
                $this->correct = false;
        $this->msg .= "L'adresse de residence est obligatoire<br />";
            }
            if ($this->valF['cp_resident'] == "") {
                $this->correct = false;
                $this->msg .= "Le code postal de residence est obligatoire<br />";
            }
            if ($this->valF['ville_resident'] == "") {
                $this->correct = false;
                $this->msg .= "La ville de residence est obligatoire<br />";
            }
        }
        // Naissance
        if ($this->valF['code_departement_naissance']=="") {
            $this->correct=false;
            $this->msg= $this->msg."Code departement naissance obligatoire<br />";
        }
        if ($this->valF['libelle_departement_naissance']=="") {
            $this->correct=false;
            $this->msg= $this->msg."Libelle departement naissance obligatoire<br />";
        }
        if ($this->valF['code_lieu_de_naissance']=="") {
            $this->correct=false;
            $this->msg= $this->msg."Code lieu de naissance obligatoire<br />";
        }
        if ($this->valF['libelle_lieu_de_naissance']=="") {
            $this->correct=false;
            $this->msg= $this->msg."Libelle lieu de naissance obligatoire<br />";
        }
        // verification departement / code lieu de naissance
        // prise en compte des DOM 971 TOM 981 et etranger 99352 ...
        // verification sur les 2 premiers caracteres
        if (substr($this->valF['code_departement_naissance'],0,2)!=substr($this->valF['code_lieu_de_naissance'],0,2)) {
            $this->correct=false;
            $this->msg= $this->msg."<br>lieu de naissance n'est pas dans le departement de naissance";
        }
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setType (&$form, $maj) {
        parent::setType($form, $maj);
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);
        if ($maj < 2) {
            //ajouter et modifier
            // HYPOTHESE NON MODIFICATION *******************************
            for ($i=0;$i<count($this->champs);$i++)
               $form->setType($this->champs[$i],'hiddenstatic');
            $form->setType('procuration','text');
            // *** demande adullact 01/09/2009
            /*$form->setType('resident','select');
            $form->setType('adresse_resident','text');
            $form->setType('ville_resident','text');
            $form->setType('cp_resident','text');
            $form->setType('complement_resident','text');*/
            // ***
            $form->setType('date_naissance','hiddenstaticdate');
            $form->setType('date_modif','hiddenstaticdate');
            // **********************************************************
            // aucune information sur le  dernier mouvement
            $form->setType('tableau','hidden');
            $form->setType('date_tableau','hidden');
            $form->setType('envoi_cnen','hidden');
            $form->setType('date_cnen','hidden');
            $form->setType('mouvement','hidden');
            /*// Identification
            $form->setType('id','hiddenstatic');
            $form->setType('liste','hiddenstatic');
            $form->setType('bureauforce','hiddenstatic');
            $form->setType('numero_bureau','hiddenstatic');
            $form->setType('date_modif','hiddenstaticdate');
            $form->setType('utilisateur','hiddenstatic');
            // Etat civil
            $form->setType('civilite','select');
            $form->setType('sexe','select');
            // Adresse
            $form->setType('numero_habitation','hiddenstatic');
            $form->setType('code_voie','hiddenstatic');
            $form->setType('libelle_voie','hiddenstatic');
            $form->setType('complement_numero','select');
            // selection et combo
            $form->setType('provenance','comboD');
            $form->setType('libelle_provenance','comboG');
            // Naissance & Nationalite
            $form->setType('date_naissance','date');
            $form->setType('code_departement_naissance','comboD');
            $form->setType('libelle_departement_naissance','comboG');
            $form->setType('code_lieu_de_naissance','comboD');
            $form->setType('libelle_lieu_de_naissance','comboG');
            $form->setType('code_nationalite','select');
            // Resident
            $form->setType('resident', 'select');*/
            // Carte en retour & Jury
            $form->setType('carte', 'select');
            $form->setType('jury', 'select');
            $form->setType('jury_effectif', 'select');
            $form->setType('date_jeffectif','date');
            $form->setType('profession','text');
            $form->setType('motif_dispense_jury','text');
        }
        // Gestion Jury
        if ($maj == 53) {
            for ($i = 0; $i < count($this->champs); $i++) {
                $form->setType($this->champs[$i], 'hidden');
            }
            $form->setType('jury', 'select');
            $form->setType('jury_effectif', 'select');
            $form->setType('date_jeffectif', 'date');
            $form->setType('profession', 'text');
            $form->setType('motif_dispense_jury', 'text');
        }
        // Gestion Coordonnées
        if ($maj == 55) {
            for ($i = 0; $i < count($this->champs); $i++) {
                $form->setType($this->champs[$i], 'hidden');
            }
            $form->setType('courriel', 'text');
            $form->setType('telephone', 'text');
        }
        $form->setType('ine', 'hidden');
        $form->setType('reu_sync_info', 'hidden');
        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans om_formulaire.class.php est levé).
        // On sélectionne donc les actions de portlet de type action-direct ou
        // assimilé et les actions spécifiques avec le même comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if ($maj < 2) {
            //DYNAMIQUE
            // nationalite
            $res = $this->f->db->query($this->get_var_sql_forminc__sql("nationalite"));
            $this->addToLog(
                __METHOD__."(): db->query(\"".$this->get_var_sql_forminc__sql("nationalite")."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            $contenu="";
            $k=0;
            while ($row=& $res->fetchRow()) {
                $contenu[0][$k]=$row[0];
                $contenu[1][$k]=$row[1];
                $k++;
            }
            $form->setSelect("code_nationalite",$contenu);
            // STATIQUE
            // civilite
            $contenu="";
            $contenu[0]=array('M.','Mme','Mlle');
            $contenu[1]=array(__('Monsieur'),__('Madame'),__('Mademoiselle'));
            $form->setSelect("civilite",$contenu);
            // Sexe
            $contenu="";
            $contenu[0]=array('M','F');
            $contenu[1]=array(__('Masculin'),__('Feminin'));
            $form->setSelect("sexe",$contenu);
            // complement
            $contenu="";
            $contenu[0]=array('','bis','ter','quater','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
            $contenu[1]=array(__('Sans'),__('bis'),__('ter'),__('quater'),'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
            $form->setSelect("complement_numero",$contenu);
            // carte
            $contenu="";
            $contenu[0]=array('0','1');
            $contenu[1]=array(__('Non'),__('Oui'));
            $form->setSelect("carte",$contenu);
            // resident
            $contenu="";
            $contenu[0]=array('Non','Oui');
            $contenu[1]=array(__('Non'),__('Oui'));
            $form->setSelect("resident",$contenu);
        }
        // Gestion Jury
        if ($maj == 53) {
            // jury (selection en cours...)
            $form->setSelect("jury", array(
                0 => array("0", "1", ),
                1 => array(__("Non"), __("Oui"), ),
            ));
            if ($this->f->is_option_enabled('option_module_jures_suppleants') === true) {
                // jury (selection en cours...)
                $form->setSelect("jury", array(
                    0 => array("0", "1", "2"),
                    1 => array(__("Non"), __("Titulaire"), __("Suppléant")),
                ));
            }
            
            // jury_effectif
            $form->setSelect("jury_effectif", array(
                0 => array("non", "oui", ),
                1 => array(__("Non"), __("Oui"), ),
            ));
        }
    }

    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setOnchange (&$form, $maj) {
        $form->setOnchange("nom","this.value=this.value.toUpperCase()");
        $form->setOnchange("prenom","this.value=this.value.toUpperCase()");
        $form->setOnchange("nom_usage","this.value=this.value.toUpperCase()");

        $form->setOnchange("numero_habitation","VerifNum(this)");
        $form->setOnchange("complement","this.value=this.value.toUpperCase()");

        $form->setOnchange("libelle_departement_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_lieu_de_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_provenance","this.value=this.value.toUpperCase()");

        $form->setOnchange("adresse_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("complement_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("cp_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("ville_resident","this.value=this.value.toUpperCase()");

        $form->setOnchange("date_naissance","fdate(this)");

        $form->setOnchange("date_jeffectif","fdate(this)");
        // Gestion Coordonnées
        $form->setOnchange("telephone", "VerifNum(this)");
        $form->setOnchange("courriel", "is_email(this)");
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setLib (&$form, $maj) {
        parent::setLib($form, $maj);
        // Identification
        $form->setLib('id',__('Id electeur no '));
        $form->setLib('numero_bureau',__(' no '));
        $form->setLib('bureauforce',__('Bureau force '));
        $form->setLib('liste',__(' dans la liste '));
        $form->setLib('date_modif',__(' Modifie le '));
        $form->setLib('utilisateur',__(' par '));
        //Etat Civil
        $form->setLib('nom',__('Nom'));
        $form->setLib('prenom',__('Prenom'));
        $form->setLib('civilite',__('Civilite'));
        $form->setLib('sexe',__('Sexe'));
        $form->setLib('nom_usage',__("Nom d'usage"));
        // Naissance & Nationalite
        $form->setLib('date_naissance',__('Date de naissance'));
        $form->setLib('code_departement_naissance',__('Departement'));
        $form->setLib('libelle_departement_naissance',' ');
        $form->setLib('code_lieu_de_naissance',__('Lieu de naissance'));
        $form->setLib('libelle_lieu_de_naissance',' ');
        $form->setLib('code_nationalite',__('Nationalite'));
            // Adresse
        $form->setLib('numero_habitation',__('No'));
        $form->setLib('code_voie',__('Id/Libelle Voie'));
        $form->setLib('libelle_voie','');
        $form->setLib('complement_numero','');
        $form->setLib('complement','<td>'.__('Complement').'</td><td></td>');
        // Resident
        $form->setLib('resident',__('Resident'));
        $form->setLib('adresse_resident',__('Adresse'));
        $form->setLib('complement_resident',__('Complement'));
        $form->setLib('cp_resident',__('Code postal'));
        $form->setLib('ville_resident',__('Ville'));
        // Provenance
        $form->setLib('provenance',__('Code/Libelle commune de provenance '));
        $form->setLib('libelle_provenance','');
        // Carte en retour & Jury
        $form->setLib('carte',__(' Carte en retour '));
        $form->setLib('jury',__(' Jure en cours '));
        $form->setLib('jury_effectif',__(' Jure effectif '));
        $form->setLib('date_jeffectif',__(' Date prefecture: '));
        $form->setLib('profession',__('Profession'));
        $form->setLib('motif_dispense_jury',__('Motif de dispense'));
        // Procuration
        $form->setLib('procuration',__('Procuration'));
    }

    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setTaille (&$form, $maj) {
        $form->setTaille('nom',40);
        $form->setTaille('nom_usage',30);
        $form->setTaille('prenom',40); //pgsql

        $form->setTaille('liste',6);
        $form->setTaille('bureauforce',3);
        $form->setTaille('date_modif',10);
        $form->setTaille('utilisateur',30);

        $form->setTaille('libelle_commune',30);
        $form->setTaille('provenance_commune',30);
        $form->setTaille('code_departement_naissance',5);
        $form->setTaille('libelle_departement_naissance',30);
        $form->setTaille('code_lieu_de_naissance',6);  //pgsql
        $form->setTaille('libelle_lieu_de_naissance',30); //pgsql
        $form->setTaille('date_naissance',10);

        $form->setTaille('complement',30);
        $form->setTaille('provenance',6);
        $form->setTaille('libelle_provenance',30);
        $form->setTaille('observation',85);

        $form->setTaille('resident',3);
        $form->setTaille('adresse_resident',20);
        $form->setTaille('complement_resident',20);
        $form->setTaille('cp_resident', 10);
        $form->setTaille('ville_resident',10);

        $form->setTaille('procuration',50);
        $form->setTaille('jury_effectif',3);
        $form->setTaille('date_jeffectif',10);
    }

    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setMax (&$form, $maj) {
        $form->setMax('nom',40);
        $form->setMax('nom_usage',40);
        $form->setMax('prenom',40);

        $form->setMax('liste',2);
        $form->setMax('bureauforce',3);
        $form->setMax('date_modif',10);
        $form->setMax('utilisateur',30);

        $form->setMax('libelle_commune',30);
        $form->setMax('provenance_commune',30);
        $form->setMax('code_departement_naissance',5);
        $form->setMax('libelle_departement_naissance',30);

        $form->setMax('code_lieu_de_naissance',6);
        $form->setMax('libelle_lieu_de_naissance',30);

        $form->setMax('complement',80);
        $form->setMax('provenance',6);
        $form->setMax('libelle_provenance',30);
        $form->setMax('observation',100);

        $form->setMax('resident',3);
        $form->setMax('adresse_resident',40);
        $form->setMax('complement_resident',40);
        $form->setMax('cp_resident', 10);
        $form->setMax('ville_resident',30);

        $form->setMax('procuration',100);
        $form->setMax('jury_effectif',3);
        $form->setMax('date_jeffectif',10);
        $form->setMax('profession',30);
        $form->setMax('motif_dispense_jury',30);
    }

    /**
     * Parametrage du formulaire - Groupement des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setGroupe (&$form, $maj) {
        // Identification
        $form->setGroupe('id','D');
        $form->setGroupe('bureauforce','D');
        $form->setGroupe('numero_bureau','G');
        $form->setGroupe('bureau','F');
        $form->setGroupe('date_modif','D');
        $form->setGroupe('utilisateur','F');
        // Etat civil
        $form->setGroupe('civilite','D');
        $form->setGroupe('sexe','F');
        $form->setGroupe('nom','D');
        $form->setGroupe('nom_usage','F');
        $form->setGroupe('prenom','D');
        // Adresse
        $form->setGroupe('numero_habitation','D');
    $form->setGroupe('code_voie','G');
    $form->setGroupe('libelle_voie','F');
    $form->setGroupe('complement_numero','D');
    $form->setGroupe('complement','F');
        // Naissance & Nationalite
        $form->setGroupe('code_departement_naissance','D');
        $form->setGroupe('libelle_departement_naissance','F');
        $form->setGroupe('code_lieu_de_naissance','D');
        $form->setGroupe('libelle_lieu_de_naissance', 'F');
        // Resident
        $form->setGroupe('resident','D');
        $form->setGroupe('adresse_resident','G');
        $form->setGroupe('complement_resident','F');
        $form->setGroupe('cp_resident','D');
        $form->setGroupe('ville_resident','F');
        // Provenance
        $form->setGroupe('provenance','D');
    $form->setGroupe('libelle_provenance','F');
        // Carte en retour & Jury
        $form->setGroupe('jury','D');
        $form->setGroupe('jury_effectif','G');
        $form->setGroupe('date_jeffectif','F');
    }

    /**
     * Parametrage du formulaire - Regroupement des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setRegroupe (&$form, $maj) {
        // Identification
        $form->setRegroupe('id','D',__('Identification'));
        $form->setRegroupe('liste','G',' ');
        $form->setRegroupe('bureauforce','G','');
        $form->setRegroupe('numero_bureau','G','');
        $form->setRegroupe('date_modif','G','');
        $form->setRegroupe('utilisateur','F','');
        // Etat civil
        $form->setRegroupe('civilite','D',__('Etat Civil'));
        $form->setRegroupe('sexe','G','');
        $form->setRegroupe('nom','G','');
        $form->setRegroupe('nom_usage','G','');
        $form->setRegroupe('prenom','G','');
        // Naissance & Nationalite
        $form->setRegroupe('date_naissance','D', __('Naissance et Nationalité'));
        $form->setRegroupe('code_departement_naissance','G','');
        $form->setRegroupe('libelle_departement_naissance','G','');
        $form->setRegroupe('code_lieu_de_naissance','G','');
        $form->setRegroupe('libelle_lieu_de_naissance','G','');
        $form->setRegroupe('code_nationalite','F','');
        // Adresse
    $form->setRegroupe('numero_habitation','D',__('Adresse'));
    $form->setRegroupe('code_voie','G','');
    $form->setRegroupe('libelle_voie','G','');
    $form->setRegroupe('complement_numero','G','');
    $form->setRegroupe('complement','F','');
    // Resident
    $form->setRegroupe('resident','D',__('Resident'));
    $form->setRegroupe('adresse_resident','G','');
    $form->setRegroupe('complement_resident','G','');
    $form->setRegroupe('cp_resident','G','');
    $form->setRegroupe('ville_resident','F','');
    // Provenance
    $form->setRegroupe('provenance','D',__('Provenance'));
    $form->setRegroupe('libelle_provenance','F','');
    }

    /**
     * Cette methode permet de remplir les nouvelles donnees de l'electeur
     * en fonction des donnees du mouvement represente par $val
     *
     * @param array $val Tableau de donnees
     * @param string $datetableau
     *
     * @return void
     */
    function setValFTraitement($val = array(), $datetableau = "") {
        // identifiant
        $this->valF['bureau'] = $val['bureau'];
        $this->valF['bureauforce'] = $val['bureauforce'];
        $this->valF['numero_bureau'] = ($val['numero_bureau'] !== '' ? $val['numero_bureau'] : null); // provisoire en inscription
        // Etat civil
        $this->valF['civilite'] = $val['civilite'];
        $this->valF['sexe'] = $val['sexe'];
        $this->valF['nom'] = $val['nom'];
        $this->valF['nom_usage'] = $val['nom_usage'];
        $this->valF['prenom'] = $val['prenom'];
        $this->valF['date_naissance'] = $val['date_naissance'];
        $this->valF['code_departement_naissance'] = $val['code_departement_naissance'];
        $this->valF['libelle_departement_naissance'] = $val['libelle_departement_naissance'];
        $this->valF['code_lieu_de_naissance'] = $val['code_lieu_de_naissance'];
        $this->valF['libelle_lieu_de_naissance'] = $val['libelle_lieu_de_naissance'];
        $this->valF['code_nationalite'] = $val['code_nationalite'];
        //adresse
        $this->valF['code_voie'] = $val['code_voie'];
        $this->valF['libelle_voie'] = $val['libelle_voie'];
        $this->valF['numero_habitation'] = $val['numero_habitation'];
        $this->valF['complement_numero'] = $val['complement_numero'] ;
        $this->valF['complement'] = $val['complement'] ;
        // provenance
        $this->valF['provenance'] = $val['provenance'];
        $this->valF['libelle_provenance'] = $val['libelle_provenance'];
        // adresse resident
        $this->valF['resident'] = $val['resident'];
        $this->valF['adresse_resident'] = $val['adresse_resident'];
        $this->valF['complement_resident'] = $val['complement_resident'];
        $this->valF['cp_resident'] = $val['cp_resident'] ;
        $this->valF['ville_resident'] = $val['ville_resident'] ;
        //  mouvement
        $this->valF['date_modif'] = $val['date_modif'];
        $this->valF['utilisateur'] = $val['utilisateur'];
        $this->valF['mouvement'] = $val['types'];
        $this->valF['date_mouvement'] = $this->dateSystemeDB();
        $this->valF['typecat'] = "";
        //tableau
        $this->valF['date_tableau'] = $datetableau;
        //liste
        $this->valF['liste'] = $val['liste'];
        //
        $this->valF['telephone'] = $val['telephone'];
        $this->valF['courriel'] = $val['courriel'];
        //
        $this->valF['carte'] = $this->getVal('carte') !== null && $this->getVal('carte') !== '' ? $this->getVal('carte') : 0;
        $this->valF['om_collectivite'] = $val['om_collectivite'];
        if (isset($val['ine']) !== true || is_numeric($val['ine']) !== true) {
            $this->valF['ine'] = null;
        } else {
            $this->valF['ine'] = $val['ine'];
        }
    }

    /**
    * Cette methode recherche le dernier numero dans le bureau et pour la liste
    * pour pouvoir recuperer le numero du nouvel electeur et l'incrementer
    * dans la table numerobureau
    * UPDATE numerobureau
    *
    * @return void
    */
    function setValFNumeroBureau() {
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done") {
            $this->valF['numero_bureau'] = null;
        } else {
            // Génère le prochain numéro
            $numerobureau = $this->f->get_inst__om_dbform(array(
                "obj" => "numerobureau",
            ));
            $this->valF['numero_bureau'] = $numerobureau->nextDernierNumero(
                $this->valF['bureau'],
                $this->valF['liste'],
                $this->valF['om_collectivite']
            );
        }
    }

    /**
     * Cette methode recupere des informations sur l'inscription
     *
     * @param array $val
     *
     * @return void
     */
    function setValFInformationInscription($val = array()) {
        // Recuperation d'informations sur l'inscription
        $this->valF['code_inscription'] = $val['types'];
        $this->valF['date_inscription'] = $this->dateSystemeDB();
    }

    /**
     * Statistiques - 'electeur_age_bureau'.
     *
     * Nombre d'electeurs, par tranche d'age par bureau pour la
     * liste en cours et pour la collectivite en cours
     *
     * @todo A quoi sert la colonne "Ecart" ?
     * @todo N'est-il pas plus pertinent de calculer l'age en fonction du 1er mars
     * de l'annee de la date de tableau en cours ?
     *
     * @return array
     */
    function compute_stats__electeur_age_bureau() {
        // Definition des tranches d'age
        $tranche = array (
            0 => array ('titre' => __("18 ans"), 'debut' => 18, 'fin' => 18, 'res' => 0),
            1 => array ('titre' => __("19-21"), 'debut' => 19, 'fin' => 21, 'res' => 0),
            2 => array ('titre' => __("22-30"), 'debut' => 22, 'fin' => 30, 'res' => 0),
            3 => array ('titre' => __("31-40"), 'debut' => 31, 'fin' => 40, 'res' => 0),
            4 => array ('titre' => __("41-50"), 'debut' => 41, 'fin' => 50, 'res' => 0),
            5 => array ('titre' => __("51-60"), 'debut' => 51, 'fin' => 60, 'res' => 0),
            6 => array ('titre' => __("61-70"), 'debut' => 61, 'fin' => 70, 'res' => 0),
            7 => array ('titre' => __("71-80"), 'debut' => 71, 'fin' => 80, 'res' => 0),
            8 => array ('titre' => __("81-90"), 'debut' => 81, 'fin' => 90, 'res' => 0),
            9 => array ('titre' => __("91+"), 'debut' => 91, 'fin' => 150, 'res' => 0)
        );
        //
        $total = 0;
        $totalecart = 0;
        //
        $data = array();
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
            //
            $datas = array();
            //
            array_push($datas, $bureau["code"]);
            array_push($datas, $bureau["libelle"]);
            /**
             * Cette requete permet de compter tous les electeurs de la table electeur en
             * fonction de la collectivite en cours, de la liste en cours et du bureau
             * selectionne
             *
             * @param string $bureau["id"] Idenitifiant du bureau
             * @param string $_SESSION["collectivite"]
             * @param string $_SESSION["liste"]
             */
            $query_count_electeur_bureau = sprintf(
                'SELECT count(*) FROM %1$selecteur WHERE electeur.liste=\'%3$s\' and electeur.om_collectivite=%2$s and electeur.bureau=%4$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $_SESSION["liste"],
                intval($bureau["id"])
            );
            $res_count_electeur_bureau = $this->f->db->getOne($query_count_electeur_bureau);
            $this->f->isDatabaseError($res_count_electeur_bureau);
            array_push($datas, $res_count_electeur_bureau);
            //
            $total += $res_count_electeur_bureau;

            //
            $ecart = 0;
            //
            foreach ($tranche as $key => $t) {
                /**
                 * Cette requete permet de compter tous les electeurs de la table electeur en
                 * fonction de la collectivite en cours, de la liste en cours, du bureau
                 * selectionne et en fonction de la categorie d'age souhaitee
                 *
                 * @param string $bureau["id"] Idenitifiant du bureau
                 * @param integer $t['debut'] Debut de la categorie d'age
                 * @param integer $t['fin'] Fin de la categorie d'age
                 * @param string $_SESSION["collectivite"]
                 * @param string $_SESSION["liste"]
                 */
                $query_count_electeur_bureau_age = $query_count_electeur_bureau;
                $query_count_electeur_bureau_age .= " and date_part('year', date_naissance)<=".(date('Y') - $t['debut'])." ";
                $query_count_electeur_bureau_age .= " and date_part('year', date_naissance)>=".(date('Y') - $t['fin'])." ";
                //
                $res_count_electeur_bureau_age = $this->f->db->getOne($query_count_electeur_bureau_age);
                $this->f->isDatabaseError($res_count_electeur_bureau_age);
                //
                array_push($datas, $res_count_electeur_bureau_age);
                //
                $tranche [$key]['res'] += $res_count_electeur_bureau_age;
                $ecart += $res_count_electeur_bureau_age;
            }

            //
            array_push($datas, $ecart);

            //
            $totalecart += $ecart;

            //
            $data[] = $datas;
        }

        $datas = array();
        array_push($datas, "-");
        array_push($datas, __("TOTAL"));
        array_push($datas, $total);
        foreach ($tranche as $t) {
            array_push($datas, $t['res']);
        }
        array_push($datas, $totalecart);
        $data[] = $datas;

        $column = array();
        array_push($column, "");
        array_push($column, __("LISTE")." : ".$_SESSION["libelle_liste"]);
        array_push($column, __("Total"));
        foreach ($tranche as $t) {
            array_push($column, $t['titre']);
        }
        array_push($column, __("Ecart"));

        $offset = array();
        array_push($offset, 10);
        array_push($offset, 0);
        array_push($offset, 11);
        foreach ($tranche as $t) {
            array_push($offset, 11);
        }
        array_push($offset, 11);

        // Array
        return array(
            "format" => "L",
            "title" => __("Statistiques"),
            "subtitle" => __("Nombre d'electeurs, par tranche d'age par bureau pour la liste en cours"),
            "offset" => $offset,
            "column" => $column,
            "data" => $data,
            "output" => "stats-electeuragebureau"
        );
    }

    /**
     * Statistiques - 'electeur_sexe_bureau'.
     *
     * Nombre d'electeurs, d'hommes, de femmes par bureau pour la
     * liste en cours et pour la collectivite en cours
     *
     * @return array
     */
    function compute_stats__electeur_sexe_bureau() {
        //
        $total_count_electeur_bureau = 0;
        $total_count_electeur_homme_bureau = 0;
        $total_count_electeur_femme_bureau = 0;
        //
        $data = array();
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
            /**
             * Cette requete permet de compter tous les electeurs de la table electeur en
             * fonction de la collectivite en cours, de la liste en cours et du bureau
             * selectionne
             *
             * @param string $bureau["id"] Identifiant du bureau
             * @param string $_SESSION["collectivite"]
             * @param string $_SESSION["liste"]
             */
            $query_count_electeur_bureau = sprintf(
                'SELECT count(*) FROM %1$selecteur WHERE electeur.liste=\'%3$s\' and electeur.om_collectivite=%2$s and electeur.bureau=%4$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $_SESSION["liste"],
                intval($bureau["id"])
            );
            //
            $res_count_electeur_bureau = $this->f->db->getOne($query_count_electeur_bureau);
            $this->f->isDatabaseError($res_count_electeur_bureau);
            //
            $total_count_electeur_bureau += $res_count_electeur_bureau;
            /**
             * Cette requete permet de compter tous les electeurs de la table electeur en
             * fonction de la collectivite en cours, de la liste en cours et du bureau
             * selectionne et de sexe Masculin
             *
             * @param string $bureau["id"] Identifiant du bureau
             * @param string $_SESSION["collectivite"]
             * @param string $_SESSION["liste"]
             */
            $query_count_electeur_homme_bureau = $query_count_electeur_bureau;
            $query_count_electeur_homme_bureau .= " and sexe='M' ";
            //
            $res_count_electeur_homme_bureau = $this->f->db->getOne($query_count_electeur_homme_bureau);
            $this->f->isDatabaseError($res_count_electeur_homme_bureau);
            //
            $total_count_electeur_homme_bureau += $res_count_electeur_homme_bureau;
            /**
             * Cette requete permet de compter tous les electeurs de la table electeur en
             * fonction de la collectivite en cours, de la liste en cours et du bureau
             * selectionne et de sexe Feminin
             *
             * @param string $bureau["id"] Identifiant du bureau
             * @param string $_SESSION["collectivite"]
             * @param string $_SESSION["liste"]
             */
            $query_count_electeur_femme_bureau = $query_count_electeur_bureau;
            $query_count_electeur_femme_bureau .= " and sexe='F' ";
            //
            $res_count_electeur_femme_bureau = $this->f->db->getOne($query_count_electeur_femme_bureau);
            $this->f->isDatabaseError($res_count_electeur_femme_bureau);
            //
            $total_count_electeur_femme_bureau += $res_count_electeur_femme_bureau;
            //
            $datas = array(
                $bureau["code"],
                $bureau["libelle"],
                $res_count_electeur_bureau,
                $res_count_electeur_homme_bureau,
                $res_count_electeur_femme_bureau
            );
            $data[] = $datas;
        }

        //
        $data[] = array(
            "-",
            __("TOTAL"),
            $total_count_electeur_bureau,
            $total_count_electeur_homme_bureau,
            $total_count_electeur_femme_bureau
        );
        // Array
        return array(
            "format" => "L",
            "title" => __("Statistiques - Electeur"),
            "subtitle" => __("Details par sexe et bureau de vote"),
            "offset" => array(10,0,40,40,40),
            "column" => array(
                "",
                __("LISTE")." : ".$_SESSION["libelle_liste"]."",
                __("Total"),
                __("Hommes"),
                __("Femmes")
            ),
            "data" => $data,
            "output" => "stats-electeursexebureau"
        );
    }

    /**
     * Statistiques - 'electeur_sexe_departementnaissance'.
     *
     * Nombre d'electeurs, d'hommes, de femmes par département
     * (ou pays) de naissance pour la liste en cours
     *
     * @return array
     */
    function compute_stats__electeur_sexe_departementnaissance() {
        // Recuperation des donnees
        $data = array();

        // STATISTIQUES :
        $inc = "stats_electeur_sexe_departementnaissance";

        //
        include "../sql/".OM_DB_PHPTYPE."/".$inc.".inc.php";

        $res = $this->f->db->query($sql_departement);
        $this->f->isDatabaseError($res);

        //
        $total = 0;
        $totalres1 = 0;
        $totalres2 = 0;

        //
        $numArray = 0;
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            include "../sql/".OM_DB_PHPTYPE."/".$inc.".inc.php";
            // Total
            $res0 = $this->f->db->getone($sqlB);
            $this->f->addToLog(
                __METHOD__."(): db->getone(\"".$sqlB."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res0);
            //
            $total += $res0;
            // Hommes
            $res1 = $this->f->db->getone($sql1);
            $this->f->isDatabaseError($res1);
            $totalres1 += $res1;
            // Femmes
            $res2 = $this->f->db->getone($sql2);
            $this->f->isDatabaseError($res2);
            $totalres2 += $res2;
            //
            $datas = array(
                $row['code'],
                $row['libelle_departement'],
                $res0,
                $res1,
                $res2
            );
            $data[$numArray] = $datas;
            $numArray++;
        }

        // Total
        $res0 = $this->f->db->getone($sqlB_a);
        $this->f->isDatabaseError($res0);
        $total += $res0;
        // Hommes
        $res1 = $this->f->db->getone($sql1_a);
        $this->f->isDatabaseError($res1);
        $totalres1 += $res1;
        // Femmes
        $res2 = $this->f->db->getone($sql2_a);
        $this->f->isDatabaseError($res2);
        $totalres2 += $res2;

        //
        $data[$numArray] = array(
            "-",
            __("Autres"),
            $res0,
            $res1,
            $res2
        );
        $numArray++;
        $data[$numArray] = array(
            "-",
            __("TOTAL"),
            $total,
            $totalres1,
            $totalres2
        );

        // Array
        return array(
            "format" => "P",
            "title" => __("Statistiques - Electeur"),
            "subtitle" => __("Details par departement (ou pays) de naissance pour la liste en cours"),
            "offset" => array(10,0,30,30,30),
            "column" => array(
                "",
                __("LISTE")." : ".$_SESSION["libelle_liste"]."",
                __("Total"),
                __("Hommes"),
                __("Femmes")
            ),
            "data" => $data,
            "output" => "stats-electeursexedeptnaiss"
        );
    }

    /**
     * Statistiques - 'electeur_sexe_nationalite'.
     *
     * Nombre d'electeurs, d'hommes, de femmes par sexe et nationalite
     * pour la liste en cours et pour la collectivite en cours
     *
     * @return array
     */
    function compute_stats__electeur_sexe_nationalite() {
        // Recuperation des donnees
        $data = array();

        // STATISTIQUES : Nombre d'electeurs, d'hommes, de femmes par nationalite pour
        //  la liste en cours
        $inc = "stats_electeur_sexe_nationalite";
        include "../sql/".OM_DB_PHPTYPE."/".$inc.".inc.php";

        //
        $res = $this->f->db->query($sql_nationalite);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$sql_nationalite."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);

        //
        $total = 0;
        $totalres1 = 0;
        $totalres2 = 0;

        //
        $numArray = 0;
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            //
            include "../sql/".OM_DB_PHPTYPE."/".$inc.".inc.php";
            // Total
            $res0 = $this->f->db->getone($sqlB);
            $this->f->addToLog(
                __METHOD__."(): db->getone(\"".$sqlB."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res0);
            //
            $total += $res0;
            // Hommes
            $res1 = $this->f->db->getone($sql1);
            $this->f->isDatabaseError($res1);
            $totalres1 += $res1;
            // Femmes
            $res2 = $this->f->db->getone($sql2);
            $this->f->isDatabaseError($res2);
            $totalres2 += $res2;
            //
            $datas = array(
                $row['code'],
                $row['libelle_nationalite'],
                $res0,
                $res1,
                $res2
            );
            $data[$numArray] = $datas;
            $numArray++;
        }

        //
        $data[$numArray] = array(
            "-",
            __("TOTAL"),
            $total,
            $totalres1,
            $totalres2
        );
        // Array
        return array(
            "format" => "L",
            "title" => __("Statistiques - Electeur"),
            "subtitle" => __("Details par sexe et nationalite"),
            "offset" => array(10,0,40,40,40),
            "column" => array(
                "",
                __("LISTE")." : ".$_SESSION["libelle_liste"]."",
                __("Total"),
                __("Hommes"),
                __("Femmes")
            ),
            "data" => $data,
            "output" => "stats-electeursexenationalite"
        );
    }

    /**
     * Statistiques - 'electeur_sexe_voie'.
     *
     * Nombre d'electeurs, d'hommes, de femmes par sexe et voie
     * pour la liste en cours et pour la collectivite en cours
     *
     * @return array
     */
    function compute_stats__electeur_sexe_voie() {
        // Recuperation des donnees
        $data = array();

        // STATISTIQUES : Nombre d'electeurs, d'hommes, de femmes par voie pour la
        //   liste en cours
        $inc = "stats_electeur_sexe_voie";
        include "../sql/".OM_DB_PHPTYPE."/".$inc.".inc.php";

        //
        $res = $this->f->db->query($sql_voie);
        $this->f->addToLog(
            __METHOD__."(): db->getone(\"".$sql_voie."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);

        //
        $total = 0;
        $totalres1 = 0;
        $totalres2 = 0;

        //
        $numArray = 0;
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            //
            include "../sql/".OM_DB_PHPTYPE."/".$inc.".inc.php";
            // Total
            $res0 = $this->f->db->getone($sqlB);
            $this->f->addToLog(
                __METHOD__."(): db->getone(\"".$sqlB."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res0);
            //
            $total += $res0;
            // Hommes
            $res1 = $this->f->db->getone($sql1);
            $this->f->isDatabaseError($res1);
            $totalres1 += $res1;
            // Femmes
            $res2 = $this->f->db->getone($sql2);
            $this->f->isDatabaseError($res2);
            $totalres2 += $res2;
            //
            $datas = array(
               $row['code'],
               $row['libelle_voie'],
               $res0,
               $res1,
               $res2
            );
            $data[$numArray] = $datas;
            $numArray++;
        }
        //
        $data[$numArray] = array(
            "-",
            __("TOTAL"),
            $total,
            $totalres1,
            $totalres2
        );
        // Array
        return array(
            "format" => "L",
            "title" => __("Statistiques - Electeur"),
            "subtitle" => __("Details par sexe et voie"),
            "offset" => array(10,0,40,40,40),
            "column" => array(
                "",
                __("LISTE")." : ".$_SESSION["libelle_liste"]."",
                __("Total"),
                __("Hommes"),
                __("Femmes")
            ),
            "data" => $data,
            "output" => "stats-electeursexevoie",
        );
    }

    /**
     * VIEW - view_edition_pdf__statistiques_electeur_age_bureau.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_electeur_age_bureau() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__electeur_age_bureau(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__statistiques_electeur_sexe_bureau.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_electeur_sexe_bureau() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__electeur_sexe_bureau(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__statistiques_electeur_sexe_departementnaissance.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_electeur_sexe_departementnaissance() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__electeur_sexe_departementnaissance(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__statistiques_electeur_sexe_nationalite.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_electeur_sexe_nationalite() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__electeur_sexe_nationalite(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__statistiques_electeur_sexe_voie.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_electeur_sexe_voie() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__electeur_sexe_voie(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     *
     */
    function get_inst__bureau() {
        return $this->f->get_inst__om_dbform(array(
            "obj" => "bureau",
            "idx" => $this->getVal("bureau"),
        ));
    }

    /**
     *
     */
    function get_inst__voie() {
        return $this->f->get_inst__om_dbform(array(
            "obj" => "voie",
            "idx" => $this->getVal("code_voie"),
        ));
    }

    /**
     *
     */
    function get_inst__nationalite() {
        return $this->f->get_inst__om_dbform(array(
            "obj" => "nationalite",
            "idx" => $this->getVal("code_nationalite"),
        ));
    }

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     *
     * @return array
     */
    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        $labels["electeur"]["electeur.civilite_libelle"] = __("Civilité en toutes lettres");
        // voie
        $labels["electeur"]["electeur.cp"] = __("Code Postal");
        $labels["electeur"]["electeur.ville"] = __("Ville");
        //
        $bureau = $this->get_inst__bureau();
        $bureau_labels = $bureau->get_merge_fields("labels");
        $labels["electeur"] = array_merge(
            $labels["electeur"],
            $bureau_labels["bureau"]
        );
        // Retour de tous les libellés
        return $labels;
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     *
     * @return array
     */
    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        if ($this->getVal("numero_habitation") == "0") {
            $values["electeur.numero_habitation"] = "";
        }
        //
        $voie = $this->get_inst__voie();
        $values["electeur.cp"] = $voie->getVal("cp");
        $values["electeur.ville"] = $voie->getVal("ville");
        //
        switch ($this->getVal("civilite")) {
            case "M.":
                $values["electeur.civilite_libelle"] = "Monsieur";
                break;
            case "Mme":
                $values["electeur.civilite_libelle"] = "Madame";
                break;
            case "Mlle":
                $values["electeur.civilite_libelle"] = "Mademoiselle";
                break;
            default:
                $values["electeur.civilite_libelle"] = $this->getVal("civilite");
        }
        //
        $bureau = $this->get_inst__bureau();
        $values = array_merge(
            $values,
            $bureau->get_merge_fields("values")
        );
        //
        return $values;
    }

    /**
     * VIEW - view_edition_pdf__listing_electeurs_carteretour.
     *
     * @return void
     */
    public function view_edition_pdf__listing_electeurs_carteretour() {
        $this->checkAccessibility();
        $usecase = "fallback";
        $params = array();

        // Vérifie si un mode d'édition a été spécifié
        if (! empty($_GET["mode_edition"])) {
            // Cas de l'édition : "parbureau"
            // En mode par bureau, l'identifiant du bureau est fourni via un formulaire
            // (POST). Vérifie si on a cet identifiant et si c'est le cas 
            if ($_GET["mode_edition"] == "parbureau") {
                $params["mode_edition"] = "parbureau";
                if (isset($_POST["bureau"]) && isset($_POST["liste"])) {
                    $params["bureau_code"] = $_POST["bureau"];
                    $params["liste"] = $_POST["liste"];
                    $usecase = "view_editions_par_bureau";
                }
            }
        } elseif (isset($_GET["bureau"]) && $_GET["bureau"] != '') {
            $params["bureau_code"] = $_GET["bureau"];
            $usecase = "view_editions_par_bureau";
        }

        // Si aucun mode d'édition n'est précisé renvoie l'utilisateur sur son tableau
        // de bord
        if ($usecase == "fallback") {
            $this->f->goToDashboard();
            return;
        }
        // Génération et affichagede l'édition
        require_once "../obj/edition_pdf__listing_electeurs_carteretour.class.php";
        $inst_edition_pdf = new edition_pdf__listing_electeurs_carteretour();
        $pdf_edition = $inst_edition_pdf->compute_pdf__listing_electeurs_carteretour($params);
        $inst_edition_pdf->expose_pdf_output(
            $pdf_edition["pdf_output"],
            $pdf_edition["filename"]
        );
        return;
    }

    /**
     * VIEW - view_edition_pdf__carte_electorale.
     *
     * @return void
     */
    public function view_edition_pdf__carte_electorale() {
        $this->checkAccessibility();
        $usecase = "fallback";
        $params = array();
        $liste = $_SESSION["liste"];
        if (isset($_GET["mode_edition"])) {
            if ($_GET["mode_edition"] == "commune") {
                $params["mode_edition"] = "commune";
                if (isset($_GET["liste"])) {
                    $params["liste"] = $this->f->get_submitted_get_value("liste");
                    $liste = $this->f->get_submitted_get_value("liste");
                }
                if (isset($_GET["stockage"])
                    && $_GET["stockage"] === "true") {
                    //
                    if (isset($_GET["origin"])
                        && $_GET["origin"] === "module") {
                        $usecase = "module";
                    } else {
                        $usecase = "view_editions_generales";
                    }
                } else {
                    $usecase = "fallback";
                }
            } elseif ($_GET["mode_edition"] == "parbureau") {
                $params["mode_edition"] = "parbureau";
                // TODO : Vérifier si le order_by_datenaissance est toujours utilisé et le supprimer sinon
                if (isset($_POST["order_by_datenaissance"])) {
                    $params["order_by_datenaissance"] = true;
                }
                if (isset($_POST["tri"])) {
                    $params['tri'] = $_POST["tri"];
                }
                if (isset($_GET["bureau_code"])) {
                    $params["bureau_code"] = $_GET["bureau_code"];
                    $usecase = "view_editions_par_bureau";
                } elseif (isset($_POST["bureau"]) && isset($_POST["liste"])) {
                    $params["bureau_code"] = $_POST["bureau"];
                    $params["liste"] = $_POST["liste"];
                    $usecase = "view_editions_par_bureau";
                } else {
                    $usecase = "fallback";
                }

            } elseif ($_GET["mode_edition"] == "traitement") {
                $params["mode_edition"] = "traitement";
                if (isset($_GET["traitement"]) && $_GET["traitement"] == "annuel") {
                    $params["traitement"] = "annuel";
                    if (isset($_GET["datetableau"])) {
                        $params["datetableau"] = $_GET["datetableau"];
                        $usecase = "revision_electorale";
                    } else {
                        $usecase = "fallback";
                    }
                } elseif (isset($_GET["traitement"]) && $_GET["traitement"] == "j5") {
                    $params["traitement"] = "j5";
                    if (isset($_GET["datetableau"])
                        && isset($_GET["datej5"])) {
                        $params["datetableau"] = $_GET["datetableau"];
                        $params["datej5"] = $_GET["datej5"];
                        $usecase = "revision_electorale";
                    } else {
                        $usecase = "fallback";
                    }
                } else {
                    $usecase = "fallback";
                }
            } elseif ($_GET["mode_edition"] == "multielecteur") {
                $params["mode_edition"] = "multielecteur";
                $params["electeur_ids"] = explode(";", $this->f->get_submitted_get_value("electeur_ids"));
                $usecase = "direct";
            } elseif ($_GET["mode_edition"] == "lastone") {
                $params["mode_edition"] = "lastone";
                if (isset($_POST["liste"])) {
                    $params["liste"] = $_POST["liste"];
                    if (isset($_POST["date"])) {
                        $params["date"] = $_POST["date"];
                    }
                    if (isset($_POST["sans_changement_de_bureau"])) {
                        $params["modifications_sans_changement_bureau"] = true;
                    }
                    if (isset($_POST["order_by_datenaissance"])) {
                        $params["order_by_datenaissance"] = true;
                    }
                    if (isset($_GET["source_lastone"])) {
                        $params["source_lastone"] = $_GET["source_lastone"];
                    }
                    if (isset($_POST["datedenaissance_au"])) {
                        $params["datedenaissance_au"] = $_POST["datedenaissance_au"];
                    }
                    $usecase = "direct";
                } else {
                    $usecase = "fallback";
                }
            } elseif ($_GET["mode_edition"] == "pardatedenaissance") {
                $params["mode_edition"] = "pardatedenaissance";
                if (isset($_POST["liste"])) {
                    $params["liste"] = $_POST["liste"];
                    if (isset($_POST["datedenaissance_du"])) {
                        $params["datedenaissance_du"] = $_POST["datedenaissance_du"];
                    }
                    if (isset($_POST["datedenaissance_au"])) {
                        $params["datedenaissance_au"] = $_POST["datedenaissance_au"];
                    }
                    $usecase = "direct";
                } else {
                    $usecase = "fallback";
                }
            } else {
                $usecase = "fallback";
            }
        }
        if ($usecase == "fallback") {
            $this->f->goToDashboard();
            return;
        }
        //
        require_once "../obj/edition_pdf__carte_electorale.class.php";
        $inst_edition_pdf = new edition_pdf__carte_electorale();
        $pdf_edition = $inst_edition_pdf->compute_pdf__carte_electorale($params);
        //
        if ($usecase === "view_editions_generales"
            || $usecase === "module") {
            // Composition des métadonnées du fichier
            $filemetadata = array(
                "filename" => "carteelecteur-".$_SESSION["collectivite"]."-".$liste.".pdf",
                "size" => strlen($pdf_edition["pdf_output"]),
                "mimetype" => "application/pdf",
            );
            // Écriture du fichier
            $uid = $this->f->store_file(
                $pdf_edition["pdf_output"],
                $filemetadata,
                "pdf"
            );
            if ($uid === false) {
                $this->f->addToMessage(
                    "error",
                    __("Erreur lors du stockage/enregistrement du fichier.")
                );
                $this->f->setFlag(null);
                $this->f->display();
                return;
            }
            if ($usecase === "module") {
                @header("Location: ../app/index.php?module=module_carte_electorale");
            } else {
                @header("Location: ../app/view_editions_generales.php");
            }
            return;
        }
        //
        if ($usecase === "view_editions_par_bureau"
            || $usecase === "revision_electorale"
            || $usecase === "direct") {
            //
            $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
            return;
        }
    }

    /**
     * VIEW - view_edition_pdf__liste_electorale.
     *
     * @return void
     */
    public function view_edition_pdf__liste_electorale() {
        $this->checkAccessibility();
        $liste = $_SESSION["liste"];
        $usecase = "fallback";
        $params = array();
        if (isset($_GET["mode_edition"])) {
            if ($_GET["mode_edition"] == "commune") {
                $params["mode_edition"] = "commune";
                if (isset($_GET["liste"])) {
                    $params["liste"] = $this->f->get_submitted_get_value("liste");
                    $liste = $this->f->get_submitted_get_value("liste");
                }
                if (isset($_GET["stockage"])
                    && $_GET["stockage"] === "true") {
                    //
                    if (isset($_GET["origin"])
                        && $_GET["origin"] === "scrutin"
                        && isset($_GET["scrutin"])) {
                        //
                        $usecase = "scrutin";
                        $scrutin = intval($this->f->get_submitted_get_value("scrutin"));
                        $params["scrutin"] = $scrutin;
                        $inst__scrutin = $this->f->get_inst__om_dbform(array(
                            "obj" => "reu_scrutin",
                            "idx" => intval($scrutin),
                        ));
                        $arrets_liste = $inst__scrutin->get_arrets_liste();
                        $params["scrutin_demande_id"] = $arrets_liste->getVal("livrable_demande_id");
                        $params["scrutin_livrable_date"] = $arrets_liste->getVal("livrable_demande_date");
                        $params["scrutin_libelle"] = $inst__scrutin->getVal("libelle");
                        $params["scrutin_tour1"] = $inst__scrutin->getVal("date_tour1");
                        $params["scrutin_tour2"] = $inst__scrutin->getVal("date_tour2");
                    } else if (isset($_GET["origin"])
                        && $_GET["origin"] === "demande"
                        && isset($_GET["demande_id"])) {
                        //
                        $usecase = "demande";
                        $demande = intval($this->f->get_submitted_get_value("demande_id"));
                        $params["demande"] = $demande;
                        $params["demande_livrable_date"] = intval($this->f->get_submitted_get_value("date_demande"));
                        $contexte = $this->f->get_submitted_get_value("contexte");
                        $obj_id = $this->f->get_submitted_get_value("obj_id");
                    } else {
                        $usecase = "view_editions_generales";
                    }
                } else {
                    $usecase = "fallback";
                }
            } elseif ($_GET["mode_edition"] == "parbureau") {
                $params["mode_edition"] = "parbureau";
                if (isset($_GET["bureau_code"])) {
                    $params["bureau_code"] = $_GET["bureau_code"];
                    $usecase = "view_editions_par_bureau";
                } elseif (isset($_POST["bureau"]) && isset($_POST["liste"])) {
                    $params["bureau_code"] = $_POST["bureau"];
                    $params["liste"] = $_POST["liste"];
                    $scrutin = intval($this->f->get_submitted_get_value("scrutin"));
                    $params["scrutin"] = $scrutin;
                    $inst__scrutin = $this->f->get_inst__om_dbform(array(
                        "obj" => "reu_scrutin",
                        "idx" => intval($scrutin),
                    ));
                    $params["scrutin_demande_id"] = $inst__scrutin->getVal("emarge_livrable_demande_id");
                    $params["scrutin_livrable_date"] = $inst__scrutin->getVal("emarge_livrable_date");
                    $params["scrutin_libelle"] = $inst__scrutin->getVal("libelle");
                    $params["scrutin_tour1"] = $inst__scrutin->getVal("date_tour1");
                    $params["scrutin_tour2"] = $inst__scrutin->getVal("date_tour2");
                    $usecase = "view_editions_par_bureau";
                } else {
                    $usecase = "fallback";
                }
            } else {
                $usecase = "fallback";
            }
        }
        if ($this->f->get_submitted_get_value("scrutin") !== null) {

        }
        //
        if ($usecase == "fallback") {
            $this->f->goToDashboard();
            return;
        }
        //
        require_once "../obj/edition_pdf__liste_electorale.class.php";
        $inst_edition_pdf = new edition_pdf__liste_electorale();
        $pdf_edition = $inst_edition_pdf->compute_pdf__liste_electorale($params);
        //
        if ($usecase === "view_editions_generales"
            || $usecase === "scrutin"
            || $usecase === "demande") {
            //
            if ($usecase === "scrutin") {
                $filename = sprintf(
                    'listeelectorale-%s-liste%s-scrutin%s.pdf',
                    $_SESSION["collectivite"],
                    $liste,
                    $scrutin
                );
                $redirect = sprintf(
                    '../app/index.php?module=form&obj=reu_scrutin&idx=%s&action=3',
                    $scrutin
                );
            } elseif ($usecase === "view_editions_generales") {
                $filename = sprintf(
                    'listeelectorale-%s-%s.pdf',
                    $_SESSION["collectivite"],
                    $liste
                );
                $redirect = sprintf(
                    '../app/view_editions_generales.php'
                );
            } elseif ($usecase === "demande") {
                $filename = sprintf(
                    'listeelectorale-%s-liste%s-demande%s.pdf',
                    $_SESSION["collectivite"],
                    $liste,
                    $demande
                );
                // Si on connais le contexte de la génération de l'édition on redirige
                // vers l'objet voulu. Sinon on redirige vers la vue générale des éditions.
                $redirect = sprintf(
                    '../app/view_editions_generales.php'
                );
                if (! empty($contexte) && ! empty($obj_id)) {
                    $redirect = sprintf(
                        '../app/index.php?module=form&obj=%s&idx=%s&action=3',
                        $contexte,
                        intval($obj_id)
                    );
                }
            }
            // Composition des métadonnées du fichier
            $filemetadata = array(
                "filename" => $filename,
                "size" => strlen($pdf_edition["pdf_output"]),
                "mimetype" => "application/pdf",
            );
            // Écriture du fichier
            $uid = $this->f->store_file(
                $pdf_edition["pdf_output"],
                $filemetadata,
                "pdf"
            );
            if ($uid === false) {
                $this->f->addToMessage(
                    "error",
                    __("Erreur lors du stockage/enregistrement du fichier.")
                );
                $this->f->setFlag(null);
                $this->f->display();
                return;
            }
            @header("Location: ".$redirect);
            return;
        }
        //
        if ($usecase === "view_editions_par_bureau") {
            //
            $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
            return;
        }
    }

    /**
     * VIEW - view_edition_pdf__liste_emargement.
     *
     * @return void
     */
    public function view_edition_pdf__liste_emargement() {
        $this->checkAccessibility();
        $usecase = "fallback";
        $params = array();
        if (isset($_GET["mode_edition"])) {
            if ($_GET["mode_edition"] == "parbureau") {
                $params["mode_edition"] = "parbureau";
                if (isset($_GET["bureau_code"])) {
                    $params["bureau_code"] = $_GET["bureau_code"];
                    $usecase = "view_editions_par_bureau";
                } elseif (isset($_POST["bureau"]) && isset($_POST["liste"])) {
                    $params["bureau_code"] = $_POST["bureau"];
                    $params["liste"] = $_POST["liste"];
                    $params["tour"] = "les_deux";
                    $params["bloc_arrete_nb_emargements"] = false;
                    if (isset($_POST["tour"])) {
                        $params["tour"] = $_POST["tour"];
                    }
                    if (isset($_POST["bloc_arrete_nb_emargements"])) {
                        $params["bloc_arrete_nb_emargements"] = true;
                    }
                    $scrutin = intval($this->f->get_submitted_get_value("scrutin"));
                    $params["scrutin"] = $scrutin;
                    if ($this->f->get_submitted_get_value("scrutin") !== null) {
                        $inst__scrutin = $this->f->get_inst__om_dbform(array(
                            "obj" => "reu_scrutin",
                            "idx" => intval($scrutin),
                        ));
                        $params["scrutin_demande_id"] = $inst__scrutin->getVal("emarge_livrable_demande_id");
                        $params["scrutin_livrable_date"] = $inst__scrutin->getVal("emarge_livrable_date");
                        $params["scrutin_libelle"] = $inst__scrutin->getVal("libelle");
                        $params["scrutin_tour1"] = $inst__scrutin->getVal("date_tour1");
                        $params["scrutin_tour2"] = $inst__scrutin->getVal("date_tour2");
                    }
                    $usecase = "view_editions_par_bureau";
                } else {
                    $usecase = "fallback";
                }
            } else {
                $usecase = "fallback";
            }
        }
        //
        if ($usecase == "fallback") {
            $this->f->goToDashboard();
            return;
        }
        //
        require_once "../obj/edition_pdf__liste_emargement.class.php";
        $inst_edition_pdf = new edition_pdf__liste_emargement();
        $pdf_edition = $inst_edition_pdf->compute_pdf__liste_emargement($params);
        //
        if ($usecase === "view_editions_par_bureau") {
            //
            $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
            return;
        }
    }

    /**
     * VIEW - view_edition_pdf__etiquette_electorale.
     *
     * @return void
     */
    public function view_edition_pdf__etiquette_electorale() {
        $this->checkAccessibility();
        $usecase = "fallback";
        $params = array(
            "obj" => "electeur",
        );
        if (isset($_GET["mode_edition"])) {
            if ($_GET["mode_edition"] == "commune") {
                $params["mode_edition"] = "commune";
                if (isset($_GET["stockage"])
                    && $_GET["stockage"] === "true") {
                    //
                    $usecase = "view_editions_generales";
                    $params["filename_with_date"] = false;
                } else {
                    $usecase = "fallback";
                }
            } elseif ($_GET["mode_edition"] == "parbureau") {
                $params["mode_edition"] = "parbureau";
                if (isset($_GET["bureau_code"])) {
                    $params["bureau_code"] = $_GET["bureau_code"];
                    $usecase = "view_editions_par_bureau";
                } else {
                    $usecase = "fallback";
                }
            } else {
                $usecase = "fallback";
            }
        }
        //
        if ($usecase == "fallback") {
            $this->f->goToDashboard();
            return;
        }
        //
        require_once "../obj/edition_pdf__etiquette.class.php";
        $inst_edition_pdf = new edition_pdf__etiquette();
        $pdf_edition = $inst_edition_pdf->compute_pdf__etiquette($params);
        //
        if ($usecase === "view_editions_generales") {
            // Composition des métadonnées du fichier
            $filemetadata = array(
                "filename" => $pdf_edition["filename"],
                "size" => strlen($pdf_edition["pdf_output"]),
                "mimetype" => "application/pdf",
            );
            // Écriture du fichier
            $uid = $this->f->store_file(
                $pdf_edition["pdf_output"],
                $filemetadata,
                "pdf"
            );
            if ($uid === false) {
                $this->f->addToMessage(
                    "error",
                    __("Erreur lors du stockage/enregistrement du fichier.")
                );
                $this->f->setFlag(null);
                $this->f->display();
                return;
            }
            @header("Location: ../app/view_editions_generales.php");
            return;
        }
        if ($usecase === "view_editions_par_bureau") {
            //
            $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
            return;
        }
    }

    /**
     *
     */
    function get_infos() {
        //
        if (intval($this->getVal($this->clePrimaire)) === 0) {
            return array();
        }
        //
        $infos = array(
            "civilite" => $this->getVal("civilite"),
            "nom" => $this->getVal("nom"),
            "prenom" => $this->getVal("prenom"),
            "nom_usage" => $this->getVal("nom_usage"),
            "date_naissance" => $this->getVal("date_naissance"),
            "libelle_lieu_de_naissance" => $this->getVal("libelle_lieu_de_naissance"),
            "libelle_departement_naissance" => $this->getVal("libelle_departement_naissance"),
            "code_departement_naissance" => $this->getVal("code_departement_naissance"),
            "sexe" => $this->getVal("sexe"),
            "code_nationalite" => $this->getVal("code_nationalite"),
        );
        return $infos;
    }

    /**
     *
     */
    function push_infos_to_reu($val = array()) {
        $this->begin_treatment(__METHOD__);
        $inst_bureau = $this->f->get_inst__om_dbform(array(
            "obj" => "bureau",
            "idx" => $this->getVal('bureau'),
        ));
        if ($inst_bureau->getVal("referentiel_id") == "") {
            $this->addToMessage("Erreur de paramétrage des bureaux de vote. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        $inst_voie = $this->f->get_inst__om_dbform(array(
            "obj" => "voie",
            "idx" => $this->getVal('code_voie'),
        ));
        //
        $datas = array(
            //
            "libelle_commune" => $this->f->getParameter("ville"),
            //
            "nom_usage" => $this->getVal("nom_usage"),
            "nationalite" => trim($this->getVal("code_nationalite")),
            //
            "bureau" => $inst_bureau->getVal("referentiel_id"),
            //
            "adresse_rattachement" => array(
                "numero" => $this->getVal("numero_habitation"),
                "complement_numero" => $this->getVal("complement_numero"),
                "voie" => $inst_voie->getVal("libelle_voie"),
                "complement" => $this->getVal("complement"),
                "code_postal" => $inst_voie->getVal("cp"),
                "ville" => $inst_voie->getVal("ville"),
            ),
            //
            "courriel" => $this->getVal("courriel"),
            "telephone" => $this->getVal("telephone"),
        );
        if ($this->getVal("resident") == "Oui") {
            $datas["adresse_contact"] = array(
                "adresse" => $this->getVal("adresse_resident"),
                "complement" => $this->getVal("complement_resident"),
                "code_postal" => $this->getVal("cp_resident"),
                "ville" => $this->getVal("ville_resident"),
            );
        }
        $inst_reu = $this->f->get_inst__reu();
        $ret = $inst_reu->handle_electeur(array(
            "mode" => "edit",
            "ine" => $this->getVal("ine"),
            "datas" => $datas,
        ));
        if (isset($ret["code"]) !== true
            || $ret["code"] != 200) {
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Les informations de l'électeur ont correctement transmises au REU."));
        $inst_mv = $this->f->get_inst__om_dbform(array(
            "obj" => "mouvement",
            "idx" => 0,
        ));
        $electeur_ids = $inst_mv->get_electeurs_with_same_ine($this->getVal('ine'));
        foreach ($electeur_ids as $electeur_id) {
            $inst_elect = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => $electeur_id,
            ));
            // Le bureau entre les électeurs doit être identique dans openElec
            // On gère ici le cas des listes complémentaires
            // On souhaite mettre à jour le numéro d'ordre des deux électeurs OEL
            // suite au changement de bureau de l'électeur REU
            if ($this->getVal("bureau") != $inst_elect->getVal("bureau")) {
                continue;
            }
            // Met à jour le numéro d'ordre de l'électeur
            $update_num_bur = $inst_elect->update_numero_bureau_oel_from_reu();
            if ($update_num_bur !== true) {
                $this->addToMessage(
                    __("Erreur lors de la mise à jour du numéro d'ordre de l'électeur dans openElec.")
                );
                return $this->end_treatment(__METHOD__, false);
            }
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Met à jour le numéro d'ordre de l'électeur en récupérant celui du REU.
     *
     * @return boolean
     */
    function update_numero_bureau_oel_from_reu() {
        $num_bur_reu = $this->get_numero_bureau_from_reu();
        if ($num_bur_reu === false) {
            return false;
        }
        //
        $valF = array(
            'numero_bureau' => $num_bur_reu,
        );
        if ($valF['numero_bureau'] !== null) {
            $ret = $this->update_autoexecute($valF, null, false);
            if ($ret !== true) {
                return false;
            }
        }
        //
        return true;
    }

    function get_infos_electeur_reu_by_handle_electeur() {
        $inst_reu = $this->f->get_inst__reu();
        $reu_datas = $inst_reu->handle_electeur(array(
            "mode" => "get",
            "ine" => intval($this->getVal("ine")),
        ));
        if ($reu_datas["code"] != "200") {
            return false;
        }
        return $reu_datas;
    }

    /**
     * Recupère le numéro d'ordre de l'électeur depuis le REU.
     *
     * @return string ou null
     */
    function get_numero_bureau_from_reu() {
        $reu_datas = $this->get_infos_electeur_reu_by_handle_electeur();
        if ($reu_datas === false) {
            return false;
        }

        //
        $params = array();
        $rattach = 'rattachementsActifs';

        // Vérifie s'il s'agit d'un électeur n'ayant pas encore la majorité
        // Les électeurs n'ayant pas encore la majorité possède seulement un
        // rattachement inactif mais sans date de radiation et dont la date
        // d'inscription est dans le futur (+1 jour au jour de naissance)
        $num_list = array();
        if (gavoe($reu_datas['result'], array('rattachementsActifs', ), null) === null
            && gavoe($reu_datas['result'], array('rattachementsInactifs', ), null) !== null) {
            //
            foreach (gavoe($reu_datas['result'], array('rattachementsInactifs', ), null)  as $key => $value) {
                //
                if (gavoe($value, array('dateRadiation', ), null) === null) {
                    $num_list[] = $key;
                }
            }
            if (isset($num_list[2]) === true) {
                // L'électeur ne peut pas avoir plus de deux listes affectées
                return false;
            }
            $rattach = 'rattachementsInactifs';
            $params['rattach'] = 'rattachementsInactifs';
            $params['num_first_list'] = strval($num_list[0]);
            $params['num_second_list'] = strval((isset($num_list[1]) === true ? $num_list[1] : '999'));
        }

        // Identifie de quelle liste le numéro d'ordre doit être récupéré
        $list_rattach = $this->choose_list_from_reu($reu_datas, $params);

        //
        return gavoe($reu_datas['result'], array($rattach, $list_rattach, 'numeroOrdre', ), null);
    }

    function choose_list_from_reu($reu_datas, $params = array()) {
        //
        $rattach = 'rattachementsActifs';
        $num_first_list = '0';
        $num_second_list = '1';
        if (count($params) > 0) {
            $rattach = $params['rattach'];
            $num_first_list = $params['num_first_list'];
            $num_second_list = $params['num_second_list'];
        }

        //
        $list_rattach = $num_first_list;
        // Récupère la seconde liste de l'électeur depuis le REU
        $second_list_reu = gavoe($reu_datas["result"], array($rattach, $num_second_list, "typeListe", "code", ), null);
        if ($second_list_reu !== null) {
            // Récupère le code INSEE de la liste de l'électeur dans openElec
            $liste_oel = $this->f->get_one_result_from_db_query(sprintf(
                'SELECT liste_insee FROM %1$sliste WHERE liste=\'%2$s\'',
                DB_PREFIXE,
                $this->getVal('liste')
            ), true);
            if ($liste_oel['code'] !== 'OK') {
                return false;
            }
            //
            if (strtolower($liste_oel['result']) === strtolower($second_list_reu)) {
                $list_rattach = $num_second_list;
            }
        }
        //
        return $list_rattach;
    }

    /**
     * Gestion des modifications d'état civil.
     *
     * - Création d'un mouvement de modification de l'électeur avec motif
     *   "MODEC" sans tenir compte d'un éventuel verrou seul l'état civil
     *   est modifié.
     * - Possibilité de modifier l'électeur sans ajouter de mouvement "MODEC".
     */
    function handle_modec($val = array(), $with_mv = true) {
        $this->begin_treatment(__METHOD__);
        //
        if (array_key_exists("reu_datas", $val) !== true) {
            $inst_reu = $this->f->get_inst__reu();
            $reu_datas = $inst_reu->handle_electeur(array(
                "mode" => "get",
                "ine" => intval($this->getVal("ine")),
            ));
            if ($reu_datas["code"] != "200") {
                $this->addToMessage(
                    __("Impossible de récupérer l'électeur depuis le REU.")
                );
                return false;
            }
            $oel_birth_place = $this->f->handle_birth_place(array(
                "code_commune_de_naissance" => gavoe($reu_datas['result'], array('etatCivil', 'communeNaissance', 'code', )),
                "libelle_commune_de_naissance" => gavoe($reu_datas['result'], array('etatCivil', 'communeNaissance', 'libelle', )),
                "code_pays_de_naissance" => gavoe($reu_datas['result'], array('etatCivil', 'paysNaissance', 'code', )),
                "libelle_pays_de_naissance" => gavoe($reu_datas['result'], array('etatCivil', 'paysNaissance', 'libelle', )),
            ));
            $fresh_infos = array(
                "nom" => gavoe($reu_datas['result'], array('etatCivil', 'nomNaissance', )),
                "prenom" => gavoe($reu_datas['result'], array('etatCivil', 'prenoms', )),
                "sexe" => gavoe($reu_datas['result'], array('etatCivil', 'sexe', 'code', )),
                "date_naissance" => $this->f->handle_birth_date(gavoe($reu_datas['result'], array('etatCivil', 'dateNaissance', ))),
                "code_departement_naissance" => $oel_birth_place["code_departement_naissance"],
                "libelle_departement_naissance" => $oel_birth_place["libelle_departement_naissance"],
                "code_lieu_de_naissance" => $oel_birth_place["code_lieu_de_naissance"],
                "libelle_lieu_de_naissance" => $oel_birth_place["libelle_lieu_de_naissance"],
            );
        } else {
            $oel_birth_place = $this->f->handle_birth_place(array(
                "code_commune_de_naissance" => gavoe($val['reu_datas'], array('code_commune_de_naissance', )),
                "libelle_commune_de_naissance" => gavoe($val['reu_datas'], array('libelle_commune_de_naissance', )),
                "code_pays_de_naissance" => gavoe($val['reu_datas'], array('code_pays_de_naissance', )),
                "libelle_pays_de_naissance" => gavoe($val['reu_datas'], array('libelle_pays_de_naissance', )),
            ));
            $fresh_infos = array(
                "nom" => gavoe($val['reu_datas'], array('nom', )),
                "prenom" => gavoe($val['reu_datas'], array('prenoms', )),
                "sexe" => gavoe($val['reu_datas'], array('sexe', )),
                "date_naissance" => $this->f->handle_birth_date(gavoe($val['reu_datas'], array('date_de_naissance', ))),
                "code_departement_naissance" => $oel_birth_place["code_departement_naissance"],
                "libelle_departement_naissance" => $oel_birth_place["libelle_departement_naissance"],
                "code_lieu_de_naissance" => $oel_birth_place["code_lieu_de_naissance"],
                "libelle_lieu_de_naissance" => $oel_birth_place["libelle_lieu_de_naissance"],
            );
        }
        if ($fresh_infos["prenom"] == "") {
            $fresh_infos["prenom"] = "-";
        }
        if ($with_mv === true) {
            //
            $inst_modification = $this->f->get_inst__om_dbform(array(
                "obj" => "modification",
                "idx" => "]",
            ));
            $inst_modification->setParameter("maj", 0);
            $inst_modification->electeur["bureau"] = $this->getVal("bureau");
            $inst_modification->electeur["numero_bureau"] = $this->getVal("numero_bureau");
            if (array_key_exists("statut", $val) === true) {
                $statut = $val["statut"];
            } else {
                $statut = "accepte";
            }
            $ret = $inst_modification->ajouter(array(
                //
                "origin" => "modec",
                //
                "id" => null,
                //
                "ine" => $this->getVal("ine"),
                //
                "nom" => $fresh_infos["nom"],
                "prenom" => $fresh_infos["prenom"],
                "sexe" => $fresh_infos["sexe"],
                "date_naissance" => $fresh_infos["date_naissance"],
                "code_departement_naissance" => $fresh_infos["code_departement_naissance"],
                "libelle_departement_naissance" => $fresh_infos["libelle_departement_naissance"],
                "code_lieu_de_naissance" => $fresh_infos["code_lieu_de_naissance"],
                "libelle_lieu_de_naissance" => $fresh_infos["libelle_lieu_de_naissance"],
                //
                "nom_usage" => $this->getVal("nom_usage"),
                "code_nationalite" => $this->getVal("code_nationalite"),
                //
                "liste" => $this->getVal("liste"),
                "bureau" => $this->getVal("bureau"),
                "bureauforce" => $this->getVal("bureauforce"),
                "ancien_bureau" => $this->getVal("bureau"),
                "numero_bureau" => $this->getVal("numero_bureau"),
                //
                "numero_habitation" => $this->getVal("numero_habitation"),
                "complement_numero" => $this->getVal("complement_numero"),
                "code_voie" => $this->getVal("code_voie"),
                "libelle_voie" => $this->getVal("libelle_voie"),
                "complement" => $this->getVal("complement"),
                //
                "resident" => $this->getVal("resident"),
                "adresse_resident" => $this->getVal("adresse_resident"),
                "complement_resident" => $this->getVal("complement_resident"),
                "cp_resident" => $this->getVal("cp_resident"),
                "ville_resident" => $this->getVal("ville_resident"),
                "courriel" => $this->getVal("courriel"),
                "telephone" => $this->getVal("telephone"),
                //
                "electeur_id" => $this->getVal($this->clePrimaire),
                "date_demande" => date("Y-m-d"),
                "date_tableau" => $this->f->getParameter("datetableau"),
                "date_j5" => date("Y-m-d"),
                "types" => "MODEC",
                "statut" => $statut,
                "etat" => "trs",
                //
                "observation" => gavoe($val, array("observation", )),
            ));
            if ($ret !== true) {
                $this->correct = false;
                $this->addToMessage($inst_modification->msg);
                return $this->end_treatment(__METHOD__, false);
            }
        }
        //
        $ret = $this->update_autoexecute($fresh_infos, null, false);
        if ($ret !== true) {
            $this->correct = false;
            $this->addToMessage(__("Une erreur est survenue. Contactez votre administrateur."));
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage("Modification d'état civil correctement appliquée sur l'électeur.");
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Affiche le cadre contenant le récapitulatif des infos
     * concernant la carte en retour.
     *
     * @param string date de saisie de la carte
     * @return void
     */
    protected function display_carte_retour($dateSaisie = '') {
        // Modifie la date pour l'avoir au format XX/XX/XXXX. Si le formatage
        // échoue la date ne sera pas affichée
        $affDateSaisie  = '&nbsp;';
        $dateFormate = $this->f->formatDate($dateSaisie, true);
        if ($dateFormate !== false) {
            $affDateSaisie = sprintf(
                'date de saisie : %s',
                 $dateFormate
            );
        }

        printf(
            '<div class="message ui-widget-content ui-corner-all carteretour">
                <div class="carteretour-32">
                    <p class="titre">
                        %s
                    </p>
                    <p class="titre date_saisie_carte_retour">
                        %s
                    </p>
                </div>
            </div>',
            __("Une carte en retour est enregistree pour cet electeur."),
            $affDateSaisie
        );
    }
}
