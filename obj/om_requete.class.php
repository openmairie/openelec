<?php
//$Id$ 
//gen openMairie le 20/06/2014 18:45

require_once "../core/obj/om_requete.class.php";

class om_requete extends om_requete_core {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[1]["condition"] = "is_not_from_configuration";
        $this->class_actions[2]["condition"] = "is_not_from_configuration";
    }


    function is_from_configuration() {
        if ($this->getVal($this->clePrimaire) < 100000) {
            return true;
        }
        return false;
    }

    function is_not_from_configuration() {
        return !$this->is_from_configuration();
    }
}
?>
