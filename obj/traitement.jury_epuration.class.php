<?php
/**
 * Ce script définit la classe 'juryEpurationTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'juryEpurationTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class juryEpurationTraitement extends traitement {

    var $fichier = "jury_epuration";

    function getDescription() {
        //
        return __("Tous les electeurs etant marques comme jures d'assises ne le".
                 "seront plus. Ce traitement se fait uniquement sur la liste ".
                 "en cours.");
    }

    function getValidButtonValue() {
        //
        return __("Epuration des jures d'assises");
    }

    function displayBeforeContentForm() {
        // Récupération des requêtes permettant de compter les jurés
        include "../sql/".OM_DB_PHPTYPE."/trt_jury_epuration.inc.php";
        // Récupération du nombre de jurés titulaire
        $nb_jury = $this->f->db->getone($query_count_jury);
        $this->f->isDatabaseError($nb_jury);
        // Récupération du nombre de jurés suppléants
        $message_suppleant = '';
        if ($this->f->is_option_enabled('option_module_jures_suppleants') === true) {
            $nb_jury_sup = $this->f->db->getone($query_count_jury_sup);
            $this->f->isDatabaseError($nb_jury);
            $message_suppleant = sprintf(
                'titulaires et de %d suppléants',
                $nb_jury_sup
            );
        }

        printf(
            '<p>%s %s est de %d %s</p>',
            __("Le nombre d'électeurs etant marques comme jures d'assise a la date du"),
            date('d/m/Y'),
            $nb_jury,
            $message_suppleant
        );
    }

    function treatment () {
        //
        $this->LogToFile("start jury_epuration");
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_jury_epuration.inc.php";
        //
        $res = $this->f->db->query($query_update_jury);
        //
        if ($this->f->isDatabaseError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(__("Contactez votre administrateur."));
        } else {
            //
            $message = $this->f->db->affectedRows()." ".__("jure(s) de-selectionne(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end jury_epuration");
    }
}


