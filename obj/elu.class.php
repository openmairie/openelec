<?php
/**
 * Ce script contient la définition de la classe *elu*.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/elu.class.php";

/**
 * Définition de la classe *elu* (om_dbform).
 */
class elu extends elu_gen {

    /**
     * SETTER_FORM - setOnchange.
     *
     * @return void
     */
    public function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        $form->setOnchange("nom", "this.value=this.value.toUpperCase()");
        $form->setOnchange("ville", "this.value=this.value.toUpperCase()");
    }
}
