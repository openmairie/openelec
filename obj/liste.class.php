<?php
/**
 * Ce script définit la classe 'liste'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/liste.class.php";

/**
 * Définition de la classe 'liste' (om_dbform).
 */
class liste extends liste_gen {

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("liste", __("Code"));
        $form->setLib("libelle_liste", __("Libelle"));
        $form->setLib("liste_insee", __("Référence INSEE"));
    }

    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("liste", "this.value=this.value.toUpperCase()");
        $form->setOnchange('libelle_liste', "this.value=this.value.toUpperCase()");
    }

}


