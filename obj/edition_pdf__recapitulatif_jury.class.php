<?php
/**
 * Ce script définit la classe 'edition_pdf__recapitulatif_jury'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__recapitulatif_jury' (edition_pdf).
 */
class edition_pdf__recapitulatif_jury extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * @return array
     */
    public function compute_pdf__recapitulatif_jury($params = array()) {
        /**
         *
         */
        //
        require_once "../obj/fpdf_fromdb_fromarray.class.php";
        //
        $pdf = new PDF("L", "mm", "A4");
        //
        $pdf->generic_document_header_left = $this->f->collectivite["ville"];
        //
        $generic_document_title = "";
        $pdf->generic_document_title = $generic_document_title;
        //
        $pdf->SetMargins(10, 5, 10);
        //
        $pdf->AliasNbPages();
        //
        $query_nb_jures_canton = sprintf(
            'SELECT
                canton.id as canton_id,
                canton.code as canton_code,
                canton.libelle as canton_libelle,
                parametrage_nb_jures.%3$s
            FROM
                %1$sparametrage_nb_jures
                LEFT JOIN %1$scanton ON parametrage_nb_jures.canton = canton.id
            WHERE
                parametrage_nb_jures.om_collectivite = %2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            ! empty($params['jury']) && $params['jury'] === 2 ?
                'nb_suppleants' :
                'nb_jures'
        );
        //
        $res = $this->f->db->query($query_nb_jures_canton);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$query_nb_jures_canton."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            //
            $params["canton"] = $row["canton_id"];
            $conf = $this->get_vars_from_config_file_pdffromdb(
                "jury_liste_preparatoire",
                $params
            );
            $pdf->add_edition_pdffromdb($conf);
            $pdf->reinit_header_pdffromdb();
        }

        /**
         * OUTPUT
         */
        //
        $filename = date("Ymd-His");
        $filename .= "-jury";
        $filename .= "-liste-preparatoire";
        $filename .= "-".$_SESSION["collectivite"];
        $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
        $filename .= ".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
