<?php
/**
 * Ce script définit la classe 'reuSyncLEInitTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_l_e.class.php";

/**
 * Définition de la classe 'reuSyncLEInitTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLEInitTraitement extends reuSyncLETraitement {

    var $fichier = "reu_sync_l_e_init";
    var $form_class = "hide_button_on_success";

    function getValidButtonValue() {
        //
        return __("Initialiser la synchronisation");
    }

    function is_done() {
        if ($this->get_nb_electeurs_reu_total() > 0) {
            return true;
        }
        return false;
    }

    function displayBeforeContentForm() {
        printf(
            '
            <div class="container-fluid" id="reu-sync-tab">
                <div class="row">
                    <div class="col-sm-3 ">
                    </div>
                    <div class="col-sm-3 ">
                        <div class="card text-center text-white bg-info">
                            <div class="card-header">
                                Électeurs openElec total
                            </div>
                            <div class="card-body">
                                <div id="nb_electeur_oel" class="card-title">%s</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-center text-white bg-success">
                            <div class="card-header">
                                Électeurs REU total
                            </div>
                            <div class="card-body">
                                <div id="nb_electeur_reu" class="card-title">%s</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 ">
                </div>
            </div>
            <div class="visualClear"></div>
            <h2 class="text-center">%s</h2>
            ',
            $this->get_nb_electeurs_oel_total(),
            $this->get_nb_electeurs_reu_total(),
            $this->f->getParameter("reu_sync_l_e_init")
        );
    }

    /**
     *
     */
    function treatment () {
        //
        $this->LogToFile("start reu_sync_l_e_init");
        //
        $inst_reu = $this->f->get_inst__reu();
        //
        $ret = $this->reload_reu_sync_listes();
        if ($ret !== true) {
            return;
        }
        /**
         * Mise à jour du flag - L'initialisation a été faite, on stocke la date d'extraction
         */
        $query = sprintf(
            'SELECT date_extraction FROM %1$sreu_sync_listes WHERE code_ugle=\'%2$s\' LIMIT 1',
            DB_PREFIXE,
            $inst_reu->get_ugle()
        );
        $date_extraction = $this->f->db->getone($query);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($date_extraction);
        $ret = $this->f->insert_or_update_parameter("reu_sync_l_e_init", $date_extraction);
        if ($ret !== true) {
            $this->error = true;
            $this->LogToFile("error lors de la création/mise à jour du flag");
            $this->LogToFile("end reu_sync_l_e_init");
            return;
        }
        /**
         *
         */
        $this->addToMessage(sprintf(
            __("Cliquez ici pour accéder à l'étape suivante : %s"),
            '<a onclick="reload_my_tab(\'#traitement-tabs\');return false;" href="#">ETAPE SUIVANTE</a>'
        ));
        //
        $this->LogToFile("end reu_sync_l_e_init");
    }
}
