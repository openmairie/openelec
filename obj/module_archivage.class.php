<?php
/**
 * Ce script définit la classe 'module_archivage'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_archivage' (module).
 */
class module_archivage extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_archivage",
            "title" => __("Traitement")." -> ".__("Module Archivage"),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_archivage-traitement_archivage",
                    "href" => "../app/index.php?module=module_archivage&view=archivage",
                    "title" => __("Archivage"),
                    "view" => "archivage",
                ),
                array(
                    "type" => "edition_pdf",
                    "right" => "module_archivage-edition_pdf__statistiques_mouvements_pour_archivage",
                    "view" => "edition_pdf__statistiques_mouvements_pour_archivage",
                ),
                array(
                    "type" => "edition_pdf",
                    "right" => "module_archivage-edition_pdf__listing_mouvements_a_archiver",
                    "view" => "edition_pdf__listing_mouvements_a_archiver",
                ),
            ),
        );
    }

    /**
     *
     */
    function view__archivage() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_archivage");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        //
        $description = __("L'archivage permet de supprimer les mouvements des ecrans ".
                         "de consultation. Ils sont deplaces vers une table archive ".
                         "qu'il est possible de consulter par le menu \"Consultation ".
                         "-> Archive\". Ce traitement supprime les mouvements de la ".
                         "date de tableau en cours les archive.");
        $this->f->displayDescription($description);

        /**
         *
         */
        //
        $datetableau = $this->f->getParameter("datetableau");
        $datetableau_show = $this->f->formatDate($this->f->getParameter("datetableau"));
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_archivage.inc.php";
        //
        $nbCnen = $this->f->db->getone($query_count_mouvement);
        $this->f->addToLog(
            "app/archivage.php: db->getone(\"".$query_count_mouvement."\")",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($nbCnen);
        // Si il y a des mouvements non transmis a l'insee ou si la date du jour est
        // inferieure a la date de tableau
        if ($nbCnen > 0 || time () < strtotime ($datetableau)) {
            //
            if ($nbCnen > 0) {
                $this->f->displayMessage("error", $nbCnen." ".__("mouvement(s) a archiver n'ont pas ete transmis a l'INSEE"));
            }
            //
            if (time () < strtotime ($datetableau)) {
                $this->f->displayMessage("error", __("La commission du tableau du")." ".$datetableau_show." ".__("n'est pas effectuee"));
            }
        }

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Editions des statistiques et des listings de mouvements");
        $this->f->displaySubTitle($subtitle);
        //
        $links = array(
            "0" => array(
                "href" => "../app/index.php?module=module_archivage&view=edition_pdf__statistiques_mouvements_pour_archivage",
                "target" => "_blank",
                "class" => "om-prev-icon statistiques-16",
                "title" => __("Statistiques des mouvements"),
                "id" => "archivage-edition-statistiques",
            ),
            "1" => array(
                "href" => "../app/index.php?module=module_archivage&view=edition_pdf__listing_mouvements_a_archiver",
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => __("Listing des mouvements a archiver a la date de tableau en cours"),
                "id" => "archivage-edition-listing",
            ),
        );
        //
        $this->f->displayLinksAsList($links);
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";

        /**
         * Affichage du formulaire de lancement du traitement
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ". __("Archivage des mouvements");
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.archivage.class.php";
        $trt = new archivageTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }

    /**
     * VIEW - view__edition_pdf__statistiques_mouvements_pour_archivage.
     *
     * @return void
     */
    function view__edition_pdf__statistiques_mouvements_pour_archivage() {
        //
        require_once "../obj/traitement.archivage.class.php";
        $trt = new archivageTraitement();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $trt->compute_stats__mouvements_pour_archivage(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view__edition_pdf__listing_mouvements_a_archiver.
     *
     * @return void
     */
    function view__edition_pdf__listing_mouvements_a_archiver() {
        //
        $params = array(
            "obj" => "archivage",
        );
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromdb($params);
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }
}
