<?php
/**
 * Ce script définit la classe 'reuSyncAncienMouvementModificationTraitement'.
 *
 * @package openelec
 * @version SVN : $Id: traitement.reu_sync_ancien_mouvement_modification.class.php 1766 2019-02-26 14:55:01Z fmichon $
 */

require_once "../obj/traitement.reu_sync_mouvement.class.php";

/**
 * Définition de la classe 'reuSyncAncienMouvementModificationTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncAncienMouvementModificationTraitement extends reuSyncMouvementTraitement {
    /**
     *
     */
    var $fichier = "reu_sync_ancien_mouvement_modification";
    /**
     *
     */
    function displayBeforeContentForm() {
        echo $this->get_displayBeforeContentForm("modification");
    }
    /**
     *
     */
    function treatment() {
        $this->LogToFile("start reu_sync_ancien_mouvement_modification");
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->error = true;
            $this->addToMessage("Erreur de connexion REU.");
            $this->LogToFile("Erreur de connexion REU.");
            $this->LogToFile("end reu_sync_ancien_mouvement_modification");
            return;
        }
        if ($inst_reu->is_connexion_standard_valid() !== true) {
            $this->error = true;
            $this->addToMessage(sprintf(
                '%s : %s',
                __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                sprintf(
                    '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                    __("cliquez ici pour vérifier")
                )
            ));
            $this->LogToFile("La connexion standard n'est pas valide.");
            $this->LogToFile("end reu_sync_ancien_mouvement_modification");
            return;
        }
        /**
         *
         */
        $this->LogToFile("Etape1");
        $modifications = $this->get_list_anciens_modifications_actif("etape_1");
        $modifications_to_treat_step_1 = $modifications['result'];
        $nb_result = count($modifications['result']);
        $this->LogToFile(sprintf(
            __("Nombre de modifications à initialiser : %s"),
            $nb_result
        ));
        $nb_res_vis = 0;
        foreach ($modifications['result'] as $key => $value) {
            $this->startTransaction();
            $inst_modification = $this->f->get_inst__om_dbform(array(
                "obj" => "modification",
                "idx" => intval($value["id"]),
            ));
            if ($inst_modification->exists() !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC La modification %s n'existe pas",
                    intval($value["id"])
                ));
                $this->rollbackTransaction();
                continue;
            }
            $inst_electeur = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => $inst_modification->getVal("electeur_id"),
            ));
            if ($inst_electeur->exists() !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC L'électeur de la modification %s n'existe pas",
                    intval($value["id"])
                ));
                $this->rollbackTransaction();
                continue;
            }
            //
            $valF = array(
                "ine" => $inst_electeur->getVal("ine"),
                "statut" => "ouvert",
            );
            $ret = $inst_modification->update_autoexecute($valF);
            if ($ret !== true) {
                $this->rollbackTransaction();
                continue;
            }
            // Gestion de l'historique du mouvement
            $val_histo = array(
                'action' => "",
                'message' => __('Initialisation du mouvement.'),
                'datas' => $valF,
            );
            $histo = $inst_modification->handle_historique($val_histo);
            if ($histo !== true) {
                $this->LogToFile(sprintf(
                    "Erreur lors de la mise à jour de l'historique du mouvement %s : %s",
                    intval($value["id"]),
                    $inst_modification->msg
                ));
                $this->rollbackTransaction();
                continue;
            }
            $nb_res_vis++;
            $this->commitTransaction();
            $this->LogToFile(sprintf(
                "Mouvement %s - INE %s - Modification initialisée avec succès",
                intval($value["id"]),
                $inst_modification->getVal("ine")
            ));
        }
        //
        if ($nb_result != 0) {
            $message = sprintf(
                __('%1$s modification(s) initialisée(s) sur un total de %2$s.'),
                $nb_res_vis,
                $nb_result
            );
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        /**
         *
         */
        $this->LogToFile("Etape2");
        $modifications = $this->get_list_anciens_modifications_actif("etape_2");
        $modifications_to_treat_step_2 = $modifications['result'];
        $nb_result = count($modifications['result']);
        $this->LogToFile(sprintf(
            __("Nombre de modifications à valider : %s"),
            $nb_result
        ));
        $nb_res_vis = 0;
        foreach ($modifications['result'] as $key => $value) {
            $this->startTransaction();
            $inst_modification = $this->f->get_inst__om_dbform(array(
                "obj" => "modification",
                "idx" => intval($value["id"]),
            ));
            if ($inst_modification->exists() !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC La modification %s n'existe pas",
                    intval($value["id"])
                ));
                $this->rollbackTransaction();
                continue;
            }
            $ret = $inst_modification->valider();
            if ($ret !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC Validation de la modification %s échouée : %s",
                    intval($value["id"]),
                    $inst_modification->msg
                ));
                $this->rollbackTransaction();
                continue;
            }
            $nb_res_vis++;
            $this->commitTransaction();
            $this->LogToFile(sprintf(
                "Mouvement %s - INE %s - Modification appliquée au REU avec succès",
                intval($value["id"]),
                $inst_modification->getVal("ine")
            ));
        }
        //
        if ($nb_result != 0) {
            $message = sprintf(
                __('%1$s modification(s) validée(s) sur un total de %2$s.'),
                $nb_res_vis,
                $nb_result
            );
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        if (count($modifications_to_treat_step_1) == 0 && count($modifications_to_treat_step_2) == 0) {
            $message = __("Aucune modification à traiter.");
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        $anciens_mouvements_total_a_transmettre = $this->get_nb_anciens_mouvements(
            "a_transmettre",
            "modification"
        );
        if ($anciens_mouvements_total_a_transmettre["result"] != 0) {
            $this->error = true;
        }
        //
        $this->LogToFile("end reu_sync_ancien_mouvement_modification");
    }
}
