<?php
/**
 * Ce script définit la classe 'edition_pdf__facturation_commune_titre_de_recette'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__facturation_commune_titre_de_recette' (edition_pdf).
 */
class edition_pdf__facturation_commune_titre_de_recette extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * @return array
     */
    public function compute_pdf__facturation_commune_titre_de_recette($params = array()) {
        //
        require_once "fpdf.php";
        $aujourdhui = date("d/m/Y");

        $nolibliste = $_SESSION["libelle_liste"];


        //===========================================================================
        // communes
        $sqlcom1 = sprintf(
            'SELECT om_collectivite.om_collectivite as id, om_collectivite.libelle as ville, (SELECT count(*) FROM %1$selecteur WHERE electeur.om_collectivite=om_collectivite.om_collectivite) as total FROM %1$som_collectivite WHERE om_collectivite.niveau=\'1\' ORDER BY om_collectivite.libelle',
            DB_PREFIXE
        );
        //===========================================================================


        // Communes

        $commune = array();

        $sqlcom = $sqlcom1;

        $rescom =& $this->f->db->query($sqlcom);
        $this->f->isDatabaseError($rescom);

        while ($rowcom =& $rescom->fetchrow(DB_FETCHMODE_ASSOC))
            array_push ($commune, $rowcom);


        // Fonction Entete & Tableau
        function pdf_stat_entete($utils, $pdf) {
            //
            $parametrage_facturation = $utils->get_parametrage_facturation();
            //
            $pdf->addpage();
            // entete
            $pdf->SetFont('courier', '', 11);
            $pdf->MultiCell(80, 5, iconv(HTTPCHARSET, "CP1252", $utils->getParameter('ville')), 0, 'C', 0);
            $pdf->ln();
            $pdf->Cell(100, 5, iconv(HTTPCHARSET, "CP1252", __('SERVICE GESTIONNAIRE :').$parametrage_facturation["fact_gestionnaire"]), 0, 0, 'L', 0);
            $pdf->SetFont('courier', 'B', 11);
            $pdf->Cell(100, 5, iconv(HTTPCHARSET, "CP1252", __('DEMANDE DE TITRE DE RECETTE')), 0, 1, 'C', 0);
            $pdf->SetFont('courier', '', 11);
            $pdf->Cell(100, 5, iconv(HTTPCHARSET, "CP1252", __('SERVICE ATTRIBUTAIRE :').$parametrage_facturation["fact_attributaire"]), 0, 1, 'L', 0);
            $pdf->Cell(100, 5, iconv(HTTPCHARSET, "CP1252", __('CHAPITRE :').$parametrage_facturation["fact_chapitre"]), 0, 1, 'L', 0);
            $pdf->SetXY(260, 5);
            $pdf->Cell(25, 7, iconv(HTTPCHARSET, "CP1252", __(' Page  :  ').$pdf->PageNo()."/{nb} "), '0', 1, 'R', 0);
            $pdf->SetY(40);
            $pdf->MultiCell(20, 5, iconv(HTTPCHARSET, "CP1252", __('NUMERO TITRE')), 1, 'R', 0);
            $pdf->SetXY(25, 40);
            $pdf->Cell(85, 10, iconv(HTTPCHARSET, "CP1252", __('COMMUNE')), 1, 0, 'C', 0);
            $pdf->Cell(120, 10, iconv(HTTPCHARSET, "CP1252", ('OBJET ET DECOMPTE')), 1, 0, 'C', 0);
            $pdf->Cell(60, 10, iconv(HTTPCHARSET, "CP1252", __('SOMME DUE')), 1, 1, 'C', 0);
            return $pdf;
        }

        function pdf_stat_pied($pdf, $tauxPage, $tauxGlobal, $tot = 0) {
            $pdf->Cell(285,.5,'',1,1,'L',0);
            $pdf->Cell(225,7,iconv(HTTPCHARSET,"CP1252",__('TOTAL DE LA PAGE  ')),0,0,'R',0);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",number_format($tauxPage,2).' '),0,1,'R',0);
            $tauxGlobal+=$tauxPage;
            $tauxPage = 0;
            if($tot)
            {
                $pdf->Cell(225,7,iconv(HTTPCHARSET,"CP1252",__('TOTAL GENERAL  ')),0,0,'R',0);
                $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",number_format($tauxGlobal,2).' '),0,1,'R',0);
            }
            $pdf->SetXY(180,186);
            $pdf->Cell(60,7,iconv(HTTPCHARSET,"CP1252",__('LE CHEF DE SERVICE :')),0,1,'L',0);
            return $pdf;
        }

        $pdf=new FPDF('L','mm','A4');
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(true);
        $pdf->SetFont('courier','',11);
        $pdf->SetDrawColor(30,7,146);
        $pdf->SetMargins(5,5,5);
        $pdf->SetDisplayMode('real','single');

        $pdf = pdf_stat_entete($this->f, $pdf);

        //  compteur de limite d'affichage
        $compteur = 0;
        //////////////////////////
        //
        $parametrage_facturation = $this->f->get_parametrage_facturation();
        //
        $tauxPage = 0;
        $tauxGlobal = 0;

        foreach ($commune as $c) {
            ////////////////////////////////////////////////////////////////////////////////////////////
            $compteur++;
            if ($compteur >= 18) {
                $pdf = pdf_stat_pied($pdf, $tauxPage, $tauxGlobal);
                $pdf = pdf_stat_entete($this->f, $pdf);
                $compteur = 1;
            }
            $pdf->Cell(20, 7, '', 0, 0, 'R', 0);
            $pdf->Cell(85, 7, iconv(HTTPCHARSET, "CP1252", '  '.$c['ville']), 0, 0, 'L', 0);
            $pdf->Cell(120, 7, iconv(HTTPCHARSET, "CP1252", __('TENUE INFORMATIQUE DU FICHIER ELECTORAL')), 0, 0, 'L', 0);
            //// calcul de la "somme du"
            $sommeDue = $parametrage_facturation["fact_electeur_unitaire"] * $c['total'];
            if ($sommeDue < $parametrage_facturation["fact_forfait"]) {
                $sommeDue = $parametrage_facturation["fact_forfait"];
            }
            $pdf->Cell(60, 7, iconv(HTTPCHARSET, "CP1252", number_format($sommeDue, 2).' '), 0, 1, 'R', 0);
            $tauxPage+=$sommeDue;
        }

        $pdf = pdf_stat_pied($pdf, $tauxPage, $tauxGlobal, 1);

        /////////////////////////////////////////////////////////////////////////////////////

        /**
         * OUTPUT
         */
        //
        $filename = "facturation_commune_titre_de_recette-".(date('Y') - 1).".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
