<?php
/**
 * Ce script définit la classe 'juryTraitement'.
 *
 * @package openelec
 * @version SVN : $Id: traitement.jury.class.php 2716 2023-05-23 12:54:52Z cgarcin $
 */

require_once "../obj/traitement.jury.class.php";

/**
 * Définition de la classe 'juryTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class jurySuppleantTraitement extends juryTraitement {

    public $jury = 2;
}
