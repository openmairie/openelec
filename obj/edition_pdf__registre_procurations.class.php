<?php
/**
 * Ce script définit la classe 'edition_pdf__registre_procurations'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__registre_procurations' (edition_pdf).
 */
class edition_pdf__registre_procurations extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * @return array
     */

    public function compute_pdf__registre_procurations($params = array()) {
        //
        $libelle_commune = $this->f->collectivite["ville"];
        $liste = $_SESSION["liste"];
        $libelle_liste = $_SESSION["libelle_liste"];
        $titre_libelle_ligne1 = __("REGISTRE DES PROCURATIONS");
        $titre_libelle_ligne2 = "";
        $bureau_code = "";
        $bureau_libelle = "";
        $canton_libelle = "";
        $circonscription_libelle = "";
        $message = "";
        //
        $mode_edition = "";
        if (isset($params["mode_edition"])) {
            if (array_key_exists("liste", $params) === true) {
                $liste = $params["liste"];
                $ret = $this->f->get_one_result_from_db_query(sprintf(
                    'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                    DB_PREFIXE,
                    $this->f->db->escapesimple($liste)
                ));
                $libelle_liste = $ret["result"];
            }
            if ($params["mode_edition"] == "parbureau") {
                if (isset($params["bureau_code"])) {
                    $mode_edition = "parbureau";
                    $bureau_code = $params["bureau_code"];
                    $message = sprintf(
                        __("%s\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),
                        $titre_libelle_ligne1,
                        $libelle_liste,
                        $bureau_code
                    );
                    $sql = sprintf(
                        'SELECT bureau.id FROM %1$sbureau WHERE bureau.om_collectivite=%2$s AND bureau.code=\'%3$s\'',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        $this->f->db->escapesimple($bureau_code)
                    );
                    $bureau_id = $this->f->db->getone($sql);
                    $this->f->isDatabaseError($bureau_id);
                    //
                    $inst_bureau = $this->f->get_inst__om_dbform(array(
                        "obj" => "bureau",
                        "idx" => $bureau_id,
                    ));
                    if ($inst_bureau->exists() !== true) {
                        return;
                    }
                    $bureau_libelle = $inst_bureau->getVal("libelle");
                    //
                    $inst_canton = $this->f->get_inst__om_dbform(array(
                        "obj" => "canton",
                        "idx" => $inst_bureau->getVal("canton"),
                    ));
                    $canton_libelle = $inst_canton->getVal("libelle");
                    //
                    $inst_circonscription = $this->f->get_inst__om_dbform(array(
                        "obj" => "circonscription",
                        "idx" => $inst_bureau->getVal("circonscription"),
                    ));
                    $circonscription_libelle = $inst_circonscription->getVal("libelle");
                }
            } elseif ($params["mode_edition"] == "commune") {
                $mode_edition = "commune";
                $bureau_code = "ALL";
                $message = sprintf(
                    __("%s\nListe %s\nAucun enregistrement selectionne"),
                    $titre_libelle_ligne1,
                    $libelle_liste
                );
            }
        }
        if ($mode_edition == "") {
            return;
        }

        /**
         *
         */
        $sql = sprintf(
            'SELECT
                mandant.nom as nom,
                mandant.nom_usage as nom_usage,
                mandant.prenom as prenom,
                procuration.mandant_ine as mandant_ine,
                bureau_mandant.code as bureau,
                CASE WHEN mandataire IS NOT NULL
                    THEN mandataire.nom
                    WHEN procuration.mandataire_infos = \'\'
                        THEN \'\'
                    ELSE (procuration.mandataire_infos::json->\'etatCivil\'->>\'nomNaissance\')
                END AS nomm,
                CASE WHEN mandataire IS NOT NULL
                    THEN mandataire.nom_usage
                    WHEN procuration.mandataire_infos = \'\'
                        THEN \'\'
                    ELSE (procuration.mandataire_infos::json->\'etatCivil\'->>\'nomUsage\')
                END AS nom_usagem,
                CASE WHEN mandataire IS NOT NULL
                    THEN mandataire.prenom
                    WHEN procuration.mandataire_infos = \'\'
                        THEN \'\'
                    ELSE (procuration.mandataire_infos::json->\'etatCivil\'->>\'prenoms\')
                END AS prenomm,
                procuration.mandataire_ine as mandataire_ine,
                CASE WHEN mandataire IS NOT NULL
                    THEN bureau_mandataire.code
                    ELSE \'HC\'
                END AS bureaum,
                CONCAT(procuration.autorite_type, \' \', TRIM(CONCAT(procuration.autorite_consulat, \' \', procuration.autorite_commune))) as origine1_1,
                substring(procuration.autorite_nom_prenom FROM 1 for 25) as origine1_2,
                substring(procuration.autorite_nom_prenom FROM 26 for 25) as origine2_1,
                substring(procuration.autorite_nom_prenom FROM 51 for 25) as origine2_2,
                CASE 
                    WHEN statut = \'demande_refusee\'
                        THEN \'REFU\'
                    WHEN statut = \'procuration_annulee\'
                        THEN \'ANUL\'
                END as refus, 
                substring(motif_refus FROM 1 for 24) as motif_refus_1,
                substring(motif_refus FROM 24 for 24) as motif_refus_2,
                substring(motif_refus FROM 48 for 24) as motif_refus_3,
                substring(motif_refus FROM 72 for 24) as motif_refus_4,
                to_char(date_accord,\'DD/MM/YYYY\') as dateaccord, 
                \'\' as heureaccord, 
                to_char(procuration.debut_validite,\'DD/MM/YYYY\') as debutvalidm,
                to_char(procuration.fin_validite,\'DD/MM/YYYY\') as finvalidm
            FROM
                %1$sprocuration
                INNER JOIN %1$selecteur AS mandant ON procuration.mandant=mandant.id
                LEFT JOIN %1$sbureau AS bureau_mandant ON mandant.bureau=bureau_mandant.id
                LEFT JOIN %1$selecteur AS mandataire ON procuration.mandataire=mandataire.id
                LEFT JOIN %1$sbureau AS bureau_mandataire ON mandataire.bureau=bureau_mandataire.id
            WHERE
                mandant.liste=\'%3$s\'
                AND procuration.om_collectivite=%2$s
                AND procuration.statut IN (\'procuration_confirmee\', \'demande_refusee\', \'procuration_annulee\', \'procuration_perimee\')
                %4$s
            ORDER BY
                withoutaccent(lower(mandant.nom)),
                withoutaccent(lower(mandant.prenom))
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $liste,
            ($mode_edition === "parbureau" && $bureau_code != "ALL" ? "AND bureau_mandant.code='".$this->f->db->escapesimple($bureau_code)."'" : "")
        );

        /**
         *
         */
        require_once "../obj/fpdf_table.class.php";
        $pdf = new PDF('L', 'mm', 'A4');
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(false);
        // Fixe la police utilisée pour imprimer les chaînes de caractères
        $pdf->SetFont(
            'Arial', // Famille de la police.
            '', // Style de la police.
            7 // Taille de la police en points.
        );
        // Fixe la couleur pour toutes les opérations de tracé
        $pdf->SetDrawColor(30, 7, 146);
        // Fixe les marges
        $pdf->SetMargins(
            10, // Marge gauche.
            10, // Marge haute.
            10 // Marge droite.
        );
        $pdf->SetDisplayMode('real', 'single');
        $pdf->AddPage();
        // COMMON
        $param = array(
            7, // Titre : Taille de la police en points.
            7, // Entete : Taille de la police en points.
            7, // Taille de la police en points.
            1, //
            1, //
            10, // Nombre maximum d'enregistrements par page pour la première page
            10, // Nombre maximum d'enregistrements par page pour les autres pages
            10, // Position de départ - X - La valeur de l'abscisse.
            10, // Position de départ - Y - La valeur de l'ordonnée.
            1, //
            0, //
        );
        $page_content_width = 277;
        // Variable de developpement pour afficher les bordures de toutes les cellules
        $allborders = false;
        //-------------------------------------------------------------------//
        // TITRE (PAGE HEADER)
        $page_header = $this->get_page_header_config(array(
            "allborders" => $allborders,
            "page_content_width" => $page_content_width,
            "titre_libelle_ligne1" => $titre_libelle_ligne1,
            "titre_libelle_ligne2" => $titre_libelle_ligne2,
            "libelle_commune" => $libelle_commune,
            "libelle_liste" => $libelle_liste,
            "bureau_code" => $bureau_code,
            "bureau_libelle" => $bureau_libelle,
            "canton_libelle" => $canton_libelle,
            "circonscription_libelle" => $circonscription_libelle,
            "page_header_line_height" => 4,
        ));
        //-------------------------------------------------------------------//
        // ENTETE (TABLE HEADER)
        $heightentete = 8;
        $entete = array(
            array(
                __('MANDANT'),
                45, $heightentete, 0,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('BV'),
                10, $heightentete, 0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('MANDATAIRE'),
                45, $heightentete, 0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('BV'),
                10, $heightentete, 0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('ORIGINE DE LA DEMANDE'),
                50, $heightentete, 0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('REFUS'),
                10, $heightentete, 0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('MOTIF'),
                45, $heightentete, 0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('DATE'),
                18, $heightentete, 0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('DUREE'),
                18, $heightentete, 0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('OBSERVATIONS'),
                24, $heightentete, 0,'1','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        );
        //-------------------------------------------------------------------//
        // FIRSTPAGE
        $firstpage = array();
        //-------------------------------------------------------------------//
        // LASTPAGE
        $nbr_enregistrement=0;
        $lastpage = array();
        //-------------------------------------------------------------------//
        $heightligne=4;
        $col=array();
        $col=array(
            'nom' => array(
                45, $heightligne, 2,'LRT','L','0','0','0','255','255','255',
                array(0,0),' ','',
                'CELLSUP_NON',0,'NA','NC','','B',8, array('0'),
            ),
            'nom_usage'=>array(
                45, $heightligne, 2,'LR','L','0','0','0','255','255','255',
                array(0,0),'       -    ','',
                'CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0, array('0'),
            ),
            'prenom'=>array(
                45, $heightligne, 2,'LR','L','0','0','0','255','255','255',
                array(0,0),'    ','',
                'CELLSUP_NON',0,'NA','NC','','NB',8, array('0'),
            ),
            'mandant_ine'=>array(
                45, $heightligne, 0,'LBR','L','0','0','0','255','255','255',
                array(0,0),'INE : ','',
                'CELLSUP_NON',0,'NA','NC','','NB',8, array('0'),
            ),
            //
            'bureau' => array(
                10, $heightligne, 2,'LTR','C','0','0','0','255','255','255',array(0, 3*$heightligne),'','',
                'CELLSUP_VIDE', array(10,$heightligne*3, 0,'LRB','C','0','0','0','255','255','255',array(0,0), ),
                0,'NA','NC','','NB',8,array('0'),
            ),
            'nomm' => array(
                45,$heightligne,2,'LRT','L','0','0','0','255','255','255',array(0, $heightligne),' ','',
                'CELLSUP_NON',0,'NA','NC','','B',8,array('0')
            ),
            'nom_usagem' => array(
                45,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','',
                'CELLSUP_NON',0,'NA','CONDITION',array('NN'),'NB',0,array('0')
            ),
            'prenomm' => array(
                45, $heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'    ','',
                'CELLSUP_NON',0,
                'NA','NC','','NB',8,array('0')
            ),
            'mandataire_ine'=>array(45,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),'INE : ','',
                'CELLSUP_NON',0,
                'NA','NC','','NB',8,array('0')
            ),
            'bureaum' => array(
                10,$heightligne,2,'LTR','C','0','0','0','255','255','255', array(0,3*$heightligne),'','',
                'CELLSUP_VIDE', array(10,$heightligne*3,0,'LRB','C','0','0','0','255','255','255',array(0,0)),
                0,'NA','NC','','NB',8,array('0')
            ),
            'origine1_1'=>array(50,$heightligne,2,'LRT','L','0','0','0','255','255','255',array(0,$heightligne),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
            'origine1_2'=>array(50,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
            'origine2_1'=>array(50,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
            'origine2_2'=>array(50,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),' ','','CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
            'refus'=>array(10,$heightligne*4,0,'1','L','0','0','0','255','255','255',array(0,$heightligne*3),' ','',
                'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
            'motif_refus_1'=>array(45,$heightligne,2,'LTR','L','0','0','0','255','255','255',array(0,0),' ','',
            'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
            'motif_refus_2'=>array(45,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','',
            'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
            'motif_refus_3'=>array(45,$heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),' ','',
            'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
            'motif_refus_4'=>array(45,$heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),' ','',
            'CELLSUP_NON',0,'NA','NC','','NB',8,array('0')),
            'dateaccord' => array(
                18,
                $heightligne*2,2,'LTR','C','0','0','0','255','255','255',array(0,$heightligne*3), " ", '', 'CELLSUP_NON',
                0,'NA','NC','','NB',7,array('0')),
            'heureaccord' => array(
                18,
                $heightligne*2,0,'LBR','C','0','0','0','255','255','255',array(0,0), " ", '', 'CELLSUP_NON',
                0,'NA','NC','','NB',7,array('0')),
            'debutvalidm' => array(
                18,
                $heightligne*2,2,'LTR','C','0','0','0','255','255','255',array(0,$heightligne*2), __("Du")." ", '', 'CELLSUP_NON',
                0,'NA','NC','','NB',7,array('0')),
            'finvalidm' => array(
                18,
                $heightligne*2,0,'LBR','C','0','0','0','255','255','255',array(0,0), __("au")." ", '', 'CELLSUP_VIDE',
                array(
                    24,
                    $heightligne*4,0,'1','C','0','0','0','255','255','255',array(0,$heightligne*2)
                ),
                0,'NA','NC','','NB',7,array('0')),
        );
        //-------------------------------------------------------------------//
        $enpied = array();
        //-------------------------------------------------------------------//
        $pdf->Table(
            $page_header,
            $entete,
            $col,
            $enpied,
            $sql,
            $this->f->db,
            $param,
            $lastpage,
            $firstpage
        );

        /**
         * Affichage d'un bloc spécifique si aucun enregistrement sélectionné.
         */
        if ($pdf->msg==1 and $message!='') {
            $pdf->SetFont('Arial','',10);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetFillColor(213,8,26);
            $pdf->SetTextColor(255,255,255);
            $pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message), ($allborders == true ? 1 : 1),'C',1);
        }

        /**
         * OUTPUT
         */
        //
        $filename = sprintf(
            'registre_procurations%s-%s-.pdf',
            ($mode_edition === "parbureau" ? "-parbureau" : ""),
            date('Ymd-His')
        );
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
