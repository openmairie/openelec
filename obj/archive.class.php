<?php
/**
 * Ce script définit la classe 'archive'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/archive.class.php";

/**
 * Définition de la classe 'archive' (om_dbform).
 */
class archive extends archive_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // Pas d'action 'ajouter'
        unset($this->class_actions[0]);
        // Pas d'action 'modifier'
        unset($this->class_actions[1]);
        // Pas d'action 'supprimer'
        unset($this->class_actions[2]);
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "electeur_id",
            "numero_bureau",
            "types",
            "bureau_de_vote_code",
            "bureau_de_vote_libelle",
            "ancien_bureau_de_vote_code",
            "ancien_bureau_de_vote_libelle",
            "bureauforce", //6
            "date_modif",
            "utilisateur",
            "civilite",
            "sexe",
            "nom",
            "nom_usage",
            "prenom",
            "date_naissance",
            "code_departement_naissance",
            "libelle_departement_naissance",
            "code_lieu_de_naissance",
            "libelle_lieu_de_naissance",
            "code_nationalite",
            "numero_habitation",   //21
            "code_voie",           //22
            "libelle_voie",
            "complement_numero",
            "complement",
            "provenance",
            "libelle_provenance",
            "observation",
            "resident",
            "adresse_resident",
            "complement_resident",
            "cp_resident",
            "ville_resident",
            "etat",
            "liste",
            "tableau",
            "date_tableau",
            "envoi_cnen",
            "date_cnen",
            "date_mouvement",
            "mouvement",
        );
    }

    /**
     * Clause where pour la requête de sélection des données de l'enregistrement.
     *
     * @return string
     */
    function get_var_sql_forminc__selection() {
        return "and archive.om_collectivite=".intval($_SESSION["collectivite"])."";
    }

    /**
     * Ajout dans la table archive des mouvements
     */
    function ajouterTraitement($row, $dateTableau) {
        // parametrage
        $this->msg="";
        $this->setValFTraitement($row, $dateTableau);
        $this->setId(); // id automatique
        //
        $res = $this->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, $this->table),
            $this->valF,
            DB_AUTOQUERY_INSERT
        );
        $this->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $this->table)."\", ".print_r($this->valF, true).", DB_AUTOQUERY_INSERT);",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $this->msg=$this->msg."<br>".__("Enregistrement")." ".
        $this->valF[$this->clePrimaire].__(" de la table ").
        $this->table." [".$this->f->db->affectedRows().
        __(" enregistrement ajoute]") ;
    }

    /**
     *
     */
    function  setValFTraitement($row, $dateTableau){
        // identifiant
        $this->valF['bureau_de_vote_code'] = $row['bureau_de_vote_code'];
        $this->valF['bureau_de_vote_libelle'] = $row['bureau_de_vote_libelle'];
        $this->valF['ancien_bureau_de_vote_code'] = $row['ancien_bureau_de_vote_code'];
        $this->valF['ancien_bureau_de_vote_libelle'] = $row['ancien_bureau_de_vote_libelle'];
        $this->valF['bureauforce'] = $row['bureauforce'];
        $this->valF['numero_bureau'] = $row['numero_bureau']; // provisoire en inscription
        $this->valF['electeur_id'] = $row['electeur_id']; //
        // Etat civil
        $this->valF['civilite'] = $row['civilite'];
        $this->valF['sexe'] = $row['sexe'];
        $this->valF['nom'] = $row['nom'];
        $this->valF['nom_usage'] = $row['nom_usage'];
        $this->valF['prenom'] = $row['prenom'];
        $this->valF['date_naissance'] = $row['date_naissance'];
        $this->valF['code_departement_naissance'] = $row['code_departement_naissance'];
        $this->valF['libelle_departement_naissance'] = $row['libelle_departement_naissance'];
        $this->valF['code_lieu_de_naissance'] = $row['code_lieu_de_naissance'];
        $this->valF['libelle_lieu_de_naissance'] = $row['libelle_lieu_de_naissance'];
        $this->valF['code_nationalite'] = $row[ 'code_nationalite'];
        //adresse
        $this->valF['code_voie'] = $row['code_voie'];
        $this->valF['libelle_voie'] = $row['libelle_voie'];
        $this->valF['numero_habitation'] = $row['numero_habitation'];
        $this->valF['complement_numero'] = $row['complement_numero'] ;
        $this->valF['complement'] = $row['complement'] ;
        // provennance
        $this->valF['provenance'] = $row['provenance'];
        $this->valF['libelle_provenance'] = $row['libelle_provenance'];
        // adresse resident
        $this->valF['resident'] = $row['resident'];
        $this->valF['adresse_resident'] = $row['adresse_resident'];
        $this->valF['complement_resident'] = $row['complement_resident'];
        $this->valF['cp_resident'] = $row['cp_resident'] ;
        $this->valF['ville_resident'] = $row['ville_resident'] ;
        //  mouvement
        $this->valF['date_modif'] = $row['date_modif'];
        $this->valF['utilisateur']= $row['utilisateur'];
        $this->valF['mouvement']=$row['id'];
        $this->valF['observation']=$row['observation'];
        $this->valF['date_mouvement']=$this->dateSystemeDB();
        $this->valF['types']=$row["types"];
        //cnen
        $this->valF['envoi_cnen']=$row["envoi_cnen"];
        // compatibilite postgreSQL (date vide non acceptees)
        if (!isset($row["date_cnen"]) )
            $this->valF['date_cnen']=null;
        else
           if ($row["date_cnen"]!='' )
              $this->valF['date_cnen']=$row["date_cnen"];
        //tableau
        $this->valF['tableau']=$row['tableau'];
        $this->valF['date_tableau']=$dateTableau;
        //liste
        $this->valF['liste']=$row['liste'];
        $this->valF['etat']=$row['etat'];
        $this->valF['typecat'] = "";
        //collectivite
        /* ++ */ $this->valF['om_collectivite'] = $row['om_collectivite'];
    }
}
