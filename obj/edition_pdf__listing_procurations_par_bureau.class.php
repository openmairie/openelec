<?php
/**
 * Ce script définit la classe 'edition_pdf__listing_procurations_par_bureau'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__listing_procurations_par_bureau' (edition_pdf).
 */
class edition_pdf__listing_procurations_par_bureau extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * @return array
     */
    public function compute_pdf__listing_procurations_par_bureau($params = array()) {
        //
        $libelle_commune = $this->f->collectivite["ville"];
        $liste = $_SESSION["liste"];
        $libelle_liste = $_SESSION["libelle_liste"];
        $titre_libelle_ligne1 = __("LISTE DES PROCURATIONS");
        $titre_libelle_ligne2 = "";
        $bureau_code = "";
        $bureau_libelle = "";
        $canton_libelle = "";
        $circonscription_libelle = "";
        $message = "";
        $date_election = "";
        //
        $mode_edition = "";
        if (isset($params["mode_edition"])) {
            if (array_key_exists("date_election", $params) === true) {
                $date_election = $params["date_election"];
                $titre_libelle_ligne2 = sprintf(
                    'Élection du %s',
                    $this->f->formatDate($date_election)
                );
            }
            if (array_key_exists("liste", $params) === true) {
                $liste = $params["liste"];
                $ret = $this->f->get_one_result_from_db_query(sprintf(
                    'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                    DB_PREFIXE,
                    $this->f->db->escapesimple($liste)
                ));
                $libelle_liste = $ret["result"];
            }
            if ($params["mode_edition"] == "parbureau") {
                if (isset($params["bureau_code"])) {
                    $mode_edition = "parbureau";
                    $bureau_code = $params["bureau_code"];
                    $message = sprintf(
                        __("%s\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),
                        $titre_libelle_ligne1,
                        $libelle_liste,
                        $bureau_code
                    );
                    $sql = sprintf(
                        'SELECT bureau.id FROM %1$sbureau WHERE bureau.om_collectivite=%2$s AND bureau.code=\'%3$s\'',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        $this->f->db->escapesimple($bureau_code)
                    );
                    $bureau_id = $this->f->db->getone($sql);
                    $this->f->isDatabaseError($bureau_id);
                    //
                    $inst_bureau = $this->f->get_inst__om_dbform(array(
                        "obj" => "bureau",
                        "idx" => $bureau_id,
                    ));
                    $bureau_libelle = $inst_bureau->getVal("libelle");
                    //
                    $inst_canton = $this->f->get_inst__om_dbform(array(
                        "obj" => "canton",
                        "idx" => $inst_bureau->getVal("canton"),
                    ));
                    $canton_libelle = $inst_canton->getVal("libelle");
                    //
                    $inst_circonscription = $this->f->get_inst__om_dbform(array(
                        "obj" => "circonscription",
                        "idx" => $inst_bureau->getVal("circonscription"),
                    ));
                    $circonscription_libelle = $inst_circonscription->getVal("libelle");
                }
            }
        }

        /**
         *
         */
        $sql = sprintf(
            'SELECT
                mandant.nom as nom,
                mandant.nom_usage as nom_usage,
                mandant.prenom as prenom,
                to_char(mandant.date_naissance,\'DD/MM/YYYY\') as naissance,
                (mandant.libelle_lieu_de_naissance||\' (\'||mandant.code_departement_naissance||\')\' ) as lieu,
                case mandant.resident when \'Non\' then (mandant.numero_habitation||\' \'||mandant.complement_numero||\' \'||mandant.libelle_voie) when \'Oui\' then mandant.adresse_resident end as adresse,
                case mandant.resident when \'Non\' then mandant.complement when \'Oui\' then (mandant.complement_resident||\' \' ||mandant.cp_resident||\' - \'||mandant.ville_resident) end as complement,
                procuration.mandant_ine,
                \' \' as empty,
                mandant.numero_bureau,
                CASE WHEN mandataire IS NOT NULL
                    THEN mandataire.nom
                    ELSE (procuration.mandataire_infos::json->\'etatCivil\'->>\'nomNaissance\')
                END AS nomm,
                CASE WHEN mandataire IS NOT NULL
                    THEN mandataire.nom_usage
                    ELSE (procuration.mandataire_infos::json->\'etatCivil\'->>\'nomUsage\')
                END AS nom_usagem,
                CASE WHEN mandataire IS NOT NULL
                    THEN mandataire.prenom
                    ELSE (procuration.mandataire_infos::json->\'etatCivil\'->>\'prenoms\')
                END AS prenomm,
                CASE WHEN mandataire IS NOT NULL
                    THEN to_char(mandataire.date_naissance,\'DD/MM/YYYY\')
                    ELSE (procuration.mandataire_infos::json->\'etatCivil\'->>\'dateNaissance\')
                END AS naissancem,
                CASE WHEN mandataire IS NOT NULL
                    THEN (mandataire.libelle_lieu_de_naissance||\' (\'||mandataire.code_departement_naissance||\')\' )
                    ELSE (procuration.mandataire_infos::json->\'etatCivil\'->\'communeNaissance\'->>\'libelle\')
                END as lieum,
                case mandataire.resident when \'Non\' then (mandataire.numero_habitation||\' \'||mandataire.complement_numero||\' \'||mandataire.libelle_voie) when \'Oui\' then mandataire.adresse_resident end as adressem,
                case mandataire.resident when \'Non\' then mandataire.complement when \'Oui\' then (mandataire.complement_resident||\' \' ||mandataire.cp_resident||\' - \'||mandataire.ville_resident) end as complementm,
                procuration.mandataire_ine AS mandataire_ine,
                CASE WHEN mandataire IS NOT NULL
                    THEN bureau_mandataire.code
                    ELSE \'Hors Commune\'
                END AS codeburm,
                mandataire.numero_bureau AS numburm,
                to_char(procuration.debut_validite,\'DD/MM/YYYY\') as debutvalidm,
                to_char(procuration.fin_validite,\'DD/MM/YYYY\') as finvalidm
            FROM
                %1$sprocuration
                INNER JOIN %1$selecteur AS mandant ON procuration.mandant=mandant.id 
                LEFT JOIN %1$sbureau AS bureau_mandant ON mandant.bureau=bureau_mandant.id
                LEFT JOIN %1$selecteur AS mandataire ON procuration.mandataire=mandataire.id
                LEFT JOIN %1$sbureau AS bureau_mandataire ON mandataire.bureau=bureau_mandataire.id
            WHERE
                mandant.liste=\'%3$s\'
                %4$s
                AND procuration.statut = \'procuration_confirmee\'
                AND procuration.om_collectivite=%2$s
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $liste,
            ($mode_edition === "parbureau" && $bureau_code != "ALL" ? "AND bureau_mandant.code='".$this->f->db->escapesimple($bureau_code)."'" : "")
        );
        //
        if ($date_election !== "") {
            $sql .= " and (procuration.debut_validite<='".$date_election."' and ";
            $sql .= " procuration.fin_validite>='".$date_election."') ";
        }
        //
        $sql=$sql." order by withoutaccent(lower(mandant.nom)), withoutaccent(lower(mandant.prenom)) ";

        /**
         *
         */
        require_once "../obj/fpdf_table.class.php";
        $pdf = new PDF('L', 'mm', 'A4');
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(false);
        // Fixe la police utilisée pour imprimer les chaînes de caractères
        $pdf->SetFont(
            'Arial', // Famille de la police.
            '', // Style de la police.
            7 // Taille de la police en points.
        );
        // Fixe la couleur pour toutes les opérations de tracé
        $pdf->SetDrawColor(30, 7, 146);
        // Fixe les marges
        $pdf->SetMargins(
            10, // Marge gauche.
            10, // Marge haute.
            10 // Marge droite.
        );
        $pdf->SetDisplayMode('real', 'single');
        $pdf->AddPage();
        // COMMON
        $param = array(
            7, // Titre : Taille de la police en points.
            7, // Entete : Taille de la police en points.
            7, // Taille de la police en points.
            1, //
            1, //
            4, // Nombre maximum d'enregistrements par page pour la première page
            4, // Nombre maximum d'enregistrements par page pour les autres pages
            10, // Position de départ - X - La valeur de l'abscisse.
            10, // Position de départ - Y - La valeur de l'ordonnée.
            0, //
            0, //
        );
        $page_content_width = 277;
        // Variable de developpement pour afficher les bordures de toutes les cellules
        $allborders = false;
        //-------------------------------------------------------------------//
        // TITRE (PAGE HEADER)
        $page_header = $this->get_page_header_config(array(
            "allborders" => $allborders,
            "page_content_width" => $page_content_width,
            "titre_libelle_ligne1" => $titre_libelle_ligne1,
            "titre_libelle_ligne2" => $titre_libelle_ligne2,
            "libelle_commune" => $libelle_commune,
            "libelle_liste" => $libelle_liste,
            "bureau_code" => $bureau_code,
            "bureau_libelle" => $bureau_libelle,
            "canton_libelle" => $canton_libelle,
            "circonscription_libelle" => $circonscription_libelle,
            "page_header_line_height" => 4,
        ));
        //-------------------------------------------------------------------//
        // ENTETE (TABLE HEADER)
        $heightentete = 8;
        $entete = array(
            array(
                __('MANDANT'),
                107, $heightentete, 2, ($allborders == true ? 1 : 'LRT'), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('Nom Patronymique,Prenoms,Nom d\'Usage'),
                87, $heightentete-4, 2, ($allborders == true ? 1 : 'LRT'), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('Date et lieu de naissance,Adresse'),
                87, $heightentete-4, 0, ($allborders == true ? 1 : 'LRB'), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                '<VCELL>',
                20, $heightentete, 0, ($allborders == true ? 1 : 1), 'C',
                '0','0','0','255','255','255',
                array(0, -4), 3, array('INE', __("No Ordre")),'NB',0,1,0),
            array(
                __('MANDATAIRE'),
                107, $heightentete, 2, ($allborders == true ? 1 : 1), 'C','0','0','0','255','255','255',array(0,16),0,'','NB',0,1,0),
            array(
                __('Nom Patronymique,Prenoms,Nom d\'Usage'),
                87, $heightentete-4, 2, ($allborders == true ? 1 : 'LRT'), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('Date et lieu de naissance,Adresse'),
                87, $heightentete-4, 0, ($allborders == true ? 1 : 'LRB'), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                '<VCELL>',
                20, $heightentete, 0, ($allborders == true ? 1 : 1), 'C',
                '0','0','0','255','255','255',
                array(0,-4), 3, array('INE, Bureau', __("No Ordre")),'NB',0,1,0),
            array(
                '<VCELL>',
                18, $heightentete+8, 0, ($allborders == true ? 1 : 1), 'C','0','0','0','255','255','255',array(0,-16), 3,array('Validité'),'NB',0,1,0),
            array(
                __('Observations'),
                44, $heightentete+8, 0, ($allborders == true ? 1 : 1), 'C','0','0','0','255','255','255',array(0,16),0,'','NB',0,1,0)
        );
        //-------------------------------------------------------------------//
        // FIRSTPAGE
        $firstpage = array();
        //-------------------------------------------------------------------//
        // LASTPAGE
        $lastpage_height = 9;
        $nbr_enregistrement=0;
        if ($date_election !== "") {
            $message_fin = sprintf(
                __("Nombre total de procurations election du %s pour le bureau  No %s    -   %s   :"),
                $this->f->formatDate($date_election),
                $bureau_code,
                $bureau_libelle
            );
        } else {
            $message_fin = sprintf(
                __("Nombre total de procurations pour le bureau  No %s    -   %s   :"),
                $bureau_code,
                $bureau_libelle
            );
        }
        $lastpage = array_merge(
            $page_header,
            array(
                array(
                    __('TOTALISATION    DES   PROCURATIONS'),
                    $page_content_width, $lastpage_height, 1, '1','C','0','0','0','255','255','255',array(0,0),0,'','NB',10,1,0),
                array(
                    '   ',
                    $page_content_width, $lastpage_height, 1, 'T','C','0','0','0','255','255','255',array(0,0),0,'','NB',9,1,0),
                array(
                    $message_fin,
                    195, $lastpage_height, 0,'0','R','0','0','0','255','255','255',array(0,0),0,'','NB',10,1,0),
                array(
                    '<LIGNE>',
                    30, $lastpage_height, 1,'0','L','0','0','0','255','255','255',array(0,0),0,'','B',9,1,0),
            )
        );
        //--------------------------------------------------------------------------//
        $heightligne=5;
        $col = array(
            'nom' => array(
                87, $heightligne, 2,'LRT','L','0','0','0','255','255','255',array(0,0),' ','', 'CELLSUP_NON',
                0,'NA','NC','','B',8,array('0')),
            'prenom' => array(
                87, $heightligne, 2,'LR','L','0','0','0','255','255','255',array(0,0),'    ','', 'CELLSUP_NON',
                0,'NA','NC','','NB',8,array('0')),
            'nom_usage' => array(
                87, $heightligne, 2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','', 'CELLSUP_NON',
                0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
            'naissance' => array(
                87, $heightligne, 2,'LR','L','0','0','0','255','255','255',array(0,0),__('     Ne(e) le '),'', 'CELLSUP_NON',
                0,'NA','NC','','NB',0,array('0')),
            'lieu' => array(
                87, $heightligne, 2,'LR','L','0','0','0','255','255','255',array(0,0),__('     a  '),'', 'CELLSUP_NON',
                0,'NA','NC','','NB',0,array('0')),
            'adresse' => array(
                87, $heightligne, 2,'LR','L','0','0','0','255','255','255',array(0,0),'     ','', 'CELLSUP_NON',
                0,0,'NA','NC','','NB',0,array('0')),
            'complement' => array(
                87, $heightligne, 0,'LBR','L','0','0','0','255','255','255',array(0,0),'     ','', 'CELLSUP_NON',
                0,0,'NA','NC','','NB',0,array('0')),
            'mandant_ine' => array(
                20, $heightligne*3, 2,'LTR','C',
                '0','0','0','255','255','255',
                array(0, $heightligne*6), '', '', 'CELLSUP_NON', 0, 0, 'NA', 'NC', '', 'NB', 0, array('0'),
            ),
            'empty' => array(
                20, $heightligne, 2,'LR','C',
                '0','0','0','255','255','255',
                array(0 ,0), '', '', 'CELLSUP_NON', 0, 0, 'NA', 'NC', '', 'NB', 0, array('0'),
            ),
            'numero_bureau' => array(
                20, $heightligne*3, 0,'LRB','C',
                '0','0','0','255','255','255',
                array(0, 0),'','', 'CELLSUP_NON', 0, 0, 'NA', 'NC', '', 'NB', 0, array('0'),
            ),
            'nomm' => array(
                87, $heightligne,2,'LRT','L','0','0','0','255','255','255',array(0, 4*$heightligne),' ','', 'CELLSUP_NON',
                0,'NA','NC','','B',8,array('0')),
            'prenomm' => array(
                87, $heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'    ','', 'CELLSUP_NON',
                0,'NA','NC','','NB',8,array('0')),
            'nom_usagem' => array(
                87, $heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','', 'CELLSUP_NON',
                0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
            'naissancem' => array(
                87, $heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),__('     Ne(e) le '),'', 'CELLSUP_NON',
                0,'NA','NC','','NB',0,array('0')),
            'lieum' => array(
                87, $heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),__('     a  '),'', 'CELLSUP_NON',
                0,'NA','NC','','NB',0,array('0')),
            'adressem' => array(
                87, $heightligne,2,'LR','L','0','0','0','255','255','255',array(0,0),'     ','', 'CELLSUP_NON',
                0,0,'NA','NC','','NB',0,array('0')),
            'complementm' => array(
                87, $heightligne,0,'LBR','L','0','0','0','255','255','255',array(0,0),'     ','', 'CELLSUP_NON',
                0,0,'NA','NC','','NB',0,array('0')),
            'mandataire_ine' => array(
                20, $heightligne*3, 2,'LRT','C',
                '0','0','0','255','255','255',
                array(0, $heightligne*6), '', '', 'CELLSUP_NON', 0, 0, 'NA', 'NC', '', 'NB', 0, array('0'),
            ),
            'codeburm' => array(
                20, $heightligne, 2,'LR','C',
                '0','0','0','255','255','255',
                array(0, 0), '', '', 'CELLSUP_NON', 0, 0, 'NA', 'NC', '', 'NB', 0, array('0'),
            ),
            'numburm' => array(
                20, $heightligne*3, 0,'LRB','C',
                '0','0','0','255','255','255',
                array(0, 0), '', '', 'CELLSUP_NON', 0, 0, 'NA', 'NC', '', 'NB', 0, array('0'),
            ),
            'debutvalidm' => array(
                18, $heightligne+13,2,'LTR','C','0','0','0','255','255','255',array(0,4*$heightligne),'du ','', 'CELLSUP_NON',
                array(44, $heightligne+30,0,'1','C','0','0','0','255','255','255',array(0,18)),
                0,'NA','NC','','NB',0,array('0')),
            'finvalidm' => array(
                18, $heightligne+12,0,'LBR','C','0','0','0','255','255','255',array(0,0),'au ','','CELLSUP_VIDE',
                array(44, $heightligne+30,0,'1','C','0','0','0','255','255','255',array(0,18)),
                0,'NA','NC','','NB',0,array('0')),
        );
        //-------------------------------------------------------------------//
        $enpied = array();
        //-------------------------------------------------------------------//
        $pdf->Table(
            $page_header,
            $entete,
            $col,
            $enpied,
            $sql,
            $this->f->db,
            $param,
            $lastpage,
            $firstpage
        );

        /**
         * Affichage d'un bloc spécifique si aucun enregistrement sélectionné.
         */
        if ($pdf->msg==1 and $message!='') {
            $pdf->SetFont('Arial','',10);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetFillColor(213,8,26);
            $pdf->SetTextColor(255,255,255);
            $pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message), ($allborders == true ? 1 : 1),'C',1);
        }

        /**
         * OUTPUT
         */
        //
        $filename = "listing_procurations_par_bureau-".date('Ymd-His').".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
