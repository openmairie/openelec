<?php
/**
 * Ce script définit la classe 'voie'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/voie.class.php";

/**
 * Définition de la classe 'voie' (om_dbform).
 *
 * Cette classe represente une rue dans la commune susceptible d'etre
 * utilisee comme adresse d'un electeur.
 */
class voie extends voie_gen {

    /**
     *
     */
    function init_class_actions() {
        parent::init_class_actions();
        //
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-electeurparvoie",
            "view" => "view_edition_pdf__electeurparvoie",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Électeurs par voie"),
               "class" => "pdf-16",
            ),
            "permission_suffix" => "edition_pdf__electeurparvoie",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
    }

    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal("libelle_voie");
    }

    /**
     * Clause select pour la requête de sélection des données de l'enregistrement.
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "code",
            "libelle_voie",
            "cp",
            "ville",
        );
    }

    /**
     * Clause where pour la requête de sélection des données de l'enregistrement.
     *
     * @return string
     */
    function get_var_sql_forminc__selection() {
        if ($_SESSION["multi_collectivite"] == 1) {
            return parent::get_var_sql_forminc__selection();
        }
        return "and voie.om_collectivite=".intval($_SESSION["collectivite"])."";
    }

    /**
     * VIEW - view_edition_pdf__electeurparvoie.
     *
     * @return void
     */
    public function view_edition_pdf__electeurparvoie() {
        $this->checkAccessibility();
        //
        $_GET['obj'] = "electeurparvoie";
        $_GET['idx'] = $this->getVal($this->clePrimaire);
        //
        $om_edition = $this->f->get_inst__om_edition();
        $om_edition->view_pdf();
        die();
    }

    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en vue de l'insertion des donnees dans la base.
     *
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     *
     * @return void
     */
    function setvalF($val = array()) {
        //
        $this->valF['libelle_voie'] = $val['libelle_voie'];
        $this->valF['cp'] = $val['cp'];
        $this->valF['ville'] = $val['ville'];
        // On insere le code collectivite en fonction de la variable de session
        $this->valF['om_collectivite'] = $_SESSION["collectivite"];
    }

    /**
     *
     * @param array $val
     */
    function setValFAjout($val = array()) {
        /**
         *
         */
        //
        $this->valF[$this->clePrimaire] = $val[$this->clePrimaire];
        /**
         *
         */
        //
        if ($this->f->is_option_code_voie_auto_enabled() === false) {
            if ($this->valF[$this->clePrimaire] == "") {
                // Recuperation de l'Id "code" du dernier enregistrement de table voie
                $id = $this->f->db->nextId(DB_PREFIXE."voie");
                $this->valF[$this->clePrimaire] = $id;
            }
        } else {
            // Increment de la sequence
            $id = $this->f->db->nextId(DB_PREFIXE."voie");
            $this->valF[$this->clePrimaire] = $id;
        }
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        if ($maj == 0) {
            if ($this->f->is_option_code_voie_auto_enabled() === false) {
                // Recuperation de l'Id "code" du dernier enregistrement de table voie
                $res = $this->f->db->nextId(DB_PREFIXE."voie");
                $form->setVal("code",$res);
            }
            // Récupération et affichage des champs ayant pour valeur par défaut
            // une valeur stockée dans un om_parametre.
            $fields_with_default_val = array('cp', 'ville');
            foreach ($fields_with_default_val as $field) {
                $value = ! empty($this->f->getParameter($field)) ?
                    $this->f->getParameter($field) :
                    '';
                $form->setVal($field, $value);
            }
        }
    }

    /**
     * Cette methode est appelee lors de l'ajout ou de la modification d'un
     * objet, elle permet d'effectuer des tests d'integrite sur les valeurs
     * saisies dans le formulaire pour en empecher l'enregistrement.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Si la valeur est vide alors on rejette l'enregistrement
        $field_required = array("libelle_voie", "cp", "ville");
        foreach($field_required as $field) {
            if (trim($this->valF[$field]) == "") {
                $this->correct = false;
                $this->msg .= $this->form->lib[$field]." ";
                $this->msg .= __("est obligatoire")."<br />";
            }
        }
    }

    /**
     *
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //
        $this->rechercheTable($this->f->db, "decoupage", "code_voie", $id);
        $this->rechercheTable($this->f->db, "electeur", "code_voie", $id);
        $this->rechercheTable($this->f->db, "mouvement", "code_voie", $id);
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        if ($maj < 2) { // ajouter et modifier
            if ($maj == 1) { // modifier
                $form->setType("code", "hiddenstatic");
                $form->setType("libelle_voie", "hiddenstatic");
            } else { // ajouter
                //
                if ($this->f->is_option_code_voie_auto_enabled() === true) {
                    $form->setType("code", "hidden");
                }
            }
        } else { // supprimer
            $form->setType("code", "hiddenstatic");
        }
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        $form->setLib("code", __("Identifiant"));
        $form->setLib("libelle_voie", __("Libelle de la voie"));
        $form->setLib("cp", __("Code Postal"));
        $form->setLib("ville", __("Ville"));
    }

    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        $form->setOnchange("code", "this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_voie", "this.value=this.value.toUpperCase()");
        $form->setOnchange("cp", "javascript:VerifNum(this)");
        $form->setOnchange("ville", "this.value=this.value.toUpperCase()");
    }

    /**
     * Parametrage du formulaire - Taille des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setTaille(&$form, $maj) {
        //
        $form->setTaille("code", 6);
        $form->setTaille("libelle_voie", 50);
        $form->setTaille("cp", 5);
        $form->setTaille("ville", 50);
    }

    /**
     * Parametrage du formulaire - Valeur max des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setMax(&$form, $maj) {
        //
        $form->setMax("code", 6);
        $form->setMax("libelle_voie", 50);
        $form->setMax("cp", 5);
        $form->setMax("ville", 50);
    }

}


