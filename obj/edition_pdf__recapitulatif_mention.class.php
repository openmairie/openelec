<?php
/**
 * Ce script définit la classe 'edition_pdf__recapitulatif_mention'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__recapitulatif_mention' (edition_pdf).
 */
class edition_pdf__recapitulatif_mention extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * @return array
     */
    public function compute_pdf__recapitulatif_mention($params = array()) {
        $tour = "les_deux";
        $libelle_scrutin = "";
        /**
         *
         */
        //
        $dateelection = null;
        $dateelection1 = null;
        $dateelection2 = null;
        //
        if (is_array($params) === true
            && array_key_exists("dateelection1", $params) === true) {
            //
            $date = explode("/", $params['dateelection1']);
            if (sizeof($date) == 3
                && (checkdate($date[1], $date[0], $date[2]))) {
                $dateelection1 = $date[2]."-".$date[1]."-".$date[0];
            }
        }
        //
        if (is_array($params) === true
            && array_key_exists("dateelection2", $params) === true) {
            //
            $date = explode("/", $params['dateelection2']);
            if (sizeof($date) == 3 && (checkdate($date[1], $date[0], $date[2]))) {
                $dateelection2 = $date[2]."-".$date[1]."-".$date[0];
            }
        }
        //
        if ($dateelection1 === null
            && $dateelection2 === null) {
            //
            $dateelection1 = date("Y-m-d");
        }
        //
        if ($dateelection1 === null) {
            $dateelection = $dateelection2;
        } else {
            $dateelection = $dateelection1;
        }

        /**
         *
         */
        $liste = $_SESSION["liste"];
        $libelle_liste = $_SESSION["libelle_liste"];
        if (array_key_exists("liste", $params) === true) {
            $liste = $params["liste"];
            $ret = $this->f->get_one_result_from_db_query(sprintf(
                'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                DB_PREFIXE,
                $this->f->db->escapesimple($liste)
            ));
            $libelle_liste = $ret["result"];
        }
        //
        if (isset($params["tour"])) {
            $tour = $params["tour"];
        }
        if ($params["scrutin_tour1"] == $params["scrutin_tour2"]) {
            $tour = "un";
            $libelle_scrutin = sprintf(
                '%1$s du %2$s',
                $params["scrutin_libelle"],
                $this->f->formatdate($params["scrutin_tour1"])
            );
        } else {
            $libelle_scrutin = sprintf(
                '%1$s des %2$s et %3$s%4$s',
                $params["scrutin_libelle"],
                $this->f->formatdate($params["scrutin_tour1"]),
                $this->f->formatdate($params["scrutin_tour2"]),
                ($tour !== "les_deux" ? sprintf(
                    ' (Tour %s)',
                    ($tour === "un" ? "1" : "2")
                ) : "")
            );
        }
        if ($tour == "les_deux") {
            $dateelection1 = $params["scrutin_tour1"];
            $dateelection2 = $params["scrutin_tour2"];
        } elseif ($tour == "un") {
            $dateelection1 = $params["scrutin_tour1"];
        } elseif ($tour == "deux") {
            $dateelection1 = $params["scrutin_tour2"];
        }
        $inst_scrutin = $this->f->get_inst__om_dbform(array(
            "obj" => "reu_scrutin",
            "idx" => $params["scrutin"],
        ));
        //
        require_once "../obj/fpdf_fromdb_fromarray.class.php";
        //
        $pdf = new PDF("L", "mm", "A4");
        //
        $generic_document_header_left = sprintf(
            '%s - %s - (%s)(%s)(%s)',
            $this->f->collectivite["ville"],
            __("Récapitulatif des mentions"),
            $inst_scrutin->getVal($inst_scrutin->clePrimaire),
            $inst_scrutin->getVal('emarge_livrable_demande_id'),
            $inst_scrutin->getVal('emarge_livrable_demande_date')
        );
        $pdf->generic_document_header_left = $generic_document_header_left;
        //
        $generic_document_title = $libelle_scrutin." - ".$libelle_liste;
        $pdf->generic_document_title = $generic_document_title;
        //
        $pdf->SetMargins(10, 5, 10);
        //
        $pdf->AliasNbPages();

        // Édition PDF n°1
        //
        $pdf->init_header_pdffromarray();
        $tableau = $inst_scrutin->compute_stats__mentions_valides_par_bureau($params);
        $tableau["output"] = null;
        $pdf->tableau($tableau);

        // Édition PDF n°2
        //
        $conf = $this->get_vars_from_config_file_pdffromdb(
            "mention",
            $params
        );
        $pdf->add_edition_pdffromdb($conf);
        $pdf->reinit_header_pdffromdb();

        // Édition PDF n°3
        //
        $conf = $this->get_vars_from_config_file_pdffromdb(
            "procurationok",
            $params
        );
        $pdf->add_edition_pdffromdb($conf);
        $pdf->reinit_header_pdffromdb();

        // Édition PDF n°4
        //
        $conf = $this->get_vars_from_config_file_pdffromdb(
            "procurationnonok",
            $params
        );
        $pdf->add_edition_pdffromdb($conf);
        $pdf->reinit_header_pdffromdb();

        /**
         * OUTPUT
         */
        // Construction du nom du fichier
        $filename = date("Ymd-His");
        $filename .= "-recapitulatif";
        $filename .= "-mention";
        $filename .= "-".$_SESSION["collectivite"];
        $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
        $filename .= ".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
