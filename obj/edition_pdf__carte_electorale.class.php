<?php
/**
 * Ce script définit la classe 'edition_pdf__carte_electorale'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__carte_electorale' (edition_pdf).
 */
class edition_pdf__carte_electorale extends edition_pdf {

    /**
     * Édition PDF - Carte électorale / Carte d'électeur.
     *
     * > mode_edition
     *  - "traitement"
     *    > traitement
     *    > datetableau
     *    > datej5
     *  - "parbureau"
     *    > bureau_code
     *  - "electeur"
     *    > electeur_id
     *  - "commune"
     *  - "multielecteur"
     *    > electeur_ids
     * > multi
     *
     * @return array
     */
    public function compute_pdf__carte_electorale($params = array()) {
        //
        $libelle_commune = $this->f->collectivite["ville"];
        $liste = $_SESSION["liste"];
        $libelle_liste = $_SESSION["libelle_liste"];
        $titre_libelle_ligne1 = __("CARTES ÉLECTORALES");
        $titre_libelle_ligne2 = "";
        $bureau_code = "ALL";
        $bureau_libelle = "";
        $canton_libelle = "";
        $circonscription_libelle = "";
        $message = "";
        $modifications_sans_changement_bureau = false;
        $order_by_datenaissance = false;
        $tri = '';
        //
        $mode_edition = "";
        if (isset($params["mode_edition"])) {
            $mode_edition = $params["mode_edition"];
            if ($mode_edition == "traitement") {
                // Les nouvelles cartes electorales suites a un traitement
                //
                $titre = __("NOUVELLES CARTES ELECTORALES");
                //
                $traitement = "";
                if (isset($params["traitement"])) {
                    //
                    $traitement = $params["traitement"];
                    //
                    $datetableau = "";
                    if (isset($params["datetableau"])) {
                        $datetableau = $params["datetableau"];
                    }
                    //
                    $annee = (string)((int)substr($datetableau, 0, 4)-1)." - ".substr($datetableau, 0, 4);
                    //
                    if ($traitement == "j5") {
                        //
                        $datej5 = "";
                        if (isset($params["datej5"])) {
                            $datej5 = $params["datej5"];
                        }
                        //
                        $titre .= " ".__("SUITE AU TRAITEMENT J-5 DU");
                        $titre .= " ".$this->f->formatdate($datej5);
                    } elseif ($traitement == "annuel") {
                        //
                        $titre .= " ".__("SUITE AU TRAITEMENT ANNUEL DU");
                        $titre .= " ".$this->f->formatdate($datetableau);
                    }
                }
            } elseif ($mode_edition == "parbureau") {
                // Toutes les cartes electorales du bureau de vote
                $bureau_code = "";
                if (isset ($params['bureau_code'])) {
                    $bureau_code = $params['bureau_code'];
                    $message = sprintf(
                        __("%s\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),
                        $titre_libelle_ligne1,
                        $libelle_liste,
                        $bureau_code
                    );
                    $sql = sprintf(
                        'SELECT bureau.id FROM %1$sbureau WHERE bureau.om_collectivite=%2$s AND bureau.code=\'%3$s\'',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        $this->f->db->escapesimple($bureau_code)
                    );
                    $bureau_id = $this->f->db->getone($sql);
                    $this->f->isDatabaseError($bureau_id);
                    //
                    $inst_bureau = $this->f->get_inst__om_dbform(array(
                        "obj" => "bureau",
                        "idx" => $bureau_id,
                    ));
                    $bureau_libelle = $inst_bureau->getVal("libelle");
                    //
                    $inst_canton = $this->f->get_inst__om_dbform(array(
                        "obj" => "canton",
                        "idx" => $inst_bureau->getVal("canton"),
                    ));
                    $canton_libelle = $inst_canton->getVal("libelle");
                    //
                    $inst_circonscription = $this->f->get_inst__om_dbform(array(
                        "obj" => "circonscription",
                        "idx" => $inst_bureau->getVal("circonscription"),
                    ));
                    $circonscription_libelle = $inst_circonscription->getVal("libelle");
                }
                if (array_key_exists("liste", $params) === true) {
                    $liste = $params["liste"];
                    $ret = $this->f->get_one_result_from_db_query(sprintf(
                        'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                        DB_PREFIXE,
                        $this->f->db->escapesimple($liste)
                    ));
                    $libelle_liste = $ret["result"];
                }
                // TODO : Vérifier si le order_by_datenaissance est toujours utilisé et le supprimer sinon
                if (array_key_exists("order_by_datenaissance", $params) === true
                    && $params["order_by_datenaissance"] === true) {
                    //
                    $order_by_datenaissance = true;
                }
                if (array_key_exists("tri", $params) === true
                    && ! empty($params["tri"])) {
                    //
                    $tri = $params["tri"];
                }
            } elseif ($mode_edition == "electeur") {
                // La carte electorale d'un seul electeur
                $electeur_id = "";
                if (isset($params['electeur_id'])) {
                    $electeur_id = $params['electeur_id'];
                }
            } elseif ($mode_edition == "commune") {
                // Toutes les cartes electorales de la commune
                if (array_key_exists("liste", $params) === true) {
                    $liste = $params["liste"];
                    $ret = $this->f->get_one_result_from_db_query(sprintf(
                        'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                        DB_PREFIXE,
                        $this->f->db->escapesimple($liste)
                    ));
                    $libelle_liste = $ret["result"];
                }
                if (array_key_exists("order_by_datenaissance", $params) === true
                    && $params["order_by_datenaissance"] === true) {
                    //
                    $order_by_datenaissance = true;
                }
            } elseif ($mode_edition == "pardatedenaissance") {
                if (array_key_exists("liste", $params) === true) {
                    $liste = $params["liste"];
                    $ret = $this->f->get_one_result_from_db_query(sprintf(
                        'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                        DB_PREFIXE,
                        $this->f->db->escapesimple($liste)
                    ));
                    $libelle_liste = $ret["result"];
                }
                $datedenaissance_du = date("Y-m-d");
                if (array_key_exists("datedenaissance_du", $params) === true
                    && $params["datedenaissance_du"] != "") {
                    //
                    $datedenaissance_du = $this->f->formatdate($params["datedenaissance_du"], false);
                }
                $datedenaissance_au = date("Y-m-d");
                if (array_key_exists("datedenaissance_au", $params) === true
                    && $params["datedenaissance_au"] != "") {
                    //
                    $datedenaissance_au = $this->f->formatdate($params["datedenaissance_au"], false);
                }
            } elseif ($mode_edition == "multielecteur") {
                if (isset($params['electeur_ids'])) {
                    $electeur_ids = $params['electeur_ids'];
                }
            } elseif ($mode_edition == "lastone") {
                if (array_key_exists("liste", $params) === true) {
                    $liste = $params["liste"];
                    $ret = $this->f->get_one_result_from_db_query(sprintf(
                        'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
                        DB_PREFIXE,
                        $this->f->db->escapesimple($liste)
                    ));
                    $libelle_liste = $ret["result"];
                }
                $date_lastone = date("Y-m-d");
                if (array_key_exists("date", $params) === true
                    && $params["date"] != "") {
                    //
                    $date_lastone = $this->f->formatdate($params["date"], false);
                }
                $source_lastone = "oel";
                if (array_key_exists("source_lastone", $params) === true
                    && $params["source_lastone"] != "") {
                    //
                    $source_lastone = $params["source_lastone"];
                }
                if (array_key_exists("modifications_sans_changement_bureau", $params) === true
                    && $params["modifications_sans_changement_bureau"] === true) {
                    //
                    $modifications_sans_changement_bureau = true;
                }
                $datedenaissance_au = date("Y-m-d");
                if (array_key_exists("datedenaissance_au", $params) === true
                    && $params["datedenaissance_au"] != "") {
                    //
                    $datedenaissance_au = $this->f->formatdate($params["datedenaissance_au"], false);
                }
                if (array_key_exists("order_by_datenaissance", $params) === true
                    && $params["order_by_datenaissance"] === true) {
                    //
                    $order_by_datenaissance = true;
                }
            }
            //
            $multi = false;
            if (isset($params["multi"])) {
                $multi = $params["multi"];
            }
        }

        /**
         *
         */
        $sql = sprintf(
            'SELECT
                electeur.id,
                electeur.ine as ine,
                bureau.code as bureau_code,
                \'\' as canton,
                CASE electeur.liste
                    WHEN \'01\'
                        THEN electeur.numero_bureau::text
                    ELSE
                        \' \'
                END AS numero_bureau,
                bureau.libelle as bureau_libelle,
                CASE
                    WHEN bureau.surcharge_adresse_carte_electorale IS TRUE
                        THEN bureau.adresse1
                    ELSE
                        trim(concat(bureau.adresse_numero_voie, \' \', bureau.adresse_libelle_voie))
                END AS adr1,
                CASE
                    WHEN bureau.surcharge_adresse_carte_electorale IS TRUE
                        THEN bureau.adresse2
                    ELSE
                        trim(concat(bureau.adresse_complement1, \' \', bureau.adresse_complement2, \' \', bureau.adresse_lieu_dit))
                END AS adr2,
                CASE
                    WHEN bureau.surcharge_adresse_carte_electorale IS TRUE
                        THEN bureau.adresse3
                    ELSE
                        trim(concat(bureau.adresse_code_postal, \' \', bureau.adresse_ville, \' \', bureau.adresse_pays))
                END AS adr3,
                case 
                    when (electeur.nom_usage <> \'\' and electeur.nom_usage <> electeur.nom)
                        then (electeur.civilite ||\' \'||electeur.nom||\' - \'||electeur.nom_usage||\' \'||electeur.prenom ) 
                    else (electeur.civilite ||\' \'||electeur.nom||\' \'||electeur.prenom) 
                end as nomprenom, 
                case electeur.resident 
                    when \'Non\' then (
                        case electeur.numero_habitation 
                            when 0 then \'\' 
                            else (electeur.numero_habitation||\' \') 
                        end
                        ||
                        case electeur.complement_numero 
                            when \'\' then \'\' 
                            else (electeur.complement_numero||\' \') 
                        end
                        ||
                        electeur.libelle_voie) 
                    when \'Oui\' then electeur.adresse_resident end as adresse,
                case electeur.resident 
                    when \'Non\' then electeur.complement 
                    when \'Oui\' then electeur.complement_resident 
                end as complement,
                case electeur.resident 
                    when \'Non\' then voie.cp 
                    when \'Oui\' then electeur.cp_resident 
                end as voiecp,
                case electeur.resident 
                    when \'Non\' then voie.ville 
                    when \'Oui\' then electeur.ville_resident 
                end as voieville,
                (to_char(electeur.date_naissance,\'DD/MM/YYYY\')) as naissance,
                electeur.code_departement_naissance, 
                electeur.libelle_lieu_de_naissance,
                CASE electeur.liste
                    WHEN \'01\'
                        THEN \' \'
                    ELSE
                       (\'Nationalité : \'||libelle_nationalite)
                END AS nationalite
            FROM
                %1$selecteur
                LEFT JOIN %1$svoie ON (electeur.code_voie = voie.code and electeur.om_collectivite=voie.om_collectivite)
                LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id 
                LEFT JOIN %1$scanton ON bureau.canton=canton.id 
                LEFT JOIN %1$snationalite ON electeur.code_nationalite = nationalite.code
                %3$s
            WHERE
                electeur.om_collectivite=%2$s
            ',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            ($mode_edition == "traitement" ? sprintf(
                'LEFT JOIN %1$smouvement ON electeur.id=mouvement.electeur_id
                LEFT JOIN %1$sparam_mouvement on mouvement.types = param_mouvement.code',
                DB_PREFIXE
            ) : '')
        );
        //
        $sql_filter__liste = sprintf(
            ' AND electeur.liste=\'%s\' ',
            $liste
        );
        if ($liste == '04') {
            $sql_filter__liste = sprintf(
                ' AND (
                (
                    (
                        electeur.liste=\'03\' AND electeur.ine IN (SELECT el.ine from openelec.electeur as el where el.liste=\'02\' and el.om_collectivite=electeur.om_collectivite)
                    )
                    AND NOT 
                    (
                        electeur.liste=\'02\' AND electeur.ine IN (SELECT el.ine from openelec.electeur as el where el.liste=\'03\' and el.om_collectivite=electeur.om_collectivite)
                    )
                )
                OR (electeur.liste=\'03\' AND electeur.ine NOT IN (SELECT el.ine from openelec.electeur as el where el.liste=\'02\' and el.om_collectivite=electeur.om_collectivite))
                OR (electeur.liste=\'02\' AND electeur.ine NOT IN (SELECT el.ine from openelec.electeur as el where el.liste=\'03\' and el.om_collectivite=electeur.om_collectivite))
                )'
            );
        }
        //
        if ($mode_edition == "commune") {
            $sql .= $sql_filter__liste;
        } elseif ($mode_edition == "parbureau") {
            $sql .= $sql_filter__liste;
            // On filtre sur le bureau de vote selectionne
            $sql .= " AND bureau.code='".$this->f->db->escapesimple($bureau_code)."'";
        } elseif ($mode_edition == "electeur") {
            // On filtre sur l'electeur selectionne
            $sql .= " AND electeur.id='".$electeur_id."'";
        } elseif ($mode_edition == "multielecteur") {
            // On vérifie que la variable existe
            if (isset($electeur_ids) && is_array($electeur_ids)) {
                // On boucle sur la variable pour ajouter les identifiants d'électeurs
                $sql .= " AND ( ";
                foreach ($electeur_ids as $idel) {
                    $sql .= " electeur.id=".intval($idel)." or";
                }
                $sql = substr($sql, 0 , -2);
                $sql .= " ) ";
            } else {
                $sql .= " AND (1=2) ";
            }
        } elseif ($mode_edition == "pardatedenaissance") {
            $sql .= $sql_filter__liste;
            $sql .= " AND electeur.date_naissance >= '".$datedenaissance_du."' ";
            $sql .= " AND electeur.date_naissance <= '".$datedenaissance_au."' ";
        } elseif ($mode_edition == "lastone") {
            $sql .= $sql_filter__liste;
            if ($source_lastone == "oel") {
                $sql .= " AND electeur.ine IN (SELECT DISTINCT electeur.ine FROM ".DB_PREFIXE."electeur ";
                $sql .= " LEFT JOIN ".DB_PREFIXE."mouvement ON electeur.ine=mouvement.ine AND electeur.om_collectivite=mouvement.om_collectivite"; 
                $sql .= " LEFT JOIN ".DB_PREFIXE."param_mouvement on mouvement.types = param_mouvement.code";
                $sql .= " WHERE electeur.om_collectivite=".intval($_SESSION["collectivite"])." AND mouvement.etat='trs' AND mouvement.statut='accepte' ";
                $sql .= " AND mouvement.date_j5 >= '".$date_lastone."' ";
                if ($modifications_sans_changement_bureau !== true) {
                    $sql .= " AND (lower(param_mouvement.typecat) = 'inscription' OR (lower(param_mouvement.typecat) = 'modification' AND mouvement.ancien_bureau_de_vote_code <> mouvement.bureau_de_vote_code)) )";
                } else {
                    $sql .= " AND (lower(param_mouvement.typecat) = 'inscription' OR lower(param_mouvement.typecat) = 'modification') )";
                }
            } elseif ($source_lastone == "reu") {
                $sql .= " AND electeur.ine IN (SELECT reu_sync_listes.numero_d_electeur::integer FROM ".DB_PREFIXE."reu_sync_listes ";
                $sql .= "  where ((to_date(SUBSTRING(reu_sync_listes.date_d_inscription, 1, 10), 'DD/MM/YYYY') > '".$date_lastone."' ) or (to_date(SUBSTRING(reu_sync_listes.date_modification, 1, 10), 'DD/MM/YYYY') > '".$date_lastone."')))";
            }
            $sql .= " AND electeur.date_naissance <= '".$datedenaissance_au."' ";
        } elseif ($mode_edition == "traitement") {
            $sql .= $sql_filter__liste;
            // On filtre sur les mouvements de la date de tableau selectionnee
            $sql .= " and mouvement.date_tableau = '".$datetableau."'";
            // On filtre sur les mouvements traites lors du traitement selectionne
            $sql .= " and mouvement.tableau = '".$traitement."' ";
            // Si c'est un traitement J5
            if ($traitement == "j5") {
                // On filtre sur les mouvements traites a la date selectionnee
                $sql .= " and mouvement.date_j5 = '".$datej5."'";
            }
            // Filtre sur le paramètre d'impression de la carte d'électeur
            // dans la table param_mouvement
            $sql .= " and (";
            // On filtre uniquement les mouvements paramétrés pour TOUJOURS
            // éditer une nouvelle carte electorale
            $sql .= " param_mouvement.edition_carte_electeur = '1' ";
            // OU
            $sql .= " or ";
            // On filtre uniquement les mouvements paramétrés pour EN CAS DE CHANGEMENT DE BUREAU
            // pour éditer une nouvelle carte electorale
            $sql .= "
            (
                param_mouvement.edition_carte_electeur = '2'
                AND mouvement.bureau_de_vote_code<>mouvement.ancien_bureau_de_vote_code
            ) ";
            //
            $sql .= " ) ";
        }

        // ORDER BY
        $sql .= " ORDER BY ";
        // 
        if ($order_by_datenaissance == true ||
            (! empty($tri) && $tri === 'date_naissance')) {
            $sql .= "electeur.date_naissance, ";
        }
        if ($tri === 'voie') {
            $sql .= "electeur.libelle_voie, electeur.numero_habitation, ";
        }
        $sql .= " withoutaccent(lower(electeur.nom)), withoutaccent(lower(electeur.prenom)) ";


        require_once "../obj/fpdf_carte_electorale.class.php";
        $pdf = new PDF('L', 'mm', 'A4');
        $pdf->SetFont('Arial', '', 7);
        // Fixe la couleur pour toutes les opérations de tracé
        $pdf->SetDrawColor(30, 7, 146);
        $pdf->SetMargins(10, 10, 10);
        $pdf->SetAutoPageBreak(false);
        $pdf->AddPage();
        $pdf->SetDisplayMode('real', 'single');
        // Barcode
        if ($this->f->is_option_code_barre_enabled() === true) {
            $pdf->barcode_enabled = true;
            $code_barre_pos = $this->f->get_parametrage_code_barre();
            $pdf->barcode_x = $code_barre_pos["pos_x"];
            $pdf->barcode_y = $code_barre_pos["pos_y"];
            if($code_barre_pos["show_bureau_code"] == "true") {
                $pdf->show_bureau_code_enabled = true;
            } else {
                $pdf->show_bureau_code_enabled = false;
            }
        }
        // Date édition
        if ($this->f->is_option_carte_electorale_date_enabled() === true) {
            $pdf->carte_electorale_date_enabled = true;
            $format_date_edition = $this->f->get_parametrage_carte_electorale_date();
            $pdf->carte_electorale_date = date($format_date_edition["format"]);
        }

        if ($this->f->is_option_carte_electorale_lieu_naissance_enabled() === false) {
            $pdf->carte_electorale_lieu_naissance_enabled = false;
        } else {
            $pdf->carte_electorale_lieu_naissance_enabled = true;
        }

        $parametrage_carte_electorale = $this->f->get_parametrage_carte_electorale();
        if (in_array($parametrage_carte_electorale["modele"], array("2019", "2022")) === true) {
            $pdf->modele = $parametrage_carte_electorale["modele"];
        }
        if ($parametrage_carte_electorale["debug"] === "level1"
            || $parametrage_carte_electorale["debug"] === "level2"
            || $parametrage_carte_electorale["debug"] === "level3") {
            //
            $pdf->debug = $parametrage_carte_electorale["debug"];
        }

        // Variable de developpement pour afficher les bordures de toutes les cellules
        $allborders = false;
        if ($pdf->get_debug_level() >= 3) {
            $allborders = true;
        }
        //
        $champs_compteur = array_fill(0, 5, 0);
        $img = array();
        $param = array(
            0,   //  0 = margin left
            0,   //  1 = margin top
            0,   //  2 = x space
            -1,  //  3 = y space
            2,   //  4 = x number
            2,   //  5 = y number
            148, //  6 = width
            105, //  7 = height
            3,   //  8 = char size
            3.5, //  9 = line height
            0,   // 10 = cpt x
            0,   // 11 = cpt y
            10,  // 12 = 
            $allborders,   // 13 = cadre
            $allborders,   // 14 = cadre zone
        );
        // Zone - Cachet de la mairie
        // Si un paramètre non vide "carte_electorale_texte_cachet_de_la_mairie existe
        // alors on positionne celui là sinon c'est le paramètre "ville" qui est postionné
        // L'objectif de cette possible surcharge est de pouvoir aujster le libellé pour
        // gérer les sauts de ligne en plein milieu des mots
        $texte_cachet_de_la_mairie = $this->f->getParameter("ville");
        if ($this->f->getParameter("carte_electorale_texte_cachet_de_la_mairie") != "") {
            $texte_cachet_de_la_mairie = $this->f->getParameter("carte_electorale_texte_cachet_de_la_mairie");
        }
        $texte = array(
            array($this->f->collectivite['maire'], 33, 90, 40, 0, 9),
            array($texte_cachet_de_la_mairie, 79, 90, 30, 0, 9),
        );
        if ($pdf->modele == "2019") {
            $champs = array(
                "numero_bureau" => array('', '', array(133, 48, 15, 1, 0), 0),
                "canton" => array('', '', array(25, 5, 114, 0, 9), 0),
                "bureau_code" => array('', '', array(8, 48, 15, 1, 0), 0),
                "id" => array('', '', array(800, 800, 0, 0, 0), 0),
                "bureau_libelle" => array('', '', array(25, 8, 114, 0, 9), 0),
                "adr1" => array('', '', array(25, 11, 114, 0, 9), 0),
                "adr2" => array('', '', array(25, 14, 114, 0, 9), 0),
                "adr3" => array('', '', array(25, 17, 114, 0, 9), 0),
                "nomprenom" => array('', '', array(25, 40, 107, 1, 10), 0),
                "adresse" => array('', '', array(25, 48, 107, 1, 10), 0),
                "complement" => array('', '', array(25, 52, 107, 1, 10), 0),
                "voiecp" => array('', '', array(25, 56, 15, 1, 10), 0),
                "voieville" => array('', '', array(40, 56, 92, 1, 10), 0),
                "ine" => array('', '', array(81, 62, 30, 1, 10), 0),
                "naissance" => array('', '', array(3, 71, 28, 1, 9), 0),
                "code_departement_naissance" => array('', '', array(40, 71, 15, 1, 9), 0),
                "libelle_lieu_de_naissance" => array('', '', array(60, 71, 85, 1, 9), 0),
                "nationalite" => array('', '', array(60, 74, 85, 1, 9), 0),
            );
        } else {
            $champs = array(
                "canton" => array('', '', array(25, 5, 114, 0, 9), 0),
                "id" => array('', '', array(-1000, -1000, 0, 0, 0), 0),
                "bureau_libelle" => array('', '', array(25, 8, 114, 0, 9), 0),
                "adr1" => array('', '', array(25, 11, 114, 0, 9), 0),
                "adr2" => array('', '', array(25, 14, 114, 0, 9), 0),
                "adr3" => array('', '', array(25, 17, 114, 0, 9), 0),
                "nomprenom" => array('', '', array(25, 40, 107, 1, 10), 0),
                "adresse" => array('', '', array(25, 48, 107, 1, 10), 0),
                "complement" => array('', '', array(25, 52, 107, 1, 10), 0),
                "voiecp" => array('', '', array(25, 56, 15, 1, 10), 0),
                "voieville" => array('', '', array(40, 56, 92, 1, 10), 0),
                "bureau_code" => array('', '', array(58, 62, 15, 1, 0), 0),
                "nationalite" => array('', '', array(75, 62, 70, 1, 9), 0),
                "ine" => array('', '', array(20, 77, 30, 1, 10), 0),
                "naissance" => array('', '', array(83, 74, 24, 1, 9), 0),
                "numero_bureau" => array('', '', array(123, 74, 15, 1, 0), 0),
                "code_departement_naissance" => array('', '', array(-1000, -1000, 15, 1, 9), 0),
                "libelle_lieu_de_naissance" => array('', '', array(-1000, -1000, 85, 1, 9), 0),                
            );
        }

        //************************PAGE DEBUT******************
        $pagedebut=array();
        if (!isset($annee)) {
            $annee=(string)((int)date("Y")-1)." - ".date("Y");
        }
        $page_content_width = 277;
        //-------------------------------------------------------------------//
        // TITRE (PAGE HEADER)
        $page_header = $this->get_page_header_config(array(
            "allborders" => $allborders,
            "page_content_width" => $page_content_width,
            "titre_libelle_ligne1" => $titre_libelle_ligne1,
            "titre_libelle_ligne2" => $titre_libelle_ligne2,
            "libelle_commune" => $libelle_commune,
            "libelle_liste" => $libelle_liste,
            "bureau_code" => $bureau_code,
            "bureau_libelle" => $bureau_libelle,
            "canton_libelle" => $canton_libelle,
            "circonscription_libelle" => $circonscription_libelle,
            "page_header_line_height" => 4,
        ));
        if ($mode_edition == "commune") {
            //
            $pagedebut = array_merge(
                $page_header,
                array(
                    array(
                        " ",
                        $page_content_width, 20, 1, ($allborders == true ? 1 : "B"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                    array(
                        " ",
                        $page_content_width, 1, 1, ($allborders == true ? 1 : "T"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                    array(
                        $titre_libelle_ligne1,
                        $page_content_width, 10, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',18,1,0),
                    array(
                        $titre_libelle_ligne2,
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                    array(
                        "",
                        $page_content_width, 5, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                    array(
                        sprintf(__("Commune : %s"), $libelle_commune),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        sprintf(__("Liste : %s"), $libelle_liste),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        ($bureau_code != "ALL" ? sprintf(__("Bureau : %s - %s"), $bureau_code, $bureau_libelle) : ""),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        ($canton_libelle != "" ? sprintf(__("Canton : %s"), $canton_libelle) : ""),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        ($circonscription_libelle != "" ? sprintf(__("Circonscription : %s"), $circonscription_libelle) : ""),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        " ",
                        $page_content_width, 10, 1, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                )
            );
        } elseif ($mode_edition == "traitement") {
            //
            $pagedebut = array_merge(
                $page_header,
                array(
                    array(
                        " ",
                        $page_content_width, 20, 1, ($allborders == true ? 1 : "B"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                    array(
                        " ",
                        $page_content_width, 1, 1, ($allborders == true ? 1 : "T"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                    array(
                        $titre_libelle_ligne1,
                        $page_content_width, 10, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',18,1,0),
                    array(
                        (isset($titre) ? $titre : __('CARTES ELECTORALES DES NOUVEAUX ELECTEURS')),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                    array(
                        "",
                        $page_content_width, 5, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                    array(
                        sprintf(__("Commune : %s"), $libelle_commune),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        sprintf(__("Liste : %s"), $libelle_liste),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        ($bureau_code != "ALL" ? sprintf(__("Bureau : %s - %s"), $bureau_code, $bureau_libelle) : ""),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        ($canton_libelle != "" ? sprintf(__("Canton : %s"), $canton_libelle) : ""),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        ($circonscription_libelle != "" ? sprintf(__("Circonscription : %s"), $circonscription_libelle) : ""),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        " ",
                        $page_content_width, 10, 1, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                )
            );
        } elseif ($mode_edition == "parbureau") {
            //
            $pagedebut = array_merge(
                $page_header,
                array(
                    array(
                        " ",
                        $page_content_width, 20, 1, ($allborders == true ? 1 : "B"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                    array(
                        " ",
                        $page_content_width, 1, 1, ($allborders == true ? 1 : "T"), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
                    array(
                        $titre_libelle_ligne1,
                        $page_content_width, 10, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','B',18,1,0),
                    array(
                        $titre_libelle_ligne2,
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                    array(
                        "",
                        $page_content_width, 5, 1, ($allborders == true ? 1 : 0), 'C','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                    array(
                        sprintf(__("Commune : %s"), $libelle_commune),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        sprintf(__("Liste : %s"), $libelle_liste),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        ($bureau_code != "ALL" ? sprintf(__("Bureau : %s - %s"), $bureau_code, $bureau_libelle) : ""),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        ($canton_libelle != "" ? sprintf(__("Canton : %s"), $canton_libelle) : ""),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        ($circonscription_libelle != "" ? sprintf(__("Circonscription : %s"), $circonscription_libelle) : ""),
                        $page_content_width, 6, 1, ($allborders == true ? 1 : 0), 'L','0','0','0','255','255','255',array(0,0),0,'','B',12,1,0),
                    array(
                        " ",
                        $page_content_width, 10, 1, ($allborders == true ? 1 : 0),'R','0','0','0','255','255','255',array(0,0),0,'','NB',14,1,0),
                )
            );
        } elseif ($mode_edition == "electeur" or $mode_edition == "multielecteur") {
            //
            $pagedebut=array();
        }
        //
        $paramPageDebut = array(7, 7, 7, 1, 1, 10, 10, 10, 10, 0, 0);
        //
        $pdf->Table_position(
            $sql,
            $this->f->db,
            $param,
            $champs,
            $texte,
            $champs_compteur,
            $img,
            $pagedebut,
            $paramPageDebut
        );

        /**
         * OUTPUT
         */
        //
        if ($mode_edition == "commune" && $multi == false && $output = "file") {
            $filename = "carteelecteur-".$_SESSION["collectivite"]."-".$liste.".pdf";
        } else {
            $filename = date("Ymd-His");
            $filename .= "-carteelecteur";
            $filename .= "-".$_SESSION["collectivite"];
            $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
            $filename .= "-liste".$liste;
            if ($mode_edition == "parbureau") {
                $filename .= "-bureau".$bureau_code;
            }
            $filename .= ".pdf";
        }
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
