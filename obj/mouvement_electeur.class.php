<?php
/**
 * Ce script définit la classe 'mouvement_electeur'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/mouvement.class.php";

/**
 * Définition de la classe 'mouvement_electeur' (om_dbform).
 *
 * Surcharge de la classe 'mouvement'.
 */
class mouvement_electeur extends mouvement {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        $this->class_actions = array();
    }

    /**
     * VIEW - view_consulter_from_idxelecteur.
     *
     * @return void
     */
    public function view_consulter_from_idxelecteur() {
        $this->checkAccessibility();
        if (!isset($_GET['idxelecteur'])) {
            return;
        }
        if ($this->get_absolute_class_name() == "modification") {
            // Recuperation d'un mouvement actif a la date de tableau en cours
            // correspondant a l'id_electeur passe en parametre
            $sql_idx = sprintf(
                'SELECT id FROM %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm on m.types=pm.code where typecat=\'Modification\' and m.electeur_id=%3$s and etat=\'actif\'',
                DB_PREFIXE,
                $this->f->getParameter("datetableau"),
                intval($_GET['idxelecteur'])
            );
        } elseif ($this->get_absolute_class_name() == "radiation") {
            // Recuperation d'un mouvement actif a la date de tableau en cours
            // correspondant a l'id_electeur passe en parametre
            $sql_idx = sprintf(
                'SELECT id FROM %1$smouvement as m INNER JOIN %1$sparam_mouvement as pm on m.types=pm.code where typecat=\'Radiation\' and m.electeur_id=%3$s and etat=\'actif\'',
                DB_PREFIXE,
                $this->f->getParameter("datetableau"),
                intval($_GET['idxelecteur'])
            );
        }
        $res = $this->f->db->getOne($sql_idx);
        // Verification de l'erreur
        $this->f->isDatabaseError($res);
        // Si un mouvement existe alors on l'affecte en idx
        if ($res != null) {
            // Si les variables arrivent en $_GET
            if (isset($_GET['nom']) or isset($_GET['exact']) or isset($_GET['datenaissance'])
                or isset($_GET['prenom']) or isset($_GET['marital'])) {
                // Initialisation des variables du formulaire
                (isset($_GET['nom']) ? $nom = $_GET['nom'] : $nom = "");
                (isset($_GET['exact']) && $_GET['exact']==true ? $exact = true : $exact = false);
                (isset($_GET['prenom']) ? $prenom = $_GET['prenom'] : $prenom = "");
                (isset($_GET['marital']) ? $marital = $_GET['marital'] : $marital = "");
                (isset($_GET['datenaissance']) ? $datenaissance = $_GET['datenaissance'] : $datenaissance = "");
            }
            (isset($_GET['origin']) ? $origin = $_GET['origin'] : $origin = null);
            (!isset($exact) ? $exact = true : $exact = $exact);

            /// Decoche recherche exact si * detecte
            if (substr($nom,strlen($nom)-1,1) == '*' && strlen($nom) >=2){
                $nom = str_replace("*","",$nom);
                $exact = false;
            }
            if (substr($prenom,strlen($prenom)-1,1) == '*' && strlen($nom) >=2) {
                $prenom = str_replace("*","", $prenom);
                $exact = false;
            }
            if (substr($marital,strlen($marital)-1,1) == '*' && strlen($nom) >=2) {
                $marital = str_replace("*","", $marital);
                $exact = false;
            }

            // Condition d'erreur
            $error = ($nom == "" and $datenaissance == "" and $prenom == "" and $marital == "" ? true : false);
            //
            $params = "";
            if ($origin != null) {
                $params .= "&origin=".$origin;
            }
            $params .= "&nom=".urlencode($nom);
            $params .= ($exact == true ? "&exact=".$exact : "");
            $params .= "&prenom=".urlencode($prenom);
            $params .= "&marital=".urlencode($marital);
            $params .= "&datenaissance=".urlencode($datenaissance);
            header("Location:".OM_ROUTE_FORM."&obj=".$this->get_absolute_class_name()."&action=3&idx=".$res.$params);
            return;
        }
        return;
    }

    /**
     * VIEW - view_add_search_form.
     *
     * @return void
     */
    public function view_add_search_form() {
        $this->checkAccessibility();

        /**
         * Parametrage de la page
         */
        //
        $page = "electeur_search";
        //
        $onglet = __("Recherche d'electeur");
        //
        (isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "electeur_mouvement");
        switch ($this->get_absolute_class_name()) {
            //
            case "modification" :
                $ent = __("Saisie")." -> ".__("Modification");
                $description = __("Ce formulaire de recherche vous permet de saisir le nom ".
                                 "patronymique et/ou le nom d'usage/marital et/ou le prenom ".
                                 "et/ou la date de naissance de l'electeur sur lequel vous ".
                                 "souhaitez creer un mouvement de modification. En cliquant ".
                                 "sur le bouton vous obtiendrez la liste des electeurs ".
                                 "correspondants a votre recherche.");
                $bouton = __("Rechercher l'electeur a modifier");
                break;
            //
            case "radiation" :
                $ent = __("Saisie")." -> ".__("Radiation");
                $description = __("Ce formulaire de recherche vous permet de saisir le nom ".
                                 "patronymique et/ou le nom d'usage/marital et/ou le prenom ".
                                 "et/ou la date de naissance de l'electeur sur lequel vous ".
                                 "souhaitez creer un mouvement de radiation. En cliquant ".
                                 "sur le bouton vous obtiendrez la liste des electeurs ".
                                 "correspondants a votre recherche.");
                $bouton = __("Rechercher l'electeur a radier");
                break;
        }
        $action = OM_ROUTE_FORM."&obj=".$this->get_absolute_class_name()."&idx=0&action=101";

        /**
         * Initialisation des variables
         */
        // Initialisation des variables du formulaire
        $nom = "";
        $prenom = "";
        $marital = "";
        $datenaissance = "";


        // Si les variables arrivent en $_GET
        if (isset($_GET['nom']) or isset($_GET['exact']) or isset($_GET['datenaissance'])
            or isset($_GET['prenom']) or isset($_GET['marital'])) {
            // Initialisation des variables du formulaire
            (isset($_GET['nom']) ? $nom = $_GET['nom'] : $nom = "");
            (isset($_GET['exact']) && $_GET['exact']==true ? $exact = true : $exact = false);
            (isset($_GET['prenom']) ? $prenom = $_GET['prenom'] : $prenom = "");
            (isset($_GET['marital']) ? $marital = $_GET['marital'] : $marital = "");
            (isset($_GET['datenaissance']) ? $datenaissance = $_GET['datenaissance'] : $datenaissance = "");
        }
        // Si les variables arrivent en $_POST
        if (isset($_POST['nom'])or isset($_POST['exact']) or isset($_POST['datenaissance'])
            or isset($_POST['prenom']) or isset($_POST['marital'])) {
            // Initialisation des variables du formulaire
            (isset($_POST['nom']) ? $nom = $_POST['nom'] : $nom = "");
            (isset($_POST['exact']) && $_POST['exact']==true ? $exact = true : (isset($exact) ? $exact = $exact : $exact = false));
            (isset($_POST['prenom']) ? $prenom = $_POST['prenom'] : $prenom = "");
            (isset($_POST['marital']) ? $marital = $_POST['marital'] : $marital = "");
            (isset($_POST['datenaissance']) ? $datenaissance = $_POST['datenaissance'] : $datenaissance = "");
        }

        (!isset($exact) ? $exact = true : $exact = $exact);

        /// Decoche recherche exact si * detecte
        if (substr($nom,strlen($nom)-1,1) == '*' && strlen($nom) >=2){
            $nom = str_replace("*","",$nom);
            $exact = false;
        }
        if (substr($prenom,strlen($prenom)-1,1) == '*' && strlen($nom) >=2) {
            $prenom = str_replace("*","", $prenom);
            $exact = false;
        }
        if (substr($marital,strlen($marital)-1,1) == '*' && strlen($nom) >=2) {
            $marital = str_replace("*","", $marital);
            $exact = false;
        }

        // Condition d'erreur
        $error = ($nom == "" and $datenaissance == "" and $prenom == "" and $marital == "" ? true : false);

        /**
         * Validation du formulaire
         */
        //
        if (isset($_POST[$page.'_form_action_valid'])) {
            //
            if (!$error) {
                //
                $params = "&nom=".urlencode($nom);
                $params .= ($exact == true ? "&exact=".$exact : "");
                $params .= "&prenom=".urlencode($prenom);
                $params .= "&marital=".urlencode($marital);
                $params .= "&datenaissance=".urlencode($datenaissance);
                //
                header ("location:".OM_ROUTE_FORM."&obj=".$this->get_absolute_class_name()."&idx=0&action=102".$params);
            }
        }

        /**
         * Parametrage du formulaire
         */
        //
        $validation = 0;
        $maj = 0;
        $champs = array("nom", "exact", "marital", "prenom", "datenaissance");
        //
        $form = $this->f->get_inst__om_formulaire(array(
            "validation" => $validation,
            "maj" => $maj,
            "champs" => $champs,
        ));
        //
        $form->setLib("nom", __("Nom patronymique"));
        $form->setType("nom", "text");
        $form->setTaille("nom", 40);
        $form->setMax("nom", 60);
        $form->setVal("nom", $nom);
        $form->setOnchange("nom", "this.value=this.value.toUpperCase()");
        //
        $form->setLib("exact", __("Recherche exacte"));
        $form->setType("exact", "checkbox");
        $form->setTaille("exact", 3);
        $form->setMax("exact", 3);
        $form->setVal("exact", $exact);
        //
        $form->setLib("marital", __("Nom d'usage ou nom marital"));
        $form->setType("marital", "text");
        $form->setTaille("marital", 40);
        $form->setMax("marital", 60);
        $form->setVal("marital", $marital);
        $form->setOnchange("marital", "this.value=this.value.toUpperCase()");
        //
        $form->setLib("prenom", __("Prenom"));
        $form->setType("prenom", "text");
        $form->setTaille("prenom", 40);
        $form->setMax("prenom", 60);
        $form->setVal("prenom", $prenom);
        $form->setOnchange("prenom", "this.value=this.value.toUpperCase()");
        //
        $form->setLib("datenaissance", __("Date de Naissance"));
        $form->setType("datenaissance", "date");
        $form->setTaille("datenaissance", 10);
        $form->setMax("datenaissance", 10);
        $form->setVal("datenaissance", $datenaissance);
        $form->setOnchange("datenaissance", "fdate(this)");
        //
        $form->setBloc("nom", "D", "", "group first-fix-width");
        $form->setBloc("exact", "F");
        $form->setBloc("marital", "DF", "", "group first-fix-width");
        $form->setBloc("prenom", "DF", "", "group first-fix-width");
        $form->setBloc("datenaissance", "DF", "", "group first-fix-width");

        /**
         * Affichage
         */
        // Affichage du message d'erreur si besoin
        if (isset($_POST[$page.'_form_action_valid'])) {
            //
            if ($error) {
                $message_class = "error";
                $message = __("Vous devez saisir au moins un critere de recherche.");
                $this->f->displayMessage($message_class, $message);
            }
        }

        // Instructions et description du contenu de l'onglet
        $this->f->displayDescription($description);

        // Ouverture de la balise - Formulaire
        echo "\n<div id=\"".$page."\" class=\"formulaire\">\n";
        echo "<form method=\"post\" id=\"".$page."_form\" ";
        echo "name=\"".$page."_form\" ";
        echo "action=\"".$action."\">\n";

        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, $validation, false, false);
        $form->enpied();

        // Ouverture de la balise - Controles du formulaire
        echo "\t<div class=\"formControls\">\n";
        // Bouton
        echo "\t\t<input name=\"".$page."_form.action.valid\" ";
        echo "value=\"".$bouton."\" ";
        echo "type=\"submit\" class=\"boutonFormulaire om-search-button\" />\n";
        // Lien retour
        echo "<a class=\"retour\" title=\"".__("Retour")."\" ";
        echo "href=\"".OM_ROUTE_DASHBOARD."\">";
        echo __("Retour");
        echo "</a>";
        // Fermeture de la balise - Controles du formulaire
        echo "\t</div>\n";

        // Fermeture de la balise - Formulaire
        echo "</form>\n";
        echo "</div>\n";
    }

    /**
     * CONDITION - is_idxelecteur_exists.
     *
     * @return boolean
     */
    public function is_idxelecteur_exists() {
        $idx_electeur = null;
        if ($this->getParameter("idxelecteur") != null
            && $this->getParameter("idxelecteur") != "]") {
            //
            $idx_electeur = $this->getParameter("idxelecteur");
            $inst_electeur = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => intval($idx_electeur),
            ));
            if ($inst_electeur->exists() === true) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * VIEW - view_add_search_form_results.
     *
     * @return void
     */
    public function view_add_search_form_results() {
        $this->checkAccessibility();

        // Initialisation des paramètres
        $params = array(
            // Nom de l'objet metier
            "obj" => array(
                "default_value" => "",
            ),
            // Premier enregistrement a afficher
            "premier" => array(
                "default_value" => 0,
            ),
            // Colonne choisie pour le tri
            "tricol" => array(
                "default_value" => "",
            ),
            // Id unique de la recherche avancee
            "advs_id" => array(
                "default_value" => "",
            ),
            // Valilite des objets a afficher
            "valide" => array(
                "default_value" => "",
            ),
            "contentonly" => array(
                "default_value" => null,
            ),
            "mode" => array(
                "default_value" => null,
            ),
            "origin" => array(
                "default_value" => null,
            ),
            "nom" => array(
                "default_value" => "",
            ),
            "prenom" => array(
                "default_value" => "",
            ),
            "marital" => array(
                "default_value" => "",
            ),
            "datenaissance" => array(
                "default_value" => "",
            ),
        );
        foreach ($this->f->get_initialized_parameters($params) as $key => $value) {
            ${$key} = $value;
        }
        if ($obj == "" && $origin != null) {
            $obj = $origin;
        }

        //
        $standard_script_path = "../sql/".OM_DB_PHPTYPE."/electeur_".$obj.".inc.php";

        /**
         * Verification des parametres
         */
        if (strpos($obj, "/") !== false
            || (file_exists($standard_script_path) === false)) {
            $class = "error";
            $message = __("L'objet est invalide.");
            $this->addToMessage($class, $message);
            $this->setFlag(null);
            $this->display();
            die();
        }

        /**
         *
         */
        //
        (isset($_GET['exact']) && $_GET['exact'] == true ? $exact = true : $exact = false);

        //
        if ($datenaissance != "")
        {
            if (preg_match ("#([0-9]{4})-([0-9]{2})-([0-9]{2})#", $datenaissance, $regs))
            {
                $datenaissanceFR = $regs[3]."/".$regs[2]."/".$regs[1];
            }
            elseif (preg_match ("#([0-9]{2})/([0-9]{2})/([0-9]{4})#", $datenaissance, $regs))
            {
                $datenaissanceFR = $datenaissance;
                $datenaissance = $regs[3]."-".$regs[2]."-".$regs[1];
            }
        }

        // Aucune action dans les listings
        $tab_actions = array(
            'corner' => array(),
            'left' => array(
                "ajouter" => array(
                    "lien" => OM_ROUTE_FORM."&obj=".$this->get_absolute_class_name()."&amp;origin=electeur_".$this->get_absolute_class_name()."&amp;action=0&amp;idxelecteur=",
                    "id" => "&retour=form&amp;nom=".(isset($nom) ? $nom : "").
                        "&amp;exact=".(isset($exact) ? $exact : "").
                        "&amp;prenom=".(isset($prenom) ? $prenom : "").
                        "&amp;premier=".(isset($premier) ? $premier : "").
                        "&amp;datenaissance=".(isset($datenaissance) ? $datenaissance : "").
                        "&amp;marital=".(isset($marital) ? $marital : "").
                        "&amp;tricol=".(isset($tricol) ? $tricol : "").
                        "&amp;premier=".(isset($premier) ? $premier : 0),
                    "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix ".$this->get_absolute_class_name()."-16\" title=\"".__("Creer un mouvement de ".$this->get_absolute_class_name()."")."\">".__("Creer un mouvement de ".$this->get_absolute_class_name()."")."</span>",
                ),
                "consulter" => array(
                    "lien" => OM_ROUTE_FORM."&obj=".$this->get_absolute_class_name()."&amp;origin=electeur_".$this->get_absolute_class_name()."&amp;action=4&amp;idx=0&amp;idxelecteur=",
                    "id" => "&amp;nom=".(isset($nom) ? $nom : "").
                            "&amp;exact=".(isset($exact) ? $exact : "").
                            "&amp;prenom=".(isset($prenom) ? $prenom : "").
                            "&amp;premier=".(isset($premier) ? $premier : "").
                            "&amp;datenaissance=".(isset($datenaissance) ? $datenaissance : "").
                            "&amp;marital=".(isset($marital) ? $marital : "").
                            "&amp;tricol=".(isset($tricol) ? $tricol : "").
                            "&amp;premier=".(isset($premier) ? $premier : 0),
                    "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix consult-16\" title=\"".__("Accéder au mouvement en cours")."\">".__("Accéder au mouvement en cours")."</span>",
                ),
            ),
            'content' => array(),
            'specific_content' => array(),
        );

        // Inclusion du script [sql/<OM_DB_PHPTYPE>/<OBJ>.inc.php]
        $custom_script_path = $this->f->get_custom("tab", $obj);
        if ($custom_script_path !== null) {
            require_once $custom_script_path;
        } elseif (file_exists($standard_script_path) === true) {
            require_once $standard_script_path;
        }

        /**
         *
         */
        echo '<div id="tab-'.$this->get_absolute_class_name().'-add_search_form_results">';
        //
        if ($edition != "") {
            $edition = OM_ROUTE_MODULE_EDITION."&obj=".$edition;
        }
        //
        if (!isset($om_validite) or $om_validite != true) {
            $om_validite = false;
        }
        //
        if (!isset($options)) {
            $options = array();
        }
        //
        $tb = $this->f->get_inst__om_table(array(
            "aff" => OM_ROUTE_FORM."&idx=0&action=102",
            "table" => $table,
            "serie" => $serie,
            "champAffiche" => $champAffiche,
            "champRecherche" => $champRecherche,
            "tri" => $tri,
            "selection" => $selection,
            "edition" => $edition,
            "options" => $options,
            "advs_id" => $advs_id,
            "om_validite" => $om_validite,
        ));
        //
        $params = array(
            "obj" => $this->get_absolute_class_name(),
            "premier" => $premier,
            "tricol" => $tricol,
            "valide" => $valide,
            "advs_id" => $advs_id,
            "nom" => $nom,
            "prenom" => $prenom,
            "marital" => $marital,
            "datenaissance" => $datenaissance,
            "origin" => "electeur_".$obj,
        );
        if ($exact == true) {
            $params["exact"] = 1;
        }
        //
        $tb->display($params, $tab_actions, $this->f->db, "tab", false);
        //
        $params = "&amp;nom=".urlencode($nom);
        $params .= ($exact == true ? "&amp;exact=".$exact : "");
        $params .= "&amp;prenom=".urlencode($prenom);
        $params .= "&amp;marital=".urlencode($marital);
        $params .= "&amp;datenaissance=".urlencode($datenaissance);
        //
        echo "\t<div class=\"formControls\">\n";
        echo "<a class=\"retour\" title=\"".__("Retour")."\" ";
        echo "href=\"".OM_ROUTE_FORM."&obj=".$this->get_absolute_class_name()."&idx=0&action=101".$params."\">";
        echo __("Retour");
        echo "</a>";
        echo "</div>\n";
        //
        echo "</div>";
    }

    /**
     *
     */
    function verifierAjout($val = array(), &$dnu1 = null) {
        // Verification du verrou electeur
        if ($this->isVerrouElecteur($val) == true) {
            $this->correct = false;
            $this->addToMessage(__("L'electeur a deja un mouvement en cours.")."<br/>");
        }
    }

    var $electeur = array();

    function setElecteurInfos() {

        //
        $idx_electeur = NULL;
        //
        if ($this->getParameter("idxelecteur") != NULL && $this->getParameter("idxelecteur") != "]") {
            //
            $idx_electeur = $this->getParameter("idxelecteur");
        } elseif ($this->getParameter("idx")) {
            //
            $sql = sprintf(
                'SELECT electeur_id FROM %1$smouvement WHERE id=%2$s',
                DB_PREFIXE,
                intval($this->getParameter("idx"))
            );
            $res = $this->f->db->getone($sql);
            $this->addToLog(
                __METHOD__."(): db->getone(\"".$sql."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            //
            $idx_electeur = $res;
        }
        //
        if ($idx_electeur != NULL) {
            //
            $sql = sprintf(
                'SELECT * FROM %1$selecteur WHERE electeur.id=%2$s',
                DB_PREFIXE,
                $idx_electeur
            );
            $res = $this->f->db->query($sql);
            $this->addToLog(
                __METHOD__."(): db->query(\"".$sql."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            //
            $results = array();
            while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                array_push($results, $row);
            }
            //
            if (count($results) == 1) {
                $this->electeur = $results[0];
            } else {

            }
        }

    }

    function getDataSubmit() {
        //
        $this->setElecteurInfos();
        //
        $datasubmit = parent::getDataSubmit();
        //
        $datasubmit .= "&amp;maj=".$this->getParameter("maj");
        //
        if ($this->getParameter("origin") == "ficheelecteur") {
            $datasubmit .= "&amp;idxelecteur=".$this->getParameter("idxelecteur");
            $datasubmit .= "&amp;origin=".$this->getParameter("origin");
        } elseif ($this->getParameter("nom") == "" && $this->getParameter("prenom") == "" && $this->getParameter("marital") == "" && $this->getParameter("datenaissance") == "") {
            //
        } else {
            $datasubmit .= "&amp;nom=".$this->getParameter("nom");
            $datasubmit .= "&amp;exact=".$this->getParameter("exact");
            $datasubmit .= "&amp;prenom=".$this->getParameter("prenom");
            $datasubmit .= "&amp;datenaissance=".$this->getParameter("datenaissance");
            $datasubmit .= "&amp;marital=".$this->getParameter("marital");
            $datasubmit .= "&amp;origin=".$this->getParameter("origin");
            $datasubmit .= "&amp;idxelecteur=".$this->getParameter("idxelecteur");
        }
        //
        return $datasubmit;
    }

    /**
     * Cette methode permet de remplir le tableau val attribut de
     * l'objet contenu dans $valE.
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        parent::setVal($form, $maj, $validation);
        //
        $valE = $this->electeur;
        if (count($valE) == 0) {
            return;
        }
        if ($validation==0) {
            if ($maj == 0) {
                // ******************************************
                // ajout => recuperation des donnees ELECTEUR
                // suivant requete electeur.form.inc
                // ******************************************
                $form->setVal('electeur_id', $valE['id']);
                $form->setVal('ine', $valE['ine']);
                $form->setVal('numero_bureau', $valE['numero_bureau']);
                $form->setVal('bureau', $valE['bureau']);
                $form->setVal('ancien_bureau', $valE['bureau']);
                $form->setVal('bureauforce', $valE['bureauforce']);
                // liste et traitement
                $form->setVal('liste', $valE['liste']);
                $form->setVal('date_tableau', $this->f->getParameter("datetableau"));
                $form->setVal('tableau',"annuel");
                $form->setVal('etat',"actif");
                // etat civil
                $form->setVal('civilite', $valE['civilite']);
                $form->setVal('sexe', $valE['sexe']);
                $form->setVal('nom', $valE['nom']);
                $form->setVal('nom_usage', $valE['nom_usage']);
                $form->setVal('prenom', $valE['prenom']);
                //naissance
                $form->setVal('date_naissance', $valE['date_naissance']);
                $form->setVal('code_departement_naissance', $valE['code_departement_naissance']);
                $form->setVal('libelle_departement_naissance', $valE['libelle_departement_naissance']);
                $form->setVal('code_lieu_de_naissance', $valE['code_lieu_de_naissance']);
                $form->setVal('libelle_lieu_de_naissance', $valE['libelle_lieu_de_naissance']);
                $naissance_type_saisie = $this->get_naissance_type_saisie(
                    $valE['code_departement_naissance'],
                    $valE['libelle_departement_naissance'],
                    $valE['code_lieu_de_naissance'],
                    $valE['libelle_lieu_de_naissance']
                );
                $form->setVal('naissance_type_saisie', $naissance_type_saisie);
                if ($naissance_type_saisie === "Etranger") {
                    $form->setVal('live_pays_de_naissance', $valE['code_departement_naissance']);
                } elseif ($naissance_type_saisie === "Ancien_dep_franc") {
                    $form->setVal('live_ancien_departement_francais_algerie', $valE['code_departement_naissance']);
                } elseif ($naissance_type_saisie === "France") {
                    $form->setVal('live_commune_de_naissance', $valE['code_lieu_de_naissance']);
                }
                $form->setVal('code_nationalite', $valE['code_nationalite']);
                // adresse dans la commune
                $form->setVal('numero_habitation', $valE['numero_habitation']);
                $form->setVal('libelle_voie', $valE['libelle_voie']);
                $form->setVal('code_voie', $valE['code_voie']);
                $form->setVal('complement_numero', $valE['complement_numero']) ;
                $form->setVal('complement', $valE['complement']) ;
                // recuperation adresse resident
                $form->setVal('resident', $valE['resident']);
                $form->setVal('adresse_resident', $valE['adresse_resident']);
                $form->setVal('complement_resident', $valE['complement_resident']);
                $form->setVal('cp_resident', $valE['cp_resident']);
                $form->setVal('ville_resident', $valE['ville_resident']);
                //
                $form->setVal('telephone', $valE['telephone']);
                $form->setVal('courriel', $valE['courriel']);
                //initialiser a vide
                $form->setVal('observation','') ;
                $form->setVal('utilisateur','');
                $form->setVal('types','');
                $form->setVal('date_modif','');
                $form->setVal('envoi_cnen','');
                $form->setVal('date_cnen','');
            }
        }
    }

    /**
     * TREATMENT - abandonner.
     *
     * @return boolean
     */
    function abandonner($val = array()) {
        $val = array(
            "electeur_id" => $this->getVal("electeur_id"),
        );
        if ($this->manageVerrouElecteur($this->getVal($this->clePrimaire), $val, false) !== true) {
            $this->correct = false;
            $this->addToMessage("Erreur lors du déverrouillage de l'électeur.");
            return $this->end_treatment(__METHOD__, false);
        }
        return parent::abandonner($val);
    }

    /**
     *
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {
        //
        $href = "";
        //
        if ($this->getParameter("origin") == "ficheelecteur") {
            $href .= OM_ROUTE_FORM;
            if ($this->exists() === true) {
                $href .= "&amp;obj=electeur&action=3&idx=".$this->getVal("electeur_id");
            } else {
                $href .= "&amp;obj=electeur&action=3&idx=".$this->getParameter("idxelecteur");
            }
        } elseif ($this->getParameter("nom") == "" && $this->getParameter("prenom") == "" && $this->getParameter("marital") == "" && $this->getParameter("datenaissance") == "") {
            parent::retour();
            return;
        } else {
            $href .= OM_ROUTE_FORM;
            $href .= "&amp;obj=".$this->get_absolute_class_name()."&amp;idx=0&amp;action=102";
            $href .= "&amp;nom=".$this->getParameter("nom");
            $href .= "&amp;prenom=".$this->getParameter("prenom");
            $href .= "&amp;exact=".$this->getParameter("exact");
            $href .= "&amp;datenaissance=".$this->getParameter("datenaissance");
            $href .= "&amp;marital=".$this->getParameter("marital");
            $href .= "&amp;origin=".$this->getParameter("origin");
        }
        $href .= "&amp;premier=".$this->getParameter("premier");
        $href .= "&amp;tricol=".$this->getParameter("tricol");
        $href .= "&amp;advs_id=".$this->getParameter("advs_id");
        $href .= "&amp;valide=".$this->getParameter("valide");
        //
        echo "\n<a class=\"retour\" ";
        echo "href=\"";
        echo $href;
        echo "\"";
        echo ">";
        //
        echo __("Retour");
        //
        echo "</a>\n";
    }

    // {{{ Gestion du verrou électeur

    /**
     * Cette méthode permet de vérifier l'état du verrou electeur, c'est-à-dire
     * de vérifier si l'électeur a déjà un mouvement en cours ou non.
     *
     * @param array $val
     */
    function isVerrouElecteur($val = array()) {
        //
        // IMPORTANT
        //
        // Ce verrou était vérifié sur le champ 'mouvement' de la table
        // electeur (si mouvement > 0 alors l'électeur était vérouillé)
        // mais le traitement de fin d'année remplit ce champ avec le code
        // du type de mouvement appliqué sur l'électeur ce qui est source
        // d'erreur. Donc désormais la vérification du verrou se fait sur le
        // champ 'typecat' de la table electeur (si typecat != "" alors
        // l'électeur est vérouillé) puisque depuis toujours les verrous
        // ont été positionnés sur ces deux champs etq ue le champ 'typecat'
        // est bien rempli à vide lors du traitement de fin d'année
        //
        //
        $verrou = NULL;
        //
        if (empty($val['electeur_id'])) {
            // Log
            $this->addToLog(__("Les paramètres ne sont pas corrects : val['electeur_id'] n'est pas défini."), DEBUG_MODE);
            //
            $verrou = NULL;
        } else {
            //
            $sql = sprintf(
                'SELECT electeur.mouvement, electeur.typecat FROM %1$selecteur WHERE electeur.id=%3$d AND electeur.om_collectivite=%2$d',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                intval($val['electeur_id'])
            );
            $res = $this->f->db->query($sql);
            $this->addToLog(
                __METHOD__."(): db->query(\"".$sql."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            //
            $results = array();
            while($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                array_push($results, $row);
            }
            //
            if (count($results) == 1) {
                // L'électeur n'est pas verouillé
                if ($results[0]['typecat'] == "") {
                    //
                    $verrou = false;
                } else { // L'électeur est verouillé
                    //
                    $verrou = true;
                }
            } else {
                // Log
                $this->addToLog(__("Probleme de coherence des donnees. Plusieurs identifiants identiques dans la table electeur."), DEBUG_MODE);
                //
                $verrou = NULL;
            }
        }
        //
        return $verrou;
    }

    /**
     * Cette méthode permet de marquer l'électeur comme vérouillé avec un
     * mouvement en cours et de supprimer ce verrou. En effet un électeur ne
     * peut pas avoir plus d'un mouvement en cours donc à chaque ajout de
     * mouvement (modification ou radiation), on marque l'électeur en
     * remplissant deux champs de la table électeur et à chaque suppression
     * de mouvement (modification ou radiation) on vide ces deux champs.
     *
     * @param string $id
     * @param array $val
     * @param boolean $mark
     *
     * @return boolean
     */
    function manageVerrouElecteur($id, $val, $mark = true) {
        // Initialisation du tableau
        $valF = array();
        if ($mark == true) {
            // Positionnement de l'identifiant du mouvement dans le champ mouvement
            $valF['mouvement'] = $this->valF[$this->clePrimaire];
            // Positionnement de la catégorie du mouvement dans le champ typecat
            $valF['typecat'] = $this->typeCat;
        } else {
            // Positionnement à vide dans le champ mouvement
            $valF['mouvement'] = "";
            // Positionnement à vide dans le champ typecat
            $valF['typecat'] = "";
        }
        $electeur_ids = array(
            $val['electeur_id'],
        );
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done") {
            $elec = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => $val['electeur_id'],
            ));
            if ($elec->exists() === true && $this->typeCat == "modification") {
                $elec_w_same_ine = $this->get_electeurs_with_same_ine($elec->getVal('ine'));
                if ($elec_w_same_ine === false) {
                    return false;
                }
                if (is_array($elec_w_same_ine) === true
                    && count($elec_w_same_ine) > 0) {
                    //
                    $electeur_ids = $elec_w_same_ine;
                }
            }
        }
        foreach ($electeur_ids as $electeur_id) {
            // Construction de la clause where de la requête
            $cle = " id = ".$electeur_id;
            //
            $res = $this->f->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, "electeur"),
                $valF,
                DB_AUTOQUERY_UPDATE,
                $cle
            );
            $this->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "electeur")."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res, true) === true) {
                return false;
            }
            //
            $this->addToLog(__METHOD__."(): Requête exécutée", VERBOSE_MODE);
            //
            $message = __("Enregistrement")."&nbsp;".$electeur_id."&nbsp;";
            $message .= __("de la table")."&nbsp;\"electeur\"&nbsp;";
            $message .= "[&nbsp;".$this->f->db->affectedRows()."&nbsp;";
            $message .= __("enregistrement(s) mis a jour")."&nbsp;]<br/>";
            $this->addToLog($message, VERBOSE_MODE);
            //
            if ($mark == true) {
                $this->addToLog(__("Un verrou a ete positionne sur l'electeur a ete positionne correctement."), VERBOSE_MODE);
            } else {
                $this->addToLog(__("Le verrou sur l'electeur a ete supprime correctement."), VERBOSE_MODE);
            }
        }
        return true;
    }
    // }}}
}
