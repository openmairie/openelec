<?php
/**
 * Ce script définit la classe 'edition_pdf__statistiques_mouvements_par_commune'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__statistiques_mouvements_par_commune' (edition_pdf).
 */
class edition_pdf__statistiques_mouvements_par_commune extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * @return array
     */
    public function compute_pdf__statistiques_mouvements_par_commune($params = array()) {
        //
        require_once "fpdf.php";
        $aujourdhui = date("d/m/Y");
        $nolibliste = $_SESSION["libelle_liste"];

        // communes ( type_interface 1 )
        $sqlcom1 = sprintf(
            'SELECT om_collectivite.om_collectivite AS id, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'ville\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as ville, (SELECT count(*) FROM %1$selecteur WHERE electeur.om_collectivite=om_collectivite.om_collectivite AND electeur.liste=\'%2$s\') AS total FROM %1$som_collectivite WHERE om_collectivite.niveau=\'1\' ORDER BY ville',
            DB_PREFIXE,
            $_SESSION["liste"]
        );

        // commune ( type_interface 0 )
        $sqlcom0 = sprintf(
            'SELECT  om_collectivite.om_collectivite AS id, (SELECT om_parametre.valeur FROM %1$som_parametre WHERE libelle=\'ville\' AND om_parametre.om_collectivite=om_collectivite.om_collectivite) as ville, (SELECT count(*) FROM %1$selecteur WHERE electeur.om_collectivite=om_collectivite.om_collectivite AND electeur.liste=\'%2$s\') AS total FROM %1$som_collectivite WHERE om_collectivite.niveau=\'1\' AND om_collectivite.libelle=\'%3$s\' ORDER BY ville',
            DB_PREFIXE,
            $_SESSION["liste"],
            $this->f->collectivite["ville"]
        );

        // Mouvements
        $sqlmov = sprintf(
            'SELECT mouvement.etat, mouvement.utilisateur, mouvement.om_collectivite, param_mouvement.typecat FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.date_tableau=\'%3$s\' AND mouvement.liste=\'%2$s\'',
            DB_PREFIXE,
            $_SESSION["liste"],
            $this->f->getParameter("datetableau")
        );


        // Communes
        $commune = array();

        $sqlcom = $sqlcom0;
        if ($this->f->isMulti() == true) $sqlcom = $sqlcom1;

        $rescom =& $this->f->db->query($sqlcom);
        $this->f->isDatabaseError($rescom);

        while ($rowcom =& $rescom->fetchrow(DB_FETCHMODE_ASSOC))
            array_push ($commune, $rowcom);

        /**
         *
         */
        function pdf_stat_entete($utils, $pdf, $aujourdhui) {
            $pdf->addpage();
            // entete
            $pdf->SetFont('courier','B',11);
            $pdf->Cell(190,7,iconv(HTTPCHARSET,"CP1252",__('SAISIES SUR LE TABLEAU EN COURS DU ').
                $utils->formatDate($utils->getParameter("datetableau")).' - '.$_SESSION["libelle_liste"]),'0',0,'L',0);
            $pdf->SetFont('courier','',11);
            $pdf->Cell(50,7, iconv(HTTPCHARSET,"CP1252",$aujourdhui),'0',0,'C',0);
            $pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",__(' Page  :  ').$pdf->PageNo()."/{nb} "),'0',1,'R',0);
            $pdf->ln();
            // Tableau
            $pdf->Cell(100,7,iconv(HTTPCHARSET,"CP1252",__('COMMUNE')),1,'0','L',0);
            $pdf->MultiCell(50,7,iconv(HTTPCHARSET,"CP1252",__("NOMBRE D'ELECTEURS AU DERNIER TABLEAU")),1,'C',0);
            $pdf->SetXY(155,19);
            $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",__('INSCRIPTIONS')),1,0,'L',0);
            $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",__('RADIATIONS')),1,0,'L',0);
            $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",__('MODIFICATIONS')),1,1,'L',0);
            $pdf->Cell(25,7,iconv(HTTPCHARSET,"CP1252",__('CODE')),1,0,'L',0);
            $pdf->Cell(75,7,iconv(HTTPCHARSET,"CP1252",__('NOM')),1,0,'L',0);
            $pdf->Cell(50,7,'',0,0,'L',0);
            $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",__('NOMBRE')),1,0,'C',0);
            $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",__('NOMBRE')),1,0,'C',0);
            $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",__('NOMBRE')),1,1,'C',0);
            return $pdf;
        }

        $pdf=new FPDF('L','mm','A4');
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(true);
        $pdf->SetFont('courier','',11);
        $pdf->SetDrawColor(30,7,146);
        $pdf->SetMargins(5,5,5);
        $pdf->SetDisplayMode('real','single');

        $pdf = pdf_stat_entete($this->f, $pdf, $aujourdhui);

        //  compteur de limite d'affichage
        $compteur = 0;

        //  Totaux
        $cpt_totElecteur = 0;
        $cpt_totInscription = 0;
        $cpt_totModification = 0;
        $cpt_totRadiation = 0;

        // Requete Mouvement
        $mouvements = array();
        $resmov =& $this->f->db->query($sqlmov);
        $this->f->isDatabaseError($resmov);
        while ($rowmov =& $resmov->fetchrow(DB_FETCHMODE_ASSOC))
            array_push ($mouvements, $rowmov);

        //
        foreach ($commune as $c) {
            // Statistique de la Commune
            // Mouvements
            $nb_insj5 = 0;
            $nb_radj5 = 0;
            $nb_modj5 = 0;
            $nb_radtrs = 0;
            $nb_instrs = 0;

            foreach ($mouvements as $m) {
                // Mouvement Inscription / Radiation / Modification
                if( $m['typecat']=='Inscription' && $m['om_collectivite']==$c['id'] ) $nb_insj5++;
                if( $m['typecat']=='Radiation' && $m['om_collectivite']==$c['id'] ) $nb_radj5++;
                if( $m['typecat']=='Modification' && $m['om_collectivite']==$c['id'] ) $nb_modj5++;
                // Mouvement trs Inscription / Radiation
                if ($m['etat']=='trs') {
                    if( $m['typecat']=='Inscription' && $m['om_collectivite']==$c['id'] ) $nb_instrs++;
                    if( $m['typecat']=='Radiation' && $m['om_collectivite']==$c['id'] ) $nb_radtrs++;
                }
            }

            //
            $totalElecteur = 0;
            $totalElecteur = ( $c['total'] + $nb_radtrs ) - $nb_instrs;

            // Compteurs Totaux
            $cpt_totElecteur += $totalElecteur;
            $cpt_totInscription += $nb_insj5;
            $cpt_totModification += $nb_modj5;
            $cpt_totRadiation += $nb_radj5;

            // Ajout Page et Entete - limite affichage atteint //////////////
            $compteur++;
            if ($compteur >= 24) {
                $pdf = pdf_stat_entete($this->f, $pdf, $aujourdhui);
                $compteur = 1;
            }
            //
            $pdf->Cell(25,7,' '.iconv(HTTPCHARSET,"CP1252",$c['id']),0,0,'L',0);
            $pdf->Cell(75,7,' '.iconv(HTTPCHARSET,"CP1252",$c['ville']),0,0,'L',0);
            $pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",$totalElecteur).'      ',0,0,'R',0);
            $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$nb_insj5).'      ',0,0,'R',0);
            $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$nb_radj5).'      ',0,0,'R',0);
            $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$nb_modj5).'      ',0,1,'R',0);

        }

        /// Totaux
        $pdf->Cell(100,7,iconv(HTTPCHARSET,"CP1252",'TOTAL    '),1,0,'R',0);
        $pdf->SetFont('courier','B',11);
        $pdf->Cell(50,7,iconv(HTTPCHARSET,"CP1252",$cpt_totElecteur).'      ',1,0,'R',0);
        $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$cpt_totInscription).'      ',1,0,'R',0);
        $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$cpt_totRadiation).'      ',1,0,'R',0);
        $pdf->Cell(45,7,iconv(HTTPCHARSET,"CP1252",$cpt_totModification).'      ',1,1,'R',0);

        /**
         * OUTPUT
         */
        //
        $filename = "statistiques_mouvements_par_commune-".date('Ymd-His').".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
