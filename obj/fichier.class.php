<?php
/**
 * Ce script définit la classe 'fichier'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/fichier.class.php";

/**
 * Définition de la classe 'fichier' (om_dbform).
 */
class fichier extends fichier_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        //
        $this->class_actions[4] = array(
            "identifier" => "all",
            "view" => "view_all",
            "permission_suffix" => "voir_tous",
        );
        $this->class_actions[5] = array(
            "identifier" => "migrate-trace-from-file-to-db",
            "portlet" => array(
                "type" => "action-self",
                "libelle" =>__("migrer"),
                "class" => "migrate-16",
                "order" => 25,
                ),
            "permission_suffix" => "migrer",
            "method" => "migrate",
            "button" => "migrer",
            "crud" => "delete",
            "condition" => array(
                "exists",
                "is_trace",
            ),
        );
    }


    /**
     * TREATMENT - migrate.
     * 
     * @return boolean
     */
    function migrate($val = array()) {
        $file = $this->f->storage->get($this->getVal("uid"));
        if ($file == OP_FAILURE || $file == null) {
            $this->addToMessage("error get file");
            return false;
        }
        $inst_trace = $this->f->get_inst__om_dbform(array(
            "obj" => "trace",
            "idx" => 0,
        ));
        $ret = $inst_trace->ajouter(array(
            "id" => "NEW",
            "creationdate" => $this->getVal("creationdate"),
            "creationtime" => $this->getVal("creationtime"),
            "type" => $this->getVal("filename"),
            "trace" => $file["file_content"],
            "om_collectivite" => $this->getVal("om_collectivite"),
        ));
        if ($ret !== true) {
            $this->addToMessage("error add trace");
            return false;
        }
        $this->setParameter("maj", 5);
        $this->id = $this->getVal("id");
        return $this->supprimer($val);
    }

    /**
     * CONDITION - is_trace.
     * 
     * @return boolean
     */
    function is_trace() {
        if ($this->getVal("type") == "trace") {
            return true;
        }
        return false;
    } 

    /**
     * VIEW - view_all().
     *
     * Cette méthode permet de lister tous les fichiers stockés (traces de
     * traitement, exports et éditions) et de permettre leur téléchargement.
     *
     * @return void
     */
    function view_all() {
        $this->checkAccessibility();

        //
        $sql = sprintf(
            'SELECT * FROM %1$sfichier WHERE om_collectivite=%2$s ORDER BY creationdate DESC, creationtime DESC',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $tab = array();
        while ($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $filename_as_array = explode(".", $row["filename"]);
            $tab[] = array(
                'date' => $row["creationdate"]." ".$row["creationtime"],
                'date_friendly' => $this->f->formatDate($row["creationdate"])." ".$row["creationtime"],
                'uid' => $row["uid"],
                'filename' => $row["filename"],
                'ext' => array_pop($filename_as_array),
            );
        }
        arsort($tab);

        //
        $listing = "";
        $col = 0;
        foreach ($tab as $elem) {
            $col ++;
            $listing .= sprintf(
                '<td>
                    <a class="om-prev-icon %1$s-16" href="%2$s" target="_blank" title="%3$s">%4$s</a>
                </td>',
                $elem['ext'],
                OM_ROUTE_FORM."&snippet=file&uid=".$elem['uid'],
                $elem["date_friendly"],
                $elem ['filename']
            );
            if ($col == 2) {
                $listing .= sprintf('</tr><tr>');
                $col = 0;
            }
        }
        printf(
            '<table class="file_list"><tr>%s</tr></table>',
            $listing
        );
    }

    /**
     *
     */
    function getFormTitle($ent) {
        //
        if ($this->getParameter("maj") == 4) {
            return __("Consultation")." -> ".__("Traces & Editions");
        }
        //
        return $ent;
    }
}


