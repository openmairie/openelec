<?php
/**
 * Ce script définit la classe 'reuSyncAncienMouvementRadiationTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_mouvement.class.php";

/**
 * Définition de la classe 'reuSyncAncienMouvementRadiationTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncAncienMouvementRadiationTraitement extends reuSyncMouvementTraitement {
    /**
     *
     */
    var $fichier = "reu_sync_ancien_mouvement_radiation";
    /**
     *
     */
    function displayBeforeContentForm() {
        echo $this->get_displayBeforeContentForm("radiation");
    }
    /**
     *
     */
    function get_reu_radiations_ouv() {
        if (isset($this->_reu_radiations_ouv) === true
            && $this->_reu_radiations_ouv !== null) {
            //
            return $this->_reu_radiations_ouv;
        }
        $inst_reu = $this->f->get_inst__reu();
        $ret = $inst_reu->handle_radiation(array(
            "mode" => "list",
            "datas" => array(
                "motif" => "MAI",
                "etat" => "OUV",
                "provenance" => "MAIRIE",
            ),
        ));
        $nb_radiations = intval(gavoe($ret, array("0", "headers", "P-Total-Count", )));
        $radiations = array();
        foreach ($ret as $key => $value) {
            if (is_array($value["result"]) !== true) {
                continue;
            }
            $radiations = array_merge(
                $radiations,
                $value["result"]
            );
        }
        if (count($radiations) == $nb_radiations) {
            $this->_reu_radiations_ouv = $radiations;
            return $this->_reu_radiations_ouv;
        }
        return null;
    }
    /**
     *
     */
    function check_if_my_radiation_is_already_in_reu($ine, $mouvement_id) {
        $radiations_reu = $this->get_reu_radiations_ouv();
        foreach ($radiations_reu as $key => $value) {
            if ($value["numeroElecteur"] == $ine
                && $value["referenceExterne"] == $mouvement_id) {
                //
                return $value["id"];
            }
        }
        return null;
    }
    /**
     *
     */
    function treatment() {
        $this->LogToFile("start reu_sync_ancien_mouvement_radiation");
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            $this->error = true;
            $this->addToMessage("Erreur de connexion REU.");
            $this->LogToFile("Erreur de connexion REU.");
            $this->LogToFile("end reu_sync_ancien_mouvement_radiation");
            return;
        }
        if ($inst_reu->is_connexion_standard_valid() !== true) {
            $this->error = true;
            $this->addToMessage(sprintf(
                '%s : %s',
                __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                sprintf(
                    '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                    __("cliquez ici pour vérifier")
                )
            ));
            $this->LogToFile("La connexion standard n'est pas valide.");
            $this->LogToFile("end reu_sync_ancien_mouvement_radiation");
            return;
        }
        /**
         * Etape 1 - On veut transmettre au REU toutes les demandes de radiation
         *           avec la date de tableau au 10/01/2019 dans l'état ACTIF
         *           sans demande rattachée au REU (id_demande).
         */
        //
        $this->LogToFile("Etape1");
        $radiations = $this->get_list_anciens_radiations_actif("etape_1");
        $radiations_to_treat_step_1 = $radiations['result'];
        $listes = array();
        $already_in_reu = array();
        $liste = "";
        foreach ($radiations_to_treat_step_1 as $key => $value) {
            $radiation_in_reu = $this->check_if_my_radiation_is_already_in_reu($value["ine"], $value["id"]);
            if ($radiation_in_reu !== null) {
                $already_in_reu[$value["id"]] = array(
                    "reu_demande_id" => $radiation_in_reu,
                    "ine" => $value["ine"],
                    "mouvement_id" => $value["id"],
                );
                continue;
            }
            if ($liste != $value["liste_insee"]) {
                $liste = $value["liste_insee"];
            }
            $listes[$liste][] = array(
                'ine' => $value["ine"],
                'mouvement_id' => $value["id"],
            );
        }
        //
        foreach ($listes as $key_liste => $value) {
            $datas = array(
                "observations" => "Transmission Mouvements 2018",
                "motif" => "MAI",
                "radiations" => $value,
                "liste" => $key_liste,
            );
            $inst_reu = $this->f->get_inst__reu();
            $ret = $inst_reu->handle_radiation(array(
                "mode" => "batch",
                "datas" => $datas,
            ));
            if ($ret["code"] != 200) {
                $this->LogToFile(sprintf(
                    "Liste %s - Erreur %s",
                    $key_liste,
                    $ret["code"]
                ));
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(
                        sprintf(
                            '%s : %s',
                            __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                            sprintf(
                                '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                __("cliquez ici pour vérifier")
                            )
                        )
                    );
                } else {
                    $this->addToMessage(
                        sprintf(__("Une erreur s'est produite. Contactez votre administrateur."))
                    );
                }
                $this->error = true;
                continue;
            }
            //
            $message = sprintf(
                __('Liste %3$s - Transmission au REU de %2$s radiation(s) avec succès sur un total de %1$s.'),
                gavoe($ret, array("result", "nbSoumission", )),
                gavoe($ret, array("result", "nbSucces", )),
                $key_liste
            );
            $this->addToMessage($message);
            $this->LogToFile($message);
            $results = gavoe($ret, array("result", "listeResultatUnitaire", ));
            if ($results == "") {
                $results = array();
            }
            $nb_result = count($results);
            $nb_res_ouv = 0;
            foreach ($results as $key => $value) {
                $this->startTransaction();
                $mouvement_id = gavoe($value, array("demandeRadiation", "referenceExterne", ));
                $ine = gavoe($value, array("electeur", "numeroElecteur", ));
                $inst_radiation = $this->f->get_inst__om_dbform(array(
                    "obj" => "radiation",
                    "idx" => $mouvement_id,
                ));
                if ($inst_radiation->exists() !== true) {
                    $this->LogToFile(sprintf(
                        "=> ECHEC Radiation de l'électeur INE %s échouée : %s",
                        $ine,
                        gavoe($value, array("erreurs", "0", "libelle", ))
                    ));
                    $this->rollbackTransaction();
                    continue;
                }
                $reu_demande_id = gavoe($value, array("demandeRadiation", "id", ));
                $valF = array(
                    "ine" => $ine,
                    "id_demande" => $reu_demande_id,
                    "statut" => "ouvert",
                );
                $ret = $inst_radiation->update_autoexecute($valF);
                if ($ret !== true) {
                    $this->LogToFile(sprintf(
                        "=> ECHEC Mouvement %s - Demande REU %s - INE %s - Echec de la mise à jour du mouvement",
                        $mouvement_id,
                        $reu_demande_id,
                        $ine
                    ));
                    $this->rollbackTransaction();
                    continue;
                }
                // Gestion de l'historique du mouvement
                $val_histo = array(
                    'action' => 'update_autoexecute',
                    'message' => __("Mise à jour de l'INE, du numéro de la demande et du statut depuis le traitement des anciens mouvements."),
                    'datas' => $valF,
                );
                $histo = $inst_radiation->handle_historique($val_histo);
                if ($histo !== true) {
                    $this->LogToFile(sprintf(
                        "=> ECHEC Mouvement %s - Demande REU %s - INE %s - Echec de la mise à jour du mouvement (historique)",
                        $mouvement_id,
                        $reu_demande_id,
                        $ine
                    ));
                    $this->rollbackTransaction();
                    continue;
                }
                $nb_res_ouv++;
                $this->commitTransaction();
                $this->LogToFile(sprintf(
                    "Mouvement %s - Demande REU %s - INE %s - Transmis au REU avec succès",
                    $mouvement_id,
                    $reu_demande_id,
                    $ine
                ));
            }
            $message = sprintf(
                __('Liste %3$s - %1$s radiation(s) rattachée(s) au REU sur un total de %2$s.'),
                $nb_res_ouv,
                $nb_result,
                $key_liste
            );
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        foreach ($already_in_reu as $key => $value) {
            $this->startTransaction();
            $mouvement_id = $value["mouvement_id"];
            $ine = $value["ine"];
            $reu_demande_id = $value["reu_demande_id"];
            $inst_radiation = $this->f->get_inst__om_dbform(array(
                "obj" => "radiation",
                "idx" => $mouvement_id,
            ));
            if ($inst_radiation->exists() !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC Radiation de l'électeur INE %s échouée : %s",
                    $ine,
                    gavoe($value, array("erreurs", "0", "libelle", ))
                ));
                $this->rollbackTransaction();
                continue;
            }
            $valF = array(
                "ine" => $ine,
                "id_demande" => $reu_demande_id,
                "statut" => "ouvert",
            );
            $ret = $inst_radiation->update_autoexecute($valF);
            if ($ret !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC Mouvement %s - Demande REU %s - INE %s - Echec de la mise à jour du mouvement",
                    $mouvement_id,
                    $reu_demande_id,
                    $ine
                ));
                $this->rollbackTransaction();
                continue;
            }
            // Gestion de l'historique du mouvement
            $val_histo = array(
                'action' => 'update_autoexecute',
                'message' => __("Mise à jour de l'INE, du numéro de la demande et du statut depuis le traitement des anciens mouvements."),
                'datas' => $valF,
            );
            $histo = $inst_radiation->handle_historique($val_histo);
            if ($histo !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC Mouvement %s - Demande REU %s - INE %s - Echec de la mise à jour du mouvement (historique)",
                    $mouvement_id,
                    $reu_demande_id,
                    $ine
                ));
                $this->rollbackTransaction();
                continue;
            }
            $nb_res_ouv++;
            $this->commitTransaction();
            $this->LogToFile(sprintf(
                "Mouvement %s - Demande REU %s - INE %s - Transmis au REU avec succès",
                $mouvement_id,
                $reu_demande_id,
                $ine
            ));
        }

        /**
         * Etape 2 - On veut viser toutes les demandes de radiation avec la date
         *           de tableau au 10/01/2019 dans l'état ACTIF avec demande
         *           rattachée au REU (id_demande).
         */
        $this->LogToFile("Etape2");
        $radiations = $this->get_list_anciens_radiations_actif("etape_2");
        $radiations_to_treat_step_2 = $radiations['result'];
        $nb_result = count($radiations['result']);
        $this->LogToFile(sprintf(
            __("Nombre de radiations à viser : %s"),
            $nb_result
        ));
        $nb_res_vis = 0;
        foreach ($radiations['result'] as $key => $value) {
            $this->startTransaction();
            $inst_radiation = $this->f->get_inst__om_dbform(array(
                "obj" => "radiation",
                "idx" => intval($value["id"]),
            ));
            if ($inst_radiation->exists() !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC La radiation %s n'existe pas",
                    intval($value["id"])
                ));
                $this->rollbackTransaction();
                continue;
            }
            $ret = $inst_radiation->viser(array(
                "date_visa" => "10/01/2019",
                "visa" => "accepte",
            ));
            if ($ret !== true) {
                $this->LogToFile(sprintf(
                    "=> ECHEC Visa de la radiation %s échouée : %s",
                    intval($value["id"]),
                    $inst_radiation->msg
                ));
                $this->rollbackTransaction();
                continue;
            }
            $nb_res_vis++;
            $this->commitTransaction();
            $this->LogToFile(sprintf(
                "Mouvement %s - Demande REU %s - INE %s - Visa du maire transmis au REU avec succès",
                intval($value["id"]),
                $inst_radiation->getVal("id_demande"),
                $inst_radiation->getVal("ine")
            ));
        }
        //
        if ($nb_result != 0) {
            $message = sprintf(
                __('%1$s radiation(s) visée(s) sur un total de %2$s.'),
                $nb_res_vis,
                $nb_result
            );
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        if (count($radiations_to_treat_step_1) == 0 && count($radiations_to_treat_step_2) == 0) {
            $message = __("Aucune radiation à traiter.");
            $this->addToMessage($message);
            $this->LogToFile($message);
        }
        $anciens_mouvements_total_a_transmettre = $this->get_nb_anciens_mouvements(
            "a_transmettre",
            "radiation"
        );
        if ($anciens_mouvements_total_a_transmettre["result"] != 0) {
            $this->error = true;
        }
        //
        $this->LogToFile("end reu_sync_ancien_mouvement_radiation");
    }
}
