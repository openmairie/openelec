<?php
/**
 * Ce script définit la classe 'traitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_base.class.php";

/**
 * Définition de la classe 'traitement' (om_base).
 */
class traitement extends om_base {

    var $page = NULL;
    var $params = null;
    var $log = "";
    var $message = "";
    var $error = false;
    var $fichier = "traitement";
    var $champs = array();
    var $form_name = "";
    var $form_class = "";
    var $generate_log_file = true;

    function __construct() {
        //
        $this->init_om_application();
        //
        $this->LogToFile ("start traitement par ".$_SESSION['login']);
        $this->LogToFile ("collectivite en cours: ".$_SESSION['libelle_collectivite']."[".$_SESSION["collectivite"]."]");
        $this->LogToFile ("liste en cours: ".$_SESSION["libelle_liste"]."[".$_SESSION["liste"]."]");
        $this->setLogFile ();
    }

    function now () {
        return date("Y-m-d_G:i:s");
    }

    function LogToFile ($msg) {
        $this->log .= $this->now ()." - ".$msg."\n";
    }

    function addToMessage ($msg) {
        $this->message .= $msg."<br/>";
    }

    function setLogFile () {
        $this->logfile = date("Ymd-Gis")."-traitement_".$this->fichier."-".$_SESSION["collectivite"]."-".$_SESSION['libelle_collectivite'].".txt";
    }

    function startTransaction ()  {
        $this->f->db->autoCommit (false);
    }

    function commitTransaction () {
        $this->f->db->commit ();
        $this->LogToFile ("commit");
        $this->f->db->autoCommit (true);
    }

    function rollbackTransaction () {
        $this->f->db->rollback ();
        $this->LogToFile ("rollback");
    }


    // {{{ FORMULAIRE

    function displayForm() {

        //
        $this->execute(true);

        //
        echo "<form";
        echo " method=\"post\"";
        echo " id=\"traitement_".$this->fichier."_form\"";
        echo " onsubmit=\"trt_form_submit('traitement_".$this->fichier."_result', '".$this->fichier."', this);return false;\"";
        echo " class=\"trt_form".($this->form_class != "" ? " ".$this->form_class : "")."\"";
        echo " action=\"#\"";
        if ($this->form_name != "") {
            echo " name=\"".$this->form_name."\"";
        }
        echo ">\n";

        //
        $description = $this->getDescription();
        if ($description != "") {
            //
            echo "<div class=\"instructions\">\n";
            echo "\t";
            echo $this->getDescription();
            echo "\n";
            echo "</div>\n";
        }

        //
        $this->displayResultSection();

        //
        $this->displayFormSection();

        //
        echo "<div class=\"formControls\">\n";
        echo "\t<input type=\"submit\" name=\"traitement.".$this->fichier.".form.valid\" ";
        echo "value=\"".$this->getValidButtonValue()."\" ";
        echo "class=\"boutonFormulaire\" />\n";
        echo "</div>\n";

        //
        echo "</form>\n";
    }

    function displayFormSection($form_only = false) {
        //
        echo "<div id=\"traitement_".$this->fichier."_status\">";
        //
        if ($form_only == false) {
            $this->displayBeforeContentForm();
        }
        //
        $this->form = $this->f->get_inst__om_formulaire(array(
            "validation" => 0,
            "maj" => 0,
            "champs" => $this->champs,
        ));
        //
        $this->setContentForm();
        //
        $this->displayContentForm();
        //
        if ($form_only == false) {
            $this->displayAfterContentForm();
        }
        //
        echo "</div>\n";
    }

    function displayResultSection() {
        //
        echo "<div id=\"traitement_".$this->fichier."_result\">\n";
        echo "\t<!-- -->\n";
        echo "</div>\n";
    }

    function getValidButtonValue() {
        //
        return __("Confirmer le traitement");
    }

    function getDescription() {
        //
        return "";
    }

    //
    function setContentForm() {

    }

    function displayContentForm() {
        //
        if (count($this->champs) != 0) {
            $this->form->entete();
            $this->form->afficher($this->champs, 0, false, false);
            $this->form->enpied();
        }
    }

    //
    function displayBeforeContentForm() {

    }

    //
    function displayAfterContentForm() {

    }

    // }}}

    function execute($check_valid_button = false) {
        //
        if ($check_valid_button == false
            || ($check_valid_button == true
                && isset($_POST["traitement_".$this->fichier."_form_valid"]))) {
            //
            $this->startTransaction ();
            $this->treatment ();
            if ($this->error) {
                $this->rollbackTransaction ();
            } else {
                $this->commitTransaction ();
            }
            //
            $this->LogToFile ("fin traitement");
            $trace_id = false;
            if ($this->generate_log_file === true) {
                $trace_id = $this->tmp($this->logfile, "\n".$this->log."\n", true);
            }
            $this->result($this->log);
        }
    }

    function setParams ($params = array()) {
        $this->params = $params;
    }

    function result($trace = false) {
        if ($this->error) {
            $message = __("Le traitement a echoue.");
            $message_class = "error";
        } else {
            $message = __("Le traitement est termine.");
            $message_class = "valid";
        }
        if ($trace !== false) {
            $message .= " ".sprintf(
                '<a href="#" onclick="trt_dialog_load()">%s</a>',
                __("Voir le détail")
            );
        }
        if ($this->message != "") {
            $message .= "<br/><br/>".$this->message;
        }
        if ($trace !== false) {
            $message .= sprintf(
                '<div id="trt_log_dialog" style="display: none"><pre>%s</pre></div>',
                $trace
            );
        }
        $this->f->displayMessage(
            $message_class,
            $message
        );
    }

    function treatment () {
        print "Not implemented";
    }

    /**
     * Fonction tmp()
     *
     * @param $fichier string Nom du fichier
     * @param $msg string Contenu du fichier
     */
    function tmp($fichier, $msg, $entete=false) {
        $this->startTransaction();
        if (!$entete) {
            $ent = date("d/m/Y G:i:s")."\n";
            $ent .= "Collectivite : ".$_SESSION ['coll']." - ".$this->f->getParameter('ville')."\n";
            $ent .= "Utilisateur : ".$_SESSION ['login']."\n";
            $ent .= "==================================================\n";
            $msg = $ent."\n".$msg ;
        }
        $inst_trace = $this->f->get_inst__om_dbform(array(
            "obj" => "trace",
            "idx" => 0,
        ));
        $val = array(
            "id" => "NEW",
            "creationdate" => date("Y-m-d"),
            "creationtime" => date("G:i:s"),
            "type" => $fichier,
            "trace" => $msg,
            "om_collectivite" => intval($_SESSION["collectivite"]),
        );
        $ret = $inst_trace->ajouter($val);
        if ($ret !== true) {
            $this->error = true;
            $this->LogToFile("Erreur lors de l'enregistrement des traces du traitement.");
            return;
        }
        $this->commitTransaction();
        return $inst_trace->valF[$inst_trace->clePrimaire];
    }

}


