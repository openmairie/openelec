<?php
/**
 * Ce script définit la classe 'nationalite'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/nationalite.class.php";

/**
 * Définition de la classe 'nationalite' (om_dbform).
 *
 * Cette classe représente la nationalite d'une personne susceptible
 * d'etre utilisee comme nationalite d'un electeur.
 */
class nationalite extends nationalite_gen {

}
