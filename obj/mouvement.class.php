<?php
/**
 * Ce script définit la classe 'mouvement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/mouvement.class.php";

/**
 * Définition de la classe 'mouvement' (om_dbform).
 */
class mouvement extends mouvement_gen {

    var $required_field = array(
        //
        "date_demande",
        "types",
        "date_tableau",
        //
        "nom",
        "prenom",
        "sexe",
        "date_naissance",
        //
        "bureau",
        "code_nationalite",
        "code_voie",
        "id",
        "liste",
        "om_collectivite"
    );

    /**
     * VIEW - dispatch.
     *
     * Permet de rediriger vers la bonne URL de la vue consulter de l'objet
     * (inscription, modification, radiation).
     *
     * @return void
     */
    function dispatch() {
        $query = sprintf(
            'SELECT typecat FROM %1$sparam_mouvement WHERE code=\'%2$s\'',
            DB_PREFIXE,
            $this->getVal("types")
        );
        $res = $this->f->db->getone($query);
        $this->f->isDatabaseError($res);
        header(sprintf(
            'Location: %s&obj=%s&action=3&idx=%s',
            OM_ROUTE_FORM,
            strtolower($res),
            $this->getVal($this->clePrimaire)
        ));
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        $this->class_actions = array();
        $this->class_actions[3] = array(
            "view" => "dispatch",
            "permission_suffix" => "dispatch",
        );
        //
        $this->class_actions[5] = array(
            "view" => "display_nb_mouvement",
            "permission_suffix" => "traiter_par_lot",
        );
        //
        $this->class_actions[304] = array(
            "identifier" => "statistiques-mouvement_bureau",
            "view" => "view_edition_pdf__statistiques_mouvement_bureau",
            "permission_suffix" => "edition_pdf__statistiques_mouvement_bureau",
        );
        //
        $this->class_actions[301] = array(
            "identifier" => "edition-pdf-statistiques_generales_des_mouvements",
            "view" => "view_edition_pdf__statistiques_generales_des_mouvements",
            "permission_suffix" => "edition_pdf__statistiques_generales_des_mouvements",
        );
        //
        $this->class_actions[302] = array(
            "identifier" => "statistiques-mouvement_voie",
            "view" => "view_edition_pdf__statistiques_mouvement_voie",
            "permission_suffix" => "edition_pdf__statistiques_mouvement_voie",
        );
        //
        $this->class_actions[303] = array(
            "identifier" => "statistiques-mouvement_io_sexe_bureau",
            "view" => "view_edition_pdf__statistiques_mouvement_io_sexe_bureau",
            "permission_suffix" => "edition_pdf__statistiques_mouvement_io_sexe_bureau",
        );
        //
        $this->class_actions[304] = array(
            "identifier" => "statistiques-mouvement_bureau",
            "view" => "view_edition_pdf__statistiques_mouvement_bureau",
            "permission_suffix" => "edition_pdf__statistiques_mouvement_bureau",
        );
    }


    /**
     * Vue du traitement par lot.
     *
     * Affichage d'un formulaire permettant de sélectionner un lot à traiter
     * et de déclencher le traitement du lot.
     *
     * Dans cette vue à chaque modification du lot sélectionné un message
     * indiquant le nombre de mouvement qui seront traité est affiché.
     * (onChange sur le select faisant appel à la function display_number_of_mouvement()
     * en javascript).
     *
     * @return void
     */
    function view_traitements_par_lot() {
        $this->checkAccessibility();
        $treatment = true;
        // Traitement suite à la validation du formulaire
        if (! empty($_POST['traitement_lot_submit'])) {
            if (empty($_POST['lot_a_traiter'])) {
                $this->addToMessage(__("Veuillez sélectionner un lot à traiter."));
                $treatment = false;
            }

            // déclenchement du traitement du lot
            if ($treatment != false) {
                $treatment = $this->traiter_all($_POST['lot_a_traiter']);
            }
        }
        if (! empty($this->msg)) {
            $this->f->displayMessage(
                $treatment === true ? 'valid' : 'error',
                $this->msg
            );
        }
        
        // Parmétrage du formulaire de sélection des lots
        $validation = 0;
        $maj = 0;
        $champs = array("lot_a_traiter");
        $form = $this->f->get_inst__om_formulaire(array(
            "validation" => $validation,
            "maj" => $maj,
            "champs" => $champs,
        ));
        // paramétrage du select de traitement du lot
        $lotATraiter = '';
        $form->setType("lot_a_traiter", "select");
        $form->setLib("lot_a_traiter", __('Lot à traiter'));
        $form->setTaille("lot_a_traiter", 20);
        $form->setMax("lot_a_traiter", 20);
        // Remplissage du select
        $contenu = array(
            0 => array(
                '',
                'mes_inscriptions_acceptees_insee',
                'mes_radiations_office_a_valider',
                'mes_radiations_acceptees_insee',
                'mes_modifications_d_etat_civil'
            ),
            1 => array(
                __('Veuillez sélectionner un lot à traiter'),
                __('Inscriptions acceptées par l\'insee'),
                __('Radiations d\'office'),
                __('Radiations acceptées par l\'insee'),
                __('Modifications d\'état civil')
            )
        );
        $form->setSelect("lot_a_traiter", $contenu);
        $form->setVal("lot_a_traiter", $lotATraiter);
        $form->setOnchange('lot_a_traiter',"display_number_of_mouvement(this.value);");
        // Affichage du formulaire
        $this->f->layout->display__form_container__begin(array(
            "id" => "traitementLotForm_form",
            "action" => "",
            "name" => "f2"
        ));
        $form->entete();
        $form->afficher($champs, $validation, false, false);
        $form->enpied();
        // Affichage du bouton de validation et du bouton de retour
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "traitement_lot.submit",
            "value" => __("Valider")
        ));
        $this->retour();
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }

    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return sprintf(
            '%s - %s %s%s %s',
            $this->getVal($this->clePrimaire),
            $this->getVal("civilite"),
            $this->getVal("nom"),
            ($this->getVal("nom_usage") != "" ? " - ".$this->getVal("nom_usage") : ""),
            $this->getVal("prenom")
        );
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "id",
            "id_demande",
            "date_demande",
            "types",
            "liste",
            "date_tableau",
            "om_collectivite",
            "tableau",
            "date_modif",
            "utilisateur",
            "visa",
            "date_visa",
            "date_complet",
            //
            "date_cnen",
            "envoi_cnen",
            "date_j5",
            "statut",
            "etat",
            "vu",
            //
            "electeur_id",
            "ine",
            //
            "civilite",
            "nom",
            "nom_usage",
            "prenom",
            "sexe",
            //
            "date_naissance",
            "'' as naissance_type_saisie",
            "'' as live_pays_de_naissance",
            "'' as live_ancien_departement_francais_algerie",
            "'' as live_commune_de_naissance",
            "code_departement_naissance",
            "libelle_departement_naissance",
            "code_lieu_de_naissance",
            "libelle_lieu_de_naissance",
            "code_nationalite",
            //
            "numero_habitation",
            "complement_numero",
            "code_voie",
            "libelle_voie",
            "complement",
            "adresse_rattachement_reu",
            //
            "bureau",
            "bureau_de_vote_code",
            "bureau_de_vote_libelle",
            "bureauforce",
            "ancien_bureau",
            "ancien_bureau_de_vote_code",
            "ancien_bureau_de_vote_libelle",
            "numero_bureau",
            //
            "resident",
            "adresse_resident",
            "complement_resident",
            "cp_resident",
            "ville_resident",
            "telephone",
            "courriel",
            //
            "provenance",
            "libelle_provenance",
            //
            "observation",
            "historique",
            "archive_electeur",
            //
            "provenance_demande",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_ancien_bureau() {
        return $this->get_common_var_sql_forminc__sql_bureau();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_ancien_bureau_by_id() {
        return $this->get_common_var_sql_forminc__sql_bureau_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return $this->get_common_var_sql_forminc__sql_bureau();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return $this->get_common_var_sql_forminc__sql_bureau_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste() {
        return $this->get_common_var_sql_forminc__sql_liste();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste_by_id() {
        return $this->get_common_var_sql_forminc__sql_liste_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_nationalite() {
        return sprintf(
            'SELECT nationalite.code, (nationalite.libelle_nationalite || \'(\' || nationalite.code || \')\') AS lib FROM %1$snationalite ORDER BY lib',
            DB_PREFIXE
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_nationalite_by_id() {
        return sprintf(
            'SELECT nationalite.code, (nationalite.libelle_nationalite || \'(\' || nationalite.code || \')\') AS lib FROM %1$snationalite WHERE nationalite.code = \'<idx>\'',
            DB_PREFIXE
        );
    }

    /**
     * CONDITION - is_marked_as_seen.
     *
     * @return boolean
     */
    function is_marked_as_seen() {
        if ($this->getVal("vu") == "t") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_marked_as_seen.
     *
     * @return boolean
     */
    function is_not_marked_as_seen() {
        if ($this->getVal("vu") == "f") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_treated.
     *
     * @return boolean
     */
    function is_not_treated() {
        if ($this->getVal("etat") == "trs") {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_reu_connected.
     *
     * @return boolean
     */
    function is_reu_connected() {
        if ($this->getVal("id_demande") !== "") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_reu_connected.
     *
     * @return boolean
     */
    function is_not_reu_connected() {
        return !$this->is_reu_connected();
    }

    /**
     * CONDITION - has_a_visa.
     *
     * @return boolean
     */
    function has_a_visa() {
        if ($this->getVal("visa") !== "") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - has_a_visa__accepte.
     *
     * @return boolean
     */
    function has_a_visa__accepte() {
        if ($this->getVal("visa") !== "accepte") {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - has_a_bureau.
     *
     * @return boolean
     */
    function has_a_bureau() {
        if ($this->getVal("bureau") == "") {
            return false;
        }
        return true;
    }
    /**
     * CONDITION - has_not_a_bureau.
     *
     * @return boolean
     */
    function has_not_a_bureau() {
        return !$this->has_a_bureau();
    }
    /**
     * CONDITION - has_not_a_visa.
     *
     * @return boolean
     */
    function has_not_a_visa() {
        return !$this->has_a_visa();
    }

    /**
     * CONDITION - is_in_status__vise_maire.
     *
     * @return boolean
     */
    function is_in_status__vise_maire() {
        if ($this->getVal("statut") === "vise_maire") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_in_status__vise_insee.
     *
     * @return boolean
     */
    function is_in_status__vise_insee() {
        if ($this->getVal("statut") === "vise_insee") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_in_status__vise_insee.
     *
     * @return boolean
     */
    function is_not_in_status__vise_insee() {
        return !$this->is_in_status__vise_insee();
    }

    /**
     * CONDITION - is_in_status__ouvert__or__en_attente.
     *
     * @return boolean
     */
    function is_in_status__ouvert__or__en_attente() {
        if ($this->is_in_status__ouvert() === true
            || $this->is_in_status__en_attente() === true) {
            //
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_in_status__ouvert__or__vise_insee.
     *
     * @return boolean
     */
    function is_in_status__ouvert__or__vise_insee() {
        if ($this->is_in_status__ouvert() === true
            || $this->is_in_status__vise_insee() === true) {
            //
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_in_status__vise_insee__or__vise_maire.
     *
     * @return boolean
     */
    function is_in_status__vise_insee__or__vise_maire() {
        if ($this->is_in_status__vise_maire() === true
            || $this->is_in_status__vise_insee() === true) {
            //
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_in_status__en_attente.
     *
     * @return boolean
     */
    function is_in_status__en_attente() {
        if ($this->getVal("statut") === "en_attente") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_in_status__abandonne.
     *
     * @return boolean
     */
    function is_in_status__abandonne() {
        if ($this->getVal("statut") === "abandonne") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_in_status__abandonne.
     *
     * @return boolean
     */
    function is_not_in_status__abandonne() {
        return !$this->is_in_status__abandonne();
    }

    /**
     * CONDITION - is_not_in_status__ouvert.
     *
     * @return boolean
     */
    function is_not_in_status__ouvert() {
        return !$this->is_in_status__ouvert();
    }

    /**
     * CONDITION - is_in_status__ouvert.
     *
     * @return boolean
     */
    function is_in_status__ouvert() {
        if ($this->getVal("statut") === "ouvert") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_in_status__complet.
     *
     * @return boolean
     */
    function is_not_in_status__complet() {
        return !$this->is_in_status__complet();
    }

    /**
     * CONDITION - is_in_status__complet.
     *
     * @return boolean
     */
    function is_in_status__complet() {
        if ($this->getVal("statut") === "complet") {
            return true;
        }
        return false;
    }

    /**
     *
     */
    function setvalF($val = array()) {
        //
        $fields = array(
            //
            "om_collectivite",
            //
            "date_modif",
            "utilisateur",
            //
            "tableau",
            "date_j5",
            "date_complet",
            "date_cnen",
            "envoi_cnen",
            "date_visa",
            "visa",
            "id_demande",
            //
            "adresse_rattachement_reu",
            //
            "historique",
            "archive_electeur",
            "vu",
            //
            "provenance",
            "libelle_provenance",
            //
            "observation",
            //
            "civilite",
            //
            "bureau_de_vote_code",
            "bureau_de_vote_libelle",
            "ancien_bureau_de_vote_code",
            "ancien_bureau_de_vote_libelle",
            //
            "provenance_demande",
        );
        foreach ($fields as $field) {
            if (array_key_exists($field, $val) === false) {
                $val[$field] = "";
            }
        }
        //
        parent::setValF($val);
        //
        $this->addToLog(__METHOD__."(): start setvalF", VERBOSE_MODE);
        //
        $this->valF['date_modif'] = $this->dateSystemeDB();
        $this->valF['utilisateur'] = $_SESSION['login'];
        //
        $this->valF['types'] = $val['types'];
        //
        if ($this->valF['sexe'] == "M") {
            $this->valF['civilite'] = "M.";
        } elseif ($this->valF['sexe'] == "F") {
            $this->valF['civilite'] = "Mme";
        } else {
            $this->valF['civilite'] = "M.";
        }
        $this->valF['nom'] = $val['nom'];
        $this->valF['nom_usage'] = $val['nom_usage'];
        $this->valF['prenom'] = $val['prenom'];
        //
        $this->valF['date_naissance'] = $val['date_naissance'];
        $this->valF['code_departement_naissance'] = trim($val['code_departement_naissance']);
        $this->valF['libelle_departement_naissance'] = trim($val['libelle_departement_naissance']);
        $this->valF['code_lieu_de_naissance'] = trim($val['code_lieu_de_naissance']);
        $this->valF['libelle_lieu_de_naissance'] = trim($val['libelle_lieu_de_naissance']);
        $this->valF['code_nationalite'] = $val[ 'code_nationalite'];
        //
        $this->valF['code_voie'] = $val['code_voie'];
        $this->valF['libelle_voie'] = $val['libelle_voie'];
        // Verification numero_habitation
        // Si le numero_habitation est vide alors on lui affecte 0
        if ($val['numero_habitation'] == "") {
            $val['numero_habitation'] = 0;
        }
        $this->valF['numero_habitation'] = $val['numero_habitation'];
        //
        $this->valF['complement_numero'] = $val['complement_numero'];
        $this->valF['complement'] = $val['complement'];
        //
        $this->valF['provenance'] = $val['provenance'];
        $this->valF['libelle_provenance'] = $val['libelle_provenance'];
        //
        $this->valF['observation'] = $val['observation'];
        // Gestion de l'adresse de contact
        $this->valF['resident'] = $val['resident'];
        if ($this->valF["resident"] == "Oui") {
            $this->valF["adresse_resident"] = $val["adresse_resident"];
            $this->valF["complement_resident"] = $val["complement_resident"];
            $this->valF["cp_resident"] = $val["cp_resident"];
            $this->valF["ville_resident"] = $val["ville_resident"];
        } else {
            $this->valF["adresse_resident"] = "";
            $this->valF["complement_resident"] = "";
            $this->valF["cp_resident"] = "";
            $this->valF["ville_resident"] = "";
        }
        // variable de traitement
        $this->valF['date_tableau'] = $val['date_tableau'];
        if (array_key_exists("etat", $val) === true) {
            $this->valF["etat"] = $val["etat"];
        } else {
            $this->valF["etat"] = 'actif';
        }
        $this->valF['tableau'] = 'annuel';
        //
        $this->valF['om_collectivite'] = intval($_SESSION["collectivite"]);
        // Pas de recalcul du bureau si radiation utilisation du bureau de l'électeur.
        if(isset($this->typeCat) and $this->typeCat!='radiation') {
            $this->manageBureauForceAndCodeBureau($val);
        } else {
            $this->valF['bureau'] = $val['bureau'];
        }
        //
        // Ces champs sont mis à jour uniquement par la gestion spécifique du log
        // et donc jamais par les actions ajouter/modifier
        unset($this->valF['historique']);
        unset($this->valF['archive_electeur']);
        // Le marqueur vu est mis à jour uniquement par sa gestion spécifique
        // (actions)
        unset($this->valF['vu']);
        $this->addToLog(__METHOD__."(): end setvalF", VERBOSE_MODE);
    }

    /**
     * TREATMENT - mark_as_seen.
     *
     * Cette methode permet de passer le marqueur *vu* à vrai.
     *
     * @return boolean true si maj effectué false sinon
     */
    function mark_as_seen() {
        // Cette méthode permet d'exécuter une routine en début des méthodes
        // dites de TREATMENT.
        $this->begin_treatment(__METHOD__);
        //
        $valF = array(
            "vu" => true,
        );
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => "mark_as_seen",
            'message' => sprintf(
                __('Action %1$s %2$s effectuée.'),
                63,
                $this->get_action_param(63, 'identifier')
            ),
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Vos modifications ont bien été enregistrées."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - mark_all_as_seen.
     *
     * @return boolean
     */
    function mark_all_as_seen($val = array()) {
        $this->begin_treatment(__METHOD__);

        return $this->end_treatment(__METHOD__, true);
    }


    /**
     * Récupère via une requête sql la liste des mouvements selon le mode souhaité.
     * Les modes possibles sont :
     *  - mes_inscriptions_acceptees_insee
     *  - mes_radiations_office_a_valider
     *  - mes_radiations_acceptees_insee
     *  - mes_modifications_d_etat_civil
     *
     * Si le paramètre type a pour valeur 'nb' seul le nombre de mouvement récupéré
     * sera renvoyé. Sinon renvoie un tableau contenant la liste des résultats.
     *
     * @param string mode
     * @param string type
     * @return integer|array
     */
    function get_mouvements($mode = '', $type = '') {
        // Si le type est 'nb', cad si l'on veut récupérer le nombre de mouvement
        // alors un count est utilisé. Sinon on récupére les identifiants des
        // mouvement
        $sqlSelect = 'mouvement.id';
        if ($type === 'nb') {
            $sqlSelect = 'COUNT(*)';
        }
        // Paramétrage de la requête selon le mode voulu
        $typeMouvement = '';
        $etat = 'trs';
        $statut = 'accepte';
        $extraSql = '';
        if ($mode === "mes_inscriptions_acceptees_insee") {
            $typeMouvement = 'inscription';
            $extraSql = 'AND mouvement.vu IS FALSE';
        } elseif ($mode === "mes_radiations_office_a_valider") {
            $typeMouvement = 'radiation';
            $statut = 'vise_insee';
        } elseif ($mode === "mes_radiations_acceptees_insee") {
            $typeMouvement = 'radiation';
            $extraSql = 'AND mouvement.vu IS FALSE';
        } elseif ($mode === "mes_modifications_d_etat_civil") {
            $typeMouvement = 'modification';
            $statut = 'vise_insee';
        }
        // Requête permettant de récupérer les mouvements
        $qres = $this->f->get_all_results_from_db_query(
            sprintf(
                'SELECT
                    %2$s
                FROM
                    %1$smouvement
                    INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
                WHERE 
                    mouvement.om_collectivite=%3$s
                    AND lower(param_mouvement.typecat)=\'%4$s\'
                    AND mouvement.etat = \'%5$s\' 
                    AND mouvement.statut=\'%6$s\'
                    %7$s',
                DB_PREFIXE,
                $sqlSelect,
                intval($_SESSION["collectivite"]),
                $typeMouvement,
                $etat,
                $statut,
                $extraSql
            )
        );

        if ($type === 'nb') {
            return $qres['result'][0]['count'];
        }
        return $qres['result'];
    }

    /**
     * Affiche un message indiquant le nombre de mouvement à traiter
     *
     * @return void
     */
    function display_nb_mouvement() {
        $mode = $this->f->get_submitted_get_value('mode');
        $res = $this->get_mouvements($mode, 'nb');
        if ($res == 0) {
            $this->f->displayMessage(
                'warning',
                'Aucun mouvement à traiter pour ce lot.'
            );
        } elseif ($res > 0) {
            $this->f->displayMessage(
                'warning',
                sprintf(
                    'Environ %d mouvement seront traités pour ce lot.',
                    intval($res)
                )
            );
        }
    }

    /**
     * TREATMENT - traiter_all.
     * Effectue le traitement d'un lot selon le mode choisi.
     * Les modes possibles sont :
     *  - mes_inscriptions_acceptees_insee :
     *        type de mouvement = inscription
     *        traitement effectué = marque les mouvement comme vu
     *  - mes_radiations_office_a_valider :
     *        type de mouvement = radiation
     *        traitement effectué = validation des mouvements
     *  - mes_radiations_acceptees_insee :
     *        type de mouvement = radiation
     *        traitement effectué = marque les mouvement comme vu
     *  - mes_modifications_d_etat_civil :
     *        type de mouvement = modification
     *        traitement effectué = validation des mouvements
     *
     * Si le traitement echoue renvoie false sinon renvoie true.
     *
     * @return boolean
     */
    function traiter_all(string $mode = '') {
        $this->begin_treatment(__METHOD__);
        $mouvement_type = '';
        $treatment = '';
        // Récupération du paramétrage et à partir du mode choisi préparation du
        // traitement.
        if ($mode == "mes_inscriptions_acceptees_insee") {
            $mouvement_type = "inscription";
            $treatment = "mark_as_seen";
        } elseif ($mode == "mes_radiations_office_a_valider") {
            $mouvement_type = "radiation";
            $treatment = "valider";
        } elseif ($mode == "mes_radiations_acceptees_insee") {
            $mouvement_type = "radiation";
            $treatment = "mark_as_seen";
        } elseif ($mode == "mes_modifications_d_etat_civil") {
            $mouvement_type = "modification";
            $treatment = "valider";
        }
        // Si le mode ou le type de mouvement correspondant n'existe pas alors
        // on ne peut pas traiter le lot et on renvoie une erreur
        if (empty($mode) || empty($mouvement_type) || empty($treatment)) {
            $this->addToMessage(sprintf(
                "=> ECHEC Le mode %s n'existe pas",
                $mode
            ));
            return $this->end_treatment(__METHOD__, false);
        }

        // Récupération des mouvements à traiter selon le mode choisi
        $mouvements = $this->get_mouvements($mode);
        $mouvements_to_treat = $mouvements;
        $nb_mouvements_to_treat = count($mouvements);
        $nb_res_vis = 0;
        // Traitement des mouvements
        foreach ($mouvements as $key => $value) {
            $inst_mouvement = $this->f->get_inst__om_dbform(array(
                "obj" => $mouvement_type,
                "idx" => intval($value["id"]),
            ));
            // Gestion des mouvements en erreur
            if ($inst_mouvement->exists() !== true) {
                $this->addToMessage(sprintf(
                    "=> ECHEC Le mouvement %s n'existe pas",
                    intval($value["id"])
                ));
                continue;
            }
            // Application du traitement souhaité
            $ret = $inst_mouvement->$treatment();
            // Gestion des mouvements en erreur
            if ($ret !== true) {
                $this->addToMessage(sprintf(
                    "=> ECHEC le traitement du mouvement %s à échouée : %s",
                    intval($value["id"]),
                    $inst_mouvement->msg
                ));
                break;
            }
            $nb_res_vis++;
        }
        // Ajoute un message indiquant le nombre de mouvement traité
        if ($nb_mouvements_to_treat != 0) {
            $message = sprintf(
                __('%1$s mouvement(s) traité(s) sur un total de %2$s.'),
                $nb_res_vis,
                $nb_mouvements_to_treat
            );
            $this->addToMessage($message);
        }

        // Ajoute un message indiquant qu'aucun mouvement n'est à traiter
        if ($nb_mouvements_to_treat == 0) {
            $message = __("Aucun mouvement à traiter.");
            $this->addToMessage($message);
        }

        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - abandonner.
     *
     * @return boolean
     */
    function abandonner($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        if ($this->getVal("id_demande") !== "") {
            $inst_reu = $this->f->get_inst__reu();
            $method = sprintf('handle_%s', $this->typeCat);
            $ret = $inst_reu->$method(array(
                "mode" => "abandonner",
                "id" => $this->getVal("id_demande"),
                "datas" => array(),
            ));
            if (isset($ret["code"]) !== true || $ret["code"] != 200) {
                $this->correct = false;
                $this->addToMessage("Erreur de transmission au REU.");
                if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                    $this->addToMessage($ret["result"]["message"]);
                }
                if ($ret["code"] == 401) {
                    if ($inst_reu->is_connexion_standard_valid() === false) {
                        $this->addToMessage(sprintf(
                            '%s : %s',
                            __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                            sprintf(
                                '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                __("cliquez ici pour vérifier")
                            )
                        ));
                    }
                }
                $this->correct = false;
                return $this->end_treatment(__METHOD__, false);
            }
        }
        //
        $valF = array(
            "statut" => "abandonne",
            "etat" => "na",
        );
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => 'abandonner',
            'message' => sprintf(
                __('Action %1$s %2$s effectuée.'),
                501,
                $this->get_action_param(501, 'identifier')
            ),
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Vos modifications ont bien été enregistrées."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function get_current_and_clean_message() {
        $message = $this->msg;
        $this->msg = "";
        return $message;
    }

    /**
     *
     */
    function get_current_and_set_message($message) {
        $message_tmp = $this->msg;
        $this->msg = $message;
        return $message_tmp;
    }

    /**
     * TREATMENT - valider.
     *
     * @return boolean
     */
    function valider($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        $message_detail = array();
        //
        if ($this->getVal("etat") == "actif") {
            $message = $this->get_current_and_clean_message();
            $ret = $this->apply();
            if ($ret !== true) {
                $this->correct = false;
                $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
                return $this->end_treatment(__METHOD__, false);
            }
            $message_detail[] = $this->get_current_and_set_message($message);
        }
        //
        $valF = array(
            "statut" => "accepte",
        );
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => 'valider',
            'message' => sprintf(
                __('Action %1$s %2$s effectuée.'),
                61,
                $this->get_action_param(61, 'identifier')
            ),
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $message_detail[] = __("Le statut du mouvement a été correctement mis à jour.");
        // On transfère les nouvelles informations de l'électeur au REU
        // pour toutes les inscriptions et pour les modifications qui ne sont pas
        // de type MODEC
        if ($this->get_mouvement_typecat() === 'inscription'
            || ($this->get_mouvement_typecat() === 'modification'
                && $this->getVal("types") != "MODEC")) {
            // Pour être sûr qu'on a l'identifiant de l'électeur dans le mouvement
            // on réinitialise les données de l'enregistrement
            $this->init_record_data($this->getVal($this->clePrimaire));
            //
            $message = $this->get_current_and_clean_message();
            $inst_electeur = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => $this->getVal("electeur_id"),
            ));
            if ($inst_electeur->exists() !== true) {
                $this->correct = false;
                $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
                return $this->end_treatment(__METHOD__, false);
            }
            $update_electeur_reu = $inst_electeur->push_infos_to_reu($val);
            if ($update_electeur_reu !== true) {
                $this->correct = false;
                $this->addToMessage("SAISIE NON ENREGISTRÉE");
                $this->addToMessage("");
                $this->addToMessage("> ".$inst_electeur->msg);
                return $this->end_treatment(__METHOD__, false);
            }
            $this->msg = $inst_electeur->msg;
            $message_detail[] = $this->get_current_and_set_message($message);
            // Gestion de l'historique
            $val_histo = array(
                'action' => __METHOD__,
                'message' => sprintf(
                    __('Transmission au REU de la modification sur l\'électeur %1$s.'),
                    $this->getVal("ine")
                ),
            );
            $histo = $this->handle_historique($val_histo);
            if ($histo !== true) {
                $this->correct = false;
                return $this->end_treatment(__METHOD__, false);
            }
        }
        $this->addToMessage(__("Le mouvement a été correctement validé."));
        $this->addToMessage("");
        foreach ($message_detail as $key => $value) {
            $this->addToMessage("> ".$value);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - completer.
     *
     * @return boolean
     */
    function completer($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        if ($this->typeCat != 'inscription') {
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $date_complet = $val["date_complet"];
        //
        $inst_reu = $this->f->get_inst__reu();
        $method = sprintf('handle_%s', $this->typeCat);
        $ret = $inst_reu->$method(array(
            "mode" => "completer",
            "id" => $this->getVal("id_demande"),
            "datas" => array(
                "date_complet" => $date_complet,
            ),
        ));
        if (isset($ret["code"]) !== true || $ret["code"] != 200) {
            $this->correct = false;
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $valF = array(
            "date_complet" => $this->f->formatDate($date_complet, false),
            "statut" => "complet",
            "vu" => true,
        );
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => 'completer',
            'message' => sprintf(
                __('Action %1$s %2$s effectuée.'),
                503,
                $this->get_action_param(503, 'identifier')
            ),
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Vos modifications ont bien été enregistrées."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - viser.
     *
     * @return boolean
     */
    function viser($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        if ($this->typeCat != 'inscription'
            && $this->typeCat != 'radiation') {
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $date_visa = $val["date_visa"];

        //
        $inst_reu = $this->f->get_inst__reu();
        $method = sprintf('handle_%s', $this->typeCat);
        $ret = $inst_reu->$method(array(
            "mode" => "viser",
            "id" => $this->getVal("id_demande"),
            "datas" => array(
                "date_visa" => $val["date_visa"],
                "visa" => $val["visa"],
            ),
        ));
        if (isset($ret["code"]) !== true || $ret["code"] != 200) {
            $this->correct = false;
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            return $this->end_treatment(__METHOD__, false);
        }
        $valF = array(
            "visa" => $val["visa"],
            "date_visa" => $this->f->formatDate($date_visa, false),
            "statut" => "vise_maire",
        );
        if ($val["visa"] == "refuse") {
            $valF["etat"] = "na";
        }
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => 'viser',
            'message' => sprintf(
                __('Action %1$s %2$s effectuée.'),
                502,
                $this->get_action_param(502, 'identifier')
            ),
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Vos modifications ont bien été enregistrées."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function view_decoupage_json() {
        $inst_voie = $this->f->get_inst__om_dbform(array(
            "obj" => "voie",
            "idx" => $this->f->get_submitted_get_value("voie"),
        ));
        if ($inst_voie->exists() !== true) {
            echo json_encode(array(
                "code" => -2,
                "message" => "La voie n'existe pas.",
            ));
            return;
        }
        $voie_infos = array(
            "code" => $inst_voie->getVal($inst_voie->clePrimaire),
            "libelle" => $inst_voie->getVal("libelle"),
            "code_postal" => $inst_voie->getVal("cp"),
            "ville" => $inst_voie->getVal("ville"),
        );
        $bureau = $this->getBureauFromDecoupage(
            $this->f->get_submitted_get_value("voie"),
            $this->f->get_submitted_get_value("numero_habitation")
        );
        if ($bureau === null) {
            echo json_encode(array(
                "code" => -1,
                "message" => "Aucune correspondance dans le découpage.",
                "voie" => $voie_infos,
            ));
            return;
        }
        echo json_encode(array(
            "code" => 0,
            "message" => "Correspondance dans le découpage.",
            "voie" => $voie_infos,
            "bureau" => $bureau,
        ));
    }

    /**
     *
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        if (isset($val["origin"]) === true && $val["origin"] == "from_reu_notification") {
            unset($this->required_field[array_search("bureau", $this->required_field)]);
        }
        parent::verifier($val);
        // Vérification de la date de demande obligatoire
        // Ce champ n'est pas obligatoire en base de données
        // Il vaut mieux s'assurer qu'il est bien rempli
        if ($this->valF['date_demande'] == "" || $this->valF['date_demande'] == null) {
            $this->correct = false;
            $message = __('Le champ').' <span class="bold">'.$this->getLibFromField("date_demande").'</span> '.__('est obligatoire');
            if (strpos($this->msg, $message) === false) {
                $this->addToMessage($message);
            }
        }
        // La date de la demande ne peut pas être dans le futur
        if ($this->valF['date_demande'] > date("Y-m-d")) {
            $this->correct = false;
            $this->addToMessage(__("La date de demande ne peut pas être dans le futur"));
        }
        // Verification coherence :  civilite/sexe
        if ($this->valF['sexe']=="F" and $this->valF['civilite']=="M." ) {
            $this->correct=false;
            $this->msg .= __("Incompatibilite civilite = M. et sexe = F")."<br />";
        }
        if ($this->valF['sexe']=="M" and $this->valF['civilite']=="Mme" ) {
            $this->correct=false;
            $this->msg .= __("Incompatibilite civilite = Mme et sexe = M")."<br />";
        }
        if ($this->valF['sexe']=="M" and $this->valF['civilite']=="Mlle" ) {
            $this->correct=false;
            $this->msg .= __("Incompatibilite civilite = Mlle et sexe = M")."<br />";
        }

        // Naissance & Nationalite
        // Verifications Date de naissance
        if ($this->valF['date_naissance'] != "") {
            // XXX Recalcul de l'âge en fonction de la date d'effet de la révision concernée par la date de tableau.
            //// calcul de l'age
            /**
             *
             */
            //
            $datetableau = $this->f->formatDate($this->valF['date_tableau'], false);
            //
            $inst_revision = $this->f->get_inst__om_dbform(array(
                "obj" => "revision",
            ));
            $current_revision = $inst_revision->get_current_revision("fromdatetableau", $datetableau);
            if ($current_revision == null) {
                $this->msg .= __("La revision n'existe pas. Contactez votre administrateur.");
                $this->correct = false;
            } else {
                $date_effet = $inst_revision->get_date("effet", "Ymd");
                //
                $temp0 = $this->valF['date_naissance'];
                //
                $temp = intval($date_effet);
                $temp1 = intval(substr ($temp0, 6, 4).substr ($temp0, 3, 2).substr ($temp0, 0, 2));
                $age = ($temp - $temp1)/10000;
                ////
                $this->valF['date_naissance'] = $this->dateDB($this->valF['date_naissance']);
                $this->valF['date_tableau'] = $this->dateDB($this->valF['date_tableau']);
                // controle electeur plus de 18 ans
                if ($age <= 18) {
                    $this->addToMessage(__("L'electeur n'aura pas plus de 18 ans le")." ".$inst_revision->get_date("effet", "d/m/Y"));
                    //
                    $effet_mouvement = false;
                    if (isset($this->valF['types']) && $this->valF['types'] != "") {
                        //
                        $sql = sprintf(
                            'SELECT * FROM %1$sparam_mouvement WHERE code=\'%2$s\'',
                            DB_PREFIXE,
                            $this->valF['types']
                        );
                        $res = $this->f->db->query($sql);
                        $this->addToLog(
                            __METHOD__."(): db->query(\"".$sql."\");",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($res);
                        // On vérifie qu'il y a un et unique mouvement correspondant
                        if ($res->numrows() != 1) {
                            $this->correct = false;
                            $this->addToMessage(__("Erreur de parametrage des mouvements."));
                        } else {
                            // On récupère l'effet du mouvement en question pour vérifier l'âge ou non
                            // de l'électeur
                            $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
                            $effet_mouvement = $row["effet"];
                        }
                    }
                    //
                    if ($effet_mouvement != "Election") {
                        $this->correct = false;
                    }
                }
            }
        }

        /**
         * Gestion des lieux de naissance
         */
        //
        if (array_key_exists("naissance_type_saisie", $val) === false
            || $val["naissance_type_saisie"] === "autre") {
            // on ne fait rien
        } elseif ($val["naissance_type_saisie"] !== "Etranger" && $val["naissance_type_saisie"] !== "Ancien_dep_franc") {
            // => Né en France
            // On vérifie que le code_lieu_de_naissance n'est pas vide sinon => INCORRECT
            if ($this->valF['code_lieu_de_naissance'] == "") {
                $this->correct=false;
                $this->addToMessage(__("Le champ <b>Commune de naissance</b> est obligatoire"));
            } else {
                // Vérification de l'existence de code_departement_naissance dans la table commune
                $sql = sprintf(
                    'SELECT * FROM %1$scommune WHERE code=\'%2$s\'',
                    DB_PREFIXE,
                    $this->valF["code_lieu_de_naissance"]
                );
                $res = $this->f->db->query($sql);
                $this->addToLog(
                    __METHOD__."(): db->query(\"".$sql."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                //
                $nbligne = $res->numrows();
                // Initialisation d'un flag temporaire
                $correct = true;
                // Si aucun résultat à la requête aucune commune ne correspond à ce code => INCORRECT
                if ($nbligne==0) {
                    $correct = false;
                    $this->correct = false;
                    $this->addToMessage(__("Le code lieu de naissance ").$this->valF['code_lieu_de_naissance'].__(" n'existe pas"));
                }
                //  Si il y a au moins un résultat
                if ($correct) {
                    // Vérification de la correspondance entre libelle_lieu_de_naissance et celui de la table commune
                    $sql = sprintf(
                        'SELECT code_departement, libelle_commune FROM %1$scommune WHERE code=\'%2$s\'',
                        DB_PREFIXE,
                        $this->valF['code_lieu_de_naissance']
                    );
                    $res = $this->f->db->query($sql);
                    $this->addToLog(
                        __METHOD__."(): db->query(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($res);
                    $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
                    $this->valF['libelle_lieu_de_naissance'] = $row['libelle_commune'];
                    $this->valF['code_departement_naissance'] = $row['code_departement'];
                    // Vérification de la correspondance entre libelle_departement_naissance et celui de la table departement
                    $sql = sprintf(
                        'SELECT libelle_departement FROM %1$sdepartement WHERE code=\'%2$s\'',
                        DB_PREFIXE,
                        $this->valF['code_departement_naissance']
                    );
                    $res = $this->f->db->query($sql);
                    $this->addToLog(
                        __METHOD__."(): db->query(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($res);
                    $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
                    $this->valF['libelle_departement_naissance'] = $row ['libelle_departement'];
                }
            }

        } else {
            // => Né à l'étranger ou Ancien département Français d'Algérie
            // On vérifie que le code_departement_naissance n'est pas vide sinon => INCORRECT
            if ($this->valF['code_departement_naissance'] == "") {
                $this->correct=false;
                $this->addToMessage(__("Le champ <b>Département/Pays de naissance</b> est obligatoire"));
            } else {
                // Vérification de l'existence de code_departement_naissance dans la table departement
                $sql = sprintf(
                    'SELECT * FROM %1$sdepartement WHERE code=\'%2$s\'',
                    DB_PREFIXE,
                    $this->valF['code_departement_naissance']
                );
                $res = $this->f->db->query($sql);
                $this->addToLog(
                    __METHOD__."(): db->query(\"".$sql."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                //
                $nbligne = $res->numrows();
                // Initialisation d'un flag temporaire
                $correct = true;
                // Si aucun résultat à la requête aucun département ne correspond à ce code => INCORRECT
                if ($nbligne == 0) {
                    $correct = false;
                    $this->correct = false;
                    $this->addToMessage(__("Le code du departement de naissance  ").$this->valF['code_departement_naissance'].__(" n'existe pas"));
                }
                //  Si il y a au moins un résultat
                if ($correct) {
                    // Vérification de la correspondance entre libelle_departement_naissance et celui de la table departement
                    $sql = sprintf(
                        'SELECT libelle_departement FROM %1$sdepartement WHERE code=\'%2$s\'',
                        DB_PREFIXE,
                        $this->valF['code_departement_naissance']
                    );
                    $res = $this->f->db->query($sql);
                    $this->addToLog(
                        __METHOD__."(): db->query(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($res);
                    $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
                    $this->valF['libelle_departement_naissance'] = $row ['libelle_departement'];
                    $this->valF['code_lieu_de_naissance'] = $this->valF['code_departement_naissance'];
                }
            }

            // On vérifie que le libelle_departement_naissance n'est pas vide sinon => INCORRECT
            if ($this->valF['libelle_lieu_de_naissance'] == "") {
                $this->correct=false;
                $this->addToMessage(__("Le champ <b>Libellé commune de naissance</b> est obligatoire"));
            }

        }

        // Verification numero_habitation
        // Si le numero_habitation n'est pas un entier on rejette la saisie
        if (is_numeric($this->valF['numero_habitation']) == false) {
            $this->correct = false;
            $this->addToMessage(__("Le numero d'habitation doit etre un entier"));
        }
        //
        if ($this->valF['code_voie'] != "") {
            // Test si la voie existe
            $sql = sprintf(
                'SELECT * FROM %1$svoie WHERE voie.code=\'%3$s\' AND voie.om_collectivite=%2$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $this->valF['code_voie']
            );
            $res = $this->f->db->query($sql);
            $this->addToLog(
                __METHOD__."(): db->query(\"".$sql."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            //
            $nbligne = $res->numrows(); //XXX
            $correct = true;
            if ($nbligne==0) {
                $this->correct=false;
                $correct = false;
                $this->addToMessage(__("La voie ").$this->valF['code_voie'].__(" n'existe pas"));
            }
            if ($correct) {
                //
                $sql = sprintf(
                    'SELECT voie.libelle_voie FROM %1$svoie WHERE voie.code=\'%3$s\' AND voie.om_collectivite=%2$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $this->valF['code_voie']
                );
                $res = $this->f->db->query($sql);
                $this->addToLog(
                    __METHOD__."(): db->query(\"".$sql."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                //
                $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
                if ($row ['libelle_voie'] != $val['libelle_voie']) {
                    $this->correct=false;
                    $this->addToMessage(__("La correspondance entre les deux champs Id / Libelle de la voie n'est pas correcte"));
                }
            }
        }
        // Resident
        if ($this->valF['resident']=="Oui") {
            if ($this->valF['adresse_resident']=="") {
                $this->correct=false;
                $this->addToMessage(__("Le champ <b>Adresse (résident)</b> est obligatoire"));
            }
            if ($this->valF['cp_resident']=="") {
                $this->correct=false;
                $this->addToMessage(__("Le champ <b>Code postal (résident)</b> est obligatoire"));
            }
            if ($this->valF['ville_resident']=="") {
                $this->correct=false;
                $this->addToMessage(__("Le champ <b>Ville (résident)</b> est obligatoire"));
            }
        }
        // Provenance
        // Obligation de remplir provenance si type = "venant autre commune"
        if ($this->valF['types'] == $this->f->get_parametrage_mouvement_venantautrecommune()) {
            if ($this->valF['provenance']=="") {
                $this->correct=false;
                $this->addToMessage(__("Code provenance obligatoire pour type ").$this->f->get_parametrage_mouvement_venantautrecommune());
            } else {
                //// Test Correspondance avec libelle_lieu_de_naissance
                if (substr($this->valF['provenance'], 0, 2) != "99") {
                    // Test si le lieu/commune existe
                    $sql = sprintf(
                        'SELECT * FROM %1$scommune WHERE code=\'%2$s\'',
                        DB_PREFIXE,
                        $this->valF['provenance']
                    );
                    $res = $this->f->db->query($sql);
                    $this->addToLog(
                        __METHOD__."(): db->query(\"".$sql."\");",
                        VERBOSE_MODE
                    );
                    $this->f->isDatabaseError($res);
                    //
                    $nbligne = $res->numrows(); //XXX
                    $correct = true;
                    if ($nbligne==0) {
                        $this->correct=false;
                        $correct = false;
                        $this->addToMessage(__("Le code de provenance ").$this->valF['provenance'].__(" n'existe pas"));
                    }
                    if ($correct) {
                        //
                        $sql = sprintf(
                            'SELECT libelle_commune FROM %1$scommune WHERE code=\'%2$s\'',
                            DB_PREFIXE,
                            $this->valF['provenance']
                        );
                        $res = $this->f->db->query($sql);
                        $this->addToLog(
                            __METHOD__."(): db->query(\"".$sql."\");",
                            VERBOSE_MODE
                        );
                        $this->f->isDatabaseError($res);
                        //
                        $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
                        if ($row ['libelle_commune'] != $val['libelle_provenance']) {
                            $this->correct=false;
                            $this->addToMessage(__("La correspondance entre les deux champs Code de provenance / Libelle de provenance n'est pas correcte"));
                        }
                    }
                }
            }
            if ($this->valF['libelle_provenance']=="") {
                $this->correct=false;
                $this->addToMessage(__("Libelle provenance obligatoire pour type ").$this->f->get_parametrage_mouvement_venantautrecommune());
            }
        }
        // Si le bureau n'a pas de valeur et que nous ne sommes dans le cas
        // d'une création depuis une notification, alors on vérifie la cohérence
        // avec la valeur bureauforce.
        if (($this->valF['bureau'] == "" || $this->valF['bureau'] == null)
            && (isset($val["origin"]) !== true || $val["origin"] !== "from_reu_notification")) {
            //
            if ($this->valF['bureauforce'] == "") {
                $this->correct = false;
                $this->addToMessage(__("Il y a une incoherence dans la saisie, il n'est ".
                                "pas possible de forcer le bureau avec la valeur ".
                                "de 'Bureau force' positionnee a 'Non'."));
            }
            // Si on choisit de forcer le bureau, il est obligatoire de choisir
            // un bureau
            if ($this->valF['bureauforce'] == "Oui") {
                $this->correct = false;
                $this->addToMessage(__("Il y a une incoherence dans la saisie, il n'est ".
                                "pas possible de ne pas selectionner de bureau ".
                                "avec la valeur de 'Bureau force' positionnee a ".
                                "'Oui'."));
            }
            //
            if ($this->valF['bureauforce'] == "Non") {
                $this->correct = false;
                $this->addToMessage(__("Le choix du bureau ne peut pas etre automatique ".
                                "car il n'y a pas de bureau correspondant dans le ".
                                "decoupage pour cette adresse. Vous devez donc ".
                                "soit saisir une correspondance dans le decoupage ".
                                "soit forcer le bureau en le selectionnant."));
            }
        }
        //
        $this->addToLog(__METHOD__."(): this->correct = ".($this->correct?"true":"false"), VERBOSE_MODE);
        //
        $this->addToLog(__METHOD__."(): end verifier", VERBOSE_MODE);
    }

    /**
     *
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        // Mouvement deja traite
        if ($this->val[array_search('etat', $this->champs)] == "trs") {
            $this->correct = false;
            $this->addToMessage(__("Operation impossible. Vous ne pouvez pas ".
                                  "supprimer un mouvement deja traite.")."<br/>");
        }
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        parent::setVal($form, $maj, $validation);
        //
        if ($maj == 0) {
            $form->setVal("id", "[...]");
        }
        //
        if (($maj == 1 || $maj == 62) && $validation == 0) {
            $naissance_type_saisie = $this->get_naissance_type_saisie(
                $this->getVal("code_departement_naissance"),
                $this->getVal("libelle_departement_naissance"),
                $this->getVal("code_lieu_de_naissance"),
                $this->getVal("libelle_lieu_de_naissance")
            );
            $form->setVal('naissance_type_saisie', $naissance_type_saisie);
            if ($naissance_type_saisie === "Etranger") {
                $form->setVal('live_pays_de_naissance', $this->getVal("code_departement_naissance"));
            } elseif ($naissance_type_saisie === "Ancien_dep_franc") {
                $form->setVal('live_ancien_departement_francais_algerie', $this->getVal("code_departement_naissance"));
            } elseif ($naissance_type_saisie === "France") {
                $form->setVal('live_commune_de_naissance', $this->getVal("code_lieu_de_naissance"));
            }
        }
    }

    // {{{ Valorisation du formulaire

    /**
     *
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Le marqueur vu n'est jamais affiché sur la fiche
        $form->setType('vu', 'hidden');
        // Champs figés pour toutes les catégories de mouvement
        $form->setType('adresse_rattachement_reu', 'hiddenstatic');
        $form->setType('id', 'hiddenstatic');
        $form->setType('date_tableau', 'hiddenstaticdate');
        $form->setType('id_demande', 'hiddenstatic');
        $form->setType('ine', 'hiddenstatic');
        //
        $form->setType('ancien_bureau', 'hidden');
        //
        $form->setType('date_cnen', 'hidden');
        $form->setType('envoi_cnen', 'hidden');
        $form->setType('date_j5', 'hidden');
        $form->setType('tableau', 'hidden');
        $form->setType('electeur_id', 'electeur');
        //
        $form->setType('historique', 'hidden');
        $form->setType('archive_electeur', 'hidden');
        //
        $form->setType('provenance_demande', 'hidden');
        //
        $form->setType('bureau_de_vote_code', 'hidden');
        $form->setType('bureau_de_vote_libelle', 'hidden');
        if ($this->getVal("etat") == "trs" || $this->getVal("etat") == "na") {
            $form->setType('bureau_de_vote_code', 'hiddenstatic');
            $form->setType('bureau_de_vote_libelle', 'hiddenstatic');
            $form->setType('bureau', 'hidden');
        }
        //
        $form->setType('ancien_bureau_de_vote_code', 'hidden');
        $form->setType('ancien_bureau_de_vote_libelle', 'hidden');
        if ($this->getVal("etat") == "trs" || $this->getVal("etat") == "na") {
            $form->setType('ancien_bureau_de_vote_code', 'hiddenstatic');
            $form->setType('ancien_bureau_de_vote_libelle', 'hiddenstatic');
            $form->setType('ancien_bureau', 'hidden');
        }
        //
        if ($this->typeCat == "inscription") {
            $form->setType('ancien_bureau_de_vote_code', 'hidden');
            $form->setType('ancien_bureau_de_vote_libelle', 'hidden');
            $form->setType('ancien_bureau', 'hidden');
            if ($maj == 0 || $maj == 1) {
                $form->setType('liste', 'select');
                $form->setType('types', 'select');
                $form->setType('provenance', 'autocomplete');
                $form->setType('libelle_provenance', 'hidden');
                // Si un électeur a été sélectionné on récupère son ine 
                if ($this->getParameter("choix_ine") != "") {
                    // Le choix de l'ine peut avoir des caractère encodé en HTML qui sont écris avec ;
                    // ce qui va augmenter le nombre d'éléments renvoyé par le explode et cassser le
                    // array_combine. Ces éléments doivent donc être décodé avant le découpage
                    $paramElecteur = array_combine(
                        array('nom', 'prenom', 'ine'),
                        explode(';', html_entity_decode($this->getParameter("choix_ine"), ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401))
                    );
                }
                // Si un ine a été fourni ou qu'un électeur a été sélectionné et
                // qu'on a pu récupérer son ine
                if ($this->getVal("ine") != "" ||
                    ($this->getParameter("choix_ine") != "" &&
                        ! empty($paramElecteur['ine']) &&
                        $paramElecteur['ine'] != "-1")) {
                    //
                    $form->setType("sexe", "hiddenstatic");
                    $form->setType("nom", "hiddenstatic");
                    $form->setType("nom_usage", "text");
                    $form->setType("prenom", "hiddenstatic");
                    $form->setType("date_naissance", "hiddenstaticdate");
                    $form->setType("code_nationalite", "select");
                    //
                    $form->setType('naissance_type_saisie', 'hidden');
                    $form->setType('live_pays_de_naissance', 'hidden');
                    $form->setType('live_commune_de_naissance', 'hidden');
                    $form->setType('live_ancien_departement_francais_algerie', 'hidden');
                    $form->setType('code_departement_naissance', 'hiddenstatic');
                    $form->setType('libelle_departement_naissance', 'hiddenstatic');
                    $form->setType('code_lieu_de_naissance', 'hiddenstatic');
                    $form->setType('libelle_lieu_de_naissance', 'hiddenstatic');
                } else {
                    //
                    $form->setType("sexe", "select");
                    $form->setType("nom", "text");
                    $form->setType("nom_usage", "text");
                    $form->setType("prenom", "text");
                    $form->setType("date_naissance", "date");
                    $form->setType("code_nationalite", "select");
                    //
                    $form->setType('naissance_type_saisie', 'select');
                    $form->setType('live_pays_de_naissance', 'autocomplete');
                    $form->setType('live_ancien_departement_francais_algerie', 'autocomplete');
                    $form->setType('live_commune_de_naissance', 'autocomplete');
                    $form->setType('code_departement_naissance', 'text');
                    $form->setType('libelle_departement_naissance', 'text');
                    $form->setType('code_lieu_de_naissance', 'text');
                    $form->setType('libelle_lieu_de_naissance', 'text');
                }
                //
                $form->setType('bureauforce', 'select');
                //
                $form->setType('numero_habitation', 'text');
                $form->setType('complement_numero', 'select');
                $form->setType('code_voie', 'autocomplete');
                $form->setType('libelle_voie', 'hidden');
                //
                $form->setType('resident', 'select');
            }
            if ($maj == 62) {
                    //
                    $form->setType("observation", "hiddenstatic");
                    $form->setType("date_demande", "hiddenstaticdate");
                    $form->setType('types', 'selecthiddenstatic');
                    //
                    $form->setType("sexe", "hiddenstatic");
                    $form->setType("nom", "hiddenstatic");
                    $form->setType("nom_usage", "hiddenstatic");
                    $form->setType("prenom", "hiddenstatic");
                    $form->setType("date_naissance", "hiddenstaticdate");
                    $form->setType("code_nationalite", "selecthiddenstatic");
                    //
                    $form->setType('naissance_type_saisie', 'hidden');
                    $form->setType('live_pays_de_naissance', 'hidden');
                    $form->setType('live_commune_de_naissance', 'hidden');
                    $form->setType('live_ancien_departement_francais_algerie', 'hidden');
                    $form->setType('code_departement_naissance', 'hiddenstatic');
                    $form->setType('libelle_departement_naissance', 'hiddenstatic');
                    $form->setType('code_lieu_de_naissance', 'hiddenstatic');
                    $form->setType('libelle_lieu_de_naissance', 'hiddenstatic');
                    //
                    $form->setType('bureauforce', 'select');
                    //
                    $form->setType('numero_habitation', 'text');
                    $form->setType('complement_numero', 'select');
                    $form->setType('code_voie', 'autocomplete');
                    $form->setType('libelle_voie', 'hidden');
                    //
                    $form->setType('resident', 'select');
            }
            if ($maj == 2 || $maj == 3 || $maj == 62) {
                $form->setType('liste', 'selecthiddenstatic');
                $form->setType('provenance', 'hiddenstatic');
                $form->setType('libelle_provenance', 'hiddenstatic');
            }
        } elseif ($this->typeCat == 'radiation') {
            // Champs inutiles en radiation
            $form->setType("date_complet", "hidden");
            $form->setType('liste', 'selecthiddenstatic');
            $form->setType('provenance', 'hidden');
            $form->setType('libelle_provenance', 'hidden');
            //
            $form->setType("sexe", "hiddenstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("nom_usage", "hiddenstatic");
            $form->setType("prenom", "hiddenstatic");
            $form->setType("date_naissance", "hiddenstaticdate");
            $form->setType("code_nationalite", "selecthiddenstatic");
            $form->setType('naissance_type_saisie', 'hidden');
            $form->setType('live_pays_de_naissance', 'hidden');
            $form->setType('live_commune_de_naissance', 'hidden');
            $form->setType('live_ancien_departement_francais_algerie', 'hidden');
            $form->setType('code_departement_naissance', 'hiddenstatic');
            $form->setType('libelle_departement_naissance', 'hiddenstatic');
            $form->setType('code_lieu_de_naissance', 'hiddenstatic');
            $form->setType('libelle_lieu_de_naissance', 'hiddenstatic');
            //
            $form->setType('code_voie', 'hidden');
            $form->setType('libelle_voie', 'hiddenstatic');
            //
            $form->setType("numero_habitation", "hiddenstatic");
            $form->setType("complement_numero", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
            //
            $form->setType("bureau", "selecthiddenstatic");
            $form->setType("bureauforce", "hiddenstatic");
            //
            $form->setType("resident", "hiddenstatic");
            $form->setType("adresse_resident", "hiddenstatic");
            $form->setType("complement_resident", "hiddenstatic");
            $form->setType("cp_resident", "hiddenstatic");
            $form->setType("ville_resident", "hiddenstatic");
            $form->setType("telephone", "hiddenstatic");
            $form->setType("courriel", "hiddenstatic");
            //
            $form->setType('ancien_bureau', 'selecthiddenstatic');
            //
            if ($maj == 0 || $maj == 1) {
                $form->setType('types', 'select');
                $form->setType('observation', 'text');
            }
        } elseif ($this->typeCat == 'modification') {
            // Champs inutiles en modification
            $form->setType('provenance', 'hidden');
            $form->setType('libelle_provenance', 'hidden');
            $form->setType('liste', 'selecthiddenstatic');
            $form->setType('archive_electeur', 'infos_archive_electeur');
            //
            $form->setType('ancien_bureau', 'selecthiddenstatic');
            if ($maj == 0 || $maj == 1) {
                $form->setType('types', 'select');
                if (isset($this->electeur) === true
                    && array_key_exists("ine", $this->electeur) === true
                    && $this->electeur["ine"] !== "") {
                    //
                    $form->setType("sexe", "hiddenstatic");
                    $form->setType("nom", "hiddenstatic");
                    $form->setType("nom_usage", "text");
                    $form->setType("prenom", "hiddenstatic");
                    $form->setType("date_naissance", "hiddenstaticdate");
                    $form->setType("code_nationalite", "select");
                    $form->setType('naissance_type_saisie', 'hidden');
                    $form->setType('live_pays_de_naissance', 'hidden');
                    $form->setType('live_commune_de_naissance', 'hidden');
                    $form->setType('live_ancien_departement_francais_algerie', 'hidden');
                    $form->setType('code_departement_naissance', 'hiddenstatic');
                    $form->setType('libelle_departement_naissance', 'hiddenstatic');
                    $form->setType('code_lieu_de_naissance', 'hiddenstatic');
                    $form->setType('libelle_lieu_de_naissance', 'hiddenstatic');
                } else {
                    //
                    $form->setType("sexe", "select");
                    $form->setType("nom", "text");
                    $form->setType("nom_usage", "text");
                    $form->setType("prenom", "text");
                    $form->setType("date_naissance", "date");
                    $form->setType("code_nationalite", "select");
                    $form->setType('naissance_type_saisie', 'select');
                    $form->setType('live_pays_de_naissance', 'autocomplete');
                    $form->setType('live_ancien_departement_francais_algerie', 'autocomplete');
                    $form->setType('live_commune_de_naissance', 'autocomplete');
                    $form->setType('code_departement_naissance', 'text');
                    $form->setType('libelle_departement_naissance', 'text');
                    $form->setType('code_lieu_de_naissance', 'text');
                    $form->setType('libelle_lieu_de_naissance', 'text');
                }
                //
                $form->setType('bureauforce', 'select');
                //
                $form->setType('numero_habitation', 'text');
                $form->setType('complement_numero', 'select');
                $form->setType('code_voie', 'autocomplete');
                $form->setType('libelle_voie', 'hidden');
                //
                $form->setType('resident', 'select');
            }
        }
        // ajout
        if ($maj == 0) {
            $form->setType('date_modif', 'hidden');
            $form->setType('utilisateur', 'hidden');
            $form->setType('statut', 'hidden');
            $form->setType('etat', 'hidden');
            $form->setType('date_complet', 'hidden');
            $form->setType('date_visa', 'hidden');
            $form->setType('visa', 'hidden');
            $form->setType('id_demande', 'hidden');
            //
            $form->setType('types', 'select');
            $form->setType('date_demande', 'date');
        } else {
            $form->setType('date_modif', 'hiddenstaticdate');
            $form->setType('utilisateur', 'hiddenstatic');
            $form->setType('statut', 'hiddenstatic');
            $form->setType('date_visa', 'hiddenstaticdate');
            $form->setType('date_complet', 'hiddenstaticdate');
            $form->setType('visa', 'hiddenstatic');
            $form->setType("etat", "hiddenstatic");
            //
            //  suppression et en consultation
            if ($maj == 2 || $maj == 3) {
                //
                $form->setType('naissance_type_saisie', 'hidden');
                $form->setType('live_pays_de_naissance', 'hidden');
                $form->setType('live_commune_de_naissance', 'hidden');
                $form->setType('live_ancien_departement_francais_algerie', 'hidden');
                //
                $form->setType('code_departement_naissance', 'hiddenstatic');
                $form->setType('libelle_departement_naissance', 'hiddenstatic');
                $form->setType('code_lieu_de_naissance', 'hiddenstatic');
                $form->setType('libelle_lieu_de_naissance', 'hiddenstatic');
                //
                $form->setType('code_voie', 'hidden');
                $form->setType('libelle_voie', 'hiddenstatic');
                //
                $form->setType('types', 'selecthiddenstatic');
            }
        }
        // Champs inutiles pour tout le monde
        // On cache la civilité, elle est calculée automatiquement en fonction du sexe
        $form->setType('civilite', 'hidden');
        // On cache le numéro de l'élcteur dans le bureau inutile en modification
        $form->setType('numero_bureau', 'hidden');

        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation"
            || $maj == 502
            || $maj == 503) {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            // $form->setType($this->clePrimaire, "hiddenstatic");
        }
        // Viser
        if ($maj == 502) {
            $form->setType("visa", "select");
            $form->setType("date_visa", "date");
        }
        // Compléter
        if ($maj == 503) {
            $form->setType("date_complet", "date");
        }
    }

    /**
     *
     */
    function get_widget_config__provenance__autocomplete() {
        return  array(
            "obj" => "commune",
            "table" => "commune",
            "identifiant" => "commune.code",
            "criteres" => array(
                "commune.code" => __("code_commune"),
                "commune.libelle_commune" => __("Nom de la commune")
            ),
            "libelle" => array(
                "commune.code",
                "commune.libelle_commune"
            ),
            // Tables liées
            "jointures" => array(),
            // Permission d'ajouter
            "droit_ajout" => false,
        );
    }

    /**
     *
     */
    function get_widget_config__code_voie__autocomplete() {
        return array(
            "obj" => "voie",
            "table" => "voie",
            "criteres" => array(
                "voie.code" => __("Code voie"),
                "voie.libelle_voie" => __("libelle_voie")
            ),
            "identifiant" => "voie.code",
            "libelle" => array (
                "voie.libelle_voie"
            ),
            // Tables liées
            "jointures" => array(),
            // Permission d'ajouter
            "droit_ajout" => false,
            // Filtre sur collectivité
            "where" => "voie.om_collectivite=".intval($_SESSION["collectivite"]),
        );
    }

    /**
     *
     */
    function get_widget_config__live_pays_de_naissance__autocomplete() {
        return array(
            "obj" => "pays-naissance",
            "table" => "departement",
            "identifiant" => "departement.code",
            "criteres" => array(
                "departement.code" => __("code insee pays"),
                "departement.libelle_departement" => __("libellé du pays"),
            ),
            "libelle" => array (
                "departement.code",
                "departement.libelle_departement"
            ),
            "jointures" => array(),
            "droit_ajout" => false,
            "where" => " (departement.code = '99' OR (character_length(departement.code) = 5 AND departement.code::int >= 99000)) ",
        );
    }

    /**
     *
     */
    function get_widget_config__live_ancien_departement_francais_algerie__autocomplete() {
        return array(
            "obj" => "ancien-departement-francais-algerie",
            "table" => "departement",
            "identifiant" => "departement.code",
            "criteres" => array(
                "departement.code" => __("code insee pays"),
                "departement.libelle_departement" => __("libellé du pays"),
            ),
            "libelle" => array (
                "departement.code",
                "departement.libelle_departement"
            ),
            "jointures" => array(),
            "droit_ajout" => false,
            "where" => "((character_length(departement.code) = 5 AND departement.code >= '91352') AND (character_length(departement.code) = 5 AND  departement.code <= '94352'))",
        );
    }

    /**
     *
     */
    function get_widget_config__live_commune_de_naissance__autocomplete() {
        return array(
            "obj" => "commune-naissance",
            "table" => "commune",
            "identifiant" => "commune.code",
            "criteres" => array(
                "commune.code" => __("code_commune"),
                "commune.libelle_commune" => __("Nom de la commune")
            ),
            "libelle" => array (
                "commune.code",
                "commune.libelle_commune"
            ),
            "jointures" => array(),
            "droit_ajout" => false,
        );
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        //
        if ($maj == 999) {
            // SPECIFIQUE type de categorie de mouvement
            $sql_type = sprintf(
                'SELECT
                    code,
                    CASE WHEN 
                        (om_validite_fin < CURRENT_DATE)
                        THEN concat(\'--- \', libelle)
                    ELSE
                        libelle
                    END AS lib
                FROM %1$sparam_mouvement WHERE lower(typecat)=\'%2$s\' order by om_validite_fin DESC NULLS FIRST, libelle',
                DB_PREFIXE,
                "inscription"
            );
            $this->init_select($form, $this->f->db, $maj, false, "motif_inscription", $sql_type, null, true);
            // SPECIFIQUE type de categorie de mouvement
            $sql_type = sprintf(
                'SELECT
                    code,
                    CASE WHEN 
                        (om_validite_fin < CURRENT_DATE)
                        THEN concat(\'--- \', libelle)
                    ELSE
                        libelle
                    END AS lib
                FROM %1$sparam_mouvement WHERE lower(typecat)=\'%2$s\' order by om_validite_fin DESC NULLS FIRST, libelle',
                DB_PREFIXE,
                "radiation"
            );
            $this->init_select($form, $this->f->db, $maj, false, "motif_radiation", $sql_type, null, true);
            // SPECIFIQUE type de categorie de mouvement
            $sql_type = sprintf(
                'SELECT
                    code,
                    CASE WHEN 
                        (om_validite_fin < CURRENT_DATE)
                        THEN concat(\'--- \', libelle)
                    ELSE
                        libelle
                    END AS lib
                FROM %1$sparam_mouvement WHERE lower(typecat)=\'%2$s\' order by om_validite_fin DESC NULLS FIRST, libelle',
                DB_PREFIXE,
                "modification"
            );
            $this->init_select($form, $this->f->db, $maj, false, "motif_modification", $sql_type, null, true);
        }
        $form->setSelect("archive_electeur", json_decode($this->getVal("archive_electeur"), true));
        // mouvement.etat
        $form->setSelect("etat", array(
            array("", "actif", "na", "trs", ),
            array(__("Choisir etat"), "(actif) actif", "(na) non applicable", "(trs) traité"),
        ));
        // mouvement.statut
        $form->setSelect("statut", array(
            array("", "ouvert", "abandonne", "en_attente", "complet", "vise_maire", "vise_insee", "accepte", "refuse", ),
            array(__("Choisir statut"), __("ouvert"), __("abandonné"), __("en attente"), __("complet"), __("visé maire"), __("visé insee"), __("accepté"), __("refusé"), ),
        ));
        // mouvement.visa
        $form->setSelect("visa", array(
            array("", "accepte", "refuse", ),
            array(__("Choisir visa"), _("accepté"), _("refusé"), ),
        ));
        // Si Mode Ajout ou Modification (on a pas de select en mode Suppression)
        if ($maj == 0 || $maj == 1 || $maj == 62) {
            //
            $form->setSelect(
                "code_voie",
                $this->get_widget_config__code_voie__autocomplete()
            );
            //
            $form->setSelect(
                "provenance",
                $this->get_widget_config__provenance__autocomplete()
            );
            //
            $form->setSelect(
                "live_commune_de_naissance",
                $this->get_widget_config__live_commune_de_naissance__autocomplete()
            );
            //
            $form->setSelect(
                "live_pays_de_naissance",
                $this->get_widget_config__live_pays_de_naissance__autocomplete()
            );
            //
            $form->setSelect(
                "live_ancien_departement_francais_algerie",
                $this->get_widget_config__live_ancien_departement_francais_algerie__autocomplete()
            );

            /**
             * DYNAMIQUES
             */
            // nationalite
            $res = $this->f->db->query($this->get_var_sql_forminc__sql("nationalite"));
            $this->addToLog(
                __METHOD__."(): db->query(\"".$this->get_var_sql_forminc__sql("nationalite")."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            $contenu = array(
                0 => array("", ),
                1 => array(__("Choix de la nationalite"), ),
            );
            $k=1;
            while ($row=& $res->fetchRow())
            {
                $contenu[0][$k]=$row[0];
                $contenu[1][$k]=$row[1];
                $k++;
            }
            $form->setSelect("code_nationalite",$contenu);

            /**
             * STATIQUES
             */
            // civilite
            $form->setSelect("civilite", array(
                0 => array("M.", "Mme", "Mlle", ),
                1 => array(__("Monsieur"), __("Madame"), __("Mademoiselle"), ),
            ));
            // Sexe
            $form->setSelect("sexe", array(
                0 => array("M", "F", ),
                1 => array(__("Masculin"), __("Feminin"), ),
            ));
            //lieu de naissance
            $contenu = array(
                0 => array("France", "Etranger", "Ancien_dep_franc", ),
                1 => array(__("Né en France"), __("Né  à l'étranger"), __("Né dans un ancien département français d'Algérie"), ),
            );
            $naissance_type_saisie = $this->get_naissance_type_saisie(
                $form->val["code_departement_naissance"],
                $form->val["libelle_departement_naissance"],
                $form->val["code_lieu_de_naissance"],
                $form->val["libelle_lieu_de_naissance"]
            );
            if ($naissance_type_saisie == "autre"
                && ($form->val["code_departement_naissance"] != ""
                    && $form->val["libelle_departement_naissance"] != ""
                    && $form->val["code_lieu_de_naissance"] != ""
                    && $form->val["libelle_lieu_de_naissance"] != "")) {
                $contenu[0][] = "autre";
                $contenu[1][] = __("Lieu de naissance non référencé");
            }
            $form->setSelect("naissance_type_saisie", $contenu);
            // complement
            $form->setSelect("complement_numero", array(
                0 => array("", "bis", "ter", "quater", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"),
                1 => array(__("Sans"),__("bis"),__("ter"),__("quater"), "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"),
            ));
            // bureauforce
            $form->setSelect("bureauforce", array(
                0 => array("Non", "Oui", ),
                1 => array(__("Non"), __("Oui"), ),
            ));
            // resident
            $form->setSelect("resident", array(
                0 => array("Non", "Oui", ),
                1 => array(__("Non"), __("Oui"), ),
            ));
        }
        if ($maj == 0 || $maj == 1 || $maj == 2 || $maj == 3 || $maj == 62) {
            // SPECIFIQUE type de categorie de mouvement
            $sql_type_common = sprintf(
                'SELECT code, libelle FROM %1$sparam_mouvement',
                DB_PREFIXE
            );
            $sql_type_where_validite = " ((param_mouvement.om_validite_debut IS NULL AND (param_mouvement.om_validite_fin IS NULL OR param_mouvement.om_validite_fin > CURRENT_DATE)) OR (param_mouvement.om_validite_debut <= CURRENT_DATE AND (param_mouvement.om_validite_fin IS NULL OR param_mouvement.om_validite_fin > CURRENT_DATE))) ";
            //
            $sql_type_M = $sql_type_common." WHERE typecat like 'Modification' AND ".$sql_type_where_validite." order by libelle";
            $sql_type_I = $sql_type_common." WHERE typeCat like 'Inscription' AND ".$sql_type_where_validite." order by libelle";
            $sql_type_R = $sql_type_common." WHERE typecat like 'Radiation' AND ".$sql_type_where_validite." order by libelle";
            //
            $sql_type_by_id = $sql_type_common." WHERE code = '<idx>'";
            if ($this->typeCat == 'inscription') {
                $sql = $sql_type_I;
            } elseif ($this->typeCat == 'radiation') {
                $sql = $sql_type_R;
            } else {
                $sql = $sql_type_M;
            }
            $this->init_select($form, $this->f->db, $maj, false, "types", $sql, $sql_type_by_id, true);
            //
            $electeur_id = $this->getVal("electeur_id");
            if ($electeur_id == "") {
                if (isset($this->electeur)
                    && array_key_exists("id", $this->electeur)
                    && $this->electeur["id"] != "") {
                    $electeur_id = $this->electeur["id"];
                } else {
                    $electeur_id = 0;
                }
            }
            $inst_electeur = $this->f->get_inst__om_dbform(array(
                "obj" => "electeur",
                "idx" => $electeur_id,
            ));
            $electeur_infos = $inst_electeur->get_infos();
            $form->setSelect("electeur_id", $electeur_infos);
        }
    }

    /**
     *
     */
    function setOnchange (&$form, $maj, $valE = array()) {
        parent::setOnchange($form, $maj);
        if( !isset($valE['bureau']) ) $valE['bureau'] = "";
        $form->setOnchange("nom","this.value=this.value.toUpperCase()");
        $form->setOnchange("prenom","this.value=this.value.toUpperCase()");
        $form->setOnchange("nom_usage","this.value=this.value.toUpperCase()");

        $form->setOnchange("libelle_voie","this.value=this.value.toUpperCase()");
        $form->setOnchange("complement","this.value=this.value.toUpperCase()");

        $form->setOnchange("libelle_departement_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_lieu_de_naissance","this.value=this.value.toUpperCase()");
        $form->setOnchange("libelle_provenance","this.value=this.value.toUpperCase()");

        $form->setOnchange("adresse_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("complement_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("cp_resident","this.value=this.value.toUpperCase()");
        $form->setOnchange("ville_resident","this.value=this.value.toUpperCase()");

        $form->setOnchange("bureau","forcerlebureau('".$valE['bureau']."')");
        $form->setOnchange("bureauforce","nepasforcerlebureau()");
        //
        $form->setOnchange("telephone", "VerifNum(this)");
        $form->setOnchange("courriel", "is_email(this)");
        //
        $form->setOnchange("naissance_type_saisie", "clear_naissance_form_widget();");
    }
    /**
     *
     */
    function setLib (&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("bureauforce", __("Bureau forcé ?"));
        $form->setLib("bureau", __("Bureau de vote"));
        $form->setLib("bureau_de_vote_code", __("Bureau de vote"));
        $form->setLib("bureau_de_vote_libelle", "");
        $form->setLib("ancien_bureau", __("Ancien bureau de vote"));
        $form->setLib("ancien_bureau_de_vote_code", __("Ancien bureau de vote"));
        $form->setLib("ancien_bureau_de_vote_libelle", "");
        //
        $form->setLib('id', 'Id');
        $form->setLib('id_demande', __(' externe : '));
        $form->setLib('date_demande', __('Date de la demande'));
        $form->setLib('electeur_id', __('Id électeur'));
        $form->setLib('ine', __('INE'));
        $form->setLib('om_collectivite', __("Collectvité"));
        //
        $form->setLib('numero_bureau', __("n° de l'électeur dans le bureau"));
        //
        $form->setLib('liste', __("Liste"));
        //
        $form->setLib('date_modif', __('Modifie le '));
        $form->setLib('utilisateur', __(' par '));
        $form->setLib('types', __('Type '));
        $form->setLib('date_tableau', __('Tableau du'));
        //Etat Civil
        $form->setLib('nom',__('Nom patronymique'));
        $form->setLib('prenom',__('Prenom'));
        $form->setLib('civilite',__('Civilite'));
        $form->setLib('sexe',__('Sexe'));
        $form->setLib('nom_usage',__("Nom d'usage"));
        // Naissance & Nationalite
        $form->setLib('date_naissance',__('Date de naissance'));
        $form->setLib("naissance_type_saisie", '');
        $form->setLib('code_departement_naissance', __('Code département/pays de naissance'));
        $form->setLib('libelle_departement_naissance', __('Libellé département/pays de naissance'));
        $form->setLib('code_lieu_de_naissance', __('Code lieu/commune de naissance'));
        $form->setLib('libelle_lieu_de_naissance', __('Libellé commune de naissance')." *");
        $form->setLib('live_commune_de_naissance', __('Commune de naissance')." *");
        $form->setLib('live_pays_de_naissance', __('Pays de naissance')." *");
        $form->setLib('live_ancien_departement_francais_algerie',__('Département de naissance')." *");
        $form->setLib('code_nationalite',__('Nationalite'));
        // Adresse
        $form->setLib('numero_habitation',__('Numero'));
        $form->setLib('code_voie', __('Voie'));
        $form->setLib('libelle_voie', '');
        $form->setLib('complement_numero', '');
        $form->setLib('complement', __('Complement'));
        $form->setLib('adresse_rattachement_reu', __('Infos REU'));
        // Resident
        $form->setLib('resident', __('Adresse de contact'));
        $form->setLib('adresse_resident', __('Adresse').'<span class="required-conditional"> *</span>');
        $form->setLib('complement_resident', __('Complement'));
        $form->setLib('cp_resident', __('Code postal').'<span class="required-conditional"> *</span>');
        $form->setLib('ville_resident', __('Ville').'<span class="required-conditional"> *</span>');
        // Provenance
        $form->setLib("provenance", __("Commune de provenance"));
        $form->setLib('libelle_provenance',' ');
        //
        $form->setLib("etat", __("État"));
        $form->setLib('tableau',__(' [Tableau] '));
        
        $form->setLib('envoi_cnen',__(' [INSEE] '));
        $form->setLib('date_cnen','');
        //
        if ($maj == 3
            || $maj == 2
            || ($this->typeCat == "radiation")) {
            //
            $naissance_type_saisie = $this->get_naissance_type_saisie(
                $this->getVal("code_departement_naissance"),
                $this->getVal("libelle_departement_naissance"),
                $this->getVal("code_lieu_de_naissance"),
                $this->getVal("libelle_lieu_de_naissance")
            );
            if ($naissance_type_saisie === "Etranger") {
                $form->setLib('code_departement_naissance', __('Pays'));
                $form->setLib('libelle_departement_naissance', '');
                $form->setLib('code_lieu_de_naissance', __('Commune'));
                $form->setLib('libelle_lieu_de_naissance', '');
            } elseif ($naissance_type_saisie === "France"
                || $naissance_type_saisie === "Ancien_dep_franc") {
                //
                $form->setLib('code_departement_naissance', __('Département'));
                $form->setLib('libelle_departement_naissance', '');
                $form->setLib('code_lieu_de_naissance', __('Commune'));
                $form->setLib('libelle_lieu_de_naissance', '');
            } elseif ($naissance_type_saisie === "autre") {
                //
                $form->setLib('code_departement_naissance', __('Département/Pays'));
                $form->setLib('libelle_departement_naissance', '');
                $form->setLib('code_lieu_de_naissance', __('Commune'));
                $form->setLib('libelle_lieu_de_naissance', '');
            }
            //
            $form->setLib('code_voie', __('Voie'));
            $form->setLib('libelle_voie', __('Voie'));
        }
        //
        $form->setLib('observation', "");
        $form->setLib('archive_electeur', "");
        $form->setLib('courriel', __('Courriel'));
        $form->setLib('telephone', __('Téléphone'));
    }

    /**
     *
     */
    function setTaille (&$form, $maj) {
        parent::setTaille($form, $maj);
        //
        $form->setTaille('nom',40);
        $form->setTaille('nom_usage',30);
        $form->setTaille('prenom',40);
        $form->setTaille('libelle_commune',30);
        $form->setTaille('provenance_commune',30);
        $form->setTaille('code_departement_naissance',6);
        $form->setTaille('libelle_departement_naissance',30);
        $form->setTaille('code_lieu_de_naissance',6);
        $form->setTaille('libelle_lieu_de_naissance',30);
        $form->setTaille('date_naissance',10);
        $form->setTaille('code_voie',6);
        $form->setTaille('libelle_voie',30);
        $form->setTaille('complement',30);
        $form->setTaille('provenance',6);
        $form->setTaille('libelle_provenance',30);
        $form->setTaille('observation',85);
        $form->setTaille('resident',3);
        $form->setTaille('adresse_resident',20);
        $form->setTaille('complement_resident',20);
        $form->setTaille('ville_resident',10);
    }

    /**
     *
     */
    function setMax(&$form, $maj) {
        parent::setMax($form, $maj);
        //
        $form->setMax('nom',40);
        $form->setMax('nom_usage',40);
        $form->setMax('prenom',40);
        $form->setMax('libelle_voie',30);
        $form->setMax('libelle_commune',30);
        $form->setMax('provenance_commune',30);
        $form->setMax('code_departement_naissance',5);
        $form->setMax('libelle_departement_naissance',30);
        $form->setMax('code_lieu_de_naissance',6);
        $form->setMax('libelle_lieu_de_naissance',30);
        $form->setMax('code_voie',6);
        $form->setMax('libelle_voie',30);
        $form->setMax('complement',80);
        $form->setMax('provenance',6);
        $form->setMax('libelle_provenance',30);
        $form->setMax('observation',100);
        $form->setMax('resident',3);
        $form->setMax('adresse_resident',40);
        $form->setMax('complement_resident',80);
        $form->setMax('ville_resident',30);
    }

    /**
     *
     */
    function setLayout(&$form, $maj) {
        // Vérifie si une adresse de rattachement est saisie et si elle est identique
        // à celle de contact. Si c'est le cas averti l'utilisateur.
        if ($this->getVal('resident') === 'Oui') {
            $inst_voie = $this->f->get_inst__om_dbform(array(
                "obj" => "voie",
                "idx" => $this->getVal('code_voie'),
            ));
            $adresse_contact = sprintf(
                '%s %s %s',
                $this->getVal('numero_habitation'),
                $this->getVal('libelle_voie'),
                str_replace(' ', '', $inst_voie->getVal('cp'))
            );
            $adresse_rattachement = sprintf(
                '%s %s',
                $this->getVal('adresse_resident'),
                str_replace(' ', '', $this->getVal('cp_resident'))
            );
            if ($adresse_contact === $adresse_rattachement) {
                ob_start();
                $this->f->layout->display_message('warning', __("L'adresse de rattachement de l'électeur est identique à son adresse de contact."));
                $message = ob_get_clean();
                $message = str_replace('message', 'panel_information', $message);
                echo $message;
            }
        }
        
        $form->setBloc($this->clePrimaire, 'D', '', "form-".$this->get_absolute_class_name()."-maj-".$this->getParameter("maj"));
        //
        $form->setFieldset($this->clePrimaire, 'D', __('Identification'));
        $form->setBloc("id", "D", "", "");
        $form->setBloc("id", "D", "", "group");
        $form->setBloc("id", "DF", "", "group first-fix-width");
        $form->setBloc("id_demande", "F", "");
        $form->setBloc("id_demande", "F", "");
        // $form->setBloc('id', 'D', '', 'group first-fix-width');
        // $form->setBloc('id_demande', 'F', '', 'first-fix-width');
        $form->setBloc('electeur_id', 'DF', '', 'first-fix-width');
        $form->setBloc('ine', 'DF', '', 'first-fix-width');
        $form->setBloc('types', 'DF', '', 'first-fix-width');
        $form->setBloc('etat', 'DF', '', 'first-fix-width');
        $form->setBloc('vu', 'DF', '', 'first-fix-width');
        $form->setBloc('liste', 'DF', '', 'first-fix-width');
        $form->setBloc("date_modif", "D", "", "");
        $form->setBloc("date_modif", "D", "", "group");
        $form->setBloc("date_modif", "DF", "", "group first-fix-width");
        $form->setBloc("utilisateur", "F", "");
        $form->setBloc("utilisateur", "F", "");
        $form->setBloc('om_collectivite', 'DF', "", "first-fix-width");
        $form->setBloc('date_complet', 'DF', "", "first-fix-width");
        $form->setBloc('date_demande', 'DF', "", "first-fix-width");
        $form->setBloc('date_visa', 'DF', "", "first-fix-width");
        $form->setBloc('visa', 'DF', "", "first-fix-width");
        $form->setBloc('statut', 'DF', "", "first-fix-width");
        $form->setBloc('date_tableau', 'DF', "", "first-fix-width");
        $form->setFieldset('vu', 'F', '');
        //
        $form->setFieldset('electeur_id', 'D', __('Électeur'));
        $form->setBloc('electeur_id', 'DF', '', 'first-fix-width');
        $form->setBloc('ine', 'DF', '', 'first-fix-width');
        $form->setFieldset('ine', 'F');
        //
        $form->setFieldset('bureau', 'D', __('Bureau'));
        $form->setBloc('bureau', 'DF', '', 'first-fix-width');
        $form->setBloc('bureau_de_vote_code', 'D', '', 'group first-fix-width');
        $form->setBloc('bureau_de_vote_libelle', 'F');
        $form->setBloc('ancien_bureau', 'DF', '', 'first-fix-width');
        $form->setBloc('ancien_bureau_de_vote_code', 'D', '', 'group first-fix-width');
        $form->setBloc('ancien_bureau_de_vote_libelle', 'F');
        $form->setBloc('bureauforce', 'DF', '', 'first-fix-width');
        $form->setBloc('numero_bureau', 'DF', '', 'first-fix-width');
        $form->setFieldset('numero_bureau', 'F', '');
        //
        $form->setFieldset('civilite', 'D', __('Etat Civil'));
        $form->setBloc('civilite', 'DF', '', 'first-fix-width');
        $form->setBloc('sexe', 'DF', '', 'first-fix-width');
        $form->setBloc('nom', 'DF', '', 'first-fix-width');
        $form->setBloc('nom_usage', 'DF', '', 'first-fix-width');
        $form->setBloc('prenom', 'DF', '', 'group first-fix-width');
        $form->setBloc('date_naissance', 'DF', '', 'first-fix-width');
        $form->setBloc('naissance_type_saisie', 'DF', '', 'first-fix-width');
        //
        if ($maj == 3
            || $maj == 2
            || $this->typeCat == "radiation") {
            //
            $form->setBloc('code_departement_naissance', 'D', '', '');
            $form->setBloc('code_departement_naissance', 'D', '', 'group');
            $form->setBloc('code_departement_naissance', 'DF', '', 'first-fix-width');
            $form->setBloc('libelle_departement_naissance', 'F');
            $form->setBloc('libelle_departement_naissance', 'F');
            //
            $form->setBloc('code_lieu_de_naissance', 'D', '', '');
            $form->setBloc('code_lieu_de_naissance', 'D', '', 'group');
            $form->setBloc('code_lieu_de_naissance', 'DF', '', 'first-fix-width');
            $form->setBloc('libelle_lieu_de_naissance', 'F');
            $form->setBloc('libelle_lieu_de_naissance', 'F');
        } else {
            $form->setBloc('code_departement_naissance', 'DF', '', 'first-fix-width field-code_departement_naissance');
            $form->setBloc('libelle_departement_naissance', 'DF', '', 'first-fix-width field-libelle_departement_naissance');
            $form->setBloc('code_lieu_de_naissance', 'DF', '', 'first-fix-width field-code_lieu_de_naissance');
            $form->setBloc('libelle_lieu_de_naissance', 'DF', '', 'first-fix-width field-libelle_lieu_de_naissance');
            $form->setBloc('live_pays_de_naissance', 'DF', '', 'first-fix-width field-live_pays_de_naissance');
            $form->setBloc('live_ancien_departement_francais_algerie', 'DF', '', 'first-fix-width field-live_ancien_departement_francais_algerie');
            $form->setBloc('live_commune_de_naissance', 'DF', '', 'first-fix-width field-live_commune_de_naissance');
        }
        //
        $form->setBloc('code_nationalite', 'DF', '', 'first-fix-width');
        $form->setFieldset('code_nationalite', 'F', '');
        //
        $form->setFieldset('numero_habitation', 'D', __('Adresse de rattachement'), '');
        $form->setBloc('numero_habitation', 'D', '', '');
        $form->setBloc('numero_habitation', 'D', '', 'group');
        $form->setBloc('numero_habitation', 'DF', '', 'first-fix-width');
        $form->setBloc('complement_numero', 'F');
        $form->setBloc('complement_numero', 'F');
        $form->setBloc('code_voie', 'D', '', '');
        $form->setBloc('code_voie', 'D', '', 'group');
        $form->setBloc('code_voie', 'DF', '', 'first-fix-width');
        $form->setBloc('libelle_voie', 'DF', '', 'first-fix-width');
        $form->setBloc('libelle_voie', 'F');
        $form->setBloc('libelle_voie', 'F');
        $form->setBloc('complement', 'DF', '', 'first-fix-width');
        $form->setBloc('adresse_rattachement_reu', 'DF', '', 'first-fix-width last-elem-bloc-adresse');
        $form->setFieldset('adresse_rattachement_reu', 'F', '');
        //
        $form->setFieldset('resident', 'D', __('Informations de contact'), "");
        $form->setBloc('resident', 'DF', '', 'first-fix-width');
        $form->setBloc('adresse_resident', 'D', '', 'resident-adresse');
        $form->setBloc('adresse_resident', 'DF', '', 'first-fix-width');
        $form->setBloc('complement_resident', 'DF', '', 'first-fix-width');
        $form->setBloc('cp_resident', 'DF', '', 'first-fix-width');
        $form->setBloc('ville_resident', 'DF', '', 'first-fix-width');
        $form->setBloc('ville_resident', 'F', '', '');
        $form->setBloc('telephone', 'DF', '', 'first-fix-width');
        $form->setBloc('courriel', 'DF', '', 'first-fix-width');
        $form->setFieldset('courriel', 'F', '');
        //
        $form->setFieldset('provenance', 'D', __('Provenance'), "");
        $form->setBloc('provenance', 'D', '', '');
        $form->setBloc('provenance', 'D', '', 'group');
        $form->setBloc('provenance', 'DF', '', 'first-fix-width');
        $form->setBloc('libelle_provenance', 'F');
        $form->setBloc('libelle_provenance', 'F');
        $form->setFieldset('libelle_provenance', 'F', '');
        //
        $form->setFieldset('observation', 'D', __('Observations'));
        $form->setFieldset('observation', 'F');
        $form->setFieldset('archive_electeur', 'D', __("Informations de l'électeur avant modification"), "startClosed");
        $form->setFieldset('archive_electeur', 'F');
        //
        $form->setBloc("archive_electeur", "F");
    }

    // }}}

    // {{{ Gestion du bureau, bureauforce et numero_bureau

    /**
     *
     */
    function manageBureauForceAndCodeBureau($val = array()) {
        // Si le bureau n'est pas force
        if ($val['bureauforce'] == "Non") {
            $this->valF['bureauforce'] = "Non";
            // Recuperation du code bureau en fonction du decoupage et de
            // l'adresse
            $bureau = $this->getBureauFromDecoupage($this->valF['code_voie'], $this->valF['numero_habitation']);
            // Si le decoupage nous donne un bureau
            if ($bureau !== null) {
                // On affecte le code bureau calcule en fonction du
                // decoupage
                $this->valF['bureau'] = $bureau;
                // On affecte le numero dans le bureau
                $this->setValFNumeroBureau();
            }
        } else { // Si le bureau est force
            $this->valF['bureauforce'] = "Oui";
            // Si un bureau est selectionne
            if ($val['bureau'] != "") {
                // On affecte le code bureau selectionne par l'utilisateur
                $this->valF['bureau'] = $val['bureau'];
                // On affecte le numero dans le bureau
                $this->setValFNumeroBureau();
            }
        }
    }

    /**
     *
     */
    function setValFNumeroBureau() {}

    /**
     *
     */
    function getBureauFromDecoupage($code_voie = 0, $numero_habitation = 0) {
        //
        $bureau = NULL;
        //
        if (intval($numero_habitation) % 2 == 0) {
            // Pair
            $sql = sprintf(
                'SELECT bureau FROM %1$sdecoupage WHERE code_voie=\'%2$s\' AND (premier_pair<=%3$s AND dernier_pair>=%3$s)',
                DB_PREFIXE,
                $code_voie,
                intval($numero_habitation)
            );
        } else {
            // Impair
            $sql = sprintf(
                'SELECT bureau FROM %1$sdecoupage WHERE code_voie=\'%2$s\' AND (premier_impair<=%3$s AND dernier_impair>=%3$s)',
                DB_PREFIXE,
                $code_voie,
                intval($numero_habitation)
            );
        }
        $res = $this->f->db->getone($sql);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        if ($res != NULL) {
            //
            $bureau = $res;
        }
        //
        return $bureau;
    }

    // }}}


    /**
     * Statistiques - 'mouvement_bureau'.
     *
     * Nombre d'inscription office (code insee 8), d'inscription nouvelle (code insee 1),
     * et de décès (code insee D) par bureau pour la liste et la date de tableau en cours
     *
     * @return array
     */
    function compute_stats__mouvement_bureau() {
        //
        $codes = array (
            0 => array ('titre' => __("Inscription D'office (8)"),
                        'champ' => "codeinscription",
                        'valeur' => "8"),
            1 => array ('titre' => __("Inscription Nouvelle (1)"),
                        'champ' => "codeinscription",
                        'valeur' => "1"),
            2 => array ('titre' => __("Deces (D)"),
                        'champ' => "coderadiation",
                        'valeur' => "D")
        );
        //
        $data = array();
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
            $datas = array();
            array_push($datas, $bureau["code"]);
            array_push($datas, $bureau["libelle"]);
            //
            foreach ($codes as $c) {
                /**
                 * Cette requete permet de compter tous les mouvements de la date de tableau
                 * en cours en fonction de la collectivite en cours, de la liste en cours,
                 * du bureau selectionne et du champ supplementaire donne
                 *
                 * @param string $bureau["code"] Code du bureau
                 * @param string $_SESSION["collectivite"]
                 * @param string $_SESSION["liste"]
                 * @param string $this->f->getParameter("datetableau")
                 * @param string $c['champ']
                 * @param string $c['valeur']
                 */
                $query_count_mouvement = sprintf(
                    'SELECT count(*) FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE mouvement.liste=\'%3$s\' and mouvement.om_collectivite=%2$s and mouvement.bureau=%5$s and mouvement.date_tableau=\'%4$s\' and %6$s=\'%7$s\'',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $_SESSION["liste"],
                    $this->f->getParameter("datetableau"),
                    intval($bureau["id"]),
                    $c['champ'],
                    $c['valeur']
                );
                //
                $res_count_mouvement = $this->f->db->getOne($query_count_mouvement);
                $this->f->isDatabaseError($res_count_mouvement);
                //
                array_push($datas, $res_count_mouvement);
            }
            //
            $data[] = $datas;
        }
        //
        $offset = array();
        $column = array();
        array_push($offset, 10);
        array_push($column, "");
        array_push($offset, 70);
        array_push($column, __("LISTE")." : ".$_SESSION["libelle_liste"]);
        foreach ($codes as $c) {
            array_push($offset, 0);
            array_push($column, $c['titre']);
        }
        // Array
        return array(
            "format" => "P",
            "title" => __("Statistiques - Mouvement par bureau"),
            "subtitle" => __("Details Inscription (Office,Nouvelle) et Deces."),
            "offset" => $offset,
            "column" => $column,
            "data" => $data,
            "output" => "stats-mouvementbureau"
        );
    }

    /**
     * Statistiques - 'mouvement_voie'.
     *
     * Nombre d'inscription office (code insee 8), d'inscription nouvelle (code insee 1),
     * et de décès (code insee D) par voie pour la liste et la date de tableau en cours
     *
     * @return array
     */
    function compute_stats__mouvement_voie() {

        // Recuperation des donnees
        $data = array();

        //
        $sql_voie = sprintf(
            'SELECT * FROM %1$svoie WHERE voie.om_collectivite=%2$s ORDER BY voie.libelle_voie ASC',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );

        $res = $this->f->db->query($sql_voie);
        $this->f->isDatabaseError($res);

        $codes = array (
            0 => array ('champ' => "codeinscription", 'valeur' => "8"),
            1 => array ('champ' => "codeinscription", 'valeur' => "1"),
            2 => array ('champ' => "coderadiation", 'valeur' => "D")
            );

        //
        $numArray = 0;
        while ($row =& $res->fetchRow (DB_FETCHMODE_ASSOC)) {
            //
            $datas = array();
            //
            array_push($datas, $row['code']);
            array_push($datas, $row['libelle_voie']);
            //
            foreach ($codes as $c) {
                //
                $sql1 = sprintf(
                    'SELECT count(id) FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE %6$s=\'%7$s\' AND mouvement.code_voie=\'%5$s\' AND mouvement.date_tableau=\'%4$s\' AND mouvement.liste=\'%3$s\' AND mouvement.om_collectivite=%2$s',
                    DB_PREFIXE,
                    intval($_SESSION["collectivite"]),
                    $_SESSION["liste"],
                    $this->f->getParameter("datetableau"),
                    $row['code'],
                    $c['champ'],
                    $c['valeur']
                );
                $res1 = $this->f->db->getone($sql1);
                $this->f->isDatabaseError($res1);
                array_push($datas, $res1);
            }
            //
            $data[$numArray] = $datas;
            $numArray++;
        }
        // Array
        return array(
            "format" => "L",
            "title" => __("Statistiques - Mouvement par voie"),
            "subtitle" => __("Details Inscription (Office,Nouvelle) et Deces."),
            "offset" => array(10,0,50,50,40),
            "column" => array(
                "",
                __("LISTE")." : ".$_SESSION["libelle_liste"]."",
                __("Inscription D'office (8)"),
                __("Inscription Nouvelle (1)"),
                __("Deces (D)")
                ),
            "data" => $data,
            "output" => "stats-mouvementvoie"
        );
    }


    /**
     * Statistiques - 'mouvement_io_sexe_bureau'.
     *
     * Nombre d'inscription office (code insee 8) (détail pas sexe) par bureau
     * pour la liste et la date de tableau en cours
     *
     * @return array
     */
    function compute_stats__mouvement_io_sexe_bureau() {
        //
        $data = array();
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $bureau) {
            //
            $c = array('champ' => "codeinscription", 'valeur' => "8");
            // homme
            $sql1 = sprintf(
                'SELECT count(id) FROM %1$smouvement INNER JOIN %1$sparam_mouvement on mouvement.types=param_mouvement.code WHERE %5$s=\'%6$s\' AND mouvement.bureau=%4$s AND mouvement.date_tableau=\'%3$s\' AND mouvement.liste=\'%2$s\' and mouvement.sexe=\'M\'',
                DB_PREFIXE,
                $_SESSION["liste"],
                $this->f->getParameter("datetableau"),
                intval($bureau["id"]),
                $c['champ'],
                $c['valeur']
            );
            $res1 = $this->f->db->getone($sql1);
            $this->f->isDatabaseError($res1);
            // femme
            //
            $sql2 = sprintf(
                'SELECT count(id) FROM %1$smouvement INNER JOIN %1$sparam_mouvement on mouvement.types=param_mouvement.code WHERE %5$s=\'%6$s\' AND mouvement.bureau=%4$s AND mouvement.date_tableau=\'%3$s\' AND mouvement.liste=\'%2$s\' and mouvement.sexe=\'F\'',
                DB_PREFIXE,
                $_SESSION["liste"],
                $this->f->getParameter("datetableau"),
                intval($bureau["id"]),
                $c['champ'],
                $c['valeur']
            );
            $res2 = $this->f->db->getone($sql2);
            $this->f->isDatabaseError($res2);

            // total
            $temp =  $res1+$res2;
            //
            $datas = array(
                $bureau["code"],
                $bureau["libelle"],
                $res1,
                $res2,
                $temp
            );
            $data[] = $datas;
        }
        // Array
        return array(
            "format" => "L",
            "title" => __("Statistiques - IO"),
            "subtitle" => __("Details par sexe"),
            "offset" => array(10,0,50,50,40),
            "column" => array(
                "",
                __("LISTE")." : ".$_SESSION["libelle_liste"]."",
                __("Inscription D'office (8) homme"),
                __("Inscription D'office (8) femme"),
                __("Inscription D'office (8)")
            ),
            "data" => $data,
            "output" => "stats-iosexe"
        );
    }

    function compute_stats__verification_doublon_inscription() {
        //
        $nom = "";
        if (isset($_GET['nom'])) {
            $nom = $_GET['nom'];
        }
        $prenom = "";
        if (isset($_GET['prenom'])) {
            $prenom = $_GET['prenom'];
        }
        $datenaissance = "";
        if (isset($_GET['datenaissance'])) {
            $datenaissance = $_GET['datenaissance'];
        }
        $marital = "";
        if (isset($_GET['marital'])) {
            $marital = $_GET['marital'];
        }
        $correct = true;
        if ($nom == "" && $prenom == "" && $marital == "" && $datenaissance == "") {
            $correct = false;
            $subtitle = "Vous devez remplir au moins un des champs : nom, prénom, nom_usage, date de naissance.";
        }
        //
        $data = array();
        if ($correct == true) {
             // Where
            $subtitle = "Recherche sur : ";
            $selection = " where om_collectivite=".intval($_SESSION["collectivite"])." ";
            if (isset($nom) && $nom != "") {
                $subtitle .= "nom (".$nom.") ";
                $selection = " where nom='".$this->f->db->escapesimple($nom)."' ";
            }
            if (isset($prenom) && $prenom != "") {
                $subtitle .= "prénom (".$prenom.") ";
                if ($selection != "") {
                    $selection .= " and prenom like '".$this->f->db->escapesimple($prenom)."%' ";
                } else {
                    $selection = " where prenom like '".$this->f->db->escapesimple($prenom)."%' ";
                }
            }
            if (isset($marital) && $marital != "") {
                $subtitle .= "nom_usage (".$marital.") ";
                if ($selection != "") {
                    $selection .= " and nom_usage='".$this->f->db->escapesimple($marital)."' ";
                } else {
                    $selection = " where nom_usage='".$this->f->db->escapesimple($marital)."' ";
                }
            }
            if (isset($datenaissance) && $datenaissance != "") {
                $subtitle .= "date_naissance (".$datenaissance.") ";
                if ($selection != "") {
                    $selection .= " and to_char(date_naissance,'DD/MM/YYYY') ='".$datenaissance."' ";
                } else {
                    $selection = " where to_char(date_naissance,'DD/MM/YYYY') ='".$datenaissance."' ";
                }
            }

            // Table électeur
            $sqli = sprintf(
                'SELECT electeur.nom, electeur.prenom, electeur.nom_usage, (to_char(electeur.date_naissance, \'DD/MM/YYYY\')) as Naissance, electeur.liste FROM %1$selecteur %3$s AND electeur.om_collectivite=%2$s ORDER BY withoutaccent(lower(electeur.nom)), withoutaccent(lower(electeur.prenom))',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $selection
            );
            $resM = $this->f->db->query($sqli);
            $this->f->isDatabaseError($resM);
            //
            while ($row1 =& $resM->fetchrow(DB_FETCHMODE_ASSOC)) {
                $datas = array(
                    $row1['nom'],
                    $row1['prenom'],
                    $row1['nom_usage'],
                    $row1['naissance'],
                    "Electeur dans la liste ".$row1["liste"]
                );
                $data[] = $datas;
            }
            // Table mouvement
            $sql = sprintf(
                'SELECT mouvement.nom, mouvement.prenom, mouvement.nom_usage, (to_char(mouvement.date_naissance, \'DD/MM/YYYY\')) as Naissance, mouvement.liste FROM %1$smouvement %3$s AND mouvement.om_collectivite=%2$s and mouvement.etat=\'actif\' ORDER BY withoutaccent(lower(mouvement.nom)),withoutaccent(lower(mouvement.prenom))',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $selection
            );
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);
            //
            while ($row2 =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                $datas = array(
                    $row2['nom'],
                    $row2['prenom'],
                    $row2['nom_usage'],
                    $row2['naissance'],
                    "Mouvement en cours dans la liste ".$row2["liste"]
                );
                $data[] = $datas;
            }
        }
        //
        if (count($data) == 0) {
            $datas = array(
                __("Aucun résultat"),
                "",
                "",
                "",
                "",
            );
            $data[] = $datas;
        }
        // Array
        return array(
            "format" => "P",
            "title" => __("Statistiques - Doublon"),
            "offset" => array(40,40,40,20,50),
            "subtitle" => $subtitle,
            "column" => array(
                __("Nom"),
                __("Prenom(s)"),
                __("Marital"),
                __("Ne(e) le"),
                "Infos"
            ),
            "data" => $data,
            "output" => "stats-doublon1"
        );
    }

    /**
     * VIEW - view_edition_pdf__statistiques_mouvement_bureau.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_mouvement_bureau() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__mouvement_bureau(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__statistiques_mouvement_voie.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_mouvement_voie() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__mouvement_voie(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__statistiques_mouvement_io_sexe_bureau.
     *
     * @return void
     */
    function view_edition_pdf__statistiques_mouvement_io_sexe_bureau() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__mouvement_io_sexe_bureau(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__verification_doublon_inscription.
     *
     * @return void
     */
    function view_edition_pdf__verification_doublon_inscription() {
        $this->checkAccessibility();
        //
        require_once "../obj/edition_pdf.class.php";
        $inst_edition_pdf = new edition_pdf();
        $pdf_edition = $inst_edition_pdf->compute_pdf__pdffromarray(array(
            "tableau" => $this->compute_stats__verification_doublon_inscription(),
        ));
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_edition_pdf__statistiques_generales_des_mouvements.
     *
     * @return void
     */
    public function view_edition_pdf__statistiques_generales_des_mouvements() {
        $this->checkAccessibility();
        $params = array();
        //
        require_once "../obj/edition_pdf__statistiques_generales_des_mouvements.class.php";
        $inst_edition_pdf = new edition_pdf__statistiques_generales_des_mouvements();
        $pdf_edition = $inst_edition_pdf->compute_pdf__statistiques_generales_des_mouvements($params);
        //
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }


    function get_inst__ancien_bureau() {
        if ($this->getval("ancien_bureau") === "") {
            $inst_bureau = $this->f->get_inst__om_dbform(array(
                "obj" => "bureau",
            ));
            $bureau_id = $inst_bureau->get_bureau_id_from_code($this->getVal("ancien_bureau_de_vote_code"));
            $inst_bureau->__destruct();
            unset($inst_bureau);
        } else {
            $bureau_id = $this->getVal("ancien_bureau");
        }
        //
        return $this->f->get_inst__om_dbform(array(
            "obj" => "bureau",
            "idx" => $bureau_id,
        ));
    }

    function get_inst__bureau() {
        if ($this->getval("bureau") === "") {
            $inst_bureau = $this->f->get_inst__om_dbform(array(
                "obj" => "bureau",
            ));
            $bureau_id = $inst_bureau->get_bureau_id_from_code($this->getVal("bureau_de_vote_code"));
            $inst_bureau->__destruct();
            unset($inst_bureau);
        } else {
            $bureau_id = $this->getVal("bureau");
        }
        //
        return $this->f->get_inst__om_dbform(array(
            "obj" => "bureau",
            "idx" => $bureau_id,
        ));
    }

    function get_inst__param_mouvement() {
        //
        return $this->f->get_inst__om_dbform(array(
            "obj" => "param_mouvement",
            "idx" => $this->getVal("types"),
        ));
    }

    /**
     *
     */
    function get_inst__voie() {
        return $this->f->get_inst__om_dbform(array(
            "obj" => "voie",
            "idx" => $this->getVal("code_voie"),
        ));
    }

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     *
     * @return array
     */
    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        $labels["mouvement"]["mouvement.civilite_libelle"] = __("Civilité en toutes lettres");
        // voie
        $labels["mouvement"]["mouvement.cp"] = __("Code Postal");
        $labels["mouvement"]["mouvement.ville"] = __("Ville");
        //
        $bureau = $this->get_inst__bureau();
        $bureau_labels = $bureau->get_merge_fields("labels");
        $labels["mouvement"] = array_merge(
            $labels["mouvement"],
            $bureau_labels["bureau"]
        );
        //
        $ancien_bureau = $this->get_inst__ancien_bureau();
        $ancien_bureau_labels = $ancien_bureau->get_merge_fields("labels");
        foreach ($ancien_bureau_labels["bureau"] as $key => $value) {
            $ancien_bureau_labels["bureau"]["ancien_".$key] = $value;
        }
        $labels["mouvement"] = array_merge(
            $labels["mouvement"],
            $ancien_bureau_labels["bureau"]
        );
        //
        $param_mouvement = $this->get_inst__param_mouvement();
        $param_mouvement_labels = $param_mouvement->get_merge_fields("labels");
        $labels["mouvement"] = array_merge(
            $labels["mouvement"],
            $param_mouvement_labels["param_mouvement"]
        );
        // Retour de tous les libellés
        return $labels;
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     *
     * @return array
     */
    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        if ($this->getVal("numero_habitation") == "0") {
            $values["mouvement.numero_habitation"] = "";
        }
        //
        $voie = $this->get_inst__voie();
        $values["mouvement.cp"] = $voie->getVal("cp");
        $values["mouvement.ville"] = $voie->getVal("ville");
        //
        switch ($this->getVal("civilite")) {
            case "M.":
                $values["mouvement.civilite_libelle"] = "Monsieur";
                break;
            case "Mme":
                $values["mouvement.civilite_libelle"] = "Madame";
                break;
            case "Mlle":
                $values["mouvement.civilite_libelle"] = "Mademoiselle";
                break;
            default:
                $values["mouvement.civilite_libelle"] = $this->getVal("civilite");
        }
        //
        $bureau = $this->get_inst__bureau();
        $values = array_merge(
            $values,
            $bureau->get_merge_fields("values")
        );
        //
        $ancien_bureau = $this->get_inst__ancien_bureau();
        $ancien_bureau_values = array();
        foreach ($ancien_bureau->get_merge_fields("values") as $key => $value) {
            $ancien_bureau_values["ancien_".$key] = $value;
        }
        $values = array_merge(
            $values,
            $ancien_bureau_values
        );
        //
        $param_mouvement = $this->get_inst__param_mouvement();
        $values = array_merge(
            $values,
            $param_mouvement->get_merge_fields("values")
        );
        //
        return $values;
    }

    /**
     * @return boolean
     */
    function is_departement_correct_code_pays_naissance($code) {
        if ($code === '99'
            || (strlen($code) === 5 && intval($code) >= 99000)) {
            return true;
        }
        return false;
    }

    /**
     * @return boolean
     */
    function is_departement_correct_code_ancien_departement_francais_algerie($code) {
        if ((strlen($code) === 5 && intval($code) === 91352)
            || (strlen($code) === 5 && intval($code) === 92352)
            || (strlen($code) === 5 && intval($code) === 93352)
            || (strlen($code) === 5 && intval($code) === 94352)) {
            return true;
        }
        return false;
    }

    function get_naissance_type_saisie($departement_code, $departement_libelle, $commune_code, $commune_libelle) {
        if ($this->is_departement_correct_code_pays_naissance($departement_code) === true) {
            $inst_departement = $this->f->get_inst__om_dbform(array(
                "obj" => "departement",
                "idx" => $departement_code
            ));
            if ($departement_code == $commune_code
                && $inst_departement->getVal($inst_departement->clePrimaire) == $departement_code
                && $inst_departement->getVal("libelle_departement") == $departement_libelle) {
                //
                return 'Etranger';
            }
        } elseif ($this->is_departement_correct_code_ancien_departement_francais_algerie($departement_code) === true) {
            $inst_departement = $this->f->get_inst__om_dbform(array(
                "obj" => "departement",
                "idx" => $departement_code
            ));
            if ($departement_code == $commune_code
                && $inst_departement->getVal($inst_departement->clePrimaire) == $departement_code
                && $inst_departement->getVal("libelle_departement") == $departement_libelle) {
                //
                return 'Ancien_dep_franc';
            }
        } else {
            $inst_departement = $this->f->get_inst__om_dbform(array(
                "obj" => "departement",
                "idx" => $departement_code
            ));
            $inst_commune = $this->f->get_inst__om_dbform(array(
                "obj" => "commune",
                "idx" => $commune_code
            ));
            if ($departement_code != ""
                && $commune_code != ""
                && $departement_libelle != ""
                && $commune_libelle != ""
                && $inst_departement->getVal($inst_departement->clePrimaire) == $inst_commune->getVal("code_departement")
                && $inst_departement->getVal($inst_departement->clePrimaire) == $departement_code
                && $inst_departement->getVal("libelle_departement") == $departement_libelle
                && $inst_commune->getVal($inst_commune->clePrimaire) == $commune_code
                && $inst_commune->getVal("libelle_commune") == $commune_libelle) {
                //
                return 'France';
            }
        }
        return "autre";
    }

    /**
     *
     */
    function get_mouvement_id_from_reu_demande_id($reu_demande_id) {
        $query = sprintf(
            'SELECT mouvement.id FROM %1$smouvement WHERE id_demande=\'%2$s\'',
            DB_PREFIXE,
            $reu_demande_id
        );
        $res = $this->f->get_all_results_from_db_query($query, true);
        if ($res['code'] !== "OK") {
            return null;
        }
        if (count($res['result']) == 0) {
            return null;
        }
        if (count($res['result']) > 1) {
            return false;
        }
        return intval($res['result'][0]['id']);
    }

    /**
     *
     */
    function view_consulter_historique() {
        $this->checkAccessibility();
        // Affichage du bouton retour
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "top",
        ));
        $this->retour(
            $this->getParameter("premier"),
            $this->getParameter("recherche"),
            $this->getParameter("tricol")
        );
        $this->f->layout->display__form_controls_container__end();
        //
        printf(
            '<h2>%s</h2>',
            __("Historique")
        );
        echo $this->f->get_display_array_as_table(json_decode($this->getVal('historique'), true));
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu == null) {
            echo "Erreur REU";
        } else {
            if (intval($this->getVal("id_demande")) > 0
                && (
                    $this->get_absolute_class_name() === "inscription"
                    || $this->get_absolute_class_name() === "radiation")) {
                printf(
                    '<h2>%s</h2>',
                    __("REU Mouvement")
                );
                $method = "handle_".$this->get_absolute_class_name();
                $ret = $inst_reu->$method(array(
                    "mode" => "get",
                    "id" => intval($this->getVal("id_demande")),
                ));
                echo "<p>Code : ".$ret["code"]."<p>";
                echo $this->f->get_display_array_as_table(
                    $ret["result"]
                );
            }
            if (intval($this->getVal("ine")) > 0) {
                printf(
                    '<h2>%s</h2>',
                    __("REU Electeur")
                );
                $method = "handle_electeur";
                $ret = $inst_reu->$method(array(
                    "mode" => "get",
                    "ine" => intval($this->getVal("ine")),
                ));
                echo "<p>Code : ".$ret["code"]."<p>";
                echo $this->f->get_display_array_as_table(
                    $ret["result"]
                );
            }
        }
        // Affichage du bouton retour
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->retour(
            $this->getParameter("premier"),
            $this->getParameter("recherche"),
            $this->getParameter("tricol")
        );
        $this->f->layout->display__form_controls_container__end();
    }

    /**
     *
     */
    function handle_historique($val = array()) {
        //
        if (isset($val['id']) !== true) {
            $val['id'] = $this->getVal($this->clePrimaire);
        }
        //
        $histo = array();
        //
        $add_histo = array(
            'date' => (isset($val['date']) === true ? $val['date'] : date('Y-m-d H:i:s')),
            'user' => (isset($val['login']) === true ? $val['login'] : $_SESSION['login']),
            'action' => (isset($val['action']) === true ? $val['action'] : sprintf(
                '%1$s %2$s',
                $this->getParameter('maj'),
                $this->get_action_param($this->getParameter('maj'), 'identifier')
            )),
            'message' => (isset($val['message']) === true ? $val['message'] : sprintf(
                __('Action %1$s %2$s effectuée.'),
                $this->getParameter('maj'),
                $this->get_action_param($this->getParameter('maj'), 'identifier')
            )),
        );
        //
        if (isset($val['datas']) === true 
            && $val['datas'] !== ''
            && $val['datas'] !== null) {
            //
            $add_histo['datas'] = $val['datas'];
        }
        //
        $histo[] = $add_histo;
        $histo_exists = $this->f->get_one_result_from_db_query(sprintf(
            'SELECT historique FROM %1$smouvement WHERE id=\'%2$s\'',
            DB_PREFIXE,
            $val['id']
        ), true);
        if ($histo_exists['code'] !== 'OK') {
            return false;
        }
        if ($histo_exists['result'] !== ''
            && $histo_exists['result'] !== null
            && $histo_exists['result'] !== 'null'
            && json_decode($histo_exists['result'], true) !== null) {
            //
            $histo = array_merge($histo, json_decode($histo_exists['result'], true));
        }
        //
        $valF = array(
            "historique" => json_encode($histo),
        );
        $ret = $this->update_autoexecute($valF, $val['id'], false);
        if ($ret !== true) {
            return false;
        }
        return true;
    }

    /**
     *
     */
    function ajouter($val = array(), &$dnu1 = null, $dnu2 = null) {
        $res = parent::ajouter($val, $dnu1, $dnu2);
        if ($res === true) {
            // Gestion de l'historique
            $val_histo = array(
                'id' => $this->valF[$this->clePrimaire],
                'action' => 'ajouter',
                'message' => sprintf(
                    __('Action %1$s %2$s effectuée.'),
                    0,
                    $this->get_action_param(0, 'identifier')
                ),
            );
            $histo = $this->handle_historique($val_histo);
            if ($histo !== true) {
                $this->correct = false;
                return $this->end_treatment(__METHOD__, false);
            }
        }
        return $res;
    }

    /**
     *
     */
    function modifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        $res = parent::modifier($val, $dnu1, $dnu2);
        if ($res === true) {
            // Gestion de l'historique
            $histo = $this->handle_historique();
            if ($histo !== true) {
                $this->correct = false;
                return $this->end_treatment(__METHOD__, false);
            }
        }
        return $res;
    }

    /**
     *
     */
    function get_mouvement_typecat($lower = true) {
        $field = 'LOWER(typecat)';
        if ($lower !== true) {
            $field = 'typecat';
        }
        $res = $this->f->get_one_result_from_db_query(sprintf(
            'SELECT %3$s FROM %1$sparam_mouvement WHERE code=\'%2$s\'',
            DB_PREFIXE,
            $this->getVal("types"),
            $field
        ), true);
        if ($res['code'] !== 'OK') {
            return false;
        }
        return $res['result'];
    }

    /**
     * Récupère l'identifiant de l'électeur par rapport à son INE et sa liste.
     *
     * @param  string $ine   INE de l'électeur
     * @param  string $liste Numéro de la liste de l'électeur
     * @return mixed         Identifiant de l'électeur ou false
     */
    function get_electeur_id_by_ine_and_list($ine, $liste) {
        $res = $this->f->get_one_result_from_db_query(
            sprintf('
                SELECT id
                FROM %1$selecteur
                WHERE om_collectivite=%2$s
                    AND ine=\'%3$s\'
                    AND liste=\'%4$s\'
                ',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $ine,
                $liste
            )
        );
        if ($res['code'] === 'OK'
            && $res['result'] !== ''
            && $res['result'] !== null) {
            //
            return $res['result'];
        }
        return false;
    }

    /**
     * Récupère les identifiants des électeurs d'openElec ayant le même INE.
     * @param  string $ine INE de l'électeur
     * @return mixed       Tableaux des identifiants ou false
     */
    function get_electeurs_with_same_ine($ine) {
        $electeurs = $this->f->get_all_results_from_db_query(sprintf(
            'SELECT id FROM %1$selecteur WHERE ine=\'%2$s\' AND om_collectivite=%3$s',
            DB_PREFIXE,
            $ine,
            intval($_SESSION["collectivite"])
        ), true);
        if ($electeurs['code'] !== 'OK') {
            return false;
        }
        $res = array();
        foreach ($electeurs['result'] as $electeur) {
            $res[] = $electeur['id'];
        }
        return $res;
    }
}
