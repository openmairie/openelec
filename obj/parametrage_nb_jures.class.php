<?php
/**
 * Ce script définit la classe 'parametrage_nb_jures'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/parametrage_nb_jures.class.php";

/**
 * Définition de la classe 'parametrage_nb_jures' (om_dbform).
 */
class parametrage_nb_jures extends parametrage_nb_jures_gen {

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton() {
        return sprintf(
            'SELECT canton.id, (canton.code || \' - \' || canton.libelle) AS lib FROM %1$scanton ORDER BY lib',
            DB_PREFIXE
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_canton_by_id() {
        return sprintf(
            'SELECT canton.id, (canton.code || \' - \' || canton.libelle) AS lib FROM %1$scanton  WHERE id=\'<idx>\'',
            DB_PREFIXE
        );
    }


    /**
     *
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Si le module de tirage au sort des jurés suppléants n'est pas actif alors
        // le paramétrage de ces jurés n'est pas visible.
        if($this->f->is_option_enabled('option_module_jures_suppleants') !== true) {
            $form->setType("nb_suppleants", "hidden");
        }
    }


    /**
     *
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        $form->setLib("nb_suppleants", __("nombre de suppléants"));
    }
}


