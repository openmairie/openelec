<?php
/**
 * Ce script définit la classe 'commune'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/commune.class.php";

/**
 * Définition de la classe 'commune' (om_dbform).
 */
class commune extends commune_gen {

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_code_departement() {
        return sprintf(
            'SELECT departement.code, (departement.code||\' \'|| departement.libelle_departement) as lib FROM %1$sdepartement ORDER BY departement.code',
            DB_PREFIXE
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_code_departement_by_id() {
        return sprintf(
            'SELECT departement.code, (departement.code||\' \'|| departement.libelle_departement) as lib FROM %1$sdepartement WHERE departement.code = \'<idx>\'',
            DB_PREFIXE
        );
    }

    /**
     * Cette methode permet de remplir le tableau associatif valF attribut de
     * l'objet en ajouter la cle primaire en vue de l'insertion des donnees
     * dans la base.
     *
     * @param array $val Tableau associatif representant les valeurs du
     *                   formulaire
     *
     * @return void
     */
    function setValFAjout($val = array()) {
        //
        $this->valF[$this->clePrimaire] = trim($val["code_departement"]." ".$val["code_commune"]);
    }

    /**
     *
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Initialisation de l'attribut correct a true
        $this->correct = true;
        //
        $this->rechercheTable($this->f->db, "electeur", "code_lieu_de_naissance", $id);
        $this->rechercheTable($this->f->db, "electeur", "provenance", $id);
        $this->rechercheTable($this->f->db, "mouvement", "code_lieu_de_naissance", $id);
        $this->rechercheTable($this->f->db, "mouvement", "provenance", $id);
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setType (&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj < 2) { // ajouter et modifier
            if ($maj == 1) { // modifier
                $form->setType("code", "hiddenstatic");
                $form->setType("code_commune", "hiddenstatic");
                $form->setType("code_departement", "selecthiddenstatic");
            } else { // ajouter
                $form->setType("code", "hidden");
                $form->setType("code_departement", "select");
            }
        } else { // supprimer
            $form->setType("code", "hiddenstatic");
            $form->setType("code_departement", "selecthiddenstatic");
        }
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("code", __("Code Insee"));
        $form->setLib("code_commune", __("Code commune"));
        $form->setLib("libelle_commune", __("Nom de la commune"));
        $form->setLib("code_departement", __("Departement"));
    }

    /**
     * Parametrage du formulaire - Onchange des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange("libelle_commune", "this.value=this.value.toUpperCase()");
        $form->setOnchange("code_commune", "this.value=this.value.toUpperCase()");
    }
}
