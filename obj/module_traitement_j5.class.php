<?php
/**
 * Ce script définit la classe 'module_traitement_j5'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_traitement_j5' (module).
 */
class module_traitement_j5 extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_traitement_j5",
            "title" => __("Traitement")." -> ".__("Module Traitement J-5"),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_traitement_j5-onglet_traitement_j5",
                    "href" => "../app/index.php?module=module_traitement_j5&view=traitement_j5",
                    "title" => __("Traitement j5"),
                    "view" => "traitement_j5",
                ),
            ),
        );
    }

    /**
     *
     */
    function view__traitement_j5() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_traitement_j5");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        echo "<div id=\"traitement_j5\">";
        echo "<div class=\"alert ui-accordion-header ui-helper-reset ui-state-default ui-corner-all\">";
        echo "<img title=\"".__("Date de tableau")."\" alt=\"".__("Date de tableau")."\" src=\"../app/img/calendar.png\">";
        echo __("Tableau du")." ".$this->f->formatDate($this->f->getParameter("datetableau"));
        echo "</div>";

        /**
         *
         */
        //
        $description = __("Le \"traitement J-5\" permet d'appliquer les rectifications ".
                         "intervenues depuis la cloture des listes ou depuis le ".
                         "dernier scrutin posterieur a cette cloture (tableau des cinq ".
                         "jours), ainsi que les additions operees au titre du deuxieme ".
                         "alinea de l'article L. 11-2 (tableau des additions). C'est un ".
                         "traitement particulier qui permet de constituer la liste ".
                         "electorale qui entre en vigueur a la date de l'election ".
                         "generale. Pour appliquer le traitement, il suffit de suivre les ".
                         "etapes suivantes.");
        $this->f->displayDescription($description);

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Quand appliquer le traitement J-5 ?");
        $this->f->displaySubTitle($subtitle);
        // Affichage du texte du paragraphe
        $description = __("Le traitement J-5 doit etre applique 5 jours avant le premier ".
            "tour du scrutin. Il est generalement effectue le mardi avant le scrutin ou ".
            "le lundi si celui-ci se deroule un samedi.");
        $this->f->displayDescription($description);
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";


        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Etape 1")." - ".__("Selection du ou des tableau(x) a appliquer sur la liste electorale");
        $this->f->displaySubTitle($subtitle);
        $description = __("Dans un premier temps, il est necessaire de selectionner ".
                         "le ou les tableau(x) que vous souhaitez appliquer sur la ".
                         "liste electorale. Si vous selectionnez plusieurs tableaux, ".
                         "les nouveaux inscrits se verront attribues un numero d'ordre ".
                         "tous tableaux confondus. Cliquer sur ce premier bouton valider ".
                         "n'applique pas le traitement mais vous permet seulement ".
                         "d'acceder a l'etape suivante.");
        $this->f->displayDescription($description);
        //
        $step1 = false;
        $links = array(
            "0" => array(
                "href" => "../app/index.php?module=form&obj=revision&action=303&mode_edition=tableau&amp;tableau=tableau_des_cinq_jours",
                "target" => "_blank",
                "class" => "om-prev-icon edition-16",
                "title" => __("Tableau des cinq jours"),
            ),
        );

        //
        echo "<form name=\"f1\" action=\"../app/index.php?module=module_traitement_j5&view=traitement_j5\" method=\"post\" onsubmit=\"ajaxItForm('traitement_j5', '../app/index.php?module=module_traitement_j5&view=traitement_j5', this); return false;\">";
        //
        $champs = array("mouvementatraiter");
        //
        $form = $this->f->get_inst__om_formulaire(array(
            "validation" => 0,
            "maj" => 0,
            "champs" => $champs,
        ));
        //
        $form->setLib("mouvementatraiter", __("Selectionner ici le ou les tableau(x) ".
                                               "qui entre(nt) en vigueur a la date de l'election"));
        $form->setType("mouvementatraiter", "checkbox_multiple");
        //
        $mouvements_io = sprintf(
            'SELECT code, libelle, effet FROM %1$smouvement INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code WHERE (effet=\'Election\' or effet=\'Immediat\') and date_tableau=\'%2$s\' and etat=\'actif\' GROUP BY code, libelle, effet ORDER BY effet DESC, code',
            DB_PREFIXE,
            $this->f->getParameter("datetableau")
        );
        $res = $this->f->db->query($mouvements_io);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$mouvements_io."\")",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        $contenu = array();
        $contenu[0] = array("Immediat",);
        $contenu[1] = array(__("Tableau des cinq jours"),);
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $this->f->addToLog(__METHOD__."(): ".print_r($row, true), EXTRA_VERBOSE_MODE);
            if ($row["effet"] == "Election") {
                array_push($contenu[0], $row["code"]);
                array_push($contenu[1], __("Tableau des additions")." - ".$row["libelle"]);
            }
            array_push($links, array(
                    "href" => "../app/index.php?module=form&obj=revision&action=303&mode_edition=tableau&amp;tableau=tableau_des_additions_des_jeunes&amp;type=".$row["code"],
                    "target" => "_blank",
                    "class" => "om-prev-icon edition-16",
                    "title" => __("Tableau des additions")." - ".$row["libelle"],
                ));
        }
        $form->setSelect("mouvementatraiter", $contenu);
        //
        $cinqjours = false;
        $additions = array();
        $mouvementatraiter_input = null;
        $mouvementatraiter = "";
        if (isset($_POST["mouvementatraiter"])) {
            $mouvementatraiter_input = $_POST["mouvementatraiter"];
        }
        if ($mouvementatraiter_input != null) {
            foreach ($mouvementatraiter_input as $elem) {
                if ($elem == "Immediat") {
                    $cinqjours = true;
                } else {
                    $additions[] = $elem;
                }
                $mouvementatraiter .= $elem.";";
            }
        }
        $form->setVal("mouvementatraiter", $mouvementatraiter);
        if ($mouvementatraiter != "") {
            $step1 = true;
        }
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        echo "<div class=\"formControls\">\n";
        if (count($contenu[0]) == 0) {
            echo "<p class=\"likeabutton\">\n";
            echo __("Aucun mouvement a appliquer");
            echo "</p>\n";
        } else {
            echo "<input value=\"".__("Valider la selection")."\" type=\"submit\" />";
        }
        echo "</div>\n";
        //
        echo "</form>\n";
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";


        if ($step1 == true) {

            /**
             *
             */
            // Ouverture de la balise - DIV paragraph
            echo "<div class=\"paragraph\">\n";
            // Affichage du titre du paragraphe
            $subtitle = "-> ".__("Etape 2 - Verification et application du Traitement J-5 du")." ".date ('d/m/Y')." [Tableau du ".$this->f->formatDate ($this->f->getParameter("datetableau"))."]";
            $this->f->displaySubTitle($subtitle);
            //
            require_once "../obj/traitement.j5.class.php";
            $trt = new j5Traitement();
            $trt->displayForm();
            // Fermeture de la balise - DIV paragraph
            echo "</div>\n";

            /**
             *
             */
            // Ouverture de la balise - DIV paragraph
            echo "<div class=\"paragraph\">\n";
            // Affichage du titre du paragraphe
            $subtitle = "-> ".__("Etape 3 - Edition des nouvelles cartes d'electeur");
            $this->f->displaySubTitle($subtitle);
            // Affichage du texte du paragraphe
            $description = __("Une fois le traitement applique, il est possible d'editer les nouvelles cartes electorales a tout moment depuis l'ecran");
            $description .= " <a href=\"../app/view_revision_electorale.php\">'Edition -> Revision Electorale'</a>.";
            $this->f->displayDescription($description);
            // Fermeture de la balise - DIV paragraph
            echo "</div>\n";

        }

        echo "</div>";
    }
}
