<?php
/**
 * Ce script définit la classe 'om_dbform'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_dbform.class.php";

/**
 * Définition de la classe 'om_dbform' (om_dbform).
 *
 * Cette classe permet la surcharge de certaines methodes de
 * la classe om_dbform pour des besoins specifiques de l'application.
 */
class om_dbform extends dbform {

    /**
     * Permet de mettre à jour un enregistrement sans passer par la fonction
     * modifier.
     *
     * @param  array  $valF  Liste des valeurs à mettre à jour
     * @param  mixed  $id    Identifiant de l'enregistrement (instance si non précisé)
     *
     * @return boolean       Vrai si traitement réalisé avec succès
     */
    function update_autoexecute($valF, $id = null, $show_valid_msg = true) {
        if (empty($id)) {
            $id = $this->getVal($this->clePrimaire);
        }
        // Begin
        $this->begin_treatment(__METHOD__);
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            // Return
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $valid_message = __("Vos modifications ont bien ete enregistrees.");
        //
        $main_res_affected_rows = $this->db->affectedRows();
        // Log
        $this->addToLog(__("Requete executee"), VERBOSE_MODE);
        // Log
        $message = __("Enregistrement")."&nbsp;".$id."&nbsp;";
        $message .= __("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
        $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
        $message .= __("enregistrement(s) mis a jour")."&nbsp;]";
        $this->addToLog($valid_message, VERBOSE_MODE);
        // Message de validation
        if ($main_res_affected_rows == 0) {
            //
            if ($show_valid_msg === true) {
                //
                $this->addToMessage(__("Attention vous n'avez fait aucune modification.")."<br/>");
            }
        } else {
            //
            if ($show_valid_msg === true) {
                //
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * VIEW - view_edition_pdf__etiquette.
     *
     * @return void
     */
    public function view_edition_pdf__etiquette() {
        $this->checkAccessibility();
        $params = array(
            "obj" => $this->get_absolute_class_name(),
        );
        //
        require_once "../obj/edition_pdf__etiquette.class.php";
        $inst_edition_pdf = new edition_pdf__etiquette();
        $pdf_edition = $inst_edition_pdf->compute_pdf__etiquette($params);
        //
        $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * Factorisation de la requête de sélection des bureaux de vote.
     *
     * @return string
     */
    function get_common_var_sql_forminc__sql_bureau() {
        return sprintf(
            'SELECT bureau.id, (bureau.code || \' \' || bureau.libelle) AS lib FROM %1$sbureau WHERE bureau.om_collectivite=%2$s ORDER BY bureau.code ASC',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
    }

    /**
     * Factorisation de la requête de sélection des bureaux de vote.
     *
     * @return string
     */
    function get_common_var_sql_forminc__sql_bureau_by_id() {
        return sprintf(
            'SELECT bureau.id, (bureau.code || \' \' || bureau.libelle) AS lib FROM %1$sbureau WHERE bureau.id=<idx>',
            DB_PREFIXE
        );
    }

    /**
     * Factorisation de la requête de sélection des listes_officielles.
     *
     * @return string
     */
    function get_common_var_sql_forminc__sql_liste_officielle() {
        return sprintf(
            'SELECT liste.liste, (liste.liste_insee || \' - \' || liste.libelle_liste) AS lib FROM %1$sliste WHERE liste.officielle IS TRUE ORDER BY liste.liste',
            DB_PREFIXE
        );
    }

    /**
     * Factorisation de la requête de sélection des listes.
     *
     * @return string
     */
    function get_common_var_sql_forminc__sql_liste() {
        return sprintf(
            'SELECT liste.liste, (liste.liste_insee || \' - \' || liste.libelle_liste) AS lib FROM %1$sliste ORDER BY liste.liste',
            DB_PREFIXE
        );
    }

    /**
     * Factorisation de la requête de sélection des listes.
     *
     * @return string
     */
    function get_common_var_sql_forminc__sql_liste_by_id() {
        return sprintf(
            'SELECT liste.liste, (liste.liste_insee || \' - \' || liste.libelle_liste) AS lib FROM %1$sliste WHERE liste = \'<idx>\'',
            DB_PREFIXE
        );
    }

    /**
     * CONDITION - is_collectivity_mono
     */
    function is_collectivity_mono() {
        return $this->f->getParameter("is_collectivity_mono");
    }

    /**
     * CONDITION - is_collectivity_multi
     */
    function is_collectivity_multi() {
        return $this->f->getParameter("is_collectivity_multi");
    }

    /**
     * CONDITION - is_user_mono
     */
    function is_user_mono() {
        return $this->f->getParameter("is_user_mono");
    }

    /**
     * CONDITION - is_user_multi
     */
    function is_user_multi() {
        return $this->f->getParameter("is_user_multi");
    }

    /**
     * CONDITION - is_option_refus_mouvement_enabled
     */
    function is_option_refus_mouvement_enabled() {
        return $this->f->is_option_refus_mouvement_enabled();
    }

    /**
     *
     */
    function get_values_substitution_vars($om_collectivite_idx = null) {
        //
        $values = parent::get_values_substitution_vars($om_collectivite_idx);
        //
        $values["aujourdhui"] = date("d/m/Y");
        $values["aujourdhui_lettre"] = strftime("%d %B %Y");
        //
        $inst_om_parametre = $this->f->get_inst__om_dbform(array(
            "obj" => "om_parametre",
        ));
        foreach ($inst_om_parametre->_hidden_and_locked_elements as $element) {
            if (array_key_exists($element, $values)) {
                unset($values[$element]);
            }
        }
        //
        return $values;
    }

    /**
     *
     */
    function get_labels_substitution_vars($om_collectivite_idx = null) {
        //
        $labels = parent::get_labels_substitution_vars($om_collectivite_idx);
        //
        $labels["divers"]["aujourdhui"] = __("Date du jour (Format : 09/01/1978)");
        $labels["divers"]["aujourdhui_lettre"] = __("Date du jour (Format : 09 janvier 1978)");
        //
        $inst_om_parametre = $this->f->get_inst__om_dbform(array(
            "obj" => "om_parametre",
        ));
        foreach ($inst_om_parametre->_hidden_and_locked_elements as $element) {
            if (array_key_exists("om_parametre", $labels)
                && is_array($labels["om_parametre"])
                && array_key_exists($element, $labels["om_parametre"])) {
                unset($labels["om_parametre"][$element]);
            }
        }
        //
        return $labels;
    }

    // {{{ SURCHARGE FRAMEWORK
    /**
     * L'objectif de la surcharge est de corriger le bug qui récupère le nom de la
     * clé primaire au lieu du nom de la table.
     *
     * @return array         tableau associatif
     */
    function get_labels_merge_fields() {
        // récupération de la table de la classe instanciée
        $table = $this->table;
        // initialisation du tableau de libellés
        $labels = array();
        // pour chaque champ de l'objet on crée un champ de fusion
        foreach ($this->champs as $key => $champ) {
            //
            if (in_array($champ, $this->get_merge_fields_to_avoid())) {
                continue;
            }
            //
            $labels[$table][$table.".".$champ] = __($champ);
        }
        return $labels;
    }
    // }}}
}


