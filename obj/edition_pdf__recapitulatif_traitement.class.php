<?php
/**
 * Ce script définit la classe 'edition_pdf__recapitulatif_traitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__recapitulatif_traitement' (edition_pdf).
 */
class edition_pdf__recapitulatif_traitement extends edition_pdf {

    /**
     * Édition PDF - .
     *
     * @return array
     */
    public function compute_pdf__recapitulatif_traitement($params = array()) {
        /**
         *
         */
        //
        $traitement = "";
        $datetableau = "";
        if (isset($params["traitement"])) {
            $traitement = $params["traitement"];
            $datetableau = "";
            if (isset($params["datetableau"])) {
                $datetableau = $params["datetableau"];
            } else {
                $datetableau = $this->f->getParameter("datetableau");
            }
            if ($traitement == "j5") {
                $datej5 = "";
                if (isset($params["datej5"])) {
                    $datej5 = $params["datej5"];
                }
            }
        }
        //
        $datetableau_formatted_for_display = $this->f->formatDate($datetableau, true);
        $datetableau_formatted_for_database = $this->f->formatDate($datetableau);
        //
        $ville = $this->f->collectivite['ville'];

        //
        // if (!in_array($traitement, array("j5", "annuel"))) {
        //     $class = "error";
        //     $message = __("L'objet est invalide.");
        //     $this->f->addToMessage($class, $message);
        //     $this->f->setFlag(null);
        //     $this->f->display();
        //     die();
        // }

        /**
         *
         */
        //
        //
        require_once "../obj/fpdf_fromdb_fromarray.class.php";
        //
        $pdf = new PDF("L", "mm", "A4");
        //
        $pdf->generic_document_header_left = $this->f->collectivite["ville"];
        $pdf->generic_document_title = $_SESSION["libelle_liste"];
        //
        $pdf->SetMargins(10, 5, 10);
        //
        $pdf->AliasNbPages();
        // Édition PDF n°1
        //
        $pdf->init_header_pdffromarray();
        $define_class_filename = "../obj/traitement.".$traitement.".class.php";
        require_once $define_class_filename;
        $class_name = $traitement."Traitement";
        $trt = new $class_name();
        $method_name = "compute_stats__traitement_".$traitement;
        $tableau = $trt->$method_name($params);
        $tableau["output"] = null;
        $pdf->tableau($tableau);

        // Édition PDF n°2
        //
        $params = array_merge(
            $params,
            array(
                "f" => $this->f,
                "ville" => $ville,
                "datetableau" => $datetableau,
                "datetableau_formatted_for_display" => $datetableau_formatted_for_display,
                "datetableau_formatted_for_database" => $datetableau_formatted_for_database,
            )
        );
        $conf = $this->get_vars_from_config_file_pdffromdb(
            "traitement_".$traitement."_inscription",
            $params
        );
        $pdf->add_edition_pdffromdb($conf);
        $pdf->reinit_header_pdffromdb();

        // Édition PDF n°3
        //
        //
        $conf = $this->get_vars_from_config_file_pdffromdb(
            "traitement_".$traitement."_modification",
            $params
        );
        $pdf->add_edition_pdffromdb($conf);
        $pdf->reinit_header_pdffromdb();

        // Édition PDF n°4
        //
        //
        $conf = $this->get_vars_from_config_file_pdffromdb(
            "traitement_".$traitement."_radiation",
            $params
        );
        $pdf->add_edition_pdffromdb($conf);
        $pdf->reinit_header_pdffromdb();

        /**
         * OUTPUT
         */
        //
        $filename = date("Ymd-His");
        $filename .= "-recapitulatif";
        if (isset($datetableau) && $datetableau != "") {
            $filename .= "-".$datetableau;
        }
        $filename .= "-traitement_".$traitement;
        if (isset($datej5) && $datej5 != "") {
            $filename .= "-".$datej5;
        }
        $filename .= "-".$_SESSION["collectivite"];
        $filename .= "-".$this->f->normalizeString($_SESSION['libelle_collectivite']);
        $filename .= "-liste".$_SESSION["liste"];
        $filename .= ".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
