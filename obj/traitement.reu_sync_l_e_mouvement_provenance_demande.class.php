<?php
/**
 * Ce script définit la classe 'reuSyncLEMouvementProvenanceDemandeTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.reu_sync_l_e.class.php";

/**
 * Définition de la classe 'reuSyncLEMouvementProvenanceDemandeTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncLEMouvementProvenanceDemandeTraitement extends reuSyncLETraitement {

    var $fichier = "reu_sync_l_e_mouvement_provenance_demande";
    /**
     *
     */
    function treatment() {
        set_time_limit(180);
        $this->LogToFile("begin ".$this->fichier);
        //
        if ($this->f->getParameter("reu_sync_l_e_valid") !== "done") {
            $this->error = true;
            $this->addToMessage($message);
            $this->LogToFile("end ".$this->fichier);
            return;
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }
        $reu_is_down = false;
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            $reu_is_down = true;
        }
        if ($reu_is_down !== false) {
            $this->error = true;
            $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
            $this->addToMessage($message);
            return;
        }

        // Liste les mouvements ayant une référence à une demande et dont le
        // champ provenance_demande est vide
        $list_mv = $this->f->get_all_results_from_db_query(
            sprintf(
                'SELECT mouvement.id, mouvement.id_demande, param_mouvement.typecat
                FROM %1$smouvement
                INNER JOIN %1$sparam_mouvement
                    ON mouvement.types = param_mouvement.code
                WHERE mouvement.om_collectivite = %2$s
                AND (mouvement.id_demande!=\'\'
                    OR mouvement.id_demande IS NOT NULL)
                AND (mouvement.provenance_demande=\'\'
                    OR mouvement.provenance_demande IS NULL)
                AND LOWER(param_mouvement.typecat) IN (\'inscription\', \'radiation\')',
                DB_PREFIXE,
                intval($_SESSION["collectivite"])
            )
        );
        if ($list_mv["code"] != "OK") {
            $this->error = true;
            $this->LogToFile("=> ECHEC - Erreur de base de données lors de la récupération des mouvements ayant une référence à une demande et dont le champ provenance_demande est vide");
            $this->LogToFile("end ".$this->fichier);
            return false;
        }
        if (count($list_mv['result']) == 0) {
            $this->LogToFile("Aucun mouvement à modifier.");
            $this->LogToFile("Le traitement a été effectué avec succès.");
            $this->LogToFile("end ".$this->fichier);
            return true;
        }
        foreach ($list_mv['result'] as $mv) {
            // Récupération de la demande REU
            $params = array(
                "mode" => 'get',
                "id" => $mv['id_demande'],
            );
            $method = sprintf('handle_%s', $mv['typecat']);
            $demande = $inst_reu->$method($params);
            if ($demande["code"] != "200") {
                $this->error = true;
                $this->LogToFile(sprintf("=> ECHEC - Erreur lors de la récupération de la demande %s depuis le REU", $mv['id_demande']));
                continue;
            }

            // maj MOUVEMENT
            $fields_values = array(
                'provenance_demande' => trim(gavoe($demande["result"], array("provenance", "code", ), null)),
            );
            $cle = sprintf('id=%s', $mv['id']);
            $res1 = $this->f->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, "mouvement"),
                $fields_values,
                DB_AUTOQUERY_UPDATE,
                $cle
            );
            $this->f->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "mouvement")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                VERBOSE_MODE
            );
            //
            if ($this->f->isDatabaseError($res1, true) !== false) {
                $this->error = true;
                $message = $res1->getMessage()." - ".$res1->getUserInfo();
                $this->LogToFile($message);
                continue;
            }
            //
            $message = sprintf("Modification de la provenace de la demande du mouvement %s", $mv['id']);
            $this->LogToFile($message);
            $this->commitTransaction();
        }

        $this->LogToFile("Le traitement a été effectué avec succès.");
        $this->LogToFile("end ".$this->fichier);
        $this->commitTransaction();
        $this->error = false;
        return true;
    }
}
