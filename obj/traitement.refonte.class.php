<?php
/**
 * Ce script définit la classe 'refonteTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'refonteTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class refonteTraitement extends traitement {

    var $fichier = "refonte";

    function getValidButtonValue() {
        //
        return __("Renumerotation de la liste electorale");
    }

    function displayBeforeContentForm() {
        if ($this->f->getParameter('reu_sync_l_e_valid') != "done") {
            /**
             * Cette requête permet de compter tous les électeurs de la table 'electeur' en
             * fonction de la collectivité en cours et de la liste en cours de l'utilisateur
             * connecté.
             *
             * @param string $_SESSION["collectivite"]
             * @param string $_SESSION["liste"]
             */
            $query_count_electeur = sprintf(
                'SELECT count(*) FROM %1$selecteur WHERE electeur.liste=\'%3$s\' AND electeur.om_collectivite=%2$s',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $_SESSION["liste"]
            );

            $nbElecteur = $this->f->db->getone($query_count_electeur);
            $this->addToLog(
                __METHOD__."(): db->getone(\"".$query_count_electeur."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($nbElecteur);
            //
            echo "\n<div class=\"field\">\n\t<label>";
            echo __("Le nombre d'electeur(s) a la date du")." ";
            echo date('d/m/Y')." ".__("est de")." ".$nbElecteur.".";
            echo "</label>\n</div>\n";
        } else {
            if ($this->f->getParameter("reu_sync_refonte_date_renumerotation") != "") {
                printf(
                    "Date de la dernière renumérotation : %s",
                    $this->f->getParameter("reu_sync_refonte_date_renumerotation")
                );
            }
            if ($this->f->getParameter("reu_sync_refonte_date_synchronisation") != "") {
                printf(
                    "<br/>Date de la dernière synchronisation : %s",
                    $this->f->getParameter("reu_sync_refonte_date_synchronisation")
                );
            }
        }
    }

    function treatment () {
        // Si la synchronisation des liste électorales REU n'est pas faite on fait le traitement normal
        if ($this->f->getParameter('reu_sync_l_e_valid') != "done") {
            //
            $this->LogToFile("start refonte");

            // TRAITEMENT 2 - RENUMEROTATION ELECTEUR BUREAU
            $this->LogToFile("RENUMEROTATION ELECTEUR BUREAU");
            $bureaux = $this->f->get_all__bureau__by_my_collectivite();
            if (is_array($bureaux) !== true || count($bureaux) == 0) {
                //
                $this->error = true;
                //
                $message = "Erreur lors de la récupération des bureaux de vote";
                $this->LogToFile($message);
            } else {
                //
                foreach ($bureaux as $bureau) {
                    /**
                     * Cette requete permet de lister tous les electeurs de la table electeur sur
                     * le bureau donne en fonction de la collectivite en cours et de la liste en
                     * cours
                     *
                     * @param string $bureau['id'] Identifiant du bureau
                     * @param string $_SESSION["collectivite"]
                     * @param string $_SESSION["liste"]
                     */
                    $query_select_electeur_id_bureau = sprintf(
                        'SELECT electeur.id FROM %1$selecteur WHERE electeur.om_collectivite=%2$s AND electeur.liste=\'%3$s\' AND electeur.bureau=%4$s ORDER BY withoutaccent(lower(electeur.nom)), withoutaccent(lower(electeur.prenom))',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        $_SESSION["liste"],
                        intval($bureau['id'])
                    );
                    //
                    $res_select_electeur_id_bureau = $this->f->db->query($query_select_electeur_id_bureau);
                    if ($this->f->isDatabaseError($res_select_electeur_id_bureau, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $res_select_electeur_id_bureau->getMessage ()." - ".$res_select_electeur_id_bureau->getUserInfo ();
                        $this->LogToFile ($message);
                    } else {
                        $query_update_electeur_reset_id_bureau = sprintf(
                            'UPDATE %1$selecteur SET numero_bureau = null WHERE electeur.om_collectivite=%2$s AND electeur.liste=\'%3$s\' AND electeur.bureau=%4$s',
                            DB_PREFIXE,
                            intval($_SESSION["collectivite"]),
                            $_SESSION["liste"],
                            intval($bureau['id'])
                        );
                        $res_update_electeur_reset_id_bureau = $this->f->db->query($query_update_electeur_reset_id_bureau);
                        if ($this->f->isDatabaseError($res_update_electeur_reset_id_bureau, true)) {
                            //
                            $this->error = true;
                            //
                            $message = $res_update_electeur_reset_id_bureau->getMessage ()." - ".$res_update_electeur_reset_id_bureau->getUserInfo ();
                            $this->LogToFile ($message);
                            //
                        } else {
                            $nb_electeur_bureau = $res_select_electeur_id_bureau->numRows ();
                            $this->LogToFile ("bureau ".$bureau["code"]." ".$bureau["libelle"]." : ".$nb_electeur_bureau." ".__("electeur(s) dans le bureau a mettre a jour"));
                            //
                            $i = 0;
                            while ($row_electeur =& $res_select_electeur_id_bureau->fetchRow (DB_FETCHMODE_ASSOC)) {
                                //
                                $i++;
                                //
                                $fields_values = array ('numero_bureau' => $i);
                                $cle = "id=".$row_electeur['id'];
                                //
                                $res_update_electeur_bureau = $this->f->db->autoexecute(
                                    sprintf('%1$s%2$s', DB_PREFIXE, "electeur"),
                                    $fields_values,
                                    DB_AUTOQUERY_UPDATE,
                                    $cle
                               );
                                $this->f->addToLog(
                                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "electeur")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                                    VERBOSE_MODE
                                );
                                if ($this->f->isDatabaseError($res_update_electeur_bureau)) {
                                    //
                                    $this->error = true;
                                    //
                                    $message = $res_update_electeur_bureau->getMessage ()." - ".$res_update_electeur_bureau->getUserInfo ();
                                    $this->LogToFile ($message);
                                    //
                                    break;
                                }
                            }
                        }
                        //
                        $res_select_electeur_id_bureau->free ();
                        //
                        if ($this->error == false) {
                            //
                            $this->LogToFile ($i." ".__("electeur(s) dans le bureau ".$bureau["code"]." sur la liste ".$_SESSION["liste"]." mis a jour"));
                            //
                            $fields_values = array ('dernier_numero' => $i, 'dernier_numero_provisoire' => 9000);
                            //
                            $key = sprintf(
                                ' numerobureau.bureau=%3$s AND numerobureau.liste=\'%2$s\' AND numerobureau.om_collectivite=%1$s ',
                                intval($_SESSION["collectivite"]),
                                $_SESSION["liste"],
                                intval($bureau["id"])
                            );
                            //
                            $sql = sprintf(
                                'SELECT dernier_numero FROM %1$snumerobureau WHERE %2$s',
                                DB_PREFIXE,
                                $key
                            );
                            $res = $this->f->db->getone($sql);
                            $this->f->addToLog(
                                __METHOD__."(): db->getone(\"".$sql."\");",
                                VERBOSE_MODE
                            );
                            if ($this->f->isDatabaseError($res, true) || $res == null) {
                                //
                                $fields_values["id"] = $this->f->db->nextid(DB_PREFIXE."numerobureau");
                                $fields_values["bureau"] = intval($bureau["id"]);
                                $fields_values["liste"] = $_SESSION["liste"];
                                $fields_values["om_collectivite"] = intval($_SESSION["collectivite"]);
                                //
                                $res_update_numerobureau = $this->f->db->autoexecute(
                                    sprintf('%1$s%2$s', DB_PREFIXE, "numerobureau"),
                                    $fields_values,
                                    DB_AUTOQUERY_INSERT
                                );
                                $this->f->addToLog(
                                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "numerobureau")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_INSERT);",
                                    VERBOSE_MODE
                                );
                            } else {
                                //
                                $res_update_numerobureau = $this->f->db->autoexecute(
                                    sprintf('%1$s%2$s', DB_PREFIXE, "numerobureau"),
                                    $fields_values,
                                    DB_AUTOQUERY_UPDATE,
                                    $key
                                );
                                $this->f->addToLog(
                                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "numerobureau")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$key."\");",
                                    VERBOSE_MODE
                                );
                            }

                            if ($this->f->isDatabaseError($res_update_numerobureau)) {
                                //
                                $this->error = true;
                                //
                                $message = $res_update_numerobureau->getMessage ()." - ".$res_update_numerobureau->getUserInfo ();
                                $this->LogToFile ($message);
                            } else {
                                //
                                $message = "mise a jour du dernier numero du bureau ".$bureau["code"]." de la liste ".$_SESSION["libelle_liste"]." a ".$i;
                                $this->LogToFile ($message);
                            }
                        }
                    }
                }
            }
            //
            $this->LogToFile ("end annuel");
        //Si la synchronisation des liste électorale est faite on applique le traitement avec le module REU
        } else {
            $this->LogToFile("start refonte");
            //
            $date_reference = date("d/m/Y H:i:s");
            $this->LogToFile(sprintf(
                "date référence : %s",
                $date_reference
            ));
            $this->LogToFile("Étape 1 - Renumérotation des bureaux de vote dans le REU");
            //On récupère les bureaux de vote correspondant à la collectivité en cours
            $bureaux = $this->f->get_all__bureau__by_my_collectivite();
            if (is_array($bureaux) !== true || count($bureaux) == 0) {
                //
                $this->error = true;
                $message = "=> ECHEC - Erreur lors de la récupération des bureaux de vote";
                $this->LogToFile($message);
                $this->LogToFile("end refonte");
                return;
            }
            //
            $inst_reu = $this->f->get_inst__reu();
            if ($inst_reu === null) {
                $this->error = true;
                $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
                $this->addToMessage($message);
                $this->LogToFile("=> ECHEC - ".$message);
                $this->LogToFile("end refonte");
                return;
            }
            $reu_is_down = false;
            if ($inst_reu->is_api_up() !== true
                || $inst_reu->is_connexion_logicielle_valid() !== true) {
                $reu_is_down = true;
            }
            if ($reu_is_down !== false) {
                $this->error = true;
                $message = __("Le REU n'est pas accessible. Vérifiez vos paramètres ou/et contactez votre administrateur.");
                $this->addToMessage($message);
                $this->LogToFile("=> ECHEC - ".$message);
                $this->LogToFile("end refonte");
                return;
            }
            //On boucle sur les bureau et on appelle le handle bureau en mode renumeroter
            foreach ($bureaux as $bureau) {
                $ret = $inst_reu->handle_bureau_de_vote(array(
                        "mode" => "renumerotation",
                        "id" => $bureau['referentiel_id'],
                    ));
                if (isset($ret["code"]) !== true
                    || $ret["code"] != 200) {
                    //
                    $this->LogToFile("=> ECHEC - Erreur de transmission au REU");
                    $this->addToMessage("Erreur de transmission au REU.");
                    if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                        $this->addToMessage($ret["result"]["message"]);
                    }
                    if ($ret["code"] == 401) {
                        if ($inst_reu->is_connexion_standard_valid() === false) {
                            $this->LogToFile("=> ECHEC - L'utilisateur n'est pas connecté au Répertoire Électoral Unique");
                            $this->addToMessage(sprintf(
                                '%s : %s',
                                __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                                sprintf(
                                    '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                    __("cliquez ici pour vérifier")
                                )
                            ));
                        }
                    }
                    $this->error = true;
                    $this->LogToFile("end refonte");
                    return;
                }
                $this->LogToFile($bureau["id"]." > end");
            }
            //
            $this->LogToFile("=> OK - Renumérotation des BV dans le REU");
            $this->addToMessage("Tous les bureaux de vote ont été traités correctement dans le REU.");
            //
            $this->startTransaction();
            $ret = $this->f->insert_or_update_parameter("reu_sync_refonte_date_renumerotation", $date_reference);
            if ($ret !== true) {
                $this->rollbackTransaction();
                $this->error = true;
                $this->LogToFile("=> ECHEC - Erreur lors de la création/mise à jour du flag");
                $this->LogToFile("end refonte");
                return;
            }
            $this->LogToFile("=> OK - Création/mise à jour du flag");
            $this->commitTransaction();
            // On resynchronise les électeurs avec les nouveaux numéros de bureau de vote
            $this->startTransaction();
            $this->LogToFile("Étape 2 - Récupération des numéros d'ordre depuis le REU");
            require_once "../obj/traitement.reu_sync_l_e_reu_numero_ordre.class.php";
            $trt = new reuSyncLEREUNumeroOrdreTraitement();
            $resultat = $trt->treatment();
            if ($resultat != true) {
                $this->rollbackTransaction();
                $this->error = true;
                $this->LogToFile("=> ECHEC - Une erreur s'est produite lors de la resynchronisation des électeurs");
                $this->addToMessage("Une erreur s'est produite lors de la resynchronisation des électeurs.");
                $this->LogToFile("end refonte");
                return;
            } else {
                $this->LogToFile("=> OK - Synchronisation des numéros d'ordre");
                $this->addToMessage("Tous les électeurs ont été resynchronisés.");
                $ret = $this->f->insert_or_update_parameter("reu_sync_refonte_date_synchronisation", $date_reference);
                if ($ret !== true) {
                    $this->rollbackTransaction();
                    $this->error = true;
                    $this->LogToFile("=> ECHEC - Erreur lors de la création/mise à jour du flag");
                    $this->LogToFile("end refonte");
                    return;
                }
                $this->LogToFile("=> OK - Création/mise à jour du flag");
                $this->commitTransaction();
            }
            $this->LogToFile("end refonte");
        }
    }
}
