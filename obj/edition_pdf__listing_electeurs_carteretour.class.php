<?php
/**
 * Ce script définit la classe 'edition_pdf__listing_electeurs_carteretour'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/edition_pdf.class.php";

/**
 * Définition de la classe 'edition_pdf__listing_electeurs_carteretour' (edition_pdf).
 */
class edition_pdf__listing_electeurs_carteretour extends edition_pdf {

    /**
     * Édition PDF - affichage des cartes en retour.
     *
     * Mode d'édition possible :
     *   - par bureau
     * 
     * L'affichage par bureau utilise les paramètres passé via l'argument params ou
     * via le GET pour récupérer les informations des électeurs appartenant à la liste
     * et au bureau voulu.
     * Si aucun résultat n'est récupéré un message s'affichera pour prévenir l'utilisateur.
     *
     * L'argument $params permet de surcharger les paramètres définis par défaut. Ces
     * paramètres sont les suivants :
     *   - allborders : booleen - variable de developpement pour afficher les bordures
     *                  de toutes les cellules (defaut = false)
     *   - page_content_width : integer - taille du contenu de la page (defaut = 277)
     *   - page_header_line_height : integer - taille de la ligne d'entête (defaut = 4)
     *   - titre_libelle_ligne1 : string - 1ère ligne du titre (defaut =
     *                            "LISTE DES CARTES RETOURNÉES")
     *   - titre_libelle_ligne2 : string - 2ème ligne du titre (defaut = '')
     *   - bureau_libelle : string - libelle du bureau (defaut = '')
     *   - libelle_commune : string - libelle de la commune (defaut = $this->f->collectivite["ville"])
     *   - canton_libelle : string - libelle du canton (defaut = '')
     *   - circonscription_libelle : string - libelle de la circonscription (defaut = '')
     *   - libelle_liste : string - libelle de la liste (defaut = libelle de la
     *                     liste de la session)
     *   - collectivite : integer - identifiant de la collectivite (defaut = identifiant
     *                    de la collectivite de la session)
     *   - bureau_code : string - code du bureau (defaut = 'undefined')
     *   - liste : integer - identifiant de la liste (defaut = identifiant de la
     *                     liste de la session)
     *   - mode_edition : string - mode d'édition (defaut = '')
     *   - sql_mode : string - sql spécifique au mode d'édition (defaut = '')
     *   - message : string - message affiché si aucun résultat n'a été récupéré (defaut =
     *               'Paramètrage invalide. Veuillez contacter votre administrateur.')
     *
     * @return array
     */
    public function compute_pdf__listing_electeurs_carteretour($params = array()) {
        // Paramétrage par défaut
        // Paramétrage de l'affichage
        // Variable de developpement pour afficher les bordures de toutes les cellules
        $allborders = false;
        $page_content_width = 277;
        $page_header_line_height = 4;
        // Paramétrage du titre
        $titre_libelle_ligne1 = __("LISTE DES CARTES RETOURNÉES");
        $titre_libelle_ligne2 = '';
        // Paramétrage des libellés
        $bureau_libelle = '';
        $libelle_commune = $this->f->collectivite["ville"];
        $canton_libelle = '';
        $circonscription_libelle = '';
        $libelle_liste = $_SESSION["libelle_liste"];
        // Paramétrage des infos nécessaires à la récupération des données du PDF
        $collectivite = $_SESSION["collectivite"];
        $bureau_code = 'undefined';
        $liste = $_SESSION["liste"];
        // Paramétrage du mode de l'édition.
        // Par bureau : affichage par bureau des cartes électorales
        $mode_edition = "";
        $sql_mode = "";
        $message = __('Paramètrage invalide. Veuillez contacter votre administrateur.');

        // Récupération de l'id.
        // Si il est renseigné le paramètre id issus de l'url indique dans quel
        // paramètre récupérer l'identifiant et de quel identifiant il s'agit
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            // Récupération d'un identifiant de bureau dans le paramètre bureau
            if ($id == "bureau") {
                $mode_edition = "parbureau";
                if (isset($_GET['bureau']) && $_GET['bureau'] != '') {
                    $bureau_code = $_GET["bureau"];
                }
            }
        }

        // Récupération du paramétrage spécifique
        foreach ($params as $key => $value) {
            ${$key} = $value;
        }

        // Si le code du bureau a été récupéré ou paramétré alors on récupère l'instance
        // du bureau à partir de son code
        if ($mode_edition === 'parbureau') {
            if (! empty($bureau_code)) {
                // Récupération du bureau à partir du code bureai passé en paramètre
                $res = $this->f->get_one_result_from_db_query(sprintf(
                    'SELECT 
                        bureau.id
                    FROM 
                        %1$sbureau
                    WHERE 
                        bureau.om_collectivite=%2$s
                        AND bureau.code = \'%3$s\'',
                    DB_PREFIXE,
                    intval($collectivite),
                    $this->f->db->escapesimple($bureau_code)
                ));
                $bureau_id = $res['result'];
                if (! empty($res['result'])) {
                    // Récupération du bureau
                    $inst_bureau = $this->f->get_inst__om_dbform(array(
                        "obj" => "bureau",
                        "idx" => $bureau_id,
                    ));
                    $bureau_libelle = $inst_bureau->getVal("libelle");
                    // Récupération du canton
                    $inst_canton = $this->f->get_inst__om_dbform(array(
                        "obj" => "canton",
                        "idx" => $inst_bureau->getVal("canton"),
                    ));
                    $canton_libelle = $inst_canton->getVal("libelle");
                    // Récupération de la circonscription
                    $inst_circonscription = $this->f->get_inst__om_dbform(array(
                        "obj" => "circonscription",
                        "idx" => $inst_bureau->getVal("circonscription"),
                    ));
                    $circonscription_libelle = $inst_circonscription->getVal("libelle");
                }
            }
    
            // Récupération de la liste
            if (! empty($liste)) {
                $inst_liste = $this->f->get_inst__om_dbform(array(
                    "obj" => "liste",
                    "idx" => $liste,
                ));
                $libelle_liste = $inst_liste->getVal("liste_insee").' - '.$inst_liste->getVal("libelle_liste");
            }

            // Si les paramètres nécessaires à l'affichage ne sont pas renseignés ce message
            // sera affiché
            $sql_mode = sprintf(
                'AND bureau.code = \'%1$s\' ',
                $this->f->db->escapesimple($bureau_code)
            );
            $message = sprintf(
                __("%s\nListe %s\nAucun enregistrement selectionne pour le bureau %s"),
                $titre_libelle_ligne1,
                $libelle_liste,
                $bureau_code
            );
        }


        // Requête permettant de récupérer les informations à afficher dans l'édition
        $sql = sprintf(
            'SELECT
                (nom||\' - \'||prenom ) AS nom,
                nom_usage,
                to_char(date_naissance,\'DD/MM/YYYY\') AS naissance,
                (libelle_lieu_de_naissance||\' (\'||code_departement_naissance||\')\' ) AS lieu,
                CASE resident
                    WHEN \'Non\' then (
                        CASE numero_habitation > 0
                            WHEN True THEN
                                (numero_habitation || \' \' || complement_numero || \' \' || electeur.libelle_voie)
                            WHEN False THEN
                                electeur.libelle_voie
                        END
                    ) WHEN \'Oui\' THEN
                        adresse_resident
                END AS adresse,
                CASE resident
                    WHEN \'Non\' THEN
                        electeur.complement
                    WHEN \'Oui\' THEN
                        (complement_resident || \' \' || cp_resident || \' - \' || ville_resident)
                END AS complement,
                procuration,
                numero_bureau
            FROM
                %1$selecteur
                LEFT JOIN %1$sbureau ON electeur.bureau=bureau.id 
            WHERE
                electeur.om_collectivite=%2$s
                AND electeur.liste=\'%3$s\'
                AND carte=\'1\' 
                %4$s
            ORDER BY
                withoutaccent(lower(nom)),
                withoutaccent(lower(prenom))',
            DB_PREFIXE,
            intval($collectivite),
            $liste,
            $sql_mode
        );

        /**
         *
         */
        require_once "../obj/fpdf_table.class.php";
        $pdf = new PDF('L', 'mm', 'A4');
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(false);
        // Fixe la police utilisée pour imprimer les chaînes de caractères
        $pdf->SetFont(
            'Arial', // Famille de la police.
            '', // Style de la police.
            7 // Taille de la police en points.
        );
        // Fixe la couleur pour toutes les opérations de tracé
        $pdf->SetDrawColor(30, 7, 146);
        // Fixe les marges
        $pdf->SetMargins(
            10, // Marge gauche.
            10, // Marge haute.
            10 // Marge droite.
        );
        $pdf->SetDisplayMode('real', 'single');
        $pdf->AddPage();
        // COMMON
        $param = array(
            7, // Titre : Taille de la police en points.
            7, // Entete : Taille de la police en points.
            7, // Taille de la police en points.
            1, //
            1, //
            10, // Nombre maximum d'enregistrements par page pour la première page
            10, // Nombre maximum d'enregistrements par page pour les autres pages
            10, // Position de départ - X - La valeur de l'abscisse.
            10, // Position de départ - Y - La valeur de l'ordonnée.
            0, //
            0, //
        );
        //-------------------------------------------------------------------//
        // TITRE (PAGE HEADER)
        $page_header = $this->get_page_header_config(array(
            "allborders" => $allborders,
            "page_content_width" => $page_content_width,
            "titre_libelle_ligne1" => $titre_libelle_ligne1,
            "titre_libelle_ligne2" => $titre_libelle_ligne2,
            "libelle_commune" => $libelle_commune,
            "libelle_liste" => $libelle_liste,
            "bureau_code" => $bureau_code,
            "bureau_libelle" => $bureau_libelle,
            "canton_libelle" => $canton_libelle,
            "circonscription_libelle" => $circonscription_libelle,
            "page_header_line_height" => $page_header_line_height,
        ));
        //-------------------------------------------------------------------//
        // ENTETE (TABLE HEADER)
        $heightentete=8;
        $entete = array(
            array(
                __('Nom Patronymique, Prenoms'),
                80,$heightentete-4,2,'LRT','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('Nom d\'Usage,Date et lieu de naissance'),
                80,$heightentete-4,0,'LRB','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            array(
                __('Adresse'),
                110,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,4),0,'','NB',0,1,0),
            array(
                '<VCELL>',
                12,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,0),3,array('No','Ordre'),'NB',0,1,0),
            array(
                ' ',
                75,$heightentete,0,'1','C','0','0','0','255','255','255',array(0,8),0,'','NB',0,1,0)
        );
        //-------------------------------------------------------------------//
        // FIRSTPAGE
        $firstpage = array();
        //-------------------------------------------------------------------//
        // LASTPAGE
        $lastpage = array_merge(
            $page_header,
            array(
        array(__(' *  *  *     A  R  R  E  T  E     *  *  *'),260,10,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','B',10,1,0),
        array(' ',60,10,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(sprintf(__('La liste des cartes retournees du BUREAU No %s - %s'),$bureau_code, $bureau_libelle),180,10,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(' ',60,10,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(__('au nombre de '),25,10,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array('<LIGNE>',20,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',9,1,0),
        array(__(' electeur(s)'),25,10,1,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(' ',155,10,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(sprintf(__('%s , le %s'),$this->f->collectivite['ville'],date("d/m/Y")),75,10,1,'0','L','0','0','0','255','255','255',array(50,0),0,'','NB',0,1,0),
        array(' ',155,20,0,'0','L','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(__('Le Maire,') ,75,20,1,'0','L','0','0','0','255','255','255',array(50,0),0,'','NB',0,1,0),
        array(' ',260,30,1,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(__('Le President,'),96,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(__('Le Secretaire, '),96,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
        array(__('Les  Assesseurs,'),96,10,0,'0','C','0','0','0','255','255','255',array(0,0),0,'','NB',0,1,0),
            )
        );
        //--------------------------------------------------------------------------//
        $heightligne=15;
        $col=array(
            'nom' => array(
                80,$heightligne-10,2,'LRT','L','0','0','0','255','255','255',array(0,0),' ','', 'CELLSUP_NON',
                0,'NA','NC','','B',8,array('0')),
            'nom_usage' => array(
                80,$heightligne-10,2,'LR','L','0','0','0','255','255','255',array(0,0),'       -    ','', 'CELLSUP_NON',
                0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
            'naissance' => array(
                31,$heightligne-10,0,'LB','L','0','0','0','255','255','255',array(0,0),__('     Ne(e) le '),'', 'CELLSUP_NON',
                0,'NA','NC','','NB',0,array('0')),
            'lieu' => array(
                49,$heightligne-10,0,'B','L','0','0','0','255','255','255',array(0,0),__(' a  '),'', 'CELLSUP_NON',
                0,'NA','NC','','NB',0,array('0')),
            'adresse' => array(
                110,$heightligne-10,2,'LRT','L','0','0','0','255','255','255',array(0,10),'','', 'CELLSUP_NON',
                0,0,'NA','NC','','NB',0,array('0')),
            'complement' => array(
                110,$heightligne-10,2,'LR','L','0','0','0','255','255','255',array(0,0),'','', 'CELLSUP_NON',
                0,0,'NA','NC','','NB',0,array('0')),
            'procuration' => array(
                110,$heightligne-10,0,'LRB','L','0','0','0','255','255','255',array(0,0),'','', 'CELLSUP_NON',
                0,'NA','CONDITION',array('NN'),'NB',0,array('0')),
            'numero_bureau' => array(
                12,$heightligne,0,'1','C','0','0','0','255','255','255',array(0,10),'','', 'CELLSUP_VIDE',
                array(75,$heightligne,0,'1','C','0','0','0','255','255','255',array(0,0)),
                0,'NA','NC','','NB',0,array('0'))
        );
        //-------------------------------------------------------------------//
        $enpied = array();
        //-------------------------------------------------------------------//
        $pdf->Table(
            $page_header,
            $entete,
            $col,
            $enpied,
            $sql,
            $this->f->db,
            $param,
            $lastpage,
            $firstpage
        );

        /**
         * Affichage d'un bloc spécifique si aucun enregistrement sélectionné.
         */
        if ($pdf->msg == 1 and $message != '') {
            $pdf->SetFont('Arial','',10);
            $pdf->SetDrawColor(0,0,0);
            $pdf->SetFillColor(213,8,26);
            $pdf->SetTextColor(255,255,255);
            $pdf->MultiCell(120,5,iconv(HTTPCHARSET,"CP1252",$message), ($allborders == true ? 1 : 1),'C',1);
        }

        /**
         * OUTPUT
         */
        //
        $filename = "listing_electeurs_carteretour-".date('Ymd-His').".pdf";
        //
        $pdf_output = $pdf->Output("", "S");
        $pdf->Close();
        //
        return array(
            "pdf_output" => $pdf_output,
            "filename" => $filename,
        );
    }
}
