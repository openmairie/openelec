<?php
/**
 * Ce script définit la classe 'notificationCourrielTraitement'.
 *
 * @package openelec
 * @version SVN : $Id: traitement.redecoupage.class.php 1991 2019-04-16 21:34:00Z fmichon $
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'notificationCourrielTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class notificationCourrielTraitement extends traitement {
    var $fichier = 'notification_courriel'; 

    function getValidButtonValue() {
        //
        return __("Tester l'envoi");
    }

    function displayBeforeContentForm() {
        $template_apercu_message = '
            <div id="bloc_message" class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        Aperçu du message
                    </div>
                    <div class="card-body">
                        %s
                    </div>
                </div>
            </div>
            <div class="visualClear"></div>
        ';

        $message_apercu_multi = ' 
            <div class="card-body">    
                Aucun aperçu disponible
            </div>                   
        ';

        $message_apercu_rien_a_notifier = '
            <div class="card-body">    
                Rien à notifier.
            </div>
        ';

        if ($_SESSION['niveau'] !== '2') {
            if ($this->get_notification_message($_SESSION['collectivite']) !== false) {
                printf(sprintf($template_apercu_message, $this->get_notification_message($_SESSION['collectivite'])));
            } else {
                printf(sprintf($template_apercu_message, $message_apercu_rien_a_notifier, ''));
            }
        } else {
            printf(sprintf($template_apercu_message, $message_apercu_multi, ''));
        }
    }

    function treatment() {
        // Template des requêtes pour récupérer les informations des utilisateur en multi et en mono
        $query_select_users_by_collectivity_mono = 'SELECT email, om_profil.libelle, om_utilisateur.nom, om_collectivite.libelle as commune FROM %2$som_utilisateur INNER JOIN %2$som_collectivite ON om_utilisateur.om_collectivite = om_collectivite.om_collectivite INNER JOIN %2$som_profil ON om_profil.om_profil = om_utilisateur.om_profil WHERE om_utilisateur.om_collectivite = '.$_SESSION['collectivite'].' AND om_utilisateur.om_profil = %s';

        $query_select_users_by_collectivity_multi = 'SELECT email, om_profil.libelle, om_utilisateur.nom, om_collectivite.libelle as commune FROM %3$som_utilisateur INNER JOIN %3$som_collectivite ON om_utilisateur.om_collectivite = om_collectivite.om_collectivite INNER JOIN %3$som_profil ON om_profil.om_profil = om_utilisateur.om_profil WHERE om_collectivite.om_collectivite = %1$s AND om_collectivite.niveau != \'2\' AND om_utilisateur.om_profil = %2$s';

        // On fixe l'objet du courriel de notification
        $objet_courriel = "[openElec] Notification des tâches en cours";

        // Si nous sommes en multi 
        if ($_SESSION['niveau'] === '2') {
            // On récupère toutes les collectivité de niveau 1
            $sql_collectivite = $this->f->get_all_results_from_db_query(
                sprintf(
                    'SELECT om_collectivite, libelle FROM %som_collectivite WHERE om_collectivite.niveau !=\'2\'', 
                    DB_PREFIXE
                ),
                true
            );
            // Gestion de l'erreur de bdd
            if ($sql_collectivite['code'] === 'KO') {
                $this->error = true;
                $this->addToMessage($sql_collectivite['message']);
                return false;
            }
            // On boucle sur les collectivités
            foreach ($sql_collectivite['result'] as $key => $value) {
                // Récupération de la valeur du paramètre notification_courriel_profils
                $om_parameter_profils = $this->get_notification_courriel_profil($value['om_collectivite']);
                // Si il n'y a aucun profil paramétré
                if ($om_parameter_profils === false) {
                    $message = sprintf(
                        __("La collectivité %s n'a pas de profil paramétré."),
                        $value['libelle']
                    );
                    $this->addToMessage($message);
                        $this->LogToFile($message);
                    continue;
                } 
                // Récupération du contenu du courriel
                $notification_message = $this->get_notification_message($value['om_collectivite']);
                // Récupération des utilisateurs à notifier
                $email_users = array();
                $profils_multi = explode(";", $om_parameter_profils['result']);
                foreach ($profils_multi as $key => $profil) {
                    // On récupère les utilisateurs des profils sélectionnés
                    $res_value = $this->f->get_all_results_from_db_query(
                        sprintf(
                            $query_select_users_by_collectivity_multi,
                            $value['om_collectivite'],
                            $profil,
                            DB_PREFIXE
                        ),
                        true
                    );
                    // Gestion d'erreur bdd
                    if ($res_value['code'] === 'KO'){
                        $this->error = true;
                        $this->addToMessage($res_value['message']);
                    }
                    // On insère les information dans un tableau
                    if (empty($res_value['result']) !==true ){
                        foreach ($res_value['result'] as $key => $user_info) {
                           array_push($email_users, $user_info);
                        }
                    }
                }
                // Si aucun utilisateur n'est à notifier
                if (empty($email_users) === true ){
                    $message = sprintf(
                        __("Aucun utilisateur à notifier pour la collectivité %s."),
                        $value['libelle']
                    );
                    $this->addToMessage($message);
                    $this->LogToFile($message);
                    continue;
                }
                // Création d'un tableau pour gérer le message de fin de traitement
                $cpt = array("sended" => 0, "ok" => 0, "error" => 0, "users" => array("infos" => array()));
                // Boolean permettant de signaler si il n'y a pas de tâche en cours
                $flag_taches_en_cours = false;
                foreach ($email_users as $key => $email) {
                    if($notification_message !== false) {
                        // On envoie le mail aux adresses
                        $sended = $this->f->sendMail(
                            $objet_courriel,
                            str_replace(
                                "[ADRESSE_COURRIEL]",
                                $email['email'],
                                $notification_message
                            ),
                            $email['email']
                        );
                        $cpt["sended"]++;
                        if ($sended === true) {
                            $cpt["ok"]++;
                        } else {
                            $cpt["error"]++;
                            array_push($cpt["users"]["infos"], array("email" => $email['email'], "nom" => $email['nom'], "commune" => $email['commune']));
                        }
                    } else {
                        $message = sprintf(
                            __("Rien à notifier pour la collectivité %s."),
                           $value['libelle']
                        );
                        $flag_taches_en_cours = true;
                        $this->addToMessage($message);
                        $this->LogToFile($message);
                    }
                }
                // Gestion des messages
                if ($cpt["error"] === 0 && $flag_taches_en_cours !== true) {
                    $message = sprintf(
                        __("Notification(s) envoyée(s) avec succès pour la collectivité %s."),
                        $value['libelle']
                    );
                    $this->addToMessage($message);
                    $this->LogToFile($message);
                } elseif ($cpt["error"] !== 0) {
                    $this->error = true;
                    foreach ($cpt['users']['infos'] as $emails) {
                        $message = sprintf(
                            __("La notification n'a pas pu être envoyée à l'utilisateur se prénomant %s de la commune %s ayant l'adresse email %s."),
                            $emails['nom'],
                            $emails['commune'],
                            $emails['email']
                        ); 
                        $this->addToMessage($message);
                        $this->LogToFile($message);
                    }
                }
            }
            return true;
        }
        // On est en mono
        $om_parameter_profils = $this->get_notification_courriel_profil($_SESSION['collectivite']);
        $notification_message = $this->get_notification_message($_SESSION['collectivite']);
        $email_users = array();
        // Vérirication de la valeur de l'om_parametre notification_courriel_profils
        if ($om_parameter_profils === false) {
                $message = __("Aucun profil n'a été sélectionné.");
                $this->addToMessage($message);
                $this->LogToFile($message);
                return true;
        } else {
            $profils_multi = explode(";", $om_parameter_profils['result']);
            // On boucle sur les profils
            foreach ($profils_multi as $key => $profil) {
                $res_value = $this->f->get_all_results_from_db_query(
                    sprintf(
                        $query_select_users_by_collectivity_mono,
                        $profil,
                        DB_PREFIXE
                    ),
                    true
                );
                // Gestion de l'erreur bdd
                if ($res_value['code'] === 'KO'){
                    $this->error = true;
                    $message = __('Erreur de base de données lors de la récupération des utilisateur de la collectivité');
                    $this->addToMessage($message);
                    $this->addToLog($message);
                    return false;
                }
                // Insertion des informations dans un tableau
                if (empty($res_value['result']) !==true ){
                    foreach ($res_value['result'] as $key => $user_info) {
                        array_push($email_users, $user_info);
                    }
                }
            }
            // Si le tableu est vide on arrête le traitement
            if (empty($email_users) === true ){
                $message = __("Aucun utilisateur ne peut recevoir la notification.");
                $this->addToMessage($message);
                $this->LogToFile($message);
                return true; 
            }
            // Création d'un tableau pour gérer le message de fin de traitement
            $cpt = array("sended" => 0, "ok" => 0, "error" => 0, "users" => array("infos" => array()));
            $flag_taches_en_cours = false;
            foreach ($email_users as $key => $email) {
                if($notification_message !== false) {
                    $sended = $this->f->sendMail(
                        $objet_courriel,
                        str_replace(
                            "[ADRESSE_COURRIEL]",
                            $email['email'],
                            $notification_message
                        ),
                        $email['email']
                    );
                    $cpt["sended"]++;
                    if ($sended === true) {
                        $cpt["ok"]++;
                    } else {
                        $cpt["error"]++;
                        array_push($cpt["users"]["infos"], array("email" => $email['email'], "nom" => $email['nom'], "commune" => $email['commune']));
                    }
                } else {
                    $message = __("Rien à notifier.");
                    $flag_taches_en_cours = true;
                    $this->addToMessage($message);
                    $this->LogToFile($message);
                }
            }
            if ($cpt["error"] == 0 && $flag_taches_en_cours !== true){
                $message = __("La notification a été envoyée avec succès.");
                $this->addToMessage($message);
                $this->LogToFile($message);
            } elseif ($cpt["error"] !== 0) {
                $this->error = true;
                foreach ($cpt['users']['infos'] as $emails) {        
                    $message = sprintf(
                        __("La notification n'a pas pu être envoyée à l'utilisateur se prénomant %s de la commune %s ayant l'adresse email %s."),
                        $emails['nom'],
                        $emails['commune'],
                        $emails['email']
                    ); 
                    $this->addToMessage($message);
                    $this->LogToFile($message);
                }
            }
        }
        return true;
    }

    /**
     * @return boolean|string
     */
    function get_notification_message($om_collectivite) {
        $this->change_collectivite_context($om_collectivite);
        $insee = $this->f->getParameter("inseeville");
        $message_connexion_logicielle = $this->get_message_connexion_logicielle($om_collectivite);
        $this->reset_collectivite_context();
        $message_taches_en_cours = $this->get_message_taches_en_cours($om_collectivite);
        if ($message_connexion_logicielle === false
            && $message_taches_en_cours === false) {
            return false;
        }
        return sprintf(
            '<p>Bonjour,</p>

%s
%s
<p>--</p>
<p>Votre assistant openElec.</p>
<p><i>Merci de ne pas répondre à ce courriel envoyé à [ADRESSE_COURRIEL], par le logiciel openElec pour la commune %s.</i></p>
            ',
            $message_connexion_logicielle,
            $message_taches_en_cours,
            $insee
        );
    }

    function change_collectivite_context($om_collectivite) {
        $this->_origin_collectivite = $_SESSION['collectivite'];
        $_SESSION['collectivite'] = $om_collectivite;
        $this->f->getCollectivite();
    }
    function reset_collectivite_context() {
        $_SESSION['collectivite'] = $this->_origin_collectivite;
        $this->f->getCollectivite();
    }

    /**
     * @return boolean|string
     */
    function get_message_connexion_logicielle($om_collectivite) {
        $inst_reu = $this->f->get_inst__reu(true);
        if ($inst_reu == null) {
            return false;
        }
        if ($inst_reu->is_connexion_logicielle_valid() === true) {
            $inst_reu->logout_connexion_logicielle();
            $inst_reu->__destruct();
            return false;
        }
        $inst_reu->logout_connexion_logicielle();
        $inst_reu->__destruct();
        return sprintf(
            '<p><b>%s</b></p>
',
            ERROR_MSG_REU_CONNECT
        );
    }

    /**
     * @return boolean|string
     */
    function get_message_taches_en_cours($om_collectivite) {
        $om_widget = $this->f->get_inst__om_dbform(array(
            "obj" => "om_widget",
            "idx" => 0,
        ));

        $cfg_taches_en_cours = $om_widget->get_config_taches_en_cours("mouvements");
        $message_content = "";
        foreach ($cfg_taches_en_cours["taches"] as $line) {
            $sqlquery_condition = "";
            foreach($line["criteria"] as $criteria) {
                if (array_key_exists("type", $criteria) === true
                    && $criteria["type"] === "boolean") {
                    //
                    $sqlquery_condition .= sprintf(
                        '%s IS %s AND ',
                        $criteria["sqlquery"],
                        ($criteria["value"] == 't' ? "TRUE" : "FALSE")
                    );
                    continue;
                }
                $sqlquery_condition .= sprintf(
                    '%s=\'%s\' AND ',
                    $criteria["sqlquery"],
                    $criteria["value"]
                );
            }
            $sqlquery_condition = substr($sqlquery_condition, 0, strlen($sqlquery_condition) - 4);
            $nb = $this->f->get_one_result_from_db_query(sprintf(
                '%1$s WHERE %3$s %4$s',
                $cfg_taches_en_cours["prefix_query_taches_en_cours"],
                DB_PREFIXE,
                sprintf('%1$s.om_collectivite=%2$s AND ', $line["table"], intval($om_collectivite)),
                $sqlquery_condition
            ));
            if ($nb['result'] != 0) {
                $message_content .= sprintf(
                    '    <li>%1$s %2$s : %3$s</li>
',
                    $line["bloc"],
                    $line["title"],
                    $nb["result"]
                );
            }
        }
        if($message_content == ''){
            return false;
        }

        return sprintf(
            '<p>Vous avez à ce jour :</p>
<ul>%s</ul>',
            $message_content
        ); 
    }

    function get_notification_courriel_profil($om_collectivite){
        // Template de la requête permettant de récupérer les valeur du paramètre notification_courriel_profils
        $requete_om_parameter_template =  'SELECT valeur FROM %2$som_parametre INNER JOIN %2$som_collectivite on om_parametre.om_collectivite = om_collectivite.om_collectivite WHERE om_parametre.libelle = \'notification_courriel_profils\' %1$s %3$s';
        // Condition pour la collectivité de niveau 2
        $requete_where_multi = 'AND om_collectivite.niveau = \'2\'';
        // Condition par identifiant de collectivité
        $requete_where_mono = 'AND om_collectivite.om_collectivite = ';
        $res_om_parameter = $this->f->get_one_result_from_db_query(
            sprintf(
                $requete_om_parameter_template, 
                $requete_where_mono, 
                DB_PREFIXE, 
                $om_collectivite
            )
        );
        if (empty($res_om_parameter['result']) === true) {
            $res_om_parameter = $this->f->get_one_result_from_db_query(
                sprintf(
                    $requete_om_parameter_template, 
                    $requete_where_multi, 
                    DB_PREFIXE,
                    ''
                )
            );
        }
        if (empty($res_om_parameter['result']) === true){
            return false;
        } else {
            return $res_om_parameter;
        }
    }
}
