<?php
/**
 * Ce script définit la classe 'module_carte_electorale'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_carte_electorale' (module).
 */
class module_carte_electorale extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "electeur_edition_pdf__carte_electorale",
            "title" => __("Éditions")." -> ".__("Cartes électorales"),
            "condition" => array("is_collectivite_mono", ),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "electeur_edition_pdf__carte_electorale",
                    "href" => "../app/index.php?module=module_carte_electorale&view=main",
                    "title" => __("Cartes électorales"),
                    "view" => "main",
                ),
            ),
        );
    }

    /**
     *
     */
    function view_main() {
        parent::view_main();
        if (isset($_GET["enable_cart"])) {
            $this->view__enable_cart();
        }
        if (isset($_GET["disable_cart"])) {
            $this->view__disable_cart();
        }
    }

    /**
     *
     */
    function view__main() {
        //
        ob_start();
        $this->view__onglet1();
        $onglet1 = ob_get_clean();
        ob_start();
        $this->view__onglet2();
        $onglet2 = ob_get_clean();
        ob_start();
        $this->view__onglet3();
        $onglet3 = ob_get_clean();
        ob_start();
        $this->view__onglet4();
        $onglet4 = ob_get_clean();
        ob_start();
        $this->view__onglet5();
        $onglet5 = ob_get_clean();
        printf(
            '
            <div class="container-fluid" id="module-carte-electorale-main-tab">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                Édition pour toute la commune
                            </div>
                            <div class="card-body">
                                %1$s
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                Édition par bureau de vote
                            </div>
                            <div class="card-body">
                                %2$s
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                Édition par lot
                            </div>
                            <div class="card-body">
                                %3$s
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                Édition depuis la date d\'inscription ou de dernière modification
                            </div>
                            <div class="card-body">
                                %4$s
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                Édition par date de naissance
                            </div>
                            <div class="card-body">
                                %5$s
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="visualClear"></div>
            ',
            $onglet1,
            $onglet2,
            $onglet3,
            $onglet4,
            $onglet5
        );
    }

    /**
     *
     */
    function view__onglet1() {
        echo "<table class=\"table\">\n";
        foreach ($this->f->get_all__listes() as $key => $value) {
            //
            $edition = array (
                'pdf' => "../app/index.php?module=form&obj=electeur&action=303&mode_edition=commune&stockage=true&origin=module&liste=".$value["liste"],
                'generer' => "OUI",
                'filename' => "carteelecteur-".$_SESSION["collectivite"]."-".$value["liste"].".pdf",
                'id' => "edition-pdf-carteelecteur",
                "right" => "electeur_edition_pdf__carte_electorale",
            );
            //
            
            echo "\t<tr class=\"editions\">";
            echo "<td class=\"title\">".$value["libelle_liste"]."</td>";
            echo "<td class=\"icone\">";
            //
            $sql = sprintf(
                'SELECT * FROM %1$sfichier WHERE fichier.om_collectivite=%2$s AND fichier.filename=\'%3$s\' ORDER BY creationdate DESC, creationtime DESC LIMIT 1',
                DB_PREFIXE,
                intval($_SESSION["collectivite"]),
                $edition['filename']
            );
            $res = $this->f->db->query($sql);
            $this->f->addToLog(
                __METHOD__."(): db->query(\"".$sql."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
            //
            $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
            //
            if ($row != null) {
                echo "<a class=\"lien\" id=\"".$edition["id"]."\" href=\"".OM_ROUTE_FORM."&snippet=file&uid=".$row["uid"]."\" target=\"_blank\">";
                echo "<span class=\"om-icon om-icon-25 om-icon-fix pdf-25\"><!-- --></span>";
                echo "<br />";
                echo __("Télécharger");
                echo "</a>";
                echo "</td><td>";
                echo " <a class=\"lien\" href=\"".$edition ['pdf']."\" id=\"".$edition["id"]."-generer\">".__("Generer")."</a><br />";
                echo " Taille : [".number_format($row["size"], 0, ',', ' ')." Octets]<br />";
                echo " Date : [". $this->f->formatDate($row["creationdate"])." à ".$row["creationtime"]."]";
            } else {
                echo "&nbsp;</td><td>";
                echo " <a class=\"lien\" href=\"".$edition ['pdf']."\" id=\"".$edition["id"]."-generer\">".__("Generer")."</a><br />";
                echo __("Aucun fichier n'a ete genere");
            }
            echo "</td>";
            echo "</tr>\n";
        }
        echo "</table>";
    }

    /**
     *
     */
    function view__onglet2() {
        //
        $this->f->layout->display__form_container__begin(array(
            "action" => "../app/index.php?module=form&obj=electeur&action=303&mode_edition=parbureau",
            "id" => "carte_electorale_par_bureau",
            "target" => "_blank",
        ));
        $champs = array("liste", "bureau", "tri", );
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
        ));
        //
        $form->setLib("liste", __("Liste"));
        $form->setType("liste", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__listes() as $key => $liste) {
            array_push($contenu[0], $liste["liste"]);
            array_push($contenu[1], $liste["libelle_liste"]);
        }
        $form->setSelect("liste", $contenu);
        //
        $form->setLib("bureau", __("Bureau de vote"));
        $form->setType("bureau", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__bureau__by_my_collectivite() as $key => $bureau) {
            array_push($contenu[0], $bureau["code"]);
            array_push($contenu[1], $bureau["code"]." ".$bureau["libelle"]);
        }
        $form->setSelect("bureau", $contenu);
        // Paramétrage du champs de sélection du mode de tri des cartes électorale dans l'édition
        $form->setLib("tri", __("Trier par"));
        $form->setType("tri", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        $form->setSelect("tri", array(
            '0' => array(
                'alphabetique',
                'date_naissance',
                'voie'
            ),
            '1' => array(
                __('ORDRE ALPHABÉTIQUE'),
                __('DATE DE NAISSANCE'),
                __('VOIE')
            )
        ));
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "carte_electorale_par_bureau.submit",
            "value" => __("Télécharger"),
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }

    /**
     *
     */
    function view__onglet3() {
        if (isset($_GET["enable_cart"])) {
            $this->view__enable_cart();
        }
        if (isset($_GET["disable_cart"])) {
            $this->view__disable_cart();
        }
        if (isset($_SESSION["panier_carte_electorale"]) !== true) {
            echo '<p>Votre panier de carte électorale est désactivé. Cliquer <a href="../app/index.php?module=module_carte_electorale&enable_cart=1">ici</a> pour l\'activer.</p>';
        } else {
            printf(
                '<p>%s carte(s) électorale(s) dans votre panier</p>',
                count($_SESSION["panier_carte_electorale"])
            );
            echo "<p><a class=\"lien\" href=\"../app/index.php?module=form&obj=electeur&action=303&mode_edition=multielecteur&electeur_ids=".implode(";", $_SESSION["panier_carte_electorale"])."\" target=\"_blank\">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix pdf-25\"><!-- --></span>";
            echo "<br />";
            echo __("Télécharger");
            echo "</a></p>";
            echo "<br/>";
            echo '<p>Cliquer <a href="../app/index.php?module=module_carte_electorale&disable_cart=1">ici</a> pour désactiver/vider le panier.</p>';
        }
    }

    /**
     *
     */
    function view__onglet4() {
        echo "<b>Source openElec</b>";
        //
        $this->f->layout->display__form_container__begin(array(
            "action" => "../app/index.php?module=form&obj=electeur&action=303&mode_edition=lastone&source_lastone=oel",
            "target" => "_blank",
            "id" => "carte_electorale_par_date_source_oel",
        ));
        $champs = array("liste", "date", "sans_changement_de_bureau", "datedenaissance_au", "order_by_datenaissance");
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
        ));
        //
        $form->setLib("liste", __("Liste"));
        $form->setType("liste", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__listes() as $key => $liste) {
            array_push($contenu[0], $liste["liste"]);
            array_push($contenu[1], $liste["libelle_liste"]);
        }
        $form->setSelect("liste", $contenu);
        //
        $form->setLib("date", __("Date"));
        $form->setType("date", "date");
        $form->setTaille("date", 12);
        $form->setMax("date", 12);
        $form->setOnchange("date", "fdate(this);");
        //
        $form->setLib("sans_changement_de_bureau", __("Prendre en compte les modifications sans changement de bureau ?"));
        $form->setType("sans_changement_de_bureau", "checkbox");
        $form->setTaille("sans_changement_de_bureau", 1);
        $form->setMax("sans_changement_de_bureau", 1);
        //
        $form->setLib("datedenaissance_au", __("Date de naissance (max)"));
        $form->setType("datedenaissance_au", "date");
        $form->setTaille("datedenaissance_au", 12);
        $form->setMax("datedenaissance_au", 12);
        $form->setOnchange("datedenaissance_au", "fdate(this);");
        //
        $form->setLib("order_by_datenaissance", __("Trier les cartes par date de naissance ?"));
        $form->setType("order_by_datenaissance", "checkbox");
        $form->setTaille("order_by_datenaissance", 1);
        $form->setMax("order_by_datenaissance", 1);
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "carte_electorale_par_date_source_oel.submit",
            "value" => __("Télécharger"),
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
        //
        echo "<b>Source REU</b>";
        $this->f->layout->display__form_container__begin(array(
            "action" => "../app/index.php?module=form&obj=electeur&action=303&mode_edition=lastone&source_lastone=reu",
            "target" => "_blank",
            "id" => "carte_electorale_par_date_source_reu",
        ));
        $champs = array("liste", "date", "datedenaissance_au", "order_by_datenaissance");
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
        ));
        //
        $form->setLib("liste", __("Liste"));
        $form->setType("liste", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__listes() as $key => $liste) {
            array_push($contenu[0], $liste["liste"]);
            array_push($contenu[1], $liste["libelle_liste"]);
        }
        $form->setSelect("liste", $contenu);
        //
        $form->setLib("date", __("Date"));
        $form->setType("date", "date");
        $form->setTaille("date", 12);
        $form->setMax("date", 12);
        $form->setOnchange("date", "fdate(this);");
        //
        $form->setLib("datedenaissance_au", __("Date de naissance (max)"));
        $form->setType("datedenaissance_au", "date");
        $form->setTaille("datedenaissance_au", 12);
        $form->setMax("datedenaissance_au", 12);
        $form->setOnchange("datedenaissance_au", "fdate(this);");
        //
        $form->setLib("order_by_datenaissance", __("Trier les cartes par date de naissance ?"));
        $form->setType("order_by_datenaissance", "checkbox");
        $form->setTaille("order_by_datenaissance", 1);
        $form->setMax("order_by_datenaissance", 1);
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "carte_electorale_par_date_source_reu.submit",
            "value" => __("Télécharger"),
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }

    /**
     *
     */
    function view__enable_cart() {
        if (isset($_SESSION["panier_carte_electorale"]) !== true) {
            $_SESSION["panier_carte_electorale"] = array();
        }
    }

    /**
     *
     */
    function view__disable_cart() {
        if (isset($_SESSION["panier_carte_electorale"]) === true) {
            unset($_SESSION["panier_carte_electorale"]);
        }
    }

    /**
     *
     */
    function view__onglet5() {
        //
        $this->f->layout->display__form_container__begin(array(
            "action" => "../app/index.php?module=form&obj=electeur&action=303&mode_edition=pardatedenaissance",
            "id" => "carte_electorale_par_datedenaissance",
            "target" => "_blank",
        ));
        $champs = array("liste", "datedenaissance_du", "datedenaissance_au", );
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
        ));
        //
        $form->setLib("liste", __("Liste"));
        $form->setType("liste", "select");
        $contenu = array(
            0 => array(),
            1 => array(),
        );
        foreach ($this->f->get_all__listes() as $key => $liste) {
            array_push($contenu[0], $liste["liste"]);
            array_push($contenu[1], $liste["libelle_liste"]);
        }
        $form->setSelect("liste", $contenu);
        //
        $form->setLib("datedenaissance_du", __("Date de naissance (du)"));
        $form->setType("datedenaissance_du", "date");
        $form->setTaille("datedenaissance_du", 12);
        $form->setMax("datedenaissance_du", 12);
        $form->setOnchange("datedenaissance_du", "fdate(this);");
        //
        $form->setLib("datedenaissance_au", __("Date de naissance (au)"));
        $form->setType("datedenaissance_au", "date");
        $form->setTaille("datedenaissance_au", 12);
        $form->setMax("datedenaissance_au", 12);
        $form->setOnchange("datedenaissance_au", "fdate(this);");
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => "carte_electorale_par_datedenaissance.submit",
            "value" => __("Télécharger"),
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }
}
