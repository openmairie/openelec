<?php
/**
 * Ce script définit la classe 'om_formulaire'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_formulaire.class.php";

/**
 * Définition de la classe 'om_formulaire' (om_formulaire).
 *
 * Cette classe est destinée à permettre la surcharge de certaines méthodes de
 * la classe 'om_formulaire' du framework pour des besoins spécifiques de
 * l'application.
 */
class om_formulaire extends formulaire {

    /**
     * WIDGET_FORM - infos_archive_electeur.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function infos_archive_electeur($champ, $validation, $DEBUG = false) {
        //
        $infos = $this->select[$champ];
        $expected_infos = array(
            "civilite",
            "nom",
            "nom_usage",
            "prenom",
            "date_naissance",
            "sexe",
            "code_departement_naissance",
            "libelle_departement_naissance",
            "code_lieu_de_naissance",
            "libelle_lieu_de_naissance",
            "libelle_nationalite",
            "numero_habitation",
            "complement_numero",
            "libelle_voie",
            "complement",
            "cp_voie",
            "ville_voie",
            "resident",
            "adresse_resident",
            "complement_resident",
            "cp_resident",
            "ville_resident",
            "telephone",
            "courriel",
        );
        if (is_array($infos) !== true) {
            $infos = array();
        }
        foreach ($expected_infos as $expected_info) {
            if (array_key_exists($expected_info, $infos) !== true) {
                $infos[$expected_info] = "";
            }
        }
        $output = "";
        $output .= sprintf(
            '<div class="col-md-4" style="float:left"><h4>État civil</h4><p>%s<br/>%s<br/>Sexe : %s<br/>Pays/Département : %s<br/>Commune : %s<br/>Nationalité : %s</p></div>',
            trim($infos["civilite"]." ".$infos["nom"]." / ".$infos["nom_usage"]." / ".$infos["prenom"]),
            trim($this->f->formatDate($infos["date_naissance"])),
            trim($infos["sexe"]),
            trim($infos["code_departement_naissance"]." ".$infos["libelle_departement_naissance"]),
            trim($infos["code_lieu_de_naissance"]." ".$infos["libelle_lieu_de_naissance"]),
            trim($infos["libelle_nationalite"])
        );
        $output .= sprintf(
            '<div class="col-md-4" style="float:left"><h4>Adresse de rattachement</h4><p>%s<br/>%s<br/>%s</p></div>',
            trim($infos["numero_habitation"]." ".$infos["complement_numero"]." ".$infos["libelle_voie"]),
            trim($infos["complement"]),
            trim($infos["cp_voie"]." ".$infos["ville_voie"])
        );
        $output .= sprintf(
            '<div class="col-md-4" style="float:left"><h4>Informations de contact</h4><p>Adresse de contact : %s%s<br/>Téléphone : %s<br/>Courriel : %s</p></div>',
            trim($infos["resident"]),
            ($infos["resident"] == "Non" ? "" : sprintf(
                '<br/>%s<br/>%s<br/>%s',
                trim($infos["adresse_resident"]),
                trim($infos["complement_resident"]),
                trim($infos["cp_resident"]." ".$infos["ville_resident"])
            )),
            trim($infos["telephone"]),
            trim($infos["courriel"])
        );
        //
        echo $output;
    }

    /**
     * WIDGET_FORM - electeur.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function electeur($champ, $validation, $DEBUG = false) {
        //
        if ($this->val[$champ] == 0) {
            echo "-";
            return;
        }
        //
        echo "<input";
        echo " type=\"hidden\"";
        echo " id=\"".$champ."\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->val[$champ]."\"";
        echo " class=\"champFormulaire\"";
        echo " />\n";
        if (count($this->select[$champ]) == 0) {
            echo "Id : ".$this->val[$champ];
            echo " => Aucun électeur dans la liste électorale";
            return;
        }
        $infos = $this->select[$champ];
        //
        $output = "<div class=\"formwidget_electeur\">";
        //
        if ($this->f->isAccredited(array("electeur", "electeur_consulter"), "OR")) {
            $output .= sprintf(
                'Id : <a id="id_electeur_link" href="%1$s&obj=electeur&action=3&idx=%2$s">%2$s</a>',
                OM_ROUTE_FORM,
                $this->val[$champ]
            );
        } else {
            $output .= sprintf(
                'Id : %1$s',
                $this->val[$champ]
            );
        }
        $output .= " => ";
        $output .= $infos["civilite"];
        $output .= " ";
        $output .= "<span class=\"nom\" title=\""._("nom")."\">";
        $output .= $infos["nom"];
        $output .= "</span>";
        $output .= " ";
        if ($infos["nom_usage"] != "") {
            $output .= "- ";
            $output .= "<span class=\"nom_usage\" title=\""._("nom d'usage")."\">";
            $output .= $infos["nom_usage"];
            $output .= "</span>";
            $output .= " ";
        }
        $output .= "<span class=\"prenom\" title=\""._("prenom(s)")."\">";
        $output .= $infos["prenom"];
        $output .= "</span>";
        $output .= " ";
        $output .= _("Ne(e) le")." ";
        $output .= $this->f->formatDate($infos["date_naissance"], true);
        $output .= "</div>";
        //
        echo $output;
    }

    /**
     *
     */
    protected function snippet__trt_ajax() {
        // Correction d'un bug rencontré sur PHP5.3
        // Il apparaît que les clés dans les POST n'étaient pas
        // correctement reconnus si elles étaient trop longues
        // et qu'elle contenaient des points. Ce qui est le cas
        // dans les formulaires de traitement.
        function getRealPOST() {
            $pairs = explode("&", file_get_contents("php://input"));
            $vars = array();
            foreach ($pairs as $pair) {
                $nv = explode("=", $pair);
                if (isset($nv[0])) {
                    $name = urldecode($nv[0]);
                } else {
                    continue;
                }
                if (isset($nv[1])) {
                    $value = urldecode($nv[1]);
                } else {
                    $value = "";
                }
                $vars[$name] = $value;
            }
            return $vars;
        }
        $_POST_alternative = getRealPOST();

        /**
         * Definition du charset de la page
         */
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        //
        $obj = "";
        if (isset($_GET["obj"])) {
            $obj = $_GET["obj"];
        }

        //
        if (strpos($obj, "/") !== false
            or !file_exists("../obj/traitement.".$obj.".class.php")) {
            die();
        }

        //
        if (isset($_POST["traitement_".$obj."_form_valid"])
            || isset($_POST_alternative["traitement.".$obj.".form.valid"])) {
            //
            require_once "../obj/traitement.".$obj.".class.php";
            //
            $constructeur = str_replace(" ", "", ucwords(str_replace("_", " ", $obj)))."Traitement";
            //
            $trt = new $constructeur();
            $trt->execute();
        }

        //
        if (isset($_GET["action"]) && $_GET["action"] = "traitement_".$obj) {
            //
            require_once "../obj/traitement.".$obj.".class.php";
            //
            $constructeur = str_replace(" ", "", ucwords(str_replace("_", " ", $obj)))."Traitement";
            //
            $trt = new $constructeur();
            $trt->displayFormSection();
        }
    }

    /**
     * WIDGET_FORM - select_multiple_permissions.
     *
     * ...
     *
     * @param string  $champ      Nom du champ.
     * @param integer $validation Etat de la validation du formulaire.
     * @param boolean $DEBUG      Parametre inutilise.
     *
     * @return void
     */
    function select_multiple_permissions($champ, $validation, $DEBUG = false) {
        //
        $this->select_multiple($champ, $validation, $DEBUG);
    }


    /**
     * WIDGET_FORM - ine.
     *
     * INE
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function ine($champ, $validation, $DEBUG = false) {
        $this->text($champ, $validation, $DEBUG);
        if (!$this->correct) {
            printf(
                '<span style="display:none;" data-href="%s" class="form-snippet-search_ine"><!-- --></span>',
                "".OM_ROUTE_FORM."&snippet=search_ine"
            );

            //
            printf(
                '<a class="search_ine ui-state-default ui-corner-all" href="javascript:%s(\'%s\');">
                    <span class="" title="%s">%s</span>
                </a>
                ',
                "snippet_search_ine",
                $champ,
                __("Cliquer ici pour chercher l'ine à partir de l'état civil"),
                __("Rechercher INE")
            );
        }
    }

    /**
     * SNIPPET_FORM - search_ine.
     */
    protected function snippet__search_ine() {
        // Initialisation des paramètres
        $params = array(
            "origine" => array(
                "default_value" => "",
                "method" => array("get", ),
            ),
            "recherche_par" => array(
                "default_value" => "",
                "method" => array("post", ),
            ),
            "nom" => array(
                "default_value" => "",
                "method" => array("post", ),
            ),
            "prenom" => array(
                "default_value" => "",
                "method" => array("post", ),
            ),
            "sexe" => array(
                "default_value" => "",
                "method" => array("post", ),
            ),
            "datenaissance" => array(
                "default_value" => "",
                "method" => array("post", ),
            ),
            "ine" => array(
                "default_value" => "",
                "method" => array("post", ),
            ),
            "choix_ine" => array(
                "default_value" => "",
                "method" => array("post", ),
            ),
            "form" => array(
                "default_value" => "f1",
                "method" => array("get", ),
            ),
        );
        foreach ($this->f->get_initialized_parameters($params) as $key => $value) {
            ${$key} = $value;
        }

        /**
         * Verification des parametres
         */
        if ($origine == "") {
            //
            if ($this->f->isAjaxRequest() == false) {
                $this->f->setFlag(NULL);
                $this->f->display();
            }
            $class = "error";
            $message = __("L'objet est invalide.");
            $this->f->displayMessage($class, $message);
            die();
        }

        if (isset($_POST["results-submited"]) && $choix_ine != "") {
            if ($this->f->isAjaxRequest()) {
                printf(
                    '<script type="text/javascript">snippet_return_one_value(\'%1$s\', \'%2$s\', \'%3$s\', \'%4$s\');</script>',
                    "search_ine",
                    $form,
                    $origine,
                    $choix_ine
                );
                return;
            } else {
                printf(
                    '<script type="text/javascript">parent.opener.document.%1$s.%2$s.value=\'%3$s\'; parent.close();</script>',
                    $form,
                    $origine,
                    $choix_ine
                );
                return;
            }
        }

        /**
         * Affichage de la structure HTML
         */
        if ($this->f->isAjaxRequest()) {
            header("Content-type: text/html; charset=".HTTPCHARSET."");
        } else {
            //
            $this->f->setFlag("htmlonly");
            $this->f->display();
        }
        //
        $this->f->displayStartContent();
        //
        $this->f->setTitle(__("Recherche d'électeur"));
        $this->f->displayTitle();

        /**
         * Gestion des erreurs
         */
        //
        $error = false;
        // Verification du post vide
        if (isset($_POST["submited"])) {
            // Condition d'erreur
            $error_empty = false;
            if ($recherche_par == ""
                || ($recherche_par == "etat_civil" && ($nom == "" || $datenaissance == "" || $sexe == ""))
                || ($recherche_par == "ine" && $ine == "")) {
                //
                $error_empty = true;
            }
            //
            $error_date = ($datenaissance != "" and $this->f->formatDate($datenaissance) == false ? true : false);
            //
            $error_ine = false;
            if ($recherche_par == "ine"
                && (intval(str_replace(" ", "", $ine)) < 0 || intval(str_replace(" ", "", $ine)) > 2147483647)) {
                //
                $error_ine = true;
            }
            // Affichage du message d'erreur si besoin
            if ($error_empty) {
                $error = true;
                $message_class = "error";
                $message = __("Vous devez saisir au moins un critere de recherche.");
                $this->f->displayMessage($message_class, $message);
            }
            if ($error_date) {
                $error = true;
                $message_class = "error";
                $message = __("La date de naissance n'est pas valide.");
                $this->f->displayMessage($message_class, $message);
            }
            if ($error_ine) {
                $error = true;
                $message_class = "error";
                $message = __("Cet INE n'est pas valide.");
                $this->f->displayMessage($message_class, $message);
            }
        } else {
            $recherche_par = "etat_civil";
        }

        /**
         * Formulaire soumis et valide
         */
        if (isset($_POST['submited']) and $error == false) {
            if (isset($_POST["submited"])
                && $error_empty === false
                && $error_date === false
                && $error_ine === false) {

                //
                printf('<div class="results"><h2>%s</h2>', _("Résultats de recherche"));
                //
                $do_not_display_results = false;
                $results = array();
                //
                $inst_reu = $this->f->get_inst__reu();
                if ($inst_reu == null) {
                    $this->f->displayMessage(
                        "error",
                        __("Problème de configuration de la connexion au REU. Contactez votre administrateur.")
                    );
                } else {
                    if ($recherche_par == "ine") {
                        $ret = $inst_reu->handle_electeur(array(
                            "mode" => "search_by_ine",
                            "ine" => intval(str_replace(" ", "", $ine)),
                        ));
                        if ($ret["code"] == 401) {
                            $do_not_display_results = true;
                            if ($inst_reu->is_connexion_standard_valid() === false) {
                                $this->f->displayMessage(
                                    "error",
                                    sprintf(
                                        '%s : %s',
                                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                                        sprintf(
                                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                            __("cliquez ici pour vérifier")
                                        )
                                    )
                                );
                            } else {
                                $this->f->displayMessage(
                                    "error",
                                    sprintf(__("Une erreur s'est produite. Contactez votre administrateur."))
                                );
                            }
                        } elseif ($ret["code"] == 204) {
                            $do_not_display_results = true;
                            $this->f->displayMessage(
                                "error",
                                sprintf('Aucun résultat pour l\'INE : %s.', $ine)
                            );
                        } elseif ($ret["code"] == 200) {
                            $results[] = gavoe($ret, array("result", ));
                        } else {
                            $do_not_display_results = true;
                            $this->f->displayMessage(
                                "error",
                                sprintf(__("Une erreur s'est produite. Contactez votre administrateur."))
                            );
                        }
                    } elseif ($recherche_par == "etat_civil") {
                        $ret = $inst_reu->handle_electeur(array(
                            "mode" => "search_by_etat_civil",
                            "datas" => array(
                                "nom" => $this->f->get_submitted_post_value("nom"),
                                "prenom" => $this->f->get_submitted_post_value("prenom"),
                                "date_naissance" => $this->f->get_submitted_post_value("datenaissance"),
                                "sexe" => $this->f->get_submitted_post_value("sexe"),
                            )
                        ));
                        if ($ret["code"] == 401) {
                            $do_not_display_results = true;
                            if ($inst_reu->is_connexion_standard_valid() === false) {
                                $this->f->displayMessage(
                                    "error",
                                    sprintf(
                                        '%s : %s',
                                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                                        sprintf(
                                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                            __("cliquez ici pour vérifier")
                                        )
                                    )
                                );
                            } else {
                                $this->f->displayMessage(
                                    "error",
                                    sprintf(__("Une erreur s'est produite. Contactez votre administrateur."))
                                );
                            }
                        } elseif ($ret["code"] == 200) {
                            $presence_homonyme = gavoe($ret, array("result", "presenceHomonyme", ));
                            $electeur_results = gavoe($ret, array("result", "electeurRechercheViews", ));
                            if ($electeur_results != "") {
                                $results = $electeur_results;
                            }
                        } elseif ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                            $do_not_display_results = true;
                            $this->f->displayMessage(
                                "error",
                                $ret["result"]["message"]
                            );
                        } else {
                            $do_not_display_results = true;
                            $this->f->displayMessage(
                                "error",
                                sprintf(__("Une erreur s'est produite. Contactez votre administrateur."))
                            );
                        }
                    }
                    //
                    if ($do_not_display_results === false) {
                        /**
                         *
                         */
                        $this->f->layout->display__form_container__begin(array(
                            "action" => sprintf(
                                '%s&snippet=search_ine&origine=%s&form=%s',
                                OM_ROUTE_FORM,
                                $origine,
                                $form
                            ),
                            "name" => "snippet-search_ine-results-form",
                            "id" => "snippet-search_ine-form",
                        ));
                        $table_content = sprintf(
                            '<table class="table table-bordered table-sm">
                                <tr>
                                    <th></th>
                                    <th>ine</th>
                                    <th>nom patronymique</th>
                                    <th>prénoms</th>
                                    <th>sexe</th>
                                    <th>date de naissance</th>
                                    <th>commune de naissance</th>
                                    <th>pays de naissance</th>
                                </tr>'
                        );
                        foreach ($results as $key => $value) {
                            $table_content .= sprintf(
                                '<tr>
                                    <td>%s</td>
                                    <td>%s</td>
                                    <td>%s</td>
                                    <td>%s</td>
                                    <td>%s</td>
                                    <td>%s</td>
                                    <td>%s</td>
                                    <td>%s</td>
                                </tr>',
                                sprintf(
                                    '<input type="radio" id="choix_ine" name="choix_ine" value="%s">',
                                    gavoe($value, array("numeroElecteur", ))
                                ),
                                gavoe($value, array("numeroElecteur", )),
                                gavoe($value, array("etatCivil", "nomNaissance", )),
                                gavoe($value, array("etatCivil", "prenoms", )),
                                gavoe($value, array("etatCivil", "sexe", "code", )),
                                gavoe($value, array("etatCivil", "dateNaissance", )),
                                gavoe($value, array("etatCivil", "communeNaissance", "libelle", )),
                                gavoe($value, array("etatCivil", "paysNaissance", "libelle", ))
                            );
                        }
                        $table_content .= sprintf('</table>');
                        echo $table_content;

                        //
                        echo "<input type=\"hidden\" value=\"1\" name=\"results-submited\" />\n";
                        $this->f->layout->display__form_input_submit(array(
                            "name" => "submit",
                            "value" => __("Valider"),
                        ));
                        $this->f->displayLinkJsCloseWindow();
                        $this->f->layout->display__form_container__end();
                    }
                }
                printf('</div>');
            }
        }

        /**
         * Formulaire non soumis ou non valide
         */
        //if (!isset($_POST["submited"]) or $error == true) {
            /**
             * Paramétrage du formulaire
             */
            //
            $validation = 0;
            $maj = 0;
            $champs = array("recherche_par", "nom", "prenom", "datenaissance", "sexe", "ine");
            //
            $inst_form = $this->f->get_inst__om_formulaire(array(
                "validation" => $validation,
                "maj" => $maj,
                "champs" => $champs,
            ));
            $inst_form->setLib("recherche_par", __("Recherche par"));
            $inst_form->setType("recherche_par", "select");
            $inst_form->setTaille("recherche_par", 10);
            $inst_form->setMax("recherche_par", 10);
            $inst_form->setVal("recherche_par", $recherche_par);
            $inst_form->setSelect("recherche_par", array(
                0 => array("ine", "etat_civil", ),
                1 => array("INE", "État civil", ),
            ));
            //
            $inst_form->setLib("nom", __("Nom patronymique"));
            $inst_form->setType("nom", "text");
            $inst_form->setTaille("nom", 40);
            $inst_form->setMax("nom", 60);
            $inst_form->setVal("nom", $nom);
            $inst_form->setOnchange("nom", "this.value=this.value.toUpperCase()");
            //
            $inst_form->setLib("prenom", __("prenom"));
            $inst_form->setType("prenom", "text");
            $inst_form->setTaille("prenom", 40);
            $inst_form->setMax("prenom", 60);
            $inst_form->setVal("prenom", $prenom);
            $inst_form->setOnchange("prenom", "this.value=this.value.toUpperCase()");
            //
            $inst_form->setLib("sexe", __("Sexe"));
            $inst_form->setType("sexe", "select");
            $inst_form->setTaille("sexe", 10);
            $inst_form->setMax("sexe", 10);
            $inst_form->setVal("sexe", $sexe);
            $inst_form->setSelect("sexe", array(
                0 => array("", "M", "F", ),
                1 => array("", "M - Masculin", "F - Féminin", ),
            ));
            //
            $inst_form->setLib("datenaissance", __("Date de Naissance"));
            $inst_form->setType("datenaissance", "date");
            $inst_form->setTaille("datenaissance", 10);
            $inst_form->setMax("datenaissance", 10);
            $inst_form->setVal("datenaissance", $datenaissance);
            $inst_form->setOnchange("datenaissance", "fdate(this)");
            //
            $inst_form->setLib("ine", __("INE"));
            $inst_form->setType("ine", "text");
            $inst_form->setTaille("ine", 40);
            $inst_form->setMax("ine", 60);
            $inst_form->setVal("ine", $ine);
            $inst_form->setOnchange("ine", 'VerifNum(this)');
            //
            $inst_form->setBloc("recherche_par", "D", "", "search_ine-form");
            $inst_form->setBloc("recherche_par", "DF", "", "first-fix-width");
            $inst_form->setBloc("nom", "D", "", "bloc-etat-civil");
            $inst_form->setBloc("nom", "DF", "", "first-fix-width");
            $inst_form->setBloc("prenom", "DF", "", "first-fix-width field-prenom");
            $inst_form->setBloc("datenaissance", "DF", "", "first-fix-width");
            $inst_form->setBloc("sexe", "DF", "", "first-fix-width field-sexe");
            $inst_form->setBloc("sexe", "F");
            $inst_form->setBloc("ine", "D", "", "bloc-ine");
            $inst_form->setBloc("ine", "DF", "", "first-fix-width");
            $inst_form->setBloc("ine", "F");
            $inst_form->setBloc("ine", "F");

            /**
             *
             */
            $this->f->layout->display__form_container__begin(array(
                "action" => sprintf(
                    '%s&snippet=search_ine&origine=%s&form=%s',
                    OM_ROUTE_FORM,
                    $origine,
                    $form
                ),
                "name" => "snippet-search_ine-form",
                "id" => "snippet-search_ine-form",
            ));
            $inst_form->entete();
            $inst_form->afficher($champs, $validation, false, false);
            $inst_form->enpied();
            //
            echo "<input type=\"hidden\" value=\"1\" name=\"submited\" />\n";
            $this->f->layout->display__form_input_submit(array(
                "name" => "submit",
                "value" => __("Rechercher"),
                "class" => "om-search-button",
            ));
            $this->f->displayLinkJsCloseWindow();
            $this->f->layout->display__form_container__end();
        //}

        /**
         *
         */
        //
        $this->f->displayEndContent();
    }

}
