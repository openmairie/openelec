<?php
/**
 * XXX
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/electeur.class.php";

/**
 * XXX
 */
class reu_sync_l_e_oel extends electeur {

    protected $_absolute_class_name = null;

    function init_class_actions() {

        // ACTION - 003 - consulter
        //
        $this->class_actions[3] = array(
            "identifier" => "consulter",
            "permission_suffix" => "consulter",
            "view" => "view_fiche_electeur_reu",
            "condition" => array(
                "exists",
            ),
        );

        // ACTION - 501 - Délier
        //
        $this->class_actions[501] = array(
            "identifier" => "delier-electeur",
            "permission_suffix" => "delier_electeur",
            "view" => "formulaire",
            "method" => "unbind_electeur",
            "portlet" => array(
               "type" => "action-direct",
               "libelle" => __("Délier"),
               "class" => "",
               "order" => 10,
            ),
            "condition" => array(
                "is_bind_to_reu",
                "has_treatment_valid_not_done",
            ),
        );

        // ACTION - 502 - Lier
        //
        $this->class_actions[502] = array(
            "identifier" => "lier-electeur",
            "permission_suffix" => "lier_electeur",
            "view" => "view_bind_electeur",
            "portlet" => array(
               "type" => "action-self",
               "libelle" => __("Lier"),
               "class" => "",
               "order" => 10,
            ),
            "condition" => array(
                "is_unbind_to_reu",
                "has_treatment_valid_not_done",
            ),
        );
    }

    function is_bind_to_reu() {
        $ine = $this->getVal('ine');
        if ($ine === null || $ine === '') {
            return false;
        }
        return true;
    }

    function is_unbind_to_reu() {
        return !$this->is_bind_to_reu();
    }

    function has_treatment_valid_not_done() {
        if ($this->f->getParameter("reu_sync_l_e_valid") != "done") {
            return true;
        }
        return false;
    }

    function display_fiche_electeur_to_compare() {
        $context_reu = false;
        if ($this->f->get_submitted_get_value('numero_d_electeur') !== null
            && $this->f->get_submitted_get_value('numero_d_electeur') !== '') {
            //
            $context_reu = true;
        }
        //
        $template_formulaire = "
            <div id=\"ficheelecteur\">\n
                <div class=\"container_ficheelecteur_oel\">\n
                    %s
                </div>\n
                <div class=\"container_ficheelecteur_reu\">\n
                    %s
                </div>\n
                <div id=\"reu_sync_info\">\n
                    %s
                </div>
            </div>\n
        ";

        //
        $fiche_electeur_oel = '';
        if ($context_reu !== true) {
            $infos_oel = array(
                "civilite" => $this->getVal('civilite'),
                "nom" => $this->getVal("nom"),
                "prenom" => $this->getVal("prenom"),
                "nom_usage" => $this->getVal("nom_usage"),
                "date_naissance" => $this->getVal("date_naissance"),
                "libelle_lieu_de_naissance" => $this->getVal("libelle_lieu_de_naissance"),
                "libelle_departement_naissance" => $this->getVal("libelle_departement_naissance"),
                "code_departement_naissance" => $this->getVal("code_departement_naissance"),
                "sexe" => $this->getVal("sexe"),
                "code_nationalite" => $this->getVal("code_nationalite"),
            );
            $fiche_electeur_oel = $this->get_display_fiche_electeur($infos_oel, true);
        }

        //
        $fiche_electeur_reu = '';
        $reu_sync_info = '';
        if (($this->getVal('ine') !== null
                && $this->getVal('ine') !== '')
            || $context_reu === true) {
            //
            $fiche_electeur_reu = $this->get_display_fiche_electeur_reu($context_reu);

            //
            $inst_liste = $this->f->get_inst__om_dbform(array(
                "obj" => "liste",
                "idx" => $this->getVal('liste'),
            ));
            //
            if ($context_reu !== true) {
                $reu_sync_info = sprintf("
                    <p>%s : %s</p>
                    <p>%s : %s</p>",
                    __("Liste"),
                    $inst_liste->getVal('libelle_liste'),
                    __("Condition de la synchronisation"),
                    $this->getVal('reu_sync_info')
                );
            }
        }

        //
        printf(
            $template_formulaire,
            $fiche_electeur_oel,
            $fiche_electeur_reu,
            $reu_sync_info
        );
    }

    function view_fiche_electeur_reu() {
        $this->checkAccessibility();
        $this->compose_portlet_actions();
        if (!empty($this->user_actions)) {
            $inst__om_formulaire = $this->f->get_inst__om_formulaire();
            $inst__om_formulaire->afficher_portlet(
                $this->getParameter("idx"),
                $this->user_actions
            );
        }
        $this->retour(
            $this->getParameter("premier"),
            $this->getParameter("recherche"),
            $this->getParameter("tricol")
        );

        $this->display_fiche_electeur_to_compare();

        $this->retour(
            $this->getParameter("premier"),
            $this->getParameter("recherche"),
            $this->getParameter("tricol")
        );
    }

    function getDataSubmit() {
        //
        $datasubmit = electeur_gen::getDataSubmit();
        if ($this->getParameter('maj') == 502) {
            $datasubmit .= "&numero_d_electeur=".$this->f->get_submitted_get_value('numero_d_electeur');
        }
        //
        return $datasubmit;
    }

    function view_bind_electeur() {
        $this->checkAccessibility();
        $context_reu = false;
        if ($this->f->get_submitted_get_value('numero_d_electeur') !== null
            && $this->f->get_submitted_get_value('numero_d_electeur') !== '') {
            //
            $context_reu = true;
        }
        //
        $error = false;
        //
        $submit = $this->f->get_submitted_post_value('submit');
        if ($context_reu === false
            && $this->f->get_submitted_post_value('submit') !== null
            && ($this->f->get_submitted_post_value('ine') !== null
                && $this->f->get_submitted_post_value('ine') !== '')) {
            //
            $valF = array();
            $valF['ine'] = $this->f->get_submitted_post_value('ine');
            $valF['reu_sync_info'] = "2 - RATTACHEMENT MANUEL";
            $res = $this->f->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, $this->table),
                $valF,
                DB_AUTOQUERY_UPDATE,
                $this->getCle($this->getVal($this->clePrimaire))
            );
            $this->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $this->table)."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\");",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res, true) !== false) {
                $error = true;
                $this->f->displayMessage(
                    "error",
                    __("L'électeur n'a pas pu être lié au REU.")
                );
            };
            if ($error === false) {
                //
                $this->msg = __("L'électeur a été correctement lié au REU.");
                $this->redirect_to_back_link('formulaire');
                return;
            }
        }
        //
        if ($context_reu === true
            && $this->f->get_submitted_post_value('submit') !== null
            && ($this->f->get_submitted_post_value('electeur_oel') !== null
                && $this->f->get_submitted_post_value('electeur_oel') !== '')) {
            //
            $valF = array();
            $valF['ine'] = preg_replace('~\D~', '', $this->f->get_submitted_get_value('numero_d_electeur'));
            $valF['reu_sync_info'] = "2 - RATTACHEMENT MANUEL";
            $res = $this->f->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, $this->table),
                $valF,
                DB_AUTOQUERY_UPDATE,
                $this->getCle($this->f->get_submitted_post_value('electeur_oel'))
            );
            $this->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $this->table)."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\");",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res, true) !== false) {
                $error = true;
                $this->f->displayMessage(
                    "error",
                    __("L'électeur n'a pas pu être lié au REU.")
                );
            };
            if ($error === false) {
                //
                $this->msg = __("L'électeur a été correctement lié au REU.");
                $this->redirect_to_back_link('formulaire');
                return;
            }
        }

        $this->f->layout->display__form_container__begin(array(
            "action" => $this->getDataSubmit(),
            "name" => "f1",
            "id" => $this->get_absolute_class_name()."_bind_electeur",
        ));

        //Compatibilite anterieure - On decremente la variable validation
        $this->setParameter("validation", $this->getParameter("validation") - 1);
        $maj = $this->getParameter("maj");
        $champs = array('electeur_oel', 'ine');

        // Instanciation de l'objet formulaire
        $form = $this->f->get_inst__om_formulaire(array(
            "validation" => $this->getParameter("validation"),
            "maj" => $maj,
            "champs" => $champs,
        ));

        $form->setParameter("obj", $this->get_absolute_class_name());
        $form->setParameter("idx", $this->getParameter("idx"));
        $form->setParameter("maj", $this->getParameter("maj"));
        $form->setParameter("form_type", "form");

        $form->setType('electeur_oel', 'hidden');
        $form->setType('ine', 'autocomplete');
        if ($context_reu === true) {
            $form->setType('electeur_oel', 'autocomplete');
            $form->setType('ine', 'hidden');
        }
        $form->setLib('electeur_oel', __("Électeur d'openElec"));
        $form->setLib('ine', __("Électeur du REU"));
        if ($context_reu !== true) {
            $form->setSelect('ine', $this->get_widget_config__ine__autocomplete());
        }
        if ($context_reu === true) {
            $form->setSelect('electeur_oel', $this->get_widget_config__electeur_oel__autocomplete());
        }
        $form->setTaille('electeur_oel', 100);
        $form->setMax('electeur_oel', 250);
        $form->setVal("electeur_oel", '');
        $form->setTaille('ine', 100);
        $form->setMax('ine', 250);
        $form->setVal("ine", '');

        // Affichage du bouton retour et du bouton
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "top",
        ));
        $this->bouton($maj);
        $this->retour(
            $this->getParameter("premier"),
            $this->getParameter("recherche"),
            $this->getParameter("tricol")
        );
        $this->f->layout->display__form_controls_container__end();
        // Ouverture du conteneur de formulaire
        $form->entete();
        $this->display_fiche_electeur_to_compare();
        $form->afficher($champs, $this->getParameter("validation"), false, false);
        $form->enpied();
        // Affichage du bouton et du bouton retour
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->bouton($maj);
        $this->retour(
            $this->getParameter("premier"),
            $this->getParameter("recherche"),
            $this->getParameter("tricol")
        );
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }

    function get_widget_config__electeur_oel__autocomplete() {
        //
        return array(
            "obj" => "electeur",
            "table" => "electeur",
            "identifiant" => "electeur.id",
            "criteres" => array(
                "electeur.nom" => __("Nom"),
                "electeur.nom_usage" => __("Nom d'usage"),
                "electeur.prenom" => __("Prenom"),
                "electeur.id" => __("Identifiant"),
                "to_char(electeur.date_naissance,'DD/MM/YYYY')" => __("Date de naissance")
            ),
            "libelle" => array(
                "electeur.id",
                "electeur.nom",
                "electeur.nom_usage",
                "electeur.prenom",
                "to_char(electeur.date_naissance,'DD/MM/YYYY')"
            ),
            "jointures" => array(),
            "droit_ajout" => false,
            "where" => sprintf(
                'ine IS NULL AND liste = (SELECT liste FROM %1$sliste WHERE liste_insee = \'%2$s\') AND electeur.om_collectivite=%3$s',
                DB_PREFIXE,
                preg_replace('~\d~', '', $this->f->get_submitted_get_value('numero_d_electeur')),
                intval($_SESSION["collectivite"])
            ),
        );
    }

    function get_widget_config__ine__autocomplete() {
        //
        $inst_reu = $this->f->get_inst__reu();
        // Mapping entre les listes d'openElec et du REU
        $listes_map = array(
            "01" => "LP",
            "02" => "LCE",
            "03" => "LCM",
        );
        $liste_electeur_reu = $listes_map[$this->getVal('liste')];
        return array(
            "obj" => "reu_sync_listes",
            "table" => "reu_sync_listes",
            "identifiant" => "reu_sync_listes.numero_d_electeur",
            "criteres" => array(
                "reu_sync_listes.nom" => __("Nom"),
                "reu_sync_listes.nom_d_usage" => __("Nom d'usage"),
                "reu_sync_listes.prenoms" => __("Prenom"),
                "reu_sync_listes.numero_d_electeur" => __("Numéro d'électeur"),
                "reu_sync_listes.date_de_naissance" => __("Date de naissance"),
            ),
            "libelle" => array(
                "reu_sync_listes.numero_d_electeur",
                "reu_sync_listes.nom",
                "reu_sync_listes.nom_d_usage",
                "reu_sync_listes.prenoms",
                "reu_sync_listes.date_de_naissance"
            ),
            "jointures" => array(),
            "droit_ajout" => false,
            "where" => sprintf(
                "reu_sync_listes.code_ugle='%s' AND numero_d_electeur::integer NOT IN (SELECT ine FROM %selecteur WHERE liste='%s' AND ine IS NOT NULL) AND reu_sync_listes.code_type_liste = '%s'",
                $inst_reu->get_ugle(),
                DB_PREFIXE,
                $this->getVal("liste"),
                $liste_electeur_reu
            ),
            "group_by" => array("reu_sync_listes.numero_d_electeur", "reu_sync_listes.nom", "reu_sync_listes.nom_d_usage", "reu_sync_listes.prenoms", "reu_sync_listes.date_de_naissance"),
        );
    }

    /**
     * TREATMENT - unbind_electeur.
     *
     * @return boolean
     */
    function unbind_electeur() {
        $this->begin_treatment(__METHOD__);
        $valF = array();
        $valF['ine'] = null;
        $valF['reu_sync_info'] = null;
        $res = $this->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, $this->table),
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($this->getVal($this->clePrimaire))
        );
        $this->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $this->table)."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($this->getVal($this->clePrimaire))."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true) !== false) {
            $this->correct = false;
            $this->addToMessage(__("L'électeur n'a pas pu être délier du REU."));
            return $this->end_treatment(__METHOD__, false);
        };
        $this->addToMessage(__("L'électeur a été correctement délier du REU."));
        return $this->end_treatment(__METHOD__, true);
    }

}
