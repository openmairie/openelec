<?php
/**
 * Ce script définit la classe 'decoupageInitBureauforceTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'decoupageInitBureauforceTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class decoupageInitBureauforceTraitement extends traitement {
    
    var $fichier = "decoupage_init_bureauforce";
    
    function getDescription() {
        //
        return __("Tous les electeurs etant marques avec un bureau force et ".
                 "etant affectes au bureau correspondant a leur adresse dans ".
                 "le découpage ne seront plus marques en bureau force pour ".
                 "qu'un redecoupage automatique soit possible. Ce traitement ".
                 "se fait sur toutes les listes.");
    }
    
    function getValidButtonValue() {
        //
        return __("Initialiser les electeurs en bureau force");
    }
    
    function treatment () {
        //
        $this->LogToFile("start decoupage_init_bureauforce");
        //
        $sql_decoupage_init_bureauforce_electeur = sprintf(
            'UPDATE %1$selecteur SET bureauforce = \'Non\'
            WHERE id in (
                SELECT electeur.id FROM %1$selecteur
                JOIN %1$sdecoupage ON decoupage.code_voie = electeur.code_voie
                    AND CASE numero_habitation %% 2
                        WHEN 0
                        THEN decoupage.premier_pair <= numero_habitation AND numero_habitation <= decoupage.dernier_pair
                        WHEN 1
                        THEN decoupage.premier_impair <= numero_habitation AND numero_habitation <= decoupage.dernier_impair
                        END
                WHERE
                electeur.om_collectivite=%2$s AND electeur.bureau = decoupage.bureau
            )
            AND electeur.bureauforce = \'Oui\'',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->query($sql_decoupage_init_bureauforce_electeur);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql_decoupage_init_bureauforce_electeur."\");",
            VERBOSE_MODE
        );
        //
        if ($this->f->isDatabaseError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(__("Contactez votre administrateur."));
        } else {
            //
            $message = $this->f->db->affectedRows()." ".__("electeur(s) modifie(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }

        //
        $sql_decoupage_init_bureauforce_mouvement = sprintf(
            'UPDATE %1$smouvement SET bureauforce = \'Non\'
            WHERE id in (
                SELECT mouvement.id FROM %1$smouvement
                JOIN %1$sdecoupage ON decoupage.code_voie = mouvement.code_voie
                    AND CASE numero_habitation %% 2
                        WHEN 0
                        THEN decoupage.premier_pair <= numero_habitation AND numero_habitation <= decoupage.dernier_pair
                        WHEN 1
                        THEN decoupage.premier_impair <= numero_habitation AND numero_habitation <= decoupage.dernier_impair
                        END
                WHERE
                mouvement.om_collectivite=%2$s AND mouvement.bureau = decoupage.bureau
            )
            AND mouvement.bureauforce = \'Oui\' AND mouvement.etat = \'actif\'',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->f->db->query($sql_decoupage_init_bureauforce_mouvement);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql_decoupage_init_bureauforce_mouvement."\");",
            VERBOSE_MODE
        );
        //
        if ($this->f->isDatabaseError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(__("Contactez votre administrateur."));
        } else {
            //
            $message = $this->f->db->affectedRows()." ".__("mouvement(s) modifie(s)");
            $this->LogToFile($message);
            $this->addToMessage($message);
        }
        //
        $this->LogToFile("end decoupage_init_bureauforce");
    }
}
