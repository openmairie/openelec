<?php
/**
 * Ce script définit la classe 'pdfetiquette'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 * Inclusion de la classe FPDF qui permet de generer des fichiers PDF.
 */
require_once "fpdf.php";

/**
 * Définition de la classe 'PDF' (fpdf).
 */
class PDF extends FPDF {

    /**
     *
     */
    function Table_position($sql, $dnu1, $param, $champs, $texte, $champs_compteur, $img) {
        $this->init_om_application();
        //
        global $police;

        //
        $_Margin_Left=$param[0];
        $_Margin_Top=$param[1];
        $_X_Space=$param[2];
        $_Y_Space=$param[3];
        $_X_Number=$param[4];
        $_Y_Number=$param[5];
        $_Width=$param[6];
        $_Height=$param[7];
        $_Char_Size=$param[8];
        $_Line_Height=$param[9];
        $_cptx=$param[10];
        $_cpty=$param[11];
        $_cadre=$param[13];
        $_cadrezone=$param[14];

        //
        $default_size = (isset($param[12]) ? $param[12] : 10);

        //
        $res = $this->f->db->query($sql);
        //
        if (database::isError($res)) {
            die($res->getDebugInfo()." ".$res->getMessage());
        }

        //
        $nbChamp=0;
        $nbtexte=0;
        $nbimg=0;
        $_PosX =0;
        $_PosY =0;
        $info=$res->tableInfo();
        $nbChamp = count($info);
        $nbtexte=count($texte);
        $nbimg=count($img);
        $compteur = 0;
        $tmpchamps="";
        $nb_txt=0;
        if (!isset($champs_compteur[0])) $champs_compteur[0]=0; //php4

        //
        $nbrow=0;
        $nbrow=$res->numrows();

        //
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {

            //
            $compteur++;
            $k=0;
            $j=0;

            //////
            // CADRE DE L'ETIQUETTE
            //////
            //
            if ($_cptx == 0) {
                //
                $_PosX = $_Margin_Left;
                $_PosY = $_Margin_Top + ($_cpty * ($_Height + $_Y_Space)) + $_cpty;
            } else {
                //
                $_PosX = $_Margin_Left + ($_cptx * ($_Width + $_X_Space));
                $_PosY = $_Margin_Top + ($_cpty * ($_Height + $_Y_Space)) + $_cpty;
            }
            //
            $this->SetXY($_PosX, $_PosY);
            $this->MultiCell($_Width, $_Height, "", $_cadrezone);
            //////

            //////
            // COMPTEUR
            //////
            //
            if ($champs_compteur[0] == 1) {
                $archx = $_PosX;
                $archy = $_PosY;
                $this->SetXY($archx + $champs_compteur[1], $archy + $champs_compteur[2]);
                //
                $style = (isset($champs_compteur[4]) && $champs_compteur[4] == 1 ? "B" : "");
                $size = (isset($champs_compteur[5]) && $champs_compteur[5] > 0 ? $champs_compteur[5] : $default_size);
                $this->SetFont($police, $style, $size);
                //
                $align = (isset($champs_compteur[7]) ? $champs_compteur[7] : "L");
                $offset = (isset($champs_compteur[6]) ? $champs_compteur[6] : 0);
                //
                $this->MultiCell($champs_compteur[3], $_Line_Height, iconv(HTTPCHARSET,"CP1252",$compteur+$offset), $_cadre, $align);
                $this->SetXY($archx, $archy);
            }
            //////

            //////
            // 
            //////
            //
            for ($j = 0; $j < $nbChamp; $j++) {
                //
                $archx = $_PosX;
                $archy = $_PosY;
                //
                $this->SetXY($archx + $champs[$info[$j]['name']][2][0], $archy + $champs[$info[$j]['name']][2][1]);
                //
                $style = (isset($champs[$info[$j]['name']][2][3]) && $champs[$info[$j]['name']][2][3] == 1 ? "B" : "");
                $size = (isset($champs[$info[$j]['name']][2][4]) && $champs[$info[$j]['name']][2][4] > 0 ? $champs[$info[$j]['name']][2][4] : $default_size);
                $this->SetFont($police, $style, $size);
                //
                $align = (isset($champs[$info[$j]['name']][2][5]) ? $champs[$info[$j]['name']][2][5] : "L");
                //
                if ($champs[$info[$j]['name']][3] == 1) {
                    $champs_num=number_format($row[$info[$j]['name']],0);
                    $this->MultiCell($champs[$info[$j]['name']][2][2], $_Line_Height,iconv(HTTPCHARSET,"CP1252",$champs[$info[$j]['name']][0].$champs_num.$champs[$info[$j]['name']][1]), $_cadre, $align);
                } else {
                    $this->MultiCell($champs[$info[$j]['name']][2][2], $_Line_Height,iconv(HTTPCHARSET,"CP1252",$champs[$info[$j]['name']][0].$row[$info[$j]['name']].$champs[$info[$j]['name']][1]), $_cadre, $align);
                }
                $this->SetXY($archx,$archy);
            }
            //////

            //////
            // 
            //////
            //
            for($i=0;$i<$nbimg;$i++){
               //
               $archx= $_PosX;
               $archy= $_PosY;
               $this->SetXY($archx+$img[$i][1],$archy+$img[$i][2]);
               //
               $this->Image($img[$i][0],$archx+$img[$i][1],$archy+$img[$i][2],$img[$i][3],$img[$i][4],$img[$i][5]);
               $this->SetXY($archx,$archy);
            }
            //////

            //////
            // 
            //////
            //
            for($k=0;$k<$nbtexte;$k++){
                $archx= $_PosX;
                $archy= $_PosY;
                $champ_bold='';
                $champ_size=$param[12];
                $this->SetXY($archx+$texte[$k][1],$archy+$texte[$k][2]);
                //bold et size
                if ($texte[$k][4]==1){$champ_bold='B';}
                if ($texte[$k][5]>0){$champ_size=$texte[$k][5];}
                $this->SetFont($police,$champ_bold,$champ_size);
                //
                $this->MultiCell($texte[$k][3],$_Line_Height,iconv(HTTPCHARSET,"CP1252",$texte[$k][0]),$_cadre, "L");
                $this->SetXY($archx,$archy);
            }
            //////

            //////
            // 
            //////
            //
            $_cptx++;
            if ($_cptx == $_X_Number) {
                $_cptx = 0;
                $_cpty++;
                if ($_cpty == $_Y_Number) {
                    $_cptx = 0;
                    $_cpty = 0;
                    if ($compteur < $nbrow) {
                        $this->AddPage();
                    }
                }
            }
            //////

        }

        // Si aucun enregistrement
        if ($nbrow == 0) {
            $this->SetTextColor(245,34,108);
            $this->SetDrawColor(245,34,108);
            $_PosX =$_Margin_Left;
            $_PosY =$_Margin_Top+($_cpty*($_Height+$_Y_Space))+$_cpty;
            $this->SetXY($_PosX+10, $_PosY+10);
            $this->MultiCell(100,10,iconv(HTTPCHARSET,"CP1252",__('Aucun enregistrement selectionne')),1);
        }

        //
        $res->free();

    }

    /**
     * Initialisation de la classe 'application'.
     *
     * Cette méthode permet de vérifier que l'attribut f de la classe contient
     * bien la ressource utils du framework et si ce n'est pas le cas de la
     * récupérer.
     *
     * @return boolean
     */
    function init_om_application() {
        //
        if (isset($this->f) && $this->f != null) {
            return true;
        }
        //
        if (isset($GLOBALS["f"])) {
            $this->f = $GLOBALS["f"];
            return true;
        }
        //
        return false;
    }
}
