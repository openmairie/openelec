<?php
/**
 * Ce script définit la classe 'module_procuration'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_procuration' (module).
 */
class module_procuration extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_procuration",
            "title" => __("Traitement")." -> ".__("Module 'Procurations'"),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_procuration-onglet_main",
                    "href" => "../app/index.php?module=module_procuration&view=main",
                    "title" => __("Procurations"),
                    "view" => "main",
                ),
                array(
                    "type" => "tab",
                    "right" => "module_procuration-onglet_epuration",
                    "href" => "../app/index.php?module=module_procuration&view=epuration",
                    "title" => __("Epuration"),
                    "view" => "epuration",
                ),
                array(
                    "type" => "edition_pdf",
                    "right" => "module_procuration-edition_pdf__recapitulatif_mention",
                    "view" => "edition_pdf__recapitulatif_mention",
                ),
            ),
        );
    }

    /**
     *
     */
    function view__main() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_procuration");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");
        //
        ob_start();
        $this->view__bloc_edition_pdf_registre_procurations();
        $bloc_edition_pdf_registre_procurations = ob_get_clean();
        ob_start();
        $this->view__bloc_edition_pdf_listing_procurations_par_bureau();
        $bloc_edition_pdf_listing_procurations_par_bureau = ob_get_clean();
        ob_start();
        $this->view__bloc_edition_pdf_registre_procurations_par_bureau();
        $bloc_edition_pdf_registre_procurations_par_bureau = ob_get_clean();
        printf(
            '
            <div class="container-fluid" id="module-procuration-main-tab">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                Édition PDF "registre pour toute la commune"
                            </div>
                            <div class="card-body">
                                %1$s
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                Édition PDF "registre par bureau de vote"
                            </div>
                            <div class="card-body">
                                %2$s
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                Édition PDF "listing par bureau de vote"
                            </div>
                            <div class="card-body">
                                %3$s
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="visualClear"></div>
            ',
            $bloc_edition_pdf_registre_procurations,
            $bloc_edition_pdf_registre_procurations_par_bureau,
            $bloc_edition_pdf_listing_procurations_par_bureau
        );
    }

    /**
     *
     */
    function view__bloc_edition_pdf_registre_procurations() {
        echo "<p>Registre de toutes les procurations, dont le mandant (toujours inscrit dans la liste électorale de la commune) est rattaché à la liste sélectionnée, triées par ordre alphabétique de nom du mandant.</p>";
        echo "<table class=\"table table-sm\">\n";
        foreach ($this->f->get_all__listes(true) as $key => $value) {
            $edition = array(
                'img' => "<span class=\"om-icon om-icon-25 om-icon-fix listeregistretotproc-25\"><!-- --></span>",
                'titre' => "REGISTRE DES PROCURATIONS CLASSEES PAR MANDANT",
                'pdf' => "../app/index.php?module=form&obj=procuration&idx=0&action=303&liste=".$value["liste"],
                'id' => "edition-pdf-registre-des-procurations-classees-par-mandant-".$value["liste"],
                'right' => "procuration_edition_pdf__registre_procurations",
            );
            //
            echo "\t<tr class=\"editions\">";
            echo "<td class=\"titre\">".$value["libelle_liste"]."</td>";
            echo "<td class=\"icone\">";
            echo " <a class=\"lien\" id=\"".$edition["id"]."\" href=\"".$edition['pdf']."\"";
            if (!preg_match("#javascript#", $edition['pdf'])) {
                echo " target=\"_blank\"";
            }
            echo ">";
            echo "<span class=\"om-icon om-icon-25 om-icon-fix pdf-25\"><!-- --></span> ";
            // echo "<br />";
            echo __("Télécharger");
            echo "</a>";
            echo "</td>";
            echo "</tr>\n";
        }
        echo "</table>";
    }

    /**
     *
     */
    function get_all__scrutin_dates() {
        if (isset($this->_all__scrutin_dates) && $this->_all__scrutin_dates != null) {
            return $this->_all__scrutin_dates;
        }
        $scrutins = $this->f->get_all_results_from_db_query(sprintf(
            'SELECT reu_scrutin.id, reu_scrutin.type, reu_scrutin.date_tour1, reu_scrutin.date_tour2 FROM %1$sreu_scrutin WHERE reu_scrutin.om_collectivite=%2$s ORDER BY reu_scrutin.date_tour1',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        ));
        $scrutin_dates = array();
        foreach ($scrutins["result"] as $key => $value) {
            $scrutin_dates[] = array(
                "date" => $value["date_tour1"],
                "libelle" => $value["type"]." du ".$this->f->formatDate($value["date_tour1"])." (tour 1)",
            );
            if ($value["date_tour1"] != $value["date_tour2"]) {
                $scrutin_dates[] = array(
                    "date" => $value["date_tour2"],
                    "libelle" => $value["type"]." du ".$this->f->formatDate($value["date_tour2"])." (tour 2)",
                );
            }
        }
        $this->_all__scrutin_dates = $scrutin_dates;
        return $this->_all__scrutin_dates;
    }

    /**
     *
     */
    function view__bloc_edition_pdf_listing_procurations_par_bureau() {
        echo "<p>Listing de toutes les procurations acceptées (statut = procuration_confirmee), dont le mandant (toujours inscrit dans la liste électorale de la commune) est rattaché à la liste sélectionnée et au bureau de vote sélectionné, éventuellement filtrée sur la date de validité sélectionnée, triées par ordre alphabétique de nom du mandant.</p>";
        $params = array(
            "action" => "../app/index.php?module=form&obj=procuration&action=301&idx=0",
            "id" => "edition_pdf_listing_procurations_par_bureau",
            "fields" => array("liste", "bureau", "validite", ),
        );
        $this->display__edition_pdf_common_form($params);
    }

    /**
     *
     */
    function view__bloc_edition_pdf_registre_procurations_par_bureau() {
        echo "<p>Registre de toutes les procurations, dont le mandant (toujours inscrit dans la liste électorale de la commune) est rattaché à la liste sélectionnée et au bureau de vote sélectionné, triées par ordre alphabétique de nom du mandant.</p>";
        $params = array(
            "action" => "../app/index.php?module=form&obj=procuration&action=302&idx=0",
            "id" => "edition_pdf_registre_procurations_par_bureau",
            "fields" => array("liste", "bureau", ),
        );
        $this->display__edition_pdf_common_form($params);
    }

    /**
     *
     */
    function view__epuration() {
        // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_procuration");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        /**
         *
         */
        //
        $description = __("L'epuration signifit la suppression des enregistrements ".
                         "dans la base de donnees. Ce traitement doit etre realise ".
                         "avec precaution.");
        $this->f->displayDescription($description);

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        // Affichage du titre du paragraphe
        $subtitle = "-> ".__("Epuration des procurations");
        $this->f->displaySubTitle($subtitle);
        //
        require_once "../obj/traitement.election_epuration_procuration.class.php";
        $trt = new electionEpurationProcurationTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }

    /**
     * VIEW - view__edition_pdf__recapitulatif_mention.
     *
     * @return void
     */
    function view__edition_pdf__recapitulatif_mention() {
        //
        $params = array();
        if (isset($_GET['dateelection1'])) {
            $params["dateelection1"] = $_GET['dateelection1'];
        }
        if (isset($_GET['dateelection2'])) {
            $params["dateelection2"] = $_GET['dateelection2'];
        }
        //
        require_once "../obj/edition_pdf__recapitulatif_mention.class.php";
        $inst_edition_pdf = new edition_pdf__recapitulatif_mention();
        $pdf_edition = $inst_edition_pdf->compute_pdf__recapitulatif_mention($params);
        $inst_edition_pdf->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     *
     */
    function display__edition_pdf_common_form($params = array()) {
        //
        $this->f->layout->display__form_container__begin(array(
            "action" => $params["action"],
            "id" => $params["id"],
            "target" => "_blank",
        ));
        $champs = $params["fields"];
        $form = $this->f->get_inst__om_formulaire(array(
            "champs" => $champs,
        ));
        //
        if (in_array("liste", $champs) === true) {
            $form->setLib("liste", __("Liste"));
            $form->setType("liste", "select");
            $contenu = array(
                0 => array(),
                1 => array(),
            );
            foreach ($this->f->get_all__listes(true) as $key => $liste) {
                array_push($contenu[0], $liste["liste"]);
                array_push($contenu[1], $liste["libelle_liste"]);
            }
            $form->setSelect("liste", $contenu);
        }
        //
        if (in_array("bureau", $champs) === true) {
            $form->setLib("bureau", __("Bureau de vote"));
            $form->setType("bureau", "select");
            $contenu = array(
                0 => array(),
                1 => array(),
            );
            foreach ($this->f->get_all__bureau__by_my_collectivite() as $key => $bureau) {
                array_push($contenu[0], $bureau["code"]);
                array_push($contenu[1], $bureau["code"]." ".$bureau["libelle"]);
            }
            $form->setSelect("bureau", $contenu);
        }
        //
        if (in_array("validite", $champs) === true) {
            $form->setLib("validite", __("Validité"));
            $form->setType("validite", "select");
            $contenu = array(
                0 => array("", ),
                1 => array(__("Toutes les procurations"), ),
            );
            foreach ($this->get_all__scrutin_dates() as $key => $scrutin) {
                array_push($contenu[0], $scrutin["date"]);
                array_push($contenu[1], $scrutin["libelle"]);
            }
            $form->setSelect("validite", $contenu);
        }
        //
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "name" => $params["id"].".submit",
            "value" => __("Télécharger"),
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
    }
}
