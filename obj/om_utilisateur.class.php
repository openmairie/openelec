<?php
/**
 * Ce script définit la classe 'om_utilisateur'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."obj/om_utilisateur.class.php";

/**
 * Définition de la classe 'om_utilisateur' (om_dbform).
 */
class om_utilisateur extends om_utilisateur_core {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        // On récupère les actions génériques définies dans la méthode
        // d'initialisation de la classe parente
        parent::init_class_actions();
        // ACTION - 001 - modifier
        if (isset($this->class_actions) === true
            && array_key_exists(1, $this->class_actions) === true
            && is_array($this->class_actions[1]) === true) {
            //
            if (array_key_exists("condition", $this->class_actions)
                && is_array($this->class_actions[1]["condition"])) {
                //
                $this->class_actions[1]["condition"][] = "has_a_higher_profil";
            } elseif (array_key_exists("condition", $this->class_actions)
                && is_string($this->class_actions[1]["condition"])) {
                //
                $this->class_actions[1]["condition"] = array(
                    $this->class_actions[1]["condition"],
                    "has_a_higher_profil"
                );
            } elseif (array_key_exists("condition", $this->class_actions) === false) {
                //
                $this->class_actions[1]["condition"] = array(
                    "has_a_higher_profil"
                );
            }
        }
        // ACTION - 002 - supprimer
        if (isset($this->class_actions) === true
            && array_key_exists(2, $this->class_actions) === true
            && is_array($this->class_actions[2]) === true) {
            //
            if (array_key_exists("condition", $this->class_actions)
                && is_array($this->class_actions[2]["condition"])) {
                //
                $this->class_actions[2]["condition"][] = "has_a_higher_profil";
            } elseif (array_key_exists("condition", $this->class_actions)
                && is_string($this->class_actions[2]["condition"])) {
                //
                $this->class_actions[2]["condition"] = array(
                    $this->class_actions[2]["condition"],
                    "has_a_higher_profil"
                );
            } elseif (array_key_exists("condition", $this->class_actions) === false) {
                //
                $this->class_actions[2]["condition"] = array(
                    "has_a_higher_profil"
                );
            }
        }
    }

    /**
     * CONDITION - has_a_higher_profil.
     *
     * @return boolean
     */
    function has_a_higher_profil() {
        $hierarchie = 0;
        if (isset($this->f->user_infos) === true
            && is_array($this->f->user_infos) === true
            && array_key_exists("hierarchie", $this->f->user_infos) === true) {
            $hierarchie = intval($this->f->user_infos["hierarchie"]);
        }
        $inst__om_profil = $this->f->get_inst__om_dbform(array(
            "obj" => "om_profil",
            "idx" => $this->getVal("om_profil")
        ));
        if ($hierarchie >= intval($inst__om_profil->getVal("hierarchie"))) {
            return true;
        }
        return false;
    }

    /**
     * QUERY - sql_om_profil.
     *
     * Liste des profils dont la hiérarchie est inférieure ou égale à celle de
     * l'utilisateur connecté.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_om_profil() {
        $hierarchie = 0;
        if (isset($this->f->user_infos) === true
            && is_array($this->f->user_infos) === true
            && array_key_exists("hierarchie", $this->f->user_infos) === true) {
            $hierarchie = intval($this->f->user_infos["hierarchie"]);
        }
        return sprintf(
            'SELECT om_profil.om_profil, om_profil.libelle FROM %1$som_profil WHERE om_profil.hierarchie <= %2$s ORDER BY om_profil.libelle ASC',
            DB_PREFIXE,
            $hierarchie
        );
    }
}
