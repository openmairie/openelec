<?php
/**
 * Ce script définit la classe 'electeur_jury'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/electeur.class.php";

/**
 * Définition de la classe 'electeur_jury' (om_dbform).
 *
 * Surcharge de la classe 'electeur'. Cette classe permet la modification des
 * informations de l'électeur pour le jury d'assises.
 */
class electeur_jury extends electeur {

    /**
     *
     */
    protected $_absolute_class_name = "electeur_jury";

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        $this->class_actions = array();
        //
        $this->class_actions[53] = array(
            "identifier" => "edit-jury",
            "view" => "formulaire",
            "method" => "edit_infos_jury",
            "permission_suffix" => "modifier",
            "condition" => array(
                "is_collectivity_mono",
            ),
            "crud" => "update",
        );
        // Actions disponible sur la fiche électeur
        // pour éditer les infos sur le jury
        $this->class_actions[54] = array(
            "identifier" => "edit-jury",
            "view" => "view_edit_infos_jury",
            "permission_suffix" => "modifier",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        // Édition des étiquettes du jury
        $this->class_actions[306] = array(
            "identifier" => "edition-pdf-etiquette_jury",
            "view" => "view_edition_pdf__etiquette",
            "permission_suffix" => "edition_pdf__etiquette",
        );
        // Édition des étiquettes des suppléants du jury
        $this->class_actions[307] = array(
            "identifier" => "edition-pdf-etiquette_jury_suppleant",
            "view" => "view_edition_pdf__etiquette",
            "permission_suffix" => "edition_pdf__etiquette",
        );
    }
}


