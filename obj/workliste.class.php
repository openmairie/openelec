<?php
/**
 * Ce script définit la classe 'workListe'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_base.class.php";

/**
 * Définition de la classe 'workListe' (om_base).
 */
class workListe extends om_base {

    /**
     *
     */
    var $action = ".";

    /**
     *
     */
    var $listes = array ();

    /**
     * Constructeur.
     *
     * @return void
     */
    function __construct($action = ".") {
        //
        $this->init_om_application();
        //
        $this->action = $action;
        //
        $this->calculateAllListes();
    }

    function validateForm () {
        //
        if (isset ($_POST['changeliste_action_cancel'])) {
            $this->f->goToDashboard ();
        }
        //
        if (isset ($_POST['liste']) or isset ($_POST['changeliste_action_valid'])) {
            //
            if (!isset ($this->listes[$_POST['liste']])) {
                // message
                $message_class = "error";
                $message = __("Cette liste n'existe pas.");
                $this->f->addToMessage($message_class, $message);
            } else {
                //
                $_SESSION["liste"] = $_POST['liste'];
                $_SESSION["libelle_liste"] = $this->listes[$_POST['liste']]['libelle'];
                // message
                $message_class = "ok";
                $message = __("Vous travaillez maintenant sur la liste : ").$_SESSION["libelle_liste"].".";
                $this->f->addToMessage($message_class, $message);
            }
        }
    }

    function getListes () {
        return $this->listes;
    }

    function countListes() {
        return count($this->getListes());
    }

    function calculateAllListes () {
        //
        $sql = sprintf(
            'SELECT liste.liste, (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.officielle IS TRUE ORDER BY liste.liste',
            DB_PREFIXE
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        //
        while ($row =& $res->fetchRow (DB_FETCHMODE_ASSOC)) {
            $this->listes[$row['liste']] = array("code" => $row['liste'],
                                              "libelle" => $row['libelle_liste']);
        }
    }


    function showForm ($full = true) {
        //
        echo "\n<div id=\"changelisteform\" class=\"formulaire\">\n";

        //
        echo "<form method=\"post\" id=\"changeliste_form\" action=\"".$this->action."\">\n";

        //
        $tabindex = 1;

        //
        echo "\t<div class=\"field\">\n";
        //

        echo "\t\t<label for=\"liste\">";
        echo __("Selectionner ici la liste sur laquelle vous souhaitez travailler.");
        echo "</label>\n";
        //
        echo "\t\t<select name=\"liste\" tabindex=\"".$tabindex++."\" ";
        echo " class=\"champFormulaire\" >\n\t\t\t";
        foreach ($this->listes as $value) {
            echo "<option value=\"".$value['code']."\"";
            if ($_SESSION["liste"] == $value['code']) {
                echo " selected=\"selected\"";
            }
            echo ">".$value['libelle']."</option>";
        }
        echo "\n\t\t</select>\n";
        //
        echo "\t</div>\n";

        //
        echo "\t<div class=\"formControls\">\n";
        echo "\t\t<input name=\"changeliste.action.valid\" tabindex=\"".$tabindex++."\" value=\"".__("Valider")."\" type=\"submit\" class=\"boutonFormulaire context\" />\n";
        if ($full == true) {
            echo "<a class=\"retour\" title=\"".__("Retour")."\" ";
            echo "href=\"".OM_ROUTE_DASHBOARD."\">";
            echo __("Retour");
            echo "</a>";
        }
        echo "\t</div>\n";
        //
        echo "</form>\n";

        //
        echo "</div>\n";
    }
}


