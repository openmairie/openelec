<?php
/**
 * Ce script contient la définition de la classe *composition_scrutin*.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/composition_scrutin.class.php";

/**
 * Définition de la classe *composition_scrutin* (om_dbform).
 */
class composition_scrutin extends composition_scrutin_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[301] = array(
            "identifier" => "edition-pdf-composition-scrutin",
            "view" => "view_edition_pdf__composition_scrutin",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Composition"),
               "class" => "pdf-16",
               "order" => 51,
            ),
            "permission_suffix" => "edition_pdf__composition_scrutin",
        );
        $this->class_actions[302] = array(
            "identifier" => "edition-pdf-scrutin-president",
            "view" => "view_edition_pdf__scrutin_president",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Liste des présidents et présidents suppléants"),
               "class" => "pdf-16",
               "order" => 52,
            ),
            "permission_suffix" => "edition_pdf__scrutin_president",
        );
        $this->class_actions[303] = array(
            "identifier" => "edition-pdf-composition-bureau",
            "view" => "view_edition_pdf__composition_bureau",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Composition par bureau de vote"),
               "class" => "pdf-16",
               "order" => 53,
            ),
            "permission_suffix" => "edition_pdf__composition_bureaux",
        );
        $this->class_actions[304] = array(
            "identifier" => "edition-pdf-scrutin-publipost-convoc-agents",
            "view" => "view_edition_pdf__scrutin_publipost_convoc_agents",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Publipostage - convocations des agents"),
               "class" => "pdf-16",
               "order" => 61,
            ),
            "permission_suffix" => "edition_pdf__scrutin_publipost_convoc_agents",
        );
        $this->class_actions[305] = array(
            "identifier" => "edition-pdf-scrutin-publipost-convoc-pres",
            "view" => "view_edition_pdf__scrutin_publipost_convoc_pres",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Publipostage - convocations des présidents"),
               "class" => "pdf-16",
               "order" => 62,
            ),
            "permission_suffix" => "edition_pdf__scrutin_publipost_convoc_pres",
        );
        $this->class_actions[306] = array(
            "identifier" => "edition-pdf-scrutin-publipost-recep-asses-deleg",
            "view" => "view_edition_pdf__scrutin_publipost_recep_asses_deleg",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Publipostage - récépissés des assesseurs et délégues"),
               "class" => "pdf-16",
               "order" => 63,
            ),
            "permission_suffix" => "edition_pdf__scrutin_publipost_recep_asses_deleg",
        );
        $this->class_actions[401] = array(
            "identifier" => "traitement-scrutin-affectation-heures",
            "method" => "traitement_scrutin_affectation_heures",
            "portlet" => array(
               "type" => "action-direct-with-confirmation",
               "libelle" => __("Traitements - affectations heures"),
               "class" => "traitement-16",
               "order" => 71,
            ),
            "permission_suffix" => "traitement_scrutin_affectation_heures",
        );
        $this->class_actions[402] = array(
            "identifier" => "traitement-scrutin-transfert-candidature",
            "view" => "view_traitement_scrutin_transfert_candidature",
            "portlet" => array(
               "type" => "action-self",
               "libelle" => __("Traitements - transfert candidature"),
               "class" => "traitement-16",
               "order" => 72,
            ),
            "button" => "valider",
            "permission_suffix" => "traitement_scrutin_transfert_candidature",
        );
        $this->class_actions[403] = array(
            "identifier" => "traitement-scrutin-transfert-affectation",
            "view" => "view_traitement_scrutin_transfert_affectation",
            "portlet" => array(
               "type" => "action-self",
               "libelle" => __("Traitements - transfert affectation"),
               "class" => "traitement-16",
               "order" => 73,
            ),
            "button" => "valider",
            "permission_suffix" => "traitement_scrutin_transfert_affectation",
        );
    }

    /**
     * GETTER_FORMINC - sql_scrutin.
     *
     * @return string
     */
    function get_var_sql_forminc__sql_scrutin() {
        return sprintf(
            'SELECT reu_scrutin.id, reu_scrutin.libelle FROM %1$sreu_scrutin WHERE reu_scrutin.om_collectivite=%2$s ORDER BY reu_scrutin.libelle ASC',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
    }

    /**
     * VIEW - view_edition_pdf__scrutin_publipost_convoc_agents.
     *
     * @return void
     */
    public function view_edition_pdf__composition_bureau() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "composition_bureau",
            null,
            implode(';', $this->get_composition_bureau_ids())
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'scrutin-composition-bureaux-de-vote-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__scrutin_president.
     *
     * @return void
     */
    public function view_edition_pdf__scrutin_president() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "composition_scrutin_liste_presidents"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'composition-scrutin-liste-presidents-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__scrutin_bureau.
     *
     * @return void
     */
    public function view_edition_pdf__composition_scrutin() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "composition_scrutin"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'composition-scrutin-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__scrutin_publipost_convoc_agents.
     *
     * @return void
     */
    public function view_edition_pdf__scrutin_publipost_convoc_agents() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "candidature",
            null,
            implode(';', $this->getAgentsCandidaturesIds())
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'scrutin-publipostage-convocations-agents-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__scrutin_publipost_convoc_pres.
     *
     * @return void
     */
    public function view_edition_pdf__scrutin_publipost_convoc_pres() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "affectation",
            null,
            implode(';', $this->getElusAffectationsIds())
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'scrutin-publipostage-convocations-presidents-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($this->clean_and_insert_composition_bureau(array("id" => $id, )) !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($this->clean_and_insert_composition_bureau() !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * VIEW - view_edition_pdf__scrutin_publipost_recep_asses_deleg.
     *
     * @return void
     */
    public function view_edition_pdf__scrutin_publipost_recep_asses_deleg() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "recepisse",
            null,
            implode(';', $this->getOthersAffectationsIds())
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'scrutin-publipostage-recepisses-assesseurs-delegues-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    function clean_and_insert_composition_bureau($val = array()) {
        if (is_array($val) === true
            && array_key_exists("id", $val) === true) {
            //
            $id = intval($val["id"]);
        } else {
            $id = $this->getVal($this->clePrimaire);
        }
        $sql = sprintf(
            'DELETE FROM %1$scomposition_bureau WHERE composition_bureau.composition_scrutin=%2$s',
            DB_PREFIXE,
            $id
        );
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true) !== false) {
            return false;
        }
        $bureaux = $this->f->get_all_results_from_db_query(sprintf(
            'SELECT * FROM %1$sbureau WHERE bureau.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        ));
        foreach ($bureaux["result"] as $bureau) {
            $sql = sprintf(
                'INSERT INTO %1$scomposition_bureau (id, composition_scrutin, bureau) VALUES (nextval(\'%1$scomposition_bureau_seq\'), %2$s, %3$s)',
                DB_PREFIXE,
                $id,
                $bureau["id"]
            );
            $res = $this->f->db->query($sql);
            if ($this->f->isDatabaseError($res, true) !== false) {
                return false;
            }
        }
        return true;
    }

    /**
     * TREAMENT - traitement_scrutin_affectation_heures.
     *
     * @return boolean
     */
    public function traitement_scrutin_affectation_heures() {
        $this->begin_treatment(__METHOD__);

        $sql = sprintf(
            "UPDATE
                ".DB_PREFIXE."candidature AS C
             SET
                debut = P.debut,
                fin = P.fin
             FROM
                ".DB_PREFIXE."periode AS P
             WHERE
                C.composition_scrutin = '%s'
                AND C.decision IS true
                AND P.periode = C.periode
            ",
            $this->getVal($this->clePrimaire));
        $res = $this->f->db->query($sql);
        $this->addToLog(__METHOD__ . " query: $sql", DEBUG_MODE);
        if (DB::isError($res)) {
            $this->correct = false;
            $this->addToMessage(__("Échec de l'affectation des heures (".$res->getMessage().")"));
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("Affectation des heures réalisée avec succès"));
        /*echo '<div id="edition">'."\n";
        $candidatures = $this->getAgentsCandidatures();
        if (!empty($candidatures)) {
            echo "".
            "\t".'<fieldset class="cadre">'."\n".
            "\t\t".'<legend>'.__("Candidatures concernées").'</legend>'."\n".
            "\t\t".'<table class="tab-tab" cellpadding="5px" cellspacing="0px" border="1">'."\n".
            "\t\t\t".'<thead>'."\n".
            "\t\t\t\t".'<tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">'."\n".
            "\t\t\t\t\t".'<th colspan="6">'.__("Affectation des heures").'</th>'."\n".
            "\t\t\t\t".'</tr>'."\n".
            "\t\t\t".'</thead>'."\n".
            "\t\t\t".'<tbody>'."\n";
            foreach($candidatures as $candidature) {
                echo "".
                "\t\t\t\t".'<tr class="tab-data">'."\n".
                "\t\t\t\t\t".'<td class="col-0">'.$candidature['nom'].'</td>'."\n".
                "\t\t\t\t\t".'<td class="col-1">'.$candidature['prenom'].'</td>'."\n".
                "\t\t\t\t\t".'<td class="col-2">'.$candidature['periode'].'</td>'."\n".
                "\t\t\t\t\t".'<td class="col-2">'.__("début").' : '.$candidature['debut'].'</td>'."\n".
                "\t\t\t\t\t".'<td class="col-2">'.__("fin").' : '.$candidature['fin'].'</td>'."\n".
                "\t\t\t\t".'</tr>'."\n";
            }
            echo "".
            "\t\t\t".'</tbody>'."\n".
            "\t\t".'</table>'."\n".
            "\t".'</fieldset>'."\n";
        }
        else {
            echo "\t".'<p class="no-data">'.__("Aucune")." ".__("candidature concernée").'</p>'."\n";
        }
        echo '</div>'."\n";*/
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * VIEW - view_traitement_scrutin_transfert_candidature.
     *
     * @return void
     */
    public function view_traitement_scrutin_transfert_candidature() {

        // vérifie l'accessibilité de la vue
        $this->checkAccessibility();

        // retour formulaire
        $return_url = OM_ROUTE_FORM;
        $return_url .= "&obj=".$this->get_absolute_class_name();
        $return_url .= "&action=3";
        $return_url .= "&idx=".$this->getParameter("idx");
        $return_url .= "&advs_id=".$this->getParameter("advs_id");
        $return_url .= "&premier=".$this->getParameter("premier");
        $return_url .= "&tricol=".$this->getParameter("tricol");
        $return_url .= "&valid=".$this->getParameter("valid");
        $return_url .= "&retour=tab";
        $return_button = '<a class="retour retour-tab" href="'.$return_url.'">'.__("retour").'</a>';

        $scrutin = $this->getVal($this->clePrimaire);

        $submitted_scrutin_solde = null;
        $submitted_poste = null;

        $message = '';

        // si un sous-formulaire a été soumis
        if (!empty($this->f->get_submitted_post_value()) > 0) {

            // on vérifie les données saisies
            $error = false;
            if (empty($this->f->get_submitted_post_value('scrutin-solde'))) {
                $message .=  "\n".
                "\t".'<div class="message ui-widget ui-corner-all ui-state-error">'."\n".
                "\t\t".__("Veuillez sélectionner un scrutin soldé")."\n".
                "\t".'</div>'."\n";
                $error = true;
            }
            else {
                $submitted_scrutin_solde = $this->f->get_submitted_post_value('scrutin-solde');
            }
            if (empty($this->f->get_submitted_post_value('poste'))) {
                $message .=  "\n".
                "\t".'<div class="message ui-widget ui-corner-all ui-state-error">'."\n".
                "\t\t".__("Veuillez sélectionner un poste")."\n".
                "\t".'</div>'."\n";
                $error = true;
            }
            else {
                $submitted_poste = $this->f->get_submitted_post_value('poste');
            }

            // traiter le sous-formulaire si aucune erreur
            if(! $error) {
                $sql = sprintf("
                    SELECT
                        COUNT(id)
                    FROM
                        ".DB_PREFIXE."candidature
                    WHERE
                        composition_scrutin = '%s'
                        AND poste ='%s'
                        AND decision IS True
                    ",
                    $scrutin,
                    $submitted_poste
                );
                $this->addToLog(__METHOD__." query: $sql", DEBUG_MODE);
                $count_candidatures = $this->f->db->getOne($sql);

                $message .=  "\n".
                "\t".'<div class="message ui-widget ui-corner-all ui-state-default">'."\n".
                "\t\t".sprintf(
                    __("Il existait (avant transfert) %d candidature%s au poste de %s (décision rendue)."),
                    $count_candidatures, ($count_candidatures > 1 ? 's' : ''), $submitted_poste)."\n".
                "\t".'</div>'."\n";

                $sql = sprintf("
                    SELECT
                        S.libelle AS \"scrutin_solde\",
                        B.id AS \"bureau_id\",
                        B.libelle AS \"bureau\",
                        C.recuperation,
                        C.periode,
                        A.id AS \"agent_id\",
                        A.nom,
                        A.prenom
                    FROM
                        ".DB_PREFIXE."composition_scrutin AS S
                        INNER JOIN ".DB_PREFIXE."candidature AS C ON C.composition_scrutin = S.id
                        INNER JOIN ".DB_PREFIXE."agent AS A ON A.id=C.agent
                        INNER JOIN ".DB_PREFIXE."bureau AS B ON B.id=C.bureau
                    WHERE
                        S.id ='%s'
                        AND C.poste ='%s'
                        AND C.decision IS True
                    ",
                    $submitted_scrutin_solde,
                    $submitted_poste
                );
                $this->addToLog(__METHOD__." query: $sql", DEBUG_MODE);
                $select_res = $this->f->db->query($sql);
                if (DB::isError($select_res)) {
                    die($select_res->getMessage().$sql);
                }
                $output_html = "\n".'<ul style="padding: 10px 0 0 30px;">'."\n";
                $transfered_candidatures = 0;
                while ($row = $select_res->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $decision = 'false';
                    if($this->is_option_decision_enabled()) {
                        $decision = 'true';
                    }
                    // TODO je pense que la logique est biaisée, car cela permet d'ajouter,
                    // autant de fois que le processus est répété, des candidatures transférées
                    $sql = sprintf("
                        INSERT INTO ".DB_PREFIXE."candidature
                            (id, composition_scrutin, decision, periode, poste, bureau, recuperation, note, agent)
                        VALUES
                            (nextval('".DB_PREFIXE."candidature_seq'), '%s', %s, '%s', '%s', %s, '%s', '%s', %s)
                        ",
                        $scrutin,
                        $decision,
                        $row['periode'],
                        $submitted_poste,
                        $row['bureau_id'],
                        $row['recuperation'],
                        'transfert '.$row['scrutin_solde'],
                        $row['agent_id']
                    );
                    $this->addToLog(__METHOD__." query: $sql", DEBUG_MODE);
                    $insert_res = $this->f->db->query($sql);
                    $this->f->isDatabaseError($insert_res);

                    $output_html .= "\t".'<li>'."\n".
                        "\t\t".sprintf(
                                __("%s %s candidat au bureau '%s' au poste de '%s' pour la période '%s'."),
                                $row['nom'], $row['prenom'],
                                $row['bureau'], $submitted_poste, $row['periode']
                            )."\n".
                        "\t".'</li>'."\n";

                    $transfered_candidatures += 1;
                }
                $output_html .= '<ul>'."\n";

                // confirme le succès
                $message .=  "\n".
                "\t".'<div class="message ui-widget ui-corner-all ui-state-highlight">'."\n".
                "\t\t".'<p>'.sprintf(
                    __("Transfert de %d candidature%s effectué avec succès."),
                    $transfered_candidatures,
                    ($transfered_candidatures > 1 ? 's' : '')).'</p>'."\n".
                "\t".$output_html.
                "\t".'</div>'."\n";
            }

            // afficher les messages
            if (!empty($message)) {
                echo "\n".'<div id="form-message">'.$message.'</div>'."\n";
            }
        }

        // liste les scrutins soldés
        $scrutins_soldes = $this->getScrutinsSoldes();

        // liste les postes
        $postes = $this->getPostes('candidature');

        // si aucun scrutin soldé
        if (empty($scrutins_soldes)) {

            // affiche un message d'information
            echo "\n".
            '<div id="form-message">'."\n".
            "\t".'<div class="message ui-widget ui-corner-all ui-state-default">'."\n".
            "\t\t".__("Transfert de candidature inopérant car aucun scrutin soldé existe")."\n".
            "\t".'</div>'."\n".
            '</div>'."\n".
            "\t".'<div class="formControls formControls-bottom">'."\n".
            "\t\t".$return_button."\n".
            '</div>'."\n";
        }
        // si aucun poste défini
        elseif (empty($postes)) {

            // affiche un message d'information
            echo "\n".
            '<div id="form-message">'."\n".
            "\t".'<div class="message ui-widget ui-corner-all ui-state-default">'."\n".
            "\t\t".__("Transfert de candidature inopérant car aucun poste existe")."\n".
            "\t".'</div>'."\n".
            '</div>'."\n".
            "\t".'<div class="formControls formControls-bottom">'."\n".
            "\t\t".$return_button."\n".
            '</div>'."\n";
        }
        // si un scrutin soldé existe et au moins un poste est défini
        else {

            // Affichage du formulaire
            echo "\n".
            '<!-- ########## START SOUS FORMULAIRE ########## -->'."\n".
            '<form id="form_val" method="post" name="f2" action="">'."\n".
            "\t".'<div class="formEntete ui-corner-all">'."\n".
            "\t\t".'<div id="form-content">'."\n".
            "\t\t\t".'<div class="field field-type-select">'."\n".
            "\t\t\t\t".'<div class="form-libelle">'."\n".
            "\t\t\t\t\t".'<label id="lib-scrutin_solde" class="libelle-scrutin-solde" for="scrutin-solde">'."\n".
            "\t\t\t\t\t\t".__('Sélectionnez un scrutin soldé')."\n".
            "\t\t\t\t\t".'</label>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t\t".'<div class="form-content">'."\n".
            "\t\t\t\t\t".'<select id="scrutin-solde" name="scrutin-solde" class="champFormulaire" size="1">'."\n".
            "\t\t\t\t\t\t".'<option value="">--</option>'."\n";
            foreach($scrutins_soldes as $scrutin_solde) {
                $selected = '';
                if ($submitted_scrutin_solde == $scrutin_solde['id']) {
                    $selected = 'selected="selected"';
                }
                echo "\t\t\t\t\t\t".'<option '.$selected.' value="'.$scrutin_solde['id'].'">'.$scrutin_solde['libelle'].'</option>'."\n";
            }
            echo "".
            "\t\t\t\t\t".'</select>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t".'</div>'."\n".
            "\t\t\t".'<div class="field field-type-select">'."\n".
            "\t\t\t\t".'<div class="form-libelle">'."\n".
            "\t\t\t\t\t".'<label id="lib-poste" class="libelle-poste" for="poste">'."\n".
            "\t\t\t\t\t\t".__('Sélectionnez un poste')."\n".
            "\t\t\t\t\t".'</label>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t\t".'<div class="form-content">'."\n".
            "\t\t\t\t\t".'<select id="poste" name="poste" class="champFormulaire" size="1">'."\n".
            "\t\t\t\t\t\t".'<option value="">--</option>'."\n";
            foreach($postes as $poste) {
                $selected = '';
                if ($submitted_poste == $poste['poste']) {
                    $selected = 'selected="selected"';
                }
                echo "\t\t\t\t\t\t".'<option '.$selected.' value="'.$poste['poste'].'">'.$poste['poste'].'</option>'."\n";
            }
            echo "".
            "\t\t\t\t\t".'</select>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t".'</div>'."\n".
            "\t\t".'</div>'."\n".
            "\t".'</div>'."\n".
            "\t".'<div class="formControls formControls-bottom">'."\n".
            "\t\t".'<input type="submit" value="Transférer les candidatures" name="submit" '.
                    'class="om-button ui-button ui-widget ui-state-default ui-corner-all">'."\n".
                    $return_button.
            "\t".'</div>'."\n".
            '</form>'."\n".
            '<!-- ########## END SOUS FORMULAIRE ########## -->'."\n";
        }
    }

    /**
     * VIEW - view_traitement_scrutin_transfert_affectation.
     *
     * @return void
     */
    public function view_traitement_scrutin_transfert_affectation() {

        // vérifie l'accessibilité de la vue
        $this->checkAccessibility();

        // retour formulaire
        $return_url = OM_ROUTE_FORM;
        $return_url .= "&obj=".$this->get_absolute_class_name();
        $return_url .= "&action=3";
        $return_url .= "&idx=".$this->getParameter("idx");
        $return_url .= "&advs_id=".$this->getParameter("advs_id");
        $return_url .= "&premier=".$this->getParameter("premier");
        $return_url .= "&tricol=".$this->getParameter("tricol");
        $return_url .= "&valid=".$this->getParameter("valid");
        $return_url .= "&retour=tab";
        $return_button = '<a class="retour retour-tab" href="'.$return_url.'">'.__("retour").'</a>';

        $scrutin = $this->getVal($this->clePrimaire);

        $submitted_scrutin_solde = null;
        $submitted_poste = null;
        $submitted_candidat = null;
        $submitted_candidat_id = null;
        $submitted_candidat_nom = null;

        $message = '';

        // si un sous-formulaire a été soumis
        if (!empty($this->f->get_submitted_post_value()) > 0) {

            // on vérifie les données saisies
            $error = false;
            if (empty($this->f->get_submitted_post_value('scrutin-solde'))) {
                $message .=  "\n".
                "\t".'<div class="message ui-widget ui-corner-all ui-state-error">'."\n".
                "\t\t".__("Veuillez sélectionner un scrutin soldé")."\n".
                "\t".'</div>'."\n";
                $error = true;
            }
            else {
                $submitted_scrutin_solde = $this->f->get_submitted_post_value('scrutin-solde');
            }
            if (empty($this->f->get_submitted_post_value('poste'))) {
                $message .=  "\n".
                "\t".'<div class="message ui-widget ui-corner-all ui-state-error">'."\n".
                "\t\t".__("Veuillez sélectionner un poste")."\n".
                "\t".'</div>'."\n";
                $error = true;
            }
            else {
                $submitted_poste = $this->f->get_submitted_post_value('poste');
            }
            if (empty($this->f->get_submitted_post_value('candidat'))) {
                $message .=  "\n".
                "\t".'<div class="message ui-widget ui-corner-all ui-state-error">'."\n".
                "\t\t".__("Veuillez sélectionner un candidat")."\n".
                "\t".'</div>'."\n";
                $error = true;
            }
            else {
                $submitted_candidat = $this->f->get_submitted_post_value('candidat');
                if(strpos($submitted_candidat, '|') !== false) {
                    $submitted_candidat_tok = explode('|', $submitted_candidat);
                    $submitted_candidat_id = $submitted_candidat_tok[0];
                    $submitted_candidat_nom = $submitted_candidat_tok[1];
                }
            }

            // traiter le sous-formulaire si aucune erreur
            if(! $error) {
                $sql = sprintf("
                    SELECT
                        COUNT(id)
                    FROM
                        ".DB_PREFIXE."affectation
                    WHERE
                        composition_scrutin = '%s'
                        AND poste ='%s'
                        AND decision IS True
                        %s
                    ",
                    $scrutin,
                    $submitted_poste,
                    ($submitted_candidat != '*' ? "AND candidat = '".$submitted_candidat_id."'" : '' )
                );
                $this->addToLog(__METHOD__." query: $sql", DEBUG_MODE);
                $count_affectations = $this->f->db->getOne($sql);

                $message .=  "\n".
                "\t".'<div class="message ui-widget ui-corner-all ui-state-default">'."\n".
                "\t\t".sprintf(
                    __("Il existait (avant transfert) %d affectation%s au poste de %s %s(décision rendue)."),
                    $count_affectations, ($count_affectations > 1 ? 's' : ''), $submitted_poste,
                    ($submitted_candidat != '*' ? " pour le candidat '$submitted_candidat_nom'": ''))."\n".
                "\t".'</div>'."\n";

                $sql = sprintf("
                    SELECT
                        S.libelle AS \"scrutin_solde\",
                        B.id AS \"bureau_id\",
                        B.libelle AS \"bureau\",
                        A.periode,
                        C.id AS \"candidat_id\",
                        C.nom AS \"candidat_nom\",
                        E.id AS \"elu_id\",
                        E.nom \"elu_nom\",
                        E.prenom \"elu_prenom\"
                    FROM
                        ".DB_PREFIXE."composition_scrutin AS S
                        INNER JOIN ".DB_PREFIXE."affectation AS A ON A.composition_scrutin = S.id
                        INNER JOIN ".DB_PREFIXE."elu AS E ON E.id=A.elu
                        INNER JOIN ".DB_PREFIXE."bureau AS B ON B.id=A.bureau
                        LEFT JOIN ".DB_PREFIXE."candidat AS C ON C.id = A.candidat
                    WHERE
                        S.id ='%s'
                        AND A.poste ='%s'
                        AND A.decision IS True
                        %s
                    ",
                    $submitted_scrutin_solde,
                    $submitted_poste,
                    ($submitted_candidat != '*' ? "AND candidat = '".$submitted_candidat_id."'" : '' )
                );
                $this->addToLog(__METHOD__." query: $sql", DEBUG_MODE);
                $select_res = $this->f->db->query($sql);
                if (DB::isError($select_res)) {
                    die($select_res->getMessage().$sql);
                }
                $output_html = "\n".'<ul style="padding: 10px 0 0 30px;">'."\n";
                $transfered_affectations = 0;
                while ($row = $select_res->fetchRow(DB_FETCHMODE_ASSOC)) {
                    $decision = 'false';
                    if($this->is_option_decision_enabled()) {
                        $decision = 'true';
                    }
                    // TODO je pense que la logique est biaisée, car cela permet d'ajouter,
                    // autant de fois que le processus est répété, des candidatures transférées
                    $sql = sprintf("
                        INSERT INTO ".DB_PREFIXE."affectation
                            (id, composition_scrutin, decision, periode, poste, bureau, candidat, note, elu)
                        VALUES
                            (nextval('".DB_PREFIXE."affectation_seq'), %s, %s, '%s', '%s', %s, %s, '%s', %s)
                        ",
                        $scrutin,
                        $decision,
                        $row['periode'],
                        $submitted_poste,
                        $row['bureau_id'],
                        (!empty($row['candidat_id']) ? $row['candidat_id'] : 'NULL'),
                        'transfert '.$row['scrutin_solde'],
                        $row['elu_id']
                    );
                    $this->addToLog(__METHOD__." query: $sql", DEBUG_MODE);
                    $insert_res = $this->f->db->query($sql);
                    $this->f->isDatabaseError($insert_res);

                    $output_html .= "\t".'<li>'."\n".
                        "\t\t".sprintf(
                                __("%sélu %s %s affecté au bureau '%s' au poste de '%s' pour la période '%s'."),
                                (!empty($row['candidat_id']) ? 'Candidat '.$row['candidat_nom'].', ' : ''),
                                $row['elu_nom'], $row['elu_prenom'],
                                $row['bureau'], $submitted_poste, $row['periode']
                            )."\n".
                        "\t".'</li>'."\n";

                    $transfered_affectations += 1;
                }
                $output_html .= '<ul>'."\n";

                // confirme le succès
                $message .=  "\n".
                "\t".'<div class="message ui-widget ui-corner-all ui-state-highlight">'."\n".
                "\t\t".'<p>'.sprintf(
                    __("Transfert de %d affectation%s effectué avec succès."),
                    $transfered_affectations,
                    ($transfered_affectations > 1 ? 's' : '')).'</p>'."\n".
                "\t".$output_html.
                "\t".'</div>'."\n";
            }

            // afficher les messages
            if (!empty($message)) {
                echo "\n".'<div id="form-message">'.$message.'</div>'."\n";
            }
        }

        // liste les scrutins soldés
        $scrutins_soldes = $this->getScrutinsSoldes();

        // liste les postes
        $postes = $this->getPostes('affectation');

        // liste des candidats
        $candidats = $this->getCandidatFromAllScrutinsSoldes();

        // si aucun scrutin soldé
        if (empty($scrutins_soldes)) {

            // affiche un message d'information
            echo "\n".
            '<div id="form-message">'."\n".
            "\t".'<div class="message ui-widget ui-corner-all ui-state-default">'."\n".
            "\t\t".__("Transfert d'affectation inopérant car aucun scrutin soldé existe")."\n".
            "\t".'</div>'."\n".
            '</div>'."\n".
            "\t".'<div class="formControls formControls-bottom">'."\n".
            "\t\t".$return_button."\n".
            '</div>'."\n";
        }
        // si aucun poste défini
        elseif (empty($postes)) {

            // affiche un message d'information
            echo "\n".
            '<div id="form-message">'."\n".
            "\t".'<div class="message ui-widget ui-corner-all ui-state-default">'."\n".
            "\t\t".__("Transfert d'affectation inopérant car aucun poste existe")."\n".
            "\t".'</div>'."\n".
            '</div>'."\n".
            "\t".'<div class="formControls formControls-bottom">'."\n".
            "\t\t".$return_button."\n".
            '</div>'."\n";
        }
        // si un scrutin soldé existe et au moins un poste est défini
        else {

            // Affichage du formulaire
            echo "\n".
            '<!-- ########## START SOUS FORMULAIRE ########## -->'."\n".
            '<form id="form_val" method="post" name="f2" action="">'."\n".
            "\t".'<div class="formEntete ui-corner-all">'."\n".
            "\t\t".'<div id="form-content">'."\n".
            "\t\t\t".'<div class="field field-type-select">'."\n".
            "\t\t\t\t".'<div class="form-libelle">'."\n".
            "\t\t\t\t\t".'<label id="lib-scrutin_solde" class="libelle-scrutin-solde" for="scrutin-solde">'."\n".
            "\t\t\t\t\t\t".__('Sélectionnez un scrutin soldé')."\n".
            "\t\t\t\t\t".'</label>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t\t".'<div class="form-content">'."\n".
            "\t\t\t\t\t".'<select id="scrutin-solde" name="scrutin-solde" class="champFormulaire" size="1">'."\n".
            "\t\t\t\t\t\t".'<option value="">--</option>'."\n";
            foreach($scrutins_soldes as $scrutin_solde) {
                $selected = '';
                if ($submitted_scrutin_solde == $scrutin_solde['id']) {
                    $selected = 'selected="selected"';
                }
                echo "\t\t\t\t\t\t".'<option '.$selected.' value="'.$scrutin_solde['id'].'">'.$scrutin_solde['libelle'].'</option>'."\n";
            }
            echo "".
            "\t\t\t\t\t".'</select>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t".'</div>'."\n".
            "\t\t\t".'<div class="field field-type-select">'."\n".
            "\t\t\t\t".'<div class="form-libelle">'."\n".
            "\t\t\t\t\t".'<label id="lib-poste" class="libelle-poste" for="poste">'."\n".
            "\t\t\t\t\t\t".__('Sélectionnez un poste')."\n".
            "\t\t\t\t\t".'</label>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t\t".'<div class="form-content">'."\n".
            "\t\t\t\t\t".'<select id="poste" name="poste" class="champFormulaire" size="1">'."\n".
            "\t\t\t\t\t\t".'<option value="">--</option>'."\n";
            foreach($postes as $poste) {
                $selected = '';
                if ($submitted_poste == $poste['poste']) {
                    $selected = 'selected="selected"';
                }
                echo "\t\t\t\t\t\t".'<option '.$selected.' value="'.$poste['poste'].'">'.$poste['poste'].'</option>'."\n";
            }
            echo "".
            "\t\t\t\t\t".'</select>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t".'</div>'."\n".
            "\t\t\t".'<div class="field field-type-select">'."\n".
            "\t\t\t\t".'<div class="form-libelle">'."\n".
            "\t\t\t\t\t".'<label id="lib-candidat" class="libelle-candidat" for="candidat">'."\n".
            "\t\t\t\t\t\t".__('Sélectionnez un candidat')."\n".
            "\t\t\t\t\t".'</label>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t\t".'<div class="form-content">'."\n".
            "\t\t\t\t\t".'<select id="candidat" name="candidat" class="champFormulaire" size="1">'."\n".
            "\t\t\t\t\t\t".'<option value="">--</option>'."\n";
            $selected = '';
            if ($submitted_candidat == '*') {
                $selected = 'selected="selected"';
            }
            echo "".
            "\t\t\t\t\t\t".'<option '.$selected.' value="*">'.__('Tous').'</option>'."\n";
            foreach($candidats as $candidat) {
                $selected = '';
                if ($submitted_candidat_id == $candidat['id']) {
                    $selected = 'selected="selected"';
                }
                echo "".
                "\t\t\t\t\t\t".'<option '.$selected.' value="'.$candidat['id'].'|'.$candidat['nom'].'">'.
                               $candidat['nom'].' ('.$candidat['scrutin'].')'.
                               '</option>'."\n";
            }
            echo "".
            "\t\t\t\t\t".'</select>'."\n".
            "\t\t\t\t".'</div>'."\n".
            "\t\t\t".'</div>'."\n".
            "\t\t".'</div>'."\n".
            "\t".'</div>'."\n".
            "\t".'<div class="formControls formControls-bottom">'."\n".
            "\t\t".'<input type="submit" value="Transférer les affectations" name="submit" '.
                    'class="om-button ui-button ui-widget ui-state-default ui-corner-all">'."\n".
                    $return_button.
            "\t".'</div>'."\n".
            '</form>'."\n".
            '<!-- ########## END SOUS FORMULAIRE ########## -->'."\n";
        }
    }

   /**
     * Vérifie que l'option 'decision' est activée.
     *
     * @return boolean
     */
    public function is_option_decision_enabled() {

        // On récupère les informations de la collectivité du dossier
        $collectivite_param = $this->f->getCollectivite($this->getVal('om_collectivite'));

        // Si l'om_parametre *option_decision* existe et qu'il vaut sig_externe
        return isset($collectivite_param['option_decision']) and $collectivite_param['option_decision'] == 'true';
    }

    /**
     * Get all 'candidats' from 'scrutins soldés'.
     * @return array
     */
    public function getCandidatFromAllScrutinsSoldes() {
        $candidats = array();
        $sql = "
            SELECT
                C.id,
                C.nom,
                S.libelle AS \"scrutin\"
             FROM
                ".DB_PREFIXE."composition_scrutin AS S
                INNER JOIN ".DB_PREFIXE."candidat AS C ON C.composition_scrutin=S.id
             WHERE
                S.solde IS true
             ORDER BY
                C.nom
        ";
        $res = $this->f->db->query($sql);
        if (DB::isError($res)) {
            die($res->getMessage().$sql);
        }
        while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $candidats[] = $row;
        }
        return $candidats;
    }

    /**
     * Get 'scrutins soldés'.
     * @return array
     */
    public function getScrutinsSoldes() {
        $scrutins = array();
        $sql = sprintf(
            "SELECT
                id,
                libelle
             FROM
                ".DB_PREFIXE."composition_scrutin
             WHERE
                solde IS true
             ORDER BY
                id",
            $this->getVal($this->clePrimaire));
        $res = $this->f->db->query($sql);
        if (DB::isError($res)) {
            die($res->getMessage().$sql);
        }
        while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $scrutins[] = $row;
        }
        return $scrutins;
    }

    function get_composition_bureau_ids() {
        $composition_bureau = $this->f->get_all_results_from_db_query(sprintf(
            'SELECT * FROM %1$scomposition_bureau WHERE composition_bureau.composition_scrutin=%2$s',
            DB_PREFIXE,
            intval($this->getVal($this->clePrimaire))
        ));
        $ids = array();
        foreach($composition_bureau["result"] as $composition_bureau_elem) {
            $ids[] = $composition_bureau_elem["id"];
        }
        return $ids;
    }

    /**
     * Get 'postes' for the specified 'nature'.
     * @return array
     */
    public function getPostes($nature) {
        $postes = array();
        $sql = sprintf(
            "SELECT
                poste
             FROM
                ".DB_PREFIXE."poste
             WHERE
                nature = '%s'
             ORDER BY
                ordre",
            $nature
        );
        $res = $this->f->db->query($sql);
        if (DB::isError($res)) {
            die($res->getMessage().$sql);
        }
        while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $postes[] = $row;
        }
        return $postes;
    }

    /**
     * Get agents candidatures for this scrutin.
     * @return array
     */
    public function getAgentsCandidatures() {
        $candidatures = array();
        $sql = sprintf(
            "SELECT
                A.nom,
                A.prenom,
                C.periode,
                C.debut,
                C.fin
             FROM
                ".DB_PREFIXE."candidature AS C
                INNER JOIN ".DB_PREFIXE."agent AS A ON A.id=C.agent
             WHERE
                C.composition_scrutin = '%s'
                AND C.decision IS true
             ORDER BY
                A.nom",
            $this->getVal($this->clePrimaire));
        $res = $this->f->db->query($sql);
        if (DB::isError($res)) {
            die($res->getMessage().$sql);
        }
        while ($row = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $candidatures[] = $row;
        }
        return $candidatures;
    }

    /**
     * Get agents candidatures (ids) for this scrutin.
     * @return array
     */
    public function getAgentsCandidaturesIds() {
        $ids = array();
        $sql = sprintf(
            "SELECT
                id
             FROM
                ".DB_PREFIXE."candidature
             WHERE
                composition_scrutin = '%s'
             ORDER BY
                bureau",
            $this->getVal($this->clePrimaire));
        $res = $this->f->db->query($sql);
        if (DB::isError($res)) {
            die($res->getMessage().$sql);
        }
        while ($row = $res->fetchRow()) {
            $ids[] = $row[0];
        }
        return $ids;
    }

    /**
     * Get elus affectations (ids) for this scrutin.
     * This exclude 'PRESIDENT SUPPLEANT'.
     * @return array
     */
    public function getElusAffectationsIds() {
        $ids = array();
        $sql = sprintf(
            "SELECT
                id
             FROM
                ".DB_PREFIXE."affectation
             WHERE
                composition_scrutin = '%s'
                AND poste = '%s'
             ORDER BY
                bureau",
            $this->getVal($this->clePrimaire),
            'PRESIDENT');
        $res = $this->f->db->query($sql);
        if (DB::isError($res)) {
            die($res->getMessage().$sql);
        }
        while ($row = $res->fetchRow()) {
            $ids[] = $row[0];
        }
        return $ids;
    }

    /**
     * Get others affectations (ids) for this scrutin.
     * This include 'PRESIDENT SUPPLEANT'.
     * @param  $decision_true_only  bool  If 'true' include only affectations that have 'decision' = 'true'
     * @return array
     */
    public function getOthersAffectationsIds($decision_true_only=true) {
        $ids = array();
        $sql = sprintf(
            "SELECT
                id
             FROM
                ".DB_PREFIXE."affectation
             WHERE
                composition_scrutin = '%s'
                ".($decision_true_only ? 'AND decision IS TRUE' : '')."
                AND poste != '%s'
             ORDER BY
                bureau",
            $this->getVal($this->clePrimaire),
            'PRESIDENT');
        $res = $this->f->db->query($sql);
        if (DB::isError($res)) {
            die($res->getMessage().$sql);
        }
        while ($row = $res->fetchRow()) {
            $ids[] = $row[0];
        }
        return $ids;
    }

    /**
     * SETTER_FORM - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);
        //
        if ($maj == 0 || $maj ==1) {
            $form->setType('tour', 'select');
            $form->setType('solde', 'checkbox');
        }
    }

    /**
     * SETTER_FORM - setTaille.
     *
     * @return void
     */
    public function setTaille(&$form, $maj){
        parent::setTaille($form, $maj);
        $form->setTaille('convocation_agent', 80);
        $form->setTaille('convocation_president', 80);
    }

    /**
     * SETTER_FORM - setSelect.
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // tour
        $contenu=array();
        $contenu[0]=array('1','2');
        $contenu[1]=array(_("1er tour"),_("2eme tour"));
        $form->setSelect("tour", $contenu);
    }
}
