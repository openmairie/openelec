<?php
/**
 * Ce script définit la classe 'membre_commission'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/membre_commission.class.php";

/**
 * Définition de la classe 'membre_commission' (om_dbform).
 */
class membre_commission extends membre_commission_gen {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        //
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-convocationcommission",
            "view" => "view_edition_pdf__convocationcommission",
            "permission_suffix" => "edition_pdf__convocationcommission",
        );
    }

    /**
     * VIEW - view_edition_pdf__convocationcommission.
     *
     * @return void
     */
    public function view_edition_pdf__convocationcommission() {
        $this->checkAccessibility();

        // Création du tableau des identifiants des membres convoqué à la commission
        $sql_membre = sprintf(
            'SELECT membre_commission.id FROM %1$smembre_commission WHERE membre_commission.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res_membre = $this->f->db->query($sql_membre);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$sql_membre."\")",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res_membre);
        //
        $idx = array();
        while ($row_membre=& $res_membre->fetchRow(DB_FETCHMODE_ASSOC)) {
            $idx[] = $row_membre["id"];
        }
        //
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "convocationcommission",
            null,
            implode(';', $idx)
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'convocationcommission-%s.pdf',
                date('YmdHis')
            )
        );
    }

    /**
     * Parametrage du formulaire - Type des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // ajouter et modifier
        if ($maj == 0 || $maj == 1) {
            $form->setType("civilite", "select");
        } else {
            $form->setType("civilite", "selecthiddenstatic");
        }
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // Select civilite
        $contenu = array(
            array('M.', 'Mme', 'Mlle'),
            array(__('Monsieur'), __('Madame'), __('Mademoiselle')),
        );
        $form->setSelect("civilite", $contenu);
    }

    /**
     * Parametrage du formulaire - Libelle des entrees de formulaire
     *
     * @param object $form Objet formulaire
     * @param integer $maj Mode de mise a jour :
     *                     - 0 => ajout
     *                     - 1 => modification
     *                     - 2 => suppression
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("id", __("Id"));
        $form->setLib("civilite", __("Civilité"));
        $form->setLib("nom", __("Nom"));
        $form->setLib("prenom", __("Prénom"));
        $form->setLib("adresse", __("Adresse"));
        $form->setLib("complement", __("Complément"));
        $form->setLib("cp", __("Code postal"));
        $form->setLib("ville", __("Ville"));
        $form->setLib("cedex", __("Cedex"));
        $form->setLib("bp", __("Bp"));
    }

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     *
     * @return array
     */
    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        $labels["membre_commission"]["membre_commission.civilite_libelle"] = __("Civilité en toutes lettres");
        $labels["convocation_commission"]["convocation_commission.message"] = __("Message de convocation");
        // Retour de tous les libellés
        return $labels;
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     *
     * @return array
     */
    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        //
        $convocation_message = "Aucun message de convocation.";
        if (isset($_POST["message"])) {
            $convocation_message = $_POST["message"];
        }
        $values["convocation_commission.message"] = $convocation_message;
        //
        switch ($this->getVal("civilite")) {
            case "M.":
                $values["membre_commission.civilite_libelle"] = "Monsieur";
                break;
            case "Mme":
                $values["membre_commission.civilite_libelle"] = "Madame";
                break;
            case "Mlle":
                $values["membre_commission.civilite_libelle"] = "Mademoiselle";
                break;
            default:
                $values["membre_commission.civilite_libelle"] = $this->getVal("civilite");
        }
        //
        return $values;
    }
}
