<?php
/**
 * Ce script définit la classe 'module_notification_courriel'.
 *
 * @package openelec
 * @version SVN : $Id: module_refonte.class.php 1991 2019-04-16 21:34:00Z fmichon $
 */

require_once "../obj/module.class.php";

/**
 * Définition de la classe 'module_refonte' (module).
 */
class module_notification_courriel extends module {

    /**
     *
     */
    function init_module() {
        $this->module = array(
            "right" => "module_notification_courriel",
            "title" => __("Paramétrage")." -> ".__("Module Notification par courriel"),
            "elems" => array(
                array(
                    "type" => "tab",
                    "right" => "module_notification_courriel",
                    "id" => "module_notification_courriel",
                    "href" => "../app/index.php?module=module_notification_courriel&view=controlpanel_notification_courriel",
                    "title" => __("Paramétrage des notifications par courriel"),
                    "view" => "controlpanel_notification_courriel",
                ),
                array(
                    "type" => "overlay",
                    "right" => "module_notification_courriel",
                    "href" => "../app/index.php?module=module_notification_courriel&view=add_profil_selected",
                    "title" => __("Sélection des profils"),
                    "view" => "add_profil_selected",
                ),
            ),
        );
    }

    /**
     * VIEW - view_controlpanel_mail_notification
     *
     * Vue permettant de paramétrer le traitement de notification par mail des utilisateurs.
     *
     * @return void
     */

    function view__controlpanel_notification_courriel(){
                // Si ce n'est pas une requete Ajax on redirige vers la page d'accueil du module
        if ($this->f->isAjaxRequest() !== true) {
            header("Location:../app/index.php?module=module_notification_courriel");
            return;
        }
        // Definition du charset de la page
        header("Content-type: text/html; charset=".HTTPCHARSET."");

        $om_parameter_value_multi = $this->f->get_one_result_from_db_query(
            $this->get_query_om_parametre('multi')
        );
        if($om_parameter_value_multi['result'] === NULL || $om_parameter_value_multi['result'] === "") {
            $requete_profils_multi = NULL;
        } else {
            $requete_profils_multi = $this->f->get_all_results_from_db_query(
                $this->get_query_profils('multi')
            );   
        }

        // On récupère les valeurs présent dans le paramètre notification_courriel_profils sur la commune mono 
        $om_parameter_value_mono = $this->f->get_one_result_from_db_query(
            $this->get_query_om_parametre('mono')
        );
        if($om_parameter_value_mono['result'] === NULL || $om_parameter_value_mono['result'] === "") {
            $requete_profils_mono = NULL;
        } else {
            $requete_profils_mono = $this->f->get_all_results_from_db_query(
                $this->get_query_profils('mono')
            );
        }
        // Initialise les templates
        // Pas de profil sélectionné
        $template_bloc_container = '
            <div id="bloc_profil">
                %1$s
                %2$s
            </div>
            <div class="visualClear"></div>
        ';
        //
        $template_bloc_param_profil_ajout = '
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        Profil(s) sélectionné(s) %s
                    </div>
                    <div class="card-body text-center">
                        <p>Aucun profil sélectionné</p>
                        %s
                    </div>
                </div>
            </div>
            %s
        ';
        // Profils sélectionnés
        $template_bloc_param_profil_selected = '
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        Profil(s) sélectionné(s) %s
                    </div>
                    <div class="card-body text-center">
                        %s
                        %s
                    </div>
                </div>
            </div>
            %s
        ';
        // Lien vers l'overlay qui permet la selection des profils
        $edit = "";
        // Partie html permettant d'afficher l'overlay
        $form_control_overlay = '<div id="tabs-1"><div class="formControls"></div></div><!-- nécessaire pour le popupit -->';
        //
        if ($requete_profils_multi === null
            || (is_array($requete_profils_multi) === true
                && array_key_exists("result", $requete_profils_multi) === true
                && $requete_profils_multi["result"] === null)) {
            //
            $edit = $this->get_collectivite_niveau();
            if($edit == ""){
                $form_control_overlay = "";
            }
            $bloc_multi_affiche = sprintf($template_bloc_param_profil_ajout, 'dans la collectivité', $edit, $form_control_overlay);
        } else {
            $edit = $this->get_collectivite_niveau();
            $res = $requete_profils_multi['result'];
            $element_list="";
            foreach($requete_profils_multi['result'] as $value => $elem){
                $element_list .= "<li>".$elem['libelle']."</li>";
            }

            $profils_list = sprintf('
                <ul>
                    %s
                </ul>
            ',
            $element_list);
            if($edit == ""){
                $form_control_overlay = "";
            }
            $bloc_multi_affiche = sprintf($template_bloc_param_profil_selected, 'dans la collectivité', $profils_list, $edit, $form_control_overlay);
        }
        // Pour les collectivité de niveau 1 on peut modifier la liste des profils
        $edit = sprintf(
            '<a id="action-notification-select-profil" href="../app/index.php?module=module_notification_courriel&view=add_profil_selected">Définir/Modifier</a>'
        );
        $form_control_overlay = '<div id="tabs-1"><div class="formControls"></div></div><!-- nécessaire pour le popupit -->';
        //
        if ($requete_profils_mono === null
            || (is_array($requete_profils_mono) === true
                && array_key_exists("result", $requete_profils_mono) === true
                && $requete_profils_mono["result"] === null)) {
            //
            $bloc_mono_affiche = sprintf($template_bloc_param_profil_ajout, 'dans cette commune', $edit, $form_control_overlay);
        } else {
            $res = $requete_profils_mono['result'];
            $element_list="";
            foreach($requete_profils_mono['result'] as $value => $elem){
                $element_list .= "<li>".$elem['libelle']."</li>";
            }

            $profils_list = sprintf('
                <ul>
                    %s
                </ul>
            ',
            $element_list);
            $bloc_mono_affiche = sprintf($template_bloc_param_profil_selected, 'dans cette commune', $profils_list, $edit, $form_control_overlay);
        }
        // Affichage des bloc en fonction de la collectivité sur laquelle on est connecté
        if ($_SESSION['niveau'] == 2) {
            printf($template_bloc_container, $bloc_multi_affiche, '');
        } elseif ($_SESSION['niveau'] == 1 && $requete_profils_multi !== NULL) {
            printf($template_bloc_container,$bloc_multi_affiche, $bloc_mono_affiche);
        } else {
            printf($template_bloc_container, $bloc_mono_affiche, '');
        }

        /**
         *
         */
        // Ouverture de la balise - DIV paragraph
        echo "<div class=\"paragraph\">\n";
        //
        require_once "../obj/traitement.notification_courriel.class.php";
        $trt = new notificationCourrielTraitement();
        $trt->displayForm();
        // Fermeture de la balise - DIV paragraph
        echo "</div>\n";
    }

    /**
     * VIEW - view__add_profil_selected.
     *
     * @return void
     */
    function view__add_profil_selected() {
        // TREATMENT
        if (isset($_POST['selectprofil_action_valid']) === true) {
            //
            $profils = $this->f->get_submitted_post_value("profils");
            if($profils != null) {
                $profils_string = implode(";", $profils);
            } else {
                $profils_string = "";
            }
            $ret = $this->f->insert_or_update_parameter("notification_courriel_profils", trim($profils_string));
            if ($ret === true) {
                $this->f->displayMessage(
                    "ok",
                    __("Vos modifications ont bien été enregistrées.")
                );
            } else {
                $this->f->displayMessage(
                    "error",
                    __("Une erreur est survenue. Contactez votre administrateur.")
                );
            }
            return;
        }

        // FORM
        echo "\n<div id=\"selectprofilform\" class=\"formulaire\">\n";
        //
        $this->f->layout->display__form_container__begin(array(
            "id" => "selectprofilform_form",
            "action" => "",
            "name" => "f2",
            "onsubmit" => "affichersform('select-profil-form', '../app/index.php?module=module_notification_courriel&view=add_profil_selected', this);return false;",
        ));
        //
        //
        $requete_profils_existant = $this->f->get_all_results_from_db_query(
            sprintf(
                'SELECT * FROM %som_profil',
                DB_PREFIXE
            )
        );
        echo "\t<div class=\"field\">\n";
        echo "\t\t<label for=\"profil\">".__("Profil(s)")."</label>\n";
        echo "<select id=\"profil_selection\"  multiple=\"multiple\" name=\"profils[]\" class=\"champFormulaire selectmultiple\">";
            foreach($requete_profils_existant['result'] as $elem => $value){
                echo "<option value=".$value['om_profil'].">".$value['libelle']."</option>";
            }
        echo "</select>";
        echo "\t</div>\n";
        //
        $this->f->layout->display__form_controls_container__begin(array(
            "controls" => "bottom",
        ));
        $this->f->layout->display__form_input_submit(array(
            "class" => "context boutonFormulaire",
            "name" => "selectprofil.action.valid",
            "value" => __("Valider"),
        ));
        $this->f->layout->display_form_retour(array(
            "id" => "form-action-select-profil-back-".uniqid(),
            "href" => "../app/index.php?module=module_notification_courriel",
        ));
        $this->f->layout->display__form_controls_container__end();
        $this->f->layout->display__form_container__end();
        //
        echo "</div>\n";
    }

    /**
     * Renvoie le lien de modification si la collectivité sur laquelle on est connecté est de niveau 2
     *
     * @return string
     */
    function get_collectivite_niveau() {
        $edit = "";
        if ($_SESSION['niveau'] == 2) {
            $edit = sprintf(
                    '<a id="action-notification-select-profil" href="../app/index.php?module=module_notification_courriel&view=add_profil_selected">Définir/Modifier</a>'
                );
        }
        return $edit;         
    }

    function get_query_om_parametre($multi_mono){
        // Template de la requête permettant de récupérer les valeur du paramètre notification_courriel_profils
        $requete_om_parameter_template =  'SELECT valeur FROM %2$som_parametre INNER JOIN %2$som_collectivite on om_parametre.om_collectivite = om_collectivite.om_collectivite WHERE om_parametre.libelle = \'notification_courriel_profils\' %1$s %3$s';
        // Condition pour la collectivité de niveau 2
        $requete_where_multi = 'AND om_collectivite.niveau = \'2\'';
        // Condition par identifiant de collectivité
        $requete_where_mono = 'AND om_collectivite.om_collectivite = ';
        if($multi_mono == 'mono') {
            $query_to_return = sprintf($requete_om_parameter_template, $requete_where_mono, DB_PREFIXE, $_SESSION['collectivite']);
        } elseif ($multi_mono == 'multi') {
            $query_to_return = sprintf($requete_om_parameter_template, $requete_where_multi, DB_PREFIXE, '');
        }

        return $query_to_return;
    }

    function get_query_profils($multi_mono){
        // Template de la requête permettant de récupérer les libellés des profils sélectionnés
        $requete_profils_template = 'SELECT om_profil.libelle FROM %2$som_profil WHERE om_profil IN (SELECT regexp_split_to_table(valeur,\';\')::integer FROM %2$som_parametre INNER JOIN %2$som_collectivite on om_parametre.om_collectivite = om_collectivite.om_collectivite WHERE om_parametre.libelle = \'notification_courriel_profils\' %1$s %3$s)';
        // Condition pour la collectivité de niveau 2
        $requete_where_multi = 'AND om_collectivite.niveau = \'2\'';
        // Condition par identifiant de collectivité
        $requete_where_mono = 'AND om_collectivite.om_collectivite = ';
        if($multi_mono == 'mono') {
            $query_to_return = sprintf(
                $requete_profils_template, 
                $requete_where_mono, 
                DB_PREFIXE, 
                $_SESSION['collectivite']
            );
        } elseif ($multi_mono == 'multi') {
            $query_to_return = sprintf(
                $requete_profils_template, 
                $requete_where_multi, 
                DB_PREFIXE, 
                ''
            );
        }

        return $query_to_return;
    }
}
