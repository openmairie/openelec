<?php
/**
 * Ce script définit la classe 'om_app_gen'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_gen.class.php";

/**
 * Définition de la classe 'om_app_gen' (om_base).
 *
 * Cette classe est destinée à permettre la surcharge de certaines méthodes de
 * la classe 'om_gen' du framework pour des besoins spécifiques de
 * l'application.
 */
class om_app_gen extends gen {

    /**
     * @return array
     */
    function get_all_permissions() {
        $permissions = parent::get_all_permissions();
        //
        $obj_contents = @scandir("../obj/");
        if ($obj_contents === false) {
            $obj_contents = array();
        }
        //
        foreach ($obj_contents as $elem) {
            if ($this->f->starts_with($elem, "module") !== true) {
                continue;
            }
            require_once $elem;
            $filename_exploded = explode(".", $elem);
            $inst = new $filename_exploded[0]();
            if (array_key_exists("right", $inst->module) === true) {
                $permissions[] = $inst->module["right"];
            }
            if (array_key_exists("elems", $inst->module) === true) {
                foreach ($inst->module["elems"] as $tab) {
                    if (array_key_exists("right", $tab)) {
                        $permissions[] = $tab["right"];
                    }
                }
            }
        }
        //
        $permissions = array_unique($permissions);
        sort($permissions);
        return $permissions;
    }
}
