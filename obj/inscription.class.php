<?php
/**
 * Ce script définit la classe 'inscription'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/mouvement.class.php";

/**
 * Définition de la classe 'inscription' (om_dbform).
 *
 * Surcharge de la classe 'mouvement'.
 */
class inscription extends mouvement {

    /**
     *
     */
    protected $_absolute_class_name = "inscription";

    /**
     * Type de mouvement correspondant à la valeur de typecat dans la table
     * param_mouvement
     * @var string Type de mouvement
     */
    var $typeCat = "inscription";

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        mouvement_gen::init_class_actions();
        //
        $this->class_actions[0]["condition"] = array(
            "is_collectivity_mono",
        );
        //
        $this->class_actions[1]["condition"] = array(
            "is_collectivity_mono",
            "is_not_treated",
            "is_not_in_status__abandonne",
            "has_not_a_visa",
        );
        //
        $this->class_actions[2]["condition"] = array(
            "is_collectivity_mono",
            "is_not_treated",
            "is_not_reu_connected",
        );
        //
        $this->class_actions[101] = array(
            "identifier" => "add-search-form",
            "view" => "view_add_search_form",
            "permission_suffix" => "ajouter",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[102] = array(
            "identifier" => "add-search-form-results",
            "view" => "view_add_search_form_results",
            "permission_suffix" => "ajouter",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[201] = array(
            "identifier" => "edition-pdf-attestationinscription",
            "view" => "view_edition_pdf__attestationinscription",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Attestation d'inscription"),
               "class" => "pdf-16",
               "order" => 52,
            ),
            "permission_suffix" => "edition_pdf__attestationinscription",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        //
        $this->class_actions[202] = array(
            "identifier" => "edition-pdf-refusinscription",
            "view" => "view_edition_pdf__refusinscription",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Refus d'inscription"),
               "class" => "courrierrefus-16",
               "order" => 53,
            ),
            "permission_suffix" => "edition_pdf__refusinscription",
            "condition" => array(
                "is_collectivity_mono",
                "is_option_refus_mouvement_enabled",
            ),
        );
        //
        $this->class_actions[203] = array(
            "identifier" => "statistiques-doublon1",
            "view" => "view_edition_pdf__verification_doublon_inscription",
            "permission_suffix" => "edition_pdf__verification_doublon_inscription",
        );
        //
        $this->class_actions[204] = array(
            "identifier" => "edition-pdf-recepisseinscription",
            "view" => "view_edition_pdf__recepisseinscription",
            "portlet" => array(
               "type" => "action-blank",
               "libelle" => __("Récépissé"),
               "class" => "pdf-16",
               "order" => 51,
            ),
            "permission_suffix" => "edition_pdf__recepisseinscription",
            "condition" => array(
                "is_collectivity_mono",
            ),
        );
        $this->class_actions[61] = array(
            "identifier" => "valider",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("valider"),
                "class" => "valider-16",
            ),
            "permission_suffix" => "valider",
            "method" => "valider",
            "condition" => array(
                "is_collectivity_mono",
                "is_reu_connected",
                "is_in_status__vise_insee",
                "has_a_visa__accepte",
                "has_a_bureau",
            ),
        );
        $this->class_actions[62] = array(
            "identifier" => "modifier_bureau",
            "crud" => "update",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("modifier"),
                "class" => "edit-16",
                "order" => 1,
            ),
            "permission_suffix" => "modifier",
            "method" => "modifier",
            "condition" => array(
                "is_collectivity_mono",
                "is_reu_connected",
                "is_in_status__vise_insee__or__vise_maire",
                "has_a_visa__accepte",
                "is_not_treated",
            ),
        );
        $this->class_actions[63] = array(
            "identifier" => "marquer_comme_vu",
            "crud" => "update",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("marquer comme vu"),
                "class" => "valider-16",
                "order" => 1,
            ),
            "permission_suffix" => "marquer_comme_vu",
            "method" => "mark_as_seen",
            "condition" => array(
                "is_collectivity_mono",
                "is_not_marked_as_seen",
            ),
        );
        $this->class_actions[367] = array(
            "identifier" => "etiquettes-from-listing",
            "permission_suffix" => "etiquettes_from_listing",
            "view" => "view__edition_pdf__etiquettes_from_listing",
        );
        //
        $this->class_actions[501] = array(
            "identifier" => "reu-abandonner",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("abandonner"),
                "class" => "abandonner-16",
            ),
            "permission_suffix" => "abandonner",
            "method" => "abandonner",
            "condition" => array(
                "is_collectivity_mono",
                "is_reu_connected",
                "has_not_a_visa",
                "is_in_status__ouvert__or__en_attente",
            ),
        );
        //
        $this->class_actions[503] = array(
            "identifier" => "reu-completer",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("compléter"),
                "class" => "completer-16",
            ),
            "permission_suffix" => "completer",
            "button" => __("compléter"),
            "method" => "completer",
            "condition" => array(
                "is_collectivity_mono",
                "is_reu_connected",
                "has_not_a_visa",
                "is_in_status__ouvert__or__en_attente",
            ),
        );
        //
        $this->class_actions[504] = array(
            "identifier" => "reu-attendre",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => __("attendre"),
                "class" => "attendre-16",
            ),
            "permission_suffix" => "attendre",
            "button" => __("attendre"),
            "method" => "attendre",
            "condition" => array(
                "is_collectivity_mono",
                "is_reu_connected",
                "has_not_a_visa",
                "is_in_status__ouvert",
            ),
        );
        //
        $this->class_actions[502] = array(
            "identifier" => "reu-viser",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => __("viser"),
                "class" => "viser-16",
            ),
            "permission_suffix" => "viser",
            "button" => __("viser"),
            "method" => "viser",
            "condition" => array(
                "is_collectivity_mono",
                "is_reu_connected",
                "has_not_a_visa",
                "is_in_status__complet",
                "has_a_bureau",
            ),
        );
        //
        $this->class_actions[251] = array(
            "identifier" => "decoupage",
            "view" => "view_decoupage_json",
            "permission_suffix" => "consulter",
        );
        //
        $this->class_actions[401] = array(
            "identifier" => "consulter-historique",
            "view" => "view_consulter_historique",
            "permission_suffix" => "consulter_historique",
            "condition" => array(
                "exists",
            ),
        );
        $this->class_actions[91] = array(
            "identifier" => "traitements_par_lot",
            "view" => "view_traitements_par_lot",
            "permission_suffix" => "traiter_par_lot",
        );
    }

    /**
     * VIEW - view__edition_pdf__etiquettes_from_listing.
     *
     * @return void
     */
    public function view__edition_pdf__etiquettes_from_listing() {
        $this->checkAccessibility();
        $f = $this->f;
        // Initialisation des paramètres
        $params = array(
            // Nom de l'objet metier
            "obj" => array(
                "default_value" => "",
            ),
            // Premier enregistrement a afficher
            "premier" => array(
                "default_value" => 0,
            ),
            // Colonne choisie pour le tri
            "tricol" => array(
                "default_value" => "",
            ),
            // Id unique de la recherche avancee
            "advs_id" => array(
                "default_value" => "",
            ),
            // Valilite des objets a afficher
            "valide" => array(
                "default_value" => "",
            ),
            "contentonly" => array(
                "default_value" => null,
            ),
            "mode" => array(
                "default_value" => null,

            ),
        );
        foreach ($this->f->get_initialized_parameters($params) as $key => $value) {
            ${$key} = $value;
        }
        //
        $standard_script_path = "../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
        $core_script_path = PATH_OPENMAIRIE."sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
        $gen_script_path = "../gen/sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
        //
        if (!isset($options)) {
            $options = array();
        }
        // Ce tableau permet a chaque application de definir des variables
        // supplementaires qui seront passees a l'objet metier dans le constructeur
        // a travers ce tableau
        // Voir le fichier dyn/form.get.specific.inc.php pour plus d'informations
        $extra_parameters = array();
        // surcharge globale
        if (file_exists('../dyn/tab.inc.php')) {
            require '../dyn/tab.inc.php';
        }
        // Inclusion du script [sql/<OM_DB_PHPTYPE>/<OBJ>.inc.php]
        $custom_script_path = $this->f->get_custom("tab", $obj);
        if ($custom_script_path !== null) {
            require $custom_script_path;
        } elseif (file_exists($standard_script_path) === false
            && file_exists($core_script_path) === true) {
            require $core_script_path;
        } elseif (file_exists($standard_script_path) === false
            && file_exists($gen_script_path) === true) {
            require $gen_script_path;
        } elseif (file_exists($standard_script_path) === true) {
            require $standard_script_path;
        }

        /**
         *
         */
        //
        if (!isset($om_validite) or $om_validite != true) {
            $om_validite = false;
        }
        if (!isset($options)) {
            $options = array();
        }
        $tb = $this->f->get_inst__om_table(array(
            "aff" => OM_ROUTE_TAB,
            "table" => $table,
            "serie" => $serie,
            "champAffiche" => $champAffiche,
            "champRecherche" => $champRecherche,
            "tri" => $tri,
            "selection" => $selection,
            "edition" => "",
            "options" => $options,
            "advs_id" => $advs_id,
            "om_validite" => $om_validite,
        ));
        $params = array(
            "obj" => $obj,
            "premier" => $premier,
            "tricol" => $tricol,
            "valide" => $valide,
            "advs_id" => $advs_id,
        );
        $params = array_merge($params, $extra_parameters);
        $tb->setParams($params);
        $tb->composeSearchTab();
        $tb->composeQuery();
        //
        require_once "../obj/edition_pdf__etiquette.class.php";
        $inst_edition_pdf = new edition_pdf__etiquette();
        $pdf_edition = $inst_edition_pdf->compute_pdf__etiquette(array(
            "obj" => "inscription_from_listing",
            "query" => $tb->sql,
        ));
        $this->expose_pdf_output($pdf_edition["pdf_output"], $pdf_edition["filename"]);
        return;
    }

    /**
     * VIEW - view_add_search_form.
     *
     * @return void
     */
    public function view_add_search_form() {
        $this->checkAccessibility();

        /**
         * Parametrage de la page
         */
        //
        $page = "inscription_search";
        //
        $onglet = __("Recherche de doublon");
        //
        $obj = "inscription";
        $ent = __("Saisie")." -> ".__("Nouvelle inscription");
        $description = __("Ce formulaire de recherche vous permet de faire une ".
                         "verification de doublon sur les inscriptions en cours et ".
                         "sur les electeurs de la liste electorale avant de pouvoir ".
                         "acceder au formulaire d'inscription. Pour effectuer la ".
                         "recherche, il vous faut saisir le nom patronymique et/ou ".
                         "la date de naissance de l'electeur a inscrire puis valider ".
                         "en cliquant sur le bouton. La case a cocher vous permet ".
                         "d'effectuer une recherche exacte sur le nom de l'electeur, ".
                         "si vous la decochez, la recherche s'effectuera seulement ".
                         "sur une partie du nom. Le bouton \"Retour\" vous permet de ".
                         "retourner au tableau de bord.");
        $bouton = __("Inscrire");
        //
        $action = OM_ROUTE_FORM."&obj=inscription&idx=0&action=101";

        /**
         * Initialisation des variables
         */
        // Initialisation des variables du formulaire
        $nom = "";
        $prenom = "";
        $datenaissance = "";
        $ine = "";
        $sexe = "";
        $choix_ine = "";
        $recherche_par = "";
        if ($this->f->getParameter('reu_sync_l_e_valid') == "done") {
            $recherche_par = "ine";
        }
        // Si les variables arrivent en $_GET
        if (isset($_GET['recherche_par'])) {
            (isset($_GET['recherche_par']) ? $recherche_par = $this->f->get_submitted_get_value("recherche_par") : $recherche_par = "ine");
            if ($recherche_par == "ine") {
                (isset($_GET['ine']) ? $ine = $this->f->get_submitted_get_value("ine") : $ine = "");
            } elseif ($recherche_par == "etat_civil" || $recherche_par == "") {
                (isset($_GET['nom']) ? $nom = $this->f->get_submitted_get_value('nom') : $nom = "");
                (isset($_GET['prenom']) ? $prenom = $this->f->get_submitted_get_value('prenom') : $prenom = "");
                (isset($_GET['exact']) && $_GET['exact']==true ? $exact = true : $exact = false);
                (isset($_GET['datenaissance']) ? $datenaissance = $_GET['datenaissance'] : $datenaissance = "");
                (isset($_GET['sexe']) ? $sexe = $_GET['sexe'] : $sexe = "");
            }
        }
        // Si les variables arrivent en $_POST
        if (isset($_POST['recherche_par'])) {
            (isset($_POST['recherche_par']) ? $recherche_par = $this->f->get_submitted_post_value("recherche_par") : $recherche_par = "ine");
            if ($recherche_par == "ine") {
                (isset($_POST['ine']) ? $ine = $this->f->get_submitted_post_value("ine") : $ine = "");
            } elseif ($recherche_par == "etat_civil" || $recherche_par == "") {
                (isset($_POST['nom']) ? $nom = $this->f->get_submitted_post_value('nom') : $nom = "");
                (isset($_POST['prenom']) ? $prenom = $this->f->get_submitted_post_value('prenom') : $prenom = "");
                (isset($_POST['exact']) && $_POST['exact']==true ? $exact = true : (isset($exact) ? $exact = $exact : $exact = false));
                (isset($_POST['datenaissance']) ? $datenaissance = $_POST['datenaissance'] : $datenaissance = "");
                (isset($_POST['sexe']) ? $sexe = $_POST['sexe'] : $sexe = "");
            }
            (isset($_POST['choix_ine']) ? $choix_ine = $this->f->get_submitted_post_value("choix_ine") : $choix_ine = "");
        }

        /// Decoche recherche exact si * detecte
        if (substr($nom,strlen($nom)-1,1) == '*') {
            $nom = str_replace("*","",$nom);
            if (strlen($nom) >= 1) $exact = false;
        }

        // Initialisation des variables du formulaire
        (!isset($exact) ? $exact = true : $exact = $exact);
        // Condition d'erreur
        $error_empty = false;
        if (($recherche_par == "etat_civil" && $nom == "" && $datenaissance == "")
            || ($recherche_par == "" && $nom == "" && $datenaissance == "")
            || ($recherche_par == "ine" && $ine == "")) {
            $error_empty = true;
        }
        $error_date = ($datenaissance != "" and $this->f->formatDate($datenaissance) == false ? true : false);
        $error_ine = false;
        if ($recherche_par == "ine" && (intval(str_replace(" ", "", $ine)) < 0 || intval(str_replace(" ", "", $ine)) > 2147483647)) {
            $error_ine = true;
        }

        /**
         * Validation du formulaire
         */
        //
        if (isset($_POST[$page.'_form_action_valid'])
            || (isset($_POST[$page.'_form_action_search']) && (
                $recherche_par == '' ||
                ($_POST[$page.'_form_action_search'] == 'inscrire')))) {
            //
            if (!$error_empty) {
                //
                $params = "nom=".urlencode($nom);
                $params .= "&prenom=".urlencode($prenom);
                $params .= "&sexe=".urlencode($sexe);
                $params .= ($exact == true ? "&exact=".$exact : "");
                $params .= "&datenaissance=".urlencode($datenaissance);
                $params .= "&recherche_par=".urlencode($recherche_par);
                $params .= "&ine=".urlencode($ine);
                $params .= "&choix_ine=".urlencode($choix_ine);
                //
                header("location:".OM_ROUTE_FORM."&obj=inscription&idx=0&action=102&".$params);
                return;
            }
        }

        /**
         * Parametrage du formulaire
         */
        //
        $validation = 0;
        $maj = 0;
        $champs = array("recherche_par", "nom", "exact", "prenom", "datenaissance", "sexe", "ine");
        //
        $form = $this->f->get_inst__om_formulaire(array(
            "validation" => $validation,
            "maj" => $maj,
            "champs" => $champs,
        ));
        $form->setLib("recherche_par", __("Recherche par"));
        $form->setType("recherche_par", "select");
        $form->setTaille("recherche_par", 10);
        $form->setMax("recherche_par", 10);
        $form->setVal("recherche_par", $recherche_par);
        $form->setSelect("recherche_par", array(
            0 => array("", "ine", "etat_civil", ),
            1 => array("", "INE", "État civil", ),
        ));
        //
        $form->setLib("nom", __("Nom patronymique"));
        $form->setType("nom", "text");
        $form->setTaille("nom", 40);
        $form->setMax("nom", 60);
        $form->setVal("nom", $nom);
        $form->setOnchange("nom", "this.value=this.value.toUpperCase()");
        //
        $form->setLib("prenom", __("prenom"));
        $form->setType("prenom", "text");
        $form->setTaille("prenom", 40);
        $form->setMax("prenom", 60);
        $form->setVal("prenom", $prenom);
        $form->setOnchange("prenom", "this.value=this.value.toUpperCase()");
        //
        $form->setLib("exact", __("Recherche exacte"));
        $form->setType("exact", "checkbox");
        $form->setTaille("exact", 3);
        $form->setMax("exact", 3);
        $form->setVal("exact", $exact);
        //
        $form->setLib("sexe", __("Sexe"));
        $form->setType("sexe", "select");
        $form->setTaille("sexe", 10);
        $form->setMax("sexe", 10);
        $form->setVal("sexe", $sexe);
        $form->setSelect("sexe", array(
            0 => array("", "M", "F", ),
            1 => array("", "M - Masculin", "F - Féminin", ),
        ));
        //
        $form->setLib("datenaissance", __("Date de Naissance"));
        $form->setType("datenaissance", "date");
        $form->setTaille("datenaissance", 10);
        $form->setMax("datenaissance", 10);
        $form->setVal("datenaissance", $datenaissance);
        $form->setOnchange("datenaissance", "fdate(this)");
        //
        $form->setLib("ine", __("INE"));
        $form->setType("ine", "text");
        $form->setTaille("ine", 40);
        $form->setMax("ine", 60);
        $form->setVal("ine", $ine);
        $form->setOnchange("ine", 'VerifNum(this)');
        //
        $form->setBloc("recherche_par", "DF", "", "first-fix-width");
        //
        $form->setBloc("nom", "D", "", "bloc-etat-civil");
        $form->setBloc("nom", "DF", "", "first-fix-width");
        $form->setBloc("prenom", "DF", "", "first-fix-width field-prenom");
        $form->setBloc("datenaissance", "DF", "", "first-fix-width");
        $form->setBloc("sexe", "DF", "", "first-fix-width field-sexe");
        $form->setBloc("sexe", "F");

        $form->setBloc("ine", "D", "", "bloc-ine");
        $form->setBloc("ine", "DF", "", "first-fix-width");
        $form->setBloc("ine", "F");

        // Affichage du message d'erreur si besoin
        if (isset($_POST[$page.'_form_action_search']) || isset($_POST[$page.'_form_action_valid'])) {
            //
            if ($error_empty) {
                $message_class = "error";
                $message = __("Vous devez saisir au moins un critere de recherche.");
                $this->f->displayMessage($message_class, $message);
            }
        }
        if ($error_date) {
            $message_class = "error";
            $message = __("La date de naissance n'est pas valide.");
            $this->f->displayMessage($message_class, $message);
        }
        if ($error_ine) {
            $message_class = "error";
            $message = __("Cet INE n'est pas valide.");
            $this->f->displayMessage($message_class, $message);
        }
        // Instructions et description du contenu de l'onglet
        $this->f->displayDescription($description);

        // Ouverture de la balise - Formulaire
        echo "\n<div id=\"".$page."\" class=\"formulaire\">\n";
        echo "<form method=\"post\" id=\"".$page."_form\" ";
        echo "name=\"".$page."_form\" ";
        echo "action=\"".$action."\">\n";

        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, $validation, false, false);
        $form->enpied();

        // Ouverture de la balise - Controles du formulaire
        echo "\t<div class=\"formControls\">\n";
        // Bouton
        echo "\t\t<input name=\"".$page."_form.action.search\" ";
        echo "value=\"Rechercher\" ";
        echo "type=\"submit\" class=\"boutonFormulaire om-search-button\" />\n";
        // Lien retour
        echo "<a class=\"retour\" title=\"".__("Retour")."\" ";
        echo "href=\"".OM_ROUTE_DASHBOARD."\">";
        echo __("Retour");
        echo "</a>";
        // Fermeture de la balise - Controles du formulaire
        echo "\t</div>\n";

        if (isset($_POST[$page.'_form_action_search'])
            && $error_empty === false
            && $error_date === false
            && $error_ine === false) {
            //
            printf('<div class="results"><h2>%s</h2>', _("Résultats de recherche"));
            //
            $do_not_display_results = false;
            $results = array();
            //
            $inst_reu = $this->f->get_inst__reu();
            if ($inst_reu == null) {
                $this->f->displayMessage(
                    "error",
                    __("Problème de configuration de la connexion au REU. Contactez votre administrateur.")
                );
            } else {
                if ($recherche_par == "ine") {
                    $ret = $inst_reu->handle_electeur(array(
                        "mode" => "search_by_ine",
                        "ine" => intval(str_replace(" ", "", $ine)),
                    ));
                    if ($ret["code"] == 401) {
                        $do_not_display_results = true;
                        if ($inst_reu->is_connexion_standard_valid() === false) {
                            $this->f->displayMessage(
                                "error",
                                sprintf(
                                    '%s : %s',
                                    __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                                    sprintf(
                                        '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                        __("cliquez ici pour vérifier")
                                    )
                                )
                            );
                        } else {
                            $this->f->displayMessage(
                                "error",
                                sprintf(__("Une erreur s'est produite. Contactez votre administrateur."))
                            );
                        }
                    } elseif ($ret["code"] == 204) {
                        $do_not_display_results = true;
                        $this->f->displayMessage(
                            "error",
                            sprintf('Aucun résultat pour l\'INE : %s.', $ine)
                        );
                    } elseif ($ret["code"] == 200) {
                        $results[] = gavoe($ret, array("result", ));
                    } else {
                        $do_not_display_results = true;
                        $this->f->displayMessage(
                            "error",
                            sprintf(__("Une erreur s'est produite. Contactez votre administrateur."))
                        );
                    }
                } elseif ($recherche_par == "etat_civil") {
                    $ret = $inst_reu->handle_electeur(array(
                        "mode" => "search_by_etat_civil",
                        "datas" => array(
                            "nom" => $this->f->get_submitted_post_value("nom"),
                            "prenom" => $this->f->get_submitted_post_value("prenom"),
                            "date_naissance" => $this->f->get_submitted_post_value("datenaissance"),
                            "sexe" => $this->f->get_submitted_post_value("sexe"),
                        )
                    ));
                    if ($ret["code"] == 401) {
                        $do_not_display_results = true;
                        if ($inst_reu->is_connexion_standard_valid() === false) {
                            $this->f->displayMessage(
                                "error",
                                sprintf(
                                    '%s : %s',
                                    __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                                    sprintf(
                                        '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                                        __("cliquez ici pour vérifier")
                                    )
                                )
                            );
                        } else {
                            $this->f->displayMessage(
                                "error",
                                sprintf(__("Une erreur s'est produite. Contactez votre administrateur."))
                            );
                        }
                    } elseif ($ret["code"] == 200) {
                        $presence_homonyme = gavoe($ret, array("result", "presenceHomonyme", ));
                        $electeur_results = gavoe($ret, array("result", "electeurRechercheViews", ));
                        if ($electeur_results != "") {
                            $results = $electeur_results;
                        }
                    } elseif ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                        $do_not_display_results = true;
                        $this->f->displayMessage(
                            "error",
                            $ret["result"]["message"]
                        );
                    } else {
                        $do_not_display_results = true;
                        $this->f->displayMessage(
                            "error",
                            sprintf(__("Une erreur s'est produite. Contactez votre administrateur."))
                        );
                    }
                }
                //
                if ($do_not_display_results === false) {
                    $table_content = sprintf(
                        '<table class="table table-bordered">
                            <tr>
                                <th></th>
                                <th>ine</th>
                                <th>nom patronymique</th>
                                <th>prénoms</th>
                                <th>sexe</th>
                                <th>date de naissance</th>
                                <th>commune de naissance</th>
                                <th>pays de naissance</th>
                            </tr>'
                    );
                    foreach ($results as $key => $value) {
                        $inputValue = implode(
                            ';',
                            array(
                                gavoe($value, array("etatCivil", "nomNaissance", )),
                                gavoe($value, array("etatCivil", "dateNaissance", )),
                                gavoe($value, array("numeroElecteur", ))
                            )
                        );
                        // Converti les valeurs envoyés en une chaîne html valide pour ne pas casser la page
                        // et s'assurer que le contenu de l'input sera correctement envoyé.
                        $inputValue = htmlentities($inputValue, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401);
                        $table_content .= sprintf(
                            '<tr>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                                <td>%s</td>
                            </tr>',
                            sprintf(
                                '<input type="radio" id="choix_ine" name="choix_ine" value=\'%s\'>',
                                $inputValue
                            ),
                            gavoe($value, array("numeroElecteur", )),
                            gavoe($value, array("etatCivil", "nomNaissance", )),
                            gavoe($value, array("etatCivil", "prenoms", )),
                            gavoe($value, array("etatCivil", "sexe", "code", )),
                            gavoe($value, array("etatCivil", "dateNaissance", )),
                            gavoe($value, array("etatCivil", "communeNaissance", "libelle", )),
                            gavoe($value, array("etatCivil", "paysNaissance", "libelle", ))
                        );
                    }
                    if ($recherche_par == "etat_civil") {
                        $inputValue = implode(
                            ';',
                            array(
                                $this->f->get_submitted_post_value('nom'),
                                $this->f->get_submitted_post_value('datenaissance'),
                                '-1'
                            )
                        );
                        // Converti les valeurs envoyés en une chaîne html valide pour ne pas casser la page
                        // et s'assurer que le contenu de l'input sera correctement envoyé.
                        $inputValue = htmlentities($inputValue, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401);
                        $table_content .= sprintf(
                            '<tr>
                                <td>%s</td>
                                <td colspan="7">%s</td>
                            </tr>',
                            sprintf(
                                '<input type="radio" id="choix_ine" name="choix_ine" value="%s">',
                                $inputValue
                            ),
                            "Saisie manuelle"
                        );
                    }
                    $table_content .= sprintf('</table>');
                    echo $table_content;

                    // Ouverture de la balise - Controles du formulaire
                    echo "\t<div class=\"formControls\">\n";
                    // Bouton
                    echo "\t\t<input name=\"".$page."_form.action.valid\" ";
                    echo "value=\"".$bouton."\" ";
                    echo "type=\"submit\" class=\"boutonFormulaire\" />\n";
                    // Fermeture de la balise - Controles du formulaire
                    echo "\t</div>\n";
                }
            }
            printf('</div>');
        }

        // Fermeture de la balise - Formulaire
        echo "</form>\n";
        echo "</div>\n";
    }

    /**
     * VIEW - view_add_search_form_results.
     *
     * @return void
     */
    public function view_add_search_form_results() {
        $this->checkAccessibility();

        /**
         * Initialisation des variables
         */
        // Initialisation des variables du formulaire
        $choix_ine = "";
        $datenaissance = "";
        $exact = true;
        $ine = "";
        $nom = "";
        $prenom = "";
        $recherche_par = "";
        $sexe = "";
        // Si les variables arrivent en $_GET
        if (isset($_GET['recherche_par'])) {
            // Initialisation des variables du formulaire
            (isset($_GET['choix_ine']) ? $choix_ine = $_GET['choix_ine'] : $choix_ine = "");
            (isset($_GET['datenaissance']) ? $datenaissance = $_GET['datenaissance'] : $datenaissance = "");
            (isset($_GET['exact']) && $_GET['exact']==true ? $exact = true : $exact = false);
            (isset($_GET['ine']) ? $ine = $this->f->get_submitted_get_value('ine') : $ine = "");
            (isset($_GET['nom']) ? $nom = $this->f->get_submitted_get_value('nom') : $nom = "");
            (isset($_GET['prenom']) ? $prenom = $this->f->get_submitted_get_value('prenom') : $prenom = "");
            (isset($_GET['recherche_par']) ? $recherche_par = $_GET['recherche_par'] : $recherche_par = "");
            (isset($_GET['sexe']) ? $sexe = $_GET['sexe'] : $sexe = "");
        }
        // Récupération des informations de l'électeur sélectionné
        if (! empty($choix_ine)) {
            // Permet de s'assurer qu'il n'y a pas de caractère encodé en html qui pourrait
            // casser le traitement par la suite
            $choix_ine = html_entity_decode($choix_ine, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401);
            $searchField = array('nom', 'datenaissance', 'ine');
            // Construction d'un tableau associatif avec les valeurs issues du
            // formulaire de sélection de l'électeur
            $searchValue = array_combine(
                $searchField,
                explode(';', $choix_ine)
            );
            // Remplissage des valeurs de recherche à partir des données issues
            // du choix de l'électeur
            foreach ($searchField as $field) {
                ${$field} = $searchValue[$field];
            }
        }
        //
        (isset($_GET['mode']) ? $mode = $_GET['mode'] : $mode = false);
        //
        $url = "nom=".urlencode($nom);
        $url .= "&amp;prenom=".urlencode($prenom);
        $url .= "&amp;recherche_par=".urlencode($recherche_par);
        $url .= "&amp;sexe=".urlencode($sexe);
        $url .= "&amp;ine=".urlencode($ine);
        $url .= "&amp;choix_ine=".urlencode($choix_ine);
        $url .= ($exact == true ? "&amp;exact=".$exact : "");
        $url .= "&amp;datenaissance=".urlencode($datenaissance);
        //
        $params = "nom=".urlencode($nom);
        $params .= "&prenom=".urlencode($prenom);
        $params .= "&recherche_par=".urlencode($recherche_par);
        $params .= "&sexe=".urlencode($sexe);
        $params .= "&ine=".urlencode($ine);
        $params .= "&choix_ine=".urlencode($choix_ine);
        $params .= ($exact == true ? "&exact=".$exact : "");
        $params .= "&datenaissance=".urlencode($datenaissance);

        /**
         * Validation du formulaire
         */
        //
        if ($recherche_par != "" && $choix_ine == "") {
            //
            header("location:".OM_ROUTE_FORM."&obj=inscription&idx=0&action=101&".$params);
            return;
        }

        /**
         * Validation du formulaire
         */
        //
        if (isset($_POST['inscription_doublon_form_action_valid'])) {
            //
            header("location:".OM_ROUTE_FORM."&obj=inscription&action=0&retour=form&".$params);
            return;
        }

        /**
         * Calcul des resultats de la recherche de doublon
         */
        //
        require "../sql/".OM_DB_PHPTYPE."/inscription_doublon.inc.php";

        //
        $res_doublon_electeur = $this->f->db->query($query_doublon_electeur);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query_doublon_electeur."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res_doublon_electeur);

        //
        $res_doublon_mouvement = $this->f->db->query($query_doublon_mouvement);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query_doublon_mouvement."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res_doublon_mouvement);

        // Si les recherches ne donnent aucun resultat
        if ($res_doublon_electeur->numrows() == 0
            && $res_doublon_mouvement->numrows() == 0) {
            //
            if ($mode != "cancel") {
                // Redirection vers le formulaire d'inscription
                header("location:".OM_ROUTE_FORM."&obj=inscription&action=0&retour=form&".$params);
                return;
            } else {
                // Redirection vers le formulaire de recherche
                header("location:".OM_ROUTE_FORM."&obj=inscription&idx=0&action=101&".$params);
                return;
            }
        }

        // Instructions et description du contenu de l'onglet
        $description = __("Voici la liste des electeurs deja presents dans la liste ".
                         "electorale ou ayant deja une inscription en cours avec un ".
                         "nom et/ou date de naissance identique a votre saisie. ".
                         "Maintenant, c'est a vous de realiser l'arbitrage : si ce ".
                         "n'est pas un doublon il faut cliquer sur le bouton ".
                         "\"Inscrire\" en bas de la page pour acceder au formulaire ".
                         "d'inscription ou alors si c'est effectivement un doublon ".
                         "il faut cliquer sur le bouton \"Retour\" pour revenir au ".
                         "formulaire de recherche initial.");
        $this->f->displayDescription($description);

        /**
         *
         */
        // Premier enregistrement a afficher
        (isset($_GET['premier']) ? $premier = $_GET['premier'] : $premier = 0);
        // Colonne choisie pour le tri
        (isset($_GET['tricol']) ? $tricol = $_GET['tricol'] : $tricol = "");
        $params = array(
            "premier" => $premier,
            "recherche" => "",
            "tricol" => "",
            "valide" => "",
            "choix_ine" => $choix_ine,
            "datenaissance" => $datenaissance,
            "ine" => $ine,
            "nom" => $nom,
            "prenom" => $prenom,
            "recherche_par" => $recherche_par,
            "sexe" => $sexe,
        );
        if ($exact == true) {
            $params["exact"] = true;
        }
        //
        $element_recherche = "";
        if ($nom != "" && $datenaissance != "") {
            $element_recherche .= " -> ".$nom ." ".__("ne(e) le")." -> ".$datenaissance."";
        } elseif ($datenaissance == "") {
            $element_recherche .= " -> ".$nom ."";
        } elseif ($nom == "") {
            $element_recherche .= __("ne(e) le")." -> ".$datenaissance."";
        }

        // Aucune action dans les listings
        $tab_actions = array(
            'corner' => array(),
            'left' => array(),
            'content' => array(),
            'specific_content' => array(),
        );

        //
        require "../sql/".OM_DB_PHPTYPE."/inscription_doublon_inscription.inc.php";
        //
        $tb = $this->f->get_inst__om_table(array(
            "aff" => OM_ROUTE_FORM."&obj=inscription&idx=0&action=102",
            "table" => $table,
            "serie" => $serie,
            "champAffiche" => $champAffiche,
            "champRecherche" => $champRecherche,
            "tri" => $tri,
            "selection" => $selection,
            "edition" => $edition,
            "options" => $options,
        ));
        $this->f->displaySubTitle($ent.$element_recherche);
        $tb->display($params, $tab_actions, $this->f->db, "tab", false);

        //
        require "../sql/".OM_DB_PHPTYPE."/inscription_doublon_electeur.inc.php";
        $tb = $this->f->get_inst__om_table(array(
            "aff" => OM_ROUTE_FORM."&obj=inscription&idx=0&action=102",
            "table" => $table,
            "serie" => $serie,
            "champAffiche" => $champAffiche,
            "champRecherche" => $champRecherche,
            "tri" => $tri,
            "selection" => $selection,
            "edition" => $edition,
            "options" => $options,
        ));
        $this->f->displaySubTitle($ent.$element_recherche);
        $tb->display($params, $tab_actions, $this->f->db, "tab", false);

        // Ouverture de la balise - Formulaire
        echo "\n<div id=\"inscription_doublon\" class=\"formulaire\">\n";
        echo "<form method=\"post\" id=\"inscription_doublon_form\" ";
        echo "name=\"inscription_doublon_form\" ";
        echo "action=\"".OM_ROUTE_FORM."&obj=inscription&idx=0&action=102&".$url."\">\n";

        // Ouverture de la balise - Controles du formulaire
        echo "\t<div class=\"formControls\">\n";
        // Bouton
        echo "\t\t<input name=\"inscription_doublon_form.action.valid\" ";
        echo "value=\"".__("Inscrire")."\" ";
        echo "type=\"submit\" class=\"boutonFormulaire\" />\n";
        // Lien retour
        echo "<a class=\"retour\" title=\"".__("Retour")."\" ";
        echo "href=\"".OM_ROUTE_FORM."&obj=inscription&idx=0&action=101&".$url."\">";
        echo __("Retour");
        echo "</a>";
        // Fermeture de la balise - Controles du formulaire
        echo "\t</div>\n";

        // Fermeture de la balise - Formulaire
        echo "</form>\n";
        echo "\t</div>\n";
    }

    /**
     * VIEW - view_edition_pdf__recepisseinscription.
     *
     * @return void
     */
    public function view_edition_pdf__recepisseinscription() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "recepisseinscription"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'recepisseinscription-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__attestationinscription.
     *
     * @return void
     */
    public function view_edition_pdf__attestationinscription() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "attestationinscription"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'attestationinscription-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     * VIEW - view_edition_pdf__refusinscription.
     *
     * @return void
     */
    public function view_edition_pdf__refusinscription() {
        $this->checkAccessibility();
        $pdfedition = $this->compute_pdf_output(
            "etat",
            "refusinscription"
        );
        $this->expose_pdf_output(
            $pdfedition["pdf_output"],
            sprintf(
                'refusinscription-%s-%s.pdf',
                $this->getVal($this->clePrimaire),
                date('YmdHis')
            )
        );
    }

    /**
     *
     */
    function setValFAjout($val = array()) {
        //
        $this->valF['electeur_id'] = 0;
        $this->valF['numero_bureau'] = 0;
        if (array_key_exists("provenance_demande", $val) === true
            && $val["provenance_demande"] !== "") {
            //
            $this->valF["provenance_demande"] = $val["provenance_demande"];
        } else {
            $this->valF["provenance_demande"] = "MAIRIE";
        }
    }

    /**
     *
     */
    function getDataSubmit() {
        //
        $datasubmit = parent::getDataSubmit();
        //
        if ($this->getParameter("maj") == 0) {
            $datasubmit .= "&amp;retour=".$this->getParameter("retour");
            $datasubmit .= "&amp;choix_ine=".$this->getParameter("choix_ine");
            $datasubmit .= "&amp;datenaissance=".$this->getParameter("datenaissance");
            $datasubmit .= "&amp;exact=".$this->getParameter("exact");
            $datasubmit .= "&amp;ine=".$this->getParameter("ine");
            $datasubmit .= "&amp;nom=".$this->getParameter("nom");
            $datasubmit .= "&amp;prenom=".$this->getParameter("prenom");
            $datasubmit .= "&amp;recherche_par=".$this->getParameter("recherche_par");
            $datasubmit .= "&amp;sexe=".$this->getParameter("sexe");
        }
        //
        return $datasubmit;
    }

    /**
     *
     */
    function bouton($maj) {
        //
        if (!$this->correct && $maj == 0 || $maj == 1) {
            echo "\t";
            echo "<a  class=\"om-prev-icon doublon-16\" href=\"javascript:doublon1()\"";
            echo " title=\"".__("Recherche de doublon sur nom, prenom(s), nom usage, date de naissance")."\"> ";
            echo __("Verification des doublons");
            echo "</a><br/>\n";
        }
        //
        parent::bouton($maj);
    }

    /**
     *
     */
    function retour($premier = 0, $recherche = "", $tricol = "") {
        //
        if ($this->getParameter("maj") != 0 || ($this->getParameter("maj") == 0
            && $this->correct && ($this->getParameter("nom") == NULL
            || $this->getParameter("nom") == ""))) {
            //
            parent::retour();
        } else {
            //
            echo "\n<a class=\"retour\" ";
            echo "href=\"";
            //
            if (!$this->correct) {
                //
                echo OM_ROUTE_FORM."&obj=inscription&idx=0&action=102";
                echo "&";
                echo "mode=cancel";
                echo "&amp;sexe=".urlencode($this->getParameter("sexe"));
                echo "&amp;ine=".urlencode($this->getParameter("ine"));
                echo "&amp;nom=".urlencode($this->getParameter("nom"));
                echo "&amp;prenom=".urlencode($this->getParameter("prenom"));
                echo "&amp;recherche_par=".urlencode($this->getParameter("recherche_par"));
                echo ($this->getParameter("exact")==true?"&amp;exact=".$this->getParameter("exact")."":"");
                echo "&amp;datenaissance=".urlencode($this->getParameter("datenaissance"));
            } else {
                //
                if ($this->getParameter("nom") != NULL && $this->getParameter("nom") != "") {
                    //
                    echo OM_ROUTE_TAB;
                    echo "&amp;obj=".get_class($this);
                    echo "&amp;recherche=".$this->getParameter("nom");
                }
            }
            //
            echo "\"";
            echo ">";
            //
            echo __("Retour");
            //
            echo "</a>\n";
        }
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        parent::setVal($form, $maj, $validation);
        // Si mode Ajout et premier affichage du formulaire
        if ($maj == 0 && $validation == 0) {
            $form->setVal('civilite', 'M.');
            // Si un électeur a été sélectionné on récupère son ine 
            if ($this->getParameter("choix_ine") != "") {
                $paramElecteur = array_combine(
                    array('nom', 'prenom', 'ine'),
                    explode(';', $this->getParameter("choix_ine"))
                );
            }
            // Si un électeur a été sélectionné, le paramétrage de l'électeur est appliqué
            if ($this->getParameter("choix_ine") != "" &&
                ! empty($paramElecteur['ine']) &&
                $paramElecteur['ine'] != '-1') {
                // Rempli le formulaire avec les informations de l'électeur
                $form->setVal('ine', $this->getParameter("ine"));
                // Récupère l'électeur par webservice
                $inst_reu = $this->f->get_inst__reu();
                if ($inst_reu == null) {
                    $this->addToMessage(__("Problème de configuration de la connexion au REU."));
                    return false;
                }
                $ret = $inst_reu->handle_electeur(array(
                    "mode" => "search_by_ine",
                    "ine" => intval($paramElecteur['ine']),
                ));
                if ($ret["code"] != "200") {
                    $this->f->displayMessage(
                        "error",
                        __("Impossible de récupérer l'électeur depuis le REU.")
                    );
                    return false;
                }
                //
                $row = array(
                    'numero_d_electeur' => gavoe($ret['result'], array('numeroElecteur', )),
                    'nom' => gavoe($ret['result'], array('etatCivil', 'nomNaissance', )),
                    'nom_d_usage' => gavoe($ret['result'], array('etatCivil', 'nomUsage', )),
                    'prenoms' => gavoe($ret['result'], array('etatCivil', 'prenoms', )),
                    'sexe' => gavoe($ret['result'], array('etatCivil', 'sexe', 'code', )),
                    'date_de_naissance' => gavoe($ret['result'], array('etatCivil', 'dateNaissance', )),
                    'code_commune_de_naissance' => gavoe($ret['result'], array('etatCivil', 'communeNaissance', 'code', )),
                    'libelle_commune_de_naissance' => gavoe($ret['result'], array('etatCivil', 'communeNaissance', 'libelle', )),
                    'code_pays_de_naissance' => gavoe($ret['result'], array('etatCivil', 'paysNaissance', 'code', )),
                    'libelle_pays_de_naissance' => gavoe($ret['result'], array('etatCivil', 'paysNaissance', 'libelle', )),
                    'code_nationalite' => gavoe($ret['result'], array('etatCivil', 'nationalite', 'code', )),
                    'libelle_nationalite' => gavoe($ret['result'], array('etatCivil', 'nationalite', 'libelle', )),
                );
                $form->setVal('nom', $row["nom"]);
                $form->setVal('prenom', $row["prenoms"]);
                $form->setVal('sexe', $row["sexe"]);
                $form->setVal('date_naissance', $this->f->handle_birth_date($row["date_de_naissance"]));
                if ($row["sexe"] == "F") {
                    $form->setVal('civilite', 'Mme');
                }
                $form->setVal('code_nationalite', $row["code_nationalite"]);
                // Lieu de naissance
                $oel_birth_place = $this->f->handle_birth_place(array(
                    "code_commune_de_naissance" => $row["code_commune_de_naissance"],
                    "libelle_commune_de_naissance" => $row["libelle_commune_de_naissance"],
                    "code_pays_de_naissance" => $row["code_pays_de_naissance"],
                    "libelle_pays_de_naissance" => $row["libelle_pays_de_naissance"],
                ));
                $form->setVal('code_departement_naissance', $oel_birth_place["code_departement_naissance"]);
                $form->setVal('libelle_departement_naissance', $oel_birth_place["libelle_departement_naissance"]);
                $form->setVal('code_lieu_de_naissance', $oel_birth_place["code_lieu_de_naissance"]);
                $form->setVal('libelle_lieu_de_naissance', $oel_birth_place["libelle_lieu_de_naissance"]);
                $naissance_type_saisie = $this->get_naissance_type_saisie(
                    $oel_birth_place["code_departement_naissance"],
                    $oel_birth_place["libelle_departement_naissance"],
                    $oel_birth_place["code_lieu_de_naissance"],
                    $oel_birth_place["libelle_lieu_de_naissance"]
                );
                $form->setVal('naissance_type_saisie', $naissance_type_saisie);
            } else {
                // Paramétrage par défaut
                if ($this->getParameter("nom") != "") {
                    $form->setVal('nom', $this->getParameter("nom"));
                }
                if ($this->getParameter("prenom") != "") {
                    $form->setVal('prenom', $this->getParameter("prenom"));
                }
                if ($this->getParameter("sexe") != "") {
                    $form->setVal('sexe', $this->getParameter("sexe"));
                }
                if ($this->getParameter("datenaissance") != "" && $this->getParameter("datenaissance") != null) {
                    $form->setVal('date_naissance', $this->dateDB($this->getParameter("datenaissance")));
                }
                if ($this->getParameter("sexe") == "F") {
                    $form->setVal('civilite', 'Mme');
                }
            }
            // Si nous sommes sur la liste generale on positionne la
            // nationalite francaise par defaut
            if ($_SESSION["liste"] == "01") {
                $form->setVal('code_nationalite', 'FR');
            }
            $form->setVal('bureauforce', 'Non');
            $form->setVal('liste', $_SESSION["liste"]);
            $form->setVal('date_tableau', $this->f->getParameter("datetableau"));
            $form->setVal('tableau', "annuel");
            $form->setVal('etat', "actif");
        }
    }

    /**
     *
     */
    function setValFNumeroBureau() {
        //
        $this->valF['numero_bureau'] = 0;
    }

    /**
     * TREATMENT - apply.
     *
     * Ce traitement permet d'appliquer le mouvement sur la liste électorale.
     *
     * @return boolean
     */
    function apply($val = array()) {
        $this->begin_treatment(__METHOD__);
        // Il y a deux modes :
        // - soit nous sommes sur une instance d'une inscription existante
        //   et on applique donc le traitement sur celle là
        // - soit on reçoit une liste d'identifiants d'inscriptions à traiter
        //   et on applique donc le traitement sur celles là
        $inscriptions_ids = array();
        if ($this->exists() === true) {
            $inscriptions_ids = array($this->getVal($this->clePrimaire), );
        } elseif (array_key_exists("inscriptions_ids", $val) === true
            && is_array($val["inscriptions_ids"]) === true) {
            $inscriptions_ids = $val["inscriptions_ids"];
        }
        $nb_inscriptions_to_apply = count($inscriptions_ids);
        if ($nb_inscriptions_to_apply == 0) {
            $this->correct = false;
            $this->addToLog("Mauvais paramètres. Aucune inscription à appliquer.", DEBUG_MODE);
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // On récupère toutes les informations dont on a besoin sur le ou les
        // mouvements pour pouvoir appliquer le traitement
        $query_select_inscription = sprintf(
            'SELECT
                mouvement.id as mouvement_id,
                mouvement.bureau_de_vote_code as bureau_code,
                mouvement.*
            FROM
                %1$smouvement
                INNER JOIN %1$sparam_mouvement ON mouvement.types=param_mouvement.code
            WHERE
                lower(param_mouvement.typecat)=\'inscription\'
                AND mouvement.etat=\'actif\'
                AND mouvement.id IN (%2$s)',
            DB_PREFIXE,
            implode(',', $inscriptions_ids)
        );
        $res_select_inscription = $this->f->db->query($query_select_inscription);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$query_select_inscription."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res_select_inscription, true)) {
            $this->correct = false;
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        if ($nb_inscriptions_to_apply != $res_select_inscription->numrows()) {
            $this->correct = false;
            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        //
        while ($row =& $res_select_inscription->fetchRow(DB_FETCHMODE_ASSOC)) {
            // Gestion des LC2
            if ($row["liste"] == "04") {
                $message = "-> Cas particulier LC2";
                $this->addToMessage($message);
                $listes = array("02", "03", );
            } else {
                $listes = array($row["liste"], );
            }
            foreach ($listes as $liste) {
                $electeur_id = $this->get_electeur_id_by_ine_and_list(
                    $row['ine'],
                    $liste
                );
                if ($electeur_id === false) {
                    // ajout ELECTEUR
                    $enr = $this->f->get_inst__om_dbform(array(
                        "obj" => "electeur",
                        "idx" => "]",
                    ));
                    $enr->valF['tableau'] = 'annuel';
                    $row["liste"] = $liste;
                    $ret = $enr->ajouterTraitement($row, $row["date_tableau"]);
                    if ($ret !== true) {
                        $this->correct = false;
                        $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
                        return $this->end_treatment(__METHOD__, false);
                    }
                    $electeur_id = $enr->valF["id"];
                    $enr->init_record_data($enr->valF[$enr->clePrimaire]);
                    // Mise à jour de l'état civil
                    $enr->handle_modec(array(), false);
                    // Mise à jour du numéro d'ordre
                    if ($row['statut'] === 'vise_maire') {
                        $enr->update_numero_bureau_oel_from_reu();
                    }
                } else {
                    // Dans le cas d'une mise à jour de l'électeur seulement
                    // dans sa liste LCE ou LCM, il faut que l'électeur soit également
                    // modifié dans l'autre liste
                    $el_ids = $this->get_electeurs_with_same_ine($row['ine']);
                    foreach ($el_ids as $el_id) {
                        $enr = $this->f->get_inst__om_dbform(array(
                            "obj" => "electeur",
                            "idx" => $el_id,
                        ));
                        // Récupère les informations de l'électeur
                        $val = array();
                        foreach ($enr->champs as $champ) {
                            $val[$champ] = $enr->getVal($champ);
                        }
                        // Met à jour l'adresse, le numéro de bureau et les informations de contact
                        $val['bureau'] = $row['bureau'];
                        $val['ancien_bureau'] = $row['ancien_bureau'];
                        $val['bureauforce'] = $row['bureauforce'];
                        $val['numero_bureau'] = $row['numero_bureau'] !== '' && $row['numero_bureau'] !== 0 ? $row['numero_bureau'] : null;
                        $val['code_voie'] = $row['code_voie'];
                        $val['libelle_voie'] = $row['libelle_voie'];
                        $val['numero_habitation'] = $row['numero_habitation'];
                        $val['complement_numero'] = $row['complement_numero'] ;
                        $val['complement'] = $row['complement'] ;
                        $val['resident'] = $row['resident'];
                        $val['adresse_resident'] = $row['adresse_resident'];
                        $val['complement_resident'] = $row['complement_resident'];
                        $val['cp_resident'] = $row['cp_resident'] ;
                        $val['ville_resident'] = $row['ville_resident'] ;
                        $val['date_modif'] = $row['date_modif'];
                        $val['utilisateur'] = $row['utilisateur'];
                        $val['types'] = $row['types'];
                        $val['date_mouvement'] = $this->dateSystemeDB();
                        $val['telephone'] = $row['telephone'];
                        $val['courriel'] = $row['courriel'];
                        $val['om_collectivite'] = $row['om_collectivite'];
                        $ret = $enr->modifierTraitement($val, $this->f->getParameter("datetableau"));
                        if ($ret !== true) {
                            $this->correct = false;
                            $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
                            return $this->end_treatment(__METHOD__, false);
                        }
                        // Mise à jour de l'état civil
                        $enr->init_record_data($enr->getVal($enr->clePrimaire));
                        $enr->handle_modec(array(), false);
                        // Mise à jour du numéro d'ordre
                        if ($row['statut'] === 'vise_maire') {
                            $enr->update_numero_bureau_oel_from_reu();
                        }
                    }
                }
            }
            // maj MOUVEMENT
            $fields_values = array(
                'etat'    => 'trs',
                'tableau' => 'annuel',
                'electeur_id' => $electeur_id,
                'date_j5' => ''.$enr->dateSystemeDB().'',
            );
            $cle = "id=".$row['mouvement_id'];
            //
            $res1 = $this->f->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, "mouvement"),
                $fields_values,
                DB_AUTOQUERY_UPDATE,
                $cle
            );
            $this->f->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "mouvement")."\", ".print_r($fields_values, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                VERBOSE_MODE
            );
            if ($this->f->isDatabaseError($res1, true) !== false) {
                $this->correct = false;
                $this->addToMessage("Une erreur est survenue. Contactez votre administrateur.");
                return $this->end_treatment(__METHOD__, false);
            }
        }
        $res_select_inscription->free();
        if ($this->exists() === true) {
            // Gestion de l'historique
            $val_histo = array(
                'action' => 'apply',
                'message' => __("Mouvement appliqué sur la liste électorale.")
            );
            $histo = $this->handle_historique($val_histo);
            if ($histo !== true) {
                $this->correct = false;
                $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $this->addToMessage(_("L'inscription a été appliquée correctement sur l'électeur."));
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - On transmet le mouvement au REU
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Transmission REU
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done"
            && ($val['id_demande'] === '' || $val['id_demande'] === null)
            && (array_key_exists('origin', $val) === false || $val['origin'] !== 'reu_regularisation')) {
            //
            $ret = $this->handle_reu_demande_inscription("add", $id, $val);
            if ($ret !== true) {
                return $ret;
            }
        }
        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - On transmet le mouvement au REU
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Transmission REU
        if ($this->f->getParameter("reu_sync_l_e_valid") == "done"
            && ($val['id_demande'] !== '' && $val['id_demande'] !== null)
            && $this->getVal('statut') !== "vise_insee"
            && $this->getVal('statut') !== "vise_maire") {
            //
            $ret = $this->handle_reu_demande_inscription("edit", $id, $val);
            if ($ret !== true) {
                return $ret;
            }
        }
        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function handle_reu_demande_inscription($mode, $id, $val) {
        //
        $inst_liste = $this->f->get_inst__om_dbform(array(
            "obj" => "liste",
            "idx" => $val['liste'],
        ));
        $inst_bureau = $this->f->get_inst__om_dbform(array(
            "obj" => "bureau",
            "idx" => $val['bureau'],
        ));
        if ($inst_bureau->getVal("referentiel_id") == "") {
            $this->addToMessage("Erreur de paramétrage des bureaux de vote. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        $inst_voie = $this->f->get_inst__om_dbform(array(
            "obj" => "voie",
            "idx" => $val['code_voie'],
        ));
        $date_demande = $val["date_demande"];
        if ($date_demande != "") {
            $date_demande = $this->f->formatDate($val["date_demande"], true);
        }
        //
        $datas = array(
            //
            "libelle_commune" => $this->f->getParameter("ville"),
            //
            "mouvement_id" => $id,
            "date_demande" => $date_demande,
            //
            "nom_patronymique" => $val["nom"],
            "nom_usage" => $val["nom_usage"],
            "prenoms" => $val["prenom"],
            "sexe" => $val["sexe"],
            "nationalite" => $val["code_nationalite"],
            "date_de_naissance" => $this->f->formatDate($val["date_naissance"], true),
            //
            "motif" => $val["types"],
            "liste" => $inst_liste->getVal("liste_insee"),
            "bureau" => $inst_bureau->getVal("referentiel_id"),
            //
            "observations" => $val["observation"],
            //
            "adresse_rattachement" => array(
                "numero" => $val["numero_habitation"],
                "complement_numero" => $val["complement_numero"],
                "voie" => $inst_voie->getVal("libelle_voie"),
                "complement" => $val["complement"],
                "code_postal" => $inst_voie->getVal("cp"),
                "ville" => $inst_voie->getVal("ville"),
            ),
            //
            "courriel" => $val["courriel"],
            "telephone" => $val["telephone"],
        );
        if ($val["resident"] == "Oui") {
            $datas["adresse_contact"] = array(
                "adresse" => $val["adresse_resident"],
                "complement" => $val["complement_resident"],
                "code_postal" => $val["cp_resident"],
                "ville" => $val["ville_resident"],
            );
        }
        //
        $provenance_code_commune = "";
        $provenance_libelle_commune = "";
        if ($val["provenance"] != "") {
            $inst_commune = $this->f->get_inst__om_dbform(array(
                "obj" => "commune",
                "idx" => $val["provenance"]
            ));
            $provenance_code_commune = str_replace(" ", "", $val["provenance"]);
            $provenance_libelle_commune = $inst_commune->getVal("libelle_commune");
        }
        $datas["provenance"] = array(
            "code_commune" => $provenance_code_commune,
            "libelle_commune" => $provenance_libelle_commune,
        );
        //
        if ($this->valF["ine"] != "") {
            $datas["ine"] = $this->valF["ine"];
        }
        //
        $referentiel_communes = $this->f->get_all_results_from_db_query(sprintf(
            'SELECT * FROM %1$sreu_referentiel_commune WHERE code=\'%2$s\'',
            DB_PREFIXE,
            str_replace(" ", "", $this->valF["code_lieu_de_naissance"])
        ));
        if (count($referentiel_communes['result']) >= 1) {
            $code_commune_de_naissance = gavoe($referentiel_communes['result'], array("0", "code"));
            $libelle_lieu_de_naissance = gavoe($referentiel_communes['result'], array("0", "libelle"));
            $code_pays_de_naissance = "";
            $libelle_pays_de_naissance = "";
        } else {
            $referentiel_pays = $this->f->get_all_results_from_db_query(sprintf(
                'SELECT * FROM %1$sreu_referentiel_pays WHERE code=\'%2$s\'',
                DB_PREFIXE,
                str_replace(" ", "", $this->valF["code_lieu_de_naissance"])
            ));
            if (count($referentiel_pays['result']) >= 1) {
                $code_commune_de_naissance = "";
                $libelle_lieu_de_naissance = $val['libelle_lieu_de_naissance'];
                $code_pays_de_naissance = gavoe($referentiel_pays['result'], array("0", "code"));
                $libelle_pays_de_naissance = gavoe($referentiel_pays['result'], array("0", "libelle"));
            } else {
                //
                $naissance_type_saisie = $this->get_naissance_type_saisie(
                    $this->valF["code_departement_naissance"],
                    $this->valF["libelle_departement_naissance"],
                    $this->valF["code_lieu_de_naissance"],
                    $this->valF["libelle_lieu_de_naissance"]
                );
                if ($naissance_type_saisie == "France") {
                    $inst_commune = $this->f->get_inst__om_dbform(array(
                        "obj" => "commune",
                        "idx" => $this->valF["code_lieu_de_naissance"]
                    ));
                    $code_commune_de_naissance = str_replace(" ", "", $this->valF["code_lieu_de_naissance"]);
                    $libelle_lieu_de_naissance = $inst_commune->getVal("libelle_commune");
                    $code_pays_de_naissance = "";
                    $libelle_pays_de_naissance = "";
                } else {
                    $code_commune_de_naissance = "";
                    $libelle_lieu_de_naissance = $this->valF["libelle_lieu_de_naissance"];
                    $code_pays_de_naissance = $this->valF["code_departement_naissance"];
                    $libelle_pays_de_naissance = $this->valF["libelle_departement_naissance"];
                }
            }
        }
        //
        if ($code_commune_de_naissance != "") {
            $datas["code_commune_de_naissance"] = $code_commune_de_naissance;
        }
        if ($libelle_lieu_de_naissance != "") {
            $datas["libelle_commune_de_naissance"] = $libelle_lieu_de_naissance;
        }
        if ($code_pays_de_naissance != "") {
            $datas["code_pays_de_naissance"] = $code_pays_de_naissance;
        }
        if ($libelle_pays_de_naissance != "") {
            $datas["libelle_pays_de_naissance"] = $libelle_pays_de_naissance;
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        $ret = $inst_reu->handle_inscription(array(
            "mode" => $mode,
            "id" => $this->getVal("id_demande"),
            "datas" => $datas,
        ));
        if (isset($ret["code"]) !== true
            || ($mode == "add" && $ret["code"] != 201)
            || ($mode == "edit" && $ret["code"] != 200)) {
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            return $this->end_treatment(__METHOD__, false);
        }
        $valF = array();
        if ($mode == "add") {
            $valF = array(
                "id_demande" => trim($ret["headers"]["X-App-params"]),
                "statut" => "ouvert",
            );
            $ret = $this->update_autoexecute($valF, $id, false);
            if ($ret !== true) {
                return $this->end_treatment(__METHOD__, false);
            }
        }
        // Gestion de l'historique du mouvement
        $val_histo = array(
            'id' => $id,
            'action' => __METHOD__,
            'message' => __('Transmission au REU.'),
            'datas' => $valF,
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->addToMessage("Erreur lors de la mise à jour de l'historique du mouvement.");
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - attendre.
     *
     * @return boolean
     */
    function attendre($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        $inst_reu = $this->f->get_inst__reu();
        $method = sprintf('handle_%s', $this->typeCat);
        $ret = $inst_reu->$method(array(
            "mode" => "attendre",
            "id" => $this->getVal("id_demande"),
        ));
        if (isset($ret["code"]) !== true || $ret["code"] != 200) {
            $this->correct = false;
            $this->addToMessage("Erreur de transmission au REU.");
            if ($ret["code"] == 400 && isset($ret["result"]["message"])) {
                $this->addToMessage($ret["result"]["message"]);
            }
            if ($ret["code"] == 401) {
                if ($inst_reu->is_connexion_standard_valid() === false) {
                    $this->addToMessage(sprintf(
                        '%s : %s',
                        __("Vous n'êtes pas connecté au Répertoire Électoral Unique"),
                        sprintf(
                            '<a href="#" onclick="load_dialog_userpage();">%s</a>',
                            __("cliquez ici pour vérifier")
                        )
                    ));
                }
            }
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $valF = array(
            "statut" => "en_attente",
        );
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => 'attendre',
            'message' => sprintf(
                __('Action %1$s %2$s effectuée.'),
                504,
                $this->get_action_param(504, 'identifier')
            ),
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(__("L'inscription a été correctement mise en attente."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - refuser.
     *
     * @return boolean
     */
    function refuser($val = array()) {
        $this->begin_treatment(__METHOD__);
        //
        $valF = array(
            "statut" => "refuse",
            "etat" => "na",
            "vu" => false,
        );
        $ret = $this->update_autoexecute($valF, null, false);
        if ($ret !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        // Gestion de l'historique
        $val_histo = array(
            'action' => 'refuser',
            'message' => __("Méthode 'refuser' appliquée."),
        );
        $histo = $this->handle_historique($val_histo);
        if ($histo !== true) {
            $this->correct = false;
            return $this->end_treatment(__METHOD__, false);
        }
        //
        return $this->end_treatment(__METHOD__, true);
    }
}
