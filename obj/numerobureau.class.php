<?php
/**
 * Ce script définit la classe 'numerobureau'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../gen/obj/numerobureau.class.php";

/**
 * Définition de la classe 'numerobureau' (om_dbform).
 */
class numerobureau extends numerobureau_gen {

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau() {
        return $this->get_common_var_sql_forminc__sql_bureau();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_bureau_by_id() {
        return $this->get_common_var_sql_forminc__sql_bureau_by_id();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste() {
        return $this->get_common_var_sql_forminc__sql_liste_officielle();
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_liste_by_id() {
        return $this->get_common_var_sql_forminc__sql_liste_by_id();
    }

    /**
     * Paramétrage des VIEW formulaire & sousformulaire - setType.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj == 1) {
            $form->setType("bureau", "selecthiddenstatic");
            $form->setType("liste", "selecthiddenstatic");
        }
    }

    /**
     *
     */
    function nextDernierNumero($bureau = null, $liste = null, $om_collectivite = null) {
        //
        if ($bureau != null
            && $liste != null
            && $om_collectivite != null) {
            $key = sprintf(
                'numerobureau.bureau=%s AND numerobureau.liste=\'%s\' AND numerobureau.om_collectivite=%s',
                intval($bureau),
                $this->f->db->escapeSimple($liste),
                intval($om_collectivite)
            );
            $query = sprintf(
                'SELECT id FROM %1$snumerobureau WHERE %2$s',
                DB_PREFIXE,
                $key
            );
            $id = $this->f->db->getone($query);
            $this->addToLog(
                __METHOD__."(): db->query(\"".$query."\");",
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($id);
            $inst__numerobureau = $this->f->get_inst__om_dbform(array(
                "obj" => "numerobureau",
                "idx" => $id,
            ));
        } else {
            $inst__numerobureau = $this;
        }
        $valF['dernier_numero'] = $inst__numerobureau->getVal("dernier_numero") + 1;
        //
        $res = $inst__numerobureau->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, $inst__numerobureau->table),
            $valF,
            DB_AUTOQUERY_UPDATE,
            $inst__numerobureau->getCle($id)
        );
        $inst__numerobureau->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, $inst__numerobureau->table)."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$inst__numerobureau->getCle($id)."\");",
            VERBOSE_MODE
        );
        $inst__numerobureau->f->isDatabaseError($res);
        // Log
        $inst__numerobureau->addToLog(__("Requete executee"), VERBOSE_MODE);
        // Log
        $message = __("Enregistrement")."&nbsp;".$id."&nbsp;";
        $message .= __("de la table")."&nbsp;\"".$inst__numerobureau->table."\"&nbsp;";
        $message .= "[&nbsp;".$inst__numerobureau->f->db->affectedRows()."&nbsp;";
        $message .= __("enregistrement(s) mis a jour")."&nbsp;]";
        $inst__numerobureau->addToLog($message, VERBOSE_MODE);
        //
        return $valF['dernier_numero'];
    }
}
