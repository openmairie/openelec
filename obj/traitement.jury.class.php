<?php
/**
 * Ce script définit la classe 'juryTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'juryTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class juryTraitement extends traitement {

    var $fichier = "jury";
    public $jury = 1;
    public $valid_button_value = "Tirage aleatoire";
    public $description = "Attention, lancer le traitement efface la sélection précédente stockée dans le système.";


    function __construct() {
        //
        $this->init_om_application();
        //
        $this->LogToFile ("start traitement par ".$_SESSION['login']);
        $this->LogToFile ("collectivite en cours: ".$_SESSION['libelle_collectivite']."[".$_SESSION["collectivite"]."]");
        $this->LogToFile ("liste en cours: ".$_SESSION["libelle_liste"]."[".$_SESSION["liste"]."]");
        $this->setLogFile ();
    }

    function setValidButtonValue(string $displayed_button_text) {
        $this->valid_button_value = $displayed_button_text;
    }

    function getValidButtonValue() {
        return $this->valid_button_value;
    }

    function setDescription(string $displayed_desc_text) {
        $this->description = $displayed_desc_text;
    }

    function getDescription() {
        return $this->description;
    }

    function setJury(int $jury) {
        $this->jury = $jury;
    }

    function setFichier($fichier) {
        $this->fichier = $fichier;
    }

    /**
     * Cette requête permet de compter tous les électeurs de la table 'electeur' en
     * fonction de la collectivité en cours et de la liste en cours de l'utilisateur
     * connecté.
     *
     * @param string $_SESSION["collectivite"]
     * @param string $_SESSION["liste"]
     */
    function count_electeur() {
        $query_count_electeur = sprintf(
            'SELECT
                count(*)
            FROM
                %1$selecteur
            WHERE
                electeur.liste=\'%3$s\'
                AND electeur.om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"]),
            $_SESSION["liste"]
        );
        $electeurs = $this->f->db->getone($query_count_electeur);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$query_count_electeur."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($electeurs);
        return $electeurs;
    }

    function displayBeforeContentForm() {
        include "../sql/".OM_DB_PHPTYPE."/trt_jury.inc.php";
        // Récupération du nombre d'électeur total appartenanr à la collectivité et à la liste
        // de l'utilisateur
        $electeurs = $this->count_electeur();
        // Récupération du paramétrage de tirage au sort des jurés par canton
        $qres = $this->f->get_all_results_from_db_query($query_nb_jures_canton);
        // Html permettant d'afficher le paramétrage par canton sous forme de liste
        $html_list_param_canton = implode("\n",
            array_map(function ($canton) {
                return sprintf(
                    __('<li> Canton %s - %s : %d</li>'),
                    $canton['canton_code'],
                    $canton['canton_libelle'],
                    $canton['nb_jures']
                );
            }, $qres['result']));
        // Affichage du contenu du formulaire de traitement des jurés
        printf(
            '<p>%s %s %s %d</p>
            <br/>
            <p>%s</p>
            <ul class="alt">
                %s
            </ul>',
            __("Le nombre d'electeurs dans la liste a la date du"),
            date('d/m/Y'),
            __("est de"),
            $electeurs,
            __("Electeur(s) a selectionner pour la liste preparatoire :"),
            $html_list_param_canton
        );
    }

    function treatment () {
        //
        $this->LogToFile("start jury");
        //
        include "../sql/".OM_DB_PHPTYPE."/trt_jury.inc.php";

        $valF = array("jury" => 0);
        $cle = sprintf(
            "%s
            electeur.om_collectivite = %d",
            ($this->jury === 2 ?
                "-- Réinitialisation de la sélection des jurés suppléants
                electeur.jury = '2' AND" :
                "-- Réinitialisation de la selection des jurés titulaires et suppléants"),
            intval($_SESSION["collectivite"])
        );

        $res = $this->f->db->autoexecute(
            sprintf('%1$s%2$s', DB_PREFIXE, "electeur"),
            $valF,
            DB_AUTOQUERY_UPDATE,
            $cle
        );
        $this->f->addToLog(
            __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "electeur")."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
            VERBOSE_MODE
        );
        // Vérification et log des erreurs de l'update
        if ($this->f->isDatabaseError($res, true)) {
            //
            $this->error = true;
            //
            $message = $res->getMessage()." - ".$res->getUserInfo();
            $this->LogToFile($message);
            //
            $this->addToMessage(__("Contactez votre administrateur."));
        } else {
            //
            $this->addToLog(__METHOD__."(): ".$this->f->db->affectedRows()." ".__("electeur(s) jure(s) supprime(s)"), EXTRA_VERBOSE_MODE);
            //
            $message = $this->f->db->affectedRows()." ".__("electeur(s) jure(s) supprime(s)");
            $this->LogToFile($message);
            // Couple(s) Canton/Nb Jures pour la collectivite en cours
            $res = $this->f->db->query($query_nb_jures_canton);
            $this->addToLog(__METHOD__."(): db->query(\"".$query_nb_jures_canton."\");", VERBOSE_MODE);
            //
            if ($this->f->isDatabaseError($res, true)) {
                //
                $this->error = true;
                //
                $message = $res->getMessage()." - ".$res->getUserInfo();
                $this->LogToFile($message);
                //
                $this->addToMessage(__("Contactez votre administrateur."));
            } else {
                //
                $where = "";
                $nb_jures_total = 0;
                $nb_cantons = 0;
                //
                while($row = $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                    //
                    $njury = intval($row["nb_jures"]);
                    //Si le nombre d'électeur est à zéro on arrête le traitement.
                    if ($njury == 0) {
                        $this->error = true;
                        $this->addToMessage('Le nombre d\'électeurs à tirer au sort est à 0.');
                        return;
                    }
                    //
                    $canton_id = $row["canton_id"];
                    $canton_code = $row["canton_code"];
                    $nb_jures_total += $njury;
                    $nb_cantons += 1;
                    //
                    $message = __("CANTON")." ".$canton_code;
                    $this->LogToFile($message);
                    $message = $njury." ".__("electeur(s) a selectionner");
                    $this->LogToFile($message);
                    // Pour les jurés supplémentaire vérifie qu'un titulaire n'est pas re-selectionné
                    $whereJurySup = $this->jury === 2 ?
                        'AND electeur.jury = 0' :
                        '';
                    // Tirage
                    $selectj = sprintf(
                        'SELECT
                            electeur.id,
                            nom,
                            nom_usage,
                            prenom
                        FROM
                            %1$selecteur
                            LEFT JOIN %1$sbureau
                                ON electeur.bureau=bureau.id
                        WHERE
                            liste=\'%3$s\'
                            AND bureau.canton=%4$s
                            AND ((date_part(\'year\', age(date_naissance)) >= 23
                                    AND date_jeffectif IS NULL)
                                OR (date_jeffectif IS NOT NULL
                                    AND date_part(\'year\',age(date_jeffectif))>=5))
                            AND electeur.om_collectivite=%2$s
                            %6$s
                        ORDER BY RANDOM()
                        LIMIT %5$s',
                        DB_PREFIXE,
                        intval($_SESSION["collectivite"]),
                        $_SESSION["liste"],
                        $canton_id,
                        $njury,
                        $whereJurySup
                    );
                    $res_selectj = $this->f->db->query($selectj);
                    $this->f->addToLog(
                        __METHOD__."(): db->query(\"".$selectj."\");",
                        VERBOSE_MODE
                    );
                    if ($this->f->isDatabaseError($res_selectj, true)) {
                        //
                        $this->error = true;
                        //
                        $message = $res_selectj->getMessage()." - ".$res_selectj->getUserInfo();
                        $this->LogToFile($message);
                        //
                        $this->addToMessage(__("Contactez votre administrateur."));
                    } else {
                        //Mettre en jury=1
                        while ($rowj=$res_selectj->fetchRow(DB_FETCHMODE_ASSOC) ) {
                            //
                            $where .= " electeur.id='".$rowj['id']."' or ";
                            //
                            $message = $rowj['nom']." ".$rowj['prenom']." ".$rowj ['nom_usage']." => ".__("selectionne comme jure d'assises");
                            $this->LogToFile($message);
                        }
                    }
                }
                //
                $where = substr($where, 0, strlen($where) - 4);
                //
                $valF = array("jury" => $this->jury);
                $cle = "(".$where.") and electeur.om_collectivite=".intval($_SESSION["collectivite"])."";
                //
                $res = $this->f->db->autoexecute(
                    sprintf('%1$s%2$s', DB_PREFIXE, "electeur"),
                    $valF,
                    DB_AUTOQUERY_UPDATE,
                    $cle
                );
                $this->f->addToLog(
                    __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "electeur")."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                //
                $message = $this->f->db->affectedRows()."/".$nb_jures_total." ".__("electeur(s) selectionne(s) sur")." ".$nb_cantons." ".__("canton(s)");
                $this->LogToFile($message);
                $this->addToMessage($message);
            }
        }
        //
        $this->LogToFile("end jury");
    }
}
