<?php
/**
 * Ce script définit la classe 'openelec'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

/**
 *
 */
if (file_exists("../dyn/locales.inc.php") === true) {
    require_once "../dyn/locales.inc.php";
}

/**
 * Inclusion des librairies PHP
 */
if (file_exists("../dyn/include.inc.php") === true) {
    require_once "../dyn/include.inc.php";
} else {
    /**
     * Définition de la constante représentant le chemin d'accès au framework
     */
    define("PATH_OPENMAIRIE", getcwd()."/../core/");
    /**
     * TCPDF specific config
     */
    define("K_TCPDF_EXTERNAL_CONFIG", true);
    define("K_TCPDF_CALLS_IN_HTML", true);
    /**
     * Dépendances PHP du framework
     * On modifie la valeur de la directive de configuration include_path en
     * fonction pour y ajouter les chemins vers les librairies dont le framework
     * dépend.
     */
    set_include_path(
        get_include_path().PATH_SEPARATOR.implode(
            PATH_SEPARATOR,
            array(
                getcwd()."/../php/pear",
                getcwd()."/../php/db",
                getcwd()."/../php/fpdf",
                getcwd()."/../php/phpmailer",
                getcwd()."/../php/tcpdf",
            )
        )
    );
}

/**
 *
 */
if (file_exists("../dyn/debug.inc.php") === true) {
    require_once "../dyn/debug.inc.php";
}

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));

/**
 *
 */
(defined("ERROR_MSG_REU_CONNECT") ? "" : define(
        "ERROR_MSG_REU_CONNECT",
        "<h4>La connexion logicielle avec le REU est en erreur.</h4><p>Votre compte logiciel est peut être périmé.</p>"));

/**
 * Get Array Value Or Empty
 */
function gavoe(array $my_array, array $tree, $ret_fallback = "") {
    $tmp = $my_array;
    $last_element = end($tree);
    foreach ($tree as $value) {
        if (array_key_exists($value, $tmp) !== true) {
            return $ret_fallback;
        }
        if ($value == $last_element) {
            return $tmp[$value];
        }
        if ($value !== $last_element
            && is_array($tmp[$value]) !== true) {
            return $ret_fallback;
        }
        $tmp = $tmp[$value];
    }
}

/**
 *
 */
require_once PATH_OPENMAIRIE."om_application.class.php";

/**
 * Surcharges du framework à réintégrer ?
 */
require_once "../obj/om_application_override.class.php";

/**
 * Définition de la classe 'openelec' (om_application).
 */
class openelec extends om_application_override {

    /**
     * Gestion du nom de l'application.
     *
     * @var mixed Configuration niveau application.
     */
    protected $_application_name = "openElec";

    /**
     * Titre HTML.
     *
     * @var mixed Configuration niveau application.
     */
    protected $html_head_title = ":: openMairie :: openElec - Gestion des listes électorales";

    /**
     * Gestion du favicon de l'application.
     *
     * @var mixed Configuration niveau application.
     */
    protected $html_head_favicon = "../app/img/favicon.ico";

    /**
     * Gestion du nom de la session.
     *
     * @var mixed Configuration niveau application.
     */
    protected $_session_name = "openelec";


    /**
     * Gestion du mode de gestion des permissions.
     *
     * @var mixed Configuration niveau framework.
     */
    protected $config__permission_by_hierarchical_profile = false;

    /**
     *
     */
    function get_inst__reu($clean = false) {
        if ($clean === true) {
            unset($this->_inst__reu);
        }
        if (isset($this->_inst__reu) === true) {
            return $this->_inst__reu;
        }
        require_once "../obj/reu.class.php";
        if (file_exists("../dyn/reu.inc.php") !== true) {
            return null;
        }
        require "../dyn/reu.inc.php";
        if (isset($reu_config) !== true) {
            return null;
        }
        $inst_reu = new reu($reu_config);
        if ($inst_reu->reu === null) {
            return null;
        }
        $this->_inst__reu = $inst_reu;
        return $this->_inst__reu;
    }

    /**
     *
     */
    function view_main() {
        $modules_availables = array(
            "user",
            "module_archivage",
            "module_carte_electorale",
            "module_carteretour",
            "module_commission",
            "module_decoupage",
            "module_jury",
            "module_multi",
            "module_procuration",
            "module_refonte",
            "module_notification_courriel",
            "module_reu",
            "module_traitement_annuel",
            "module_traitement_j5",
        );
        $module = $this->get_submitted_get_value("module");
        if (in_array($module, $modules_availables) === true) {
            if ($module == "user") {
                $this->view_userpage();
                return;
            }
            require_once "../obj/".$module.".class.php";
            $inst__module = new $module();
            $inst__module->view_main();
            return;
        }
        parent::view_main();
    }

    /**
     *
     */
    function get_one_result_from_db_query($query = "", $force_return = false) {
        $res = $this->db->getone($query);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$query."\");",
            VERBOSE_MODE
        );
        $result = array(
            'code' => '',
            'message' => '',
            'result' => array(),
        );
        if ($this->isDatabaseError($res, $force_return) !== false) {
            $result['code'] = 'KO';
            $result['message'] = __("Erreur de base de donnees. Contactez votre administrateur.");
            return $result;
        }
        $result['code'] = 'OK';
        $result['result'] = $res;
        return $result;
    }

    /**
     *
     */
    function get_all_results_from_db_query($query = "", $force_return = false) {
        $res = $this->db->query($query);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        $result = array(
            'code' => '',
            'message' => '',
            'result' => array(),
        );
        if ($this->isDatabaseError($res, $force_return) !== false) {
            $result['code'] = 'KO';
            $result['message'] = __("Erreur de base de donnees. Contactez votre administrateur.");
            return $result;
        }
        $result['code'] = 'OK';
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            array_push($result['result'], $row);
        }
        $res->free();
        return $result;
    }

    /**
     *
     * @return void
     */
    function display_js_userpage_reload_and_close_popup() {
        printf(
            '<script type="text/javascript">parent.opener.userpage_reload_marker.dispatchEvent(new Event(\'change\'));window.close();</script>'
        );
    }

    /**
     *
     * @return void
     */
    function view_userpage() {
        //
        if ($this->isAjaxRequest() == false) {
            $this->setFlag(null);
            $this->display();
            printf('<div id="userpage">');
        }
        //
        printf('<input type="hidden" onchange="reload_dialog_userpage();" id="userpage_reload_marker" name="userpage_reload_marker" value="" />');
        //
        $user_infos = $this->retrieveUserProfile($_SESSION["login"]);
        //
        printf(
            '<div id="userpage-infos">
                <h1>%s</h1>
                <div class="detail">
                    <p>Nom : <b>%s</b></p>
                    <p>Courriel : <b>%s</b></p>
                    <p>Profil : <b>%s</b></p>
                </div>
                <div class="visualClear"></div>
            </div>',
            $_SESSION["login"],
            $user_infos["nom"],
            $user_infos["email"],
            $user_infos["libelle"]
        );
        //
        $inst_reu = $this->get_inst__reu();
        if ($inst_reu === null) {
            if ($this->isAjaxRequest() == false) {
                printf('</div>');
            }
            return;
        }
        printf(
            '<div id="userpage-reu"><h2>Répertoire Électoral Unique</h2>'
        );
        if ($inst_reu->is_connexion_logicielle_valid() !== true) {
            $this->displayMessage(
                "error",
                ERROR_MSG_REU_CONNECT
            );
        }
        $inst_reu->display_userpage_widget();
        printf(
            '<div class="visualClear"></div>
            </div>'
        );
        if ($this->isAjaxRequest() == false) {
            printf('</div>');
        }
    }

    /**
     *
     */
    function checkParams() {
        parent::checkParams();
        //
        $this->config['upload_extension'] = ".png;.txt;.xml;.sld;.csv;.pdf;";
    }

    /**
     * Instanciation de la classe 'gen'.
     *
     * @param array $args Arguments à passer au constructeur.
     * @return gen
     */
    function get_inst__om_gen($args = array()) {
        require_once "../obj/om_gen.class.php";
        return new om_app_gen();
    }

    /**
     *
     */
    function store_file($filecontent, $filemetadata, $type, $collectivite = null) {
        if ($collectivite === null) {
            $collectivite = intval($_SESSION["collectivite"]);
        }
        if ($type === "trace") {
            $inst_trace = $this->get_inst__om_dbform(array(
                "obj" => "trace",
                "idx" => 0,
            ));
            $val = array(
                "id" => "NEW",
                "creationdate" => date("Y-m-d"),
                "creationtime" => date("G:i:s"),
                "type" => $filemetadata["filename"],
                "trace" => $filecontent,
                "om_collectivite" => $collectivite,
            );
            return $inst_trace->ajouter($val);
        }
        $uid = $this->storage->create($filecontent, $filemetadata);
        if ($uid == 'OP_FAILURE') {
            return false;
        }
        $inst_fichier = $this->get_inst__om_dbform(array(
            "obj" => "fichier",
            "idx" => 0,
        ));
        $val = array(
            "id" => "NEW",
            "creationdate" => date("Y-m-d"),
            "creationtime" => date("G:i:s"),
            "uid" => $uid,
            "filename" => $filemetadata["filename"],
            "size" => $filemetadata["size"],
            "mimetype" => $filemetadata["mimetype"],
            "type" => $type,
            "om_collectivite" => $collectivite,
        );
        $ret = $inst_fichier->ajouter($val);
        if ($ret !== true) {
            return false;
        }
        //
        return $uid;
    }

    /**
     * Cette
     */
    function isMulti() {
        //
        $sql = sprintf(
            'SELECT niveau FROM %1$som_collectivite WHERE om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        //
        $niveau = $this->db->getone($sql);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->isDatabaseError($niveau);
        //
        if ($niveau == "1") {
            return false;
        } else {
            return true;
        }
    }

    function getActionsToDisplay() {
        //
        $actions_to_display = array();
        //
        if ($this->authenticated == false) {
            return $actions_to_display;
        }
        // Récupère les actions
        $actions = parent::getActionsToDisplay();
        if ($this->isAccredited('liste_de_travail_changer')) {
            $liste = array(
                "title" => __("Liste")." = ".$_SESSION["libelle_liste"],
                "description" => __("Liste de travail"),
                "class" => "liste",
                "href" => "../app/changeliste.php",
            );
            // On ajoute la nouvelle action au début du tableau
            array_unshift($actions, $liste);
        }
        //
        $show_changecollectivite = false;
        if ($this->isAccredited('collectivite_de_travail_changer')
            && $this->getParameter("is_user_multi") === true) {
            $GLOBALS["f"] = $this;
            require_once "../obj/workcollectivite.class.php";
            $workcollectivite = new workCollectivite();
            if ($workcollectivite->countCollectivites() > 1) {
                $show_changecollectivite = true;
            }
        }
        //
        $collectivite_title = $_SESSION['libelle_collectivite'];
        if ($this->getParameter("is_collectivity_multi") === true) {
            $collectivite_title .= " (MULTI)";
        }
        if ($show_changecollectivite) {
            $collectivite = array(
                "title" => $collectivite_title,
                "description" => __("Collectivite de travail"),
                "class" => "collectivite",
                "href" => "../app/changecollectivite.php",
            );
        } else {
            $collectivite = array(
                "title" => $collectivite_title,
                "class" => "collectivite",
            );
        }
        // On ajoute la nouvelle action au début du tableau
        array_unshift($actions, $collectivite);
        //
        $login_title = $_SESSION["login"];
        if ($this->getParameter("is_user_multi") === true) {
            $login_title .= " (MULTI)";
        }
        $login = array(
            "title" => $login_title,
            "class" => "login",
        );
        // On ajoute la nouvelle action au début du tableau
        array_unshift($actions, $login);

        // Retourne les actions
        return $actions;
    }

    /**
     *
     */
    function notExistsError ($explanation = NULL) {
        // message
        $message_class = "error";
        $message = __("Cette page n'existe pas.");
        $this->addToMessage ($message_class, $message);
        //
        $this->setFlag(null);
        $this->display();
        //
        die();
    }

    function triggerAfterLogin($utilisateur = NULL) {
        /**
         * Gestion de la liste de travail
         */
        // Dans les versions strictement inférieures à 5.0, il était possible
        // de définir une liste par défaut pour chaque utilisateur. Cette
        // fonctionnalité a été supprimée dans la version 5.0 car jugée
        // inutile. A la connexion, on fixe donc la liste de travail de
        // l'utilisateur sur la liste principale.
        $_SESSION["liste"] = '01';
        //
        $sql = sprintf(
            'SELECT (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste FROM %1$sliste WHERE liste.liste=\'%2$s\'',
            DB_PREFIXE,
            $_SESSION["liste"]
        );
        $lib = $this->db->getone($sql);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->isDatabaseError($lib);
        //
        if (isset($lib)) {
            $_SESSION["libelle_liste"] = $lib;
        } else {
            $_SESSION["libelle_liste"] = "non d&eacute;fini";
        }
        /**
         * Gestion de la collectivité de travail
         */
        //
        $sql = sprintf(
            'SELECT libelle FROM %1$som_collectivite WHERE om_collectivite=%2$s',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $lib = $this->db->getone($sql);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        $this->isDatabaseError($lib);
        //
        if (isset($lib)) {
            $_SESSION ['libelle_collectivite'] = $lib;
        } else {
            $_SESSION ['libelle_collectivite'] = "non d&eacute;fini";
        }
        /**
         * Gestion des informations MONO/MULTI
         */
        //
        if ($this->isMulti() === true) {
            $_SESSION['niveau_origine'] = $_SESSION["niveau"];
            $_SESSION['multi_utilisateur'] = 1;
            $_SESSION['multi_collectivite'] = 1;
        } else {
            $_SESSION['multi_utilisateur'] = 0;
            $_SESSION['multi_collectivite'] = 0;
        }
    }

    // {{{ DISPLAY

    function displayLinksAsList($links = array()) {
        //
        echo "\t<div class=\"list\">\n";
        foreach ($links as $link) {
            //
            echo "<div class=\"choice ui-corner-all ui-widget-content\">\n";
            echo "<span>\n";
            //
            echo "<a";
            echo " href=\"".$link["href"]."\"";
            echo (isset($link["id"]) ? " id=\"".$link["id"]."\"" : "");
            echo (isset($link["class"]) ? " class=\"".$link["class"]."\"" : "");
            echo (isset($link["target"]) ? " target=\"".$link["target"]."\"" : "");
            echo (isset($link["description"]) ? " title=\"".$link["description"]."\"" : "");
            echo ">";
            echo $link["title"];
            echo "</a>\n";
            //
            echo "</span>\n";
            echo "</div>\n";
        }
        echo "</div>\n";
        echo "<div class=\"both\"><!-- --></div>";
    }

    // }}}

    /**
     *
     */
    public function normalizeString($string_to_normalize = ""){
        //
        $string_to_normalize = str_replace(" ", "", $string_to_normalize);
        $string_to_normalize = str_replace("/", "-", $string_to_normalize);
        $string_to_normalize = str_replace("'", "", $string_to_normalize);
        return $string_to_normalize;
    }

    function getParameter($param = NULL) {
        //
        if ($param === "is_user_multi") {
            if ($_SESSION["multi_utilisateur"] == 1) {
                return true;
            } else {
                return false;
            }
        }
        //
        if ($param === "is_user_mono") {
            if ($_SESSION["multi_utilisateur"] == 0) {
                return true;
            } else {
                return false;
            }
        }
        //
        if ($param === "is_collectivity_multi") {
            if ($_SESSION["multi_collectivite"] == 1) {
                return true;
            } else {
                return false;
            }
        }
        //
        if ($param === "is_collectivity_mono") {
            if ($_SESSION["multi_collectivite"] == 0) {
                return true;
            } else {
                return false;
            }
        }
        if ($param === "is_option_composition_scrutin_enabled") {
            return $this->is_option_composition_scrutin_enabled();
        }
        // openDébitDeBoisson est fait pour fonctionner avec la view 'settings'
        // pour gérer le menu 'Administration & Paramétrage'. On force donc le
        // paramètre.
        if ($param == "is_settings_view_enabled") {
            return true;
        }
        return parent::getParameter($param);
    }

    function setDefaultValues() {
        $this->addHTMLHeadCss(
            array(
                "../app/lib/jquery-ui-theme/jquery-ui.custom.css",
                "../app/css/om.css",
            ),
            21
        );
        $this->addHTMLHeadJs(
            array(
                "../app/lib/dual-list-box/dual-list-box.min.js",
            ),
            21
        );
    }

    /**
     * OPTION - La liste d'émargement peut être dressée par ordre des numéros
     * d'inscription ou par ordre alphabétique des électeurs, au choix de la mairie
     *
     * - "alpha"; => Ordre alphabétique des électeurs
     * - "numero"; => Ordre des numéros d'inscription
     *
     * default : $option_tri_liste_emargement = "alpha";
     *
     * @return boolean
     */
    function is_option_tri_liste_emargement_alpha() {
        if (trim($this->getParameter("option_tri_liste_emargement")) === "numero") {
            return false;
        }
        return true;
    }

    /**
     * OPTION - Possibilite de choisir si les courriers de refus de mouvement sont
     * dispo dans l'appli.
     *
     * - false; => refus de mouvement sont pas dispo
     * - true; => refus de mouvement sont dispo
     *
     * default : true;
     *
     * @return boolean
     */
    function is_option_refus_mouvement_enabled() {
        if (trim($this->getParameter("option_refus_mouvement")) !== "false") {
            return true;
        }
        return false;
    }

    /**
     * OPTION - Possibilite de choisir si les courriers de refus de mouvement sont
     * dispo dans l'appli.
     *
     * - false; => refus de mouvement sont pas dispo
     * - true; => refus de mouvement sont dispo
     *
     * default : true;
     *
     * @return boolean
     */
    function is_option_composition_scrutin_enabled() {
        if (trim($this->getParameter("option_composition_scrutin")) === "true") {
            return true;
        }
        return false;
    }

    /**
     * OPTION - Possibilite de realiser la generation des editions au format zip
     * ou fichier par fichier. Cette option n'est necessaire que pour le mode
     * MULTI.
     *
     * - false; => fichiers pdf separes
     * - true => fichiers pdf dans une archive zip
     *
     * default : true;
     *
     * @return boolean
     */
    function is_option_generation_multi_as_an_archive_enabled() {
        if (trim($this->getParameter("option_generation_multi_as_an_archive")) !== "false") {
            return true;
        }
        return false;
    }

    /**
     * OPTION - Possibilite de choisir si les codes voie sont automatiques ou non.
     *
     * - false; => les codes voies ne sont pas automatiques
     * - true; => les codes voie sont automatiques
     *
     * default : true;
     *
     * @return boolean
     */
    function is_option_code_voie_auto_enabled() {
        if (trim($this->getParameter("option_code_voie_auto")) !== "false") {
            return true;
        }
        return false;
    }


    /**
     * OPTION - Possibilite d'afficher la date ou l'année d'édition de la carte électorale
     *
     * - false; => n'affiche pas la date
     * - true; => affiche la date
     *
     * default : false;
     *
     * @return boolean
     */
    function is_option_carte_electorale_date_enabled() {
        $option_carte_electorale_date = $this->get_parametrage_carte_electorale_date();
        if ( trim($option_carte_electorale_date['enabled']) !== "false") {
            return true;
        }
        return false;
    }

    /**
     * OPTION - Possibilite d'afficher le lieu de naissance sur la carte électorale
     *
     * - false; => n'affiche pas le lieu de naissance
     * - true; => affiche le lieux de naissance
     *
     * default : true;
     *
     * @return boolean
     */
    function is_option_carte_electorale_lieu_naissance_enabled() {
        if ( trim($this->getParameter("option_carte_electorale_lieu_naissance")) !== "false") {
            return true;
        }
        return false;
    }


    /**
     * Affichage des codes  barres
     *  - true : affichage
     *  - false : pas d'affichage
     *
     * default : false
     *
     * @return boolean
     */
    function is_option_code_barre_enabled() {
        $option_code_barre = $this->get_parametrage_code_barre();
        if (trim($option_code_barre['enabled']) !== 'true') {
            return false;
        }
        return true;
    }

    /**
     * pdf_commission.inc : Etat a la commission de fin d année
     * si true = que les modifications de changement de bureaux [arles]
     * sinon : toutes les modifications
     *
     * default : true
     *
     * @return boolean
     */
    function is_option_commission_sans_modification_enabled() {
        if (trim($this->getParameter("option_commission_sans_modification")) !== "false") {
            return true;
        }
        return false;
    }

    /**
     * Vérifie si une option est active, c'est à dire si l'élèment de la table
     * om_parametre dont le libellé est passé en argument à pour valeur "true".
     *
     * Si le paramètre n'existe pas ou que sa valeur n'est pas true renvoie false.
     *
     * @param string option_name : nom de l'option
     * @return boolean
     */
    function is_option_enabled($option_name) {
        return trim($this->getParameter($option_name)) === "true";
    }

    /**
     * Export INSEE - Séparateur de ligne
     *
     * @return string
     */
    function get_newline_character() {
        if (trim($this->getParameter('parametrage_newline')) == "\r\n") {
            return "\r\n";
        }
        return "\n";
    }

    /**
     * Code du type de mouvement obligeant la saisie d'une commune de provenance
     *
     * @return string
     */
    function get_parametrage_mouvement_venantautrecommune() {
        if (trim($this->getParameter('parametrage_mouvement_venantautrecommune')) !== "") {
            return trim($this->getParameter('parametrage_mouvement_venantautrecommune'));
        }
        return "CC";
    }

    /**
     * Code du type de mouvement pour les inscriptions d'office
     *
     * @return string
     */
    function get_parametrage_mouvement_inscriptionoffice() {
        if (trim($this->getParameter('parametrage_mouvement_inscriptionoffice')) !== "") {
            return trim($this->getParameter('parametrage_mouvement_inscriptionoffice'));
        }
        return "IO";
    }

    /**
     * @return array
     */
    function get_parametrage_etiquettes() {
        //
        $param = array();
        foreach (explode("\n", $this->getParameter("parametrage_etiquettes")) as $elem) {
            $arg = explode("=", $elem);
            if (count($arg) == 2) {
                $param[trim($arg[0])] = intval(trim($arg[1]));
            }
        }
        //
        (!isset($param["etiquette_bordure_etiquette"]) ? $param["etiquette_bordure_etiquette"] = 1 : "");
        (!isset($param["etiquette_bordure_texte"]) ? $param["etiquette_bordure_texte"] = 0 : "");
        (!isset($param["planche_marge_ext_haut"]) ? $param["planche_marge_ext_haut"] = 1 : "");
        (!isset($param["planche_marge_ext_gauche"]) ? $param["planche_marge_ext_gauche"] = 0 : "");
        (!isset($param["planche_nb_colonnes"]) ? $param["planche_nb_colonnes"] = 2 : "");
        (!isset($param["planche_nb_lignes"]) ? $param["planche_nb_lignes"] = 7 : "");
        (!isset($param["etiquette_largeur"]) ? $param["etiquette_largeur"] = 105 : "");
        (!isset($param["etiquette_hauteur"]) ? $param["etiquette_hauteur"] = 42 : "");
        (!isset($param["etiquette_marge_int_haut"]) ? $param["etiquette_marge_int_haut"] = 8 : "");
        (!isset($param["etiquette_marge_int_gauche"]) ? $param["etiquette_marge_int_gauche"] = 8 : "");
        (!isset($param["etiquette_marge_int_droite"]) ? $param["etiquette_marge_int_droite"] = 8 : "");
        (!isset($param["etiquette_espace_entre_colonnes"]) ? $param["etiquette_espace_entre_colonnes"] = 0 : "");
        (!isset($param["etiquette_espace_entre_lignes"]) ? $param["etiquette_espace_entre_lignes"] = -1 : "");
        (!isset($param["etiquette_hauteur_de_ligne_du_texte"]) ? $param["etiquette_hauteur_de_ligne_du_texte"] = 4 : "");
        (!isset($param["etiquette_taille_du_texte"]) ? $param["etiquette_taille_du_texte"] = 10 : "");
        //
        return $param;
    }

     /**
     * @return array
     */
    function get_parametrage_code_barre() {
        //
        $param = array();
        foreach (explode("\n", $this->getParameter("option_code_barre")) as $elem) {
            if(strpos($elem, "=") === false) {
                $param["enabled"] = $elem;
            }
            $arg = explode("=", $elem);
            if (count($arg) == 2) {
                $param[trim($arg[0])] = trim($arg[1]);
            }
        }
        //
        (!isset($param["enabled"]) ? $param["enabled"] = "false" : "");
        (!isset($param["pos_x"]) ? $param["pos_x"] = "100" : "");
        (!isset($param["pos_y"]) ? $param["pos_y"] = "5" : "");
        (!isset($param["show_bureau_code"]) ? $param["show_bureau_code"] = "false" : "");
        //
        return $param;
    }

    /**
     * @return array
     */
    function get_parametrage_carte_electorale() {
        //
        $param = array();
        foreach (explode("\n", $this->getParameter("parametrage_carte_electorale")) as $elem) {
            if (strpos($elem, "=") === false) {
                $param["enabled"] = $elem;
            }
            $arg = explode("=", $elem);
            if (count($arg) == 2) {
                $param[trim($arg[0])] = trim($arg[1]);
            }
        }
        //
        (!isset($param["modele"]) ? $param["modele"] = "2022" : "");
        (!isset($param["debug"]) ? $param["debug"] = "false" : "");
        //
        return $param;
    }

     /**
     * @return array
     */
    function get_parametrage_carte_electorale_date() {
        //
        $param = array();
        foreach (explode("\n", $this->getParameter("option_carte_electorale_date")) as $elem) {
            if(strpos($elem, "=") === false) {
                $param["enabled"] = $elem;
            }
            $arg = explode("=", $elem);
            if (count($arg) == 2) {
                $param[trim($arg[0])] = trim($arg[1]);
            }
        }
        //
        (!isset($param["enabled"]) ? $param["enabled"] = "false" : "");
        (!isset($param["format"]) ? $param["format"] = "Y" : "");
        //
        return $param;
    }

    /**
     * @return array
     */
    function get_parametrage_facturation() {
        //
        $param = array();
        foreach (explode("\n", $this->getParameter("parametrage_facturation")) as $elem) {
            $arg = explode("=", $elem);
            if (count($arg) == 2) {
                $param[trim($arg[0])] = trim($arg[1]);
            }
        }
        //
        (!isset($param['fact_gestionnaire']) ? $param['fact_gestionnaire'] = "A304" : "");
        (!isset($param['fact_attributaire']) ? $param['fact_attributaire'] = "A304" : "");
        (!isset($param['fact_chapitre']) ? $param['fact_chapitre'] = "70/7087801" : "");
        (!isset($param['fact_electeur_unitaire']) ? $param['fact_electeur_unitaire'] = .28 : "");
        (!isset($param['fact_forfait']) ? $param['fact_forfait'] = 30.49 : "");
        //
        return $param;
    }

    /**
     * Permet de définir la configuration des liens des shortlinks.
     *
     * @return void
     */
    protected function set_config__shortlinks() {
        parent::set_config__shortlinks();
        //
        $shortlinks = array();
        // Inscription
        $shortlinks[] = array(
            "title" => "<span class=\"om-icon om-icon-25 om-icon-fix mvt-inscription-25\">".__("Inscription")."</span>",
            "description" => __("Inscription"),
            "href" => OM_ROUTE_FORM."&obj=inscription&idx=0&action=101",
            "right" => "inscription",
            "class" => "shortlinks-inscription",
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        // Modification
        $shortlinks[] = array(
            "title" => "<span class=\"om-icon om-icon-25 om-icon-fix mvt-modification-25\">".__("Modification")."</span>",
            "description" => __("Modification"),
            "href" => OM_ROUTE_FORM."&obj=modification&idx=0&action=101",
            "right" => "electeur_modification",
            "class" => "shortlinks-electeur_modification",
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        // Radiation
        $shortlinks[] = array(
            "title" => "<span class=\"om-icon om-icon-25 om-icon-fix mvt-radiation-25\">".__("Radiation")."</span>",
            "description" => __("Radiation"),
            "href" => OM_ROUTE_FORM."&obj=radiation&idx=0&action=101",
            "right" => "electeur_radiation",
            "class" => "shortlinks-electeur_radiation",
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        //
        $this->config__shortlinks = array_merge(
            $shortlinks,
            $this->config__shortlinks
        );
    }

    /**
     * Permet de définir la configuration des liens du footer.
     *
     * @return void
     */
    protected function set_config__footer() {
        $footer = array();
        // Documentation du site
        $footer[] = array(
            "title" => __("Documentation"),
            "description" => __("Acceder a l'espace documentation de l'application"),
            "href" => "http://docs.openmairie.org/?project=openelec&version=5.13&format=html&path=manuel_utilisateur",
            "target" => "_blank",
            "class" => "footer-documentation",
        );

        // Forum openMairie
        $footer[] = array(
            "title" => __("Forum"),
            "description" => __("Espace d'échange ouvert du projet openMairie"),
            "href" => "https://communaute.openmairie.org/c/openelec",
            "target" => "_blank",
            "class" => "footer-forum",
        );

        // Portail openMairie
        $footer[] = array(
            "title" => __("openMairie.org"),
            "description" => __("Site officiel du projet openMairie"),
            "href" => "http://www.openmairie.org/catalogue/openelec",
            "target" => "_blank",
            "class" => "footer-openmairie",
        );
        //
        $this->config__footer = $footer;
    }

    /**
     * Surcharge - set_config__menu().
     *
     * @return void
     */
    protected function set_config__menu() {
        // Récupération du menu du framework
        parent::set_config__menu();
        $parent_menu = $this->config__menu;
        // Suppression de la rubrique 'Export'
        unset($parent_menu["om-menu-rubrik-export"]);
        //
        $menu = array();
        // {{{ Rubrique SAISIE - BEGIN
        $rubrik = array(
            "title" => __("Saisie Electeur"),
            "class" => "saisie",
            "right" => array(
                "menu_saisie",
            ),
        );
        //
        $links = array();
        //
        $links[] = array(
            "href" => "../app/changecollectivite.php",
            "title" => __("Selectionner la commune"),
            "right" => array(
                "collectivite_de_travail_changer",
            ),
            "open" => array(
                "changecollectivite.php|",
            ),
            "parameters" => array("is_user_multi" => true, ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=inscription&idx=0&action=101",
            "title" => __("Inscription"),
            "right" => array(
                "inscription",
                "inscription_ajouter",
            ),
            "class" => "inscription",
            "open" => array(
                "index.php|inscription[module=form][action=0]",
                "index.php|inscription[module=form][action=101]",
                "index.php|inscription[module=form][action=102]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "modification",
                "modification_ajouter",
                "radiation",
                "radiation_ajouter",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=modification&idx=0&action=101",
            "title" => __("Modification"),
            "right" => array(
                "modification",
                "modification_ajouter",
            ),
            "class" => "modification",
            "open" => array(
                "index.php|modification[module=form][maj=0]",
                "index.php|modification[module=form][action=0]",
                "index.php|modification[module=form][action=101]",
                "index.php|modification[module=form][action=102]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=radiation&idx=0&action=101",
            "title" => __("Radiation"),
            "right" => array(
                "radiation",
                "radiation_ajouter",
            ),
            "class" => "radiation",
            "open" => array(
                "index.php|radiation[module=form][maj=0]",
                "index.php|radiation[module=form][action=0]",
                "index.php|radiation[module=form][action=101]",
                "index.php|radiation[module=form][action=102]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "procuration",
                "procuration_ajouter",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=procuration&action=0&retour=form",
            "title" => __("Procuration"),
            "right" => array(
                "procuration",
                "procuration_ajouter",
            ),
            "class" => "procuration",
            "open" => array(
                "index.php|procuration[module=form][action=0]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $rubrik['links'] = $links;
        array_push($menu, $rubrik);
        // }}} Rubrique SAISIE - END
        // {{{ Rubrique CONSULTATION
        //
        $rubrik = array(
            "title" => __("Consultation"),
            "class" => "consultation",
            "right" => array(
                "menu_consultation",
            ),
        );
        //
        $links = array();
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=electeur&action=601",
            "title" => __("Liste electorale"),
            "right" => array(
                "electeur_tab",
            ),
            "class" => "electeurs",
            "open" => array(
                "index.php|electeur[module=tab]",
                "index.php|electeur[module=form][action=3]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=archive",
            "title" => __("Archive"),
            "right" => array(
                "archive",
                "archive_tab",
            ),
            "class" => "archive",
            "open" => array(
                "index.php|archive[module=tab]",
                "index.php|archive[module=form][action=3]",
            ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "inscription",
                "inscription_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=inscription",
            "title" => __("Inscription"),
            "right" => array(
                "inscription",
                "inscription_tab",
            ),
            "class" => "inscriptions",
            "open" => array(
                "index.php|inscription[module=tab]",
                "index.php|inscription[module=form][action=1]",
                "index.php|inscription[module=form][action=2]",
                "index.php|inscription[module=form][action=3]",
                "index.php|inscription[module=form][action=62]",
                "index.php|inscription[module=form][action=502]",
                "index.php|inscription[module=form][action=503]",
            ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "modification",
                "modification_tab",
                "radiation",
                "radiation_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=modification",
            "title" => __("Modification"),
            "right" => array(
                "modification",
                "modification_tab",
            ),
            "class" => "modifications",
            "open" => array(
                "index.php|modification[module=tab]",
                "index.php|modification[module=form][action=1]",
                "index.php|modification[module=form][action=2]",
                "index.php|modification[module=form][action=3]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=radiation",
            "title" => __("Radiation"),
            "right" => array(
                "radiation",
                "radiation_tab",
            ),
            "class" => "radiations",
            "open" => array(
                "index.php|radiation[module=tab]",
                "index.php|radiation[module=form][action=1]",
                "index.php|radiation[module=form][action=2]",
                "index.php|radiation[module=form][action=3]",
                "index.php|radiation[module=form][action=502]",
            ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "procuration_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=procuration",
            "title" => __("Procurations"),
            "right" => array(
                "procuration",
                "procuration_tab",
            ),
            "class" => "procurations",
            "open" => array(
                "index.php|procuration[module=tab]",
                "index.php|procuration[module=form][action=1]",
                "index.php|procuration[module=form][action=2]",
                "index.php|procuration[module=form][action=3]",
                "index.php|procuration[module=form][action=21]",
                "index.php|procuration[module=form][action=22]",
                "index.php|procuration[module=form][action=23]",
                "index.php|procuration[module=form][action=91]",
            ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "fichier_voir_tous",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_FORM."&obj=fichier&idx=0&action=4",
            "title" => __("Traces et Editions"),
            "class" => "traces",
            "right" => array(
                "fichier_voir_tous",
            ),
            "open" => array(
                "index.php|fichier[module=form][action=4]",
            ),
        );
        $rubrik['links'] = $links;
        array_push($menu, $rubrik);
        // }}}
        // {{{ Rubrique SCRUTIN
        //
        $rubrik = array(
            "title" => __("Scrutins"),
            "class" => "scrutin",
            "right" => array(
                "menu_scrutin",
            ),
            "parameters" => array("is_collectivity_mono" => true),
        );
        //
        $links = array();
        //
        $links[] = array(
            "class" => "category",
            "title" => __("composition des bureaux"),
            "right" => array(
                "composition_scrutin", "composition_scrutin_tab", "agent", "agent_tab",
                "elu", "elu_tab",
            ),
            "parameters" => array(
                "is_collectivity_mono" => true,
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "composition_scrutin", "composition_scrutin_tab", "agent", "agent_tab",
                "elu", "elu_tab",
            ),
            "parameters" => array(
                "is_collectivity_mono" => true,
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=composition_scrutin",
            "title" => __("Composition"),
            "right" => array(
                "composition_scrutin",
                "composition_scrutin_tab",
            ),
            "class" => "composition-scrutin",
            "open" => array(
                "index.php|composition_scrutin[module=form]",
                "index.php|composition_scrutin[module=tab]",
            ),
            "parameters" => array(
                "is_collectivity_mono" => true,
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=agent",
            "title" => __("Agents"),
            "right" => "agent",
            "class" => "agent",
            "open" => array(
                "index.php|agent[module=form]",
                "index.php|agent[module=tab]",
            ),
            "parameters" => array(
                "is_collectivity_mono" => true,
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=elu",
            "title" => __("Élus"),
            "right" => "elu",
            "class" => "elu",
            "open" => array(
                "index.php|elu[module=form]",
                "index.php|elu[module=tab]",
            ),
            "parameters" => array(
                "is_collectivity_mono" => true,
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        $rubrik['links'] = $links;
        array_push($menu, $rubrik);
        // }}}
        // {{{ Rubrique EDITION
        //
        $rubrik = array(
            "title" => __("Edition"),
            "right" => array(
                "menu_edition",
            ),
            "class" => "edition",
        );
        //
        $links = array();
        $links[] = array(
            "href" => "../app/view_editions_par_bureau.php",
            "title" => __("Par Bureau"),
            "right" => array(
                "bureau_edition",
            ),
            "class" => "menu-editions-par-bureau",
            "open" => array(
                "view_editions_par_bureau.php|",
            ),
        );
        $links[] = array(
            "href" => "../app/view_editions_generales.php",
            "title" => __("Generales"),
            "right" => array(
                "edition",
            ),
            "class" => "menu-editions-generales",
            "open" => array(
                "view_editions_generales.php|",
            ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "module_carte_electorale",
            ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_carte_electorale",
            "title" => __("Cartes électorales"),
            "right" => array(
                "electeur_edition_pdf__carte_electorale",
            ),
            "class" => "carte_electorale",
            "open" => array(
                "index.php|[module=module_carte_electorale]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "statistiques",
            ),
        );
        $links[] = array(
            "href" => "../app/view_statistiques.php",
            "title" => __("Statistiques"),
            "right" => array(
                "statistiques",
            ),
            "class" => "statistiques",
            "open" => array(
                "view_statistiques.php|",
            ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "reqmo",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_MODULE_REQMO,
            "class" => "reqmo",
            "title" => __("Requetes memorisees"),
            "right" => array(
                "reqmo",
            ),
            "open" => array(
                "reqmo.php|",
                "index.php|[module=reqmo]",
            ),
        );
        $rubrik['links'] = $links;
        array_push($menu, $rubrik);
        // }}}
        // {{{ Rubrique TRAITEMENT
        //
        $rubrik = array(
            "title" => __("Traitement"),
            "class" => "traitement",
            "right" => array(
                "menu_traitement",
            ),
        );
        //
        $links = array();
        $links[] = array(
            "href" => "../app/index.php?module=module_commission",
            "title" => __("Commissions"),
            "right" => array(
                "module_commission",
            ),
            "class" => "commission",
            "open" => array(
                "index.php|[module=module_commission]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_reu",
            "title" => __("REU"),
            "right" => array(
                "module_reu",
            ),
            "class" => "reu",
            "open" => array(
                "index.php|[module=module_reu]",
                "index.php|reu_sync_l_e_oel_rattaches_approx",
                "index.php|reu_sync_l_e_oel_rattaches_exact",
                "index.php|reu_sync_l_e_oel_non_rattaches",
                "index.php|reu_sync_l_e_reu_non_rattaches",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "reu_scrutin",
                "reu_scrutin_tab",
                "module_procuration",
                "module_traitement_j5",
                "module_traitement_annuel",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        //
        $links[] = array(
            "href" => "../app/index.php?module=tab&obj=reu_scrutin",
            "title" => __("Élections"),
            "right" => array(
                "reu_scrutin",
                "reu_scrutin_tab",
            ),
            "class" => "module-election",
            "open" => array(
                "index.php|reu_scrutin",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        //
        $links[] = array(
            "href" => "../app/index.php?module=tab&obj=arrets_liste",
            "title" => __("Arrêts des listes"),
            "right" => array(
                "arrets_liste",
                "arrets_liste_tab",
            ),
            "class" => "module-arret_liste",
            "open" => array(
                "index.php|arrets_liste",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_procuration",
            "title" => __("Procurations"),
            "right" => array(
                "module_procuration",
            ),
            "class" => "module-procuration",
            "open" => array(
                "index.php|[module=module_procuration]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_traitement_j5",
            "title" => __("Traitement J-5"),
            "right" => array(
                "module_traitement_j5",
            ),
            "class" => "module-traitement-j5",
            "open" => array(
                "index.php|[module=module_traitement_j5]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_traitement_annuel",
            "title" => __("Traitement Fin d'annee"),
            "right" => array(
                "module_traitement_annuel",
            ),
            "class" => "module-traitement-annuel",
            "open" => array(
                "index.php|[module=module_traitement_annuel]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "module_carteretour",
                "module_jury",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );

        $links[] = array(
            "href" => "../app/index.php?module=module_carteretour",
            "title" => __("Cartes en retour"),
            "right" => array(
                "module_carteretour",
            ),
            "class" => "carteretour",
            "open" => array(
                "index.php|[module=module_carteretour]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_jury",
            "title" => __("Jury d'assises"),
            "right" => array(
                "module_jury",
            ),
            "class" => "jury",
            "open" => array(
                "index.php|[module=module_jury]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "module_refonte",
                "module_archivage",
                "module_redecoupage",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_refonte",
            "title" => __("Refonte"),
            "right" => array(
                "module_refonte",
            ),
            "class" => "refonte",
            "open" => array(
                "index.php|[module=module_refonte]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_archivage",
            "title" => __("Archivage"),
            "right" => array(
                "module_archivage",
            ),
            "class" => "archivage",
            "open" => array(
                "index.php|[module=module_archivage]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_decoupage",
            "title" => __("Redecoupage"),
            "right" => array(
                "module_redecoupage",
            ),
            "class" => "redecoupage",
            "open" => array(
                "index.php|[module=module_decoupage]",
            ),
            "parameters" => array("is_collectivity_mono" => true, ),
        );
        $links[] = array(
            "title" => "<hr />",
            "right" => array(
                "module_multi",
            ),
            "parameters" => array("is_collectivity_multi" => true, ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_multi",
            "title" => __("Multi Collectivites"),
            "class" => "menu-traitements-multi",
            "right" => array(
                "module_multi",
            ),
            "open" => array(
                "index.php|[module=module_multi]",
            ),
            "parameters" => array("is_collectivity_multi" => true, ),
        );
        $rubrik['links'] = $links;
        array_push($menu, $rubrik);
        // }}}
        // {{{ Rubrique PARAMETRAGE
        //
        $rubrik = array(
            "title" => __("Paramétrage Métier"),
            "class" => "parametrage",
            "right" => "menu_parametrage",
            "parameters" => array("is_settings_view_enabled" => false, ),
        );
        //
        $links = array();
        $links[] = array(
            "class" => "category",
            "title" => __("Métier"),
            "right" => array(
                "revision",
                "revision_tab",
                "liste",
                "liste_tab",
                "param_mouvement",
                "param_mouvement_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=revision",
            "title" => __("Revisions électorales"),
            "description" => __("Paramétrage des révisions électorales et des dates de tableau."),
            "right" => array(
                "revision",
                "revision_tab",
            ),
            "class" => "revision",
            "open" => array(
                "index.php|revision[module=tab]",
                "index.php|[module=form][obj=revision][action=0]",
                "index.php|[module=form][obj=revision][action=1]",
                "index.php|[module=form][obj=revision][action=2]",
                "index.php|[module=form][obj=revision][action=3]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=param_mouvement",
            "title" => __("Mouvements"),
            "description" => __("Paramétrage des types de mouvements : le type de catégorie, l’effet immédiat (j-5) ou 1er Mars (annuel), le transfert ou non à l'INSEE, si la carte d’électeur doit être imprimé à la suite de ce mouvement ou non..."),
            "right" => array(
                "param_mouvement",
                "param_mouvement_tab",
            ),
            "class" => "mouvement",
            "open" => array(
                "index.php|param_mouvement[module=tab]",
                "index.php|param_mouvement[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=liste",
            "title" => __("Liste"),
            "description" => __("Paramétrage des listes électorales et de leur références INSEE (liste principale, listes complémentaires, ...)."),
            "right" => array(
                "liste",
                "liste_tab",
            ),
            "class" => "liste",
            "open" => array(
                "index.php|liste[module=tab]",
                "index.php|liste[module=form]",
            ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=module_notification_courriel",
            "title" => __("Notification par courriel"),
            "right" => array(
                "module_notification_courriel",
            ),
            "description" => __("Permet de paramétrer les profils qui recevront les notifications et de tester l'envoie de la notification."),
            "class" => "notification_courriel",
            "open" => array(
                "index.php|[module=module_notification_courriel]",
            ),
        );
        $links[] = array(
            "href" => "../app/index.php?module=tab&obj=trace",
            "title" => __("Traces"),
            "right" => array(
                "trace", "trace_tab",
            ),
            "description" => __("Toutes les traces générées par les traitements réguliers..."),
            "class" => "trace",
            "open" => array(
                "index.php|trace[module=tab]",
                "index.php|trace[module=form]",
            ),
        );
        //
        $links[] = array(
            "class" => "category",
            "title" => __("découpage"),
            "right" => array(
                "canton", "canton_tab", "circonscription", "circonscription_tab",
                "bureau", "bureau_tab", "voie", "voie_tab",
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "canton", "canton_tab", "circonscription", "circonscription_tab",
                "bureau", "bureau_tab", "voie", "voie_tab",
            ),
        );
        //
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=canton",
            "title" => __("Canton"),
            "description" => __("Paramétrage des cantons d’appartenance des bureaux de vote."),
            "class" => "canton",
            "right" => array(
                "canton",
                "canton_tab",
            ),
            "open" => array(
                "index.php|canton[module=tab]",
                "index.php|canton[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=circonscription",
            "title" => __("Circonscription"),
            "class" => "circonscription",
            "description" => __("Paramétrage des circonscriptions législatives d’appartenance des bureaux de vote."),
            "right" => array(
                "circonscription",
                "circonscription_tab",
            ),
            "open" => array(
                "index.php|circonscription[module=tab]",
                "index.php|circonscription[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=bureau",
            "title" => __("Bureau"),
            "class" => "bureau",
            "description" => __("Paramétrage des bureaux de vote de la commune (adresse, canton, circonscription, ...) et de leur découpage."),
            "right" => array(
                "bureau",
                "bureau_tab",
            ),
            "open" => array(
                "index.php|bureau[module=tab]",
                "index.php|bureau[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=voie",
            "title" => __("Voie"),
            "description" => __("Paramétrage de toutes les voies / rues de la commune et de leur découpage."),
            "class" => "voie",
            "right" => array(
                "voie",
                "voie_tab",
            ),
            "open" => array(
                "index.php|voie[module=tab]",
                "index.php|voie[module=form]",
            ),
        );
        $links[] = array(
            "class" => "category",
            "title" => __("Tables de références"),
            "right" => array(
                "commune",
                "commune_tab",
                "departement",
                "departement_tab",
                "nationalite",
                "nationalite_tab",
                "piece_type",
                "piece_type_tab",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "commune_tab",
                "departement_tab",
                "nationalite_tab"
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=commune",
            "title" => __("Commune"),
            "description" => __("Toutes les communes françaises avec leur code INSEE utilisées principalement pour les lieux de naissance des électeurs."),
            "class" => "commune",
            "right" => array(
                "commune",
                "commune_tab",
            ),
            "open" => array(
                "index.php|commune[module=tab]",
                "index.php|commune[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=departement",
            "title" => __("Departement / Pays"),
            "description" => __("Tous les dếpartements français et les pays avec leur code INSEE utilisés principalement pour les lieux de naissance des électeurs."),
            "class" => "departement",
            "right" => array(
                "departement",
                "departement_tab",
            ),
            "open" => array(
                "index.php|departement[module=tab]",
                "index.php|departement[module=form]",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=nationalite",
            "title" => __("Nationalite"),
            "description" => __("Toutes les nationalités européennes autorisées sur les lsites électorales principales et complémentaires."),
            "class" => "nationalite",
            "right" => array(
                "nationalite",
                "nationalite_tab",
            ),
            "open" => array(
                "index.php|nationalite[module=tab]",
                "index.php|nationalite[module=form]",
            ),
        );
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "piece_type",
                "piece_type_tab",
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=piece_type",
            "title" => __("Type de pièce"),
            "description" => __("Tous les types de pièce utilisées comme justificatif."),
            "class" => "piece_type",
            "right" => array(
                "piece_type",
                "piece_type_tab",
            ),
            "open" => array(
                "index.php|piece_type[module=tab]",
                "index.php|piece_type[module=form]",
            ),
        );
        //
        $links[] = array(
            "class" => "category",
            "title" => __("composition des bureaux"),
            "right" => array(
                "service", "service_tab", "grade", "grade_tab",
                "poste", "poste_tab", "periode", "periode_tab",
            ),
            "parameters" => array(
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        //
        $links[] = array(
            "title" => "<hr/>",
            "right" => array(
                "service", "service_tab", "grade", "grade_tab",
                "poste", "poste_tab", "periode", "periode_tab",
            ),
            "parameters" => array(
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=service",
            "title" => __("Services"),
            "description" => __("Tous les services des agents."),
            "class" => "service",
            "right" => array(
                "service",
                "service_tab",
            ),
            "open" => array(
                "index.php|service[module=tab]",
                "index.php|service[module=form]",
            ),
            "parameters" => array(
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=grade",
            "title" => __("Grades"),
            "description" => __("Tous les grades des agents."),
            "class" => "grade",
            "right" => array(
                "grade",
                "grade_tab",
            ),
            "open" => array(
                "index.php|grade[module=tab]",
                "index.php|grade[module=form]",
            ),
            "parameters" => array(
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=poste",
            "title" => __("Postes"),
            "description" => __("Tous les postes des affectations des élus."),
            "class" => "poste",
            "right" => array(
                "poste",
                "poste_tab",
            ),
            "open" => array(
                "index.php|poste[module=tab]",
                "index.php|poste[module=form]",
            ),
            "parameters" => array(
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        $links[] = array(
            "href" => OM_ROUTE_TAB."&obj=periode",
            "title" => __("Périodes"),
            "description" => __("Tous les périodes des affectations des élus."),
            "class" => "periode",
            "right" => array(
                "periode",
                "periode_tab",
            ),
            "open" => array(
                "index.php|periode[module=tab]",
                "index.php|periode[module=form]",
            ),
            "parameters" => array(
                "is_option_composition_scrutin_enabled" => true,
            ),
        );
        $rubrik['links'] = $links;
        array_push($menu, $rubrik);
        // }}}
        $this->config__menu = array_merge(
            $menu,
            $parent_menu
        );
    }

    // {{{

    /**
     * Récupère la liste de tous les bureaux de vote filtrée sur la collectivité
     * de l'utilisateur connecté.
     *
     * @return array
     */
    function get_all__bureau__by_my_collectivite() {
        $query = sprintf(
            'SELECT
                bureau.id,
                bureau.referentiel_id,
                bureau.code,
                bureau.libelle,
                bureau.canton,
                bureau.circonscription,
                bureau.adresse_numero_voie,
                bureau.adresse_libelle_voie,
                bureau.adresse_complement1,
                bureau.adresse_complement2,
                bureau.adresse_lieu_dit,
                bureau.adresse_code_postal,
                bureau.adresse_ville,
                bureau.adresse_pays,
                bureau.surcharge_adresse_carte_electorale,
                bureau.adresse1,
                bureau.adresse2,
                bureau.adresse3,
                bureau.om_collectivite,
                canton.id as canton_id,
                canton.code as canton_code,
                canton.libelle as canton_libelle,
                circonscription.id as circonscription_id,
                circonscription.code as circonscription_code,
                circonscription.libelle as circonscription_libelle
            FROM
                %1$sbureau
                INNER JOIN %1$scanton ON bureau.canton=canton.id
                INNER JOIN %1$scirconscription ON bureau.circonscription=circonscription.id
            WHERE
                bureau.om_collectivite=%2$s
            ORDER BY
                bureau.code::integer',
            DB_PREFIXE,
            intval($_SESSION["collectivite"])
        );
        $res = $this->db->query($query);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        $this->isDatabaseError($res);
        $datas = array();
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $datas[] = $row;
        }
        return $datas;
    }

    /**
     * Récupère avec une requête toutes les listes enregistrées.
     * Si l'argument "listeOficcielle" est passé à true alors seules les
     * listes officielles seront récupérées.
     * Renvoies l'identifiant des listes et leur libellé sous la forme d'un
     * tableau.
     *
     * @param boolean si true alors seules les listes officielles seront
     * récupérées
     * @return array
     */
    function get_all__listes($listeOfficielle = false) {
        if (isset($this->_all__listes) && $this->_all__listes != null) {
            return $this->_all__listes;
        }
        $sqlFiltreListeOfficielle = '';
        if ($listeOfficielle === true) {
            $sqlFiltreListeOfficielle =
                'WHERE
                    liste.officielle IS TRUE';
        }
        $listes = $this->get_all_results_from_db_query(sprintf(
            'SELECT
                liste.liste,
                (liste.liste_insee || \' - \' || liste.libelle_liste) as libelle_liste
            FROM
                %1$sliste
            %2$s
            ORDER BY
                liste.liste',
            DB_PREFIXE,
            $sqlFiltreListeOfficielle
        ));
        $this->_all__listes = $listes["result"];
        return $this->_all__listes;
    }

    // }}}

    /**
     * Permet d'insérer ou de mettre à jour un paramètre.
     *
     * @param string $parameter
     * @param string $value
     *
     * @return array
     */
    function insert_or_update_parameter($parameter, $value) {
        //
        $cle = sprintf(
            ' om_parametre.libelle=\'%2$s\' AND om_parametre.om_collectivite=%1$s ',
            intval($_SESSION["collectivite"]),
            $parameter
        );
        $sql = sprintf(
            'SELECT count(*) FROM %1$som_parametre WHERE %2$s',
            DB_PREFIXE,
            $cle
        );
        $res = $this->db->getone($sql);
        $this->addToLog(
            __METHOD__."(): db->getone(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->isDatabaseError($res, true) === true) {
            return false;
        }
        if ($res == 0) {
            $valF = array(
                "om_parametre" => $this->db->nextid(DB_PREFIXE."om_parametre"),
                "libelle" => $parameter,
                "valeur" => $value,
                "om_collectivite" => intval($_SESSION["collectivite"]),
            );
            $res = $this->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, "om_parametre"),
                $valF,
                DB_AUTOQUERY_INSERT
            );
            $this->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "om_parametre")."\", ".print_r($valF, true).", DB_AUTOQUERY_INSERT);",
                VERBOSE_MODE
            );
        } else {
            $valF = array("valeur" => $value);
            $res = $this->db->autoexecute(
                sprintf('%1$s%2$s', DB_PREFIXE, "om_parametre"),
                $valF,
                DB_AUTOQUERY_UPDATE,
                $cle
            );
            $this->addToLog(
                __METHOD__."(): db->autoexecute(\"".sprintf('%1$s%2$s', DB_PREFIXE, "om_parametre")."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$cle."\");",
                VERBOSE_MODE
            );
        }
        if ($this->isDatabaseError($res, true) === true) {
            return false;
        }
        //
        if ($this->db->affectedRows() != 1) {
            return false;
        }
        return true;
    }

    /**
     * L'objectif de cette méthode est de transformer les informations de lieu
     * de naissance provenant du REU en informations gérables dans openElec.
     *
     * @param array $val Les valeurs en entrée :
     *                   - 'code_commune_de_naissance',
     *                   - 'libelle_commune_de_naissance',
     *                   - 'code_pays_de_naissance',
     *                   - 'libelle_pays_de_naissance'.
     *
     * @return array Les valeurs en sortie :
     *               - 'code_lieu_de_naissance',
     *               - 'libelle_lieu_de_naissance',
     *               - 'code_departement_naissance',
     *               - 'libelle_departement_naissance'.
     */
    function handle_birth_place(array $val = array()) {
        // Lieu de naissance
        $code_departement_naissance = "";
        $libelle_departement_naissance = "";
        $code_lieu_de_naissance = "";
        $libelle_lieu_de_naissance = "";
        // Né à l'étranger
        if (gavoe($val, array("code_commune_de_naissance", )) === "") {
            //
            $code_lieu_de_naissance = gavoe($val, array("code_pays_de_naissance", ));
            if (gavoe($val, array("libelle_commune_de_naissance", )) === "") {
                //
                $libelle_lieu_de_naissance = gavoe($val, array("libelle_pays_de_naissance", ));
            } else {
                $libelle_lieu_de_naissance = gavoe($val, array("libelle_commune_de_naissance", ));
            }
            $code_departement_naissance = gavoe($val, array("code_pays_de_naissance", ));
            $libelle_departement_naissance = gavoe($val, array("libelle_pays_de_naissance", ));
        } else {// Né en france
            $libelle_lieu_de_naissance = gavoe($val, array("libelle_commune_de_naissance", ));
            $code_lieu_de_naissance = gavoe($val, array("code_commune_de_naissance", ));
            if ($this->starts_with($code_lieu_de_naissance, "984") === true
                || $this->starts_with($code_lieu_de_naissance, "986") === true
                || $this->starts_with($code_lieu_de_naissance, "987") === true
                || $this->starts_with($code_lieu_de_naissance, "988") === true
                || $this->starts_with($code_lieu_de_naissance, "989") === true
                || $this->starts_with($code_lieu_de_naissance, "971") === true
                || $this->starts_with($code_lieu_de_naissance, "972") === true
                || $this->starts_with($code_lieu_de_naissance, "973") === true
                || $this->starts_with($code_lieu_de_naissance, "974") === true
                || $this->starts_with($code_lieu_de_naissance, "975") === true
                || $this->starts_with($code_lieu_de_naissance, "976") === true
                || $this->starts_with($code_lieu_de_naissance, "977") === true
                || $this->starts_with($code_lieu_de_naissance, "978") === true) {
                //
                $code_departement_naissance = substr($code_lieu_de_naissance, 0, 3);
                $code_lieu_de_naissance = $code_departement_naissance." ".substr($code_lieu_de_naissance, 3, 2);
            } elseif ($code_lieu_de_naissance != "91352"
                && $code_lieu_de_naissance != "92352"
                && $code_lieu_de_naissance != "93352"
                && $code_lieu_de_naissance != "94352"
                && $this->starts_with($code_lieu_de_naissance, "98") !== true) {
                //
                $code_departement_naissance = substr($code_lieu_de_naissance, 0, 2);
                $code_lieu_de_naissance = $code_departement_naissance." ".substr($code_lieu_de_naissance, 2, 3);
            } else {
                $code_departement_naissance = gavoe($val, array("code_commune_de_naissance", ));
            }
            $libelle_departement_naissance = $this->db->getone(
                sprintf(
                    'SELECT libelle_departement from %1$sdepartement where code=\'%2$s\'',
                    DB_PREFIXE,
                    $code_departement_naissance
                )
            );
            $this->isDatabaseError($libelle_departement_naissance);
            if ($libelle_departement_naissance == "") {
                $libelle_departement_naissance = "-";
            }
        }
        $oel_birth_place = array(
            "code_departement_naissance" => $code_departement_naissance,
            "libelle_departement_naissance" => $libelle_departement_naissance,
            "code_lieu_de_naissance" => $code_lieu_de_naissance,
            "libelle_lieu_de_naissance" => $libelle_lieu_de_naissance,
        );
        return $oel_birth_place;
    }

    /**
     * L'objectif de cette méthode est de transformer les informations de date
     * de naissance provenant du REU en informations gérables dans openElec.
     *
     * @param string $date Date au format 01/04/2010 avec des dates qui peuvent
     *                     être invalides comme 00/04/2010 ou 00/00/2010.
     *
     * @return string Date au format 2010-01-01 ou lorsque les dates en entrées
     *                sont invalides avec des dates valides mais défavorables
     *                2010-04-30 ou 2010-12-31.
     */
    function handle_birth_date($date) {
        //
        if ($date === '') {
            return '';
        }
        if ($date === null) {
            return null;
        }
        // Change le format pour être inséré dans la bdd
        $birth_date = $this->formatDate($date, false);
        if ($birth_date === false) {
            $birth_date_elems = explode("/", $date);
            // Si le jour n'est pas renseigné la valeur par défaut est 01
            $birth_date_elems[0] = str_replace("00", "01", $birth_date_elems[0]);
            // Si le mois n'est pas renseigné la valeur par défaut est 12 (mois
            // de décembre)
            $birth_date_elems[1] = str_replace("00", "12", $birth_date_elems[1]);
            $birth_date = implode("/", $birth_date_elems);
            // Change le format pour être inséré dans la bdd
            $birth_date = $this->formatDate($birth_date, false);
            // Récupère le dernier jour du mois
            $d = new DateTime($birth_date);
            $birth_date = $d->format('Y-m-t');
        }
        //
        return $birth_date;
    }

    /**
     * Indique si le tableau passé en paramètre est associatif ou non.
     * 
     * @param array $arr Tableau à vérifier.
     * 
     * @return boolean
     */
    function is_array_associative($arr) {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * Retourne l'affichage HTML du tableau passé en paramètre.
     * 
     * @param array $arr Tableau à afficher.
     *
     * @return string Tableau HTML
     */
    function get_display_array_as_table($arr) {
        $output = "";
        //
        $template_table = '<table class="table table-bordered table-sm table-striped">%s</table>';
        $template_line = '<tr>%s</tr>';
        $template_head = '<th>%s</th>';
        $template_cell = '<td>%s</td>';
        //
        $keys = array();
        if ($this->is_array_associative($arr) === true) {
            // isAssoc
            $keys = array_merge(
                $keys,
                array_keys($arr)
            );
        } else {
            // isList
            foreach ($arr as $list_elem) {
                if ($this->is_array_associative($list_elem) !== true) {
                    return "We do not do that";
                } else {
                    $keys = array_merge(
                        $keys,
                        array_keys($list_elem)
                    );
                }
            }
        }
        $keys = array_unique($keys);
        $table_content = "";
        if ($this->is_array_associative($arr) === true) {
            // isAssoc
            foreach ($arr as $assoc_key => $assoc_elem) {
                $line_content = sprintf(
                    $template_head,
                    $assoc_key
                );
                if (is_array($assoc_elem) === true) {
                    $line_content .= sprintf(
                        $template_cell,
                        $this->get_display_array_as_table($assoc_elem)
                    );
                } else {
                    $line_content .= sprintf(
                        $template_cell,
                        $assoc_elem
                    );
                }
                $table_content .= sprintf(
                    $template_line,
                    $line_content
                );
            }
        } else {
            // isList
            $headline_content = "";
            foreach($keys as $key) {
                $headline_content .= sprintf(
                    $template_head,
                    $key
                );
            }
            $table_content .= sprintf(
                $template_line,
                $headline_content
            );
            foreach ($arr as $list_elem) {
                $line_content = "";
                foreach ($keys as $column) {
                    if (array_key_exists($column, $list_elem) !== true) {
                        $line_content .= sprintf(
                            $template_cell,
                            ""
                        );
                    } elseif (is_array($list_elem[$column]) === true) {
                        $line_content .= sprintf(
                            $template_cell,
                            $this->get_display_array_as_table($list_elem[$column])
                        );
                    } else {
                        $line_content .= sprintf(
                            $template_cell,
                            $list_elem[$column]
                        );
                    }
                }
                $table_content .= sprintf(
                    $template_line,
                    $line_content
                );
            }
        }
        $output .= sprintf($template_table, $table_content);
        return $output;
    }

    function insert_reu_livrable($field, $demande_id, $obj, $id_scrutin = null) {
        if (empty($obj) || empty($field)) {
            return false;
        }
        $uid = $obj->getVal($field);
        // Mapping des champs entre la BDD et le CSV
        // CSV field => BDD field
        $map_fields = array(
            "type de mouvement" => "type_de_mouvement",
            "libellé du type de liste" => "libelle_du_type_de_liste",
            "nom de naissance" => "nom_de_naissance",
            "nom d'usage" => "nom_d_usage",
            "prénoms" => "prenoms",
            "sexe" => "sexe",
            "date de naissance" => "date_de_naissance",
            "commune de naissance" => "commune_de_naissance",
            "code de la commune de naissance" => "code_de_la_commune_de_naissance",
            "département de naissance" => "departement_de_naissance",
            "pays de naissance" => "pays_de_naissance",
            "code du bureau de vote" => "code_du_bureau_de_vote",
            "libellé du bureau de vote" => "libelle_du_bureau_de_vote",
            "numéro d'ordre dans le bureau de vote" => "numero_d_ordre_dans_le_bureau_de_vote",
            "motif" => "motif",
            "libellé du scrutin" => "libelle_du_scrutin",
            "date du premier tour" => "date_du_premier_tour",
            "date du second tour" => "date_du_second_tour",
            "numéro de voie" => "numero_de_voie",
            "libellé de voie" => "libelle_de_voie",
            "complément 1" => "complement_1",
            "complément 2" => "complement_2",
            "lieu-dit" => "lieu_dit",
            "code postal" => "code_postal",
            "commune" => "commune",
            "pays" => "pays",
            "code du département" => "code_du_departement",
            "libellé de l'ugle" => "libelle_de_l_ugle",
            "code du département de naissance" => "code_de_departement_de_naissance",
            "majeur" => "majeur",
            "mentionCode" => "mention_code",
            "circonscription législative du bureau de vote" => "circonscription_du_bureau_de_vote",
            "circonscription du bureau de vote" => "circonscription_du_bureau_de_vote",
            "canton du bureau de vote" => "canton_du_bureau_de_vote",
            "identifiant nationalité" => "identifiant_nationalite",
            "libellé de la nationalité" => "libelle_de_la_nationalite",
            "nom naissance mandataire tour1" => "nom_naissance_mandataire_tour1",
            "nom usage mandataire tour1" => "nom_usage_mandataire_tour1",
            "prénoms mandataire tour1" => "prenoms_mandataire_tour1",
            "sexe mandataire tour1" => "sexe_mandataire_tour1",
            "date naissance mandataire tour1" => "date_naissance_mandataire_tour1",
            "nom naissance mandataire tour2" => "nom_naissance_mandataire_tour2",
            "nom usage mandataire tour2" => "nom_usage_mandataire_tour2",
            "prénoms mandataire tour2" => "prenoms_mandataire_tour2",
            "sexe mandataire tour2" => "sexe_mandataire_tour2",
            "date naissance mandataire tour2" => "date_naissance_mandataire_tour2",
            "date validité procuration" => "date_validite_procuration",
            "Libellé circonscription métropolitaine du bureau de vote" => "libelle_circonscription_metropolitaine_du_bureau_de_vote",
        );
        $ignored_fields = array(
            "mention validité procuration EUR"
        );
        // Mapping des valeurs des listes
        // CSV field => BDD field
        $liste_ids = array(
            "Liste principale" => '01',
            "Liste complémentaire européenne" => '02',
            "Liste complémentaire municipale" => '03',
        );
        // Séparateur CSV
        $delimiter = ';';

        // Récupère le fichier CSV depuis le zip
        $zip = new ZipArchive();
        $zip->open($this->storage->getPath($uid));
        // Cherche le fichier correspondant dans l'archive
        $file = null;
        for ($i = 0; $i < $zip->numFiles; $i++) { // Check file by file
            $filename = $zip->getNameIndex($i); // Retrieve entry name
            if (strtolower(substr($filename, -3)) === "csv" 
                && strtolower(substr($filename, 0, 5)) === "liste"
                && strtolower(substr($filename, -4, -14) != "annexe_ine")) {
                //
                $file = $zip->getStream($filename);
                break;
            }
        }
        if ($file === null) {
            return false;
        }

        // Complète le CSV avec l'identifiant de l'électeur et l'identifiant de la collectivité
        $n_file = tmpfile();
        $nb_line = 0;

        while (($line = fgetcsv($file, 0, $delimiter)) !== false) {
            if ($nb_line === 0) {
                $header = $line;
            }
            $new_line = $line;
            foreach ($ignored_fields as $value) {
                unset($new_line[array_search($value, $header)]);
            }
            if ($nb_line === 0) {
                $new_line[] = "scrutin_id";
                $new_line[] = "demande_id";
                $new_line[] = "liste";
                $new_line[] = "electeur_id";
                $new_line[] = "om_collectivite";
            } else {
                // $line[] = $reu_scrutin["id"];
                $new_line[] = $id_scrutin;
                $new_line[] = $demande_id;
                $new_line[] = $liste_ids[$line[array_search(array_search('libelle_du_type_de_liste', $map_fields), $header)]];
                $new_line[] = $this->get_electeur_id_by_liste_and_bureau_code_and_numero_ordre(
                    $liste_ids[$line[array_search(array_search('libelle_du_type_de_liste', $map_fields), $header)]],
                    $line[array_search(array_search('code_du_bureau_de_vote', $map_fields), $header)],
                    $line[array_search(array_search('numero_d_ordre_dans_le_bureau_de_vote', $map_fields), $header)]
                );
                $new_line[] = $_SESSION["collectivite"];
            }
            // Ecrit dans le fichier csv
            $put_csv = fputcsv($n_file, $new_line, $delimiter);
            if ($put_csv === false) {
                return false;
            }
            //
            $nb_line++;
        }
        rewind($n_file);
        $n_file_content = stream_get_contents($n_file);

        // Métadonnées du fichier csv
        $n_file_metadata['filename'] = $filename;
        $n_file_metadata['size'] = strlen($n_file_content);
        $n_file_metadata['mimetype'] = "text/csv";
        // Création du fichier sur le storage temporaire
        $n_file_uid = $this->storage->create_temporary($n_file_content, $n_file_metadata);

        // Compose les champs de la table à saisir dans le COPY
        rewind($n_file);
        $n_header = fgetcsv($n_file, 0, $delimiter);
        $fields = array();
        foreach ($n_header as $key => $column) {
            if (array_key_exists($column, $map_fields) === true) {
                $fields[$key] = $map_fields[$column];
            } else {
                $fields[$key] = $column;
            }
        }
        // Requête SQL
        // COPY pour insérer données du CSV dans la table
        $query_copy = sprintf('
            COPY %1$sreu_livrable(%2$s)
            FROM \'%3$s\' DELIMITER AS \'%4$s\' CSV HEADER QUOTE \'"\';
            ',
            DB_PREFIXE,
            implode(', ', $fields),
            realpath($this->storage->getPath_temporary($n_file_uid)),
            $delimiter
        );
        $res = $this->db->query($query_copy);
        if ($this->isDatabaseError($res, true) === true) {
            return false;
        }
        //
        fclose($n_file);
        fclose($file);
        return true;
    }


    function get_electeur_id_by_liste_and_bureau_code_and_numero_ordre($liste, $bureau_code, $numero_ordre) {
        $query = sprintf('
            SELECT electeur.id
            FROM %1$selecteur
            INNER JOIN %1$sbureau ON electeur.bureau = bureau.id
            WHERE electeur.liste=\'%5$s\'
                AND bureau.code=\'%2$s\'
                AND electeur.numero_bureau=%3$s
                AND electeur.om_collectivite=%4$s
            ',
            DB_PREFIXE,
            $bureau_code,
            intval($numero_ordre),
            intval($_SESSION["collectivite"]),
            $liste
        );
        $res = $this->get_one_result_from_db_query($query, true);
        if ($res['code'] !== 'OK') {
            return false;
        }
        return $res['result'];
    }
}
