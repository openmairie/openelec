<?php
/**
 * Ce script définit la classe 'reuSyncBureauTraitement'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once "../obj/traitement.class.php";

/**
 * Définition de la classe 'reuSyncBureauTraitement' (traitement).
 *
 * Surcharge de la classe 'traitement'.
 */
class reuSyncBureauTraitement extends traitement {
    var $fichier = "reu_sync_bureau";
    
    /**
     *
     */
    function prerequis() {
        $query = sprintf(
            'INSERT INTO  %1$scanton(id, code, libelle)
            SELECT nextval(\'%1$scanton_seq\'), \'NC\', \'NON COMMUNIQUE\'
            WHERE NOT EXISTS (SELECT code FROM %1$scanton WHERE code=\'NC\');',
            DB_PREFIXE
        );
        $res = $this->f->db->query($query);
        $this->f->isDatabaseError($res);

        $query = sprintf(
            'INSERT INTO  %1$scirconscription(id, code, libelle)
            SELECT  nextval(\'%1$scirconscription_seq\'), \'NC\', \'NON COMMUNIQUEE\'
            WHERE NOT EXISTS (SELECT code FROM %1$scirconscription WHERE code=\'NC\');',
            DB_PREFIXE
        );
        $res = $this->f->db->query($query);
        $this->f->isDatabaseError($res);
    }  


    /**
     *
     */
    function treatment () {
        $has_not_canton_circon = false;
        //
        $this->LogToFile("start reu_sync_bureau");
        //
        $this->prerequis();
        if ($this->error === true) {
            $this->LogToFile("end reu_sync_l_e_valid");
            return;
        }
        //
        $inst_reu = $this->f->get_inst__reu();
        if ($inst_reu === null) {
            return "";
        }
        //
        if ($inst_reu->is_api_up() !== true
            || $inst_reu->is_connexion_logicielle_valid() !== true) {
            return "";
        }
        //
        $ugle_infos = $inst_reu->get_referentiel_ugle();
        if ($ugle_infos["code"] == 200) {
            //
            $cantons = gavoe($ugle_infos, array("result", "canton", ), array());
            foreach ($cantons as $key => $value) {
                $query = sprintf(
                    'INSERT INTO %1$scanton(id, code, libelle) SELECT nextval(\'%1$scanton_seq\'), \'%2$s\', \'%3$s\' WHERE NOT EXISTS (SELECT code FROM %1$scanton WHERE code=\'%2$s\');',
                    DB_PREFIXE,
                    $this->f->db->escapeSimple($value["code"]),
                    $this->f->db->escapeSimple($value["libelle"])
                );
                $res = $this->f->db->query($query);
                if ($this->f->isDatabaseError($res, true) !== false) {
                    $this->error = true;
                    return;
                }
            }
            $circonscriptions = gavoe($ugle_infos, array("result", "circonscriptionLegislative", ), array());
            foreach ($circonscriptions as $key => $value) {
                $query = sprintf(
                    'INSERT INTO %1$scirconscription(id, code, libelle) SELECT nextval(\'%1$scirconscription_seq\'), \'%2$s\', \'%3$s\' WHERE NOT EXISTS (SELECT code FROM %1$scirconscription WHERE code=\'%2$s\');',
                    DB_PREFIXE,
                    $this->f->db->escapeSimple($value["code"]),
                    $this->f->db->escapeSimple($value["libelle"])
                );
                $res = $this->f->db->query($query);
                if ($this->f->isDatabaseError($res, true) !== false) {
                    $this->error = true;
                    return;
                }
            }
        }
        //
        $bureaux_de_vote_reu = $inst_reu->get_bureaux_de_vote();
        //
        $bureaux_de_vote_oel = $this->f->get_all__bureau__by_my_collectivite();
        if (count($bureaux_de_vote_oel) == 0) {
            // => On crée les bureaux de vote du REU dans openElec
            foreach ($bureaux_de_vote_reu["result"] as $reu_bureau) {
                $code_circonscription = gavoe($reu_bureau, array("circonscriptionLegislative", "code", ), "NC");
                $id_circonscription = $this->f->get_one_result_from_db_query(sprintf(
                    'SELECT id FROM %1$scirconscription WHERE code=\'%2$s\'',
                    DB_PREFIXE,
                    $code_circonscription
                ));
                if ($id_circonscription["code"] != "OK" || $id_circonscription["result"] == ""  || $code_circonscription == "NC") {
                    $this->LogToFile("Toutes les circonscriptions ne sont pas renseignées dans le REU.");
                    $has_not_canton_circon = true;
                }
                $code_canton = gavoe($reu_bureau, array("canton", "code", ), "NC");
                $id_canton = $this->f->get_one_result_from_db_query(sprintf(
                    'SELECT id FROM %1$scanton WHERE code=\'%2$s\'',
                    DB_PREFIXE,
                    $code_canton
                ));
                if ($id_canton["code"] != "OK" || $id_canton["result"] == ""  || $code_canton == "NC") {
                    $this->LogToFile("Tous les cantons ne sont pas renseignés dans le REU.");
                    $has_not_canton_circon = true;
                }
                $inst_bureau = $this->f->get_inst__om_dbform(array(
                    "obj" => "bureau",
                    "idx" => "]",
                ));
                $val = array(
                    "origin" => "from_reu",
                    "id" => "",
                    "code" => $reu_bureau["code"],
                    "libelle" => $reu_bureau["libelle"],
                    "adresse1" => "",
                    "adresse2" => "",
                    "adresse3" => "",
                    "surcharge_adresse_carte_electorale"=>"",
                    "adresse_numero_voie" => gavoe($reu_bureau, array("adresse", "numVoie", )),
                    "adresse_libelle_voie" => gavoe($reu_bureau, array("adresse", "libVoie", )),
                    "adresse_complement1" => gavoe($reu_bureau, array("adresse", "complement1", )),
                    "adresse_complement2" => gavoe($reu_bureau, array("adresse", "complement2", )),
                    "adresse_lieu_dit" => gavoe($reu_bureau, array("adresse", "lieuDit", )),
                    "adresse_code_postal" => gavoe($reu_bureau, array("adresse", "codePostal", )),
                    "adresse_ville" => gavoe($reu_bureau, array("adresse", "commune", )),
                    "adresse_pays" => gavoe($reu_bureau, array("adresse", "pays", )),
                    "canton" => intval($id_canton["result"]),
                    "circonscription" => intval($id_circonscription["result"]),
                    "om_collectivite" => intval($_SESSION["collectivite"]),
                    "referentiel_id" => $reu_bureau["id"],
                );
                $ret = $inst_bureau->ajouter($val);
                if ($ret !== true) {
                    $this->error = true;
                    $this->LogToFile($inst_bureau->msg);
                    $this->LogToFile("end reu_sync_bureau");
                    return;
                }
                $inst_bureau->__destruct();
            }
            $bureaux_de_vote_oel = $this->f->get_all__bureau__by_my_collectivite();
        }
        $global_error_status = false;
        //
        $bureaux_oel_manquants = array();
        $bureaux_reu_manquants = array();
        if ($bureaux_de_vote_reu["code"] == 200) {
            foreach ($bureaux_de_vote_oel as $oel_bureau) {
                $exists = false;
                $inst_bureau = $this->f->get_inst__om_dbform(array(
                    "obj" => "bureau",
                    "idx" => $oel_bureau["id"],
                ));
                foreach ($bureaux_de_vote_reu["result"] as $reu_bureau) {
                    if (($oel_bureau["referentiel_id"] !== ""
                        && $oel_bureau["referentiel_id"] == $reu_bureau["id"])
                        || ($oel_bureau["referentiel_id"] == ""
                        && $oel_bureau["code"] == $reu_bureau["code"])) {
                        //
                        $code_circonscription = gavoe($reu_bureau, array("circonscriptionLegislative", "code", ), "NC");
                        $id_circonscription = $this->f->get_one_result_from_db_query(sprintf(
                            'SELECT id FROM %1$scirconscription WHERE code=\'%2$s\'',
                            DB_PREFIXE,
                            $code_circonscription
                        ));
                        if ($id_circonscription["code"] != "OK" || $id_circonscription["result"] == ""  || $code_circonscription == "NC") {
                            $this->LogToFile("Toutes les circonscriptions ne sont pas renseignées dans le REU.");
                            $has_not_canton_circon = true;
                        }
                        $code_canton = gavoe($reu_bureau, array("canton", "code", ), "NC");
                        $id_canton = $this->f->get_one_result_from_db_query(sprintf(
                            'SELECT id FROM %1$scanton WHERE code=\'%2$s\'',
                            DB_PREFIXE,
                            $code_canton
                        ));
                        if ($id_canton["code"] != "OK" || $id_canton["result"] == "" || $code_canton == "NC") {
                            $this->LogToFile("Tous les cantons ne sont pas renseignés dans le REU.");
                            $has_not_canton_circon = true;
                        }
                        //
                        $val = array(
                            "origin" => "from_reu",
                            "id" => $inst_bureau->getVal($inst_bureau->clePrimaire),
                            "code" => $reu_bureau["code"],
                            "libelle" => $reu_bureau["libelle"],
                            "surcharge_adresse_carte_electorale" => $inst_bureau->getVal("surcharge_adresse_carte_electorale"),
                            "adresse1" => $inst_bureau->getVal("adresse1"),
                            "adresse2" => $inst_bureau->getVal("adresse2"),
                            "adresse3" => $inst_bureau->getVal("adresse3"),
                            "adresse_numero_voie" => gavoe($reu_bureau, array("adresse", "numVoie", )),
                            "adresse_libelle_voie" => gavoe($reu_bureau, array("adresse", "libVoie", )),
                            "adresse_complement1" => gavoe($reu_bureau, array("adresse", "complement1", )),
                            "adresse_complement2" => gavoe($reu_bureau, array("adresse", "complement2", )),
                            "adresse_lieu_dit" => gavoe($reu_bureau, array("adresse", "lieuDit", )),
                            "adresse_code_postal" => gavoe($reu_bureau, array("adresse", "codePostal", )),
                            "adresse_ville" => gavoe($reu_bureau, array("adresse", "commune", )),
                            "adresse_pays" => gavoe($reu_bureau, array("adresse", "pays", )),
                            "canton" => intval($id_canton["result"]),
                            "circonscription" => intval($id_circonscription["result"]),
                            "om_collectivite" => intval($_SESSION["collectivite"]),
                            "referentiel_id" => $reu_bureau["id"],
                        );
                        $this->LogToFile(print_r($val, true));
                        $inst_bureau->setParameter("maj", 1);
                        $ret = $inst_bureau->modifier($val);
                        if ($ret === true) {
                            $message = sprintf(
                                __("Mise à jour du bureau %s dans openElec (%s) avec son identifiant REU (%s)"),
                                $oel_bureau["code"],
                                $oel_bureau["id"],
                                $reu_bureau["id"]
                            );
                            $this->LogToFile($message);
                            $exists = true;
                            continue;
                        } else {
                            $message = sprintf(
                                __("=> Erreur lors de la mise à jour du bureau %s dans openElec (%s) avec son identifiant REU (%s)"),
                                $oel_bureau["code"],
                                $oel_bureau["id"],
                                $reu_bureau["id"]
                            );
                            $this->LogToFile($message);
                        }
                    }
                }
                $inst_bureau->__destruct();
                if ($exists !== true) {
                    $global_error_status = true;
                    $bureaux_oel_manquants[] = $oel_bureau["code"];
                }
            }
            foreach ($bureaux_de_vote_reu["result"] as $reu_bureau) {
                $exists = false;
                foreach ($bureaux_de_vote_oel as $oel_bureau) {
                    // Comparaison entre le code du bureau dans l'application et celui issus du REU
                    // pour vérifier si ils correspondent.
                    // Un trim est appliqué pour éviter des erreurs de comparaison liées à
                    // des espaces non nécessaire.
                    if ($oel_bureau["code"] == trim($reu_bureau["code"])) {
                        $exists = true;
                        continue;
                    }
                }
                if ($exists !== true) {
                    $global_error_status = true;
                    $bureaux_reu_manquants[] = $reu_bureau["code"];
                }
            }
            if (count($bureaux_de_vote_oel) != count($bureaux_de_vote_reu["result"])) {
                $global_error_status = true;
            }
            //
            if ($global_error_status === false) {
                $message = __("Les bureaux de vote et leurs codes sont identiques entre openElec et le REU.");
                $this->addToMessage($message);
                $this->LogToFile($message);
                if ($has_not_canton_circon == true){
                    $this->f->insert_or_update_parameter("reu_sync_bureaux", "false");
                    $message = __("Toutes les informations de cantons et de circonscriptions ne sont pas renseignées dans le REU.");
                    $this->addToMessage($message);
                    $this->LogToFile($message);
                }  else {
                    $this->f->insert_or_update_parameter("reu_sync_bureaux", "true");
                }
                $this->LogToFile("end reu_sync_bureau");
                return;
            } else {
                $this->rollbackTransaction();
                $this->f->insert_or_update_parameter("reu_sync_bureaux", "false");
                $this->commitTransaction();
                $this->error = true;
                $message = __("Les bureaux de vote et/ou leurs codes ne sont pas identiques entre openElec et le REU.");
                $this->LogToFile($message);
                $this->addToMessage($message);
                if (count($bureaux_de_vote_oel) != count($bureaux_de_vote_reu["result"])) {
                    $message = sprintf(
                        __("Le nombre de bureaux de vote est différent entre openElec (%s) et le REU (%s)."),
                        count($bureaux_de_vote_oel),
                        count($bureaux_de_vote_reu["result"])
                    );
                    $this->addToMessage($message);
                    $this->LogToFile($message);
                }
                if ($has_not_canton_circon == true){
                    $message = __("Toutes les informations de cantons et de circonscriptions ne sont pas renseignées dans le REU.");
                    $this->addToMessage($message);
                    $this->LogToFile($message);
                }
                $this->LogToFile("end reu_sync_bureau");
                return;
            }
        }
        //
        $this->LogToFile("end reu_sync_bureau");
    }
}
