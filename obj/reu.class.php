<?php
/**
 * Ce script définit la classe 'reu'.
 *
 * @package openelec
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_base.class.php";

/**
 * Définition de la classe 'reu'.
 */
class reu {

    /**
     * Cet attribut permet de stocker l'instance du connecteur REU utilisé.
     * Sa valeur doit être remplie en fonction du paramétrage de la collectivité.
     *
     * @var object  instance du connecteur REU
     */
    var $reu = null;

    /**
     *
     */
    public function __construct(array $conf) {
        //
        if (array_key_exists("connector", $conf) !== true
            || array_key_exists("path", $conf) !== true) {
            return null;
        }
        $connector_full_path = $conf["path"].$conf["connector"].'.class.php';
        if (file_exists($connector_full_path) !== true) {
            return null;
        }
        require_once $connector_full_path;
        $this->reu = new $conf["connector"]($conf);
    }

    public function __destruct() {
        //
        if($this->reu !== null){
            $this->reu->__destruct();
        }
    }

    /**
     *
     */
    public function get_ugle() {
        return $this->reu->get_ugle();
    }

    /**
     *
     */
    public function set_ugle($ugle) {
        return $this->reu->set_ugle($ugle);
    }

    /**
     *
     */
    public function get_compte_logiciel() {
        return $this->reu->get_compte_logiciel();
    }

    /**
     *
     */
    public function set_compte_logiciel($username, $password) {
        return $this->reu->set_compte_logiciel($username, $password);
    }

    /**
     *
     */
    public function is_config_valid() {
        return $this->reu->is_config_valid();
    }

    /**
     *
     */
    public function is_api_up() {
        return $this->reu->is_api_up();
    }

    /**
     *
     */
    public function is_connexion_logicielle_valid() {
        return $this->reu->is_connexion_logicielle_valid();
    }

    /**
     *
     */
    public function is_connexion_standard_valid() {
        return $this->reu->is_connexion_standard_valid();
    }

    /**
     *
     */
    public function logout_connexion_logicielle() {
        return $this->reu->logout_connexion_logicielle();
    }

    /**
     *
     */
    public function display_userpage_widget() {
        return $this->reu->display_userpage_widget();
    }

    /**
     *
     */
    public function display_connexion_standard() {
        return $this->reu->display_connexion_standard();
    }

    /**
     *
     */
    public function display_deconnexion_standard() {
        return $this->reu->display_deconnexion_standard();
    }

    /**
     *
     */
    public function get_liste_in_csv($liste = "LP") {
        return $this->reu->get_liste_in_csv($liste);
    }

    /**
     *
     */
    public function get_liste_electeurs_sans_bureau() {
        return $this->reu->get_liste_electeurs_sans_bureau();
    }
    /**
     *
     */
    public function get_referentiel_lieu_in_csv($referentiel_type) {
        return $this->reu->get_referentiel_lieu_in_csv($referentiel_type);
    }

    /**
     *
     */
    public function get_referentiel_ugle() {
        return $this->reu->get_referentiel_ugle();
    }

    /**
     *
     */
    public function get_actualites() {
        return $this->reu->get_actualites();
    }

    /**
     *
     */
    public function get_indicateurs() {
        return $this->reu->get_indicateurs();
    }

    /**
     *
     */
    public function get_bureaux_de_vote() {
        return $this->reu->get_bureaux_de_vote();
    }

    /**
     *
     */
    public function get_bureaux_de_vote_in_csv() {
        return $this->reu->get_bureaux_de_vote_in_csv();
    }

    /**
     *
     */
    public function get_bureaux_de_vote_indicateurs_in_csv() {
        return $this->reu->get_bureaux_de_vote_indicateurs_in_csv();
    }

    /**
     *
     */
    public function get_procurations_in_csv() {
        return $this->reu->get_procurations_in_csv();
    }

    /**
     *
     */
    public function get_referentiel_reference($type) {
        return $this->reu->get_referentiel_reference($type);
    }

    /**
     *
     */
    public function get_nationalites() {
        return $this->reu->get_nationalites();
    }

    /**
     *
     */
    public function get_notifications($params = array()) {
        return $this->reu->get_notifications($params);
    }

    /**
     *
     */
    public function handle_bureau_de_vote($params = array()) {
        return $this->reu->handle_bureau_de_vote($params);
    }

    /**
     *
     */
    public function handle_electeur($params = array()) {
        return $this->reu->handle_electeur($params);
    }

    /**
     *
     */
    public function handle_inscription($params = array()) {
        return $this->reu->handle_inscription($params);
    }

    /**
     *
     */
    public function handle_procuration($params = array()) {
        return $this->reu->handle_procuration($params);
    }

    /**
     *
     */
    public function handle_radiation($params = array()) {
        return $this->reu->handle_radiation($params);
    }

    /**
     *
     */
    public function handle_scrutin($params = array()) {
        return $this->reu->handle_scrutin($params);
    }

   /**
     *
     */
    public function handle_livrable($params = array()) {
        return $this->reu->handle_livrable($params);
    }

}

/**
 * Classe parente de tous les connecteurs REU
 */
class reu_base extends om_base {

    /**
     *
     */
    public function __construct(array $conf) {
        $this->init_om_application();
        $this->reu_config = $conf;
    }

    public function __destruct() {
        return null;
    }
    /**
     *
     */
    protected function get_config($elem = null) {
        //
        if (isset($this->reu_config) === false
            || $this->reu_config === null) {
            //
            $this->reu_config = $reu_config;
        }
        if ($elem === null) {
            return $this->reu_config;
        }
        if (array_key_exists($elem, $this->reu_config) !== true) {
            return null;
        }
        return $this->reu_config[$elem];
    }

    /**
     * @return string|null
     */
    public function get_ugle() {
        if ($this->f->getParameter("ugle") == "") {
            return null;
        }
        return $this->f->getParameter("ugle");
    }

    /**
     * @return boolean
     */
    public function set_ugle($ugle) {
        return $this->f->insert_or_update_parameter("ugle", trim($ugle));
    }

    /**
     * @return array|null
     */
    public function get_compte_logiciel() {
        if ($this->f->getParameter("reu_compte_logiciel") == "") {
            return null;
        }
        $compte_logiciel = explode("\n", $this->f->getParameter("reu_compte_logiciel"));
        return array(
            "username" => (isset($compte_logiciel[0]) ? trim($compte_logiciel[0]) : ""),
            "password" => (isset($compte_logiciel[1]) ? trim($compte_logiciel[1]) : ""),
        );
    }

    /**
     * @return boolean
     */
    public function set_compte_logiciel($username, $password) {
        $compte_logiciel = trim($username)."\n".trim($password);
        return $this->f->insert_or_update_parameter("reu_compte_logiciel", $compte_logiciel);
    }

    public function is_config_valid() {
        return null;
    }
    public function is_api_up() {
        return null;
    }
    public function is_connexion_logicielle_valid() {
        return null;
    }
    public function is_connexion_standard_valid() {
        return null;
    }
    public function logout_connexion_logicielle() {
        return null;
    }
    public function display_userpage_widget() {
        return null;
    }
    public function display_connexion_standard() {
        return null;
    }
    public function display_deconnexion_standard() {
        return null;
    }
    public function get_liste_in_csv($liste = "LP") {
        return null;
    }
    public function get_liste_electeurs_sans_bureau() {
        return null;
    }
    public function get_referentiel_lieu_in_csv($referentiel_type) {
        return null;
    }
    public function get_referentiel_ugle() {
        return null;
    }
    public function get_actualites() {
        return null;
    }
    public function get_indicateurs() {
        return null;
    }
    public function get_bureaux_de_vote() {
        return null;
    }
    public function get_bureaux_de_vote_indicateurs_in_csv() {
        return null;
    }
    public function get_bureaux_de_vote_in_csv() {
        return null;
    }
    public function get_procurations_in_csv() {
        return null;
    }
    public function get_referentiel_reference($type) {
        return null;
    }
    public function get_nationalites() {
        return null;
    }
    public function get_notifications($params = array()) {
        return null;
    }
    public function handle_bureau_de_vote($params = array()) {
        return null;
    }
    public function handle_electeur($params = array()) {
        return null;
    }
    public function handle_inscription($params = array()) {
        return null;
    }
    public function handle_procuration($params = array()) {
        return null;
    }
    public function handle_radiation($params = array()) {
        return null;
    }
    public function handle_scrutin($params = array()) {
        return null;
    }
    public function handle_livrable($params = array()) {
        return null;
    }
}
