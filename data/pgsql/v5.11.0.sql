--
-- Fichier de passage de version SQL : v5.10.0 > v5.11.0
--
-- @package openelec
-- @version SVN : $Id$
--

-- Ticket #9842 - BEGIN
ALTER TABLE electeur
    ADD COLUMN date_saisie_carte_retour date;
COMMENT ON COLUMN electeur.date_saisie_carte_retour IS 'Date de saisie de la carte en retour.';
-- Ticket #9842 - END
