--
-- Fichier de passage de version SQL : v5.3.x > v5.4.0
--
-- @package openelec
-- @version SVN : $Id$
--

--
DROP TRIGGER code_bureau_changes ON openelec.electeur;
DROP TRIGGER code_bureau_changes ON openelec.mouvement;
DROP FUNCTION openelec.update_code_bureau();
--
ALTER TABLE openelec.electeur DROP COLUMN code_bureau;
--
ALTER TABLE openelec.mouvement RENAME COLUMN code_bureau TO bureau_de_vote_code;
ALTER TABLE openelec.mouvement ALTER COLUMN bureau_de_vote_code DROP NOT NULL;
ALTER TABLE openelec.mouvement ALTER COLUMN bureau_de_vote_code DROP DEFAULT;
ALTER TABLE openelec.mouvement ALTER COLUMN bureau_de_vote_code TYPE character varying(20);
--
ALTER TABLE openelec.mouvement RENAME COLUMN ancien_bureau TO ancien_bureau_de_vote_code;
ALTER TABLE openelec.mouvement ALTER COLUMN ancien_bureau_de_vote_code DROP NOT NULL;
ALTER TABLE openelec.mouvement ALTER COLUMN ancien_bureau_de_vote_code DROP DEFAULT;
ALTER TABLE openelec.mouvement ALTER COLUMN ancien_bureau_de_vote_code TYPE character varying(20);
--
ALTER TABLE openelec.mouvement ADD COLUMN ancien_bureau integer;
ALTER TABLE openelec.mouvement ADD COLUMN bureau_de_vote_libelle character varying(100);
ALTER TABLE openelec.mouvement ADD COLUMN ancien_bureau_de_vote_libelle character varying(100);
--
ALTER TABLE openelec.archive DROP COLUMN bureau;
--
ALTER TABLE openelec.archive RENAME COLUMN ancien_bureau TO ancien_bureau_de_vote_code;
ALTER TABLE openelec.archive ALTER COLUMN ancien_bureau_de_vote_code DROP NOT NULL;
ALTER TABLE openelec.archive ALTER COLUMN ancien_bureau_de_vote_code DROP DEFAULT;
ALTER TABLE openelec.archive ALTER COLUMN ancien_bureau_de_vote_code TYPE character varying(20);
--
ALTER TABLE openelec.archive RENAME COLUMN code_bureau TO bureau_de_vote_code;
ALTER TABLE openelec.archive ALTER COLUMN bureau_de_vote_code DROP NOT NULL;
ALTER TABLE openelec.archive ALTER COLUMN bureau_de_vote_code DROP DEFAULT;
ALTER TABLE openelec.archive ALTER COLUMN bureau_de_vote_code TYPE character varying(20);
--
ALTER TABLE openelec.archive ADD COLUMN bureau_de_vote_libelle character varying(100);
--
ALTER TABLE openelec.archive ADD COLUMN ancien_bureau_de_vote_libelle character varying(100);
--
ALTER TABLE ONLY openelec.mouvement
    ADD CONSTRAINT mouvement_ancien_bureau_fkey FOREIGN KEY (ancien_bureau) REFERENCES openelec.bureau(id);
--
UPDATE openelec.mouvement SET bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=mouvement.bureau);
UPDATE openelec.mouvement SET bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=mouvement.bureau);
UPDATE openelec.mouvement SET bureau = NULL WHERE etat<>'actif';
--
UPDATE openelec.mouvement SET ancien_bureau = (SELECT bureau.id FROM openelec.bureau WHERE bureau.code=mouvement.ancien_bureau_de_vote_code);
--
UPDATE openelec.mouvement SET ancien_bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=mouvement.ancien_bureau);
UPDATE openelec.mouvement SET ancien_bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=mouvement.ancien_bureau);
UPDATE openelec.mouvement SET ancien_bureau = NULL WHERE etat<>'actif';
--
CREATE FUNCTION openelec.handle_bureau_for_one_mouvement() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
if NEW.etat = 'actif' then
 NEW.bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=NEW.bureau);
 NEW.bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=NEW.bureau);
 NEW.ancien_bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=NEW.ancien_bureau);
 NEW.ancien_bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=NEW.ancien_bureau);
end if;
if (NEW.etat = 'trs' OR NEW.etat = 'na') AND (TG_OP = 'INSERT' OR OLD.etat<>NEW.etat) then
 NEW.bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=NEW.bureau);
 NEW.bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=NEW.bureau);
 NEW.ancien_bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=NEW.ancien_bureau);
 NEW.ancien_bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=NEW.ancien_bureau);
 NEW.bureau = NULL;
 NEW.ancien_bureau = NULL;
end if;
 RETURN NEW;
END;
$$;
--
CREATE FUNCTION openelec.handle_bureau_for_all_mouvements() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE
        openelec.mouvement
    SET
        bureau_de_vote_code = NEW.code,
        bureau_de_vote_libelle = NEW.libelle
    WHERE
        bureau = NEW.id
        AND etat='actif';
    UPDATE
        openelec.mouvement
    SET
        ancien_bureau_de_vote_code = NEW.code,
        ancien_bureau_de_vote_libelle = NEW.libelle
    WHERE
        ancien_bureau = NEW.id
        AND etat='actif';
  RETURN NEW;
END;
$$;
--
CREATE TRIGGER handle_bureau_one_mouvement_changes BEFORE INSERT OR UPDATE ON openelec.mouvement FOR EACH ROW EXECUTE PROCEDURE openelec.handle_bureau_for_one_mouvement();
CREATE TRIGGER handle_bureau_all_mouvements_changes AFTER UPDATE ON openelec.bureau FOR EACH ROW EXECUTE PROCEDURE openelec.handle_bureau_for_all_mouvements();
--
ALTER TABLE openelec.canton DROP COLUMN code_insee;
ALTER TABLE ONLY openelec.bureau ADD COLUMN surcharge_adresse_carte_electorale boolean;
COMMENT ON COLUMN openelec.bureau.surcharge_adresse_carte_electorale IS 'Marqueur de surcharge de l''adresse du bureau de vote standard sur l''édition de la carte électorale';
ALTER TABLE ONLY openelec.bureau ADD COLUMN adresse_numero_voie character varying(20);
ALTER TABLE ONLY openelec.bureau ADD COLUMN adresse_libelle_voie character varying(150);
ALTER TABLE ONLY openelec.bureau ADD COLUMN adresse_complement1 character varying(50);
ALTER TABLE ONLY openelec.bureau ADD COLUMN adresse_complement2 character varying(50);
ALTER TABLE ONLY openelec.bureau ADD COLUMN adresse_lieu_dit character varying(50);
ALTER TABLE ONLY openelec.bureau ADD COLUMN adresse_code_postal character varying(20);
ALTER TABLE ONLY openelec.bureau ADD COLUMN adresse_ville character varying(50);
ALTER TABLE ONLY openelec.bureau ADD COLUMN adresse_pays character varying(50);
