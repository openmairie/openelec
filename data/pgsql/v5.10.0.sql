--
-- Fichier de passage de version SQL : v5.9.0 > v5.10.0
--
-- @package openelec
-- @version SVN : $Id$
--

-- Ticket #9821.
ALTER TABLE openelec.reu_scrutin ALTER COLUMN zone TYPE text;

-- Ticket #9827.
ALTER TABLE openelec.reu_sync_procurations ADD COLUMN IF NOT EXISTS om_collectivite integer;

-- Ticket #9831.
ALTER TABLE reu_scrutin ADD COLUMN canton integer;
ALTER TABLE reu_scrutin ADD COLUMN circonscription_legislative integer;
ALTER TABLE reu_scrutin ADD COLUMN circonscription_metropolitaine integer;
ALTER TABLE ONLY reu_scrutin
    ADD CONSTRAINT reu_scrutin_canton_fkey FOREIGN KEY (canton) REFERENCES canton(id);
ALTER TABLE ONLY reu_scrutin
    ADD CONSTRAINT reu_scrutin_circonscription_legislative_fkey FOREIGN KEY (circonscription_legislative) REFERENCES circonscription(id);
ALTER TABLE ONLY reu_scrutin
    ADD CONSTRAINT reu_scrutin_circonscription_metropolitaine_fkey FOREIGN KEY (circonscription_metropolitaine) REFERENCES circonscription(id);

-- TABLES & SEQUENCES

CREATE TABLE composition_scrutin (
    id integer NOT NULL,
    scrutin integer NOT NULL,
    libelle character varying(250) NOT NULL,
    tour character(1) NOT NULL,
    date_tour date NOT NULL,
    solde boolean,
    convocation_agent character varying(250),
    convocation_president character varying(250)
);
CREATE SEQUENCE composition_scrutin_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE composition_scrutin_seq OWNED BY composition_scrutin.id;

CREATE TABLE affectation (
    id integer NOT NULL,
    elu integer NOT NULL,
    composition_scrutin integer NOT NULL,
    periode character varying(20) NOT NULL,
    poste character varying(20) NOT NULL,
    bureau integer NOT NULL,
    note text,
    decision boolean,
    candidat integer
);
CREATE SEQUENCE affectation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE affectation_seq OWNED BY affectation.id;

CREATE TABLE agent (
    id integer NOT NULL,
    nom character varying(40) NOT NULL,
    prenom character varying(40) NOT NULL,
    adresse character varying(80) NOT NULL,
    cp character varying(5) NOT NULL,
    ville character varying(40) NOT NULL,
    telephone character varying(14),
    service character varying(10),
    telephone_pro character varying(14),
    grade character varying(10)
);
CREATE SEQUENCE agent_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE agent_seq OWNED BY agent.id;

CREATE TABLE candidat (
    id integer NOT NULL,
    nom character varying(50) NOT NULL,
    composition_scrutin integer NOT NULL
);
CREATE SEQUENCE candidat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE candidat_seq OWNED BY candidat.id;

CREATE TABLE candidature (
    id integer NOT NULL,
    agent integer NOT NULL,
    composition_scrutin integer NOT NULL,
    periode character varying(20) NOT NULL,
    poste character varying(20) NOT NULL,
    bureau integer NOT NULL,
    note text,
    decision boolean,
    recuperation boolean,
    debut time without time zone,
    fin time without time zone
);
CREATE SEQUENCE candidature_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE candidature_seq OWNED BY candidature.id;

CREATE TABLE elu (
    id integer NOT NULL,
    nom character varying(40) NOT NULL,
    prenom character varying(40) NOT NULL,
    nomjf character varying(40),
    date_naissance date,
    lieu_naissance character varying(40),
    adresse character varying(80) NOT NULL,
    cp character varying(5) NOT NULL,
    ville character varying(40) NOT NULL
);
CREATE SEQUENCE elu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE elu_seq OWNED BY elu.id;

CREATE TABLE grade (
    grade character varying(10) NOT NULL,
    libelle character varying(40) NOT NULL
);

CREATE TABLE periode (
    periode character varying(20) NOT NULL,
    libelle character varying(40) NOT NULL,
    debut time without time zone NOT NULL,
    fin time without time zone NOT NULL
);

CREATE TABLE poste (
    poste character varying(20) NOT NULL,
    libelle character varying(20) NOT NULL,
    nature character varying(15) NOT NULL,
    ordre integer NOT NULL
);

CREATE TABLE service (
    service character varying(10) NOT NULL,
    libelle character varying(40) NOT NULL
);

CREATE TABLE composition_bureau (
    id integer NOT NULL,
    composition_scrutin integer NOT NULL,
    bureau integer NOT NULL
);
CREATE SEQUENCE composition_bureau_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE composition_bureau_seq OWNED BY composition_bureau.id;

-- PRIMARY KEYS

ALTER TABLE ONLY affectation
    ADD CONSTRAINT affectation_pkey PRIMARY KEY (id);

ALTER TABLE ONLY agent
    ADD CONSTRAINT agent_pkey PRIMARY KEY (id);

ALTER TABLE ONLY candidat
    ADD CONSTRAINT candidat_pkey PRIMARY KEY (id);

ALTER TABLE ONLY candidature
    ADD CONSTRAINT candidature_pkey PRIMARY KEY (id);

ALTER TABLE ONLY composition_scrutin
    ADD CONSTRAINT composition_scrutin_pkey PRIMARY KEY (id);

ALTER TABLE ONLY composition_bureau
    ADD CONSTRAINT composition_bureau_pkey PRIMARY KEY (id);

ALTER TABLE ONLY elu
    ADD CONSTRAINT elu_pkey PRIMARY KEY (id);

ALTER TABLE ONLY grade
    ADD CONSTRAINT grade_pkey PRIMARY KEY (grade);

ALTER TABLE ONLY periode
    ADD CONSTRAINT periode_pkey PRIMARY KEY (periode);

ALTER TABLE ONLY poste
    ADD CONSTRAINT poste_pkey PRIMARY KEY (poste);

ALTER TABLE ONLY service
    ADD CONSTRAINT service_pkey PRIMARY KEY (service);

-- FOREIGN KEYS

ALTER TABLE ONLY affectation
    ADD CONSTRAINT affectation_bureau_fkey FOREIGN KEY (bureau) REFERENCES bureau(id);

ALTER TABLE ONLY affectation
    ADD CONSTRAINT affectation_candidat_fkey FOREIGN KEY (candidat) REFERENCES candidat(id);

ALTER TABLE ONLY affectation
    ADD CONSTRAINT affectation_elu_fkey FOREIGN KEY (elu) REFERENCES elu(id);

ALTER TABLE ONLY affectation
    ADD CONSTRAINT affectation_periode_fkey FOREIGN KEY (periode) REFERENCES periode(periode);

ALTER TABLE ONLY affectation
    ADD CONSTRAINT affectation_poste_fkey FOREIGN KEY (poste) REFERENCES poste(poste);

ALTER TABLE ONLY affectation
    ADD CONSTRAINT affectation_composition_scrutin_fkey FOREIGN KEY (composition_scrutin) REFERENCES composition_scrutin(id);

ALTER TABLE ONLY agent
    ADD CONSTRAINT agent_grade_fkey FOREIGN KEY (grade) REFERENCES grade(grade);

ALTER TABLE ONLY agent
    ADD CONSTRAINT agent_service_fkey FOREIGN KEY (service) REFERENCES service(service);

ALTER TABLE ONLY candidat
    ADD CONSTRAINT candidat_composition_scrutin_fkey FOREIGN KEY (composition_scrutin) REFERENCES composition_scrutin(id);

ALTER TABLE ONLY candidature
    ADD CONSTRAINT candidature_agent_fkey FOREIGN KEY (agent) REFERENCES agent(id);

ALTER TABLE ONLY candidature
    ADD CONSTRAINT candidature_bureau_fkey FOREIGN KEY (bureau) REFERENCES bureau(id);

ALTER TABLE ONLY candidature
    ADD CONSTRAINT candidature_periode_fkey FOREIGN KEY (periode) REFERENCES periode(periode);

ALTER TABLE ONLY candidature
    ADD CONSTRAINT candidature_poste_fkey FOREIGN KEY (poste) REFERENCES poste(poste);

ALTER TABLE ONLY candidature
    ADD CONSTRAINT candidature_composition_scrutin_fkey FOREIGN KEY (composition_scrutin) REFERENCES composition_scrutin(id);

ALTER TABLE ONLY composition_bureau
    ADD CONSTRAINT composition_bureau_composition_scrutin_fkey FOREIGN KEY (composition_scrutin) REFERENCES composition_scrutin(id);

ALTER TABLE ONLY composition_bureau
    ADD CONSTRAINT composition_bureau_bureau_fkey FOREIGN KEY (bureau) REFERENCES bureau(id);

ALTER TABLE ONLY composition_scrutin
    ADD CONSTRAINT composition_scrutin_scrutin_fkey FOREIGN KEY (scrutin) REFERENCES reu_scrutin(id);

