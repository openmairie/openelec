--
-- Fichier de passage de version SQL : v5.6.x > v5.7.0
--
-- @package openelec
-- @version SVN : $Id$
--


--
ALTER TABLE openelec.reu_scrutin ADD COLUMN IF NOT EXISTS referentiel_id integer;
CREATE SEQUENCE IF NOT EXISTS openelec.reu_scrutin_seq
START WITH 100
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;
ALTER SEQUENCE openelec.reu_scrutin_seq OWNED BY openelec.reu_scrutin.id;
ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_unique_referentiel_key UNIQUE (referentiel_id, om_collectivite);
--UPDATE openelec.reu_scrutin SET referentiel_id=id;


ALTER TABLE openelec.procuration ALTER COLUMN utilisateur TYPE character varying(30);

-- Ajout d'une nouvelle colonne dans l'export CSV de la liste électorale du REU
ALTER TABLE openelec.reu_sync_listes ADD COLUMN date_modification character varying(250); 

-- Ajout de nouvelles colonnes dans l'export CSV de la liste électorale du REU
ALTER TABLE openelec.reu_sync_listes ADD COLUMN code_circonscription_metropolitaine character varying(250); 
ALTER TABLE openelec.reu_sync_listes ADD COLUMN libelle_circonscription_metropolitaine character varying(250); 

-- Ajout d'une nouvelle colonne dans l'export CSV de la liste électorale du REU
ALTER TABLE openelec.reu_sync_listes ADD COLUMN adresse_de_contact_identique character varying(250);

--
--
--
CREATE TABLE openelec.trace(
    id integer NOT NULL,
    creationdate date NOT NULL,
    creationtime time without time zone NOT NULL,
    type character varying(256) NOT NULL,
    trace text,
    om_collectivite integer NOT NULL
);
CREATE SEQUENCE IF NOT EXISTS openelec.trace_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;
ALTER SEQUENCE openelec.trace_seq OWNED BY openelec.trace.id;
ALTER TABLE ONLY openelec.trace
    ADD CONSTRAINT trace_pkey PRIMARY KEY (id);
ALTER TABLE ONLY openelec.trace
    ADD CONSTRAINT trace_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);
UPDATE openelec.fichier SET type='archive' WHERE type='trace' and mimetype='application/zip';
UPDATE openelec.fichier SET type='export' WHERE type='trace' and mimetype='test/plain';
UPDATE openelec.fichier SET mimetype='text/plain' WHERE type='export' and mimetype='test/plain';
--
--
--

--
ALTER TABLE openelec.mouvement ADD COLUMN vu boolean;
UPDATE openelec.mouvement SET vu=true WHERE statut<>'refuse';

