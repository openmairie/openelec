--
-- Fichier de passage de version SQL : v5.0.0 > v5.1.0
--
-- @package openelec
-- @version SVN : $Id$
--

--
-- Ticket #9120 - Begin
--
DROP TABLE openelec.inscription_office;
DROP TABLE openelec.radiation_insee;
--
-- Ticket #9120 - End
--

--
-- Ticket #9125 - Begin
--
ALTER TABLE openelec.electeur ADD COLUMN ine integer;
ALTER TABLE openelec.electeur ADD COLUMN reu_sync_info character varying(250);
ALTER TABLE ONLY openelec.electeur ADD CONSTRAINT electeur_unique_ine_key UNIQUE (ine, liste);
CREATE TABLE openelec.reu_sync_listes (
    code_ugle character varying(250),
    code_commune_de_l_ugle character varying(250),
    libelle_ugle character varying(250),
    date_extraction character varying(250),
    code_type_liste character varying(250),
    libelle_type_liste character varying(250),
    numero_d_electeur character varying(250),
    code_categorie_d_electeur character varying(250),
    libelle_categorie_d_electeur character varying(250),
    date_d_inscription character varying(250),
    nom character varying(250),
    nom_d_usage character varying(250),
    prenoms character varying(250),
    sexe character varying(250),
    date_de_naissance character varying(250),
    code_commune_de_naissance character varying(250),
    libelle_commune_de_naissance character varying(250),
    code_pays_de_naissance character varying(250),
    libelle_pays_de_naissance character varying(250),
    code_nationalite character varying(250),
    libelle_nationalite character varying(250),
    numero_de_voie character varying(250),
    type_et_libelle_de_voie character varying(250),
    premier_complement_adresse character varying(250),
    second_complement_adresse character varying(250),
    lieu_dit character varying(250),
    code_postal character varying(250),
    libelle_de_commune character varying(250),
    libelle_du_pays character varying(250),
    id_bureau_de_vote character varying(250),
    code_bureau_de_vote character varying(250),
    libelle_bureau_de_vote character varying(250),
    numero_d_ordre_dans_le_bureau_de_vote character varying(250),
    code_circonscription_legislative character varying(250),
    libelle_circonscription_legislative character varying(250),
    code_canton character varying(250),
    libelle_canton character varying(250),
    code_motif_inscription character varying(250),
    libelle_motif_inscription character varying(250)
);
CREATE INDEX ON openelec.electeur ((replace(public.withoutaccent(lower(trim(nom))),'-',' ')));
CREATE INDEX ON openelec.electeur ((replace(public.withoutaccent(lower(trim(prenom))),'-',' ')));
CREATE INDEX ON openelec.reu_sync_listes ((replace(public.withoutaccent(lower(trim(nom))),'-',' ')));
CREATE INDEX ON openelec.reu_sync_listes ((replace(public.withoutaccent(lower(trim(prenoms))),'-',' ')));
ALTER TABLE openelec.nationalite ALTER COLUMN libelle_nationalite TYPE character varying(50);
ALTER TABLE openelec.mouvement ADD COLUMN ine integer;
ALTER TABLE openelec.electeur ALTER COLUMN adresse_resident TYPE character varying(100);
ALTER TABLE openelec.electeur ALTER COLUMN complement_resident TYPE character varying(100);
ALTER TABLE openelec.electeur ALTER COLUMN ville_resident TYPE character varying(100);
ALTER TABLE openelec.mouvement ALTER COLUMN adresse_resident TYPE character varying(100);
ALTER TABLE openelec.mouvement ALTER COLUMN complement_resident TYPE character varying(100);
ALTER TABLE openelec.mouvement ALTER COLUMN ville_resident TYPE character varying(100);
--
-- Ticket #9125 - End
--
