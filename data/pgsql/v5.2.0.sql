--
-- Fichier de passage de version SQL : v5.1.x > v5.2.0
--
-- @package openelec
-- @version SVN : $Id$
--


-- Ticket [#9129] Gérer les pièces dématérialisées sur les mouvements - Begin
--

CREATE TABLE openelec.piece(
    id integer NOT NULL,
    mouvement integer NOT NULL,
    libelle character varying(100) NOT NULL,
    piece_type integer NOT NULL,
    fichier character varying(50) NOT NULL,
    om_collectivite integer NOT NULL
);

CREATE SEQUENCE openelec.piece_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER SEQUENCE openelec.piece_seq OWNED BY openelec.piece.id;

CREATE TABLE openelec.piece_type(
    id integer NOT NULL,
    libelle character varying(100),
    code character varying(20),
    description text,
    om_validite_debut date,
    om_validite_fin date
);


CREATE SEQUENCE openelec.piece_type_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

ALTER SEQUENCE openelec.piece_type_seq OWNED BY openelec.piece_type.id;

ALTER TABLE ONLY openelec.piece_type
    ADD CONSTRAINT piece_type_pkey PRIMARY KEY (id);

ALTER TABLE ONLY openelec.piece
    ADD CONSTRAINT piece_pkey PRIMARY KEY (id);

ALTER TABLE ONLY openelec.piece
    ADD CONSTRAINT piece_piece_type_fkey FOREIGN KEY (piece_type) REFERENCES openelec.piece_type(id);

ALTER TABLE ONLY openelec.piece
    ADD CONSTRAINT piece_mouvement_fkey FOREIGN KEY (mouvement) REFERENCES openelec.mouvement(id);

ALTER TABLE ONLY openelec.piece
    ADD CONSTRAINT piece_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);
--
-- Ticket [#9129] Gérer les pièces dématérialisées sur les mouvements - End
--

--
-- Ticket [#9132] Gérer la synchronisation des notifications du REU - Begin
--

CREATE TABLE openelec.reu_notification (
    id integer NOT NULL,
    date_de_creation timestamp without time zone,
    libelle character varying(500),
    id_demande character varying(20),
    id_electeur integer,
    lu boolean,
    lien_uri character varying(100),
    resultat_de_notification character varying(250),
    type_de_notification character varying(250),
    date_de_collecte timestamp without time zone,
    om_collectivite integer NOT NULL
);

ALTER TABLE ONLY openelec.reu_notification
    ADD CONSTRAINT reu_notification_pkey PRIMARY KEY (id);

--
-- Ticket [#9132] Gérer la synchronisation des notifications du REU - End
--

--
-- Ticket  [#????] REU - Gérer les traitements des notifications - Begin
--

--
ALTER TABLE ONLY openelec.mouvement ADD COLUMN id_demande character varying(20);
COMMENT ON COLUMN openelec.mouvement.id_demande IS 'Identifiant de la demande REU liée au mouvement';
ALTER TABLE ONLY openelec.mouvement ADD COLUMN statut character varying(20);
COMMENT ON COLUMN openelec.mouvement.statut IS 'Statut de la demande';
ALTER TABLE ONLY openelec.mouvement ADD COLUMN visa character varying(20);
COMMENT ON COLUMN openelec.mouvement.visa IS 'Visa de la demande';
ALTER TABLE ONLY openelec.mouvement ADD COLUMN date_visa date;
COMMENT ON COLUMN openelec.mouvement.date_visa IS 'Date de visa de la demande';
ALTER TABLE ONLY openelec.mouvement ADD COLUMN date_complet date;
COMMENT ON COLUMN openelec.mouvement.date_complet IS 'Date de complétude de la demande';
ALTER TABLE ONLY openelec.mouvement ADD COLUMN date_demande date;
COMMENT ON COLUMN openelec.mouvement.date_demande IS 'Date de la demande';
ALTER TABLE ONLY openelec.mouvement ADD COLUMN adresse_rattachement_reu text;
COMMENT ON COLUMN openelec.mouvement.adresse_rattachement_reu IS 'Adresse de rattachement récupérée depuis le REU';

--
ALTER TABLE ONLY openelec.reu_notification ADD COLUMN traitee boolean DEFAULT false;
COMMENT ON COLUMN openelec.reu_notification.traitee IS 'Traitement de la notification dans openElec';
ALTER TABLE ONLY openelec.reu_notification ADD COLUMN rapport_traitement text;
COMMENT ON COLUMN openelec.reu_notification.rapport_traitement IS 'Rapport du traitement de la notification';

--
alter table openelec.mouvement alter column bureau drop not null;
alter table openelec.mouvement alter column code_bureau drop not null;

--
ALTER TABLE openelec.mouvement ALTER COLUMN observation TYPE character varying(500);

--
ALTER TABLE openelec.param_mouvement ALTER COLUMN code TYPE character varying(20);
ALTER TABLE openelec.mouvement ALTER COLUMN types TYPE character varying(20);
ALTER TABLE openelec.electeur ALTER COLUMN code_inscription TYPE character varying(20);
ALTER TABLE openelec.archive ALTER COLUMN types TYPE character varying(20);

--
ALTER TABLE openelec.param_mouvement ALTER COLUMN libelle TYPE character varying(100);

--
ALTER TABLE ONLY openelec.mouvement ADD COLUMN historique text;
COMMENT ON COLUMN openelec.mouvement.historique IS 'Historique des actions sur le mouvement';

--
-- Ticket  [#????] REU - Gérer les traitements des notifications - End
--

ALTER TABLE ONLY openelec.nationalite ADD COLUMN om_validite_debut date;
ALTER TABLE ONLY openelec.nationalite ADD COLUMN om_validite_fin date;


--
-- Ticket [#????] REU - Synchronisation Référentiel Lieu de Naissance - Begin
--
CREATE TABLE openelec.reu_referentiel_commune (
    code character varying(20),
    libelle character varying(100)
);
CREATE TABLE openelec.reu_referentiel_pays (
    code character varying(20),
    libelle character varying(100)
);
--
-- Ticket [#????] REU - Synchronisation Référentiel Lieu de Naissance - End
--

--
-- Ticket [#????] REU - Bureau de vote - Begin
--
ALTER TABLE openelec.bureau ADD COLUMN referentiel_id integer;
COMMENT ON COLUMN openelec.bureau.referentiel_id IS 'Identifiant du bureau de vote dans le réferentiel';
--
-- Ticket [#????] REU - Bureau de vote - End
--
