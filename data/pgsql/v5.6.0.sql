--
-- Fichier de passage de version SQL : v5.5.x > v5.6.0
--
-- @package openelec
-- @version SVN : $Id$
--

--
ALTER TABLE openelec.electeur ALTER COLUMN libelle_departement_naissance TYPE character varying(70);
ALTER TABLE openelec.mouvement ALTER COLUMN libelle_departement_naissance TYPE character varying(70);
ALTER TABLE openelec.archive ALTER COLUMN libelle_departement_naissance TYPE character varying(70);
ALTER TABLE openelec.electeur ALTER COLUMN libelle_lieu_de_naissance TYPE character varying(70);
ALTER TABLE openelec.mouvement ALTER COLUMN libelle_lieu_de_naissance TYPE character varying(70);
ALTER TABLE openelec.archive ALTER COLUMN libelle_lieu_de_naissance TYPE character varying(70);
--
ALTER TABLE openelec.archive ALTER COLUMN prenom TYPE character varying(70);
ALTER TABLE openelec.mouvement ALTER COLUMN prenom TYPE character varying(70);
ALTER TABLE openelec.electeur ALTER COLUMN prenom TYPE character varying(70);
--
ALTER TABLE ONLY openelec.electeur
    DROP CONSTRAINT electeur_unique_ine_key;
ALTER TABLE ONLY openelec.electeur
    ADD CONSTRAINT electeur_unique_ine_key UNIQUE (ine, liste, om_collectivite);

--
ALTER TABLE ONLY openelec.electeur
    DROP COLUMN situation;
ALTER TABLE ONLY openelec.archive
    DROP COLUMN situation;
ALTER TABLE ONLY openelec.mouvement
    DROP COLUMN situation;

-- Ticket #9203
DROP SEQUENCE openelec.centrevote_seq;
DROP TABLE openelec.centrevote;

--
ALTER TABLE ONLY openelec.electeur
    DROP COLUMN numero_electeur;
ALTER TABLE ONLY openelec.archive
    DROP COLUMN numero_electeur;
ALTER TABLE ONLY openelec.mouvement
    DROP COLUMN numero_electeur;
DROP SEQUENCE openelec.numeroliste_seq;
DROP TABLE openelec.numeroliste;

--
ALTER TABLE ONLY openelec.electeur
    ADD CONSTRAINT electeur_unique_numero_bureau_key UNIQUE (numero_bureau, bureau, liste, om_collectivite);

