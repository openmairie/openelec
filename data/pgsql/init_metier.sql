--------------------------------------------------------------------------------     
-- Instructions de base de l'applicatif     
--      
-- Génération de ce fichier : voir le fichier make_init.sh et faire un meld     
--      
-- @package openelec    
-- @version SVN : $Id$      
--------------------------------------------------------------------------------    
 
--      
-- SPECIFIC - start     
--      
 
CREATE OR REPLACE FUNCTION public.withoutaccent(text) RETURNS text AS $$    
 SELECT translate($1,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyaaaaaceeeeiiiinooooouuuuy');   
$$ LANGUAGE SQL;    
 
--      
-- SPECIFIC - end   
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.17 (Ubuntu 12.17-1.pgdg20.04+1)
-- Dumped by pg_dump version 12.17 (Ubuntu 12.17-1.pgdg20.04+1)

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET idle_in_transaction_session_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SELECT pg_catalog.set_config('search_path', '', false);
-- SET check_function_bodies = false;
-- SET xmloption = content;
-- SET client_min_messages = warning;
-- SET row_security = off;

--
-- Name: openelec; Type: SCHEMA; Schema: -; Owner: -
--

-- CREATE SCHEMA openelec;


--
-- Name: handle_bureau_for_all_mouvements(); Type: FUNCTION; Schema: openelec; Owner: -
--

CREATE FUNCTION openelec.handle_bureau_for_all_mouvements() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE
        openelec.mouvement
    SET
        bureau_de_vote_code = NEW.code,
        bureau_de_vote_libelle = NEW.libelle
    WHERE
        bureau = NEW.id
        AND etat='actif';
    UPDATE
        openelec.mouvement
    SET
        ancien_bureau_de_vote_code = NEW.code,
        ancien_bureau_de_vote_libelle = NEW.libelle
    WHERE
        ancien_bureau = NEW.id
        AND etat='actif';
  RETURN NEW;
END;
$$;


--
-- Name: handle_bureau_for_one_mouvement(); Type: FUNCTION; Schema: openelec; Owner: -
--

CREATE FUNCTION openelec.handle_bureau_for_one_mouvement() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
if NEW.etat = 'actif' then
 NEW.bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=NEW.bureau);
 NEW.bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=NEW.bureau);
 NEW.ancien_bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=NEW.ancien_bureau);
 NEW.ancien_bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=NEW.ancien_bureau);
end if;
if (NEW.etat = 'trs' OR NEW.etat = 'na') AND (TG_OP = 'INSERT' OR OLD.etat<>NEW.etat) then
 NEW.bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=NEW.bureau);
 NEW.bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=NEW.bureau);
 NEW.ancien_bureau_de_vote_code = (SELECT bureau.code FROM openelec.bureau WHERE bureau.id=NEW.ancien_bureau);
 NEW.ancien_bureau_de_vote_libelle = (SELECT bureau.libelle FROM openelec.bureau WHERE bureau.id=NEW.ancien_bureau);
 NEW.bureau = NULL;
 NEW.ancien_bureau = NULL;
end if;
 RETURN NEW;
END;
$$;


-- SET default_tablespace = '';

-- SET default_table_access_method = heap;

--
-- Name: affectation; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.affectation (
    id integer NOT NULL,
    elu integer NOT NULL,
    composition_scrutin integer NOT NULL,
    periode character varying(20) NOT NULL,
    poste character varying(20) NOT NULL,
    bureau integer NOT NULL,
    note text,
    decision boolean,
    candidat integer
);


--
-- Name: affectation_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.affectation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: affectation_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.affectation_seq OWNED BY openelec.affectation.id;


--
-- Name: agent; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.agent (
    id integer NOT NULL,
    nom character varying(40) NOT NULL,
    prenom character varying(40) NOT NULL,
    adresse character varying(80) NOT NULL,
    cp character varying(5) NOT NULL,
    ville character varying(40) NOT NULL,
    telephone character varying(14),
    service character varying(10),
    telephone_pro character varying(14),
    grade character varying(10)
);


--
-- Name: agent_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.agent_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: agent_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.agent_seq OWNED BY openelec.agent.id;


--
-- Name: archive; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.archive (
    id integer NOT NULL,
    types character varying(20) DEFAULT 'pi'::bpchar NOT NULL,
    electeur_id integer,
    liste character varying(6) NOT NULL,
    bureau_de_vote_code character varying(20),
    bureauforce character(3) DEFAULT ''::bpchar NOT NULL,
    numero_bureau bigint DEFAULT (0)::bigint,
    date_modif date,
    utilisateur character varying(30) DEFAULT ''::character varying NOT NULL,
    civilite character varying(4) DEFAULT 'M.'::character varying NOT NULL,
    sexe character(1) DEFAULT ''::bpchar NOT NULL,
    nom character varying(63) DEFAULT ''::character varying NOT NULL,
    nom_usage character varying(63),
    prenom character varying(70) DEFAULT ''::character varying NOT NULL,
    date_naissance date NOT NULL,
    code_departement_naissance character varying(5) DEFAULT ''::character varying NOT NULL,
    libelle_departement_naissance character varying(70) DEFAULT ''::character varying NOT NULL,
    code_lieu_de_naissance character varying(6) DEFAULT ''::character varying NOT NULL,
    libelle_lieu_de_naissance character varying(70) DEFAULT ''::character varying NOT NULL,
    code_nationalite character varying(4) NOT NULL,
    code_voie character varying(10) NOT NULL,
    libelle_voie character varying(50) DEFAULT ''::character varying NOT NULL,
    numero_habitation integer DEFAULT 0 NOT NULL,
    complement_numero character varying(10),
    complement character varying(80),
    provenance character varying(6),
    libelle_provenance character varying(50) DEFAULT ''::character varying NOT NULL,
    ancien_bureau_de_vote_code character varying(20),
    observation character varying(100) DEFAULT ''::character varying NOT NULL,
    resident character(3),
    adresse_resident character varying(40),
    complement_resident character varying(40),
    cp_resident character varying(10),
    ville_resident character varying(50),
    tableau character(10),
    date_tableau date,
    envoi_cnen character(3),
    date_cnen date,
    mouvement bigint DEFAULT (0)::bigint NOT NULL,
    typecat character varying(20) DEFAULT ''::character varying NOT NULL,
    date_mouvement date,
    etat character varying(10),
    om_collectivite integer NOT NULL,
    telephone character varying(30) DEFAULT ''::character varying,
    courriel character varying(100) DEFAULT ''::character varying,
    bureau_de_vote_libelle character varying(100),
    ancien_bureau_de_vote_libelle character varying(100)
);


--
-- Name: archive_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.archive_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: archive_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.archive_seq OWNED BY openelec.archive.id;


--
-- Name: arrets_liste; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.arrets_liste (
    arrets_liste integer NOT NULL,
    livrable_demande_id integer,
    livrable_demande_date date,
    livrable character varying(100),
    livrable_date date,
    om_collectivite integer
);


--
-- Name: COLUMN arrets_liste.arrets_liste; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.arrets_liste.arrets_liste IS 'Identifiant unique de l''arret des listes.';


--
-- Name: COLUMN arrets_liste.livrable_demande_id; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.arrets_liste.livrable_demande_id IS 'Identifiant de la demande de livrable d''aret des listes.';


--
-- Name: COLUMN arrets_liste.livrable_demande_date; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.arrets_liste.livrable_demande_date IS 'Date de demande du livrable d''arret des listes.';


--
-- Name: COLUMN arrets_liste.livrable; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.arrets_liste.livrable IS 'UID du livrable d''arret des listes.';


--
-- Name: COLUMN arrets_liste.livrable_date; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.arrets_liste.livrable_date IS 'Date de reception du livrable d''arret des listes.';


--
-- Name: COLUMN arrets_liste.om_collectivite; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.arrets_liste.om_collectivite IS 'Collectivité pour laquelle l''arret des listes a été demandé.';


--
-- Name: arrets_liste_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.arrets_liste_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bureau; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.bureau (
    id integer NOT NULL,
    code character varying(4) NOT NULL,
    libelle character varying(80) NOT NULL,
    adresse1 character varying(40) DEFAULT ''::character varying,
    adresse2 character varying(40) DEFAULT ''::character varying,
    adresse3 character varying(40) DEFAULT ''::character varying,
    canton integer NOT NULL,
    circonscription integer NOT NULL,
    om_collectivite integer NOT NULL,
    referentiel_id integer,
    surcharge_adresse_carte_electorale boolean,
    adresse_numero_voie character varying(20),
    adresse_libelle_voie character varying(150),
    adresse_complement1 character varying(50),
    adresse_complement2 character varying(50),
    adresse_lieu_dit character varying(50),
    adresse_code_postal character varying(20),
    adresse_ville character varying(50),
    adresse_pays character varying(50)
);


--
-- Name: COLUMN bureau.referentiel_id; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.bureau.referentiel_id IS 'Identifiant du bureau de vote dans le réferentiel';


--
-- Name: COLUMN bureau.surcharge_adresse_carte_electorale; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.bureau.surcharge_adresse_carte_electorale IS 'Marqueur de surcharge de l''adresse du bureau de vote standard sur l''édition de la carte électorale';


--
-- Name: bureau_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.bureau_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bureau_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.bureau_seq OWNED BY openelec.bureau.id;


--
-- Name: candidat; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.candidat (
    id integer NOT NULL,
    nom character varying(50) NOT NULL,
    composition_scrutin integer NOT NULL
);


--
-- Name: candidat_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.candidat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: candidat_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.candidat_seq OWNED BY openelec.candidat.id;


--
-- Name: candidature; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.candidature (
    id integer NOT NULL,
    agent integer NOT NULL,
    composition_scrutin integer NOT NULL,
    periode character varying(20) NOT NULL,
    poste character varying(20) NOT NULL,
    bureau integer NOT NULL,
    note text,
    decision boolean,
    recuperation boolean,
    debut time without time zone,
    fin time without time zone
);


--
-- Name: candidature_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.candidature_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: candidature_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.candidature_seq OWNED BY openelec.candidature.id;


--
-- Name: canton; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.canton (
    id integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(80) NOT NULL
);


--
-- Name: canton_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.canton_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: canton_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.canton_seq OWNED BY openelec.canton.id;


--
-- Name: circonscription; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.circonscription (
    id integer NOT NULL,
    code character varying(10) NOT NULL,
    libelle character varying(80) NOT NULL
);


--
-- Name: circonscription_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.circonscription_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: circonscription_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.circonscription_seq OWNED BY openelec.circonscription.id;


--
-- Name: cnen_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.cnen_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: commune; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.commune (
    code character varying(6) NOT NULL,
    code_commune character varying(4) NOT NULL,
    libelle_commune character varying(45) NOT NULL,
    code_departement character varying(5) NOT NULL
);


--
-- Name: composition_bureau; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.composition_bureau (
    id integer NOT NULL,
    composition_scrutin integer NOT NULL,
    bureau integer NOT NULL
);


--
-- Name: composition_bureau_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.composition_bureau_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: composition_bureau_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.composition_bureau_seq OWNED BY openelec.composition_bureau.id;


--
-- Name: composition_scrutin; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.composition_scrutin (
    id integer NOT NULL,
    scrutin integer NOT NULL,
    libelle character varying(250) NOT NULL,
    tour character(1) NOT NULL,
    date_tour date NOT NULL,
    solde boolean,
    convocation_agent character varying(250),
    convocation_president character varying(250)
);


--
-- Name: composition_scrutin_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.composition_scrutin_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: composition_scrutin_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.composition_scrutin_seq OWNED BY openelec.composition_scrutin.id;


--
-- Name: decoupage; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.decoupage (
    id integer NOT NULL,
    bureau integer NOT NULL,
    code_voie character varying(10) NOT NULL,
    premier_impair integer NOT NULL,
    dernier_impair integer NOT NULL,
    premier_pair integer NOT NULL,
    dernier_pair integer NOT NULL
);


--
-- Name: decoupage_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.decoupage_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: decoupage_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.decoupage_seq OWNED BY openelec.decoupage.id;


--
-- Name: departement; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.departement (
    code character varying(5) NOT NULL,
    libelle_departement character varying(51) NOT NULL
);


--
-- Name: electeur; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.electeur (
    id integer NOT NULL,
    liste character varying(6) NOT NULL,
    bureau integer NOT NULL,
    bureauforce character(3) DEFAULT ''::bpchar NOT NULL,
    numero_bureau bigint,
    date_modif date,
    utilisateur character varying(30) DEFAULT ''::character varying NOT NULL,
    civilite character varying(4) DEFAULT 'M.'::character varying NOT NULL,
    sexe character(1) DEFAULT ''::bpchar NOT NULL,
    nom character varying(63) DEFAULT ''::character varying NOT NULL,
    nom_usage character varying(63),
    prenom character varying(70) DEFAULT ''::character varying NOT NULL,
    date_naissance date,
    code_departement_naissance character varying(5) DEFAULT ''::character varying NOT NULL,
    libelle_departement_naissance character varying(70) DEFAULT ''::character varying NOT NULL,
    code_lieu_de_naissance character varying(6) DEFAULT ''::character varying NOT NULL,
    libelle_lieu_de_naissance character varying(70) DEFAULT ''::character varying NOT NULL,
    code_nationalite character varying(4) NOT NULL,
    code_voie character varying(10) NOT NULL,
    libelle_voie character varying(50) DEFAULT ''::character varying NOT NULL,
    numero_habitation integer DEFAULT 0 NOT NULL,
    complement_numero character varying(10),
    complement character varying(80),
    provenance character varying(6),
    libelle_provenance character varying(50) DEFAULT ''::character varying NOT NULL,
    resident character(3),
    adresse_resident character varying(100),
    complement_resident character varying(100),
    cp_resident character varying(10),
    ville_resident character varying(100),
    tableau character(10),
    date_tableau date,
    mouvement character varying(20),
    date_mouvement date,
    typecat character varying(30) DEFAULT ''::character varying NOT NULL,
    carte smallint DEFAULT (0)::smallint NOT NULL,
    procuration character varying(255),
    jury smallint DEFAULT 0 NOT NULL,
    date_inscription date,
    code_inscription character varying(20),
    jury_effectif character(3) DEFAULT 'non'::bpchar,
    date_jeffectif date,
    om_collectivite integer NOT NULL,
    profession character varying(80),
    motif_dispense_jury character varying(80),
    telephone character varying(30) DEFAULT ''::character varying,
    courriel character varying(100) DEFAULT ''::character varying,
    ine integer,
    reu_sync_info character varying(250),
    a_transmettre boolean,
    date_saisie_carte_retour date,
    CONSTRAINT electeur_jury_effectif_check CHECK ((jury_effectif = ANY (ARRAY['oui'::bpchar, 'non'::bpchar])))
);


--
-- Name: COLUMN electeur.jury; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.electeur.jury IS 'Détermine si l''électeur n''est pas jurés (0), est jurés titulaire (1) ou suppléant (2).';


--
-- Name: COLUMN electeur.a_transmettre; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.electeur.a_transmettre IS 'Marqueur pour le traitement de transmission par lot au REU';


--
-- Name: COLUMN electeur.date_saisie_carte_retour; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.electeur.date_saisie_carte_retour IS 'Date de saisie de la carte en retour.';


--
-- Name: electeur_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.electeur_seq
    START WITH 100000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: electeur_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.electeur_seq OWNED BY openelec.electeur.id;


--
-- Name: elu; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.elu (
    id integer NOT NULL,
    nom character varying(40) NOT NULL,
    prenom character varying(40) NOT NULL,
    nomjf character varying(40),
    date_naissance date,
    lieu_naissance character varying(40),
    adresse character varying(80) NOT NULL,
    cp character varying(5) NOT NULL,
    ville character varying(40) NOT NULL
);


--
-- Name: elu_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.elu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: elu_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.elu_seq OWNED BY openelec.elu.id;


--
-- Name: fichier; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.fichier (
    id integer NOT NULL,
    creationdate date NOT NULL,
    creationtime time without time zone NOT NULL,
    uid character varying(100) NOT NULL,
    filename character varying(256) NOT NULL,
    size integer NOT NULL,
    mimetype character varying(256) NOT NULL,
    type character varying(256) NOT NULL,
    om_collectivite integer NOT NULL
);


--
-- Name: fichier_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.fichier_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fichier_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.fichier_seq OWNED BY openelec.fichier.id;


--
-- Name: grade; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.grade (
    grade character varying(10) NOT NULL,
    libelle character varying(40) NOT NULL
);


--
-- Name: liste; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.liste (
    liste character varying(6) NOT NULL,
    libelle_liste character varying(40) NOT NULL,
    liste_insee character varying(10) NOT NULL,
    officielle boolean
);


--
-- Name: COLUMN liste.officielle; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.liste.officielle IS 'Marqueur de liste officielle';


--
-- Name: membre_commission; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.membre_commission (
    id integer NOT NULL,
    civilite character varying(4) NOT NULL,
    nom character varying(80) NOT NULL,
    prenom character varying(80) NOT NULL,
    adresse character varying(80) NOT NULL,
    complement character varying(80),
    cp character varying(5) NOT NULL,
    ville character varying(80) NOT NULL,
    cedex character varying(20),
    bp character varying(20),
    om_collectivite integer NOT NULL
);


--
-- Name: membre_commission_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.membre_commission_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: membre_commission_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.membre_commission_seq OWNED BY openelec.membre_commission.id;


--
-- Name: mouvement; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.mouvement (
    id integer NOT NULL,
    etat character varying(6) DEFAULT ''::character varying NOT NULL,
    liste character varying(6) NOT NULL,
    types character varying(20) DEFAULT ''::bpchar NOT NULL,
    electeur_id integer,
    bureau integer,
    bureau_de_vote_code character varying(20),
    bureauforce character(3) DEFAULT ''::bpchar NOT NULL,
    numero_bureau bigint DEFAULT (0)::bigint,
    date_modif date,
    utilisateur character varying(30) DEFAULT ''::character varying NOT NULL,
    civilite character varying(4) DEFAULT 'M.'::character varying NOT NULL,
    sexe character(1) DEFAULT ''::bpchar NOT NULL,
    nom character varying(63) DEFAULT ''::character varying NOT NULL,
    nom_usage character varying(63),
    prenom character varying(70) DEFAULT ''::character varying NOT NULL,
    date_naissance date,
    code_departement_naissance character varying(5) DEFAULT ''::character varying NOT NULL,
    libelle_departement_naissance character varying(70) DEFAULT ''::character varying NOT NULL,
    code_lieu_de_naissance character varying(6) DEFAULT ''::character varying NOT NULL,
    libelle_lieu_de_naissance character varying(70) DEFAULT ''::character varying NOT NULL,
    code_nationalite character varying(4) NOT NULL,
    code_voie character varying(10) NOT NULL,
    libelle_voie character varying(50) DEFAULT ''::character varying NOT NULL,
    numero_habitation integer DEFAULT 0 NOT NULL,
    complement_numero character varying(10),
    complement character varying(80),
    provenance character varying(6),
    libelle_provenance character varying(50) DEFAULT ''::character varying NOT NULL,
    ancien_bureau_de_vote_code character varying(20),
    observation character varying(500) DEFAULT ''::character varying NOT NULL,
    resident character(3),
    adresse_resident character varying(100),
    complement_resident character varying(100),
    cp_resident character varying(10),
    ville_resident character varying(100),
    tableau character(10),
    date_j5 date,
    date_tableau date,
    envoi_cnen character(3),
    date_cnen date,
    om_collectivite integer NOT NULL,
    telephone character varying(30) DEFAULT ''::character varying,
    courriel character varying(100) DEFAULT ''::character varying,
    ine integer,
    id_demande character varying(20),
    statut character varying(20),
    visa character varying(20),
    date_visa date,
    date_complet date,
    date_demande date,
    adresse_rattachement_reu text,
    historique text,
    archive_electeur text,
    ancien_bureau integer,
    bureau_de_vote_libelle character varying(100),
    ancien_bureau_de_vote_libelle character varying(100),
    provenance_demande character varying(255),
    vu boolean
);


--
-- Name: COLUMN mouvement.id_demande; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.id_demande IS 'Identifiant de la demande REU liée au mouvement';


--
-- Name: COLUMN mouvement.statut; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.statut IS 'Statut de la demande';


--
-- Name: COLUMN mouvement.visa; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.visa IS 'Visa de la demande';


--
-- Name: COLUMN mouvement.date_visa; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.date_visa IS 'Date de visa de la demande';


--
-- Name: COLUMN mouvement.date_complet; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.date_complet IS 'Date de complétude de la demande';


--
-- Name: COLUMN mouvement.date_demande; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.date_demande IS 'Date de la demande';


--
-- Name: COLUMN mouvement.adresse_rattachement_reu; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.adresse_rattachement_reu IS 'Adresse de rattachement récupérée depuis le REU';


--
-- Name: COLUMN mouvement.historique; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.historique IS 'Historique des actions sur le mouvement';


--
-- Name: COLUMN mouvement.archive_electeur; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.archive_electeur IS 'Archive de l''état civil de l''électeur';


--
-- Name: COLUMN mouvement.provenance_demande; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.mouvement.provenance_demande IS 'Provenance de la demande REU liée au mouvement';


--
-- Name: mouvement_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.mouvement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mouvement_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.mouvement_seq OWNED BY openelec.mouvement.id;


--
-- Name: nationalite; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.nationalite (
    code character varying(4) DEFAULT ''::character varying NOT NULL,
    libelle_nationalite character varying(50) DEFAULT ''::character varying NOT NULL,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: numerobureau; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.numerobureau (
    id integer NOT NULL,
    om_collectivite integer NOT NULL,
    liste character varying(6) NOT NULL,
    bureau integer NOT NULL,
    dernier_numero bigint NOT NULL,
    dernier_numero_provisoire bigint NOT NULL
);


--
-- Name: numerobureau_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.numerobureau_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: numerobureau_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.numerobureau_seq OWNED BY openelec.numerobureau.id;


--
-- Name: param_mouvement; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.param_mouvement (
    code character varying(20) DEFAULT ''::bpchar NOT NULL,
    libelle character varying(100) DEFAULT ''::character varying NOT NULL,
    typecat character varying(20) DEFAULT ''::character varying NOT NULL,
    effet character varying(10) DEFAULT ''::character varying NOT NULL,
    cnen character(3) DEFAULT ''::bpchar NOT NULL,
    codeinscription character(1) DEFAULT ''::bpchar NOT NULL,
    coderadiation character(1) DEFAULT ''::bpchar NOT NULL,
    edition_carte_electeur integer,
    insee_import_radiation character varying(100),
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: parametrage_nb_jures; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.parametrage_nb_jures (
    id integer NOT NULL,
    canton integer NOT NULL,
    om_collectivite integer NOT NULL,
    nb_jures integer DEFAULT 0,
    nb_suppleants integer
);


--
-- Name: TABLE parametrage_nb_jures; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON TABLE openelec.parametrage_nb_jures IS 'Table permettant de paramétrer le nombre de jurés titulaires et suppléants devant être tirés au sort par canton et par collectivité.';


--
-- Name: COLUMN parametrage_nb_jures.id; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.parametrage_nb_jures.id IS 'Identifiant unique.';


--
-- Name: COLUMN parametrage_nb_jures.canton; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.parametrage_nb_jures.canton IS 'Canton sur lequel le nombre de jurés à tirer au sort est paramétré.';


--
-- Name: COLUMN parametrage_nb_jures.om_collectivite; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.parametrage_nb_jures.om_collectivite IS 'Collectivité sur laquelle le nombre de jurés à tirer au sort est paramétré.';


--
-- Name: COLUMN parametrage_nb_jures.nb_jures; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.parametrage_nb_jures.nb_jures IS 'Nombre de titulaires devant être tirés au sort.';


--
-- Name: COLUMN parametrage_nb_jures.nb_suppleants; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.parametrage_nb_jures.nb_suppleants IS 'Nombre de suppléants devant être tirés au sort.';


--
-- Name: parametrage_nb_jures_id_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.parametrage_nb_jures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parametrage_nb_jures_id_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.parametrage_nb_jures_id_seq OWNED BY openelec.parametrage_nb_jures.id;


--
-- Name: periode; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.periode (
    periode character varying(20) NOT NULL,
    libelle character varying(40) NOT NULL,
    debut time without time zone NOT NULL,
    fin time without time zone NOT NULL
);


--
-- Name: piece; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.piece (
    id integer NOT NULL,
    mouvement integer NOT NULL,
    libelle character varying(100) NOT NULL,
    piece_type integer NOT NULL,
    fichier character varying(50) NOT NULL,
    om_collectivite integer NOT NULL
);


--
-- Name: piece_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.piece_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: piece_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.piece_seq OWNED BY openelec.piece.id;


--
-- Name: piece_type; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.piece_type (
    id integer NOT NULL,
    libelle character varying(100),
    code character varying(20),
    description text,
    om_validite_debut date,
    om_validite_fin date
);


--
-- Name: piece_type_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.piece_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: piece_type_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.piece_type_seq OWNED BY openelec.piece_type.id;


--
-- Name: poste; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.poste (
    poste character varying(20) NOT NULL,
    libelle character varying(20) NOT NULL,
    nature character varying(15) NOT NULL,
    ordre integer NOT NULL
);


--
-- Name: procuration; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.procuration (
    id integer NOT NULL,
    mandant integer,
    mandataire integer,
    debut_validite date NOT NULL,
    fin_validite date NOT NULL,
    date_accord date NOT NULL,
    motif_refus character varying(250),
    id_externe character varying(20),
    statut character varying(50) NOT NULL,
    mandant_ine integer NOT NULL,
    mandataire_ine integer NOT NULL,
    mandant_infos text,
    mandataire_infos text,
    mandant_resume character varying(250),
    mandataire_resume character varying(250),
    om_collectivite integer NOT NULL,
    vu boolean,
    historique text,
    autorite_nom_prenom character varying(250),
    autorite_commune character varying(250),
    autorite_consulat character varying(250),
    autorite_type character varying(250),
    notes text,
    annulation_autorite_nom_prenom character varying(250),
    annulation_autorite_commune character varying(250),
    annulation_autorite_consulat character varying(250),
    annulation_autorite_type character varying(250),
    annulation_date date
);


--
-- Name: procuration_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.procuration_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: procuration_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.procuration_seq OWNED BY openelec.procuration.id;


--
-- Name: reu_livrable; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.reu_livrable (
    type_de_mouvement character varying(255),
    libelle_du_type_de_liste character varying(255),
    nom_de_naissance character varying(255),
    nom_d_usage character varying(255),
    prenoms character varying(255),
    sexe character varying(255),
    date_de_naissance character varying(255),
    commune_de_naissance character varying(255),
    code_de_la_commune_de_naissance character varying(255),
    departement_de_naissance character varying(255),
    pays_de_naissance character varying(255),
    code_du_bureau_de_vote character varying(255),
    libelle_du_bureau_de_vote character varying(255),
    numero_d_ordre_dans_le_bureau_de_vote character varying(255),
    motif character varying(255),
    libelle_du_scrutin character varying(255),
    date_du_premier_tour character varying(255),
    date_du_second_tour character varying(255),
    numero_de_voie character varying(255),
    libelle_de_voie character varying(255),
    complement_1 character varying(255),
    complement_2 character varying(255),
    lieu_dit character varying(255),
    code_postal character varying(255),
    commune character varying(255),
    pays character varying(255),
    code_du_departement character varying(255),
    libelle_de_l_ugle character varying(255),
    code_de_departement_de_naissance character varying(255),
    majeur character varying(255),
    mention_code character varying(255),
    circonscription_du_bureau_de_vote character varying(255),
    canton_du_bureau_de_vote character varying(255),
    identifiant_nationalite character varying(255),
    libelle_de_la_nationalite character varying(255),
    procuration character varying(255),
    scrutin_id integer,
    demande_id integer,
    liste character varying(6),
    electeur_id integer,
    om_collectivite integer NOT NULL,
    nom_naissance_mandataire_tour1 character varying(255),
    nom_usage_mandataire_tour1 character varying(255),
    prenoms_mandataire_tour1 character varying(255),
    sexe_mandataire_tour1 character varying(255),
    date_naissance_mandataire_tour1 character varying(255),
    nom_naissance_mandataire_tour2 character varying(255),
    nom_usage_mandataire_tour2 character varying(255),
    prenoms_mandataire_tour2 character varying(255),
    sexe_mandataire_tour2 character varying(255),
    date_naissance_mandataire_tour2 character varying(255),
    date_validite_procuration character varying(255),
    libelle_circonscription_metropolitaine_du_bureau_de_vote character varying(255)
);


--
-- Name: reu_notification; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.reu_notification (
    id integer NOT NULL,
    date_de_creation timestamp without time zone,
    libelle character varying(500),
    id_demande character varying(20),
    id_electeur integer,
    lu boolean,
    lien_uri character varying(100),
    resultat_de_notification character varying(250),
    type_de_notification character varying(250),
    date_de_collecte timestamp without time zone,
    om_collectivite integer NOT NULL,
    traitee boolean DEFAULT false,
    rapport_traitement text
);


--
-- Name: COLUMN reu_notification.traitee; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.reu_notification.traitee IS 'Traitement de la notification dans openElec';


--
-- Name: COLUMN reu_notification.rapport_traitement; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.reu_notification.rapport_traitement IS 'Rapport du traitement de la notification';


--
-- Name: reu_referentiel_commune; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.reu_referentiel_commune (
    code character varying(20),
    libelle character varying(100)
);


--
-- Name: reu_referentiel_pays; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.reu_referentiel_pays (
    code character varying(20),
    libelle character varying(100)
);


--
-- Name: reu_referentiel_ugle; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.reu_referentiel_ugle (
    code character varying(20),
    libelle character varying(100),
    code_commune character varying(20),
    libelle_commune character varying(100),
    code_consulat character varying(20),
    libelle_consulat character varying(100)
);


--
-- Name: reu_scrutin; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.reu_scrutin (
    id integer NOT NULL,
    type character varying(250),
    partiel boolean,
    libelle character varying(250),
    zone text,
    date_tour1 date,
    date_tour2 date,
    date_debut date,
    date_l30 date,
    date_fin date,
    j5_livrable_demande_id integer,
    j5_livrable_demande_date timestamp without time zone,
    j5_livrable character varying(100),
    j5_livrable_date timestamp without time zone,
    emarge_livrable_demande_id integer,
    emarge_livrable_demande_date timestamp without time zone,
    emarge_livrable character varying(100),
    emarge_livrable_date timestamp without time zone,
    om_collectivite integer NOT NULL,
    referentiel_id integer,
    canton integer,
    circonscription_legislative integer,
    circonscription_metropolitaine integer,
    arrets_liste integer
);


--
-- Name: COLUMN reu_scrutin.arrets_liste; Type: COMMENT; Schema: openelec; Owner: -
--

COMMENT ON COLUMN openelec.reu_scrutin.arrets_liste IS 'Identifiant de l''arrêt des listes associé.';


--
-- Name: reu_scrutin_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.reu_scrutin_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reu_scrutin_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.reu_scrutin_seq OWNED BY openelec.reu_scrutin.id;


--
-- Name: reu_sync_listes; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.reu_sync_listes (
    code_ugle character varying(250),
    code_commune_de_l_ugle character varying(250),
    libelle_ugle character varying(250),
    date_extraction character varying(250),
    code_type_liste character varying(250),
    libelle_type_liste character varying(250),
    numero_d_electeur character varying(250),
    code_categorie_d_electeur character varying(250),
    libelle_categorie_d_electeur character varying(250),
    date_d_inscription character varying(250),
    nom character varying(250),
    nom_d_usage character varying(250),
    prenoms character varying(250),
    sexe character varying(250),
    date_de_naissance character varying(250),
    code_commune_de_naissance character varying(250),
    libelle_commune_de_naissance character varying(250),
    code_pays_de_naissance character varying(250),
    libelle_pays_de_naissance character varying(250),
    code_nationalite character varying(250),
    libelle_nationalite character varying(250),
    numero_de_voie character varying(250),
    type_et_libelle_de_voie character varying(250),
    premier_complement_adresse character varying(250),
    second_complement_adresse character varying(250),
    lieu_dit character varying(250),
    code_postal character varying(250),
    libelle_de_commune character varying(250),
    libelle_du_pays character varying(250),
    id_bureau_de_vote character varying(250),
    code_bureau_de_vote character varying(250),
    libelle_bureau_de_vote character varying(250),
    numero_d_ordre_dans_le_bureau_de_vote character varying(250),
    code_circonscription_legislative character varying(250),
    libelle_circonscription_legislative character varying(250),
    code_canton character varying(250),
    libelle_canton character varying(250),
    code_motif_inscription character varying(250),
    libelle_motif_inscription character varying(250),
    date_modification character varying(250),
    code_circonscription_metropolitaine character varying(250),
    libelle_circonscription_metropolitaine character varying(250),
    adresse_de_contact_identique character varying(250)
);


--
-- Name: reu_sync_procurations; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.reu_sync_procurations (
    id character varying(250),
    date_creation character varying(250),
    date_debut character varying(250),
    date_fin character varying(250),
    date_modification character varying(250),
    date_modification_etat character varying(250),
    lieu_etablissement_ugle character varying(250),
    nom_prenom_autorite character varying(250),
    autorite_etablissement character varying(250),
    type_localisation_code character varying(250),
    date_etablissement character varying(250),
    motif_annulation_office_code character varying(250),
    etat_procuration_code character varying(250),
    electeur_mandant character varying(250),
    electeur_mandataire character varying(250),
    procuration_validite_scrutin_code character varying(250),
    nom_naissance_mandant character varying(250),
    nom_usage_mandant character varying(250),
    prenom_mandant character varying(250),
    sexe_mandant character varying(250),
    date_naissance_mandant character varying(250),
    nom_naissance_mandataire character varying(250),
    nom_usage_mandataire character varying(250),
    prenom_mandataire character varying(250),
    sexe_mandataire character varying(250),
    date_naissance_mandataire character varying(250),
    mandataire_dans_ugle character varying(250),
    provenance character varying(250),
    om_collectivite integer
);


--
-- Name: revision; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.revision (
    id integer NOT NULL,
    libelle character varying(150) NOT NULL,
    date_debut date NOT NULL,
    date_effet date NOT NULL,
    date_tr1 date NOT NULL,
    date_tr2 date NOT NULL
);


--
-- Name: revision_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.revision_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: revision_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.revision_seq OWNED BY openelec.revision.id;


--
-- Name: service; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.service (
    service character varying(10) NOT NULL,
    libelle character varying(40) NOT NULL
);


--
-- Name: trace; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.trace (
    id integer NOT NULL,
    creationdate date NOT NULL,
    creationtime time without time zone NOT NULL,
    type character varying(256) NOT NULL,
    trace text,
    om_collectivite integer NOT NULL
);


--
-- Name: trace_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.trace_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: trace_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.trace_seq OWNED BY openelec.trace.id;


--
-- Name: voie; Type: TABLE; Schema: openelec; Owner: -
--

CREATE TABLE openelec.voie (
    code character varying(10) NOT NULL,
    libelle_voie character varying(50),
    cp character varying(5),
    ville character varying(50),
    abrege character(20),
    om_collectivite integer NOT NULL
);


--
-- Name: voie_seq; Type: SEQUENCE; Schema: openelec; Owner: -
--

CREATE SEQUENCE openelec.voie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voie_seq; Type: SEQUENCE OWNED BY; Schema: openelec; Owner: -
--

ALTER SEQUENCE openelec.voie_seq OWNED BY openelec.voie.code;


--
-- Name: parametrage_nb_jures id; Type: DEFAULT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.parametrage_nb_jures ALTER COLUMN id SET DEFAULT nextval('openelec.parametrage_nb_jures_id_seq'::regclass);


--
-- Name: affectation affectation_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.affectation
    ADD CONSTRAINT affectation_pkey PRIMARY KEY (id);


--
-- Name: agent agent_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.agent
    ADD CONSTRAINT agent_pkey PRIMARY KEY (id);


--
-- Name: archive archive_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.archive
    ADD CONSTRAINT archive_pkey PRIMARY KEY (id);


--
-- Name: arrets_liste arrets_liste_pk; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.arrets_liste
    ADD CONSTRAINT arrets_liste_pk PRIMARY KEY (arrets_liste);


--
-- Name: bureau bureau_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.bureau
    ADD CONSTRAINT bureau_pkey PRIMARY KEY (id);


--
-- Name: bureau bureau_unique_key; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.bureau
    ADD CONSTRAINT bureau_unique_key UNIQUE (code, om_collectivite);


--
-- Name: candidat candidat_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.candidat
    ADD CONSTRAINT candidat_pkey PRIMARY KEY (id);


--
-- Name: candidature candidature_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.candidature
    ADD CONSTRAINT candidature_pkey PRIMARY KEY (id);


--
-- Name: canton canton_code_unique_key; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.canton
    ADD CONSTRAINT canton_code_unique_key UNIQUE (code);


--
-- Name: canton canton_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.canton
    ADD CONSTRAINT canton_pkey PRIMARY KEY (id);


--
-- Name: circonscription circonscription_code_unique_key; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.circonscription
    ADD CONSTRAINT circonscription_code_unique_key UNIQUE (code);


--
-- Name: circonscription circonscription_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.circonscription
    ADD CONSTRAINT circonscription_pkey PRIMARY KEY (id);


--
-- Name: commune commune_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.commune
    ADD CONSTRAINT commune_pkey PRIMARY KEY (code);


--
-- Name: composition_bureau composition_bureau_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.composition_bureau
    ADD CONSTRAINT composition_bureau_pkey PRIMARY KEY (id);


--
-- Name: composition_scrutin composition_scrutin_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.composition_scrutin
    ADD CONSTRAINT composition_scrutin_pkey PRIMARY KEY (id);


--
-- Name: decoupage decoupage_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.decoupage
    ADD CONSTRAINT decoupage_pkey PRIMARY KEY (id);


--
-- Name: departement departement_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.departement
    ADD CONSTRAINT departement_pkey PRIMARY KEY (code);


--
-- Name: electeur electeur_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.electeur
    ADD CONSTRAINT electeur_pkey PRIMARY KEY (id);


--
-- Name: electeur electeur_unique_ine_key; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.electeur
    ADD CONSTRAINT electeur_unique_ine_key UNIQUE (ine, liste, om_collectivite);


--
-- Name: electeur electeur_unique_numero_bureau_key; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.electeur
    ADD CONSTRAINT electeur_unique_numero_bureau_key UNIQUE (numero_bureau, bureau, liste, om_collectivite);


--
-- Name: elu elu_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.elu
    ADD CONSTRAINT elu_pkey PRIMARY KEY (id);


--
-- Name: fichier fichier_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.fichier
    ADD CONSTRAINT fichier_pkey PRIMARY KEY (id);


--
-- Name: grade grade_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.grade
    ADD CONSTRAINT grade_pkey PRIMARY KEY (grade);


--
-- Name: liste liste_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.liste
    ADD CONSTRAINT liste_pkey PRIMARY KEY (liste);


--
-- Name: membre_commission membre_commission_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.membre_commission
    ADD CONSTRAINT membre_commission_pkey PRIMARY KEY (id);


--
-- Name: mouvement mouvement_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.mouvement
    ADD CONSTRAINT mouvement_pkey PRIMARY KEY (id);


--
-- Name: nationalite nationalite_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.nationalite
    ADD CONSTRAINT nationalite_pkey PRIMARY KEY (code);


--
-- Name: numerobureau numerobureau_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.numerobureau
    ADD CONSTRAINT numerobureau_pkey PRIMARY KEY (id);


--
-- Name: numerobureau numerobureau_unique_key; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.numerobureau
    ADD CONSTRAINT numerobureau_unique_key UNIQUE (bureau, liste, om_collectivite);


--
-- Name: param_mouvement param_mouvement_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.param_mouvement
    ADD CONSTRAINT param_mouvement_pkey PRIMARY KEY (code);


--
-- Name: parametrage_nb_jures parametrage_nb_jures_canton_key; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_canton_key UNIQUE (canton, om_collectivite);


--
-- Name: parametrage_nb_jures parametrage_nb_jures_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_pkey PRIMARY KEY (id);


--
-- Name: periode periode_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.periode
    ADD CONSTRAINT periode_pkey PRIMARY KEY (periode);


--
-- Name: piece piece_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.piece
    ADD CONSTRAINT piece_pkey PRIMARY KEY (id);


--
-- Name: piece_type piece_type_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.piece_type
    ADD CONSTRAINT piece_type_pkey PRIMARY KEY (id);


--
-- Name: poste poste_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.poste
    ADD CONSTRAINT poste_pkey PRIMARY KEY (poste);


--
-- Name: procuration procuration_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.procuration
    ADD CONSTRAINT procuration_pkey PRIMARY KEY (id);


--
-- Name: reu_notification reu_notification_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.reu_notification
    ADD CONSTRAINT reu_notification_pkey PRIMARY KEY (id);


--
-- Name: reu_scrutin reu_scrutin_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_pkey PRIMARY KEY (id);


--
-- Name: reu_scrutin reu_scrutin_unique_referentiel_key; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_unique_referentiel_key UNIQUE (referentiel_id, om_collectivite);


--
-- Name: revision revision_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.revision
    ADD CONSTRAINT revision_pkey PRIMARY KEY (id);


--
-- Name: service service_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.service
    ADD CONSTRAINT service_pkey PRIMARY KEY (service);


--
-- Name: trace trace_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.trace
    ADD CONSTRAINT trace_pkey PRIMARY KEY (id);


--
-- Name: voie voie_pkey; Type: CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.voie
    ADD CONSTRAINT voie_pkey PRIMARY KEY (code);


--
-- Name: electeur_replace_idx; Type: INDEX; Schema: openelec; Owner: -
--

CREATE INDEX electeur_replace_idx ON openelec.electeur USING btree (replace(public.withoutaccent(lower(btrim((nom)::text))), '-'::text, ' '::text));


--
-- Name: electeur_replace_idx1; Type: INDEX; Schema: openelec; Owner: -
--

CREATE INDEX electeur_replace_idx1 ON openelec.electeur USING btree (replace(public.withoutaccent(lower(btrim((prenom)::text))), '-'::text, ' '::text));


--
-- Name: ndx_electeur_date_naissance; Type: INDEX; Schema: openelec; Owner: -
--

CREATE INDEX ndx_electeur_date_naissance ON openelec.electeur USING btree (date_naissance);


--
-- Name: ndx_electeur_nom; Type: INDEX; Schema: openelec; Owner: -
--

CREATE INDEX ndx_electeur_nom ON openelec.electeur USING btree (nom);


--
-- Name: reu_sync_listes_replace_idx; Type: INDEX; Schema: openelec; Owner: -
--

CREATE INDEX reu_sync_listes_replace_idx ON openelec.reu_sync_listes USING btree (replace(public.withoutaccent(lower(btrim((nom)::text))), '-'::text, ' '::text));


--
-- Name: reu_sync_listes_replace_idx1; Type: INDEX; Schema: openelec; Owner: -
--

CREATE INDEX reu_sync_listes_replace_idx1 ON openelec.reu_sync_listes USING btree (replace(public.withoutaccent(lower(btrim((prenoms)::text))), '-'::text, ' '::text));


--
-- Name: bureau handle_bureau_all_mouvements_changes; Type: TRIGGER; Schema: openelec; Owner: -
--

CREATE TRIGGER handle_bureau_all_mouvements_changes AFTER UPDATE ON openelec.bureau FOR EACH ROW EXECUTE FUNCTION openelec.handle_bureau_for_all_mouvements();


--
-- Name: mouvement handle_bureau_one_mouvement_changes; Type: TRIGGER; Schema: openelec; Owner: -
--

CREATE TRIGGER handle_bureau_one_mouvement_changes BEFORE INSERT OR UPDATE ON openelec.mouvement FOR EACH ROW EXECUTE FUNCTION openelec.handle_bureau_for_one_mouvement();


--
-- Name: affectation affectation_bureau_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.affectation
    ADD CONSTRAINT affectation_bureau_fkey FOREIGN KEY (bureau) REFERENCES openelec.bureau(id);


--
-- Name: affectation affectation_candidat_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.affectation
    ADD CONSTRAINT affectation_candidat_fkey FOREIGN KEY (candidat) REFERENCES openelec.candidat(id);


--
-- Name: affectation affectation_composition_scrutin_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.affectation
    ADD CONSTRAINT affectation_composition_scrutin_fkey FOREIGN KEY (composition_scrutin) REFERENCES openelec.composition_scrutin(id);


--
-- Name: affectation affectation_elu_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.affectation
    ADD CONSTRAINT affectation_elu_fkey FOREIGN KEY (elu) REFERENCES openelec.elu(id);


--
-- Name: affectation affectation_periode_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.affectation
    ADD CONSTRAINT affectation_periode_fkey FOREIGN KEY (periode) REFERENCES openelec.periode(periode);


--
-- Name: affectation affectation_poste_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.affectation
    ADD CONSTRAINT affectation_poste_fkey FOREIGN KEY (poste) REFERENCES openelec.poste(poste);


--
-- Name: agent agent_grade_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.agent
    ADD CONSTRAINT agent_grade_fkey FOREIGN KEY (grade) REFERENCES openelec.grade(grade);


--
-- Name: agent agent_service_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.agent
    ADD CONSTRAINT agent_service_fkey FOREIGN KEY (service) REFERENCES openelec.service(service);


--
-- Name: archive archive_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.archive
    ADD CONSTRAINT archive_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: arrets_liste arrets_liste_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.arrets_liste
    ADD CONSTRAINT arrets_liste_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: bureau bureau_canton_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.bureau
    ADD CONSTRAINT bureau_canton_fkey FOREIGN KEY (canton) REFERENCES openelec.canton(id);


--
-- Name: bureau bureau_circonscription_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.bureau
    ADD CONSTRAINT bureau_circonscription_fkey FOREIGN KEY (circonscription) REFERENCES openelec.circonscription(id);


--
-- Name: bureau bureau_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.bureau
    ADD CONSTRAINT bureau_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: candidat candidat_composition_scrutin_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.candidat
    ADD CONSTRAINT candidat_composition_scrutin_fkey FOREIGN KEY (composition_scrutin) REFERENCES openelec.composition_scrutin(id);


--
-- Name: candidature candidature_agent_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.candidature
    ADD CONSTRAINT candidature_agent_fkey FOREIGN KEY (agent) REFERENCES openelec.agent(id);


--
-- Name: candidature candidature_bureau_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.candidature
    ADD CONSTRAINT candidature_bureau_fkey FOREIGN KEY (bureau) REFERENCES openelec.bureau(id);


--
-- Name: candidature candidature_composition_scrutin_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.candidature
    ADD CONSTRAINT candidature_composition_scrutin_fkey FOREIGN KEY (composition_scrutin) REFERENCES openelec.composition_scrutin(id);


--
-- Name: candidature candidature_periode_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.candidature
    ADD CONSTRAINT candidature_periode_fkey FOREIGN KEY (periode) REFERENCES openelec.periode(periode);


--
-- Name: candidature candidature_poste_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.candidature
    ADD CONSTRAINT candidature_poste_fkey FOREIGN KEY (poste) REFERENCES openelec.poste(poste);


--
-- Name: commune commune_code_departement_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.commune
    ADD CONSTRAINT commune_code_departement_fkey FOREIGN KEY (code_departement) REFERENCES openelec.departement(code);


--
-- Name: composition_bureau composition_bureau_bureau_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.composition_bureau
    ADD CONSTRAINT composition_bureau_bureau_fkey FOREIGN KEY (bureau) REFERENCES openelec.bureau(id);


--
-- Name: composition_bureau composition_bureau_composition_scrutin_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.composition_bureau
    ADD CONSTRAINT composition_bureau_composition_scrutin_fkey FOREIGN KEY (composition_scrutin) REFERENCES openelec.composition_scrutin(id);


--
-- Name: composition_scrutin composition_scrutin_scrutin_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.composition_scrutin
    ADD CONSTRAINT composition_scrutin_scrutin_fkey FOREIGN KEY (scrutin) REFERENCES openelec.reu_scrutin(id);


--
-- Name: decoupage decoupage_bureau_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.decoupage
    ADD CONSTRAINT decoupage_bureau_fkey FOREIGN KEY (bureau) REFERENCES openelec.bureau(id);


--
-- Name: decoupage decoupage_code_voie_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.decoupage
    ADD CONSTRAINT decoupage_code_voie_fkey FOREIGN KEY (code_voie) REFERENCES openelec.voie(code);


--
-- Name: electeur electeur_bureau_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.electeur
    ADD CONSTRAINT electeur_bureau_fkey FOREIGN KEY (bureau) REFERENCES openelec.bureau(id);


--
-- Name: electeur electeur_code_nationalite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.electeur
    ADD CONSTRAINT electeur_code_nationalite_fkey FOREIGN KEY (code_nationalite) REFERENCES openelec.nationalite(code);


--
-- Name: electeur electeur_liste_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.electeur
    ADD CONSTRAINT electeur_liste_fkey FOREIGN KEY (liste) REFERENCES openelec.liste(liste);


--
-- Name: electeur electeur_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.electeur
    ADD CONSTRAINT electeur_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: fichier fichier_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.fichier
    ADD CONSTRAINT fichier_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: membre_commission membre_commission_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.membre_commission
    ADD CONSTRAINT membre_commission_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: mouvement mouvement_ancien_bureau_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.mouvement
    ADD CONSTRAINT mouvement_ancien_bureau_fkey FOREIGN KEY (ancien_bureau) REFERENCES openelec.bureau(id);


--
-- Name: mouvement mouvement_bureau_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.mouvement
    ADD CONSTRAINT mouvement_bureau_fkey FOREIGN KEY (bureau) REFERENCES openelec.bureau(id);


--
-- Name: mouvement mouvement_code_nationalite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.mouvement
    ADD CONSTRAINT mouvement_code_nationalite_fkey FOREIGN KEY (code_nationalite) REFERENCES openelec.nationalite(code);


--
-- Name: mouvement mouvement_liste_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.mouvement
    ADD CONSTRAINT mouvement_liste_fkey FOREIGN KEY (liste) REFERENCES openelec.liste(liste);


--
-- Name: mouvement mouvement_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.mouvement
    ADD CONSTRAINT mouvement_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: numerobureau numerobureau_bureau_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.numerobureau
    ADD CONSTRAINT numerobureau_bureau_fkey FOREIGN KEY (bureau) REFERENCES openelec.bureau(id);


--
-- Name: numerobureau numerobureau_liste_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.numerobureau
    ADD CONSTRAINT numerobureau_liste_fkey FOREIGN KEY (liste) REFERENCES openelec.liste(liste);


--
-- Name: numerobureau numerobureau_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.numerobureau
    ADD CONSTRAINT numerobureau_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: parametrage_nb_jures parametrage_nb_jures_canton_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_canton_fkey FOREIGN KEY (canton) REFERENCES openelec.canton(id);


--
-- Name: parametrage_nb_jures parametrage_nb_jures_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.parametrage_nb_jures
    ADD CONSTRAINT parametrage_nb_jures_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: piece piece_mouvement_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.piece
    ADD CONSTRAINT piece_mouvement_fkey FOREIGN KEY (mouvement) REFERENCES openelec.mouvement(id);


--
-- Name: piece piece_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.piece
    ADD CONSTRAINT piece_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: piece piece_piece_type_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.piece
    ADD CONSTRAINT piece_piece_type_fkey FOREIGN KEY (piece_type) REFERENCES openelec.piece_type(id);


--
-- Name: procuration procuration_mandant_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.procuration
    ADD CONSTRAINT procuration_mandant_fkey FOREIGN KEY (mandant) REFERENCES openelec.electeur(id);


--
-- Name: procuration procuration_mandataire_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.procuration
    ADD CONSTRAINT procuration_mandataire_fkey FOREIGN KEY (mandataire) REFERENCES openelec.electeur(id);


--
-- Name: procuration procuration_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.procuration
    ADD CONSTRAINT procuration_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: reu_scrutin reu_scrutin_arrets_liste_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_arrets_liste_fkey FOREIGN KEY (arrets_liste) REFERENCES openelec.arrets_liste(arrets_liste);


--
-- Name: reu_scrutin reu_scrutin_canton_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_canton_fkey FOREIGN KEY (canton) REFERENCES openelec.canton(id);


--
-- Name: reu_scrutin reu_scrutin_circonscription_legislative_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_circonscription_legislative_fkey FOREIGN KEY (circonscription_legislative) REFERENCES openelec.circonscription(id);


--
-- Name: reu_scrutin reu_scrutin_circonscription_metropolitaine_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_circonscription_metropolitaine_fkey FOREIGN KEY (circonscription_metropolitaine) REFERENCES openelec.circonscription(id);


--
-- Name: reu_scrutin reu_scrutin_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: trace trace_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.trace
    ADD CONSTRAINT trace_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- Name: voie voie_om_collectivite_fkey; Type: FK CONSTRAINT; Schema: openelec; Owner: -
--

ALTER TABLE ONLY openelec.voie
    ADD CONSTRAINT voie_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);


--
-- PostgreSQL database dump complete
--

 
--      
-- SPECIFIC - start     
--      

ALTER SEQUENCE openelec.om_logo_seq RESTART WITH 100001;
ALTER SEQUENCE openelec.om_etat_seq RESTART WITH 100001;
ALTER SEQUENCE openelec.om_lettretype_seq RESTART WITH 100001;
ALTER SEQUENCE openelec.om_requete_seq RESTART WITH 100001;
ALTER SEQUENCE openelec.om_sousetat_seq RESTART WITH 100001;

--      
-- SPECIFIC - end   
--
