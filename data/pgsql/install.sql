--------------------------------------------------------------------------------
-- Script d'installation
--
-- ATTENTION ce script peut supprimer des données de votre base de données
-- il n'est à utiliser qu'en connaissance de cause
--
-- Usage :
-- cd data/pgsql/
-- dropdb openelec && createdb openelec && psql openelec -f install.sql
--
-- @package openelec
-- @version SVN : $Id$
--------------------------------------------------------------------------------

-- Début du bloc transactionnel
START TRANSACTION;
\set ON_ERROR_STOP on

-- Initialisation de postgis
CREATE EXTENSION IF NOT EXISTS postgis;

-- Suppression, Création et Utilisation du schéma
\set schema 'openelec'
DROP SCHEMA IF EXISTS :schema CASCADE;
CREATE SCHEMA :schema;
SET search_path = :schema, public, pg_catalog;

-- Instructions de base du framework openmairie
\i ../../core/data/pgsql/init.sql

-- Instructions de base de l'applicatif
\i init_metier.sql

-- Initialisation du paramétrage
\i init_permissions.sql
\i ../../core/data/pgsql/init_parametrage.sql

-- Mise à jour depuis la dernière version (en cours de développement)
\i v5.13.1.dev0.sql
\i v5.13.1.dev0.init_data.sql

-- Fin du bloc transactionnel
COMMIT;

