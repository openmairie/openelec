--
-- Fichier de passage de version SQL : v5.2.0 > v5.2.1
--
-- @package openelec
-- @version SVN : $Id$
--

--
-- BEGIN - [#9149] Augmenter le nombre de caractère du champ mouvement de la table electeur
--

--
ALTER TABLE openelec.electeur ALTER COLUMN mouvement TYPE character varying(20);

--
-- END - [#9149] Augmenter le nombre de caractère du champ mouvement de la table electeur
--
