--
-- [BEGIN] - [#10202] - Module de tirage au sort des jurés d'assise suppléants 
--

-- Colonne permettant de paramétrer le nombre de jurés suppléant à tirer au sort
ALTER TABLE parametrage_nb_jures
ADD COLUMN nb_suppleants integer;

-- Commentaires de la table parametrage_nb_jures et de ses colonnes
COMMENT ON TABLE parametrage_nb_jures IS 'Table permettant de paramétrer le nombre de jurés titulaires et suppléants devant être tirés au sort par canton et par collectivité.';
COMMENT ON COLUMN parametrage_nb_jures.id IS 'Identifiant unique.';
COMMENT ON COLUMN parametrage_nb_jures.canton IS 'Canton sur lequel le nombre de jurés à tirer au sort est paramétré.';
COMMENT ON COLUMN parametrage_nb_jures.om_collectivite IS 'Collectivité sur laquelle le nombre de jurés à tirer au sort est paramétré.';
COMMENT ON COLUMN parametrage_nb_jures.nb_jures IS 'Nombre de titulaires devant être tirés au sort.';
COMMENT ON COLUMN parametrage_nb_jures.nb_suppleants IS 'Nombre de suppléants devant être tirés au sort.';

-- Commentaire de la colonne jury de la table electeur
COMMENT ON COLUMN electeur.jury IS 'Détermine si l''électeur n''est pas jurés (0), est jurés titulaire (1) ou suppléant (2).';

--
-- [END] - [#10202] - Module de tirage au sort des jurés d'assise suppléants 
--