--
-- Fichier de passage de version SQL : v5.2.1 > v5.3.0
--
-- @package openelec
-- @version SVN : $Id$
--

--
-- Ticket #9169 - Begin
--
ALTER TABLE ONLY openelec.liste ADD COLUMN officielle boolean;
COMMENT ON COLUMN openelec.liste.officielle IS 'Marqueur de liste officielle';
--
-- Ticket #9169 - End
--

--
-- Ticket #9174 - Begin
--

--
ALTER TABLE ONLY openelec.mouvement ADD COLUMN archive_electeur text;
COMMENT ON COLUMN openelec.mouvement.archive_electeur IS 'Archive de l''état civil de l''électeur';

--
-- Ticket #9174 - End
--
