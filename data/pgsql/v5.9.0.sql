--
-- Fichier de passage de version SQL : v5.8.0 > v5.9.0
--
-- @package openelec
-- @version SVN : $Id$
--

--
--
--
CREATE TABLE openelec.reu_sync_procurations (
    id character varying(250),
    date_creation character varying(250),
    date_debut character varying(250),
    date_fin character varying(250),
    date_modification character varying(250),
    date_modification_etat character varying(250),
    lieu_etablissement_ugle character varying(250),
    nom_prenom_autorite character varying(250),
    autorite_etablissement character varying(250),
    type_localisation_code character varying(250),
    date_etablissement character varying(250),
    motif_annulation_office_code character varying(250),
    etat_procuration_code character varying(250),
    electeur_mandant character varying(250),
    electeur_mandataire character varying(250),
    procuration_validite_scrutin_code character varying(250),
    nom_naissance_mandant character varying(250),
    nom_usage_mandant character varying(250),
    prenom_mandant character varying(250),
    sexe_mandant character varying(250),
    date_naissance_mandant character varying(250),
    nom_naissance_mandataire character varying(250),
    nom_usage_mandataire character varying(250),
    prenom_mandataire character varying(250),
    sexe_mandataire character varying(250),
    date_naissance_mandataire character varying(250),
    mandataire_dans_ugle character varying(250),
    provenance character varying(250)
);

--
--
--
CREATE TABLE openelec.reu_referentiel_ugle (
    code character varying(20),
    libelle character varying(100),
    code_commune character varying(20),
    libelle_commune character varying(100),
    code_consulat character varying(20),
    libelle_consulat character varying(100)
);

--
--
--
--
ALTER TABLE openelec.procuration ADD COLUMN id_externe character varying(20);
--
ALTER TABLE openelec.procuration ADD COLUMN statut character varying(50);
UPDATE openelec.procuration SET statut='demande_refusee' WHERE refus='O';
UPDATE openelec.procuration SET statut='demande_a_transmettre' WHERE statut IS NULL;
ALTER TABLE openelec.procuration ALTER COLUMN statut SET NOT NULL;
ALTER TABLE openelec.procuration DROP COLUMN refus;
ALTER TABLE openelec.procuration DROP COLUMN heure_accord;
ALTER TABLE openelec.procuration ALTER COLUMN motif_refus TYPE character varying(250);
--
ALTER TABLE openelec.procuration ADD COLUMN mandant_ine integer;
ALTER TABLE openelec.procuration ADD COLUMN mandataire_ine integer;
ALTER TABLE openelec.procuration ALTER COLUMN mandant DROP NOT NULL;
ALTER TABLE openelec.procuration ALTER COLUMN mandataire DROP NOT NULL;
UPDATE openelec.procuration SET mandant_ine=(SELECT electeur.ine FROM openelec.electeur WHERE electeur.id=procuration.mandant) WHERE mandant_ine IS NULL;
UPDATE openelec.procuration SET mandataire_ine=(SELECT electeur.ine FROM openelec.electeur WHERE electeur.id=procuration.mandataire) WHERE mandataire_ine IS NULL;
ALTER TABLE openelec.procuration ALTER COLUMN mandant_ine SET NOT NULL;
ALTER TABLE openelec.procuration ALTER COLUMN mandataire_ine SET NOT NULL;
ALTER TABLE openelec.procuration ADD COLUMN mandant_infos text;
ALTER TABLE openelec.procuration ADD COLUMN mandataire_infos text;
ALTER TABLE openelec.procuration ADD COLUMN mandant_resume character varying(250);
ALTER TABLE openelec.procuration ADD COLUMN mandataire_resume character varying(250);
--
ALTER TABLE openelec.procuration ADD COLUMN om_collectivite integer;
ALTER TABLE ONLY openelec.procuration
    ADD CONSTRAINT procuration_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);
UPDATE openelec.procuration SET om_collectivite=(SELECT om_collectivite FROM openelec.electeur WHERE electeur.id=procuration.mandant);
ALTER TABLE openelec.procuration ALTER COLUMN om_collectivite SET NOT NULL;
--
ALTER TABLE openelec.procuration ADD COLUMN vu boolean;
ALTER TABLE openelec.procuration ADD COLUMN historique text;
UPDATE openelec.procuration SET historique=CONCAT('[{"date":"', date_modif, ' 00:00:00","user":"', utilisateur,'","action":"procuration::ajouter|modifier","message":"Date de la dernière modification.","datas":{}}]') WHERE historique is null;
ALTER TABLE openelec.procuration DROP COLUMN date_modif;
ALTER TABLE openelec.procuration DROP COLUMN utilisateur;
--
ALTER TABLE openelec.procuration ADD COLUMN autorite_nom_prenom character varying(250);
UPDATE openelec.procuration SET autorite_nom_prenom = TRIM(CONCAT(origine1, ' ', origine2));
ALTER TABLE openelec.procuration DROP COLUMN origine1;
ALTER TABLE openelec.procuration DROP COLUMN origine2;
ALTER TABLE openelec.procuration ADD COLUMN autorite_commune character varying(250);
ALTER TABLE openelec.procuration ADD COLUMN autorite_consulat character varying(250);
ALTER TABLE openelec.procuration ADD COLUMN autorite_type character varying(250);
UPDATE openelec.procuration SET autorite_type='CSL' WHERE types='HF';
UPDATE openelec.procuration SET autorite_type='PN' WHERE autorite_nom_prenom ILIKE '%police%';
UPDATE openelec.procuration SET autorite_type='GN' WHERE autorite_nom_prenom ILIKE '%gendarm%';
UPDATE openelec.procuration SET autorite_type='TRIB' WHERE autorite_nom_prenom ILIKE '%tribunal%';
--
ALTER TABLE openelec.procuration ADD COLUMN notes text;
UPDATE openelec.procuration SET notes = types WHERE autorite_type IS NULL;
ALTER TABLE openelec.procuration DROP COLUMN types;
--
ALTER TABLE openelec.procuration ADD COLUMN annulation_autorite_nom_prenom character varying(250);
ALTER TABLE openelec.procuration ADD COLUMN annulation_autorite_commune character varying(250);
ALTER TABLE openelec.procuration ADD COLUMN annulation_autorite_consulat character varying(250);
ALTER TABLE openelec.procuration ADD COLUMN annulation_autorite_type character varying(250);
ALTER TABLE openelec.procuration ADD COLUMN annulation_date date;
--
UPDATE openelec.procuration SET statut='procuration_perimee' WHERE fin_validite < CURRENT_DATE AND statut='demande_a_transmettre';
UPDATE openelec.procuration SET mandant_resume=(SELECT 
    concat(nom, ' - ', prenom, ' - ', to_char(date_naissance, 'DD/MM/YYYY'), ' - ', liste_insee) 
    FROM openelec.electeur LEFT JOIN openelec.liste ON electeur.liste=liste.liste 
    WHERE electeur.id=procuration.mandant) WHERE mandant_resume IS NULL;
UPDATE openelec.procuration SET mandataire_resume=(SELECT 
    concat(nom, ' - ', prenom, ' - ', to_char(date_naissance, 'DD/MM/YYYY'), ' - ', liste_insee) 
    FROM openelec.electeur LEFT JOIN openelec.liste ON electeur.liste=liste.liste 
    WHERE electeur.id=procuration.mandataire) WHERE mandataire_resume IS NULL;
--

--
--
--
ALTER TABLE openelec.reu_livrable ADD COLUMN nom_naissance_mandataire_tour1 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN nom_usage_mandataire_tour1 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN prenoms_mandataire_tour1 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN sexe_mandataire_tour1 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN date_naissance_mandataire_tour1 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN nom_naissance_mandataire_tour2 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN nom_usage_mandataire_tour2 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN prenoms_mandataire_tour2 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN sexe_mandataire_tour2 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN date_naissance_mandataire_tour2 character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN date_validite_procuration character varying(255);
ALTER TABLE openelec.reu_livrable ADD COLUMN libelle_circonscription_metropolitaine_du_bureau_de_vote character varying(255);
