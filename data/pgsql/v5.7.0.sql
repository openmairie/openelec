--
-- Fichier de passage de version SQL : v5.6.x > v5.7.0
--
-- @package openelec
-- @version SVN : $Id$
--

--
ALTER TABLE openelec.mouvement ADD COLUMN IF NOT EXISTS provenance_demande character varying(255);
COMMENT ON COLUMN openelec.mouvement.provenance_demande IS 'Provenance de la demande REU liée au mouvement';
--

ALTER TABLE openelec.electeur ADD COLUMN IF NOT EXISTS a_transmettre boolean;
COMMENT ON COLUMN openelec.electeur.a_transmettre IS 'Marqueur pour le traitement de transmission par lot au REU';

-- 
DROP SEQUENCE openelec.mairieeurope_seq;
DROP TABLE openelec.mairieeurope;

--
CREATE TABLE openelec.reu_scrutin (
    id integer NOT NULL,
    type character varying(250),
    partiel boolean,
    libelle character varying(250),
    zone character varying(250),
    date_tour1 date,
    date_tour2 date,
    date_debut date,
    date_l30 date,
    date_fin date,
    j20_livrable_demande_id integer,
    j20_livrable_demande_date timestamp without time zone,
    j20_livrable character varying(100),
    j20_livrable_date timestamp without time zone,
    j5_livrable_demande_id integer,
    j5_livrable_demande_date timestamp without time zone,
    j5_livrable character varying(100),
    j5_livrable_date timestamp without time zone,
    emarge_livrable_demande_id integer,
    emarge_livrable_demande_date timestamp without time zone,
    emarge_livrable character varying(100),
    emarge_livrable_date timestamp without time zone,
    om_collectivite integer NOT NULL
);
ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_pkey PRIMARY KEY (id);
ALTER TABLE ONLY openelec.reu_scrutin
    ADD CONSTRAINT reu_scrutin_om_collectivite_fkey FOREIGN KEY (om_collectivite) REFERENCES openelec.om_collectivite(om_collectivite);

--
CREATE TABLE openelec.reu_livrable(
    type_de_mouvement character varying(255),
    libelle_du_type_de_liste character varying(255),
    nom_de_naissance character varying(255),
    nom_d_usage character varying(255),
    prenoms character varying(255),
    sexe character varying(255),
    date_de_naissance character varying(255),
    commune_de_naissance character varying(255),
    code_de_la_commune_de_naissance character varying(255),
    departement_de_naissance character varying(255),
    pays_de_naissance character varying(255),
    code_du_bureau_de_vote character varying(255),
    libelle_du_bureau_de_vote character varying(255),
    numero_d_ordre_dans_le_bureau_de_vote character varying(255),
    motif character varying(255),
    libelle_du_scrutin character varying(255),
    date_du_premier_tour character varying(255),
    date_du_second_tour character varying(255),
    numero_de_voie character varying(255),
    libelle_de_voie character varying(255),
    complement_1 character varying(255),
    complement_2 character varying(255),
    lieu_dit character varying(255),
    code_postal character varying(255),
    commune character varying(255),
    pays character varying(255),
    code_du_departement character varying(255),
    libelle_de_l_ugle character varying(255),
    code_de_departement_de_naissance character varying(255),
    majeur character varying(255),
    mention_code character varying(255),
    circonscription_du_bureau_de_vote character varying(255),
    canton_du_bureau_de_vote character varying(255),
    identifiant_nationalite character varying(255),
    libelle_de_la_nationalite character varying(255),
    procuration character varying(255),
    scrutin_id integer,
    demande_id integer,
    liste character varying(6),
    electeur_id integer,
    om_collectivite integer NOT NULL
);


