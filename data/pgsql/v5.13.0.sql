--
-- [BEGIN] -  [#10264] - Arrêts des listes sans scrutin
--

CREATE TABLE IF NOT EXISTS arrets_liste(
    arrets_liste INTEGER,
    livrable_demande_id INTEGER,
    livrable_demande_date DATE,
    livrable CHARACTER VARYING(100),
    livrable_date DATE,
    om_collectivite INTEGER,
    CONSTRAINT arrets_liste_pk PRIMARY KEY (arrets_liste),
    CONSTRAINT arrets_liste_om_collectivite_fkey
        FOREIGN KEY (om_collectivite) REFERENCES om_collectivite(om_collectivite)
);

CREATE SEQUENCE IF NOT EXISTS arrets_liste_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

COMMENT ON COLUMN arrets_liste.arrets_liste IS 'Identifiant unique de l''arret des listes.';
COMMENT ON COLUMN arrets_liste.livrable_demande_id IS 'Identifiant de la demande de livrable d''aret des listes.';
COMMENT ON COLUMN arrets_liste.livrable_demande_date IS 'Date de demande du livrable d''arret des listes.';
COMMENT ON COLUMN arrets_liste.livrable IS 'UID du livrable d''arret des listes.';
COMMENT ON COLUMN arrets_liste.livrable_date IS 'Date de reception du livrable d''arret des listes.';
COMMENT ON COLUMN arrets_liste.om_collectivite IS 'Collectivité pour laquelle l''arret des listes a été demandé.';

-- Liaison entre la table reu_scrutin et arrets_liste
ALTER TABLE ONLY reu_scrutin
    ADD COLUMN IF NOT EXISTS arrets_liste INTEGER,
    DROP CONSTRAINT IF EXISTS reu_scrutin_arrets_liste_fkey,
    ADD CONSTRAINT reu_scrutin_arrets_liste_fkey
        FOREIGN KEY (arrets_liste) REFERENCES arrets_liste(arrets_liste);

COMMENT ON COLUMN reu_scrutin.arrets_liste IS 'Identifiant de l''arrêt des listes associé.';

-- Reprise des données existante
-- Ajout d'un champ temporaire dans la table arrets_liste permettant de référencer l'élection d'origine
ALTER TABLE ONLY arrets_liste
    ADD COLUMN IF NOT EXISTS reu_scrutin_tmp INTEGER,
    DROP CONSTRAINT IF EXISTS arrets_liste_reu_scrutin_fkey,
    ADD CONSTRAINT arrets_liste_reu_scrutin_fkey
        FOREIGN KEY (reu_scrutin_tmp) REFERENCES reu_scrutin(id);

-- Ajout d'un arret des liste par élection en reprennant les informations issues de l'élection
-- et en stockant le numéro de l'élection
INSERT INTO arrets_liste (
    arrets_liste,
    livrable_demande_id,
    livrable_demande_date,
    livrable,
    livrable_date,
    om_collectivite,
    reu_scrutin_tmp)
SELECT
    nextval('arrets_liste_seq'),
    j20_livrable_demande_id,
    j20_livrable_demande_date,
    j20_livrable,
    j20_livrable_date,
    om_collectivite,
    reu_scrutin.id
FROM
    reu_scrutin;
-- Liaison entre scrutin et les arrêts des listes
UPDATE reu_scrutin
SET arrets_liste = arrets_liste.arrets_liste
FROM arrets_liste
WHERE arrets_liste.reu_scrutin_tmp = reu_scrutin.id;

-- Suppression du champ temporaire dans la table arrets_liste permettant de référencer l'élection d'origine
ALTER TABLE ONLY arrets_liste
    DROP COLUMN IF EXISTS reu_scrutin_tmp,
    DROP CONSTRAINT IF EXISTS arrets_liste_reu_scrutin_fkey;

-- Suppression des champs en relation avec les arrets des listes dans la table reu_scrutin
ALTER TABLE ONLY reu_scrutin
    DROP COLUMN IF EXISTS j20_livrable_demande_id,
    DROP COLUMN IF EXISTS j20_livrable_demande_date,
    DROP COLUMN IF EXISTS j20_livrable,
    DROP COLUMN IF EXISTS j20_livrable_date;

--
-- [END] -  [#10264] - Arrêts des listes sans scrutin
--